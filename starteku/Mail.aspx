<%@ Page Language="C#" %>
<%@ Import Namespace="System.Net.Mail"%>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">

    protected void Button1_Click(object sender, EventArgs e)
    {
        MailMessage mail = new MailMessage();
        mail.From = new MailAddress(txtFrom.Text, txtFromName.Text);
        mail.Sender = new MailAddress(txtFrom.Text, txtFromName.Text);
        
        mail.To.Add(txtTo.Text);
        mail.IsBodyHtml = true;
        mail.Subject = "This is an email";
        mail.Body = txtMessage.Text;
        SmtpClient smtp = new SmtpClient(txtSMTP.Text);
        if (RadioButton1.Checked == true)
        {
            smtp.UseDefaultCredentials = true;
        }
        else
        {
            System.Net.NetworkCredential basicAuthenticationInfo = new System.Net.NetworkCredential(txtNetUser.Text,txtNetPass.Text);
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = basicAuthenticationInfo;
        }
        try
        {
            smtp.Send(mail);
            Response.Write("Mail sent");
        }
        catch (Exception ex)
        {
            Response.Write("Error in Mail sent");
            Response.Write(ex.Message);
        }

    }
</script>

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Send Mail</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
     <table style="position: relative">
            <tr>
                <td style="width: 186px; height: 26px">
                    From</td>
                <td style="width: 100px; height: 26px">
                    <asp:TextBox ID="txtFrom" runat="server" Style="position: relative"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width: 186px; height: 26px">
                    display name</td>
                <td style="width: 100px; height: 26px">
                    <asp:TextBox ID="txtFromName" runat="server" Style="position: relative"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width: 186px">
                    To</td>
                <td style="width: 100px">
                    <asp:TextBox ID="txtTo" runat="server" Style="position: relative"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width: 186px">
                    SMTP</td>
                <td style="width: 100px">
                    <asp:TextBox ID="txtSMTP" runat="server" Style="position: relative"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width: 186px">
                    Network Cre. User Name</td>
                <td style="width: 100px">
                    <asp:TextBox ID="txtNetUser" runat="server" Style="position: relative"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width: 186px">
                    Network Cre. Password</td>
                <td style="width: 100px">
                    <asp:TextBox ID="txtNetPass" runat="server" Style="position: relative"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width: 186px">
                    Use Default Authentcation?</td>
                <td style="width: 100px">
                    &nbsp;<asp:RadioButton ID="RadioButton1" runat="server" Checked="True" GroupName="auth" Text="Yes" />
                    <asp:RadioButton ID="RadioButton2" runat="server"  GroupName="auth"
                        Text="No" /></td>
            </tr>
            <tr>
                <td style="width: 186px; height: 42px;">
                    Msg</td>
                <td style="width: 100px; height: 42px;">
                    <asp:TextBox ID="txtMessage" runat="server" Height="112px" Style="position: relative"
                        TextMode="MultiLine"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width: 186px">
                </td>
                <td style="width: 100px">
                    <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Style="position: relative"
                        Text="Send" /></td>
            </tr>
        </table>

    </div>
    </form>
</body>
</html>
