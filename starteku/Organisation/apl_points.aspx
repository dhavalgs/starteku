﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="apl_points.aspx.cs" Inherits="Organisation_apl_points" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .yellow {
            border-radius: 0px;
        }

        body {
            font-family: Geneva,Arial,Helvetica,sans-serif;
        }

        #ContentPlaceHolder1_chkList input {
            width: 33px;
            margin-bottom: 0px !important;
        }

        #ContentPlaceHolder1_chkList label {
            margin-top: 2px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:HiddenField ID="hdnsignin" runat="server" meta:resourcekey="signin" EnableViewState="false" />
    <asp:HiddenField ID="hdnuploaddoc" runat="server" meta:resourcekey="uploaddoc" EnableViewState="false" />
    <asp:HiddenField ID="hdnreaddoc" runat="server" meta:resourcekey="readdoc" EnableViewState="false" />
    <asp:HiddenField ID="hdnopencomscreen" runat="server" meta:resourcekey="opencomscreen" EnableViewState="false" />
    <asp:HiddenField ID="hdnfinalfillcom" runat="server" meta:resourcekey="finalfillcom" EnableViewState="false" />
    <asp:HiddenField ID="hdnreqsession" runat="server" meta:resourcekey="reqsession" EnableViewState="false" />
    <asp:HiddenField ID="hdnsuggsession" runat="server" meta:resourcekey="suggsession" EnableViewState="false" />

    <asp:HiddenField ID="hdnacchelp" runat="server" meta:resourcekey="AcceptProvidedHelp" EnableViewState="false" />
    <asp:HiddenField ID="hdnreaddocbyother" runat="server" meta:resourcekey="ReadDocByOther" EnableViewState="false" />

    <asp:HiddenField ID="hdnGotPDP" runat="server" meta:resourcekey="hdnGotPDP" EnableViewState="false" />
    <asp:HiddenField ID="hdnMovingLevel" runat="server" meta:resourcekey="hdnMovingLevel" EnableViewState="false" />

    <div class="">
        <div class="container">
            <div class="col-md-6">
                <div class="heading-sec">
                    <h1>
                        <%--   <%= CommonMessages.Point%>--%>
                        <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Point" EnableViewState="false" />
                        <i><span runat="server" id="Points"></span></i>
                    </h1>
                </div>
            </div>
            <!--<div class="col-md-6 range ">
				 <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
                   <i class="fa fa-calendar-o icon-calendar icon-large"></i>
                   <span>August 5, 2014 - September 3, 2014</span>  <b class="caret"></b>                </div>-->
            <div class="col-md-12">
                <%--<div class="col-md-2" style="height: 50px;">
                    <div class="stat-boxes widget-body real-time alp-point-round" style="border-radius: 5px;">
                        <a href="apl_points.aspx?ALP=1">
                            <h3>
                                <asp:Label ID="lblALP" runat="server" Text="0"></asp:Label>
                            </h3>
                            <i>ALP</i> </a>
                    </div>
                </div>--%>
                <%--<div class="col-md-2 ">
                    <div class="stat-boxes widget-body real-time plp-point-round " style="border-radius: 5px;">
                        <a href="apl_points.aspx?PLP=1">
                            <h3>
                                <asp:Label ID="lblPLP" runat="server" Text="0"></asp:Label></h3>
                            <i>PLP</i></a>
                    </div>
                </div>--%>
                <%--<div class="col-md-2">
                    <div class="stat-boxes widget-body real-time pdp-point-round" style="border-radius: 5px;">
                        <a href="pdp_points.html">
                            <h3>
                               <asp:Label ID="lblTotal" runat="server"></asp:Label></h3>
                            <i>PDP</i> </a>
                    </div>
                </div>--%>
                <%-- <div class="col-md-2">
                    <div class="stat-boxes widget-body real-time total-point-round " style="border-radius: 5px;">
                        <a href="apl_points.aspx?All=1">
                            <h3>
                                <asp:Label ID="lblTotal" runat="server" Text="0"></asp:Label>
                            </h3>
                            <i>Total</i> </a>
                    </div>
                </div>--%>
                <div class="col-md-12" style="margin-left: -10px; margin-top: 23px;">
                    <div class="col-md-2 ">
                        <div class="stat-boxes widget-body real-time plp-point-round " style="border-radius: 5px;">
                            <a href="#" onclick="showHideDiv('ALP');">
                                <h3>
                                    <asp:Label ID="lblALP" runat="server" Text="0"
                                        meta:resourcekey="lblALPResource1"></asp:Label>
                                </h3>
                                <h4>
                                    <i>ALP</i></h4>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-2" style="height: 50px;">
                        <div class="stat-boxes widget-body real-time alp-point-round" style="border-radius: 5px;">
                            <a href="#" onclick="showHideDiv('PLP');">
                                <h3>
                                    <asp:Label ID="lblPLP" runat="server" Text="0"
                                        meta:resourcekey="lblPLPResource1"></asp:Label>
                                </h3>
                                <h4>
                                    <i>PLP</i>
                                </h4>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="stat-boxes widget-body real-time pdp-point-round " style="border-radius: 5px; height: 75px;">
                            <a href="#" onclick="showHideDiv('All');">
                                <h3>
                                    <asp:Label ID="lblTotal" runat="server" Text="0"
                                        meta:resourcekey="lblTotalResource1"></asp:Label>
                                </h3>
                                <h4>
                                    <i>Total</i></h4>
                            </a>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="stat-boxes widget-body real-time pdp-point-round " style="border-radius: 5px; height: 75px;">
                            <a href="#" onclick="showHideDiv('PDP');">
                                <h3>
                                    <asp:Label ID="lblpdp" runat="server" Text="0"
                                        meta:resourcekey="lblTotalResource1"></asp:Label>
                                </h3>
                                <h4>
                                    <i>PDP</i></h4>
                            </a>
                        </div>
                    </div>

                    <%--   <div class="col-md-2">
                    <div class="stat-boxes widget-body real-time total-point-round " style="border-radius: 5px;">
                        <a href="apl_points.aspx?All=1">
                            <h3>
                                <asp:Label ID="lblTotal" runat="server" Text="0"></asp:Label>
                            </h3>
                            <i>Total</i> </a>
                    </div>
                </div>--%>
                </div>
                <div class="col-md-12">
                    <div class="invoice ALP" style="margin-top: 20px; display: none;" id="ALP" runat="server">
                        <h2 class="pointtable_h2" style="margin-bottom: -11px; width: 78%;">

                            <span><%-- <%= CommonMessages.ActiveLearningPoints%> --%>
                                <asp:Literal ID="Literal2" runat="server" meta:resourcekey="ActiveLearningPoints" EnableViewState="false" />
                            </span>
                            <asp:Label runat="server" ID="alpUser"></asp:Label>
                            <span id="AAlp" runat="server" style="float: right;"></span>
                            <%--Active Learning Points--%>
                            <%-- <asp:Label ID="lblname" runat="server" />--%></h2>
                        <%--<div class="invoice-head chat-widget-head yellow" style="border-radius: 0px;">
                            <h2 class="description">
                                Session</h2>
                            <h2 class="date">
                                Date
                            </h2>
                            <h2 class="date">
                                Points</h2>
                        </div>--%>
                        <!-- Invoice Table Head -->
                        <asp:GridView ID="gvGrid" runat="server" AutoGenerateColumns="False" CellPadding="0"
                            Width="100%" GridLines="None" DataKeyNames="logUserId" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                            EmptyDataText='<%# Convert.ToString(GetLocalResourceObject("RecordNotfoundResource.Text")) %>'
                            BackColor="White" meta:resourcekey="gvGridResource1">
                            <HeaderStyle CssClass="aa" />
                            <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:TemplateField HeaderText="Session"
                                    meta:resourcekey="TemplateFieldResource1">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUNamer" runat="server" Text="<%# bind('logPointInfo') %>"
                                            meta:resourcekey="lblUNamerResource1"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="100px" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="50%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>
                                <%--<asp:TemplateField HeaderText="Date">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUNamer1" runat="server" Text="<%# bind('logCreateDate') %>"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>--%>
                                <asp:TemplateField HeaderText="Point" meta:resourcekey="TemplateFieldResource2">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserNamer1" runat="server" Text="<%# bind('TotalPoint') %>"
                                            meta:resourcekey="lblUserNamer1Resource1"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="View Detail"
                                    meta:resourcekey="TemplateFieldResource3">
                                    <ItemTemplate>
                                        <%--<asp:Button runat="server" ID="btnDivision" Text="Detail" CssClass="yellow"
                                            Style="border-radius: 5px; height: 30px; width: 75px; color: white;" 
                                            PostBackUrl='<%# String.Format("DetailPoints.aspx?ALP={0}&Uid={1}", Eval("logPointInfo"), Eval("logUserId")) %>' 
                                            meta:resourcekey="btnDivisionResource1"/>--%>
                                        <asp:Button runat="server" ID="btnDivision" Text="Detail" CssClass="yellow"
                                            Style="border-radius: 5px; height: 30px; width: 75px; color: white;"
                                            meta:resourcekey="btnDivisionResource1" CommandArgument='<%#Eval("logPointInfoDBValue")+","+ Eval("logUserId")%>' CommandName="ALP"
                                            OnCommand="btnDivision_Click" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                        <!-- Invoice Table Structure -->
                    </div>
                    <div class="invoice PLP" style="margin-top: 20px; display: none" id="PLP" runat="server">
                        <h2 class="pointtable_h2" style="margin-bottom: -11px; width: 78%;">

                            <span><%--<%= CommonMessages.PassiveLearningPoints%> --%>
                                <asp:Literal ID="Literal3" runat="server" meta:resourcekey="PassiveLearningPoints" EnableViewState="false" />
                            </span>
                            <asp:Label runat="server" ID="plpUser"></asp:Label>
                            <span id="PPpl" runat="server" style="float: right;"></span>
                            <%-- Passive Learning Points--%></h2>
                        <%--<div class="invoice-head chat-widget-head yellow" style="border-radius: 0px;">
                            <h2 class="description">
                                Session</h2>
                            <h2 class="date">
                                Date
                            </h2>
                            <h2 class="date">
                                Points</h2>
                        </div>--%>
                        <!-- Invoice Table Head -->
                        <asp:GridView ID="gvplp" runat="server" AutoGenerateColumns="False" CellPadding="0"
                            Width="100%" GridLines="None" DataKeyNames="logUserId" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                            EmptyDataText='<%# Convert.ToString(GetLocalResourceObject("RecordNotfoundResource.Text")) %>'
                            BackColor="White" meta:resourcekey="gvplpResource1">
                            <HeaderStyle CssClass="aa" />
                            <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:TemplateField HeaderText="Session"
                                    meta:resourcekey="TemplateFieldResource4">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUNamer" runat="server" Text="<%# bind('logPointInfo') %>"
                                            meta:resourcekey="lblUNamerResource2"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="100px" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="50%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>
                                <%--<asp:TemplateField HeaderText="Date">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUNamer1" runat="server" Text="<%# bind('logCreateDate') %>"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>--%>
                                <asp:TemplateField HeaderText="Point" meta:resourcekey="TemplateFieldResource5">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserNamer1" runat="server" Text="<%# bind('TotalPoint') %>"
                                            meta:resourcekey="lblUserNamer1Resource2"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="View Detail"
                                    meta:resourcekey="TemplateFieldResource6">
                                    <ItemTemplate>
                                        <%--  <asp:Button runat="server" ID="btnDivision" Text="Detail" CssClass="yellow"
                                            Style="border-radius: 5px; height: 30px; width: 75px; color: white;"  
                                            PostBackUrl='<%# String.Format("DetailPoints.aspx?PLP={0}&Uid={1}", Eval("logPointInfo"), Eval("logUserId")) %>' 
                                            meta:resourcekey="btnDivisionResource2"/>--%>
                                        <asp:Button runat="server" ID="btnDivision" Text="Detail" CssClass="yellow"
                                            Style="border-radius: 5px; height: 30px; width: 75px; color: white;"
                                            meta:resourcekey="btnDivisionResource2" CommandArgument='<%#Eval("logPointInfoDBValue")+","+ Eval("logUserId")%>' CommandName="PLP"
                                            OnCommand="btnDivision_Click" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                        <!-- Invoice Table Structure -->
                    </div>
                    <div class="invoice ALL" style="margin-top: 20px; display: none;" id="All" runat="server">
                        <h2 class="pointtable_h2" style="margin-bottom: -11px; width: 78%;">
                            <span><%--<%= CommonMessages.Point%>--%>
                                <asp:Literal ID="Literal4" runat="server" meta:resourcekey="TotalPoint" EnableViewState="false" /></span>
                            <asp:Label runat="server" ID="allUserName"></asp:Label>
                            <span id="PAll" runat="server" style="float: right"></span>
                            <%--Total Points--%></h2>
                        <%--<div class="invoice-head chat-widget-head yellow" style="border-radius: 0px;">
                            <h2 class="description">
                                Session</h2>
                            <h2 class="date">
                                Date
                            </h2>
                            <h2 class="date">
                                Points</h2>
                        </div>--%>
                        <!-- Invoice Table Head -->
                        <asp:GridView ID="gvall" runat="server" AutoGenerateColumns="False" CellPadding="0"
                            Width="100%" GridLines="None" DataKeyNames="logUserId" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                            EmptyDataText='<%# Convert.ToString(GetLocalResourceObject("RecordNotfoundResource.Text")) %>'
                            BackColor="White" meta:resourcekey="gvallResource1" OnDataBound="gvall_DataBound">
                            <HeaderStyle CssClass="aa" />
                            <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:TemplateField HeaderText="Session"
                                    meta:resourcekey="TemplateFieldResource7">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUNamer" runat="server" Text="<%# bind('logPointInfo') %>"
                                            meta:resourcekey="lblUNamerResource3"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="100px" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="50%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>
                                <%--<asp:TemplateField HeaderText="Date">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUNamer1" runat="server" Text="<%# bind('logCreateDate') %>"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>--%>
                                <asp:TemplateField HeaderText="Point" meta:resourcekey="TemplateFieldResource8">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserNamer1" runat="server" Text="<%# bind('TotalPoint') %>"
                                            meta:resourcekey="lblUserNamer1Resource3"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="View Detail"
                                    meta:resourcekey="TemplateFieldResource9">
                                    <ItemTemplate>
                                        <%--  <asp:Button runat="server" ID="btnDivision" Text="Detail" CssClass="yellow"
                                            Style="border-radius: 5px; height: 30px; width: 75px; color: white;"  
                                            PostBackUrl='<%# String.Format("DetailPoints.aspx?All={0}&Uid={1}", Eval("logPointInfo"), Eval("logUserId")) %>' 
                                            meta:resourcekey="btnDivisionResource3"/>--%>
                                        <asp:Button runat="server" ID="btnDivision" Text="Detail" CssClass="yellow"
                                            Style="border-radius: 5px; height: 30px; width: 75px; color: white;"
                                            meta:resourcekey="btnDivisionResource3" CommandArgument='<%#Eval("logPointInfoDBValue")+","+ Eval("logUserId")%>' CommandName="All"
                                            OnCommand="btnDivision_Click" />
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                        <!-- Invoice Table Structure -->
                    </div>
                    <div class="invoice PDP" style="margin-top: 20px; display: none;" id="PDP" runat="server">
                        <h2 class="pointtable_h2" style="margin-bottom: -11px; width: 78%;">

                            <span><%--<%= CommonMessages.personaldevelopmentpoints%>--%>
                                <asp:Literal ID="Literal5" runat="server" meta:resourcekey="personaldevelopmentpoints" EnableViewState="false" /></span>
                            <asp:Label runat="server" ID="pdpUser"></asp:Label>
                            <span id="spdp" runat="server" style="float: right;"></span></h2>

                        <%--  <asp:GridView ID="grpdp" runat="server" AutoGenerateColumns="False" CellPadding="0"
                            Width="100%" GridLines="None" DataKeyNames="comId" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                           EmptyDataText='<%# Convert.ToString(GetLocalResourceObject("RecordNotfoundResource.Text")) %>'
                            BackColor="White" meta:resourcekey="gvGridResource1">
                            <HeaderStyle CssClass="aa" />
                            <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:TemplateField HeaderText="Competence" meta:resourcekey="TemplateFieldResource10">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUNamer" runat="server" Text="<%# bind('comCompetence') %>"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="100px" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="50%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>                               
                                <asp:TemplateField HeaderText="Point" meta:resourcekey="TemplateFieldResource11">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserNamer1" runat="server" Text="<%# bind('PDP') %>"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="20%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>
                                
                            </Columns>
                        </asp:GridView>--%>

                        <asp:GridView ID="grpdp" runat="server" AutoGenerateColumns="False" CellPadding="0"
                            Width="100%" GridLines="None" DataKeyNames="comId" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                            EmptyDataText='<%# Convert.ToString(GetLocalResourceObject("RecordNotfoundResource.Text")) %>'
                            BackColor="White" meta:resourcekey="gvGridResource1"
                            OnRowDataBound="grpdp_RowDataBound">
                            <HeaderStyle CssClass="aa" />
                            <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:TemplateField HeaderText="Competence" Visible="true" meta:resourcekey="TemplateFieldResource10">
                                    <ItemTemplate>
                                          <asp:HiddenField runat="server" ID="hdnPDPDescription" Value='<%#Eval("pdpDescription") %>' />
                                        <asp:Label ID="lblCompetenceName" runat="server" Text="<%# bind('comCompetence') %>"></asp:Label>
                                         <asp:Label ID="lblCompetenceNameDN" runat="server" Text="<%# bind('comCompetenceDN') %>"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="100px" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="50%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>

                              <%--  <asp:TemplateField HeaderText="Competence" Visible="false" meta:resourcekey="TemplateFieldResource10">
                                    <ItemTemplate>
                                        <asp:Label ID="lblcomDN" runat="server" Text="<%# bind('comCompetenceDN') %>"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="100px" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="50%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>--%>

                                <asp:TemplateField HeaderText="Point" meta:resourcekey="TemplateFieldResource11">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserNamer1" runat="server" Text="<%# bind('PDP') %>"></asp:Label>
                                        <%-- <asp:Label ID="lblUserNamer1" runat="server" ></asp:Label>--%>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="20%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>

                            </Columns>
                        </asp:GridView>

                        <!-- Invoice Table Structure -->
                    </div>
                    <asp:Button runat="server" ID="Button2" class="btn btn-default black pull-right" Text="Back"
                        OnClick="Button2_Click" meta:resourcekey="Button2Resource1"></asp:Button>
                    <%--<a href="#" onclick="javascript:history.back(); return false;" class="btn black pull-right" style="border-radius: 5px;margin-left:0px;margin-top:5px;float:right;">Back</a>--%>
                </div>
            </div>
        </div>
    </div>
    <script src="../assets/assets/js/libs/jquery-1.10.2.min.js"></script>
    <script type="text/jscript">
        $(document).ready(function () {
            // SetExpandCollapse();
            setTimeout(function () {
                $(".active").click();
                onLoad();
                // tabMenuClick();
                // $(".myCompetence").addClass("active_page");
            }, 500);
        });



        function showHideDiv(divName) {
            if (divName == "PLP") {
                $('.PLP').show();
                $('.ALP').hide();
                $('.ALL').hide();
                $('.PDP').hide();

            } else if (divName == "ALP") {
                $('.PLP').hide();
                $('.ALP').show();
                $('.ALL').hide();
                $('.PDP').hide();
            }
            else if (divName == "PDP") {
                $('.PLP').hide();
                $('.ALP').hide();
                $('.ALL').hide();
                $('.PDP').show();
            }
            else {
                $('.ALL').show();
                $('.PLP').hide();
                $('.ALP').hide();
                $('.PDP').hide();
            }
        }
        function onLoad() {
            setTimeout(function () {
                ExampanCollapsMenu();

                // tabMenuClick();
                $(".pending_helprequest").addClass("active_page");
            }, 500);
        }

    </script>
</asp:Content>
