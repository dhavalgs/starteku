﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using System.Configuration;
using starteku_BusinessLogic.Model;
using startetku.Business.Logic;
using System.Web.UI.HtmlControls;
using System.Threading;
using System.Globalization;
using System.Drawing;


public partial class Organisation_TeamTargetView : System.Web.UI.Page
{
    string temp = "0";
    static string temp1 = "0";
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {


            if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
            {
                Response.Redirect("login.aspx");
            }

            if (!IsPostBack)
            {
                temp = Convert.ToString(Session["OrgUserId"]);
                temp1 = temp;

                Employee.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
                GetAllTeamCompanyId();
                GetAllEmployeeList();

            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "Organisation_TeamTargetView->Page_Load");
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion


    protected void ddActCate_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GetAllEmployeeList();
        }
        catch (Exception ex)
        {

            ExceptionLogger.LogException(ex, 0, "ddActCate_OnSelectedIndexChanged");
        }
    }


    #region Team
    protected void GetAllTeamCompanyId()
    {
        try
        {
            JobTypeBM obj = new JobTypeBM();
            obj.TeamIDs = "0";
            obj.jobIsActive = true;
            obj.jobIsDeleted = false;
            obj.jobCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.GetAllTeam();
            DataSet ds = obj.ds;

            ViewState["ds"] = ds;

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlTeam.DataSource = ds.Tables[0];
                    if (Convert.ToString(Session["Culture"]) == "Danish")
                    {
                        ddlTeam.DataTextField = "TeamNameDN";
                    }
                    else
                    {
                        ddlTeam.DataTextField = "TeamName";
                    }
                    ddlTeam.DataValueField = "TeamID";
                    ddlTeam.DataBind();
                    ddlTeam.Items.Insert(0, new ListItem(GetLocalResourceObject("SelectTeam.Text").ToString(), "0"));
                }

            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "GetAllTeamCompanyId");
        }
    }

    #endregion

    #region method
    public void getData()
    {
        //CompetenceMasterBM obj = new CompetenceMasterBM();
        //obj.GetAllCategoryBypenddingrequest(Convert.ToInt32(temp));
        //DataSet ds = obj.ds;
        //{
        //    if (ds.Tables[0].Rows.Count > 0)
        //    {
        //        Accordion1.DataSource = ds.Tables[0].DefaultView;
        //        Accordion1.DataBind();

        //        if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[1].Rows[0]["userFirstName"])))
        //        {
        //            spname.InnerHtml = Convert.ToString(ds.Tables[1].Rows[0]["userFirstName"]);
        //        }

        //    }
        //}

    }
    protected void InsertNotification()
    {
        try
        {


            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                temp = Convert.ToString(Request.QueryString["id"]);
                temp1 = temp;
            }
            else if (!String.IsNullOrEmpty(Request.QueryString["not"]))
            {
                temp = Convert.ToString(Request.QueryString["not"]);
                temp1 = temp;
            }
            if (!String.IsNullOrEmpty(Request.QueryString["rid"]))
            {
                temp = Convert.ToString(Request.QueryString["rid"]);
                temp1 = temp;
            }

            UserBM obj1 = new UserBM();
            obj1.SetUserId = Convert.ToInt32(temp);
            obj1.GetUserSettingById();
            DataSet ds1 = obj1.ds;
            if (ds1.Tables[0].Rows.Count > 0)
            {
                if (!String.IsNullOrEmpty(Convert.ToString(ds1.Tables[0].Rows[0]["SetInvitationNotification"])))
                {
                    if (Convert.ToBoolean(ds1.Tables[0].Rows[0]["SetNewFileUploadwarning"]))
                    {

                        NotificationBM obj = new NotificationBM();
                        obj.notsubject = "Has Accepted Competence.";
                        obj.notUserId = Convert.ToInt32(Session["OrgUserId"]);
                        obj.notIsActive = true;
                        obj.notIsDeleted = false;
                        obj.notCreatedDate = DateTime.Now;
                        obj.notCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
                        obj.notpage = "CompetenceSkill.aspx?id=" + Convert.ToString(Session["OrgUserId"]);
                        obj.nottype = "UserAcceptCompetence-Notification";
                        obj.notToUserId = Convert.ToInt32(temp);
                        obj.InsertNotification();
                        // sendmail(obj.notToUserId);
                    }

                }
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "InsertNotification");
        }
    }
    protected void updatenotification()
    {
        try
        {


            NotificationBM obj = new NotificationBM();
            obj.notIsActive = false;
            obj.notUpdatedDate = DateTime.Now;
            obj.notUserId = Convert.ToInt32(Request.QueryString["not"]);
            obj.nottype = "Notification";
            obj.UpdateNotification();
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "updatenotification");
        }
    }
    protected void SetDefaultMessage()
    {
        // lblMsg.Text = GetLocalResourceObject("lblHelloWorld.Text").ToString();

        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["msg"])))
        {
            if (Convert.ToString(Request.QueryString["msg"]) == "del")
            {
                //lblMsg.Text = CommonModule.msgRecordHasBeenDeletedSuccessfully;
                //lblMsg.Text = CommonMessages.msgRecordHasBeenDeletedSuccessfully;
                lblMsg.Text = GetLocalResourceObject("msgRecordHasBeenDeletedSuccessfully.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "ins")
            {
                //lblMsg.Text = CommonMessages.msgRecordInsertedSuccessfully;
                lblMsg.Text = GetLocalResourceObject("msgRecordInsertedSuccessfully.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "upd")
            {
                //lblMsg.Text = CommonMessages.msgRecordUpdatedSuccss;
                lblMsg.Text = GetLocalResourceObject("msgRecordUpdatedSuccss.Text").ToString();
                lblMsg.ForeColor = System.Drawing.Color.White;
            }

        }

    }
    protected void GetAllEmployeeList()
    {
        try
        {
            EmployeeSkillBM obj = new EmployeeSkillBM();
            obj.skillIsActive = true;
            obj.skillIsDeleted = false;
            if (Convert.ToString(Session["OrguserType"]) == "1")
            {
                obj.skillStatus = 0;
            }
            else
            {
                obj.skillStatus = 3;
            }
            obj.skillUserId = Convert.ToInt32(Session["OrgUserId"]);

            obj.skillTeamID = Convert.ToInt32(ddlTeam.SelectedItem.Value);

            obj.GetTeamTargetSkillRequest();

            DataSet ds = obj.ds;
            ViewState["data"] = ds;
            if (ds != null && ds.Tables.Count>0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvGrid.DataSource = ds.Tables[0];
                    gvGrid.DataBind();
                    gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;

                }
                else
                {
                    gvGrid.DataSource = null;
                    gvGrid.DataBind();
                }


                if (ds.Tables[0].Rows.Count > 0)
                {
                    gridTotalPoint.DataSource = ds.Tables[1];
                    gridTotalPoint.DataBind();
                    gridTotalPoint.HeaderRow.TableSection = TableRowSection.TableHeader;

                }
                else
                {
                    gridTotalPoint.DataSource = null;
                    gridTotalPoint.DataBind();
                }

            }
            else
            {
                gvGrid.DataSource = null;
                gvGrid.DataBind();
                gridTotalPoint.DataSource = null;
                gridTotalPoint.DataBind();
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "GetAllEmployeeList");
        }
    }


    #endregion

    //#region Button

    //protected void btnback_click(object sender, EventArgs e)
    //{
    //    if (!String.IsNullOrEmpty(Request.QueryString["view"]))
    //    {
    //        Response.Redirect("pending_request.aspx");
    //    }
    //    else
    //    {
    //        Response.Redirect("pending_helprequest.aspx");
    //    }
    //}
    //protected void btnCancel_click(object sender, EventArgs e)
    //{
    //    if (!String.IsNullOrEmpty(Request.QueryString["id"]))
    //    {
    //        Response.Redirect("pending_request.aspx");
    //    }
    //    else
    //    {
    //        Response.Redirect("pending_helprequest.aspx");
    //    }
    //}
    //#endregion
    #region Button
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        int i = 0;
        int ttID = 0;
        int tSkillComID = 0;
        int teamActID = 0;
        int UserID = 0;
        var db = new startetkuEntities1();
        try
        {
            var userid = Convert.ToString(Session["OrgUserId"]);
            for (i = 0; i < gvGrid.Rows.Count; i++)
            {
                if (gvGrid.Rows[i].RowType == DataControlRowType.DataRow)
                {
                    HiddenField skillComId = (HiddenField)gvGrid.Rows[i].FindControl("skillComId");
                    HiddenField tID = (HiddenField)gvGrid.Rows[i].FindControl("tID");

                    ttID = Convert.ToInt32(tID.Value);
                    tSkillComID = Convert.ToInt32(skillComId.Value);
                    TextBox txtTargetOne = (TextBox)gvGrid.Rows[i].FindControl("txtTargetOne");
                    TextBox txtTargetTwo = (TextBox)gvGrid.Rows[i].FindControl("txtTargetTwo");
                    TextBox txtTargetThree = (TextBox)gvGrid.Rows[i].FindControl("txtTargetThree");
                    TextBox txtTargetFour = (TextBox)gvGrid.Rows[i].FindControl("txtTargetFour");
                    TextBox txtTargetFive = (TextBox)gvGrid.Rows[i].FindControl("txtTargetFive");


                    var tTarget = db.TeamTargetMasters.FirstOrDefault(o => o.tID == ttID && o.tskillComId == tSkillComID);
                    if (tTarget != null)
                    {
                        if (tTarget.TargetOne != Convert.ToInt32(txtTargetOne.Text) || tTarget.TargetTwo != Convert.ToInt32(txtTargetTwo.Text) ||
                             tTarget.TargetThree != Convert.ToInt32(txtTargetThree.Text) || tTarget.TargetFour != Convert.ToInt32(txtTargetFour.Text)
                            || tTarget.TargetFive != Convert.ToInt32(txtTargetFive.Text))
                        {
                            tTarget.TargetOne = Convert.ToInt32(txtTargetOne.Text);
                            tTarget.TargetTwo = Convert.ToInt32(txtTargetTwo.Text);
                            tTarget.TargetThree = Convert.ToInt32(txtTargetThree.Text);
                            tTarget.TargetFour = Convert.ToInt32(txtTargetFour.Text);
                            tTarget.TargetFive = Convert.ToInt32(txtTargetFive.Text);
                            db.SaveChanges();
                        }
                    }
                }
            }

            for (i = 0; i < gridTotalPoint.Rows.Count; i++)
            {
                if (gridTotalPoint.Rows[i].RowType == DataControlRowType.DataRow)
                {
                    HiddenField skillUserID = (HiddenField)gridTotalPoint.Rows[i].FindControl("skillUserID");
                    HiddenField hdnteamActID = (HiddenField)gridTotalPoint.Rows[i].FindControl("hdnteamActID");

                    teamActID = Convert.ToInt32(hdnteamActID.Value);
                    UserID = Convert.ToInt32(skillUserID.Value);

                    TextBox txtPLPTarget = (TextBox)gridTotalPoint.Rows[i].FindControl("txtPLPTarget");
                    TextBox txtALPTarget = (TextBox)gridTotalPoint.Rows[i].FindControl("txtALPTarget");
                    TextBox txtPointTotalTarget = (TextBox)gridTotalPoint.Rows[i].FindControl("txtPointTotalTarget");
                    TextBox txtPDPTarget = (TextBox)gridTotalPoint.Rows[i].FindControl("txtPDPTarget");


                    var tTarget = db.TeamTargetActualMasters.FirstOrDefault(o => o.UserID == UserID && o.teamActID == teamActID);
                    if (tTarget != null)
                    {
                        if (tTarget.TargetPLP != Convert.ToInt32(txtPLPTarget.Text) || tTarget.TargetALP != Convert.ToInt32(txtALPTarget.Text) ||
                             tTarget.TargetTotal != Convert.ToInt32(txtPointTotalTarget.Text) || tTarget.PDPTarget != Convert.ToInt32(txtPDPTarget.Text)
                            )
                        {
                            tTarget.TargetPLP = Convert.ToInt32(txtPLPTarget.Text);
                            tTarget.TargetALP = Convert.ToInt32(txtALPTarget.Text);
                            tTarget.TargetTotal = Convert.ToInt32(txtPointTotalTarget.Text);
                            tTarget.PDPTarget = Convert.ToInt32(txtPDPTarget.Text);
                            db.SaveChanges();
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {

        }
        Response.Redirect("TeamTargetView.aspx");
    }
    protected void btnback_click(object sender, EventArgs e)
    {
        if (Convert.ToString(Session["OrguserType"]) == "2")
        {
            Response.Redirect("add_employee.aspx");
        }
        else
        {
            Response.Redirect("AllManager.aspx");
        }
    }
    protected void btnCancel_click(object sender, EventArgs e)
    {
        if (Convert.ToString(Session["OrguserType"]) == "2")
        {
            Response.Redirect("add_employee.aspx");
        }
        else
        {
            Response.Redirect("AllManager.aspx");
        }
    }
    #endregion
    #region Grid Event

    protected void gvGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            DataSet ds = (DataSet)ViewState["data"];
            double totalActual = 0;
            double totalTarget = 0;
            double ActualAvg = 0;
            double TargetAvg = 0;
            int act = 0;
            int a = 0;
            int tar = 0;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label ActualOne = (Label)e.Row.FindControl("txtActualOne");
                Label ActualTwo = (Label)e.Row.FindControl("txtActualTwo");
                Label ActualThree = (Label)e.Row.FindControl("txtActualThree");
                Label ActualFour = (Label)e.Row.FindControl("txtActualFour");
                Label ActualFive = (Label)e.Row.FindControl("txtActualFive");

                TextBox TargetOne = (TextBox)e.Row.FindControl("txtTargetOne");
                TextBox TargetTwo = (TextBox)e.Row.FindControl("txtTargetTwo");
                TextBox TargetThree = (TextBox)e.Row.FindControl("txtTargetThree");
                TextBox TargetFour = (TextBox)e.Row.FindControl("txtTargetFour");
                TextBox TargetFive = (TextBox)e.Row.FindControl("txtTargetFive");

                Label TotalActual = (Label)e.Row.FindControl("txtTotalActual");
                Label TotalTarget = (Label)e.Row.FindControl("txtTotalTarget");

                Label ActualAverage = (Label)e.Row.FindControl("txtActualAverage");
                Label TargetAverage = (Label)e.Row.FindControl("txtTargetAverage");

                HiddenField skillComId = (HiddenField)e.Row.FindControl("skillComId");

                for (int i = e.Row.RowIndex; i < ds.Tables[0].Rows.Count; i = ds.Tables[0].Rows.Count)
                {
                    totalActual = 0;
                    totalTarget = 0;
                    ActualAvg = 0;
                    TargetAvg = 0;
                    if (Convert.ToInt32(skillComId.Value) == Convert.ToInt32(ds.Tables[0].Rows[i]["skillComId"]))
                    {
                        if (!String.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[i]["ActualOne"])))
                        {
                            a = Convert.ToInt32(ds.Tables[0].Rows[i]["ActualOne"]);
                            ActualAvg = ActualAvg + (a * 1);
                            totalActual = totalActual + a;
                            ActualOne.Text = Convert.ToString(a);

                        }
                        if (!String.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[i]["ActualTwo"])))
                        {
                            a = Convert.ToInt32(ds.Tables[0].Rows[i]["ActualTwo"]);
                            ActualAvg = ActualAvg + (a * 2);
                            totalActual = totalActual + a;
                            ActualTwo.Text = Convert.ToString(a);

                        }
                        if (!String.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[i]["ActualThree"])))
                        {
                            a = Convert.ToInt32(ds.Tables[0].Rows[i]["ActualThree"]);
                            ActualAvg = ActualAvg + (a * 3);
                            totalActual = totalActual + a;
                            ActualThree.Text = Convert.ToString(a);

                        }
                        if (!String.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[i]["ActualFour"])))
                        {
                            a = Convert.ToInt32(ds.Tables[0].Rows[i]["ActualFour"]);
                            ActualAvg = ActualAvg + (a * 4);
                            totalActual = totalActual + a;
                            ActualFour.Text = Convert.ToString(a);

                        }
                        if (!String.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[i]["ActualFive"])))
                        {
                            a = Convert.ToInt32(ds.Tables[0].Rows[i]["ActualFive"]);
                            ActualAvg = ActualAvg + (a * 5);
                            totalActual = totalActual + a;
                            ActualFive.Text = Convert.ToString(a);

                        }

                        if (!String.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[i]["TargetOne"])))
                        {
                            tar = Convert.ToInt32(ds.Tables[0].Rows[i]["TargetOne"]);
                            act = Convert.ToInt32(ds.Tables[0].Rows[i]["ActualOne"]);

                            if (act >= tar)
                            {
                                ActualOne.BackColor = Color.FromArgb(235, 235, 224); //System.Drawing.ColorTranslator.FromHtml("#d8e3b4cc");
                            }
                            else if (act < tar)
                            {
                                ActualOne.BackColor = Color.FromArgb(255, 230, 230); //System.Drawing.ColorTranslator.FromHtml("#f6dfcbcc");
                            }

                            TargetAvg = TargetAvg + (tar * 1);
                            totalTarget = totalTarget + tar;
                            TargetOne.Text = Convert.ToString(tar);
                        }
                        if (!String.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[i]["TargetTwo"])))
                        {
                            //  a=Convert.ToInt32(ds.Tables[0].Rows[i]["TargetTwo"]);
                            tar = Convert.ToInt32(ds.Tables[0].Rows[i]["TargetTwo"]);
                            act = Convert.ToInt32(ds.Tables[0].Rows[i]["ActualTwo"]);
                            if (act >= tar)
                            {
                                ActualTwo.BackColor = Color.FromArgb(235, 235, 224);
                            }
                            else if (act < tar)
                            {
                                ActualTwo.BackColor = Color.FromArgb(255, 230, 230);
                            }

                            TargetAvg = TargetAvg + (tar * 2);
                            totalTarget = totalTarget + tar;
                            TargetTwo.Text = Convert.ToString(tar);

                        }
                        if (!String.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[i]["TargetThree"])))
                        {
                            //a=Convert.ToInt32(ds.Tables[0].Rows[i]["TargetThree"]);
                            tar = Convert.ToInt32(ds.Tables[0].Rows[i]["TargetThree"]);
                            act = Convert.ToInt32(ds.Tables[0].Rows[i]["ActualThree"]);
                            if (act >= tar)
                            {
                                ActualThree.BackColor = Color.FromArgb(235, 235, 224);
                            }
                            else if (act < tar)
                            {
                                ActualThree.BackColor = Color.FromArgb(255, 230, 230);
                            }

                            TargetAvg = TargetAvg + (tar * 3);
                            totalTarget = totalTarget + tar;

                            TargetThree.Text = Convert.ToString(tar);
                        }
                        if (!String.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[i]["TargetFour"])))
                        {
                            //a=Convert.ToInt32(ds.Tables[0].Rows[i]["TargetFour"]);
                            tar = Convert.ToInt32(ds.Tables[0].Rows[i]["TargetFour"]);
                            act = Convert.ToInt32(ds.Tables[0].Rows[i]["ActualFour"]);
                            if (act >= tar)
                            {
                                ActualFour.BackColor = Color.FromArgb(235, 235, 224);
                            }
                            else if (act < tar)
                            {
                                ActualFour.BackColor = Color.FromArgb(255, 230, 230);
                            }

                            TargetAvg = TargetAvg + (tar * 4);
                            totalTarget = totalTarget + tar;
                            TargetFour.Text = Convert.ToString(tar);

                        }
                        if (!String.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[i]["TargetFive"])))
                        {
                            // a=Convert.ToInt32(ds.Tables[0].Rows[i]["TargetFive"]);
                            tar = Convert.ToInt32(ds.Tables[0].Rows[i]["TargetFive"]);
                            act = Convert.ToInt32(ds.Tables[0].Rows[i]["ActualFive"]);
                            if (act >= tar)
                            {
                                ActualFive.BackColor = Color.FromArgb(235, 235, 224);
                            }
                            else if (act < tar)
                            {
                                ActualFive.BackColor = Color.FromArgb(255, 230, 230);
                            }

                            TargetAvg = TargetAvg + (tar * 5);
                            totalTarget = totalTarget + tar;
                            TargetFive.Text = Convert.ToString(tar);

                        }


                        TotalActual.Text = Convert.ToString(totalActual);
                        TotalTarget.Text = Convert.ToString(totalTarget);

                        ActualAvg = (Convert.ToDouble(ActualAvg) / (totalActual == 0.0 ? 1.0 : totalActual));
                        TargetAvg = (Convert.ToDouble(TargetAvg) / (totalTarget == 0.0 ? 1.0 : totalTarget));
                        ActualAverage.Text = Convert.ToString(Math.Round(ActualAvg, 1));
                        TargetAverage.Text = Convert.ToString(Math.Round(TargetAvg, 1));

                    }
                }
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "Team Target -> gvGrid_RowDataBound");
        }
    }


    protected void gridTotalPoint_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {


            DataSet ds = (DataSet)ViewState["data"];
            int act = 0;
            int tar = 0;
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TextBox ALPActual = (TextBox)e.Row.FindControl("txtALPActual");
                TextBox ALPTarget = (TextBox)e.Row.FindControl("txtALPTarget");

                TextBox PLPActual = (TextBox)e.Row.FindControl("txtPLPActual");
                TextBox PLPTarget = (TextBox)e.Row.FindControl("txtPLPTarget");

                TextBox PointTotalActual = (TextBox)e.Row.FindControl("txtPointTotalActual");
                TextBox PointTotalTarget = (TextBox)e.Row.FindControl("txtPointTotalTarget");

                TextBox PDPActual = (TextBox)e.Row.FindControl("txtPDPActual");
                TextBox PDPTarget = (TextBox)e.Row.FindControl("txtPDPTarget");

                HiddenField skillUserID = (HiddenField)e.Row.FindControl("skillUserID");

                for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                {
                    if (Convert.ToInt32(skillUserID.Value) == Convert.ToInt32(ds.Tables[1].Rows[i]["UserID"]))
                    {
                        if (!String.IsNullOrEmpty(Convert.ToString(ds.Tables[1].Rows[i]["ActualALP"])))
                        {
                            act = Convert.ToInt32(ds.Tables[1].Rows[i]["ActualALP"]);
                            ALPActual.Text = Convert.ToString(act);
                        }
                        if (!String.IsNullOrEmpty(Convert.ToString(ds.Tables[1].Rows[i]["TargetALP"])))
                        {
                            tar = Convert.ToInt32(ds.Tables[1].Rows[i]["TargetALP"]);
                            ALPTarget.Text = Convert.ToString(tar);
                            if (act < tar)
                            {
                                ALPActual.BackColor = Color.FromArgb(235, 235, 224);
                            }
                            else
                            {
                                ALPActual.BackColor = Color.FromArgb(255, 230, 230);
                            }
                        }

                        if (!String.IsNullOrEmpty(Convert.ToString(ds.Tables[1].Rows[i]["ActualPLP"])))
                        {
                            act = Convert.ToInt32(ds.Tables[1].Rows[i]["ActualPLP"]);
                            PLPActual.Text = Convert.ToString(act);

                        }
                        if (!String.IsNullOrEmpty(Convert.ToString(ds.Tables[1].Rows[i]["TargetPLP"])))
                        {
                            tar = Convert.ToInt32(ds.Tables[1].Rows[i]["TargetPLP"]);
                            PLPTarget.Text = Convert.ToString(tar);
                            if (act < tar)
                            {
                                PLPActual.BackColor = Color.FromArgb(235, 235, 224);
                            }
                            else
                            {
                                PLPActual.BackColor = Color.FromArgb(255, 230, 230);
                            }
                        }


                        if (!String.IsNullOrEmpty(Convert.ToString(ds.Tables[1].Rows[i]["ActualTotal"])))
                        {
                            act = Convert.ToInt32(ds.Tables[1].Rows[i]["ActualTotal"]);
                            PointTotalActual.Text = Convert.ToString(act);
                        }
                        if (!String.IsNullOrEmpty(Convert.ToString(ds.Tables[1].Rows[i]["TargetTotal"])))
                        {
                            tar = Convert.ToInt32(ds.Tables[1].Rows[i]["TargetTotal"]);
                            PointTotalTarget.Text = Convert.ToString(tar);
                            if (act < tar)
                            {
                                PointTotalActual.BackColor = Color.FromArgb(235, 235, 224);
                            }
                            else
                            {
                                PointTotalActual.BackColor = Color.FromArgb(255, 230, 230);
                            }
                        }



                        if (!String.IsNullOrEmpty(Convert.ToString(ds.Tables[1].Rows[i]["PDPActual"])))
                        {
                            act = Convert.ToInt32(ds.Tables[1].Rows[i]["PDPActual"]);
                            PDPActual.Text = Convert.ToString(act);
                        }
                        if (!String.IsNullOrEmpty(Convert.ToString(ds.Tables[1].Rows[i]["PDPTarget"])))
                        {
                            tar = Convert.ToInt32(ds.Tables[1].Rows[i]["PDPTarget"]);
                            PDPTarget.Text = Convert.ToString(tar);
                            if (act < tar)
                            {
                                PDPActual.BackColor = Color.FromArgb(235, 235, 224);
                            }
                            else
                            {
                                PDPActual.BackColor = Color.FromArgb(255, 230, 230);
                            }
                        }



                    }
                }
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "gridTotalPoint_RowDataBound");
        }
    }

    [WebMethod(EnableSession = true)]
    public static comment[] GetcommentById(string comId)
    {

        DataTable dt = new DataTable();
        List<comment> cmnt = new List<comment>();

        if (string.IsNullOrWhiteSpace(comId))
        {
            return cmnt.ToArray();
        }
        CompetenceBM obj = new CompetenceBM();


        obj.GetCompetenceCommentById(Convert.ToInt32(comId), Convert.ToInt32(temp1));
        DataSet ds = obj.ds;




        if (ds.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow dtrow in ds.Tables[0].Rows)
            {
                comment comenmt = new comment();
                comenmt.compId = dtrow["compId"].ToString();
                comenmt.name = dtrow["name"].ToString();
                comenmt.Image = dtrow["userImage"].ToString();
                comenmt.comuserid = dtrow["comuserid"].ToString();
                comenmt.compcomment = dtrow["compcomment"].ToString();
                comenmt.comcreatedate = dtrow["comcreatedate"].ToString();

                cmnt.Add(comenmt);
            }
        }
        return cmnt.ToArray();
    }

    [WebMethod(EnableSession = true)]
    public static string insertmessage(string message, string comid)
    {
        string msg = string.Empty;

        try
        {


            CompetenceBM obj = new CompetenceBM();
            Int32 compid = Convert.ToInt32(comid);
            Int32 userid = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
            Int32 touserid = Convert.ToInt32(temp1);

            if (obj.InsertCompetenceComment(compid, userid, message, touserid))
            {

                msg = "true";
            }
            else
            {
                msg = "false";
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "Team Target -> insertmessage");
        }

        return msg;
    }


    public class comment
    {
        public string compId { get; set; }

        public string name { get; set; }
        public string comuserid { get; set; }
        public string Image { get; set; }
        public string compcomment { get; set; }
        public string comcreatedate { get; set; }

    }
    #endregion
    #region Culture
    protected override void InitializeCulture()
    {
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Danish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
}