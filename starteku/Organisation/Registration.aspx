﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="Registration.aspx.cs" Inherits="Organisation_Registration" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .ProfileImage
        {
            width: 150px;
            height: 150px;
            border-radius: 100px;
            border: 1px solid blue;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg"></asp:Label>
    <div class="col-md-6">
        <div class="heading-sec">
            <h1>
                Registration <i><span runat="server" id="Registration"></span></i>
            </h1>
        </div>
    </div>
    <br />
    <br />
    <div class="col-md-6 range ">
        <%--<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
            <i class="fa fa-calendar-o icon-calendar icon-large"></i>
            <span>August 5, 2014 - September 3, 2014</span>  <b class="caret"></b>
        </div>--%>
    </div>
    <div class="col-md-12">
        <div class="chat-widget widget-body" style="background: #fff;">
            <div class="chat-widget-head yellow">
                <h4 style="margin: -6px 0px 0px;">
                    Personal Information</h4>
            </div>
            <div class="indicatesRequireFiled">
                <i>* Indicates required field</i>
            </div>
            <div class="col-md-6">
                <div style="width: 100%; float: left;">
                    <label class="c-label">
                        <asp:Image ID="img_profile" CssClass="ProfileImage" ImageUrl="~/Organisation/images/sign-in1.jpg"
                            runat="server" />
                    </label>
                    <div style="margin-top: 80px; margin-left: 170px;">
                        <asp:FileUpload ID="flupload1" runat="server"></asp:FileUpload>
                        <asp:RegularExpressionValidator ID="reFile1" runat="server" ControlToValidate="flupload1"
                            CssClass="legend" Display="Dynamic" ErrorMessage="Please upload only .gif | .jpeg | .jpg   | .png image."
                            ValidationExpression="^.*\.(JPG|jpg|jpeg|PNG|png|JPEG)$" ForeColor="Red" ValidationGroup="chk1"></asp:RegularExpressionValidator>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label">
                        First Name:*</label>
                    <asp:TextBox runat="server" Text="" ID="txtfName" MaxLength="80" onkeyup="run(this)"
                        CssClass="input-style" />
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom"
                        TargetControlID="txtfName" Enabled="True" ValidChars=" ">
                    </cc1:FilteredTextBoxExtender>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtfName"
                        ErrorMessage="Please enter First Name." CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label">
                        Last Name:*</label>
                    <asp:TextBox runat="server" Text="" ID="txtlname" MaxLength="80" onkeyup="run(this)"
                        CssClass="input-style" />
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom"
                        TargetControlID="txtlname" Enabled="True" ValidChars=" ">
                    </cc1:FilteredTextBoxExtender>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtlname"
                        ErrorMessage="Please enter Last name." CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label">
                        Email:*</label>
                    <asp:TextBox runat="server" Text="" ID="txtEmail" MaxLength="80" onkeyup="run(this)"
                        CssClass="input-style" />
                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" CssClass="commonerrormsg"
                        ControlToValidate="txtEmail" Display="Dynamic" ValidationGroup="chk" ErrorMessage="Please enter email."></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        ControlToValidate="txtEmail" CssClass="commonerrormsg" Display="Dynamic" ValidationGroup="chk"
                        ErrorMessage="Please enter valid email."></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label" style="width: 100%">
                        Gender:*</label>
                    <asp:DropDownList ID="ddlgender" runat="server" Style="width: 250px; height: 36px;
                        display: inline;">
                        <asp:ListItem Value="Male" Text="Male"></asp:ListItem>
                        <asp:ListItem Value="Female" Text="Female"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label">
                        Password:*</label>
                    <asp:TextBox ID="txtPassword" class="input-style" runat="server" MaxLength="20" TextMode="Password" />
                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" CssClass="commonerrormsg"
                        ControlToValidate="txtPassword" Display="Dynamic" ValidationGroup="chk" ErrorMessage="Password is required."></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revPassword" runat="server" ControlToValidate="txtpassword"
                        ValidationExpression="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,8}$" CssClass="commonerrormsg"
                        Display="Dynamic" ValidationGroup="chk" ErrorMessage="Password must be at least 4 characters, no more than 8 characters, and must include at least one upper case letter, one lower case letter, and one numeric digit" />
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label">
                        Confirm Password:*</label>
                    <asp:TextBox ID="txtConfirmPassword" type="text" class="input-style" runat="server"
                        MaxLength="20" TextMode="Password" />
                    <asp:CompareValidator ID="cvPasswordNotMatch" ControlToValidate="txtConfirmPassword"
                        ControlToCompare="txtPassword" runat="server" CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk" ErrorMessage="Password and confirm password does not match."></asp:CompareValidator>
                    <asp:RequiredFieldValidator ID="rfvConfirmPassword" runat="server" CssClass="commonerrormsg"
                        ControlToValidate="txtConfirmPassword" Display="Dynamic" ErrorMessage="Confirm password is required."
                        ValidationGroup="chk"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label">
                        Phone:*</label>
                    <asp:TextBox runat="server" ID="txtMobile" CssClass="input-style" MaxLength="15"></asp:TextBox>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtMobile"
                        ErrorMessage="Minimum mobile length is 10." Display="Dynamic" CssClass="commonerrormsg"
                        ValidationGroup="chk" ValidationExpression=".{10}.*" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtMobile"
                        ErrorMessage="Please enter mobile." CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk"></asp:RequiredFieldValidator>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers,custom" ValidChars=" ,+,(,),-"
                        TargetControlID="txtMobile" Enabled="True">
                    </cc1:FilteredTextBoxExtender>
                </div>
            </div>
            <asp:UpdatePanel ID="qw" runat="server">
                <ContentTemplate>
                    <div class="col-md-6">
                        <div class="inline-form">
                            <label class="c-label" style="width: 100%">
                                Country:*</label>
                            <asp:DropDownList ID="ddlcountry" runat="server" Style="width: 250px; height: 36px;
                                display: inline;" AutoPostBack="true" OnSelectedIndexChanged="ddlcountry_SelectedIndexChanged">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="ddlcountry"
                                ErrorMessage="Please select Country." InitialValue="0" CssClass="commonerrormsg"
                                Display="Dynamic" ValidationGroup="chk"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group" id="state" runat="server" style="display: none">
                        <div class="col-md-6">
                            <div class="inline-form">
                                <label class="c-label" style="width: 100%">
                                    State:*</label>
                                <asp:DropDownList ID="ddlstate" runat="server" Style="width: 250px; height: 36px;
                                    display: inline;" AutoPostBack="true" OnSelectedIndexChanged="ddlstate_SelectedIndexChanged">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlstate"
                                    ErrorMessage="Please select State." InitialValue="0" CssClass="commonerrormsg"
                                    Display="Dynamic" ValidationGroup="chk"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>
                    <%--<div class="form-group" id="city" runat="server" style="display: none">
                        <div class="col-md-6">
                            <div class="inline-form">
                                <label class="c-label" style="width: 100%">
                                    City:*</label>
                                <asp:DropDownList ID="ddlcity" runat="server" Style="width: 250px; height: 36px;
                                    display: inline;">
                                </asp:DropDownList>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="ddlcity"
                                    ErrorMessage="Please select City." InitialValue="0" CssClass="commonerrormsg"
                                    Display="Dynamic" ValidationGroup="chk"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>--%>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlcountry" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>
             <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label">
                        city:*</label>
                    <asp:TextBox runat="server" ID="txtcity" CssClass="input-style" MaxLength="50"
                        ></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" CssClass="commonerrormsg"
                        ControlToValidate="txtcity" Display="Dynamic" ValidationGroup="chk" ErrorMessage="city is required."></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label">
                        DOB:*</label>
                    <asp:TextBox runat="server" Text="" ID="txtfromdate" MaxLength="10" CssClass="input-style" />
                    <cc1:CalendarExtender ID="CalendarExtender2" PopupPosition="TopLeft" runat="server"
                        TargetControlID="txtfromdate" Format="dd/MM/yyyy">
                    </cc1:CalendarExtender>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtfromdate"
                        ErrorMessage="Please enter special price start date." CssClass="commonerrormsg"
                        Display="Dynamic" Visible="false" ValidationGroup="chk"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label">
                        Address:*</label>
                    <asp:TextBox runat="server" ID="txtaddress" CssClass="input-style" MaxLength="500"
                        TextMode="MultiLine"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" CssClass="commonerrormsg"
                        ControlToValidate="txtaddress" Display="Dynamic" ValidationGroup="chk" ErrorMessage="Address is required."></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label">
                        Zip:</label>
                    <asp:TextBox runat="server" ID="txtzip" CssClass="input-style" MaxLength="15"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label" style="width: 100%">
                        Job Type:*</label>
                    <asp:DropDownList ID="ddljobtype" runat="server" Style="width: 250px; height: 36px;
                        display: inline;">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="ddljobtype"
                        ErrorMessage="Please select Job Type." InitialValue="0" CssClass="commonerrormsg"
                        Display="Dynamic" ValidationGroup="chk"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="col-xs-12 profile_bottom">
                <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="btn black pull-right"
                    ValidationGroup="chk" CausesValidation="false" OnClick="btnCancel_click" />
                <asp:Button runat="server" ID="btnsubmit" Text="Submit" CssClass="btn blue pull-right"
                    ValidationGroup="chk" OnClick="btnsubmit_click" />
            </div>
        </div>
    </div>
    <asp:Label runat="server" ID="lblDataDisplayTitle"></asp:Label>
</asp:Content>
