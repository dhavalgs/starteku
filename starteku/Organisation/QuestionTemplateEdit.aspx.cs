﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using System.Threading;
using System.Globalization;
using startetku.Business.Logic;

public partial class Organisation_QuestionTemplateEdit : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "my", " SetExpandCollapse();", true);

        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {

            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                GetAllQuestionTemplateByID();
            }
        }
    }
    protected void GetAllQuestionTemplateByID()
    {
        //ddlQuesCat
        QuestionBM obj = new QuestionBM();
       // obj.quesTmpIDs = "0";
        obj.queIsActive = Convert.ToBoolean(Session["IsActive"]);
        obj.quesIsPublic = Convert.ToBoolean(Session["IsPublic"]);
        obj.quesIsMandatory = Convert.ToBoolean(Session["IsMandatory"]);
        obj.queIsDeleted = false;
        obj.quesTmpIDs = Convert.ToString(Request.QueryString["id"]);
        obj.queCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.CreatedByID = Convert.ToInt32(Session["OrgUserId"]);
        

        obj.GetAllQuestionTemplateList();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["qtmpID"])))
                txtqtmpID.Value = Convert.ToString(ds.Tables[0].Rows[0]["qtmpID"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["tmpName"])))
                txttmpName.Text = Convert.ToString(ds.Tables[0].Rows[0]["tmpName"]);


            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["tmpNameDN"])))
                txttmpNameDN.Text = Convert.ToString(ds.Tables[0].Rows[0]["tmpNameDN"]);


            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["tmpDescription"])))
                txtDESCRIPTION.Text = Convert.ToString(ds.Tables[0].Rows[0]["tmpDescription"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["IsActive"])) && Convert.ToBoolean(ds.Tables[0].Rows[0]["IsActive"]) == false)
            {
                rdoIsActive.SelectedValue = "false";


            }
            else
            {
                rdoIsActive.SelectedValue = "true";
            }

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["IsPublic"])) && Convert.ToBoolean(ds.Tables[0].Rows[0]["IsPublic"]) == false)
            {
                roIsPublic.SelectedValue = "false";


            }
            else
            {
                roIsPublic.SelectedValue = "true";
            }

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["IsMandatory"])) && Convert.ToBoolean(ds.Tables[0].Rows[0]["IsMandatory"]) == false)
            {
                roIsMandatory.SelectedValue = "false";


            }
            else
            {
                roIsMandatory.SelectedValue = "true";
            }
            //if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["IsActive"])))
            //    rdoIsActive.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["IsActive"]);

            //if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["IsPublic"])))
            //    roIsPublic.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["IsPublic"]);

            //if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["IsMandatory"])))
            //    roIsMandatory.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["IsMandatory"]);

        }


    }
    #region Culture
    protected override void InitializeCulture()
    {
        // string language = "French";
        string language = Convert.ToString(Session["Culture"]);

        string languageId = "";

        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!String.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }

        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion

    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        string msg = "";
        if (!String.IsNullOrEmpty(Request.QueryString["id"]))
        {
            msg = updateQuestionTemplate();
        }
        if (!String.IsNullOrEmpty(msg))
        {
            if (msg == "success")
            {
                lblMsg.Text = CommonModule.msgRecordInsertedSuccessfully;
                lblMsg.ForeColor = System.Drawing.Color.White;

                Response.Redirect("QuestionTemplate.aspx");
            }
            else
            {
                lblMsg.Text = CommonModule.msgAlreadyExists;
            }
        }
        else
        {
            lblMsg.Text = CommonModule.msgSomeProblemOccure;
        }

    }
    protected void btnCancel_click(object sender, EventArgs e)
    {
        Response.Redirect("QuestionTemplate.aspx");
    }

    protected string updateQuestionTemplate()
    {
        string returnMsg = "";
        try
        {
            // check Duplicate
            var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
            var userId = Convert.ToInt32(Session["OrgUserId"]);
            var logginUserType = Convert.ToInt32(Session["OrguserType"]);
            QuestionBM obj = new QuestionBM();
            obj.quesTmpID = Convert.ToInt32(txtqtmpID.Value);
            obj.quesTemplateName = txttmpName.Text;
            obj.quesTemplateNameDN = txttmpNameDN.Text;
            obj.quesDescription = txtDESCRIPTION.Text;
            obj.CreatedByID = userId;
            obj.queCompanyId = companyId;

            obj.queIsActive = Convert.ToBoolean(rdoIsActive.SelectedItem.Value);
            obj.quesIsPublic = Convert.ToBoolean(roIsPublic.SelectedItem.Value);
            obj.quesIsMandatory = Convert.ToBoolean(roIsMandatory.SelectedItem.Value);

            obj.queIsDeleted = false;
            obj.queCreatedDate = DateTime.Now;
            obj.InsertQuestionTemplateData();
            DataSet ds = obj.ds;

            returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex);
        }
        return returnMsg;
    }
    #endregion
}