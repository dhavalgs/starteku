﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="CompetenceTargetSet.aspx.cs" Inherits="Organisation_CompetenceTargetSet" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style type="text/css">
        .table > thead > tr > th {
            vertical-align: middle;
        }
    </style>
    <style type="text/css">
        body {
            font-family: Arial;
            font-size: 11pt;
        }

        .modalBackground {
            background-color: Black;
            filter: alpha(opacity=40);
            opacity: 0.4;
        }

        .modalPopup1 {
            background-color: #FFFFFF;
            width: 300px;
            border: 3px solid #0DA9D0;
        }

        .modalPopup {
            background: none repeat scroll 0 0 #ffffff;
            display: table;
            float: none;
            margin: 96px auto;
            width: 600px !important;
            top: 10px !important;
        }

            .modalPopup .header {
                background-color: #2FBDF1;
                height: 30px;
                color: White;
                line-height: 30px;
                text-align: center;
                font-weight: bold;
            }

            .modalPopup .body {
                min-height: 50px;
                line-height: 30px;
                text-align: center;
                padding: 5px;
            }

            .modalPopup .footer {
                padding: 3px;
            }

            .modalPopup .button {
                height: 23px;
                color: White;
                line-height: 23px;
                text-align: center;
                font-weight: bold;
                cursor: pointer;
                background-color: #9F9F9F;
                border: 1px solid #5C5C5C;
            }

            .modalPopup td {
                text-align: left;
            }

        .button {
            background-color: transparent;
        }

        .yellow {
            border-radius: 0px;
        }

        .reply {
            clear: both;
        }

        #scrollbox6 {
            overflow-x: hidden;
            overflow-y: auto;
            background: #f4f4f4;
            height: 350px !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:HiddenField ID="hdnDivision" runat="server" Value="Division" meta:resourcekey="DivisionRes" />
    <asp:HiddenField ID="hdnJobType" runat="server" Value="JobType" meta:resourcekey="JobTypeRes" />

    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg"
        meta:resourcekey="lblMsgResource1"></asp:Label>
    <div class="col-md-6">
        <div class="heading-sec">
            <h1 style="margin-left: 15px;">
                <%-- <%= CommonMessages.Viewrequest%>--%>
                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Viewrequest" EnableViewState="false" />
                <i><span runat="server" id="Employee"></span>
                </i>
            </h1>
        </div>
    </div>
    <div class="chat-widget widget-body" style="background: #fff;">
    </div>
    <div class="col-md-12" style="margin-top: 20px; width: 100%;">
        <%--<div id="graph-wrapper">--%>
        <div class="chat-widget-head yellow" style="width: 97.8%; margin-left: 15px; margin-bottom: -16px;">
            <h4 style="margin: -6px 0px 0px;">
                <span runat="server" id="spname"></span>'s  <%--<%= CommonMessages.Competencelevel%>--%>
                <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Competencelevel" EnableViewState="false" />
            </h4>
        </div>
        <div class="col-md-12">
            <div class="chart-tab">
                <div id="tabs-container">

                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:GridView ID="gvGrid" runat="server" AutoGenerateColumns="False" CellPadding="0"
                                Width="100%" GridLines="None" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                                EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'
                                BackColor="White" OnRowDataBound="gvGrid_RowDataBound"
                                meta:resourcekey="gvGridResource1">
                                <HeaderStyle CssClass="aa" />
                                <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                    <asp:TemplateField HeaderText="ID" meta:resourcekey="TemplateFieldResource1">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="10px" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="2%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="COMPETENCE"
                                        meta:resourcekey="TemplateFieldResource2">
                                        <ItemTemplate>
                                            <%if(Convert.ToString(Session["Culture"]) == "Danish"){%>
                                            <asp:Label ID="lblUNamer" runat="server" Text='<%# Eval("comCompetenceDN") %>'
                                                meta:resourcekey="lblUNamerResource1"></asp:Label>
                                            <%} %>
                                            <%else{ %>
                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("comCompetence") %>'
                                                meta:resourcekey="lblUNamerResource1"></asp:Label>
                                            <%} %>
                                            <asp:HiddenField ID="tarCompID" runat="server" Value="<%# bind('tarCompID') %>" />
                                            <asp:HiddenField ID="targetID" runat="server" Value="<%# bind('targetID') %>" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="15%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Long Term Target"
                                        meta:resourcekey="LongTermRes">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtLongTerm" runat="server" AutoPostBack="True" Text='<%#Eval("tarLongTermLevel" )%>'
                                                MaxLength="1" Width="50px"></asp:TextBox><br />
                                            <asp:RequiredFieldValidator ID="txtTarget122" runat="server" CssClass="commonerrormsg"
                                                ControlToValidate="txtLongTerm" Display="Dynamic" ValidationGroup="chk"
                                                ErrorMessage="Please enter Target." meta:resourcekey="txtTarget122Resource1"></asp:RequiredFieldValidator>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender33" runat="server" FilterType="Numbers"
                                                TargetControlID="txtLongTerm" Enabled="True">
                                            </cc1:FilteredTextBoxExtender>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Max Level"
                                        meta:resourcekey="MaxLevelRes">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtMaxLevel" runat="server" AutoPostBack="True" Text='<%#Eval("tarMaxLevel" )%>'
                                                MaxLength="1" Width="50px"></asp:TextBox><br />
                                            <asp:RequiredFieldValidator ID="txtTarget1222" runat="server" CssClass="commonerrormsg"
                                                ControlToValidate="txtMaxLevel" Display="Dynamic" ValidationGroup="chk"
                                                ErrorMessage="Please enter Target." meta:resourcekey="txtTarget1222Resource1"></asp:RequiredFieldValidator>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender333" runat="server" FilterType="Numbers"
                                                TargetControlID="txtMaxLevel" Enabled="True">
                                            </cc1:FilteredTextBoxExtender>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>

                    </asp:UpdatePanel>
                    <input type="hidden" id="hdncom" value='' />
                    <asp:LinkButton ID="lnkFake" runat="server" meta:resourcekey="lnkFakeResource1" />
                    <cc1:ModalPopupExtender ID="mpe" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkFake"
                        CancelControlID="btnClose" BackgroundCssClass="modalBackground" DynamicServicePath=""
                        Enabled="True">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup modal-dialog" meta:resourcekey="pnlPopupResource1" Style="top: 0px !important;">
                        <div class="modal-header blue yellow-radius" style="color: #ffffff;">
                            <asp:Label runat="server" ID="Label2" CssClass="modal-title" Style="font-size: 135%;" meta:resourcekey="Comments"></asp:Label>
                            <%--<h4 class="modal-title">Comments</h4>--%>
                            <div style="float: right; margin-top: -10px;">
                                <asp:Button ID="btnClose" runat="server" Text="×" CssClass="close" />
                            </div>
                        </div>
                        <div class="modal-body">
                            <ul id="scrollbox6">
                                <li class="reply">
                                    <asp:Literal ID="Literal3" runat="server" meta:resourcekey="NoRecordsFound" EnableViewState="false" />
                                </li>
                            </ul>

                            <div class="reply-sec">
                                <div class="reply-sec-MESSAGE" style="margin-left: 20px;">

                                    <input type="text" meta:resourcekey="PlsEnterComments" id="txtcomment" class="txtcomment" style="padding: 10px;" />



                                    <a href="#" title="" class="black" onclick="insertcomment()"><i class="fa fa-comments-o"></i></a>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                </div>
                <div class="col-xs-12" id="submit" runat="server" style="margin: 10px; padding: 10px; width: 98%;">
                    <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="btn black pull-right"
                        ValidationGroup="chk" CausesValidation="False" OnClick="btnCancel_click"
                        Style="color: white;" meta:resourcekey="btnCancelResource1" />
                    <asp:Button runat="server" ID="btnsubmit" Text="Save" CssClass="btn blue pull-right"
                        ValidationGroup="chk" OnClick="btnsubmit_click" Style="color: white;"
                        meta:resourcekey="btnsubmitResource1" />
                    <asp:Button runat="server" ID="btnback" Text="Back" CssClass="btn black pull-right"
                        CausesValidation="False" OnClick="btnback_click" Visible="False"
                        Style="color: white;" meta:resourcekey="btnbackResource1" />
                </div>
            </div>
        </div>
        <%--</div>--%>
    </div>
    <div style="display: none">
        <asp:Label runat="server" ID="lblNoRecord" CssClass="lblNoRecords" meta:resourcekey="NoRecordsFound"></asp:Label>
        <asp:Label runat="server" ID="lblTypeYourComments" CssClass="lblTypeYourComments" meta:resourcekey="PlsEnterComments"></asp:Label>
    </div>
    <style type="text/css">
        .yellow {
            border-radius: 0px;
        }
    </style>
    <script src="../assets/assets/js/libs/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            setTimeout(function () {
                $(".active").click();
                onLoad();

                // tabMenuClick();
                // $(".myCompetence").addClass("active_page");
            }, 500);
            $(".pending_helprequest").addClass("active_page");
        });

        function checkNumber(ele) {
            var key = window.event ? event.keyCode : event.which;
            var keychar = String.fromCharCode(key);
            var el = ele.value;
            var input = el + "" + keychar;
            alert(input);
            //your code to valide it.........

        }
        function onLoad() {

            SetExpandCollapse();
            setTimeout(function () {
                //ExampanCollapsMenu();

                // tabMenuClick();
                $(".pending_helprequest").addClass("active_page");
            }, 500);
        }
    </script>
</asp:Content>
