﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master" AutoEventWireup="true" CodeFile="OrgQuestion.aspx.cs" Inherits="Organisation_OrgQuestion" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .treeNode
        {
        }
        
        .treeNode input
        {
            width: auto;
            margin: 5px;
            float: left !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg"></asp:Label>
    <div class="col-md-6">
        <div class="heading-sec">
            <h1>
                Competence <i><span runat="server" id="Competence"></span></i>
            </h1>
        </div>
    </div>
    <br />
    <br />
    <div class="col-md-6 range ">
        <%--<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
            <i class="fa fa-calendar-o icon-calendar icon-large"></i>
            <span>August 5, 2014 - September 3, 2014</span>  <b class="caret"></b>
        </div>--%>
    </div>
    <div class="col-md-12">
        <div class="chat-widget widget-body" style="background: #fff;">
            <div class="chat-widget-head yellow">
                <h4 style="margin: -6px 0px 0px;">
                    Competence Information</h4>
            </div>
            <div class="indicatesRequireFiled">
                <i>* Indicates required field</i>
                <asp:Label runat="server" ID="lblDataDisplayTitle" Text="Competence"></asp:Label>
            </div>
            <div class="col-md-6" style="width: 80%;">
                <div class="inline-form">
                    <label class="c-label" style="width: 100%;">
                        Job Type:</label>
                    <asp:DropDownList ID="ddljobtype" runat="server" Style="width: 100px; height: 32px;
                        display: inline;">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="ddljobtype"
                        ErrorMessage="Please select country." InitialValue="0" CssClass="commonerrormsg"
                        Display="Dynamic" ValidationGroup="chk"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="col-md-6" style="width: 80%;">
                <div class="inline-form">
                    <label class="c-label">
                        Department:</label>
                    <br />
                    <br />
                    <asp:TreeView ID="TreeView1" runat="server" ShowCheckBoxes="All" OnClick="OnTreeClick(event)"
                        OnTreeNodePopulate="TreeView_TreeNodePopulateDepartment" Style="cursor: pointer"
                        ShowLines="True" NodeStyle-CssClass="treeNode" />
                </div>
            </div>
            <div class="col-md-6" style="width: 80%;">
                <div class="inline-form">
                    <label class="c-label">
                        Division:</label>
                    <br />
                    <br />
                    <asp:TreeView ID="TreeView2" runat="server" ShowCheckBoxes="All" OnClick="OnTreeClick(event)"
                        OnTreeNodePopulate="TreeView_TreeNodePopulateDivision" Style="cursor: pointer"
                        ShowLines="True" NodeStyle-CssClass="treeNode" />
                </div>
            </div>
            <div class="col-md-6" style="width: 80%;">
                <div class="inline-form">
                    <label class="c-label" style="width: 100%;">
                        Level:</label>
                    <asp:TextBox runat="server" Text="" ID="txtlevel" MaxLength="50" CssClass="form-control" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator16" runat="server" ControlToValidate="txtlevel"
                        ErrorMessage="Please enter Level." CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk"></asp:RequiredFieldValidator>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers"
                        TargetControlID="txtlevel" Enabled="True">
                    </cc1:FilteredTextBoxExtender>
                </div>
            </div>
            <div class="col-md-6" style="width: 80%;">
                <div class="inline-form">
                    <label class="c-label" style="width: 100%;">
                        Competence:</label>
                    <asp:TextBox runat="server" Text="" ID="txtCompetence" MaxLength="50" CssClass="form-control" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtCompetence"
                        ErrorMessage="Please enter competence." CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="col-md-6" style="width: 80%;">
                <div class="inline-form">
                    <label class="c-label" style="width: 100%;">
                        Questions:</label>
                    <asp:TextBox runat="server" Text="" ID="txt_Question" MaxLength="50" CssClass="form-control"
                        Height="80" TextMode="MultiLine" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txt_Question"
                        ErrorMessage="Please enter questions." CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk"></asp:RequiredFieldValidator>
                </div>
            </div>
            <asp:UpdatePanel ID="UpdatePanel11" runat="server">
                <ContentTemplate>
                    <div class="col-md-6" style="width: 80%;">
                        <div class="inline-form">
                            <label class="c-label" style="width: 100%;">
                                Numbers of Answers:</label>
                            <asp:TextBox runat="server" Text="" ID="txt_Number" MaxLength="50" CssClass="form-control"
                                Width="100%" OnTextChanged="txt_number_TextChanged" AutoPostBack="true" Visible="false"/>
                            <asp:DropDownList ID="ddlNumber" runat="server" Style="width: 250px; height: 36px;
                                display: inline;" onselectedindexchanged="ddlNumber_SelectedIndexChanged" AutoPostBack="True">
                                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                <asp:ListItem Value="6" Text="6"></asp:ListItem>
                                <asp:ListItem Value="7" Text="7"></asp:ListItem>
                                <asp:ListItem Value="8" Text="8"></asp:ListItem>
                                <asp:ListItem Value="9" Text="9"></asp:ListItem>
                                <asp:ListItem Value="10" Text="10"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_Number"
                                ErrorMessage="Please enter numbers of answers." CssClass="commonerrormsg" Display="Dynamic"
                                ValidationGroup="chk" Height="50px"></asp:RequiredFieldValidator>
                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                                TargetControlID="txt_Number" Enabled="True">
                            </cc1:FilteredTextBoxExtender>
                        </div>
                    </div>
                    <div id="div_contener">
                        <asp:Repeater ID="Repeater1" runat="server" OnItemCommand="Repeater1_ItemCommand">
                            <ItemTemplate>
                                <div class="col-md-6" style="width: 80%;">
                                    <div class="inline-form">
                                        <label class="c-label">
                                            Answers:<%# Container.ItemIndex +1 %></label>
                                        <table style="width: 100%;">
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="TextBox1" Text='<%# Eval("answer") %>' runat="server" CssClass="form-control"></asp:TextBox>
                                                </td>
                                                <td style="width: 5%;">
                                                    <asp:ImageButton ID="Button1" CommandName="<%# Container.ItemIndex +1 %>" runat="server"
                                                        BorderStyle="None" Height="35" Width="40" ImageAlign="Middle" ImageUrl="../Organisation/img/false_check.png">
                                                    </asp:ImageButton>
                                                </td>
                                            </tr>
                                        </table>
                                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBox1"
                                                ErrorMessage="Please enter answers." CssClass="commonerrormsg" Display="Dynamic"
                                                ValidationGroup="chk" Height="50px"></asp:RequiredFieldValidator>--%>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
            <div class="col-xs-12 profile_bottom">
                <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="btn black pull-right"
                    ValidationGroup="chk" CausesValidation="false" OnClick="btnCancel_click" />
                <asp:Button runat="server" ID="btnsubmit" Text="Save" CssClass="btn blue pull-right"
                    ValidationGroup="chk" OnClick="btnsubmit_click" />
            </div>
        </div>
    </div>
    <script type="text/javascript">

        function OnTreeClick(evt) {
            var src = window.event != window.undefined ? window.event.srcElement : evt.target;
            var isChkBoxClick = (src.tagName.toLowerCase() == "input" && src.type == "checkbox");
            if (isChkBoxClick) {
                if (src.checked == true) {
                    var nodeText = getNextSibling(src).innerText || getNextSibling(src).innerHTML;
                    var nodeValue = GetNodeValue(getNextSibling(src));
                    document.getElementById("label").innerHTML += nodeText + ",";
                }

                else {

                    var nodeText = getNextSibling(src).innerText || getNextSibling(src).innerHTML;
                    var nodeValue = GetNodeValue(getNextSibling(src));
                    var val = document.getElementById("label").innerHTML;
                    document.getElementById("label").innerHTML = val.replace(nodeText + ",", "");
                }

                var parentTable = GetParentByTagName("table", src);
                var nxtSibling = parentTable.nextSibling;
                //check if nxt sibling is not null & is an element node
                if (nxtSibling && nxtSibling.nodeType == 1) {
                    //if node has children   
                    if (nxtSibling.tagName.toLowerCase() == "div") {
                        //check or uncheck children at all levels
                        CheckUncheckChildren(parentTable.nextSibling, src.checked);
                    }
                }
                //check or uncheck parents at all levels
                CheckUncheckParents(src, src.checked);
            }
        }

        function CheckUncheckChildren(childContainer, check) {
            var childChkBoxes = childContainer.getElementsByTagName("input");
            var childChkBoxCount = childChkBoxes.length;
            for (var i = 0; i < childChkBoxCount; i++) {
                childChkBoxes[i].checked = check;
            }
        }



        function CheckUncheckParents(srcChild, check) {
            var parentDiv = GetParentByTagName("div", srcChild);
            var parentNodeTable = parentDiv.previousSibling;
            if (parentNodeTable) {
                var checkUncheckSwitch;
                //checkbox checked
                if (check) {
                    var isAllSiblingsChecked = AreAllSiblingsChecked(srcChild);
                    if (isAllSiblingsChecked)
                        checkUncheckSwitch = true;
                    else
                        return; //do not need to check parent if any(one or more) child not checked
                }

                else //checkbox unchecked
                {
                    checkUncheckSwitch = false;
                }

                var inpElemsInParentTable = parentNodeTable.getElementsByTagName("input");
                if (inpElemsInParentTable.length > 0) {
                    var parentNodeChkBox = inpElemsInParentTable[0];
                    parentNodeChkBox.checked = checkUncheckSwitch;
                    //do the same recursively
                    CheckUncheckParents(parentNodeChkBox, checkUncheckSwitch);
                }
            }
        }

        function AreAllSiblingsChecked(chkBox) {
            var parentDiv = GetParentByTagName("div", chkBox);
            var childCount = parentDiv.childNodes.length;
            for (var i = 0; i < childCount; i++) {
                if (parentDiv.childNodes[i].nodeType == 1) {
                    //check if the child node is an element node
                    if (parentDiv.childNodes[i].tagName.toLowerCase() == "table") {
                        var prevChkBox = parentDiv.childNodes[i].getElementsByTagName("input")[0];
                        //if any of sibling nodes are not checked, return false
                        if (!prevChkBox.checked) {
                            return false;
                        }
                    }
                }
            }
            return true;
        }

        //utility function to get the container of an element by tagname

        function GetParentByTagName(parentTagName, childElementObj) {
            var parent = childElementObj.parentNode;
            while (parent.tagName.toLowerCase() != parentTagName.toLowerCase()) {
                parent = parent.parentNode;
            }
            return parent;
        }



        function getNextSibling(element) {
            var n = element;
            do n = n.nextSibling;
            while (n && n.nodeType != 1);
            return n;
        }

        //returns NodeValue

        function GetNodeValue(node) {
            var nodeValue = "";
            var nodePath = node.href.substring(node.href.indexOf(",") + 2, node.href.length - 2);
            var nodeValues = nodePath.split("\\");
            if (nodeValues.length > 1)
                nodeValue = nodeValues[nodeValues.length - 1];
            else
                nodeValue = nodeValues[0].substr(1);
            return nodeValue;
        }
    </script>
</asp:Content>

