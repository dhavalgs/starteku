﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="ContactList.aspx.cs" Inherits="Organisation_ContactList" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<%@ Register Assembly="DelayedSubmit" Namespace="DelayedSubmit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .contact
        {
            width: 100%;
            padding: 2px;
        }
        
        .contact label
        {
            padding: 5px;
            margin-bottom: 0px;
        }
        
        .contact :hover
        {
            background-color: white;
            width: 100%;
        }
        
        .pnl_result
        {
            max-height: 100px;
            overflow: auto;
        }
    </style>
    <script src="js/jquery.caret.js" type="text/javascript"></script>
    <script type="text/javascript">
        function AnotherFunction() {
            var $inputExample = $('#ContentPlaceHolder1_txt_name');
            $inputExample.caretToEnd();
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="col-md-6">
        <div class="heading-sec">
            <h1>
                Conatct colleagus <i><span runat="server" id="Conatct"></span></i>
            </h1>
        </div>
    </div>
    <div class="col-md-12" style="margin-top: 15px;">
        <a href="#add-post-title" data-toggle="modal" title="" onclick="javascript:showpopup1();">
            <button type="button" class="btns  yellow  lrg-btn flat-btn add_user" style="border: 0px;">
               <%-- <%= CommonMessages.AddNew%>--%>
                 <asp:Literal ID="Literal1" runat="server" meta:resourcekey="AddNewContact" enableviewstate="false"/>
                </button></a>
       
        <div aria-hidden="true" role="dialog" tabindex="-1" class="modal fade" id="add-post-title"
            style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header blue yellow-radius">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                            ×
                        </button>
                        <h4 class="modal-title">
                          <%--  <%= CommonMessages.AddNew%>--%>
                            <asp:Literal ID="Literal2" runat="server" meta:resourcekey="AddNew" enableviewstate="false"/>
                         </h4>
                    </div>
                    <div class="modal-body">
                        <asp:UpdatePanel runat="server" ID="UpdatePanel11" UpdateMode="Conditional">
                            <ContentTemplate>
                                <%--<cc1:DelayedSubmitExtender ID="DisableButtonExtender1" runat="server" Timeout="100"
                                    TargetControlID="txt_name" Enabled="True" />--%>
                                <asp:TextBox runat="server" ID="txt_name" AccessKey="u" placeholder="Name :" onkeyup="GetCourt(this.id);"
                                    OnTextChanged="txt_name_TextChanged" AutoPostBack="true"></asp:TextBox>
                                
                                <%--<div style="background-color: #CCC; position: absolute; z-index: 100; margin-top: 44px;
                                    width: 93.5%;">
                                    <asp:Panel ID="pnlresult" CssClass="pnl_result" runat="server" 
                                        meta:resourcekey="pnlresultResource1">
                                        <asp:Repeater ID="lst_contacts" runat="server">
                                            <ItemTemplate>
                                                <div class="contact">
                                                    <label style="width: 100%;">
                                                        <asp:LinkButton ID="lb_contact" runat="server" Text='<%# Eval("value") %>' CommandName='<%# Eval("key") %>'
                                                            OnClick="lb_contact_Click" meta:resourcekey="lb_contactResource1"></asp:LinkButton>
                                                    </label>
                                                </div>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </asp:Panel>
                                </div>--%>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txt_name"
                                    ErrorMessage="Please enter Name." CssClass="commonerrormsg" Display="Dynamic"
                                    ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator5Resource1"></asp:RequiredFieldValidator>
                                <div>
                                    <asp:TextBox runat="server" ID="txt_address" placeholder="Address :" TextMode="MultiLine"
                                        Rows="3" meta:resourcekey="txt_addressResource1"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txt_address"
                                        ErrorMessage="Please enter Address." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator2Resource1"></asp:RequiredFieldValidator>
                                </div>
                                <div>
                                    <asp:TextBox runat="server" ID="txt_email" placeholder="Email :" meta:resourcekey="txt_emailResource1"></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" CssClass="commonerrormsg"
                                        ControlToValidate="txt_email" Display="Dynamic" ValidationGroup="chk" ErrorMessage="Please enter email."
                                        meta:resourcekey="rfvEmailResource1"></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                        ControlToValidate="txt_email" CssClass="commonerrormsg" Display="Dynamic" ValidationGroup="chk"
                                        ErrorMessage="Please enter valid email." meta:resourcekey="revEmailResource1"></asp:RegularExpressionValidator>
                                </div>
                                <div>
                                    <asp:TextBox runat="server" ID="txt_phone" placeholder="Phone No :" meta:resourcekey="txt_phoneResource1"></asp:TextBox>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txt_phone"
                                        ErrorMessage="Minimum mobile length is 10." Display="Dynamic" CssClass="commonerrormsg"
                                        ValidationGroup="chk" ValidationExpression=".{10}.*" meta:resourcekey="RegularExpressionValidator6Resource1" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txt_phone"
                                        ErrorMessage="Please enter mobile." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator6Resource1"></asp:RequiredFieldValidator>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Custom, Numbers"
                                        ValidChars=" ,+,(,),-" TargetControlID="txt_phone" Enabled="True">
                                    </cc1:FilteredTextBoxExtender>
                                </div>
                                <asp:HiddenField runat="server" ID="hf_userid" />
                                <asp:HiddenField runat="server" ID="hf_jobid" />
                                <asp:HiddenField runat="server" ID="hf_userlevel" />
                                 <asp:HiddenField runat="server" ID="hdnImage" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                    
                                <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
                                <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>
                                <script type="text/javascript">

                                    function GetCourt(id) {
                                        //var $accui = jQuery.noConflict(true);

                                        $("#" + id).autocomplete({
                                            source: function (request, response) {
                                                $.ajax({
                                                    type: "POST",
                                                    contentType: "application/json; charset=utf-8",
                                                    url: "ContactList.aspx/GetAutoCompleteCourt",
                                                    data: "{'court':'" + document.getElementById('<%=txt_name.ClientID %>').value + "'}",
                                                    dataType: "json",
                                                    success: function (data) {
                                                        response(data.d);
                                                    },
                                                    error: function (result) {
                                                        alert("Error");
                                                    }
                                                });
                                            }
                                        });
                                    }
        
                                </script>
                    <div class="modal-footer">
                        <asp:Button ID="btn_submit" runat="server" class="btn btn-primary yellow" Text="Submit"
                            OnClientClick="SubmitBtn();return false;" ValidationGroup="chk" meta:resourcekey="btn_submitResource1">
                        </asp:Button>
                        <button data-dismiss="modal" class="btn btn-default black" type="button" id="btnClose">
                           <%-- <%= CommonMessages.Close%>--%>
                         <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Close" enableviewstate="false"/> </button>
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
        </div>
        <asp:UpdatePanel runat="server" ID="UpdatePanellist" UpdateMode="Conditional">
            <ContentTemplate>
                <div class="col-md-3" style="float: right; margin-right: 3%;">
                    <asp:TextBox ID="txtsearch" class="library_search" runat="server" AutoPostBack="True"
                        OnTextChanged="txtsearch_TextChanged" meta:resourcekey="txtsearchResource1"></asp:TextBox>
                </div>
                <div id="tab-content" style="background: none;">
                    <div id="tab0">
                        <ul class="document">
                            <asp:Repeater ID="rep_contact" runat="server">
                                <ItemTemplate>
                                    <li>
                                        <img src='../Log/upload/Userimage/<%# Eval("userImage") %>' alt="userImg" onerror='../Organisation/images/sign-in.jpg' />
                                        <asp:HiddenField runat="server" ID="hdnImage"/>
                                        <h1 style="text-transform: uppercase;">
                                            <a href="<%# String.Format("Profile.aspx?view={0}", Eval("ContactUserId")) %>">
                                                <%# Eval("userFullname") %></a>
                                        </h1>
                                        <p style="text-transform: uppercase;">
                                            <i class="fa fa-map-marker"></i>
                                            <%# Eval("couName") %>,
                                            <%# Eval("staName") %>
                                        </p>
                                        <p>
                                            <strong>Email :</strong><a href="#">
                                                <%# Eval("userEmail") %></a><br>
                                            <strong>Phone no :</strong>
                                            <%# Eval("usercontact") %></p>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>
                        </ul>
                    </div>
                  
                </div>
                  <asp:CheckBox runat="server" ID="refresh" OnCheckedChanged="refreshPage" Text="refresh" AutoPostBack="True" style="display: none"/>
            </ContentTemplate>
            <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="refresh" />
                    </Triggers>
        </asp:UpdatePanel>
       <%-- <asp:RadioButton runat="server" OnCheckedChanged="refreshPage" Text="refresh" ID="rd"/>--%>
              
        <%----%>
    </div>
    <!-- TIME LINE -->
    <!-- Recent Post -->
    <div class="col-md-3">
    </div>
    <!-- Twitter
    Widget -->
    <!-- Weather Widget -->
    <script type="text/javascript">
        $(document).ready(function () {
            setTimeout(function () { $('.document1').show(); }, 500);
        });
        function SubmitBtn() {


            if (!CheckValidations("chk")) return;
            var txtName = $('#<%=txt_name.ClientID %>').val();
            var txtDesc = $('#<%=txt_email.ClientID %>').val();
            //var returnData = CommonAJSON("Library.aspx/BtnSubmitAJAX", '{txtName:"' + txtName + '",txtDesc:"' + txtDesc + '"}');
            var ReturnData;
            $.ajax({
                type: "POST",
                cache: false,
                async: true,
                url: "ContactList.aspx/BtnSubmitAJAX",
                data: '{txtName:"' + txtName + '",txtDesc:"' + txtDesc + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    ReturnData = response.d;
                    if (ReturnData == "success") {

                        showpopup1();
                        $("#btnClose").click();
                        $('#<%=refresh.ClientID %>').click();
                        $(".active").click();
                        setTimeout(function () { $(".active").click(); }, 2000);

                    } else {
                        alert(ReturnData);
                    }
                },
                failure: function (msg) {

                    alert(msg);
                    ReturnData = msg;
                }
            });

        }
        function showpopup1() {
            document.getElementById("ContentPlaceHolder1_txt_name").value = "";
            document.getElementById("ContentPlaceHolder1_txt_address").value = "";
            document.getElementById("ContentPlaceHolder1_txt_email").value = "";
            document.getElementById("ContentPlaceHolder1_txt_phone").value = "";

        }
        function showpopup2() {
            ValidatorEnable(document.getElementById('<%=RequiredFieldValidator5.ClientID%>'),
    true); ValidatorEnable(document.getElementById('<%=RequiredFieldValidator2.ClientID%>'),
    true); ValidatorEnable(document.getElementById('<%=revEmail.ClientID%>'), true);
            ValidatorEnable(document.getElementById('<%=RegularExpressionValidator6.ClientID%>'),
    true); ValidatorEnable(document.getElementById('<%=RequiredFieldValidator6.ClientID%>'),
    true); ValidatorEnable(document.getElementById('<%=FilteredTextBoxExtender3.ClientID%>'),
    true);
        } </script>
        <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/themes/base/jquery-ui.css"
                                    rel="stylesheet" type="text/css" />
</asp:Content>
