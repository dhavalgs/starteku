﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using startetku.Business.Logic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading;
using System.Drawing;

public partial class Organisation_CompanyDirectionCategoryList : System.Web.UI.Page
{
    public static int ResLangId = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            GetAllCompanyCategory();
        }
    }

    protected void OpenCreateCompanyDirCat(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lblQuestionCat').click();", true);
        Session["CompCatID"] = 0;
        ResourceLanguageBM obj = new ResourceLanguageBM();
        DataSet ds = new DataSet();
        obj.LangCompanyID = Convert.ToInt32(Session["OrgUserId"]);
        obj.LangActCatId = Convert.ToInt32(Session["CompCatID"]);
        obj.LangType = "compcat";
        ds = obj.GetAllResourceLangTran();

        gridTranslate.DataSource = ds.Tables[0];
        gridTranslate.DataBind();
    }
    public void InsertCategory()
    {
        try
        {
            DirectionCategory obj = new DirectionCategory();

            var CompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            var LoggedUserId = Convert.ToInt32(Session["OrgUserId"]);

            obj.catName = txtname.Text;
            obj.catNameDN = txtnameDN.Text;
            obj.catDescription = txtdescription.Text;
            obj.catIndex = Convert.ToInt32(txtno.Text);
            obj.catCreatedBy = LoggedUserId;
            obj.catCreateDate = DateTime.Now;
            obj.catUpdateDate = DateTime.Now;
            obj.catIsActive = true;
            obj.catCompanyID = CompanyId;
            obj.catIsDelete = false;

            obj.InsertDirectionCategory();
            DataSet ds = obj.ds;

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) > 0)
                    {
                        int CompCatID = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
                        LangTranslationMasterBM lobj = new LangTranslationMasterBM();
                        string resLangID, LangText;
                        for (int i = 0; i < gridTranslate.Rows.Count; i++)
                        {
                            resLangID = ((HiddenField)gridTranslate.Rows[i].Cells[0].FindControl("hdnResLangID")).Value;
                            LangText = ((TextBox)gridTranslate.Rows[i].Cells[0].FindControl("txtTranValue")).Text;
                            lobj.LangType = "compcat";
                            lobj.LangActCatId = CompCatID;
                            lobj.ResLangID = Convert.ToInt32(resLangID);
                            lobj.LangText = LangText;
                            lobj.LangCompanyID = LoggedUserId;
                            lobj.InsertLangResource();
                        }
                    }
                    else
                    {
                        lblMsg.Text = hdnRecordAlreadyFound.Value;
                        lblMsg.ForeColor = Color.White;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex);
        }
    }

    protected void btnsave_Click(object sender, EventArgs e)
    {
        try
        {
            InsertCategory();
            GetAllCompanyCategory();
            //ddldirCat.SelectedIndex = 0;
            txtname.Text = "";
            txtnameDN.Text = "";
            txtdescription.Text = "";
            txtno.Text = "";
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "btnsave_Click");
        }


    }

    protected void GetAllCompanyCategory()
    {
        //ddlQuesCat
        ResLangId = Convert.ToInt32(Session["resLangID"]);

        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);

        DirectionCategory obj = new DirectionCategory();
        obj.catIsActive = true;
        obj.catIsDelete = false;
        obj.catID = "0";
        obj.LangID = ResLangId;
        obj.catCompanyID = companyId;
        obj.GetAllCompanyCategoryName();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvGrid_DirCat.DataSource = ds.Tables[0];
                gvGrid_DirCat.DataBind();

                gvGrid_DirCat.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvGrid_DirCat.DataSource = null;
                gvGrid_DirCat.DataBind();
            }
        }
        else
        {
            gvGrid_DirCat.DataSource = null;
            gvGrid_DirCat.DataBind();
        }

    }
    #region Culture
    protected override void InitializeCulture()
    {
        // string language = "French";
        string language = Convert.ToString(Session["Culture"]);

        string languageId = "";

        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!String.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }

        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion

    protected void gvGrid_DirCat_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "archive")
        {
            DirectionCategory obj = new DirectionCategory();
            obj.catID = Convert.ToString(e.CommandArgument);
            obj.catIsActive = true;
            obj.catIsDelete = false;
            obj.CompanyDirCatDelete();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    GetAllCompanyCategory();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
    }
}