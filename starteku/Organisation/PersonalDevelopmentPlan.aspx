﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="PersonalDevelopmentPlan.aspx.cs" meta:resourcekey="PageResource1" Inherits="Organisation_PersonalDevelopmentPlan" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <%-- <link href="https://cdn.datatables.net/1.10.15/css/dataTables.jqueryui.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.dataTables.min.css" rel="stylesheet" />--%>

    <link href="js/dataTables.jqueryui.min.css" rel="stylesheet" />
    <link href="js/responsive.dataTables.min.css" rel="stylesheet" />


    <%--  <link href="js/responsive.dataTables.min.css" rel="stylesheet" />--%>
    <style type="text/css">
        /*.tabs-menu li {
            width: 20%;
        }*/

        .ModalControlStyle {
            width: 300px !important;
        }

        .DirectionElement {
            border: 0px !important;
            font-weight: 100;
        }

        .DateDisplay {
            width: 20% !important;
            border: 0px !important;
        }

        .DirectionCategory {
            border: 0px !important;
            font-weight: bolder;
        }

        #divCompanyDirectionPDF tbody {
            border: 0px !important;
        }

        #childElement tbody {
            border: 0px !important;
        }

        #meetingDataList tbody {
            border: 1px solid #ddd;
        }

        #divRealtedDocumentList tbody {
            border: 1px solid #ddd;
        }
    </style>

    <link href="../Scripts/pickadate.js-3.5.6/lib/themes/classic.css" rel="stylesheet" />
    <link href="../Scripts/fullcalendor/pickADate/classic.date.css" rel="stylesheet" />


</asp:Content>

<%------------------------Jainam Shah 19-4-2017---- Resource maintain-- and code done-----------------------%>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">
        function ajaxindicatorstart(text) {


            if ($('body').find('#resultLoading').attr('id') != 'resultLoading') {
                $('body').append('<div id="resultLoading" style="display:none"><div><img src="img/ajax-loader.gif"><div>' + text + '</div></div><div class="bg"></div></div>');
            }

            $('#resultLoading').css({
                'width': '100%',
                'height': '100%',
                'position': 'fixed',
                'z-index': '10000000',
                'top': '0',
                'left': '0',
                'right': '0',
                'bottom': '0',
                'margin': 'auto'
            });

            $('#resultLoading .bg').css({
                'background': '#000000',
                'opacity': '0.7',
                'width': '100%',
                'height': '100%',
                'position': 'absolute',
                'top': '0'
            });

            $('#resultLoading>div:first').css({
                'width': '250px',
                'height': '75px',
                'text-align': 'center',
                'position': 'fixed',
                'top': '0',
                'left': '0',
                'right': '0',
                'bottom': '0',
                'margin': 'auto',
                'font-size': '16px',
                'z-index': '10',
                'color': '#ffffff'

            });

            $('#resultLoading .bg').height('100%');
            $('#resultLoading').fadeIn(300);
            $('body').css('cursor', 'wait');
        }
        function ajaxindicatorstop() {
            $('#resultLoading .bg').height('100%');
            $('#resultLoading').fadeOut(300);
            $('body').css('cursor', 'default');
        }

    </script>
    <%-- <script type="text/jscript" src="js/dataTables.responsive.min.js"></script>--%>


    <script type="text/javascript">
        var dataLP;
        var dataLP2;

        function GetCompDesc(comId, comActual, t) {
           
            //
            //var $divMyTest = $('#graphArea').clone();
            //$divMyTest.empty();
            //var htmlGridData = $('#ContentPlaceHolder1_test').html();
            //$divMyTest.append(htmlGridData);
            //$divMyTest
           // alert('hi');
            debugger;
           // $('#ContentPlaceHolder1_test').html($('#ContentPlaceHolder1_test').html().replace('background-color: lightyellow;', ''));
            //$('#ContentPlaceHolder1_test').html($('#ContentPlaceHolder1_test').html().replace('background-color:lightyellow;', ''));

            //$(t).parent().parent().parent().find('tr').each(function () {
            //    $(this).parent().html($(this).parent().html().replace('background-color: lightyellow;', ''));
            //});
            var dataText = $(t).text();
           // $(t).parent().parent().css('background-color', 'lightyellow');
            $("#ContentPlaceHolder1_Label6").text(dataText);
            $("#ContentPlaceHolder1_Label7").text(dataText);
            $.ajax({
                type: "POST",
                cache: false,
                async: true,
                url: "PersonalDevelopmentPlan.aspx/GetCompDesc",
                data: '{comId:"' + comId + '",comActual:"' + comActual + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                  

                    dataLP = response.d;
                    dataLP2 = response;



                    setTimeout(function () {
                     
                        //TextArea3.innerHtml(response.d[0].comchildlevel);
                        $('#TextArea3').empty();

                        // debugger;
                        if ($("#ContentPlaceHolder1_Hdn_Lang_id").val() == "English") {
                            var ttt = response.d[0].comchildlevel;
                            //ttt = ttt.replace(/<\/?[^>]+>/gi, '');
                            ttt = ttt.replace(/(<p[^>]+?>|<p>|<\/p>)/img, "");
                            //ttt = ttt.replace("width=\"1447\"", "");
                            //ttt = ttt.replace('<p>', '');                           
                            // ttt = ttt.replace('</p>', '');

                            //alert(JSON.stringify(ttt));
                            $('#TextArea3').append(ttt);
                        }

                        if ($("#ContentPlaceHolder1_Hdn_Lang_id").val() == "Danish") {
                            var ttt = response.d[0].comchildlevelDN;
                            //ttt = ttt.replace('<p>', '');
                            //ttt = ttt.replace('</p>', '');
                            ttt = ttt.replace(/(<p[^>]+?>|<p>|<\/p>)/img, "");
                            $('#TextArea3').append(ttt);
                        }

                        //
                        if (comActual != 5) {
                           
                            GetCompDescPlus1(comId, comActual + 1);
                        } else {
                            TextArea4.value = "";
                        }

                    }, 001);


                },
                failure: function (msg) {

                    alert(msg);
                    ReturnDataLP = msg;
                }
            });



        }
        function GetCompDescPlus1(comId, comActual) {
           
            $.ajax({
                type: "POST",
                cache: false,
                async: true,
                url: "PersonalDevelopmentPlan.aspx/GetCompDesc",
                data: '{comId:"' + comId + '",comActual:"' + comActual + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    dataLP = response.d;
                    dataLP2 = response;


                    setTimeout(function () {

                        $('#TextArea4').empty();


                        if ($("#ContentPlaceHolder1_Hdn_Lang_id").val() == "English") {
                            var ttt = response.d[0].comchildlevel;
                            //ttt = ttt.replace('<p>', '');
                            //ttt = ttt.replace('</p>', '');
                            ttt = ttt.replace(/(<p[^>]+?>|<p>|<\/p>)/img, "");
                            $('#TextArea4').append(ttt);
                        }

                        if ($("#ContentPlaceHolder1_Hdn_Lang_id").val() == "Danish") {
                            var ttt = response.d[0].comchildlevelDN;
                            //ttt = ttt.replace('<p>', '');
                            //ttt = ttt.replace('</p>', '');
                            ttt = ttt.replace(/(<p[^>]+?>|<p>|<\/p>)/img, "");
                            $('#TextArea4').append(ttt);
                        }

                        //  TextArea4.value = response.d[0].comchildlevel;

                    }, 01);


                },
                failure: function (msg) {

                    alert(msg);
                    ReturnDataLP = msg;
                }
            });



        }
        function GetCompDesc_FirstTime(comId, comActual, t) {
           
            $.ajax({
                type: "POST",
                cache: false,
                async: true,
                url: "PersonalDevelopmentPlan.aspx/GetCompDesc",
                data: '{comId:"' + comId + '",comActual:"' + comActual + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    dataLP = response.d;
                    dataLP2 = response;


                    if ($('#ContentPlaceHolder1_Hdn_Lang_id').val() == "Danish") {
                        $("#ContentPlaceHolder1_Label6").text(response.d[0].comCompetenceDN);
                        $("#ContentPlaceHolder1_Label7").text(response.d[0].comCompetenceDN);
                    }
                    else {
                        $("#ContentPlaceHolder1_Label6").text(response.d[0].comCompetence);
                        $("#ContentPlaceHolder1_Label7").text(response.d[0].comCompetence);
                    }

                    setTimeout(function () {

                        //TextArea3.innerHtml(response.d[0].comchildlevel);
                        $('#TextArea3').empty();

                        // debugger;
                        if ($("#ContentPlaceHolder1_Hdn_Lang_id").val() == "English") {
                            var ttt = response.d[0].comchildlevel;
                            //ttt = ttt.replace(/<\/?[^>]+>/gi, '');
                            ttt = ttt.replace(/(<p[^>]+?>|<p>|<\/p>)/img, "");
                            //ttt = ttt.replace('<p>', '');                           
                            // ttt = ttt.replace('</p>', '');

                            //alert(JSON.stringify(ttt));
                            $('#TextArea3').append(ttt);
                        }

                        if ($("#ContentPlaceHolder1_Hdn_Lang_id").val() == "Danish") {
                            var ttt = response.d[0].comchildlevelDN;
                            //ttt = ttt.replace('<p>', '');
                            //ttt = ttt.replace('</p>', '');
                            ttt = ttt.replace(/(<p[^>]+?>|<p>|<\/p>)/img, "");
                            $('#TextArea3').append(ttt);
                        }

                        //
                        if (comActual != 5) {
                            GetCompDescPlus1(comId, comActual + 1);
                        } else {
                            TextArea4.value = "";
                        }

                    }, 001);


                },
                failure: function (msg) {

                    alert(msg);
                    ReturnDataLP = msg;
                }
            });



        }
    </script>




    <script type="text/javascript">
        function closeModel() { if (CheckValidations('chkdoc')) { $('#popupAddNewDoc').modal('hide'); } }
        function closeModelCancel(a) {
            $('#' + a).modal('hide');
            //alert(a);
            //if (a == 'popupQuesAnswer')
            //{
            //    DisplayTab(6);
            //}
        }
    </script>

    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" meta:resourcekey="lblMsgResource1"></asp:Label>
    <asp:HiddenField runat="server" ID="GraphSubTitle" meta:resourcekey="ResourceGraphSubTitle"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="GraphAxisLevel" meta:resourcekey="ResourceGraphAxisLevel"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="GraphToolTip" meta:resourcekey="ResourceGraphToolTip"></asp:HiddenField>
    <asp:HiddenField ID="Hdn_Lang_id" runat="server"></asp:HiddenField>


    <asp:HiddenField ID="hdnqpercomID" runat="server" Value="0"></asp:HiddenField>

    <asp:HiddenField ID="hdnPDPHistoryID" runat="server" Value="0"></asp:HiddenField>

    <asp:HiddenField ID="hdnIsPDFPersonalComment" runat="server" Value="0"></asp:HiddenField>

    <asp:HiddenField ID="hdnIsDisplayInReport" runat="server" Value="0"></asp:HiddenField>

    <asp:HiddenField ID="hdnQueryUmID" runat="server" Value="0"></asp:HiddenField>
    <asp:HiddenField ID="hdnLoginUserID" runat="server" Value="0"></asp:HiddenField>
    <asp:HiddenField ID="hdnUserType" runat="server" Value="0"></asp:HiddenField>


    <asp:HiddenField ID="hdnPDPHistoryIDNotification" runat="server" Value="0"></asp:HiddenField>


    <asp:HiddenField ID="hdnQuestionID" runat="server" Value="0"></asp:HiddenField>


    <asp:HiddenField ID="hdnShow" runat="server" Value="Show" meta:resourcekey="ShowRes"></asp:HiddenField>
    <asp:HiddenField ID="hdnentries" runat="server" Value="entries" meta:resourcekey="entriesRes"></asp:HiddenField>
    <asp:HiddenField ID="hdnSearch" runat="server" Value="Search" meta:resourcekey="SearchRes"></asp:HiddenField>

    <asp:HiddenField ID="hdnShowing" runat="server" Value="Showing" meta:resourcekey="ShowingRes"></asp:HiddenField>
    <asp:HiddenField ID="hdnto" runat="server" Value="to" meta:resourcekey="toRes"></asp:HiddenField>
    <asp:HiddenField ID="hdnof" runat="server" Value="of" meta:resourcekey="ofRes"></asp:HiddenField>
    <asp:HiddenField ID="hdnentrie" runat="server" Value="entries" meta:resourcekey="entrieRes"></asp:HiddenField>

    <asp:HiddenField ID="hdnPrevious" runat="server" Value="of" meta:resourcekey="PreviousRes"></asp:HiddenField>
    <asp:HiddenField ID="hdnNext" runat="server" Value="entries" meta:resourcekey="NextRes"></asp:HiddenField>
    
    

    <asp:HiddenField ID="hdnPLPPointRes" runat="server" Value="Points" meta:resourcekey="PLPPointsRes1"></asp:HiddenField>
    <asp:HiddenField ID="hdnALPointRes" runat="server" Value="Points" meta:resourcekey="ALPPointRes1"></asp:HiddenField>
    <asp:HiddenField ID="hdnPDPointRes" runat="server" Value="Points" meta:resourcekey="PDPPointres1"></asp:HiddenField>
    <div id="competenceDevelopment" style="display: none;">
        <asp:Label ID="lblcompetenceDevelopment" runat="server" Text=""></asp:Label>
        <asp:Label ID="lblcompetenceDevelopmentPoint" runat="server" Text=""></asp:Label>
    </div>




    <a href="#popupQuesAnswer" data-toggle="modal" title="" style="display: none">
        <asp:Label runat="server" ID="lblQuestionAnswer" CssClass="lblModel" Style="font-size: 19px; margin-top: 4px;"></asp:Label>
    </a>
    <%--QuestionAnswer--%>
    <div>
        <div aria-hidden="true" role="dialog" tabindex="-1" class="modal fade col-md-12" id="popupQuesAnswer" style="display: none;">
            <div class="modal-dialog" style="width: 55%">
                <div class="modal-content">
                    <div class="modal-header blue yellow-radius">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                            ×</button><h4 class="modal-title">
                                <asp:Label runat="server" ID="Label8" CssClass="" Style="color: white; font-size: 19px; margin-top: 4px;">
                                    <asp:Literal ID="Literal18" runat="server" Text="Answer Question" meta:resourcekey="AnswerQuesRes"
                                        EnableViewState="true" />
                                </asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <div id="Div3" runat="server" style="clear: both" visible="false">
                                <div class="col-md-3" style="padding-top: 10px;">
                                    <asp:Label runat="server" ID="Label9" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                        <asp:Literal ID="Literal19" runat="server" meta:resourcekey="Error" EnableViewState="false" />
                                    </asp:Label>
                                </div>
                                <div class="col-md-9">
                                    <asp:Label runat="server" ID="Label10" ForeColor="red" />
                                </div>
                            </div>
                        </div>
                        <br />

                        <div class="form-group">
                            <div class="col-md-3" style="padding-top: 10px;">
                                <asp:Label runat="server" ID="Label11" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                    <asp:Literal ID="Literal20" runat="server" Text="Question" EnableViewState="false" meta:resourcekey="QuestionResource" />
                                </asp:Label>
                            </div>
                            <div class="col-md-9" style="margin-top: 10px;">
                                <asp:TextBox runat="server" ID="txtQuestion" placeholder="Question" ReadOnly="true" TextMode="MultiLine" MaxLength="200" meta:resourcekey="Quesres" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3" style="padding-top: 10px;">
                                <asp:Label runat="server" ID="Label13" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                    <asp:Literal ID="Literal22" runat="server" Text="Answer" EnableViewState="false" meta:resourcekey="AnswerRes" />
                                </asp:Label>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox runat="server" ID="txtAnswer" placeholder="Enter Answer" TextMode="MultiLine" MaxLength="250" meta:resourcekey="EnterAnsRes" />
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <asp:Button runat="server" ID="btnSaveAnswer"
                            Text="Create" CssClass="btn btn-primary yellow"  meta:resourcekey="btnCreate"
                            type="button" OnClick="SaveQuestionAnswer_Click"
                            OnClientClick="closeModelCancel('popupQuesAnswer');"
                            Style="border-radius: 5px; width: 100px; height: 38px; margin-left: 359px;" />

                        <%-- OnClick="CreateQuestionCategory" --%>

                        <%--  <asp:Button runat="server" 
                           CssClass="btn btn-primary yellow"
                             OnClick="btnCreateCat_click" Text="Create Category"
                            Style="border-radius: 5px; width: 150px; height: 38px; margin-left: 359px;" />--%>
                        <asp:Button runat="server" ID="Button2" data-dismiss="modal"
                            Text="Close" CssClass="btn btn-default black" meta:resourcekey="btnClose"
                            type="button" ValidationGroup="chkdoc1" OnClientClick="closeModelCancel('popupQuesAnswer');"
                            Style="border-radius: 5px; width: 100px; height: 38px;" />


                    </div>
                </div>
            </div>
        </div>
    </div>




    <div class="">
        <div class="container">
            <div class="col-md-12">
                <div class="heading-sec">
                    <h1 style="color: white;">
                        <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Personaldevelopment" EnableViewState="false" /><i><span style="color: white;"
                            runat="server" id="Personaldevelopment"></span></i>
                        <i style="color: white;"><span runat="server" id="Settings"></span></i>
                    </h1>
                </div>
                <%--<div class="heading-sec">
                    <h1 style="color: white;">
                        <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Personaldevelopment" EnableViewState="false" />

                    </h1>
                    <div class="col-md-2" style="margin-top: 2%">
                        <i>
                            <span style="color: white;" runat="server" id="Personaldevelopment"></span>
                        </i>
                        <i style="color: white;"><span runat="server" id="Settings"></span></i>
                    </div>
                </div>--%>
            </div>

            <div class="col-md-12" style="margin-top: 15px;">
                <div class="col-md-2">
                    <div class="dropdown-example">
                        <asp:DropDownList Width="270px" ID="ddActCate" AutoPostBack="false" runat="server" CssClass="chkliststyle form-control" onchange="dateget(this)"
                            RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                            DataValueField="value" Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; color: white; overflow: auto;" Visible="True">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-4" style="float: left; color: white; font-size: medium; margin-left: -2%; display: none;">
                    <asp:Label ID="lblddlUserName" runat="server" EnableViewState="false" />
                </div>
            </div>



            <div id="styleData">
                <style type="text/css">
                    body {
                        margin: 0px;
                    }

                    header {
                        width: 100%;
                    }

                    .fullpart {
                        width: 100%;
                        padding-top: 20px;
                        padding-bottom: 10px;
                        float: left;
                        background-color: #ffffff;
                    }

                    .container {
                        width: 960px;
                        margin: 0 auto;
                    }

                    .logo {
                        float: right;
                        height: 62px;
                    }

                    .middle-text {
                        float: left;
                        margin-left: 15px;
                        font-size: 20px;
                    }

                    .person {
                        float: left;
                    }


                    section {
                        width: 100%;
                        float: left;
                    }

                    h1 {
                        font-size: 23px;
                        font-family: Verdana, Arial, Helvetica, sans-serif;
                        color: #5781bd;
                    }


                        h1 span {
                            color: #e59c2e;
                        }

                    h2 {
                        font-size: 23px;
                        font-family: Verdana, Arial, Helvetica, sans-serif;
                        color: #5781bd;
                    }

                    .borderbox-text {
                        border: solid #000000 1px;
                        font-size: 20px;
                        color: #818181;
                        font-style: italic;
                        padding: 10px;
                    }

                    .person1-box {
                        width: 960px;
                        float: left;
                        margin-top: 27px;
                    }

                    .bluecolor-part {
                        /*width: 1080px;*/
                        width: 100%;
                        margin-top: 30px;
                        float: left;
                        background-color: #025c9c;
                        padding: 14px;
                    }

                    .bluecolor-part-box1 {
                        width: 23%;
                        /*width: 240px;*/
                        float: left;
                        background-color: #00c8ff;
                        border-radius: 5px;
                        text-align: center;
                        color: #f9fbff;
                        padding-top: 14px;
                        padding-bottom: 14px;
                    }

                    .bluecolor-part-box2 {
                        width: 23%;
                        float: left;
                        background-color: #5fd8f5;
                        border-radius: 5px;
                        text-align: center;
                        color: #f9fbff;
                        padding-top: 14px;
                        padding-bottom: 14px;
                        margin-left: 25px;
                    }

                    .bluecolor-part-box3 {
                        width: 23%;
                        float: left;
                        background-color: #35aaff;
                        border-radius: 5px;
                        text-align: center;
                        color: #f9fbff;
                        padding-top: 14px;
                        padding-bottom: 14px;
                        margin-left: 25px;
                    }

                    .bluecolor-part-box4 {
                        width: 23%;
                        float: left;
                        background-color: #35aaff;
                        border-radius: 5px;
                        text-align: center;
                        color: #f9fbff;
                        padding-top: 14px;
                        padding-bottom: 14px;
                        margin-left: 25px;
                    }

                    .chart-part {
                        width: 220px;
                        float: left;
                        padding: 5px;
                    }

                    .para {
                        font-size: 20px;
                        font-weight: bold;
                    }

                    /*Comment DIV*/
                    #txtCmt {
                        overflow-x: hidden;
                        overflow-y: auto;
                        background: #f4f4f4;
                        height: 315px;
                    }

                    .reply {
                        clear: both;
                    }

                    .chat-desc p {
                        word-wrap: break-word;
                        width: 100%;
                    }
                </style>
            </div>


            <div class="col-md-12" style="margin-top: 20px;" id="htmlData" runat="server">
                <div id="graph-wrapper">
                    <div class="col-md-12">
                        <div id="tabitems_my">
                            <ul class="tabs-menu maintab" style="width: 100%;">
                                <li id="t1" class="tabsitem"><a href="#" class="current" onclick="DisplayTab(1);">
                                    <asp:Literal ID="Literal7" runat="server" Text="Basic Info" meta:resourcekey="BasicInfo"
                                        EnableViewState="false" /></a></li>
                                <li id="t6" class="tabsitem"><a href="#" onclick=" DisplayTab(6);">
                                    <asp:Literal ID="Literal17" runat="server" Text="Question" meta:resourcekey="Question" EnableViewState="false" /></a></li>

                                <li id="t2" class="tabsitem"><a href="#" onclick="DisplayTab(2); setTimeout(function () {DrawChartAlpPLpPDP(umId);}, 1000);">
                                    <asp:Literal ID="Literal8" runat="server" Text="Points" meta:resourcekey="PointsTitle" EnableViewState="false" /></a></li>
                                <li id="t3" class="tabsitem"><a href="#" onclick="DisplayTab(3);">
                                    <asp:Literal ID="Literal9" runat="server" Text="Competence Development " meta:resourcekey="CompetenceDevelopment" EnableViewState="false" /></a></li>

                                <li id="t4" class="tabsitem"><a href="#" onclick="DisplayTab(4);  ">
                                    <asp:Literal ID="Literal10" runat="server" Text="Activitites " meta:resourcekey="Activitites" EnableViewState="false" /></a></li>

                                <li id="t5" class="tabsitem" style="display: none;"><a href="#" onclick=" DisplayTab(5);">
                                    <asp:Literal ID="Literal2" runat="server" Text="Comments" meta:resourcekey="Comments" EnableViewState="false" /></a></li>


                            </ul>
                        </div>
                        <div class="chart-tab">
                            <div id="tabs-container">
                                <div class="loaderDiv" style="display: none">
                                    <div class="progress small-progress">
                                        <div style="width: 40%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40"
                                            role="progressbar" class="progress-bar blue">
                                        </div>
                                    </div>

                                    <div class="home_grap">
                                        <div id="ContainerDevelopment" style="min-width: 310px; height: 400px; margin: 0 auto">
                                            <div id="waitDevelopment" style="margin: 190px 602px">
                                                <img src="../images/wait.gif" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="tab">
                                    <div id="tab-1" class="tab-content">
                                        <div class="col-md-12">
                                            <% if (string.IsNullOrWhiteSpace(Convert.ToString(Session["Cemcompid"])))
                                               { %>
                                            <div class="col-md-12">
                                                <% }
                                               else
                                               {
                                                %>
                                                <div class="col-md-6">
                                                    <% } %>
                                                    <div id="tab1Data">
                                                        <div style="clear: both"></div>
                                                        <h1 id="divPersonalData">
                                                            <asp:Literal ID="Literal12" runat="server" meta:resourcekey="PersonalDevPlan" EnableViewState="false" />

                                                        </h1>
                                                        <div class="col-md-12" style="font-size: 15px; padding-top: 5px; margin-bottom: 5%;" id="divEmpDesc">
                                                            <div class=" col-md-2" style="width: 22%; float: left; text-align: right;" id="userImageData">

                                                                <asp:Image ID="userImg" CssClass="userImgClass" Style="width: 100px; height: 100px;" ImageUrl="~/Organisation/images/profile_img.png" runat="server"
                                                                    onerror="this.onerror=null;this.src='/starteku/organisation/images/profile_img.png'" />
                                                            </div>
                                                            <div class=" col-md-8" style="font-size: 15px;">
                                                                <asp:Label ID="Lblcom" runat="server" Text="Company name"></asp:Label>/
                                                            <asp:Label ID="Lbldiv" runat="server" Text="Division"></asp:Label><br />
                                                                <asp:Label ID="LdlEmpna" runat="server" Text=" Employees name"></asp:Label>/
                                                            <asp:Label ID="LblJob" runat="server" Text="Jobtitle"></asp:Label>Jobtitle
                                                    /
                                                            <asp:Label ID="lblTeamName" runat="server" Text="TeamName"></asp:Label><br />
                                                                <asp:Label ID="LblEmail" runat="server" Text="E-mail"></asp:Label>/
                                                            <asp:Label ID="Lblpnum" runat="server" Text="Phone number"></asp:Label><br />

                                                            </div>
                                                        </div>

                                                        <div style="margin-bottom: 5%;">
                                                            <p id="tab1IntroData" class=" col-md-12" style="width: 100%; line-height: 15px; padding: 0px; margin: 0px; margin-bottom: 5%; border: solid #000000 1px; font-size: 15px; color: #818181; font-style: italic; padding: 10px; font-weight: normal;" align="left" dir="ltr">
                                                                <asp:Literal ID="Literal6" runat="server" meta:resourcekey="Tab1desc" EnableViewState="false" />
                                                            </p>
                                                        </div>

                                                    </div>
                                                    <div>
                                                        <div class="" id="divMeetingStatus">
                                                            <table style="width: 100%;">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label runat="server" ID="lbl" meta:resourcekey="Meetingstatus" Font-Bold="true"></asp:Label></td>
                                                                    <td>
                                                                        <asp:Label ID="Label5" runat="server"></asp:Label>
                                                                        <asp:Label ID="Label15" runat="server"></asp:Label>
                                                                        <br />
                                                                    </td>
                                                                    <td><a href="#myModal" data-toggle="modal" title="" style="display: none;">
                                                                        <asp:Label runat="server" ID="lblMeeting" CssClass="lblModel" Style="font-size: 19px; margin-top: 4px;"></asp:Label></a>
                                                                        <asp:Button runat="server" ID="btnAddQues" Text="Add Meeting" CssClass="btn btn-default yellow HideControl" meta:resourcekey="btnAddmeeting"
                                                                            type="button" OnClick="OpenAddMeeting" Style="border-radius: 5px; height: 38px; color: white; width: 140px; float: right;" /></td>
                                                                </tr>
                                                            </table>
                                                        </div>


                                                        <div class="" style="height: 80px; overflow: auto;">
                                                            <div id="meetingDataList">
                                                                <asp:GridView ID="gvStatus_Display" runat="server" AutoGenerateColumns="false" ShowHeader="false" BorderStyle="None"
                                                                    CssClass="table table-striped table-bordered table-hover table-checkable datatable" DataKeyNames="metID"
                                                                    OnRowCommand="gvStatus_Display_RowCommand"
                                                                    OnRowDataBound="gvStatus_Display_RowDataBound">
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <%#Eval("Date_part") %>
                                                                            </ItemTemplate>
                                                                            <ItemStyle HorizontalAlign="Left" CssClass="DirectionElement" />
                                                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />

                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <%#Eval("Participants") %>
                                                                            </ItemTemplate>
                                                                            <ItemStyle HorizontalAlign="Left" CssClass="DirectionElement" />
                                                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />

                                                                        </asp:TemplateField>


                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:HiddenField runat="server" ID="hdnMetStatus" Value=' <%#Eval("metStatus") %>' />
                                                                                <asp:Label runat="server" ID="lblMetStatus"></asp:Label>

                                                                            </ItemTemplate>
                                                                            <ItemStyle HorizontalAlign="Left" CssClass="DirectionElement" />
                                                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />

                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField HeaderText="">
                                                                            <ItemTemplate>

                                                                                <asp:Panel runat="server" ID="viewPanel" Visible='<%# Convert.ToInt32( Eval("metID")) != 0 ? true : false %>'>

                                                                                    <asp:LinkButton ID="lnkEdit" CausesValidation="false" CssClass="def HideControl" runat="server" CommandName="EditRow" CommandArgument='<%# Eval("metID") %>'
                                                                                        ToolTip="Edit" meta:resourcekey="Edit">
                                                                     <i class="fa fa-pencil"></i>
                                                                                    </asp:LinkButton>
                                                                                    <asp:LinkButton ID="lnkBtnName" CausesValidation="false" CssClass="def lnkBtnName HideControl" runat="server"
                                                                                        OnClientClick="return confirm('Are you sure you want to Delete  this record?');"
                                                                                        CommandName="archive" CommandArgument='<%# Eval("metID") %>'
                                                                                        ToolTip="Archive" meta:resourcekey="Archive"><i class="fa fa-trash-o"></i></asp:LinkButton>
                                                                                </asp:Panel>
                                                                            </ItemTemplate>
                                                                            <ItemStyle HorizontalAlign="center" CssClass="hidden-xs" Width="50px" />
                                                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                        </asp:TemplateField>

                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>
                                                        <div class="" id="divRelatedDoc" style="margin-top: 5%;">
                                                            <table style="width: 100%;">
                                                                <tr>
                                                                    <td>
                                                                        <asp:Label runat="server" ID="lbl2" meta:resourcekey="RelatedDocRes" Font-Bold="true"></asp:Label>
                                                                    </td>
                                                                    <td>
                                                                        <asp:Label ID="Label16" runat="server"></asp:Label>
                                                                        <asp:Label ID="Label19" runat="server"></asp:Label>
                                                                        <br />
                                                                    </td>
                                                                    <td><a href="#popupAddNewQuesCat" data-toggle="modal" title="" style="display: none;">
                                                                        <asp:Label runat="server" ID="lblQuestionCat" CssClass="lblModel" meta:resourcekey="RelatedDocRes" Style="font-size: 19px; margin-top: 4px;"></asp:Label></a>
                                                                        <asp:Button runat="server" ID="btnAddQuesCat" Text="Upload Document" CssClass="btn btn-default yellow HideControl" meta:resourcekey="btnUpload"
                                                                            type="button" Style="border-radius: 5px; height: 38px; color: white; width: 140px; float: right;" OnClick="OpenUploadDocument" /></td>
                                                                </tr>
                                                            </table>
                                                        </div>

                                                        <div style="height: 80px; overflow: auto;">
                                                            <div class="" style="" id="divRealtedDocumentList">
                                                                <asp:GridView ID="gridMeetingDocument" runat="server" AutoGenerateColumns="false" ShowHeader="false" BorderStyle="None"
                                                                    CssClass="table table-striped table-bordered table-hover table-checkable datatable" EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'
                                                                    OnRowCommand="gridMeetingDocument_RowCommand">
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <%#Eval("CreateDate") %>
                                                                            </ItemTemplate>
                                                                            <ItemStyle HorizontalAlign="Left" CssClass="DateDisplay" />
                                                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                Width="5%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />

                                                                        </asp:TemplateField>

                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Label21" Text='<%# Eval("DocumentName") != "" ? Eval("DocumentName")+".pdf" : "" %>'
                                                                                    runat="server" />



                                                                            </ItemTemplate>
                                                                            <ItemStyle HorizontalAlign="Left" CssClass="DirectionElement" />
                                                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                Width="50%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />

                                                                        </asp:TemplateField>
                                                                        <asp:TemplateField HeaderText="">
                                                                            <ItemTemplate>

                                                                                <asp:Panel runat="server" ID="viewPanel" Visible='<%# Convert.ToInt32( Eval("meetDocID")) != 0 ? true : false %>'>

                                                                                    <asp:LinkButton ID="lnkBtnName" CausesValidation="false" CssClass="def lnkBtnName HideControl" runat="server"
                                                                                        OnClientClick="return confirm('Are you sure you want to Delete  this record?');"
                                                                                        CommandName="archive" CommandArgument='<%# Eval("meetDocID") %>'
                                                                                        ToolTip="Archive" meta:resourcekey="Archiveres"><i class="fa fa-trash-o"></i></asp:LinkButton>
                                                                                    <a href="#" title="" class="tooltipRead" data-tooltip="Read"
                                                                                        data-placement="bottom" onclick="window.open('../Log/upload/Document/<%# Eval("DocumentPath") %>', '_blank');"><i class="fa fa-eye" style="margin-top: 9px;"></i></a>
                                                                                </asp:Panel>
                                                                            </ItemTemplate>
                                                                            <ItemStyle HorizontalAlign="center" CssClass="hidden-xs" Width="50px" />
                                                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                        </asp:TemplateField>
                                                                    </Columns>
                                                                </asp:GridView>
                                                            </div>
                                                        </div>


                                                        <div class="" style="font-size: 15px; margin-top: 5%;" id="divApproval">

                                                            <div class="" style="margin-top: -15px;">
                                                                <div style="margin-top: 10px;">
                                                                    <%-- <label style="margin-left: -50px;">Approvals:</label>--%>
                                                                    <asp:Label ID="Label14" Style="font-weight: bolder;" meta:resourcekey="Thisre" runat="server" Text="Approvals:" Visible="true">

                                                                    </asp:Label>
                                                                </div>
                                                                <div class="col-sm-8 enddiv">
                                                                </div>
                                                            </div>
                                                            <%-- <div class="form-group col-md-12" style="margin-left: -30px;">--%>
                                                            <table style="width: 100%;" border="1">
                                                                <tr>
                                                                    <td style="width: 40%;">
                                                                        <asp:Label ID="Label3" runat="server" meta:resourcekey="HighApprovedby" Text="High Approved by:" Visible="true"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 40%;">
                                                                        <asp:Label ID="lblHigherAuthName" runat="server" meta:resourcekey="NotApproved"></asp:Label>
                                                                        <asp:Label ID="hidate" runat="server"></asp:Label>
                                                                        <br />
                                                                    </td>
                                                                    <td class="RemoveTD">
                                                                        <asp:Button runat="server" ID="btnManap"
                                                                            OnClientClick="return fnApprove(false);"
                                                                            Text="Manager Approved"
                                                                            CssClass="btn btn-primary yellow HideControl"
                                                                            type="button" Style="border-radius: 5px; height: 38px; color: white; width: 140px; float: right;"
                                                                            meta:resourcekey="BHighApprovedby"
                                                                            Visible="false" />

                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="width: 40%;">
                                                                        <asp:Label ID="Label4" runat="server" meta:resourcekey="LowApprovedby" Text="Low Approved by:" Style="margin-right: 4px;" Visible="true"></asp:Label>
                                                                    </td>
                                                                    <td style="width: 40%;">
                                                                        <asp:Label ID="lblLowerAuthName" runat="server" meta:resourcekey="NotApproved">
                                                                        </asp:Label><asp:Label ID="lowdate" runat="server"></asp:Label>
                                                                        <br />
                                                                    </td>
                                                                    <td class="RemoveTD">
                                                                        <asp:Button runat="server" ID="btnEmpap" Text="Employee Approved"
                                                                            OnClientClick="return fnApprove(true);"
                                                                            CssClass="btn btn-primary yellow HideControl"
                                                                            type="button" Visible="true"
                                                                            Style="border-radius: 5px; height: 38px; color: white; width: 140px; float: right;"
                                                                            meta:resourcekey="BLowApprovedby" />

                                                                    </td>
                                                                </tr>
                                                            </table>

                                                            <%--</div>--%>
                                                        </div>
                                                        <%--<div class="form-group col-md-6">
                                                        <a href="myBtn" data-toggle="modal" id="myBtn" class="btn btn-default yellow ">Add Meeting</a>--%><%--<asp:Button runat="server" ID="btnManap"
                                                    OnClientClick="return fnApprove();"
                                                    Text="Manager Approved"
                                                    CssClass="btn btn-primary yellow"
                                                    type="button" Style="margin-top: 5px;"
                                                    meta:resourcekey="HighApprovedby"
                                                    Visible="False" />&nbsp;--%><%--<asp:Button runat="server" ID="btnEmpap" Text="Employee Approved"
                                                OnClientClick="return fnApprove();"  
                                                CssClass="btn btn-primary yellow"
                                                type="button"
                                                Style="margin-top: 5px;"
                                                meta:resourcekey="LowApprovedby"
                                                Visible="False" />&nbsp;--%><%--</div>--%>
                                                    </div>
                                                </div>
                                                <% if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["Cemcompid"])))
                                                   { %>
                                                <div style="overflow: auto; border-style: solid; border-width: thin; height: 600px; margin-top: 5%; margin-left: 2%; padding-left: 1%;">
                                                    <div class="" id="divCompanyDirectionPDF">

                                                        <asp:GridView ID="gridPublishDirectionElement" runat="server" AutoGenerateColumns="False" CellPadding="0"
                                                            Width="100%" GridLines="None" DataKeyNames="catID"
                                                            EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>' BorderStyle="None"
                                                            BackColor="White" ShowHeader="false"
                                                            meta:resourcekey="GridRecordNotfound"
                                                            OnRowDataBound="gridPublishDirectionElement_RowDataBound">

                                                            <HeaderStyle CssClass="" />
                                                            <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                                            <AlternatingRowStyle BackColor="White" />
                                                            <Columns>
                                                                <asp:TemplateField>
                                                                    <ItemTemplate>

                                                                        <asp:HiddenField runat="server" ID="hdngridPublishcatID" Value='<%#Eval("catID") %>' />
                                                                        <%#Eval("CatNameInLang") %>

                                                                        <br />
                                                                        <asp:Label ID="lblNoRecordFoundQues" Visible="false" ForeColor="Red" runat="server" Text="No record found" meta:resourcekey="RecordNotfoundResource" EnableViewState="false" />
                                                                        <div class="col-md-12">
                                                                            <div id="childElement">
                                                                                <asp:GridView ID="gvPublishElement_AssignList" runat="server" AutoGenerateColumns="false"
                                                                                    ShowHeader="false" BorderStyle="None" EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'
                                                                                    CssClass="table table-striped table-hover table-checkable datatable">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="">
                                                                                            <ItemTemplate>
                                                                                                <%#Eval("dirDescription") %>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle HorizontalAlign="Left" CssClass="DirectionElement" />
                                                                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                                Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />

                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Left" CssClass="DirectionCategory" />
                                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                        Width="5%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <FooterStyle BackColor="#cccccc" Font-Bold="True" ForeColor="Black" HorizontalAlign="Left" />
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                                <% } %>
                                            </div>
                                            <div class="form-group col-md-12" style="text-align: right;">
                                                &nbsp;
                                            <a id="A2" href="javascript:void(0)" runat="server" onclick="SaveIntroManagementDataPDF();" style="margin-top: 5px;" class="btn btn-primary yellow" meta:resourcekey="btnpdf">PDF</a>
                                                <%--  <asp:Button runat="server" ID="Button11" 
                                                Text="PDF" CssClass="btn btn-primary yellow" meta:resourcekey="btnpdf"
                                                type="button" OnClientClick="SaveIntroManagementDataPDF();" CausesValidation="false"
                                                Style="margin-top: 5px;" />--%>
                                            </div>
                                        </div>

                                        <div id="tab-2" class="tab-content">
                                            <h1>
                                                <asp:Literal ID="Literal13" runat="server" meta:resourcekey="PersonalDevPlan" EnableViewState="false" /></h1>
                                            <div id="plpPDFData">

                                                <div>

                                                    <div class="bluecolor-part" id="tab2Data">

                                                        <div class="bluecolor-part-box1">
                                                            <font style="font-size: 50px; font-family: Arial, Helvetica, sans-serif;"><strong>
                                                                <asp:Label ID="LblAlp" runat="server" Text="Label"></asp:Label></strong></font><br />
                                                            <font style="font-size: 20px; font-family: Arial, Helvetica, sans-serif;"><strong>ALP</strong></font>
                                                        </div>
                                                        <div class="bluecolor-part-box2">
                                                            <font style="font-size: 50px; font-family: Arial, Helvetica, sans-serif;"><strong>
                                                                <asp:Label ID="LblPpl" runat="server" Text="Label"></asp:Label></strong></font><br />
                                                            <font style="font-size: 20px; font-family: Arial, Helvetica, sans-serif;"><strong>PLP</strong></font>
                                                        </div>
                                                        <div class="bluecolor-part-box3">
                                                            <font style="font-size: 50px; font-family: Arial, Helvetica, sans-serif;"><strong>
                                                                <asp:Label ID="Lblallpoint" runat="server" Text="Label"></asp:Label></strong></font><br />
                                                            <font style="font-size: 20px; font-family: Arial, Helvetica, sans-serif;"><strong>Total</strong></font>
                                                        </div>
                                                        <div class="bluecolor-part-box4">
                                                            <font style="font-size: 50px; font-family: Arial, Helvetica, sans-serif;"><strong>
                                                                <asp:Label ID="LblPDPTotal" runat="server" Text="0"></asp:Label></strong></font><br />
                                                            <font style="font-size: 20px; font-family: Arial, Helvetica, sans-serif;"><strong>PDP</strong></font>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-12" id="graphArea" style="margin-top: 50px">
                                                    <div class="home_grap" style="" id="chartpdp">
                                                        <div class="graph-info" style="z-index: 9999999999">
                                                            <a href="#" id="AlpBars" style="z-index: 9999999999"><span><i class="fa fa-bar-chart-o"></i></span></a>
                                                            <a href="#" id="Alplines" class="active" style="z-index: 9999999999"><span><i class="fa fa-code-fork"></i></span></a>
                                                        </div>
                                                        <div class="graph-container">
                                                            <div id="graph-lines-alpplp" style="display: none">
                                                            </div>
                                                            <div id="graph-bars-alpplp">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="home_grap" id="chart" style="">
                                                        <div class="graph-info" style="z-index: 9999999999">
                                                            <a href="#" id="PDPBars" style="z-index: 9999999999"><span><i class="fa fa-bar-chart-o"></i></span></a>
                                                            <a href="#" id="PDPlines" class="active" style="z-index: 9999999999"><span><i class="fa fa-code-fork"></i></span></a>
                                                        </div>
                                                        <div class="graph-container">
                                                            <div id="graph-lines-pdp" style="display: none">
                                                            </div>
                                                            <div id="graph-bars-pdp">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <asp:HiddenField runat="server" ID="PDPImgBase64" Value="asdc" />
                                                    <asp:HiddenField runat="server" ID="alpImgBase64" Value="sef" />
                                                    <asp:HiddenField runat="server" ID="cmtimagedt" Value="sef" />
                                                    <asp:HiddenField runat="server" ID="hdnPdfUserId" />

                                                    <asp:HiddenField runat="server" ID="hdnPdfButtonId" />



                                                </div>
                                            </div>
                                            <%--/////////////////////////////////////////////////////////////////////upload Points//////////////////////////////////////////////////--%>

                                            <div class="invoice ALL" style="margin-top: 20px;">
                                                <h2 class="pointtable_h2" style="margin-bottom: -11px; width: 81%;">

                                                    <%--<span runat="server" id="Points" meta:resourcekey="PointsResource"></span>--%>
                                                    <asp:Label ID="Points" meta:resourcekey="PointsResource" runat="server" />
                                                    <asp:Label ID="lblFor" meta:resourcekey="ForResource" runat="server" />
                                                    <asp:Label ID="lblUserName" runat="server"></asp:Label><asp:HiddenField runat="server" ID="lblAcceptProvidedHelp" meta:resourcekey="lblAcceptProvidedHelpResource"></asp:HiddenField>
                                                    <asp:HiddenField runat="server" ID="lblRequestSession" meta:resourcekey="lblRequestSessionResource"></asp:HiddenField>
                                                    <asp:HiddenField runat="server" ID="lblSuggestSession" meta:resourcekey="lblSuggestSessionResource"></asp:HiddenField>
                                                    <asp:HiddenField runat="server" ID="lblReadDoc" meta:resourcekey="lblReadDocResource"></asp:HiddenField>
                                                    <asp:HiddenField runat="server" ID="lblReadDocByOther" meta:resourcekey="ReadDocByOtherResource"></asp:HiddenField>
                                                    <asp:HiddenField runat="server" ID="lblUploadPublicDoc" meta:resourcekey="lblUploadPublicDocResource"></asp:HiddenField>
                                                    <asp:HiddenField runat="server" ID="lblFinalFillCompetences" meta:resourcekey="lblFinalFillCompetencesResource"></asp:HiddenField>
                                                    <asp:HiddenField runat="server" ID="lblOpenCompetencesScreen" meta:resourcekey="lblOpenCompetencesScreenResource"></asp:HiddenField>
                                                    <asp:HiddenField runat="server" ID="lblSignIn" meta:resourcekey="lblSignInResource"></asp:HiddenField>

                                                    <span id="AAlp" runat="server" style="float: right;"></span>
                                                    <span id="Span1" runat="server" style="float: right;">
                                                        <asp:Label ID="lbltotal" runat="server" meta:resourcekey="Total"></asp:Label>: </span></h2>

                                            </div>
                                            <div class="plpalppointClass">
                                                <div style="width: 100%;">
                                                    <asp:GridView ID="gvGrid" runat="server" AutoGenerateColumns="False" CellPadding="0"
                                                        Width="100%" GridLines="None"
                                                        DataKeyNames="logUserId"
                                                        CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                                                        EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'
                                                        BackColor="White" meta:resourcekey="NoRecordResource"
                                                        OnRowDataBound="gvGridLog_RowDataBound">
                                                        <HeaderStyle CssClass="aa" />
                                                        <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                                        <AlternatingRowStyle BackColor="White" />
                                                        <Columns>
                                                            <asp:TemplateField HeaderText="Session"
                                                                meta:resourcekey="Session">
                                                                <ItemTemplate>
                                                                    <%-- <asp:Label ID="lblUNamer" CssClass="pointinfo" runat="server" Text='<%# Eval("logPointInfo") %>'
                                                                meta:resourcekey="lblUNamerResource1"></asp:Label>--%>
                                                                    <asp:Label ID="lblUNamer" CssClass="pointinfo" runat="server"
                                                                        meta:resourcekey="lblUNamerResource1"></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="20%" ForeColor="White" BorderWidth="0px" />
                                                            </asp:TemplateField>

                                                            <%-- <asp:TemplateField HeaderText="Document Name">

                                                        <ItemTemplate>
                                                            <asp:Label ID="lblUNamer1" runat="server" Text='<%# Eval("docFileName_Friendly") %>'></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                            Width="20%" ForeColor="White" BorderWidth="0px" />
                                                    </asp:TemplateField>--%>

                                                            <asp:TemplateField HeaderText="Description"
                                                                meta:resourcekey="Description">
                                                                <ItemTemplate>
                                                                    <asp:HiddenField runat="server" ID="lbllogpointinfo" Value='<%#Eval("logpointinfo") %>' />
                                                                    <asp:HiddenField runat="server" ID="hdnlblFromname" Value='<%#Eval("Fromname") %>' />
                                                                    <asp:HiddenField runat="server" ID="HiddenField1" Value='<%#Eval("Fromname") %>' />
                                                                    <asp:HiddenField runat="server" ID="hdnlblToname" Value='<%#Eval("Toname") %>' />
                                                                    <asp:HiddenField runat="server" ID="hdnlbllogDescription" Value='<%#Eval("logDescription") %>' />
                                                                    <asp:HiddenField runat="server" ID="hdnlblrdDoc" Value='<%#Eval("rdDoc") %>' />
                                                                    <asp:HiddenField runat="server" ID="hdnlbldocFileName_Friendly" Value='<%#Eval("docFileName_Friendly") %>' />
                                                                    <asp:HiddenField runat="server" ID="hdnlbldocFileName_System" Value='<%#Eval("docFileName_inSystem") %>' />
                                                                    <asp:Label ID="lblDescriptionLog" runat="server"></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="40%" ForeColor="White" BorderWidth="0px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Date" meta:resourcekey="Date">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblUNamer11" runat="server" Text='<%# Eval("logCreateDate") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="20%" ForeColor="White" BorderWidth="0px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Point" meta:resourcekey="PointRes">
                                                                <ItemTemplate>
                                                                    <asp:Label ID="lblUserNamer1" runat="server" Text='<%# Eval("TotalPoint") %>'></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="30%" ForeColor="White" BorderWidth="0px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="View" meta:resourcekey="ViewHed">
                                                                <ItemTemplate>

                                                                    <asp:HyperLink ID="linkDocumentOpen" runat="server" title="" data-tooltip="Read" data-placement="bottom"
                                                                        class="tooltipRead" Target="_blank">                                                               
                                                                <i class="fa fa-eye" style="margin-top: 9px;"></i></asp:HyperLink>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs lastrow" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow lastrow"
                                                                    Width="10%" ForeColor="White" BorderWidth="0px" />
                                                            </asp:TemplateField>
                                                        </Columns>
                                                    </asp:GridView>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12" style="text-align: right;">
                                                <asp:Button runat="server" ID="Button3" Text="Add Activity" OnClick="reac"
                                                    CssClass="btn btn-primary yellow"
                                                    type="button"
                                                    Style="margin-top: 5px;"
                                                    meta:resourcekey="Add_Act_but"
                                                    Visible="true" />
                                                &nbsp;
                                            <asp:Button runat="server" ID="Button6"
                                                Text="PDF" CssClass="btn btn-primary yellow"
                                                type="button" OnClientClick="SavePointDataPDF();" CausesValidation="false"
                                                Style="margin-top: 5px;" />
                                            </div>
                                            <%--////////////////////////////////////////////////////////////////////////upload Points/////////////////////////////////////////////////////--%>
                                        </div>

                                        <div id="tab-3" class="tab-content">
                                            <div id="tab3Data">
                                                <h1>
                                                    <asp:Literal ID="Literal14" runat="server" meta:resourcekey="PersonalDevPlan" EnableViewState="false" /></h1>

                                                <div class="col-md-12">

                                                    <div class="col-md-6" id="competencePDFData">
                                                        <%--  <asp:Label ID="Label14" runat="server" meta:resourcekey="CompetenceCategoryName"  Style="color: #02406D;"></asp:Label><br />--%>
                                                        <div id="test" runat="server" style="margin-top: 10px;">
                                                        </div>

                                                        <div class="para">
                                                            <p style="width: 100%; line-height: 40px; padding: 0px; margin: 0px; font-size: 20px; color: #000000; font-style: italic; padding: 10px; font-weight: normal;" align="left" dir="ltr">
                                                                <asp:Literal ID="Literal11" runat="server" meta:resourcekey="Tab3desc" EnableViewState="false" />
                                                            </p>


                                                        </div>
                                                    </div>



                                                    <div class="col-md-6" id="DescData">
                                                        <h2 class="step_titel" style="z-index: 0 !important; font-size: 18px; color: white">
                                                            <asp:Label ID="Label1" runat="server" meta:resourcekey="CurrentCompetencelevelDescription" Style=""></asp:Label>:
                                                        <asp:Label ID="Label6" runat="server" Style=""></asp:Label><br />
                                                        </h2>
                                                        <%-- <textarea id="TextArea3" cols="20" rows="2" style="width: 521px; height: 183px; margin-top: 10px; margin-bottom: 10px;" readonly></textarea><br />--%>
                                                        <div id="TextArea3" style="overflow-y: auto; display: inline-block;word-break: break-all;width: 100%; word-wrap: break-word; height: 183px; margin-top: 10px; margin-bottom: 10px; background-color: white;"></div>

                                                        <h2 class="step_titel" style="z-index: 0 !important; font-size: 18px;">
                                                            <asp:Label ID="Label2" runat="server" meta:resourcekey="NewCompetencelevelDescription"
                                                                Style=""></asp:Label>:
                                                        <asp:Label ID="Label7" runat="server" Style=""></asp:Label><br />
                                                        </h2>
                                                        <%--<textarea id="TextArea4" cols="20" rows="2" style="margin-top: 10px; width: 521px; height: 183px; top: 10px;" readonly></textarea>--%>
                                                        <div id="TextArea4" style="overflow-y: auto; overflow-x: hidden; margin-top: 10px; width: 100%; word-wrap: break-word; height: 183px; top: 10px; background-color: white;"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12" style="text-align: right;">
                                                <asp:Button runat="server" ID="Button1" Text="Add Activity" OnClick="reac"
                                                    CssClass="btn btn-primary yellow"
                                                    type="button"
                                                    Style="margin-top: 5px;"
                                                    meta:resourcekey="Add_Act_but"
                                                    Visible="true" />
                                                &nbsp;
                                            <asp:Button runat="server" ID="Button7" meta:resourcekey="btnpdf"
                                                Text="PDF" CssClass="btn btn-primary yellow"
                                                type="button" OnClientClick="SaveCompetenceDataPDF();" CausesValidation="false"
                                                Style="margin-top: 5px;" />
                                            </div>
                                        </div>

                                        <div id="tab-4" class="tab-content">


                                            <h1>
                                                <asp:Literal ID="Literal16" runat="server" meta:resourcekey="PersonalDevPlan" EnableViewState="false" /></h1>
                                            <p>
                                                <%-- <asp:Literal ID="Knowledgeshare" runat="server" meta:resourcekey="Knowledgeshare"
                                                EnableViewState="false" />--%>
                                            </p>
                                            <%-- <asp:Literal ID="Knowledgeshare" runat="server" meta:resourcekey="Knowledgeshare"
                                            EnableViewState="false" />--%>

                                            <div class="col-md-12 comp_table">

                                                <asp:GridView ID="gvGridCategory" runat="server" AutoGenerateColumns="False" CellPadding="0" Style="background-color: #f9f9f9;"
                                                    Width="100%" GridLines="None" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                                                    OnRowCommand="gvGrid_RowCommand"
                                                    OnRowDataBound="gvActCat_OnRowDataBound"
                                                    ShowHeader="False"
                                                    EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'
                                                    meta:resourcekey="NoRecordResource">


                                                    <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                                    <AlternatingRowStyle BackColor="White" />
                                                    <Columns>
                                                        <asp:TemplateField>

                                                            <ItemTemplate>
                                                                <h4>
                                                                    <asp:Label ID="Label20" runat="server" meta:resourcekey="ListOf" Text="List's Of"></asp:Label>
                                                                    <asp:Label ID="lblrNamer1" runat="server" Text='<%# Eval("ActCatName") %>'></asp:Label></h4>
                                                                <asp:Panel ID="pnlOrders" runat="server">
                                                                    <asp:HiddenField runat="server" ID="hdnActCatId" Value='<%#Eval("ActCatId") %>' />
                                                                    <asp:GridView ID="gvGrid_ActivityStatus_Tab4" runat="server" AutoGenerateColumns="False"
                                                                        CellPadding="0"
                                                                        Width="100%" GridLines="None" DataKeyNames="ActId"
                                                                        CssClass="gvGrid_ActivityStatus table1 table-striped table-bordered table-hover table-checkable datatable"
                                                                        EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'
                                                                        BackColor="White" UseAccessibleHeader="true"
                                                                        meta:resourcekey="NoRecordResource"
                                                                        OnRowCommand="gvGrid_RowCommand"
                                                                        OnRowDataBound="gvGrid_RowDataBound"
                                                                        OnRowCreated="gvGrid_RowCreated">


                                                                        <HeaderStyle CssClass="aa" />
                                                                        <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                                                        <AlternatingRowStyle BackColor="White" />
                                                                        <Columns>
                                                                            <asp:TemplateField HeaderText="Sr.No" meta:resourcekey="SrNores">
                                                                                <ItemTemplate>
                                                                                    <%# Container.DataItemIndex + 1 %>
                                                                                </ItemTemplate>
                                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                    Width="3%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField meta:resourcekey="ActivityName" HeaderText="Activity Name">
                                                                                <ItemTemplate>
                                                                                    <%#Eval("ActName") %>
                                                                                </ItemTemplate>
                                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                    Width="35%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField meta:resourcekey="ActivityType" HeaderText="Activity Type">
                                                                                <ItemTemplate>

                                                                                    <%#Eval("ActCatName") %>
                                                                                </ItemTemplate>
                                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                    Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                            </asp:TemplateField>
                                                                            <asp:TemplateField meta:resourcekey="Status" HeaderText="Status">
                                                                                <ItemTemplate>

                                                                                    <%--<%# ((Eval("ActReqStatus") == string.Empty ) || (Eval("ActReqStatus") == null ))? "Available" : Convert.ToString(Eval("ActReqStatus"))%>--%>

                                                                                    <asp:HiddenField runat="server" ID="hdnActReqStatus" Value='<%#Eval("ActReqStatus") %>' />
                                                                                    <asp:Label runat="server" ID="lblActReqStatus"></asp:Label>
                                                                                </ItemTemplate>
                                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                    Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField meta:resourcekey="CreatedDate" HeaderText="Created Date">
                                                                                <ItemTemplate>
                                                                                    <%#Convert.ToDateTime(Eval("ActCreatedDate")).ToString("dd-MMM-yyyy") %>
                                                                                </ItemTemplate>
                                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                    Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <FooterStyle BackColor="#cccccc" Font-Bold="True" ForeColor="Black" HorizontalAlign="Left" />
                                                                    </asp:GridView>
                                                                </asp:Panel>

                                                            </ItemTemplate>

                                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Font-Size="14px" />
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                Width="90%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                        </asp:TemplateField>


                                                    </Columns>
                                                </asp:GridView>



                                            </div>
                                            <div class="form-group col-md-12" style="text-align: right;">

                                                <asp:Button runat="server" ID="Button8" meta:resourcekey="btnpdf"
                                                    Text="PDF" CssClass="btn btn-primary yellow"
                                                    type="button" OnClientClick="SaveActivityDataPDF();" CausesValidation="false"
                                                    Style="margin-top: 5px;" />
                                            </div>
                                        </div>

                                        <div id="tab-5" class="tab-content">
                                            <div id="cmt"></div>
                                            <h1>
                                                <asp:Literal ID="Literal15" runat="server" meta:resourcekey="PersonalDevPlan" EnableViewState="false" /></h1>
                                            <div class="heading-sec">
                                                <h1 style="margin-left: 15px; margin-bottom: 15px;"></h1>
                                            </div>

                                            <div class="row">

                                                <div id="txtCmtDiv">
                                                    <ul id="txtCmt">
                                                        <li class="reply">No Record Found. </li>
                                                    </ul>
                                                    <%-- <div class="reply-sec">
                                                    <div class="reply-sec-MESSAGE" style="margin-left: 20px;">

                                                        <input type="text" meta:resourcekey="Comment" id="Text2" onkeydown="return (event.keyCode!=13);" class="txtcomment" style="padding: 10px;" />
                                                        <a href="#" title="" class="black" onclick="insertcomment()"><i class="fa fa-comments-o"></i></a>
                                                    </div>
                                                </div>--%><div class="reply-sec">
                                                    <div class="reply-sec-MESSAGE" style="margin-left: 20px;">

                                                        <input type="text" meta:resourcekey="Comment" id="Text2" onkeydown="return (event.keyCode!=13);" class="txtcomment" style="padding: 10px;" />
                                                        <a href="#" title="" class="black" onclick="insertcomment()"><i class="fa fa-comments-o"></i></a>
                                                    </div>
                                                </div>
                                                </div>
                                                <asp:TextBox ID="txtComment" align="right" runat="server" AutoPostBack="false" Text=""
                                                    MaxLength="1" TextMode="MultiLine" Style="float: left; margin-left: 50px; width: 295px; height: 196px; display: none"
                                                    Enabled="false"></asp:TextBox><br />

                                                <a href="#" id='btnChat' onclick="return GetActivityCommentById()" data-index="txtComment" class="black" style="display: none; float: left; margin-left: 310px; margin-top: 8px; padding-left: 10px; padding-top: 10px; height: 35px; width: 35px;"><i class="fa fa-comments-o"></i></a>
                                                <%----%>
                                                <asp:HiddenField ID="hdnActReqIdInt" runat="Server" Value="This is the Value of Hidden field" />
                                                <asp:HiddenField ID="hdnActReqUserId" runat="Server" Value="This is the Value of Hidden field" />


                                            </div>

                                        </div>

                                        <div id="tab-6" class="tab-content">
                                            <div class="col-md-12">
                                                <div class="col-md-7" style="height: 525px; overflow-y: scroll;">
                                                    <div id="QuesDataHtml1" style="width: 100%;">
                                                        <div style="margin-bottom: 2%; font-size: medium; font-weight: bolder;">
                                                            <%--                                                        <label>Questions & Answer</label>--%>
                                                            <asp:Label runat="server" ID="lbl3" meta:resourcekey="QuestionResource"></asp:Label>
                                                        </div>
                                                        <asp:GridView ID="gvGrid_Question" runat="server" AutoGenerateColumns="False" CellPadding="0"
                                                            Width="100%" GridLines="None" DataKeyNames="quesID" CssClass="table1 table-striped table-bordered table-hover table-checkable datatable"
                                                            EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'
                                                            BackColor="White"
                                                            meta:resourcekey="GridRecordNotfound"
                                                            OnRowCommand="gvGrid_Question_RowCommand"
                                                            OnRowDataBound="gvGrid_Question_OnRowDataBound">
                                                            <%-- OnRowDataBound="gvGrid_Question_RowDataBound"
                                                    OnRowCommand="gvGrid_Question_RowCommand">--%>

                                                            <%--<HeaderStyle CssClass="aa" />--%>
                                                            <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                                            <AlternatingRowStyle BackColor="White" />
                                                            <Columns>
                                                                <asp:TemplateField HeaderText="SrNo" meta:resourcekey="SrNoResource1">
                                                                    <ItemTemplate>
                                                                        Q.<%# Container.DataItemIndex + 1 %>
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="hidden-xs ItemStyle" Width="5%" />
                                                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="Question" meta:resourcekey="QuestionHeaderResource">
                                                                    <ItemTemplate>
                                                                        <div style="font-weight: 200;">
                                                                            <b>
                                                                                <%#Eval("Question") %>
                                                                            </b>
                                                                        </div>
                                                                        <br />
                                                                        <div style="margin: 2%;">
                                                                            <%#Eval("Answer") %>
                                                                        </div>
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Left" VerticalAlign="Top" CssClass="hidden-xs ItemStyle" Width="85%" />
                                                                    <HeaderStyle HorizontalAlign="Left" VerticalAlign="Top" />
                                                                </asp:TemplateField>

                                                                <asp:TemplateField HeaderText="" ItemStyle-Width="70px">
                                                                    <ItemTemplate>
                                                                        <asp:HiddenField runat="server" ID="hdnAnswerText" Value='<%# Eval("Answer") %>' />
                                                                         <asp:HiddenField runat="server" ID="hdnQuesText" Value='<%# Eval("Question") %>' />
                                                                        <asp:Panel runat="server" ID="EditQuesPanel">
                                                                            <i class="fa fa-pencil"></i>
                                                                            <asp:LinkButton ID="lnkEdit" CssClass="def HideControl" runat="server"
                                                                                CommandName="EditRow" CommandArgument='<%# Eval("quesID")  %>'
                                                                                ToolTip="Edit" meta:resourcekey="EditlnkRes">
                                                                                

                                                                            </asp:LinkButton>
                                                                        </asp:Panel>
                                                                    </ItemTemplate>
                                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs grEditRow" Width="15%" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <FooterStyle BackColor="#cccccc" Font-Bold="True" ForeColor="Black" HorizontalAlign="Left" />
                                                        </asp:GridView>
                                                    </div>
                                                </div>
                                                <div class="col-md-5">
                                                    <div class="row">
                                                        <div style="margin-bottom: 2%; margin-top: 2%;" id="QuesDataHtml2">
                                                            <%--                                                        <label style="font-size: medium; font-weight: bolder;">General Dialogue:</label>--%>
                                                            <asp:Label runat="server" ID="lbl4" meta:resourcekey="Generaldia" Font-Bold="true" Font-Size="Larger"></asp:Label>
                                                        </div>
                                                        <div id="QuesDataHtml5" style="display: none;">
                                                            <style type="text/css"></style>
                                                        </div>
                                                        <div id="txtCmtDivQuestion">
                                                            <div id="quesChatArea" style="height: 250px; overflow: hidden; overflow-y: scroll;">
                                                                <div id="QuesDataHtml3" style="width: 100%;">
                                                                    <ul id="txtCmtQuestion">
                                                                        <li class="reply">
                                                                            <asp:Label runat="server" ID="lbl55" meta:resourcekey="Notrecordfound"></asp:Label>
                                                                        </li>
                                                                    </ul>
                                                                    <%--                                                                    <li class="reply">No Record Found. </li>--%>
                                                                </div>
                                                            </div>
                                                            <div class="reply-sec">
                                                                <div class="reply-sec-MESSAGE" style="margin-left: 2px;">
                                                                    <input type="text" id="Text1" onkeydown="return (event.keyCode!=13);" style="padding: 5px; width: 85%;"
                                                                        class="txtcommentQuestion" />
                                                                    <a href="#" title="" class="black" onclick="insertcommentQuestion(this)"><i class="fa fa-comments-o"></i></a>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <asp:TextBox ID="txtCommentQuestion" align="right" runat="server" AutoPostBack="false" Text=""
                                                            MaxLength="1" TextMode="MultiLine" Style="float: left; margin-left: 50px; width: 295px; height: 196px; display: none"
                                                            Enabled="false"></asp:TextBox><br />

                                                        <a href="#" id='A1' onclick="return GetQuestionCommentById()" data-index="txtCommentQuestion" class="black" style="display: none; float: left; margin-left: 310px; margin-top: 8px; padding-left: 10px; padding-top: 10px; height: 35px; width: 35px;"><i class="fa fa-comments-o"></i></a>
                                                    </div>
                                                    <div class="row" style="height: 100px;">
                                                        <div>
                                                            <%--                                                        <label id="QuesDataHtml6">Personal Comment:</label>--%>
                                                            <asp:Label runat="server" ID="QuesDataHtml6" meta:resourcekey="personalcmt" Text="Personal Comment:"></asp:Label>
                                                            <span><a href="#" title="" class="black HideControl" onclick="GetPersonalComment('-1')"><i class="fa fa-chevron-left"></i></a>
                                                                <asp:Label runat="server" ID="lbl7" meta:resourcekey="History"></asp:Label>
                                                                <a href="#" title="" class="black HideControl" onclick="GetPersonalComment('1')"><i class="fa fa-chevron-right"></i></a>&nbsp;&nbsp; 
                                                            <asp:HiddenField runat="server" ID="hdn1" meta:resourcekey="Hiddenbtn" />
                                                                <a href="#" title="" id="btnPublicComment" style="width: 25%; text-align: center; float: right;" class="black HideControl" onclick="SetPersonalCommentPublic(1);">
                                                                    <asp:Label runat="server" ID="lbl8" meta:resourcekey="Displayinrep"></asp:Label></a>

                                                                <a href="#" title="" id="btnPrivateComment" style="width: 37%; text-align: center; margin-right: 4%; float: right;" class="black HideControl" onclick="SetPersonalCommentPublic(2);">
                                                                    <asp:Label runat="server" ID="lbl9" meta:resourcekey="Donotdisprep"></asp:Label></a></span>

                                                        </div>
                                                        <div id="QuesDataHtml7" style="overflow: scroll; overflow-x: hidden;width:100%;margin-top:2%;">
                                                            <asp:TextBox runat="server" ID="txtPersonalComment" Columns="55" Rows="4" meta:resourcekey="perosnalcmtarea" placeholder="Please enter comment" TextMode="MultiLine" MaxLength="200" Width="100%" />
                                                        </div>
                                                          <a href="javascript:void(0)" id="A3" class="btn btn-primary yellow" style="margin-top: 5px; width: 30%; float: left;" onclick="SaveCommentQuestion();">
                                                                <asp:Label runat="server" ID="Label22" meta:resourcekey="btnsavereso"></asp:Label></a>
                                                       <%-- <asp:Panel runat="server" ID="questionPanel">
                                                            <a href="Question.aspx" id="btnManageQuestion" class="btn btn-primary yellow" style="margin-top: 5px; width: 30%; float: left;display:none;">
                                                                <asp:Label runat="server" ID="lbl10" meta:resourcekey="btnManageque"></asp:Label></a>
                                                        </asp:Panel>--%>
                                                       

                                                       <%-- <asp:Button runat="server" ID="btnSaveComment" OnClick="btnSaveComment_Click" meta:resourcekey="btnsavereso"
                                                            Text="Save Comment" CssClass="btn btn-primary yellow HideControl"
                                                            type="button" ValidationGroup="chkdoc3"
                                                            Style="border-radius: 5px; width: 120px; height: 35px; float: right; margin-top: 1%;" />--%>

                                                       

                                                       <%--  <asp:Button runat="server" ID="btnSaveComment" OnClientClick="btnSaveCommentQuestion();" meta:resourcekey="btnsavereso"
                                                            Text="Save Comment" CssClass="btn btn-primary yellow HideControl"
                                                            type="button" ValidationGroup="chkdoc3"
                                                            Style="border-radius: 5px; width: 120px; height: 35px; float: right; margin-top: 1%;" />--%>

                                                        &nbsp;
                                                    <asp:Button runat="server" ID="btnQuesPDF" meta:resourcekey="btnpdf"
                                                        Text="PDF" CssClass="btn btn-primary yellow"
                                                        type="button" OnClientClick="SaveQuesDataPDF();" CausesValidation="false"
                                                        Style="border-radius: 5px; width: 50px; height: 35px; float: right; margin-top: 1%; margin-right: 1%;" />

                                                         <asp:Button runat="server" ID="Button5" Text="Add Activity" OnClick="reac"
                                                            CssClass="btn btn-primary yellow"
                                                            type="button"
                                                            Style="margin-top: 5px;float:right;"
                                                            meta:resourcekey="Add_Act_but"
                                                            Visible="true" />
                                                    </div>

                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <!--  <h3 class="custom-heading darkblue">Original competence level</h3>
				 <div class="visitor-stats widget-body blue">
					
					<div class="home_grap"><div class="graph-info">
								
								 <a href="#" id="bars"><span><i class="fa fa-bar-chart-o"></i></span></a>
								 <a href="#" id="lines" class="active"><span><i class="fa fa-code-fork"></i></span></a>
							 </div>
							 <div class="graph-container">
							
								 <div id="graph-lines"></div>
								 <div id="graph-bars"></div>
							 </div></div>
							 <div class="col-md-12" style="background:#f4f4f4; padding-left:0px !important;" >
							 <div class="col-md-1 profile-details" style="padding-left:0px !important;" >
							 
							 
							 </div>
					
					
				 </div>
					 -->
                    </div>
                </div>


                <div class="form-group col-md-12" style="text-align: right;">
                    <a href="Report.aspx" class="btn black pull-right" style="border-radius: 5px; margin-right: 15px; margin-top: 5px; float: right;">
                        <asp:Literal ID="Literal5" runat="server" meta:resourcekey="back" Text="back"></asp:Literal></a><a href="#" class="btn btn-primary yellow" id="btnsubmitdoc"
                            style="border-radius: 5px; margin-right: 15px; margin-top: 5px; float: right;"
                            onclick="SaveFullPagePDF();"
                            meta:resourcekey="btnsubmitResource1"><asp:Literal ID="Literal4" runat="server" meta:resourcekey="PDF" Text="PDF"></asp:Literal></a>&nbsp; 
                   <%-- <a href="#" class="btn btn-primary yellow" runat="server" id="save" style="border-radius: 5px; margin-right: 15px; margin-top: 5px; float: right; display: none;" onclick="fnSaveHtmlData(false);">
                        <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Save" Text="Save"></asp:Literal></a>&nbsp;--%>
                </div>
                <div style="clear: both">
                </div>
                <!-- TIME LINE -->
            </div>
        </div>

        <div class="col-md-3">
        </div>
    </div>
    <!-- Modal content -->


    <div>
        <div aria-hidden="true" role="dialog" tabindex="-1" class="modal fade col-md-12" id="myModal" style="display: none;">
            <div class="modal-dialog" style="width: 55%">
                <div class="modal-content">
                    <div class="modal-header blue yellow-radius">
                        <%-- <span class="close">&times;</span> --%>
                        <h4 style="color: white;">
                            <asp:Literal ID="lblCompanyDirModalHeader" runat="server" Text="Add Meeting" meta:resourcekey="AddMeeting"
                                EnableViewState="true" />

                        </h4>
                    </div>
                    <div class="modal-body">
                        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <div class="col-md-12">
                                    <div class="col-md-3" style="padding-top: 20px;">
                                        <label style="color: red; float: right">*</label>
                                        <label style="float: right">
                                            <asp:Label runat="server" ID="lbl11" meta:resourcekey="Dateres"></asp:Label></label>
                                    </div>
                                    <div class="col-md-4" style="padding-top: 10px;">
                                        <%--<asp:TextBox ID="txtdate" runat="server" placeholder="Date" ></asp:TextBox>--%>
                                        <asp:TextBox runat="server" CssClass="datepicker1"
                                            ID="txtdate"
                                            placeholder="Date" meta:resourcekey="EnterDate"
                                            data-mask="99/99/2099" AutoPostBack="False" Style="height: 42px;"
                                            onmouseover="SetDatePicker();">

                                        </asp:TextBox>
                                    </div>
                                    <div class="col-md-3" style="padding-top: 10px;">
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtdate"
                                            ErrorMessage="Please Select Date" CssClass="commonerrormsg" ValidationGroup="MeetingValidation"
                                            Display="Dynamic" meta:resourcekey="RequiredFieldValidator5Resource3">

                                        </asp:RequiredFieldValidator>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="col-md-3" style="padding-top: 10px;">
                                            <label style="color: red; float: right">*</label>
                                            <label style="float: right">
                                                <asp:Label runat="server" ID="lbl12" meta:resourcekey="DescriptionRes"></asp:Label></label>
                                        </div>
                                        <div class="col-md-5" style="padding-top: 10px;">
                                            <asp:TextBox runat="server" ID="txtdescription" placeholder="Description" TextMode="MultiLine" CssClass="ModalControlStyle" meta:resourcekey="EnterDescription" />
                                        </div>
                                        <div class="col-md-3" style="padding-top: 10px;">
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtdescription"
                                                ErrorMessage="Please Fill Description" CssClass="commonerrormsg" ValidationGroup="MeetingValidation"
                                                Display="Dynamic" meta:resourcekey="RequiredFieldValidator5Resource4">

                                            </asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="col-md-3" style="padding-top: 10px;">
                                            <label style="color: red; float: right">*</label>
                                            <label style="float: right">
                                                <asp:Label runat="server" ID="lbl13" meta:resourcekey="DescriptionRes"></asp:Label></label>
                                        </div>
                                        <div class="col-md-5" style="padding-top: 10px;">
                                            <asp:TextBox runat="server" ID="txtdescriptionDN" placeholder="Description(Denish)" TextMode="MultiLine" CssClass="ModalControlStyle" meta:resourcekey="Enterdescriptiondanish" />
                                        </div>
                                        <div class="col-md-3" style="padding-top: 10px;">
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtdescriptionDN"
                                                ErrorMessage="Please Fill Description" CssClass="commonerrormsg" ValidationGroup="MeetingValidation"
                                                Display="Dynamic" meta:resourcekey="RequiredFieldValidator5Resource4">

                                            </asp:RequiredFieldValidator>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-3" style="padding-top: 20px;">
                                        <%-- <label style="color:red;float:right">*</label> --%>
                                        <label style="float: right">
                                            <asp:Label runat="server" ID="lbl14" meta:resourcekey="Search"></asp:Label></label>
                                    </div>
                                    <div class="col-md-5" style="padding-top: 10px;">
                                        <asp:TextBox ID="txtsearch" runat="server" placeholder="Search Term" CssClass="ModalControlStyle" meta:resourcekey="Entersearchterm"></asp:TextBox>
                                    </div>
                                    <div class="col-md-2" style="padding-top: 10px; margin-left: 5%;">
                                        <asp:Button ID="btnsearch" runat="server" OnClick="btnsearch_Click" Text="Search" ForeColor="White" CssClass="btn btn-default black" CausesValidation="false" ValidationGroup="chkdoc6" meta:resourcekey="btnsearch" />

                                        <%--<asp:LinkButton ID="btnsearch" runat="server" OnClick="btnsearch_Click"  Class="btn btn-default yellow" CausesValidation="false" >Search</asp:LinkButton>--%>
                                    </div>

                                </div>



                                <div class="col-md-12">
                                    <div class="col-md-3" style="padding-top: 20px;">
                                        <%-- <label style="color:red;float:right">*</label> --%>
                                        <label style="float: right">
                                            <asp:Label runat="server" ID="lbl15" meta:resourcekey="Searchresult"></asp:Label></label>
                                    </div>
                                    <div class="col-md-5" style="padding-top: 10px;">
                                        <asp:ListBox ID="listresult" runat="server" Width="300px" SelectionMode="Multiple" CssClass="ModalControlStyle"></asp:ListBox>
                                    </div>
                                    <div class="col-md-2" style="padding-top: 10px; margin-left: 5%;">
                                        <asp:Button ID="btnadd" runat="server" Text="Add" OnClick="btnadd_Click" ValidationGroup="chkdoc5" CssClass="btn btn-default black" ForeColor="White" BackColor="Black" meta:resourcekey="btnAdd" />

                                        <%--<asp:LinkButton ID="btnsearch" runat="server" OnClick="btnsearch_Click"  Class="btn btn-default yellow" CausesValidation="false" >Search</asp:LinkButton>--%>
                                    </div>

                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-3" style="padding-top: 20px;">
                                        <%-- <label style="color:red;float:right">*</label> --%>
                                        <label style="color: red; float: right"></label>
                                        <label style="float: right">
                                            <asp:Label runat="server" ID="lbl16" meta:resourcekey="Participant"></asp:Label></label>
                                    </div>
                                    <div class="col-md-4" style="padding-top: 10px;">
                                        <asp:ListBox ID="listparticipants" runat="server" Width="300px" SelectionMode="Multiple"></asp:ListBox>
                                    </div>
                                    <div class="col-md-2" style="padding-top: 10px; margin-left: 13%">
                                        <asp:Button ID="btnremove" runat="server" Text="Remove" OnClick="btnremove_Click" ValidationGroup="chkdoc5" CssClass="btn btn-default black" ForeColor="White" BackColor="Black" meta:resourcekey="btnRemove" />
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="col-md-4" style="padding-top: 10px;">
                                </div>
                                <div class="col-md-2" style="padding-top: 10px;">
                                    <asp:Button ID="btnsave" runat="server" Text="Save" OnClick="btnsave_Click" ValidationGroup="MeetingValidation" CssClass="btn btn-primary yellow" CausesValidation="true" meta:resourcekey="btnSaveres" />
                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" OnClick="btnUpdate_Click" ValidationGroup="MeetingValidation" CssClass="btn btn-primary yellow" CausesValidation="true" Visible="false" meta:resourcekey="btnUpload" />
                                </div>

                                <div class="col-md-2" style="margin-top: 10px;">
                                    <asp:Button ID="Button4" runat="server" Text="Close" data-dismiss="modal" ForeColor="White" CssClass="btn btn-default black" meta:resourcekey="btnClose" />
                                </div>

                            </div>
                        </div>
                    </div>
                    <%--<div class="modal-footer">
      <h3>Modal Footer</h3>
    </div>--%>
                    <asp:HiddenField runat="server" ID="hdnQuesID" />
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->


    <%-- Meeting Document Modal --%>
    <div>
        <div aria-hidden="true" role="dialog" tabindex="-1" class="modal fade col-md-12" id="popupAddNewQuesCat" style="display: none;">
            <div class="modal-dialog" style="width: 55%">
                <div class="modal-content">
                    <div class="modal-header blue yellow-radius">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                            ×</button><h4 class="modal-title">
                                <asp:Label runat="server" ID="Label12" CssClass="" Style="color: white; font-size: 19px; margin-top: 4px;">
                                    <asp:Literal ID="lblQuesCatModalHeader" runat="server" Text="Add Meeting Document" meta:resourcekey="AddMeetingDoc"
                                        EnableViewState="true" />
                                </asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="col-md-3" style="padding-top: 10px;">
                                <asp:Label runat="server" ID="Label17" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                    <asp:Literal ID="Literal23" runat="server" Text="Document Name" EnableViewState="false" meta:resourcekey="Documentnm" />
                                </asp:Label>
                            </div>
                            <div class="col-md-9" style="margin-top: 10px;">
                                <asp:TextBox runat="server" ID="txtDocumentName" placeholder="Enter Document name" MaxLength="100" meta:resourcekey="Enterdocnm" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtDocumentName"
                                    ErrorMessage="Please enter Document name." CssClass="commonerrormsg" Display="Dynamic"
                                    ValidationGroup="chkdoc7" meta:resourcekey="RequiredFieldValidator5Resource2"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3" style="padding-top: 10px;">
                                <asp:Label runat="server" ID="Label18" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                    <asp:Literal ID="Literal24" runat="server" Text="File Upload" EnableViewState="false" meta:resourcekey="Fileupld" />
                                </asp:Label>
                            </div>
                            <div class="col-md-9" style="margin-top: 10px;">


                                <a class="btn btn-primary yellow" style="display: block; width: 120px; height: 30px;" onclick="document.getElementById('ContentPlaceHolder1_FileUpload1').click()">
                                    <asp:Literal ID="Literal21" runat="server" EnableViewState="false" meta:resourcekey="Browse" /> 
                                    
                                </a>
                                <asp:Label ID="Label23" runat="server" Text="No File Selected" meta:resourcekey="nofileselectedres"></asp:Label>
                                <div style="display: none;">
                                    <asp:FileUpload ID="FileUpload1" runat="server"  onchange="getFileData(this);"></asp:FileUpload>
                                </div>
                                <%-- <input type='file' id="getFile" style="display: none">--%>

                                <asp:RegularExpressionValidator ID="reFile1" runat="server" ControlToValidate="FileUpload1"
                                                    CssClass="legend" Display="Dynamic" ErrorMessage="Please upload only .pdf file."
                                                    ValidationExpression="^.*\.(PDF|pdf)$" ForeColor="Red" ValidationGroup="chkdoc"
                                                    meta:resourcekey="reFile1Resource1"/><br />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="FileUpload1"
                                    ErrorMessage="Please select file." CssClass="commonerrormsg" Display="Dynamic"
                                    ValidationGroup="chkdoc7" meta:resourcekey="RequiredFieldValidator5Resource1"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">

                        <asp:Button runat="server" ID="btnCreateCat"
                            Text="Create" CssClass="btn btn-primary yellow" OnClick="btnsubmitdoc_click" meta:resourcekey="btnCreate"
                            type="button" OnClientClick="CheckValidations('chkdoc7');closeModelCancel('popupAddNewQuesCat');"
                            Style="border-radius: 5px; width: 100px; height: 38px; margin-left: 359px;" />
                        <%--  <asp:Button runat="server" 
                           CssClass="btn btn-primary yellow"
                             OnClick="btnCreateCat_click" Text="Create Category"
                            Style="border-radius: 5px; width: 150px; height: 38px; margin-left: 359px;" />--%>
                        <asp:Button runat="server" ID="Button9" data-dismiss="modal"
                            Text="Close" CssClass="btn btn-default black" meta:resourcekey="btnClose"
                            type="button" ValidationGroup="chkdoc1" OnClientClick="closeModelCancel('popupAddNewQuesCat');"
                            Style="border-radius: 5px; width: 100px; height: 38px;" />


                    </div>
                </div>
            </div>
        </div>
    </div>
    <%-- Meeting Document Modal End --%>


    <asp:HiddenField ID="hdnSelectCat" runat="server" />
    <asp:Label runat="server" ID="hdnActivityVal" Text="1" CssClass="hdnActivityVal" Style="display: none;" />
    <asp:Label runat="server" ID="hdnCompetenceEnableVal" Text="1" CssClass="hdnCompetenceEnableVal" Style="display: none;" />
    <asp:Label runat="server" ID="hdnPointEnableVal" Text="1" CssClass="hdnPointEnableVal" Style="display: none;" />
    <asp:Label runat="server" ID="hdnQuestionEnableVal" Text="1" CssClass="hdnQuestionEnableVal" Style="display: none;" />

    <%--    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js" type="text/javascript"></script>--%>
    <script src="../Scripts/Scripts/exporting.js" type="text/javascript"></script>


    <script type="text/javascript" src="https://code.jquery.com/jquery.min.js"></script>

    <script src="../Scripts/chartCat.js" type="text/javascript"></script>
    <script src="../Scripts/flotJs.js" type="text/javascript"></script>
    <script src="../Scripts/chartCat.js" type="text/javascript"></script>

    <script type="text/javascript" src="../Scripts/ckeditor/ckeditor.js"> </script>
    <script src="../Scripts/pickadate.js-3.5.6/lib/legacy.js" type="text/javascript"></script>
    <script src="../Scripts/pickadate.js-3.5.6/lib/picker.js" type="text/javascript"></script>
    <script src="../Scripts/pickadate.js-3.5.6/lib/picker.date.js" type="text/javascript"></script>
    <script src="../Scripts/pickadate.js-3.5.6/lib/picker.time.js" type="text/javascript"></script>

    <script type="text/javascript">
        var pointTabImage;
        var pointTopTabImage;
        var introManagementTabImage1;
        var introManagementTabImage2;
        var introManagementTabImage3;
        var introManagementTabImage4;

        var competenceTabImage;
        var activityTabImage;

        var ComDirImage;

        function clearMeetingData() {
            $('.datepicker1').val('');
            $('#ContentPlaceHolder1_txtdescription').val('');
            $('#ContentPlaceHolder1_txtdescriptionDN').val('');
            $('#ContentPlaceHolder1_txtsearch').val('');
            $('#ContentPlaceHolder1_listresult').empty();
            $('#ContentPlaceHolder1_listparticipants').empty();

        }
    </script>
    <script type="text/javascript">
        function LoadChartFirstTime() {
           
            setTimeout(function () {
                DisplayTab(2);
                //setTimeout(function () {
                //    DisplayTab(3);
                //    setTimeout(function () {
                // DisplayTab(1);
                //  GetPersonalComment('0');
                //    }, 1000);
                //}, 500);
                setTimeout(function () {
                    //  alert('hi');
                    DisplayTab(1);
                    //  alert($('#ContentPlaceHolder1_hdnIsDisplayInReport').val());
                    if ($('#ContentPlaceHolder1_hdnIsDisplayInReport').val() == "true" || $('#ContentPlaceHolder1_hdnIsDisplayInReport').val() == true || $('#ContentPlaceHolder1_hdnIsDisplayInReport').val() == "True" || $('#ContentPlaceHolder1_hdnIsDisplayInReport').val() == "0") {
                        $('#btnPrivateComment').css("display", "block");
                        $('#btnPublicComment').css("display", "none");
                    }
                    else {

                        $('#btnPublicComment').css("display", "block");
                        $('#btnPrivateComment').css("display", "none");
                    }
                    if ($('#ContentPlaceHolder1_hdnQueryUmID').val() != $('#ContentPlaceHolder1_hdnLoginUserID').val()) {
                        //$('#btnPublicComment').css("display", "none");
                        //$('#btnPrivateComment').css("display", "none");
                        //  $('#ContentPlaceHolder1_btnSaveComment').css('display', 'none');
                    }
                        // else if (($('#ContentPlaceHolder1_hdnIsPDFPersonalComment').val() == "1") && $('#ContentPlaceHolder1_hdnQueryUmID').val() == $('#ContentPlaceHolder1_hdnLoginUserID').val()) {
                    else if ($('#ContentPlaceHolder1_hdnQueryUmID').val() == $('#ContentPlaceHolder1_hdnLoginUserID').val()) {

                        //$('#btnPublicComment').css("display", "block");
                        //$('#btnPrivateComment').css("display", "none");
                        //alert($('#ContentPlaceHolder1_hdnIsPDFPersonalComment').val());
                        //alert($('#ContentPlaceHolder1_hdnIsDisplayInReport').val());

                        //swati
                        //  $('#ContentPlaceHolder1_btnSaveComment').css('display', 'block');



                        //if ($('#ContentPlaceHolder1_hdnIsDisplayInReport').val() == "true" || $('#ContentPlaceHolder1_hdnIsDisplayInReport').val() == true || $('#ContentPlaceHolder1_hdnIsDisplayInReport').val()=="True") {
                        //    $('#btnPrivateComment').css("display", "block");
                        //    $('#btnPublicComment').css("display", "none");
                        //}
                        //else {

                        //    $('#btnPublicComment').css("display", "block");
                        //    $('#btnPrivateComment').css("display", "none");
                        //}


                    }
                    else {
                        //alert('settimeout');
                        //alert($('#ContentPlaceHolder1_hdnIsPDFPersonalComment').val());
                        //  $('#ContentPlaceHolder1_btnSaveComment').css('display', 'none');

                        //$('#btnPublicComment').css("display", "none"); //swati

                        //if ($('#ContentPlaceHolder1_hdnIsDisplayInReport').val() == "true" || $('#ContentPlaceHolder1_hdnIsDisplayInReport').val() == true) {
                        //    $('#btnPrivateComment').css("display", "block");
                        //}
                        //else {
                        //    $('#btnPublicComment').css("display", "block");
                        //    $('#btnPrivateComment').css("display", "none");
                        //}
                    }

                    if ($('#ContentPlaceHolder1_hdnUserType').val() != "3") {
                        $('#btnManageQuestion').css("display", "block");
                    }
                    else {
                        $('#btnManageQuestion').css("display", "none");
                    }



                }, 1500);
            }, 2000);
        }
        </script>
    <script type="text/javascript">
        var icons = new Skycons();
        icons.set("rain", Skycons.RAIN);
        icons.play();


        function ChangeColor(a) {
            $("#" + a).parent().css('background', 'ActiveBorder');
        }


        function imger() {
            $(".userImgClass").attr('src', "/starteku/organisation/images/profile_img.png");
        }



        $(document).ready(function () {
            
            //$('form').submit(function (ev) {
            //    if (ev.keyCode == 13) {
            //        // Prevent form submission behaviors if the event was fired by enter keypress
            //        ev.preventDefault(); return false;
            //    }
            //    // And code for form submission here, or just keep the return true to make it behave normally.
            //    return true;
            //});
            setTimeout(function () {

                if ($('#t4').is(":visible") == false) {
                    $(".tabsitem").css('width', '25% !important');
                    $(".tabs-menu li").css("width", "25% !important");
                }
                else {
                    $(".tabsitem").css('width', '20% !important');
                    $(".tabs-menu li").css("width", "20% !important");
                }
            }, 1200);

            if ($("#ContentPlaceHolder1_ddActCate :selected").val() != "0") {
                $("#ContentPlaceHolder1_save").hide();
            } else {
                $("#ContentPlaceHolder1_save").show();
            }
            $(".logo").css("float", "left");
            //alert();
            GetPDPComments();
            // GetQuestionComment();

            setTimeout(function () {
                //  $("#ContentPlaceHolder1_btnManap").hide();
                //   $("#ContentPlaceHolder1_btnEmpap").hide();

                GetCommentImgtab1();


                //swati

            }, 1000);
            setTimeout(function () {
                setTimeout(function () {
                    if ($('#ContentPlaceHolder1_hdnIsDisplayInReport').val() == "true" || $('#ContentPlaceHolder1_hdnIsDisplayInReport').val() == true || $('#ContentPlaceHolder1_hdnIsDisplayInReport').val() == "True" || $('#ContentPlaceHolder1_hdnIsDisplayInReport').val()=="0") {
                        $('#btnPrivateComment').css("display", "block");
                        $('#btnPublicComment').css("display", "none");
                    }
                    else {

                        $('#btnPublicComment').css("display", "block");
                        $('#btnPrivateComment').css("display", "none");
                    }
                    if ($('#ContentPlaceHolder1_hdnQueryUmID').val() != $('#ContentPlaceHolder1_hdnLoginUserID').val()) {
                      
                    }
                    else if ($('#ContentPlaceHolder1_hdnQueryUmID').val() == $('#ContentPlaceHolder1_hdnLoginUserID').val()) {
                    }
                    else {
                       
                    }

                    if ($('#ContentPlaceHolder1_hdnUserType').val() != "3") {
                        $('#btnManageQuestion').css("display", "block");
                    }
                    else {
                        $('#btnManageQuestion').css("display", "none");
                    }

                }, 1500);
            }, 2000);



            //   $("#ContentPlaceHolder1_btnManap").show();
            //   $("#ContentPlaceHolder1_btnEmpap").show();
            //SetExpandCollapse();
            setTimeout(function () {

                var subtext = $('#<%=GraphSubTitle.ClientID %>').val();
                var AxisText = $('#<%=GraphAxisLevel.ClientID %>').val();
                var ToolTipText = $('#<%=GraphToolTip.ClientID %>').val();

                if (subtext == "") {
                    subtext = "Click and drag mouse arrow to zoom chart";
                }
                //if (text == "") {   //Commented on 4-1-18
                //    text = $("#ddLabel").text();
                //    if (text == "") {
                //        text = "Original Competence Level";
                //    }
                //}
                if (AxisText == "") {

                    text = "Level";
                }
                if (ToolTipText == "") {

                    ToolTipText = "Level";
                }

                //  $(".highcharts-title").text(text); //Commented on 4-1-18
                $(".highcharts-subtitle").text(subtext);
                $(".highcharts-yaxis-title").text(AxisText);
                $(".spanText").text(ToolTipText);
            }, 3000);


        });


        var totalLevel;
        var dataLP;
        function SubmitBtn1() {

            GetPageName();
            var ReturnDataLP;
            $.ajax({
                type: "POST",
                cache: false,
                async: true,
                url: "ReportALL.aspx/BtnSubmitAJAXLP",
                data: '{txtName:"' + '' + '",txtDesc:"' + '' + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    dataLP = response.d;

                    Developmentcomnames1();
                    setTimeout(function () {


                        //$("#waitLearninipoint").fadeOut();

                    }, 500);


                },
                failure: function (msg) {

                    alert(msg);
                    ReturnDataLP = msg;
                }
            });

        }

        function Developmentcomnames1() {

            var ReturnDataLP;
            $.ajax({
                type: "POST",
                cache: false,
                async: true,
                url: "ReportALL.aspx/DevelopmentcomnamesLP",
                data: '{txtName:"' + '' + '",txtDesc:"' + '' + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    totalLevel = response.d;
                    return totalLevel;

                },
                failure: function (msg) {

                    alert(msg);
                    ReturnDataLP = msg;
                }
            });

        }
        function GetQuestionComment() {

            //javascript e hdn file value lavani
            var umId = 0;
            var urlParms = new URLSearchParams(window.location.search); //specfic text
            umId = urlParms.get('umId');
            //  alert(umId);
            // $("#scrollbox5").scrollTop(99999999999999999999);
            $("#quesChatArea").scrollTop(99999999999999999999);
            //var comid = document.getElementById("hdncom").value;

            //  $("#scrollbox5").html('');
            // $("#txtCmtQuestion").html('');
            htmlq = "";
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "PersonalDevelopmentPlan.aspx/GetQuestionCommentList",
                data: '{ "umId":"' + umId + '"}',
                dataType: "json",
                async: true,
                success: function (data) {

                    for (var i = 0; i < data.d.length; i++) {
                        debugger;
                        var cls = "reply";
                        var onerror = "this.onerror=null;this.src='/starteku/organisation/images/profile_img.png'"
                        htmlq += "<li class='" + cls + "'><div class='chat-thumb'><img src='../Log/upload/Userimage/" + data.d[i].fromuserImage + "' onerror=" + onerror + " alt=''/></div>";
                        htmlq += "<div class='chat-desc'><p>" + data.d[i].Comment + "</p><i class='chat-time'>" + parseJsonDate(data.d[i].CreateDate) + "</i>";
                        htmlq += "</div></li>";
                    }
                    // alert(data.d.length );
                    if (data.d.length > 0) {
                        $("#txtCmtQuestion").html('');
                        $("#txtCmtQuestion").append(htmlq);

                        //  htmlq += "No records found.";
                        // htmlq += "<li class='reply'><asp:Label runat='server' meta:resourcekey='Notrecordfound'></asp:Label></li>";
                    }

                    //  $("#scrollbox5").append(html);


                    //  $("#scrollbox5").scrollTop(99999999999999999999);
                    $("#quesChatArea").scrollTop(99999999999999999999);
                },
                error: function (result) {
                }
            });
        }
        function GetPersonalComment(a) {
            var b = $("#ContentPlaceHolder1_hdnqpercomID").val();
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "PersonalDevelopmentPlan.aspx/GetQuestionPersonalCommentData",
                data: '{"Index":"' + a + '","umId":"' + umId + '","hdnqpercomID":"' + b + '"}',

                dataType: "json",
                success: function (data) {
                    // alert($('#ContentPlaceHolder1_hdnIsPDFPersonalComment').val());
                    $("#ContentPlaceHolder1_hdnqpercomID").val(data.d.qpercomID);
                    $("#ContentPlaceHolder1_txtPersonalComment").val(data.d.PersonalComment);

                    if (data.d.btnSaveComment == 1) { //swati
                        $('#ContentPlaceHolder1_hdnIsPDFPersonalComment').val(data.d.IsPDFPersonalComment);
                        $('#ContentPlaceHolder1_hdnIsDisplayInReport').val(data.d.IsDisplayInReportComment);
                        // $('#ContentPlaceHolder1_btnSaveComment').css('display', 'block');
                    }
                    else {
                        //   $('#ContentPlaceHolder1_btnSaveComment').css('display', 'none');
                    }


                    // alert($('#ContentPlaceHolder1_hdnIsPDFPersonalComment').val());
                },
                error: function (result) {
                }
            });
        }
        function SetPersonalCommentPublic(a) {
            if (a == 1) {
                $('#ContentPlaceHolder1_hdnIsPDFPersonalComment').val('0');
                $('#ContentPlaceHolder1_hdnIsDisplayInReport').val('False');
                $('#btnPublicComment').css("display", "none");
                $('#btnPrivateComment').css("display", "block");
            } else {
                $('#ContentPlaceHolder1_hdnIsPDFPersonalComment').val('1');
                $('#ContentPlaceHolder1_hdnIsDisplayInReport').val('True');
                $('#btnPublicComment').css("display", "block");
                $('#btnPrivateComment').css("display", "none");

            }
            var b = $("#ContentPlaceHolder1_hdnqpercomID").val();
            var umidd = $("#ContentPlaceHolder1_hdnQueryUmID").val();
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "PersonalDevelopmentPlan.aspx/SetQuestionPersonalCommentDataPublic",
                data: '{"hdnqpercomID":"' + b + '","umID":"' + umidd + '","PrivatePublic":"' + a + '"}',
                dataType: "json",
                async: false,
                success: function (data) {
                    if (a == 1) {
                        $('#ContentPlaceHolder1_hdnIsPDFPersonalComment').val('0');
                        $('#ContentPlaceHolder1_hdnIsDisplayInReport').val('False');
                        $('#btnPublicComment').css("display", "none");
                        $('#btnPrivateComment').css("display", "block");
                    } else {
                        $('#ContentPlaceHolder1_hdnIsPDFPersonalComment').val('1');
                        $('#ContentPlaceHolder1_hdnIsDisplayInReport').val('True');
                        $('#btnPublicComment').css("display", "block");
                        $('#btnPrivateComment').css("display", "none");

                    }

                },
                error: function (result) {
                    if (a == 1) {
                        $('#ContentPlaceHolder1_hdnIsPDFPersonalComment').val('0');
                        $('#ContentPlaceHolder1_hdnIsDisplayInReport').val('False');
                        $('#btnPublicComment').css("display", "none");
                        $('#btnPrivateComment').css("display", "block");
                    } else {
                        $('#ContentPlaceHolder1_hdnIsPDFPersonalComment').val('1');
                        $('#ContentPlaceHolder1_hdnIsDisplayInReport').val('True');
                        $('#btnPublicComment').css("display", "block");
                        $('#btnPrivateComment').css("display", "none");

                    }
                }
            });
        }
        function insertcomment(aId) {
            var message = $(".txtcomment").val();


            if (message != '') {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "PersonalDevelopmentPlan.aspx/insertmessage",
                    data: '{"message":"' + message + '","umId":"' + umId + '"}',

                    dataType: "json",
                    success: function (data) {
                        var obj = data.d;
                        if (obj == 'true') {

                            $(".txtcomment").val("");
                            GetPDPComments();


                            setTimeout(function () {
                                $("#scrollbox6").scrollTop(99999999999999999999);
                            }, 122);
                            $("#" + aId).prev().prev().text(message); // copy text and paste on parent page's comment box
                        }
                    },
                    error: function (result) {
                        // alert("Error");
                    }
                });
            }
            else {
                generate("warning", "Oops! You have missed text to send a massage.", "bottomCenter");
                return false;
            }
        }

        function insertcommentQuestion(aId) {
            var message = $(".txtcommentQuestion").val();

           // alert(message);
            var umId = 0;
            var urlParms = new URLSearchParams(window.location.search); //specfic text
            umId = urlParms.get('umId');
            // alert(message);
            if (message != '') {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "PersonalDevelopmentPlan.aspx/insertmessageQuestion",
                    data: '{"message":"' + message + '","umId":"' + umId + '"}',

                    dataType: "json",
                    success: function (data) {
                        // alert(data.d);
                        var obj = data.d;

                      //  alert(data.d);
                        if (obj == 'true') {

                            $(".txtcommentQuestion").val("");
                            GetQuestionComment();
                            setTimeout(function () {
                                $("#quesChatArea").scrollTop(99999999999999999999);
                            }, 122);
                            $("#" + aId).prev().prev().text(message); // copy text and paste on parent page's comment box
                        }
                    },
                    error: function (result) {
                        // alert("Error");
                    }
                });
            }
            else {
                generate("warning", "Oops! You have missed text to send a massage.", "bottomCenter");
                return false;
            }
        }

        function SaveCommentQuestion() {
            var message = $("#ContentPlaceHolder1_txtPersonalComment").val();

            
            var umId = 0;
            var urlParms = new URLSearchParams(window.location.search); //specfic text
            umId = urlParms.get('umId');
            // alert(message);
            if (message != '') {
                debugger;
              //  alert(message);
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "PersonalDevelopmentPlan.aspx/btnSaveCommentQuestion",
                    data: '{"PersonalComment":"' + message + '","umId":"' + umId + '"}',

                    dataType: "json",
                    success: function (data) {
                        // alert(data.d);
                        var obj = data.d;
                        $("#ContentPlaceHolder1_hdnqpercomID").val(obj);
                    },
                    error: function (result) {
                       // alert(result.responseText);
                    }
                });
            }
            else {
                generate("warning", "Oops! You have missed text to send a massage.", "bottomCenter");
                return false;
            }
        }

        $(function () {
            $("#ddlFruits").change(function () {
                var selectedText = $(this).find("option:selected").text();
                var selectedValue = $(this).val();
                alert("Selected Text: " + selectedText + " Value: " + selectedValue);
            });
        });




        var html = "";
        function GetPDPComments() {

            //javascript e hdn file value lavani
            var umId = 0;
            var urlParms = new URLSearchParams(window.location.search); //specfic text
            umId = urlParms.get('umId');

            // $("#scrollbox6").scrollTop(99999999999999999999);
            $("#txtCmt").scrollTop(99999999999999999999);

            //var comid = document.getElementById("hdncom").value;
            htmlp = "";
            //  $("#scrollbox6").html('');
            $("#txtCmt").html('');

            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "PersonalDevelopmentPlan.aspx/GetPersonal",
                data: '{ "umId":"' + umId + '"}',
                dataType: "json",
                async: true,
                success: function (data) {

                    //$("#hdncom").val(a);

                    //cat = data.d;

                    for (var i = 0; i < data.d.length; i++) {

                        var cls = "reply";
                        var onerror = "this.onerror=null;this.src='/starteku/organisation/images/profile_img.png'"
                        htmlp += "<li class='" + cls + "'><div class='chat-thumb'><img src='../Log/upload/Userimage/" + data.d[i].userImage + "' onerror=" + onerror + " alt=''/></div>";
                        htmlp += "<div class='chat-desc'><p>" + data.d[i].PerComment + "</p><i class='chat-time'>" + parseJsonDate(data.d[i].PerComCreateDate) + "</i>";
                        htmlp += "</div></li>";
                    }

                    if (data.d.length == 0) {
                        htmlp += "No records found.";
                    }


                    // $("#scrollbox6").append(htmlp);
                    $("#txtCmt").append(htmlp);



                    // $("#scrollbox6").scrollTop(99999999999999999999);
                    $("#txtCmt").scrollTop(99999999999999999999);
                },



                error: function (result) {
                    //alert("Error");

                }
            });
        }



        function parseJsonDate(jsonDateString) {


            //return new Date(parseInt(jsonDateString.replace('/Date(', '')));

            var formattedDate = new Date(parseInt(jsonDateString.substr(6)));
            var d = formattedDate.getDate();
            var m = formattedDate.getMonth();
            m += 1; // JavaScript months are 0-11
            var y = formattedDate.getFullYear();
            var min = formattedDate.getMinutes();
            var hour = formattedDate.getHours();
            var sec = formattedDate.getSeconds();

            //$("#txtDate").val(d + "." + m + "." + y);
            return d + "/" + m + "/" + y + " " + hour + ":" + min + ":" + sec;
        }

        function formatDate(d) {
            if (hasTime(d)) {
                var s = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
                s += ' ' + d.getHours() + ':' + zeroFill(d.getMinutes()) + ':' + zeroFill(d.getSeconds());
            } else {
                var s = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
            }

            return s;
        }










    </script>
    <script type="text/javascript">
        // Get the modal
        var modal = document.getElementById('myModal');

        // Get the button that opens the modal
        var btn = document.getElementById("myBtn");


        // Get the <span> element that closes the modal
        //var span = document.getElementsByClassName("close")[0];

        $('.close').click(function () {
            $('#myModal').hide();
        });

        // When the user clicks the button, open the modal 
        //btn.onclick = function () {
        //    modal.style.display = "block";
        //    clearMeetingData();
        //}

        // When the user clicks on <span> (x), close the modal
        //span.onclick = function () {
        //    modal.style.display = "none";
        //}

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function (event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    </script>
    <script>
        function openModal() {
            $('#myModal').show();
        }
    </script>
    <script type="text/javascript">
        function fnSaveHtmlData(shouldCallReload) {

            var umId = 0;
            var urlParms = new URLSearchParams(window.location.search); //specfic text
            umId = urlParms.get('umId');

            $(".btnEmpap").hide();
            $(".btnManap").hide();

            var dData = $('#ContentPlaceHolder1_htmlData').html().trim();
            var dataToSend = JSON.stringify($('#ContentPlaceHolder1_htmlData').html().trim());
            //  var dData = escape($("#ContentPlaceHolder1_htmlData").html());

            $.ajax({
                type: "POST",
                cache: false,
                async: true,
                url: "PersonalDevelopmentPlan.aspx/SaveHtmlData",
                data: '{"data":' + JSON.stringify($('#ContentPlaceHolder1_htmlData').html().trim()) + ',"umId":"' + umId + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    if (shouldCallReload == true) {
                        // window.location.reload();
                        $(location).attr('href', 'PersonalDevelopmentPlan.aspx?umId=' + umId + "&sel=" + "true")
                        //window.open('PersonalDevelopmentPlan.aspx?umId=' + umId + "&sel=" + "true");
                    }
                    // window.open('PersonalDevelopmentPlan.aspx?umId=' + umId);

                },
                failure: function (msg) {
                    //alert('failure');
                }
            });
        }

        function fnSaveHtmlData_PDPHistoryUpdate(PDPHistID) {
            // alert('fnSaveHtmlData_PDPHistoryUpdate' + PDPHistID);
            var umId = 0;
            var urlParms = new URLSearchParams(window.location.search); //specfic text
            umId = urlParms.get('umId');
            var histId = $("#ContentPlaceHolder1_ddActCate").val(); 
            $("#ContentPlaceHolder1_btnEmpap").hide();
            $("#ContentPlaceHolder1_btnManap").hide();

            //var dData = $('#ContentPlaceHolder1_htmlData').html().trim();
            //var dataToSend = JSON.stringify($('#ContentPlaceHolder1_htmlData').html().trim());
            //  var dData = escape($("#ContentPlaceHolder1_htmlData").html());

            $.ajax({
                type: "POST",
                cache: false,
                async: true,
                url: "PersonalDevelopmentPlan.aspx/GetAproveDetailsemp_Detail",
                data: "{'PDPHistID':'" + PDPHistID + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    dataLP = response.d;
                    //alert(dataLP.highAuthDetails);
                    //  alert(dataLP.lowerAuthDetails);
                    $("#ContentPlaceHolder1_lblHigherAuthName").text(dataLP.highAuthDetails);
                    $("#ContentPlaceHolder1_lblLowerAuthName").text(dataLP.lowerAuthDetails);
                    $("#ContentPlaceHolder1_lowdate").text(dataLP.empaptime);

                    //var umId = 0;
                    //var urlParms = new URLSearchParams(window.location.search); //specfic text
                    //umId = urlParms.get('umId');

                    $(".btnEmpap").hide();
                    $(".btnManap").hide();


                }
            });
        }

        function SaveAgainHtmlData(PDPHistID) {
            // alert('SaveAgainHtmlData');
            var dData = $('#ContentPlaceHolder1_htmlData').html().trim();
            var dataToSend = JSON.stringify($('#ContentPlaceHolder1_htmlData').html().trim());
            //  var dData = escape($("#ContentPlaceHolder1_htmlData").html());

            $.ajax({
                type: "POST",
                async: false,
                url: "PersonalDevelopmentPlan.aspx/SaveHtmlDataUpdate",
                data: '{"data":' + JSON.stringify($('#ContentPlaceHolder1_htmlData').html().trim()) + ',"PDPHistID":"' + PDPHistID + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    //if (shouldCallReload == true) {
                    //    // window.location.reload();
                    //    $(location).attr('href', 'PersonalDevelopmentPlan.aspx?umId=' + umId + "&sel=" + "true")
                    //    //window.open('PersonalDevelopmentPlan.aspx?umId=' + umId + "&sel=" + "true");
                    //}
                    // window.open('PersonalDevelopmentPlan.aspx?umId=' + umId);

                },
                failure: function (msg) {
                    //alert('failure');
                },
                complete: function () {
                    // e.preventDefault();
                    //alert('SaveHtmlDataUpdate complete');
                    //window.open('PersonalDevelopmentPlan.aspx?umId=' + umId, "_self");

                }
            });
        }
        function fnSaveHtmlData1() {

            var umId = 0;
            var urlParms = new URLSearchParams(window.location.search); //specfic text
            umId = urlParms.get('umId');
            var histId = $("#ContentPlaceHolder1_ddActCate").val();
            $("#ContentPlaceHolder1_btnEmpap").hide();
            $("#ContentPlaceHolder1_btnManap").hide();

            var dData = $('#ContentPlaceHolder1_htmlData').html().trim();
            var dataToSend = JSON.stringify($('#ContentPlaceHolder1_htmlData').html().trim());
            //  var dData = escape($("#ContentPlaceHolder1_htmlData").html());

            $.ajax({
                type: "POST",
                cache: false,
                async: true,
                url: "PersonalDevelopmentPlan.aspx/GetAproveDetailsemp",
                data: "{'umId':'" + umId + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    dataLP = response.d;
                    //alert(dataLP.highAuthDetails);
                    $("#ContentPlaceHolder1_lblHigherAuthName").text(dataLP.highAuthDetails);
                    $("#ContentPlaceHolder1_lblLowerAuthName").text(dataLP.lowerAuthDetails);
                    $("#ContentPlaceHolder1_lowdate").text(dataLP.empaptime);


                    debugger;

                    fnSaveHtmlData(true);

                },
                failure: function (msg) {
                    //alert('failure');
                }
            });

        }

    </script>






    <script type="text/javascript">

        var testTrue;
        function fnApprove(shouldCallSave) {
            testTrue = shouldCallSave;
            // return false;

            //alert('fnApprove');

            var dData = $('#ContentPlaceHolder1_htmlData').html().trim();
            var dataToSend = JSON.stringify(dData);
            var umId = 0;
            var urlParms = new URLSearchParams(window.location.search); //specfic text
            umId = urlParms.get('umId');
            debugger;
            //setTimeout(function () {
            $.ajax({
                type: "POST",
                async: false,
                url: "PersonalDevelopmentPlan.aspx/Approve",
                data: '{ strdata:' + dataToSend + ',umId:' + umId + '}',
                // data: "{strdata:'" + dataToSend + "',umId:'" + umId + "'}",
                // data: '{ data:' + dataToSend + ',umId:' + umId + '}',
                //data: "{'data':'" + dataToSend +','umId':'11'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    //window.confirm("Are sure Your want to approve?");
                    //alert(shouldCallSave);

                    debugger;
                    //  alert(response.d);
                    $('#ContentPlaceHolder1_hdnPDPHistoryID').val(response.d);
                    if (shouldCallSave == true) {

                        //  fnSaveHtmlData1();

                    }
                    // $('#ContentPlaceHolder1_btnManap').hide();
                    //window.location.reload();

                    if (shouldCallSave == false) {
                        // e.preventDefault();
                        //window.location.reload();
                        //$("#ContentPlaceHolder1_btnManap").hide();
                    }

                },
                failure: function (msg) {
                    //alert('failure');
                    alert(msg);
                },
                complete: function () {
                    //  alert('complete');
                    //SaveAgainHtmlData($('#ContentPlaceHolder1_hdnPDPHistoryID').val());
                    // e.preventDefault();
                    // alert('approve complete');
                    // window.location.reload(true);
                    //window.open('PersonalDevelopmentPlan.aspx?umId=' + umId, "_self");
                }
            });

            //}, 1000);
            //return false;
        }

    </script>
    <script src=" https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"
        type="text/javascript"></script>

    <script type="text/javascript">

        var umId = 0;
        $(document).ready(function () {

            var urlParms = new URLSearchParams(window.location.search);


            umId = urlParms.get('umId');
            setTimeout(function () {
                DrawChartAlpPLpPDP(umId);
                checkIsActivityAllow();

                ChangeDropdownSelectionAfteApproveByEmp();
            }, 1000);

            dateget_Notification();
        });

        function checkIsActivityAllow() {

            var actVal = $(".hdnActivityVal").text();
            if (actVal == "0") {
                $("#tab-4").hide();
                $("#t4").hide();
            }

            var compVal = $(".hdnCompetenceEnableVal").text();
            if (compVal == "0") {
                $("#tab-3").hide();
                $("#t3").hide();
            }

            var pointVal = $(".hdnPointEnableVal").text();
            if (pointVal == "0") {
                $("#tab-2").hide();
                $("#t2").hide();
            }
            var quesVal = $(".hdnQuestionEnableVal").text();
            if (quesVal == "0") {
                $("#tab-6").hide();
                $("#t6").hide();
            }

            if (quesVal != "0") {
                if (actVal == "0" && compVal == "0" && pointVal == "0") {
                    $(".tabsitem").css('width', '50%');
                    $("#tabitems_my ul li").css("width", "50% !important");
                }
                else if (actVal != "0" && compVal == "0" && pointVal == "0") {
                    $(".tabsitem").css('width', '33.33%');
                    $("#tabitems_my ul li").css("width", "33.33% !important");
                }
                else if (actVal != "0" && compVal != "0" && pointVal == "0") {
                    $(".tabsitem").css('width', '25%');
                    $("#tabitems_my ul li").css("width", "25% !important");
                }
                else if (actVal != "0" && compVal != "0" && pointVal != "0") {
                    $(".tabsitem").css('width', '20%');
                    $("#tabitems_my ul li").css("width", "20% !important");
                }
                else if (actVal == "0" && compVal == "0" && pointVal != "0") {
                    $(".tabsitem").css('width', '33.33%');
                    $("#tabitems_my ul li").css("width", "33.33% !important");
                }
                else if (actVal == "0" && compVal != "0" && pointVal == "0") {
                    $(".tabsitem").css('width', '33.33%');
                    $("#tabitems_my ul li").css("width", "33.33% !important");
                }
                else if (actVal == "0" && compVal != "0" && pointVal != "0") {
                    $(".tabsitem").css('width', '25%');
                    $("#tabitems_my ul li").css("width", "25% !important");
                }
            }
            else {
                //quesVal == "0"
                if (actVal == "0" && compVal == "0" && pointVal == "0") {
                    $(".tabsitem").css('width', '100%');
                    $("#tabitems_my ul li").css("width", "100% !important");
                }
                else if (actVal != "0" && compVal == "0" && pointVal == "0") {
                    $(".tabsitem").css('width', '50%');
                    $("#tabitems_my ul li").css("width", "50% !important");
                }
                else if (actVal != "0" && compVal != "0" && pointVal == "0") {
                    $(".tabsitem").css('width', '33.33%');
                    $("#tabitems_my ul li").css("width", "33.33% !important");
                }
                else if (actVal != "0" && compVal != "0" && pointVal != "0") {
                    $(".tabsitem").css('width', '25%');
                    $("#tabitems_my ul li").css("width", "25% !important");
                }
                else if (actVal == "0" && compVal == "0" && pointVal != "0") {
                    $(".tabsitem").css('width', '50%');
                    $("#tabitems_my ul li").css("width", "50% !important");
                }
                else if (actVal == "0" && compVal != "0" && pointVal == "0") {
                    $(".tabsitem").css('width', '50%');
                    $("#tabitems_my ul li").css("width", "50% !important");
                }
                else if (actVal == "0" && compVal != "0" && pointVal != "0") {
                    $(".tabsitem").css('width', '33.33%');
                    $("#tabitems_my ul li").css("width", "33.33% !important");
                }
            }
        }

        function DisplayTab(a) {
            //  alert(a);
            $('#hdnQuestionID').val('0');
            if (a == 1) {
                $("#tab-1").show();
                $(".maintab li").removeClass("current");
                $("#t1").addClass("current");
                $("#tab-2").hide();
                $("#tab-3").hide();
                $("#tab-4").hide();
                $("#tab-5").hide();
                $("#tab-6").hide();
            }
            else if (a == 2) {
                $("#tab-1").hide();
                $("#tab-2").show();
                $(".maintab li").removeClass("current");
                $("#t2").addClass("current");
                $("#tab-3").hide();
                $("#tab-4").hide();
                $("#tab-5").hide();
                $("#tab-6").hide();
                setTimeout(function () { DrawChartAlpPLpPDP(umId); }, 500);
                setTimeout(function () {
                    //html2canvas($("#tab-2").get(0), {
                    //    onrendered: function (canvas) {
                    //        var imgData = canvas.toDataURL('image/png');
                    //        pointTabImage = imgData;
                    //    }
                    //});
                    html2canvas($("#graphArea").get(0), {
                        onrendered: function (canvas) {
                            var imgData = canvas.toDataURL('image/png');
                            pointTabImage = imgData;
                        }
                    });
                }, 1000);
                setTimeout(function () {
                    html2canvas($("#tab2Data").get(0), {
                        onrendered: function (canvas) {
                            var imgData = canvas.toDataURL('image/png');
                            pointTopTabImage = imgData;
                        }
                    });
                }, 1000);

            }
            else if (a == 3) {
                $("#tab-1").hide();
                $("#tab-2").hide();
                $("#tab-3").show();
                $(".maintab li").removeClass("current");
                $("#t3").addClass("current");
                $("#tab-4").hide();
                $("#tab-5").hide();
                $("#tab-6").hide();


                setTimeout(function () {

                    html2canvas($("#competencePDFData").get(0), {
                        onrendered: function (canvas) {
                            var imgData = canvas.toDataURL('image/png');
                            competenceTabImage = imgData;
                        }
                    });
                }, 500);
            }
            else if (a == 4) {
                $("#tab-1").hide();
                $("#tab-2").hide();
                $("#tab-3").hide();
                $("#tab-4").show();
                $(".maintab li").removeClass("current");
                $("#t4").addClass("current");
                $("#tab-5").hide();
                $("#tab-6").hide();
                setTimeout(function () {
                    html2canvas($("#tab-4").get(0), {
                        onrendered: function (canvas) {
                            var imgData = canvas.toDataURL('image/png');
                            activityTabImage = imgData;
                        }
                    });
                }, 500);

            }
            else if (a == 5) {
                $("#tab-1").hide();
                $("#tab-2").hide();
                $("#tab-3").hide();
                $("#tab-4").hide();
                $("#tab-5").show();
                $("#tab-6").hide();
                $(".maintab li").removeClass("current");
                $("#t5").addClass("current");
            }
            else if (a == 6) {
                $("#tab-1").hide();
                $("#tab-2").hide();
                $("#tab-3").hide();
                $("#tab-4").hide();
                $("#tab-5").hide();
                $("#tab-6").show();
                $(".maintab li").removeClass("current");
                $("#t6").addClass("current");

                setTimeout(function () {
                    //var quesgridData = $("#QuesDataHtml1").get(0);
                    //var gd = '<style>.grEditRow{display:none;}</style>'.concat(quesgridData);
                    //quesgridData.innerHTML.concat('<style>.grEditRow{display:none;}</style>');
                    //html2canvas($("#QuesDataHtml1").get(0), {
                    //    onrendered: function (canvas) {
                    //        var imgData = canvas.toDataURL('image/png');
                    //        introManagementTabImage1 = imgData;
                    //    }
                    //});

                    //html2canvas($("#ContentPlaceHolder1_gvGrid_Question").get(0), {
                    //    onrendered: function (canvas) {
                    //        var imgData = canvas.toDataURL('image/png');
                    //        introManagementTabImage4 = imgData;
                    //    }
                    //});
                    //html2canvas($("#QuesDataHtml2").get(0), {
                    //    onrendered: function (canvas) {
                    //        var imgData = canvas.toDataURL('image/png');
                    //        introManagementTabImage2 = imgData;
                    //    }
                    //});

                }, 1000);
                setTimeout(function () {

                    //html2canvas($("#QuesDataHtml3").get(0), {
                    //    onrendered: function (canvas) {
                    //        var imgData = canvas.toDataURL('image/png');
                    //        introManagementTabImage3 = imgData;
                    //    }
                    //});
                }, 1500);
                if ($('#ContentPlaceHolder1_ddActCate').val() == "0") {
                    GetQuestionComment();
                }
            }
        }

        var alpImgBase64;
        var PDPImgBase64;
        function myFunction() {
            //Get QueryString Id

            var urlParms = new URLSearchParams(window.location.search);


            var id = urlParms.get('umId');
            var conclusion = '';

            $('#chartpdp').show();
            $('#chart').show();
            setTimeout(function () {
                DrawChartAlpPLpPDP(id);
            }, 1000);

            setTimeout(function () {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Report.aspx/Getdata",
                    data: "{'id':'" + id + "','pdp':'" + PDPImgBase64 + "','apl':'" + alpImgBase64 + "','conclusion':'" + conclusion + "'}",
                    dataType: "json",
                    success: function (data) {
                        var obj = data.d;
                        if (obj != '') {
                            $(".loader").hide();
                            $(".btn").show();
                            $('#chartpdp').hide();
                            $('#chart').hide();
                            window.open(data.d, "_blank");
                        }
                    },
                    error: function (result) {

                        $(".loader").hide();
                        $(".btn").show();
                    }
                });
            }, 4000);

        }

        /*2015-02-19 |GetTotalAlpPlpGroupByMonthByUserId|*/
        /*Function to get data and draw chart for employee skill by month |Saurin |20150219*/

        function DrawChartAlpPLpPDP(userId) {
            //alert(userId);
            //This two function will draw chart for particular user..
            GetTotalAlpPlpGroupByMonthByUserId(userId);
            PDPdataByMonthForPdfReport(userId);
            //Now convert chart to IMAGE
            setTimeout(function () {
                //GetChartImage("#graph-lines-pdp");
                GetChartImage("#graph-bars-pdp"); // Pass Id Which is used for chart DIV :        

                //GetChartImage("#graph-lines-alpplp");
                GetChartImage("#graph-bars-alpplp");
            }, 0);

        }


        var tab2Image;

        function GetTab2Image() {
            // alert('GetTab2Image');
            html2canvas($("#tab-2").get(0), {
                onrendered: function (canvas) {
                    var imgData = canvas.toDataURL('image/png');
                    tab2Image = imgData;
                }
            });
        }


        var alpImgBase64 = "";
        var PDPImgBase64 = "";

        function GetChartImage(GraphID) {

            /* || Saurin || 20150220*/
            /*include this two script on page where u need to apply this function. 
            "http://mrrio.github.io/jsPDF/dist/jspdf.min.js  // <--- optional where need to  save chart in pdf format
              
            and  https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js    */
            //GraphID = "#graph-linesEmployee";


            //  debugger;
            html2canvas($(GraphID).get(0), {
                onrendered: function (canvas) {
                    //document.body.appendChild(canvas);

                    var imgData = canvas.toDataURL('image/png');
                    // alert(imgData);
                    // if (GraphID == "#graph-lines-pdp") {
                    if (GraphID == "#graph-bars-pdp") {
                        PDPImgBase64 = imgData;

                    } else if (GraphID == "#graph-bars-alpplp") {  //graph-bars-alpplp
                        alpImgBase64 = imgData;

                    }

                    return;

                    //                    console.log('Report Image URL: ' + imgData);
                    //                    var doc = new jsPDF('landscape');

                    //                    doc.addImage(imgData, 'PNG', 10, 10, 190, 10);
                    //                    doc.save('c:/sample-file.jpeg');
                }
            });

        }

        var alpPlpDataByMonth;
        ///////////////////////////////////////////// tab-1 image////////////////////////////////////////////////////

        var Tab1;
        function GetCommentImgtab1() {
            //  alert('GetCommentImgtab1');
            if ($("#t1").hasClass("current") == true) {
                DisplayTab(1);

                html2canvas($("#tab-1").get(0), {
                    onrendered: function (canvas) {
                        Tab1 = canvas.toDataURL('image/png');
                    }
                });
            }
            return;

        }

        ////////////////////////////////////////////comment Image

        //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        var CmtimgData;
        function GetCommentImg() {
            // alert('GetCommentImg');
            DisplayTab(5);

            html2canvas($("#tab-5").get(0), {
                onrendered: function (canvas) {
                    CmtimgData = canvas.toDataURL('image/png');
                }
            });

            return;

        }
        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////





        function GetTotalAlpPlpGroupByMonthByUserId(dataNeedsOf) {

            // ie. : >>  var checkVideo = CommonAJSON("VideoList.asmx/IsItABadVideo", '{watchID:"' + watchID + '",platform:"Web"}'); 

            $.ajax({
                type: "POST",
                async: true,
                url: "webservice1.asmx/GetTotalAlpPlpGroupByMonthByUserId",
                data: "{'dataNeedsOf':'" + dataNeedsOf + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#divResult").html(msg.d);
                    alpPlpDataByMonth = msg.d;

                    setTimeout(function () {
                        DrawChartAlpPlpByMonth();
                        drwaGraphAlpPlpByMonth();
                        ForAlpPlpGraph1();
                    }, 0);

                    //return msg.d;

                },
                error: function (e) {
                    $("#divResult").html("WebSerivce unreachable");
                }
            });

            //  alert("aaa");
        }



        var ReturnData;
        function dateget(Date) {
            $("#save").hide();
            var histId = $("#ContentPlaceHolder1_ddActCate").val(); // .val by this the particular id will be passed.
            var urlParms = new URLSearchParams(window.location.search);

            if ($("#ContentPlaceHolder1_ddActCate :selected").val() != "0") {
                $("#ContentPlaceHolder1_save").hide();
            } else {
                window.location.reload(true);
                $("#ContentPlaceHolder1_save").show();
            }

            umId = urlParms.get('umId');
            if (histId == 0) {
                location.href = location.href.replace("true", "false");//Saurin
            }
            else {
                var ddlusername = $("#ContentPlaceHolder1_ddActCate :selected").text().split(' ');
                var len = ddlusername.length - 1;

                $("#ContentPlaceHolder1_lblddlUserName").text(ddlusername[len - 1] + " " + ddlusername[len]);
                $.ajax({
                    type: "POST",
                    async: false,
                    url: "PersonalDevelopmentPlan.aspx/datedata",
                    data: "{'histId':'" + histId + "'}", // 'histId': this will pass to method 
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (result) {
                        ReturnData = result;

                        $("#ContentPlaceHolder1_htmlData").empty().append(ReturnData.d); // .d by this we can get data which is store in returndata
                        $(".reply-sec").hide();
                        //alert("success");
                        //var div = document.getElementById('ContentPlaceHolder1_htmlData');

                        //div.innerHTML = div.innerHTML + 'getdata';

                        //return msg.d;
                        $("#save").hide();
                    },
                    error: function (e) {
                        window.open('PersonalDevelopmentPlan.aspx?umId=' + umId);

                    }
                });
            }
        }

        function dateget_Notification() {
            $("#save").hide();
            var histId = $("#ContentPlaceHolder1_hdnPDPHistoryIDNotification").val(); // .val by this the particular id will be passed.
            // alert(histId);
            if ($("#ContentPlaceHolder1_hdnPDPHistoryIDNotification").val() != "0") {

                $("#ContentPlaceHolder1_hdnPDPHistoryIDNotification").val("0");
                var urlParms = new URLSearchParams(window.location.search);
                $("#<%=ddActCate.ClientID%>").val(histId);
                //  $("#ContentPlaceHolder1_ddActCate :selected").val($("#ContentPlaceHolder1_hdnPDPHistoryIDNotification").val());

                if ($("#ContentPlaceHolder1_ddActCate :selected").val() != "0") {
                    $("#ContentPlaceHolder1_save").hide();
                } else {
                    window.location.reload(true);
                    $("#ContentPlaceHolder1_save").show();
                }

                umId = urlParms.get('umId');
                if (histId == 0) {
                    location.href = location.href.replace("true", "false");//Saurin
                }
                else {
                    var ddlusername = $("#ContentPlaceHolder1_ddActCate :selected").text().split(' ');
                    var len = ddlusername.length - 1;

                    $("#ContentPlaceHolder1_lblddlUserName").text(ddlusername[len - 1] + " " + ddlusername[len]);
                    $.ajax({
                        type: "POST",
                        async: false,
                        url: "PersonalDevelopmentPlan.aspx/datedata",
                        data: "{'histId':'" + histId + "'}", // 'histId': this will pass to method 
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (result) {
                            ReturnData = result;

                            $("#ContentPlaceHolder1_htmlData").empty().append(ReturnData.d); // .d by this we can get data which is store in returndata
                            $(".reply-sec").hide();
                            //alert("success");
                            //var div = document.getElementById('ContentPlaceHolder1_htmlData');

                            //div.innerHTML = div.innerHTML + 'getdata';

                            //return msg.d;
                            $("#save").hide();
                        },
                        error: function (e) {
                            window.open('PersonalDevelopmentPlan.aspx?umId=' + umId);

                        }
                    });
                }
            }
        }


        function DrawChartAlpPlpByMonth() {
            //var data1111a = dataa;



            var Achive = JSON.parse(alpPlpDataByMonth[1]);
            var Local = JSON.parse(alpPlpDataByMonth[0]);
            // var target = JSON.parse(data[2]);
            var graphData = [{
                // Visits
                label: "PLP Points",
//                label: $('#ContentPlaceHolder1_hdnPLPPointRes').val(),
                data: Local,
                color: '#a6d87a'
            }, {
                // Returning Visits
                label: "ALP Points",
                data: Achive,
                color: '#282828',
                points: { radius: 4, fillColor: '#ffffff' }
            }
            ];



            setTimeout(function () {
                $.plot("#graph-lines-alpplp", graphData, {
                    series: {
                        points: {
                            show: true,
                            radius: 5
                        },
                        lines: {
                            show: true
                        },
                        shadowSize: 0
                    },
                    grid: {
                        color: '#282828',
                        borderColor: 'transparent',
                        borderWidth: 60,
                        hoverable: true
                    },
                    xaxis: {
                        mode: "categories",
                        tickLength: 0
                    }
                });
            }, 0);

        }




        function drwaGraphAlpPlpByMonth() {

            var Achive = JSON.parse(alpPlpDataByMonth[1]);
            var Local = JSON.parse(alpPlpDataByMonth[0]);

            var graphData = [{
                //                label: "PLP Points",
                label: $('#ContentPlaceHolder1_hdnPLPPointRes').val(),
                data: Local,
                color: '#a6d87a'
            }, {
//                label: "ALP Points",
                label: $('#ContentPlaceHolder1_hdnALPointRes').val(),
                data: Achive,
                color: '#282828',
                points: { radius: 4, fillColor: '#282828' }
            }
            ];
            $.plot($('#graph-bars-alpplp'), graphData, {
                series: {
                    bars: {
                        show: true,
                        barWidth: .7,
                        align: 'center'
                    },
                    shadowSize: 0
                },
                grid: {

                    borderColor: 'transparent',
                    borderWidth: 40,
                    hoverable: true
                },
                xaxis: {
                    mode: "categories",
                    tickLength: 0
                }
            });

        }




        function ForAlpPlpGraph1() {
            $('#graph-bars-alpplp').show();

            $('#Alplines').on('click', function (e) {
                $('#AlpBars').removeClass('active');
                $('#graph-bars-alpplp').fadeOut();
                $(this).addClass('active');
                $('#graph-lines-alpplp').fadeIn();
                e.preventDefault();
            });

            $('#AlpBars').on('click', function (e) {
                $('#Alplines').removeClass('active');
                $('#graph-lines-alpplp').fadeOut();
                $(this).addClass('active');
                $('#graph-bars-alpplp').fadeIn().removeClass('hidden');
                e.preventDefault();
            });

            var previousPoint = null;

            $('#graph-lines-alpplp, #graph-bars-alpplp').bind('plothover', function (event, pos, item) {
                if (item) {
                    if (previousPoint != item.dataIndex) {
                        previousPoint = item.dataIndex;
                        $('#tooltip').remove();
                        var x = item.datapoint[0],
                            y = item.datapoint[1];
                        showTooltip(item.pageX, item.pageY, ' Point ' + y);
                    }
                } else {
                    $('#tooltip').remove();
                    previousPoint = null;
                }
            });

        }
        function showTooltip(x, y, contents) {
            $('<div id="tooltip">' + contents + '</div>').css({
                top: y - 16,
                left: x + 20
            }).appendTo('body').fadeIn();
        }
        /*2015-02-19 |GetTotalAlpPlpGroupByMonthByUserId|*/
        var PDPdataByMonth;
        function PDPdataByMonthForPdfReport(dataNeedsOf) {

            // ie. : >>  var checkVideo = CommonAJSON("VideoList.asmx/IsItABadVideo", '{watchID:"' + watchID + '",platform:"Web"}'); 
            var ReturnData;
            TeamID = 0;
            $.ajax({
                type: "POST",
                async: true,
                url: "webservice1.asmx/GetPDPpointByUserIdByMonth",
                data: "{'dataNeedsOf':'" + dataNeedsOf + "','TeamID': '" + TeamID + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#divResult").html(msg.d);
                    PDPdataByMonth = msg.d;

                    setTimeout(function () {
                        DrawChartPDPByMonth();
                        drwaGraphPDPByMonth();
                        ForPDPGraphByMonth();
                    }, 0);

                    //return msg.d;

                },
                error: function (e) {
                    $("#divResult").html("WebSerivce unreachable");

                }
            });

            //  alert("aaa");
        }

        function ForPDPGraphByMonth() {
           
            $('#graph-bars-pdp').show();

            $('#PDPlines').on('click', function (e) {
                $('#PDPBars').removeClass('active');
                $('#graph-bars-pdp').fadeOut();
                $(this).addClass('active');
                $('#graph-lines-pdp').fadeIn();
                e.preventDefault();
            });

            $('#PDPBars').on('click', function (e) {
                $('#PDPlines').removeClass('active');
                $('#graph-lines-pdp').fadeOut();
                $(this).addClass('active');
                $('#graph-bars-pdp').fadeIn().removeClass('hidden');
                e.preventDefault();
            });

            var previousPoint = null;

            $('#graph-lines-pdp, #graph-bars-pdp').bind('plothover', function (event, pos, item) {
                if (item) {
                    if (previousPoint != item.dataIndex) {
                        previousPoint = item.dataIndex;
                        $('#tooltip').remove();
                        var x = item.datapoint[0],
                            y = item.datapoint[1];
                        showTooltip(item.pageX, item.pageY, ' Point ' + y);
                    }
                } else {
                    $('#tooltip').remove();
                    previousPoint = null;
                }
            });

        }
        function DrawChartPDPByMonth() {


            var Achive = JSON.parse(PDPdataByMonth[0]);

            // var target = JSON.parse(data[2]);
            var graphData = [{
                // Returning Visits
//                label: "PDP Points",
//                label: $('#ContentPlaceHolder1_hdnPDPointRes').val(),
                data: Achive,
                color: '#282828',
                points: { radius: 4, fillColor: '#ffffff' }
            }
            ];



            setTimeout(function () {
                $.plot("#graph-lines-pdp", graphData, {
                    series: {
                        points: {
                            show: true,
                            radius: 5
                        },
                        lines: {
                            show: true
                        },
                        shadowSize: 0
                    },
                    grid: {
                        color: '#282828',
                        borderColor: 'transparent',
                        borderWidth: 60,
                        hoverable: true
                    },
                    xaxis: {
                        mode: "categories",
                        tickLength: 0
                    }
                });
            }, 0);

        }
        function drwaGraphPDPByMonth() {

            var Achive = JSON.parse(PDPdataByMonth[0]);


            var graphData = [{
             //  label: "PDP Points",
                label: $('#ContentPlaceHolder1_hdnPDPointRes').val(),
                data: Achive,
                color: '#282828',
                points: { radius: 4, fillColor: '#282828' }
            }
            ];
            $.plot($('#graph-bars-pdp'), graphData, {
                series: {
                    bars: {
                        show: true,
                        barWidth: .7,
                        align: 'center'
                    },
                    shadowSize: 0
                },
                grid: {

                    borderColor: 'transparent',
                    borderWidth: 40,
                    hoverable: true
                },
                xaxis: {
                    mode: "categories",
                    tickLength: 0
                }
            });

        }

    </script>
    <script type="text/javascript">
        function SaveQuesDataPDF() {
            ajaxindicatorstart('loading...');
            var t1 = JSON.stringify($('#divPersonalData').html());
            var t2 = JSON.stringify($('#userImageData').html());
            var t3 = JSON.stringify($('#ContentPlaceHolder1_Lblcom').html());
            var t4 = JSON.stringify($('#ContentPlaceHolder1_Lbldiv').html());
            var t5 = JSON.stringify($('#ContentPlaceHolder1_LdlEmpna').html());
            var t6 = JSON.stringify($('#ContentPlaceHolder1_LblJob').html());
            var t7 = JSON.stringify($('#ContentPlaceHolder1_lblTeamName').html());
            var t8 = JSON.stringify($('#ContentPlaceHolder1_LblEmail').html());
            var t9 = JSON.stringify($('#ContentPlaceHolder1_Lblpnum').html());

            var $divMyTest = $('#graphArea').clone();
            $divMyTest.empty();
            var htmlGridData = $('#QuesDataHtml1').html();
            $divMyTest.append(htmlGridData);
            $divMyTest.find('.grEditRow').remove();

            var t10 = JSON.stringify($divMyTest.html());

            var tmpt555 = $('#QuesDataHtml2').html();
            var tmpt5 = $('#QuesDataHtml5').html();
            var tmpt55 = $('#QuesDataHtml3').html();
            var tm = tmpt555 + tmpt5 + tmpt55;
            var t11 = JSON.stringify(tm);

            var tmpt6 = '';
            var tmpt66 = '';
            tmpt66 = $('#ContentPlaceHolder1_txtPersonalComment').val();
            tmpt6 = $('#ContentPlaceHolder1_QuesDataHtml6').html();
            var t6m = '';
            if (tmpt66 == '' || tmpt66 == null) {
            }
            else {
                t6m = tmpt6 + "<br/><br/>" + tmpt66;
            }

            var t12 = JSON.stringify(t6m);


            $.ajax({
                type: "POST",
                cache: false,
                async: false,
                url: "PersonalDevelopmentPlan.aspx/SaveQuestionHtmlDataAsPDF",
                data: "{ t1:" + t1 + ", t2:" + t2 + ", t3:" + t3 + ", t4:" + t4 + ", t5:" + t5 + ", t6:" + t6 + ", t7:" + t7 + ", t8:" + t8 + ", t9:" + t9 + ", t10:" + t10 + ", t11:" + t11 + ", t12:" + t12 + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                //beforeSend: function () {
                //    ajaxindicatorstart('loading...');
                //},
                success: function (response) {

                    var str = response.d;

                    window.open(str);

                },
                failure: function (msg) {
                    alert('error');
                },
                complete: function () {
                    ajaxindicatorstop();
                    window.location.reload(true);
                }
            });
        }
        function SavePointDataPDF() {
            ajaxindicatorstart('loading...');
            var t1 = JSON.stringify($('#divPersonalData').html());
            var t2 = JSON.stringify($('#userImageData').html());
            var t3 = JSON.stringify($('#ContentPlaceHolder1_Lblcom').html());
            var t4 = JSON.stringify($('#ContentPlaceHolder1_Lbldiv').html());
            var t5 = JSON.stringify($('#ContentPlaceHolder1_LdlEmpna').html());
            var t6 = JSON.stringify($('#ContentPlaceHolder1_LblJob').html());
            var t7 = JSON.stringify($('#ContentPlaceHolder1_lblTeamName').html());
            var t8 = JSON.stringify($('#ContentPlaceHolder1_LblEmail').html());
            var t9 = JSON.stringify($('#ContentPlaceHolder1_Lblpnum').html());

            var $divMy2 = $('#graphArea').clone();
            $divMy2.empty();
            var img1 = $('<img>'); //Equivalent: $(document.createElement('img'))
            img1.attr('src', pointTopTabImage);
            img1.attr('width', "1000px");
            img1.appendTo($divMy2);
            var htmlTab2Datapoint = $divMy2.html();

            $divMy2.empty();
            img1 = $('<img>'); //Equivalent: $(document.createElement('img'))
            img1.attr('src', pointTabImage);
            img1.attr('width', "1000px");
            img1.appendTo($divMy2);

            htmlTab2Datapoint = htmlTab2Datapoint + $divMy2.html();

            var t10 = JSON.stringify(htmlTab2Datapoint);


            var $divMyTest = $('#graphArea').clone();
            $divMyTest.empty();
            var htmlGridData = $('.plpalppointClass').html();
            $divMyTest.append(htmlGridData);
            $divMyTest.find('.dataTables_filter').remove();
            $divMyTest.find('.dataTables_length').remove();
            $divMyTest.find('.dataTables_info').remove();
            $divMyTest.find('.dataTables_paginate').remove();
            $divMyTest.find('.lastrow').remove();
            // $divMyTest.find('#ContentPlaceHolder1_gvGrid').width('150%');

            var t11 = JSON.stringify($divMyTest.html());

            $.ajax({
                type: "POST",
                cache: false,
                async: false,
                url: "PersonalDevelopmentPlan.aspx/SavePointHtmlDataAsPDF",
                data: "{ t1:" + t1 + ", t2:" + t2 + ", t3:" + t3 + ", t4:" + t4 + ", t5:" + t5 + ", t6:" + t6 + ", t7:" + t7 + ", t8:" + t8 + ", t9:" + t9 + ", t10:" + t10 + ", t11:" + t11 + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                //beforeSend: function () {
                //    ajaxindicatorstart('loading...');
                //},
                success: function (response) {

                    var str = response.d;
                    window.open(str);
                },
                failure: function (msg) {
                    alert('error');
                },
                complete: function () {
                    ajaxindicatorstop();
                }
            });

        }
        function SaveCompetenceDataPDF() {
            ajaxindicatorstart('loading...');
            var t1 = JSON.stringify($('#divPersonalData').html());
            var t2 = JSON.stringify($('#userImageData').html());
            var t3 = JSON.stringify($('#ContentPlaceHolder1_Lblcom').html());
            var t4 = JSON.stringify($('#ContentPlaceHolder1_Lbldiv').html());
            var t5 = JSON.stringify($('#ContentPlaceHolder1_LdlEmpna').html());
            var t6 = JSON.stringify($('#ContentPlaceHolder1_LblJob').html());
            var t7 = JSON.stringify($('#ContentPlaceHolder1_lblTeamName').html());
            var t8 = JSON.stringify($('#ContentPlaceHolder1_LblEmail').html());
            var t9 = JSON.stringify($('#ContentPlaceHolder1_Lblpnum').html());

            var t10 = JSON.stringify($('#ContentPlaceHolder1_lbl').html());
            var t11 = JSON.stringify($('#ContentPlaceHolder1_lbl2').html());

            var $divMyTest1 = $('#graphArea').clone();
            $divMyTest1.empty();
            $divMyTest1.append($('#divApproval').html());
            $divMyTest1.find('.RemoveTD').remove();
            var t12 = JSON.stringify($divMyTest1.html());

            //var t13 = JSON.stringify($('#ContentPlaceHolder1_lblcompetenceDevelopmentPoint').html());
            //var t14 = JSON.stringify($('#ContentPlaceHolder1_lblcompetenceDevelopment').html());


            var $divMyTest7 = $('#graphArea').clone();
            $divMyTest7.empty();
            var htmlGridData7 = $('#ContentPlaceHolder1_lblcompetenceDevelopmentPoint').html();
            $divMyTest7.append(htmlGridData7);
            $divMyTest7.find('td').css('width', '');
            $divMyTest7.find('td').css('width', '');

            var $divMyTest8 = $('#graphArea').clone();
            $divMyTest8.empty();
            var htmlGridData8 = $('#ContentPlaceHolder1_lblcompetenceDevelopment').html();
            $divMyTest8.append(htmlGridData8);
            $divMyTest8.find('td').css('width', '');
            $divMyTest8.find('td').css('width', '');

            var t13 = JSON.stringify($divMyTest7.html());
            var t14 = JSON.stringify($divMyTest8.html());

            var $divMyTest = $('#graphArea').clone();
            $divMyTest.empty();
            var htmlGridData = $('.comp_table').html();
            $divMyTest.append(htmlGridData);
            $divMyTest.find('.dataTables_filter').remove();
            $divMyTest.find('.dataTables_length').remove();
            $divMyTest.find('.dataTables_info').remove();
            $divMyTest.find('.dataTables_paginate').remove();
            var t15 = JSON.stringify($divMyTest.html());

            $.ajax({
                type: "POST",
                cache: false,
                async: false,
                url: "PersonalDevelopmentPlan.aspx/SaveCompetenceHtmlDataAsPDF",
                data: "{ t1:" + t1 + ", t2:" + t2 + ", t3:" + t3 + ", t4:" + t4 + ", t5:" + t5 + ", t6:" + t6 + ", t7:" + t7 + ", t8:" + t8 + ", t9:" + t9 + ", t10:" + t10 + ", t11:" + t11 + ", t12:" + t12 + ", t13:" + t13 + ", t14:" + t14 + ", t15:" + t15 + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                //beforeSend: function () {
                //    ajaxindicatorstart('loading...');
                //},
                success: function (response) {
                    var str = response.d;
                    window.open(str);
                },
                failure: function (msg) {
                    alert('error');
                },
                complete: function () {
                    ajaxindicatorstop();
                }
            });
        }
        function SaveIntroManagementDataPDF() {
            ajaxindicatorstart('loading...');
            var t1 = JSON.stringify($('#divPersonalData').html());
            var t2 = JSON.stringify($('#userImageData').html());
            var t3 = JSON.stringify($('#ContentPlaceHolder1_Lblcom').html());
            var t4 = JSON.stringify($('#ContentPlaceHolder1_Lbldiv').html());
            var t5 = JSON.stringify($('#ContentPlaceHolder1_LdlEmpna').html());
            var t6 = JSON.stringify($('#ContentPlaceHolder1_LblJob').html());
            var t7 = JSON.stringify($('#ContentPlaceHolder1_lblTeamName').html());
            var t8 = JSON.stringify($('#ContentPlaceHolder1_LblEmail').html());
            var t9 = JSON.stringify($('#ContentPlaceHolder1_Lblpnum').html());

            var t10 = JSON.stringify($('#ContentPlaceHolder1_lbl').html());
            var t11 = JSON.stringify($('#ContentPlaceHolder1_lbl2').html());

            var $divMyTest1 = $('#graphArea').clone();
            $divMyTest1.empty();
            $divMyTest1.append($('#divApproval').html());
            $divMyTest1.find('.RemoveTD').remove();
            var t12 = JSON.stringify($divMyTest1.html());

            $.ajax({
                type: "POST",
                cache: false,
                async: false,
                url: "PersonalDevelopmentPlan.aspx/SaveIntroManagementDataPDF",
                data: "{ t1:" + t1 + ", t2:" + t2 + ", t3:" + t3 + ", t4:" + t4 + ", t5:" + t5 + ", t6:" + t6 + ", t7:" + t7 + ", t8:" + t8 + ", t9:" + t9 + ", t10:" + t10 + ", t11:" + t11 + ", t12:" + t12 + "}",
                contentType: "application/json; charset=utf-8",
                //beforeSend: function () {
                //    ajaxindicatorstart('loading...');
                //},
                success: function (response) {
                    var str = response.d;
                    window.open(str);
                },
                failure: function (msg) {
                    alert('error');
                },
                complete: function () {
                    ajaxindicatorstop();
                }
            });
        }
        function SaveActivityDataPDF() {
            ajaxindicatorstart('loading...');
            debugger;
            var t1 = JSON.stringify($('#divPersonalData').html());
            var t2 = JSON.stringify($('#userImageData').html());
            var t3 = JSON.stringify($('#ContentPlaceHolder1_Lblcom').html());
            var t4 = JSON.stringify($('#ContentPlaceHolder1_Lbldiv').html());
            var t5 = JSON.stringify($('#ContentPlaceHolder1_LdlEmpna').html());
            var t6 = JSON.stringify($('#ContentPlaceHolder1_LblJob').html());
            var t7 = JSON.stringify($('#ContentPlaceHolder1_lblTeamName').html());
            var t8 = JSON.stringify($('#ContentPlaceHolder1_LblEmail').html());
            var t9 = JSON.stringify($('#ContentPlaceHolder1_Lblpnum').html());


            var $divMyTest = $('#graphArea').clone();
            $divMyTest.empty();
            var htmlGridData = $('.comp_table').html();
            $divMyTest.append(htmlGridData);
            $divMyTest.find('.dataTables_filter').remove();
            $divMyTest.find('.dataTables_length').remove();
            $divMyTest.find('.dataTables_info').remove();
            $divMyTest.find('.dataTables_paginate').remove();


            var t10 = JSON.stringify($divMyTest.html());

            $.ajax({
                type: "POST",
                cache: false,
                async: false,
                url: "PersonalDevelopmentPlan.aspx/SaveActivityHtmlDataAsPDF",
                data: "{ t1:" + t1 + ", t2:" + t2 + ", t3:" + t3 + ", t4:" + t4 + ", t5:" + t5 + ", t6:" + t6 + ", t7:" + t7 + ", t8:" + t8 + ", t9:" + t9 + ", t10:" + t10 + "}",
                //data: '{ t1:' + t1 + ',t2:' + '' + ',t3:' + '' + ',t4:' + '' + ',t5:' + '' + ',styleData:' + '' + '}',

                contentType: "application/json; charset=utf-8",
                dataType: "json",
                //beforeSend: function () {
                //    ajaxindicatorstart('loading...');
                //},
                success: function (response) {
                    var str = response.d;
                    window.open(str);
                },
                failure: function (msg) {
                    alert('error');
                },
                complete: function () {
                    ajaxindicatorstop();
                }
            });
        }

        function SaveFullPagePDF() {
            ajaxindicatorstart('loading...');

            //---------------FirstTab-----------------------
            var t1 = JSON.stringify($('#divPersonalData').html());
            var t2 = JSON.stringify($('#userImageData').html());
            var t3 = JSON.stringify($('#ContentPlaceHolder1_Lblcom').html());
            var t4 = JSON.stringify($('#ContentPlaceHolder1_Lbldiv').html());
            var t5 = JSON.stringify($('#ContentPlaceHolder1_LdlEmpna').html());
            var t6 = JSON.stringify($('#ContentPlaceHolder1_LblJob').html());
            var t7 = JSON.stringify($('#ContentPlaceHolder1_lblTeamName').html());
            var t8 = JSON.stringify($('#ContentPlaceHolder1_LblEmail').html());
            var t9 = JSON.stringify($('#ContentPlaceHolder1_Lblpnum').html());

            var t10 = JSON.stringify($('#ContentPlaceHolder1_lbl').html());
            var t11 = JSON.stringify($('#ContentPlaceHolder1_lbl2').html());

            var $divMyTest1 = $('#graphArea').clone();
            $divMyTest1.empty();
            $divMyTest1.append($('#divApproval').html());
            $divMyTest1.find('.RemoveTD').remove();
            var t12 = JSON.stringify($divMyTest1.html());
            //---------------FirstTab-----------------------
            //---------------SecondTab----------------------
            var $divMyTest2 = $('#graphArea').clone();
            $divMyTest2.empty();
            var htmlGridData = $('#QuesDataHtml1').html();
            $divMyTest2.append(htmlGridData);
            $divMyTest2.find('.grEditRow').remove();

            var tmpt555 = $('#QuesDataHtml2').html();
            var tmpt5 = $('#QuesDataHtml5').html();
            var tmpt55 = $('#QuesDataHtml3').html();
            var tm = tmpt555 + tmpt5 + tmpt55;

            var tmpt6 = '';
            var tmpt66 = '';
            tmpt66 = $('#ContentPlaceHolder1_txtPersonalComment').val();
            tmpt6 = $('#ContentPlaceHolder1_QuesDataHtml6').html();
            var t6m = '';
            if (tmpt66 == '' || tmpt66 == null) {
            }
            else {
                t6m = '<span style="font-size: large;font-weight: bolder;">' + tmpt6 + "</span><br/><br/>" + tmpt66;
            }

            var t13 = JSON.stringify(tm);
            var t14 = JSON.stringify($divMyTest2.html());
            var t15 = JSON.stringify(t6m);
            //---------------SecondTab----------------------
            //---------------ThirdTab----------------------
            var $divMy2 = $('#graphArea').clone();
            $divMy2.empty();
            var img1 = $('<img>'); //Equivalent: $(document.createElement('img'))
            img1.attr('src', pointTopTabImage);
            img1.attr('width', "1000px");
            img1.appendTo($divMy2);
            var htmlTab2Datapoint = $divMy2.html();

            $divMy2.empty();
            img1 = $('<img>'); //Equivalent: $(document.createElement('img'))
            img1.attr('src', pointTabImage);
            img1.attr('width', "1000px");
            img1.appendTo($divMy2);

            htmlTab2Datapoint = htmlTab2Datapoint + $divMy2.html();

            var $divMyTest = $('#graphArea').clone();
            $divMyTest.empty();
            var htmlGridData = $('.plpalppointClass').html();
            $divMyTest.append(htmlGridData);
            $divMyTest.find('.dataTables_filter').remove();
            $divMyTest.find('.dataTables_length').remove();
            $divMyTest.find('.dataTables_info').remove();
            $divMyTest.find('.dataTables_paginate').remove();
            $divMyTest.find('.lastrow').remove();
            // $divMyTest.find('#ContentPlaceHolder1_gvGrid').width('150%');

            var t16 = JSON.stringify(htmlTab2Datapoint);
            var t17 = JSON.stringify($divMyTest.html());
            //---------------ThirdTab----------------------
            //---------------FourthTab----------------------
            var $divMyTest = $('#graphArea').clone();
            $divMyTest.empty();
            var htmlGridData = $('.comp_table').html();
            $divMyTest.append(htmlGridData);
            $divMyTest.find('.dataTables_filter').remove();
            $divMyTest.find('.dataTables_length').remove();
            $divMyTest.find('.dataTables_info').remove();
            $divMyTest.find('.dataTables_paginate').remove();

            //var t18 = JSON.stringify($('#ContentPlaceHolder1_lblcompetenceDevelopmentPoint').html());
            //var t19 = JSON.stringify($('#ContentPlaceHolder1_lblcompetenceDevelopment').html());

            var $divMyTest7 = $('#graphArea').clone();
            $divMyTest7.empty();
            var htmlGridData7 = $('#ContentPlaceHolder1_lblcompetenceDevelopmentPoint').html();
            $divMyTest7.append(htmlGridData7);
            $divMyTest7.find('td').css('width', '');
            $divMyTest7.find('td').css('width', '');

            var $divMyTest8 = $('#graphArea').clone();
            $divMyTest8.empty();
            var htmlGridData8 = $('#ContentPlaceHolder1_lblcompetenceDevelopment').html();
            $divMyTest8.append(htmlGridData8);
            $divMyTest8.find('td').css('width', '');
            $divMyTest8.find('td').css('width', '');

            var t18 = JSON.stringify($divMyTest7.html());
            var t19 = JSON.stringify($divMyTest8.html());

            var t20 = JSON.stringify($divMyTest.html());
            //---------------FourthTab----------------------
            //---------------5thTab----------------------
            //var $divMyTest = $('#graphArea').clone();
            //$divMyTest.empty();
            //var htmlGridData = $('.comp_table').html();
            //$divMyTest.append(htmlGridData);
            //$divMyTest.find('.dataTables_filter').remove();
            //$divMyTest.find('.dataTables_length').remove();
            //$divMyTest.find('.dataTables_info').remove();
            //$divMyTest.find('.dataTables_paginate').remove();

            //var t10 = JSON.stringify($divMyTest.html());
            //---------------5thTab----------------------
            $.ajax({
                type: "POST",
                cache: false,
                async: false,
                url: "PersonalDevelopmentPlan.aspx/SaveHtmlDataAsPDF",
                data: "{ t1:" + t1 + ", t2:" + t2 + ", t3:" + t3 + ", t4:" + t4 + ", t5:" + t5 + ", t6:" + t6 + ", t7:" + t7 + ", t8:" + t8 + ", t9:" + t9 + ", t10:" + t10 + ", t11:" + t11 + ", t12:" + t12 + ", t13:" + t13 + ", t14:" + t14 + ", t15:" + t15 + ", t16:" + t16 + ", t17:" + t17 + ", t18:" + t18 + ", t19:" + t19 + ", t20:" + t20 + "}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    //$(".tab").fadeIn();
                    //$(".loaderDiv").fadeOut();//show
                    var str = response.d;
                    window.open(str);


                },
                failure: function (msg) {
                    alert('error');
                },
                complete: function () {
                    ajaxindicatorstop();
                    // window.location.reload(true);
                }
            });
        }



    </script>
    <script type="text/javascript">
        function SaveActivityDataPDF_old() {
            // generate("warning", "Please wait, we are generating pdf, it will take some time.", "bottomCenter");
            var $divMy2 = $('#graphArea').clone();
            var $divMyTest = $('#graphArea').clone();
            $divMy2.empty();
            $divMyTest.empty();

            var img1 = $('<img>'); //Equivalent: $(document.createElement('img'))
            img1.attr('src', activityTabImage);
            img1.attr('width', "1000px");
            img1.appendTo($divMy2);
            var htmlTab2Dataact = $divMy2.html();


            var htmlGridData = $('.comp_table').html();
            $divMyTest.append(htmlGridData);

            var tab1 = "<div style='font-size: 23px;font-family: Verdana, Arial, Helvetica, sans-serif;color: #5781bd;'>" + $('#divPersonalData').html() + "</div>";

            var tab1IntroData = $('#tab1IntroData').html();
            var tab12 = $('#divEmpDesc').html();

            $divMyTest.find('.dataTables_filter').remove();
            $divMyTest.find('.dataTables_length').remove();
            $divMyTest.find('.dataTables_info').remove();
            $divMyTest.find('.dataTables_paginate').remove();

            htmlTab2Dataact = tab1 + "<br/>" + tab12 + "<br/>" + tab1IntroData + "<br/>" + $divMyTest.html();

            var t1 = JSON.stringify(htmlTab2Dataact);

            $.ajax({
                type: "POST",
                cache: false,
                async: false,
                url: "PersonalDevelopmentPlan.aspx/SaveActivityHtmlDataAsPDF",
                data: '{ t1:' + t1 + '}',
                //data: '{ t1:' + t1 + ',t2:' + '' + ',t3:' + '' + ',t4:' + '' + ',t5:' + '' + ',styleData:' + '' + '}',

                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    ajaxindicatorstart('loading...');
                },
                success: function (response) {
                    var str = response.d;
                    window.open(str);
                },
                failure: function (msg) {
                    alert('error');
                },
                complete: function () {
                    ajaxindicatorstop();
                }
            });

        }
        function SaveQuesDataPDF_old() {
            // generate("warning", "Please wait, we are generating pdf, it will take some time.", "bottomCenter");
            //var $divMy2 = $('#graphArea').clone();
            //$divMy2.empty();
            //var img1 = $('<img>'); //Equivalent: $(document.createElement('img'))
            //img1.attr('src', introManagementTabImage1);
            //img1.attr('width', "1000px");
            //img1.appendTo($divMy2);
            //var htmlTab2Datapoint = $divMy2.html();


            //$divMy2.empty();
            //var img2 = $('<img>'); //Equivalent: $(document.createElement('img'))
            //img2.attr('src', introManagementTabImage4);
            //img2.attr('width', "1000px");
            //img2.appendTo($divMy2);
            //var htmlTab2Datapoint2 = $divMy2.html();

            //$divMy2.empty();
            //var img3 = $('<img>'); //Equivalent: $(document.createElement('img'))
            //img3.attr('src', introManagementTabImage2);
            //img3.attr('width', "500px");
            //img3.appendTo($divMy2);
            //var htmlTab2Datapoint3 = $divMy2.html();

            //$divMy2.empty();
            //var img3 = $('<img>'); //Equivalent: $(document.createElement('img'))
            //img3.attr('src', introManagementTabImage3);
            //img3.attr('width', "500px");
            //img3.appendTo($divMy2);
            //var htmlTab2Datapoint4 = $divMy2.html();

            // var t1 = JSON.stringify(htmlTab2Datapoint);
            //var t2 = JSON.stringify(htmlTab2Datapoint2);
            //var t3 = JSON.stringify(htmlTab2Datapoint3);
            //var t4 = JSON.stringify(htmlTab2Datapoint4);
            //  var t6 = JSON.stringify($('#QuesDataHtml5').html().trim());

            var t1 = JSON.stringify($('#QuesDataHtml1').html());
            var tmpt5 = $('#QuesDataHtml5').html();
            var tmpt55 = $('#QuesDataHtml3').html();
            var tm = tmpt5 + tmpt55;
            var t5 = JSON.stringify(tm);

            var tmpt6 = '';
            var tmpt66 = '';

            //if ($('#ContentPlaceHolder1_hdnIsDisplayInReport').val() == "True") {
            //  alert($('#ContentPlaceHolder1_hdnIsDisplayInReport').val());
            tmpt66 = $('#ContentPlaceHolder1_txtPersonalComment').html();
            tmpt6 = $('#QuesDataHtml6').html();
            // }
            //tmpt66 = $('#ContentPlaceHolder1_txtPersonalComment').html();
            var t6m = tmpt6 + "<br/><br/>" + tmpt66;
            var t6 = JSON.stringify(t6m);

            $.ajax({
                type: "POST",
                cache: false,
                async: false,
                url: "PersonalDevelopmentPlan.aspx/SaveQuestionHtmlDataAsPDF",
                // data: '{ t1:' + t1 + ',t2:' + t2 + ',t3:' + t3 + ',t4:' + t4 + ',t5:' + t5 + '}',
                data: '{ t1:' + t1 + ',t5:' + t5 + ',t6:' + t6 + '}',
                //data: '{ t1:' + t1 + ',t2:' + '' + ',t3:' + '' + ',t4:' + '' + ',t5:' + '' + ',styleData:' + '' + '}',

                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    ajaxindicatorstart('loading...');
                },
                success: function (response) {

                    var str = response.d;

                    window.open(str);

                },
                failure: function (msg) {
                    alert('error');
                },
                complete: function () {
                    ajaxindicatorstop();
                    window.location.reload(true);
                }
            });

        }
        function SavePointDataPDF_old() {



            var $divMy2 = $('#graphArea').clone();
            $divMy2.empty();
            var img1 = $('<img>'); //Equivalent: $(document.createElement('img'))
            img1.attr('src', pointTabImage);
            img1.attr('width', "1000px");
            img1.appendTo($divMy2);
            var htmlTab2Datapoint = $divMy2.html();
            var t1 = JSON.stringify(htmlTab2Datapoint);

            //setTimeout(function () {

            $.ajax({
                type: "POST",
                cache: false,
                async: false,
                url: "PersonalDevelopmentPlan.aspx/SavePointHtmlDataAsPDF",
                data: '{ t1:' + t1 + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    ajaxindicatorstart('loading...');
                },
                success: function (response) {

                    var str = response.d;
                    window.open(str);
                },
                failure: function (msg) {
                    alert('error');
                },
                complete: function () {
                    ajaxindicatorstop();
                }
            });

            //}, 2000);
        }
        function SaveCompetenceDataPDF_old() {
            //  generate("warning", "Please wait, we are generating pdf, it will take some time.", "bottomCenter");
            var $divMy2 = $('#graphArea').clone();
            $divMy2.empty();
            var img1 = $('<img>'); //Equivalent: $(document.createElement('img'))
            img1.attr('src', competenceTabImage);
            img1.attr('width', "1000px");
            img1.appendTo($divMy2);
            var htmlTab2Datacomp = $divMy2.html();

            var t1 = JSON.stringify(htmlTab2Datacomp);
            var t2 = JSON.stringify($('#ContentPlaceHolder1_lblcompetenceDevelopment').html());

            $.ajax({
                type: "POST",
                cache: false,
                async: false,
                url: "PersonalDevelopmentPlan.aspx/SaveCompetenceHtmlDataAsPDF",
                data: '{ t1:' + t1 + ',t2:' + t2 + '}',

                //data: '{ t1:' + t1 + ',t2:' + '' + ',t3:' + '' + ',t4:' + '' + ',t5:' + '' + ',styleData:' + '' + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                beforeSend: function () {
                    ajaxindicatorstart('loading...');
                },
                success: function (response) {
                    var str = response.d;
                    window.open(str);
                },
                failure: function (msg) {
                    alert('error');
                },
                complete: function () {
                    ajaxindicatorstop();
                }
            });

        }
        function SaveIntroManagementDataPDF_old() {
            ajaxindicatorstart('loading...');

            var t_1 = "<div style='font-size: 23px;font-family: Verdana, Arial, Helvetica, sans-serif;color: #5781bd;'>" + $('#divPersonalData').html() + "</div>";
            var t_11 = $('#tab1IntroData').html();
            var t_111 = $('#divEmpDesc').html();
            var t1 = "<div class='col-md-12'>" + t_1 + "<br/><br/>" + t_11 + "<br/><br/>" + t_111 + "</div>";

            var t4 = "<div style='margin-left: 50px;'>" + $('#divApproval').html() + "</div>";

            var $divMy4 = $('#graphArea').clone();
            $divMy4.empty();
            $divMy4.append(t4);
            $divMy4.find('.HideControl').remove();
            t4 = $divMy4.html();


            var finalData = t1;
            var tt = JSON.stringify(finalData);
            var t432 = JSON.stringify(t4);

            var mnam = $('#ContentPlaceHolder1_lbl').text();
            var mnam1 = $('#ContentPlaceHolder1_lbl2').text();

            // alert(t432);
            $.ajax({
                type: "POST",
                cache: false,
                async: false,
                url: "PersonalDevelopmentPlan.aspx/SaveIntroManagementDataPDF",
                data: "{ t1:" + tt + ", t2:" + JSON.stringify(mnam) + ", t3:" + JSON.stringify(mnam1) + ", t4:" + t432 + "}",
                //data: '{ t1:"' + tt + '",t2:"' + mnam + '"}',
                //data: '{ t1:' + tt + ',metTitle:' + $('#ContentPlaceHolder1_lbl').text() + ',docTitle:' + $('#ContentPlaceHolder1_lbl2').text() + ',approvalData:' + t432 + '}',
                // data: '{ t1:' + tt + ',metTitle:' + mnam + '}',
                //data: '{ t1:' + tt + '}',
                //data: '{ t1:' + t1 + ',t2:' + '' + ',t3:' + '' + ',t4:' + '' + ',t5:' + '' + ',styleData:' + '' + '}',

                contentType: "application/json; charset=utf-8",
                beforeSend: function () {
                    ajaxindicatorstart('loading...');
                },
                success: function (response) {
                    var str = response.d;
                    window.open(str);
                },
                failure: function (msg) {
                    alert('error');
                },
                complete: function () {
                    ajaxindicatorstop();
                }
            });
        }
        function SaveIntroManagementDataPDF_old() {
            //  generate("warning", "Please wait, we are generating pdf, it will take some time.", "bottomCenter");
            ajaxindicatorstart('loading...');

            var t_1 = "<div style='font-size: 23px;font-family: Verdana, Arial, Helvetica, sans-serif;color: #5781bd;'>" + $('#divPersonalData').html() + "</div>";
            var t_11 = $('#tab1IntroData').html();
            var t_111 = $('#divEmpDesc').html();
            var t1 = t_1 + "<br/>" + t_11 + "<br/>" + t_111;

            //var $divMy2 = $('#graphArea').clone();
            //$divMy2.empty();
            var t_2 = $('#divMeetingStatus').html();
            var t_22 = $('#meetingDataList').html();
            t_22 = t_22.replace("border='1'", "");
            t2 = "<div style='margin-left: 35px;'>" + t_2 + "<br/>" + t_22 + "</div>";
            //$divMy2.append(t2);
            //$divMy2.css({ 'margin-left': '20px;' });
            //t2 = $divMy2.html();
            //alert(t2);

            //var $divMy3 = $('#graphArea').clone();
            //$divMy3.empty();
            var t_3 = $('#divRelatedDoc').html();
            var t_33 = $('#divRealtedDocumentList').html();
            t_33 = t_33.replace("border='1'", "");
            t3 = "<div style='margin-left: 35px;'>" + t_3 + "<br/>" + t_33 + "</div>";
            //$divMy3.append(t3);
            //$divMy3.css({ 'margin-left': '20px;' });
            //t3 = $divMy3.html();

            //var $divMy4 = $('#graphArea').clone();
            //$divMy4.empty();
            //$divMy4.append($('#divApproval').html());
            //$divMy4.css({ 'margin-left': '20px;' });
            //var t4 = $divMy4.html();

            var t4 = "<div style='margin-left: 50px;'>" + $('#divApproval').html() + "</div>";

            //   alert(t4);
            //debugger;
            //var t5 ="<div style='margin-left: 35px;'>" + $('#divCompanyDirectionPDF').html().replace(/border-style:None/g, "border-style:hidden") +"</div>";
            //var $divMy5 = $('#graphArea').clone();
            //$divMy5.empty();
            //$divMy5.append(t5);
            //$divMy5.find('.table').removeAttr("border");
            //t5 = $divMy5.html();


            //  replace( /border-style:None/g, "border-style:hidden" )
            //if ($('#divCompanyDirectionPDF :contains(No Record Found)')) {
            //    alert('no record');
            //     t5 = '<br/><br/>';
            //}
            //else {
            //    alert(' record');
            //     t5 = $('#divCompanyDirectionPDF').html();
            //}
            debugger;
            var t5 = '';
            setTimeout(function () {

                html2canvas($("#divCompanyDirectionPDF").get(0), {
                    onrendered: function (canvas) {
                        var imgData = canvas.toDataURL('image/png');
                        ComDirImage = imgData;
                    }
                });


                setTimeout(function () {
                    var $divcomDir = $('#graphArea').clone();
                    $divcomDir.empty();

                    var img1 = $('<img>'); //Equivalent: $(document.createElement('img'))
                    img1.attr('src', ComDirImage);
                    img1.attr('width', "550px");
                    img1.appendTo($divcomDir);
                    var t5 = "<div style='margin-left: 35px;'>" + $divcomDir.html() + "</div>";



                    // var finalData = t1 + "<br/>" + t2 + "<br/>" + t3 + "<br/>" + t4 + "<br/>" + t5;
                    //var $divMy4 = $('#graphArea').clone();
                    //$divMy4.empty();
                    //$divMy4.append(finalData);
                    //$divMy4.find('.HideControl').remove();
                    //finalData = $divMy4.html();

                    var finalData = t1;
                    var tt = JSON.stringify(finalData);
                    var t432 = JSON.stringify(t4);

                    var mnam = $('#ContentPlaceHolder1_lbl').text();
                    var mnam1 = $('#ContentPlaceHolder1_lbl2').text();
                    $.ajax({
                        type: "POST",
                        cache: false,
                        async: false,
                        url: "PersonalDevelopmentPlan.aspx/SaveIntroManagementDataPDF",
                        data: '{ t1:"' + tt + '",t2:"' + mnam + '"}',
                        //data: '{ t1:' + tt + ',metTitle:' + $('#ContentPlaceHolder1_lbl').text() + ',docTitle:' + $('#ContentPlaceHolder1_lbl2').text() + ',approvalData:' + t432 + '}',
                        // data: '{ t1:' + tt + ',metTitle:' + mnam + '}',
                        //data: '{ t1:' + tt + '}',
                        //data: '{ t1:' + t1 + ',t2:' + '' + ',t3:' + '' + ',t4:' + '' + ',t5:' + '' + ',styleData:' + '' + '}',

                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        beforeSend: function () {
                            ajaxindicatorstart('loading...');
                        },
                        success: function (response) {
                            var str = response.d;
                            window.open(str);
                        },
                        failure: function (msg) {
                            alert('error');
                        },
                        complete: function () {
                            ajaxindicatorstop();
                        }
                    });
                }, 1000);
            }, 1500);




        }
        function fnPdfHtmlData_old() {
            generate("warning", "Please wait, we are generating pdf, it will take some time.", "bottomCenter");


            var t1_3 = '';
            var t1_4 = '';
            var t2_4 = '';
            ///////////////////////////////////tab-1
            if ($('#divCompanyDirectionPDF :contains(No Record Found)')) {
                var t3 = '<br/><br/>';
            }
            else {
                var t3 = $('#divCompanyDirectionPDF').html();
            }
            var t4 = $('#divMeetingStatus').html();

            var t444 = $('#meetingDataList').html();

            t4 = t4 + "<br/>" + t444;

            var t1 = "<div style='font-size: 23px;font-family: Verdana, Arial, Helvetica, sans-serif;color: #5781bd;'>" + $('#divPersonalData').html() + "</div>";

            var t1IntroData = $('#tab1IntroData').html();

            var t2 = $('#divEmpDesc').html();

            var finalData = t1 + "<br/>" + t1IntroData + "<br/><br/>" + t2 + "<br/>" + t3 + "<br/>" + t4;

            var tt = JSON.stringify(finalData);
            ///////////////////////////////////

            ///////////////////////////////////tab-2
            var t1_2 = JSON.stringify($('#QuesDataHtml1').html());
            var tmpt5 = $('#QuesDataHtml5').html();
            var tmpt55 = $('#QuesDataHtml3').html();
            var tm = tmpt5 + tmpt55;
            var t5_2 = JSON.stringify(tm);

            var tmpt6 = '';
            var tmpt66 = '';

            // if ($('#ContentPlaceHolder1_hdnIsDisplayInReport').val() == "True") {
            tmpt6 = $('#QuesDataHtml6').html();
            tmpt66 = $('#ContentPlaceHolder1_txtPersonalComment').html();
            //  }
            var t6m = tmpt6 + "<br/><br/>" + tmpt66;
            var t6_2 = JSON.stringify(t6m);
            ///////////////////////////////////

            ///////////////////////////////////tab-3
            setTimeout(function () {

                DisplayTab(2);
                setTimeout(function () {
                    var $divMy2 = $('#graphArea').clone();
                    $divMy2.empty();
                    var img1 = $('<img>'); //Equivalent: $(document.createElement('img'))
                    img1.attr('src', pointTabImage);
                    img1.attr('width', "500px");
                    img1.appendTo($divMy2);
                    var htmlTab2Datapoint = $divMy2.html();

                    t1_3 = JSON.stringify(htmlTab2Datapoint);
                    setTimeout(function () {

                        DisplayTab(3);
                        setTimeout(function () {
                            var $divMy22 = $('#graphArea').clone();
                            $divMy22.empty();
                            var img12 = $('<img>'); //Equivalent: $(document.createElement('img'))
                            img12.attr('src', competenceTabImage);
                            img12.attr('width', "1000px");
                            img12.appendTo($divMy22);
                            var htmlTab2Datacomp = $divMy22.html();

                            t1_4 = JSON.stringify(htmlTab2Datacomp);
                            t2_4 = JSON.stringify($('#ContentPlaceHolder1_lblcompetenceDevelopment').html());


                            setTimeout(function () {

                                $.ajax({
                                    type: "POST",
                                    cache: false,
                                    async: true,
                                    url: "PersonalDevelopmentPlan.aspx/SaveHtmlDataAsPDF",
                                    data: '{ t1:' + tt + ',t2_1:' + t1_2 + ',t2_2:' + t5_2 + ',t2_3:' + t6_2 + ',t3:' + t1_3 + ',t4_1:' + t1_4 + ',t4_2:' + t2_4 + '}',
                                    //data: '{ t1:' + t1 + ',t2:' + '' + ',t3:' + '' + ',t4:' + '' + ',t5:' + '' + ',styleData:' + '' + '}',
                                    contentType: "application/json; charset=utf-8",
                                    dataType: "json",
                                    beforeSend: function () {
                                        ajaxindicatorstart('loading...');
                                    },
                                    success: function (response) {
                                        // alert('response');
                                        $(".tab").fadeIn();
                                        $(".loaderDiv").fadeOut();//show
                                        var str = response.d;
                                        //  alert(str);


                                        window.open(str);
                                        $("#cmt").hide();
                                        $("#tab-1").hide();

                                    },
                                    failure: function (msg) {
                                        alert('error');
                                    },
                                    complete: function () {
                                        ajaxindicatorstop();
                                        DisplayTab(1);
                                    }
                                });
                            }, 1000);
                        }, 1000);
                    }, 1500);
                }, 2500);
            }, 1000);
            ///////////////////////////////////

            ///////////////////////////////////tab-4

            ///////////////////////////////////

            ///////////////////////////////////tab-5
            //var $divMy2 = $('#graphArea').clone();
            //$divMy2.empty();
            //var img1 = $('<img>'); //Equivalent: $(document.createElement('img'))
            //img1.attr('src', activityTabImage);
            //img1.attr('width', "1000px");
            //img1.appendTo($divMy2);
            //var htmlTab2Dataact = $divMy2.html();

            //var t1 = JSON.stringify(htmlTab2Dataact);
            ///////////////////////////////////

        }
        function fnPdfHtmlData_old() {
            generate("warning", "Please wait, we are generating pdf, it will take some time.", "bottomCenter");

            ///Tab-5 mate
            setTimeout(function () {
                GetCommentImg();
            }, 3500); // 35000
            /////////

            var umId = 0;
            var urlParms = new URLSearchParams(window.location.search); //specfic text
            umId = urlParms.get('umId');

            // alert(urlParms);
            ///Tab-2
            DisplayTab(2);
            //GetTab2Image();
            setTimeout(function () { GetTab2Image(); }, 1000);

            /////


            $(".reply-sec").hide();
            // alert('hi');

            //DrawChartAlpPLpPDP(umId);

            //Tab-1
            if (Tab1 == "") {
                setTimeout(function () { GetCommentImgtab1(); }, 10000);
            }
            //

            var $divMy3 = $('#tab3Data').clone();
            var htmlD = '<div class="col-md-10" style="border: black 1px;border-style: groove;"><h2>' + $('#ContentPlaceHolder1_Label1').html() + '</h2><br/>' + $('#TextArea3').html() + '</div>';
            htmlD = htmlD + '<br/><div class="col-md-10" style="border: black 1px;border-style: groove;"><h2>' + $('#ContentPlaceHolder1_Label2').html() + '</h2><br/>' + $('#TextArea4').html() + '</div>';

            // alert('htmlD');

            $divMy3.find('#DescData').remove();       // 
            var htmlTab3Data = $divMy3.html().trim();
            htmlTab3Data = htmlTab3Data + htmlD;

            //alert('htmlTab3Data');



            setTimeout(function () {

                var $divMy2 = $('#graphArea').clone();
                $divMy2.empty();

                /*  var img = $('<img>'); //Equivalent: $(document.createElement('img'))
                  img.attr('src', PDPImgBase64);
                  img.appendTo($divMy2);*/




                // var htmlTab2Data = $('#tab2Data').html().trim();


                /*  var htmlTab2Data = '<h3 class="custom-heading darkblue"> ALP &amp; PLP Points Chart </h3>' + $divMy2.html();
                  $divMy2.empty();
                  var img1 = $('<img>'); //Equivalent: $(document.createElement('img'))
                  img1.attr('src', alpImgBase64);
                  img1.appendTo($divMy2);
                  htmlTab2Data = htmlTab2Data + '<h3 class="custom-heading darkblue">PDP Chart</h3>' + $divMy2.html();*/

                var styleData = JSON.stringify($('#styleData').html().trim());


                //alert('styleData');

                var $div = $('#tab-1').clone();
                $div.empty();
                var img7 = $('<img>'); //Equivalent: $(document.createElement('img'))
                // alert(Tab1);
                img7.attr('src', Tab1);
                img7.attr('width', "1000px");
                img7.appendTo($div);

                //  alert('img7');




                $divMy2.empty();
                var img1 = $('<img>'); //Equivalent: $(document.createElement('img'))
                img1.attr('src', tab2Image);
                img1.attr('width', "1000px");
                img1.appendTo($divMy2);
                htmlTab2Data = $divMy2.html();

                // alert('htmlTab2Data');

                var t1 = JSON.stringify($div.html().trim());
                var t2 = JSON.stringify(htmlTab2Data);
                var t3 = JSON.stringify(htmlTab3Data);
                var t4 = JSON.stringify($('#tab-4').html().trim());

                //alert('t4');

                var img2 = $('<img>'); //Equivalent: $(document.createElement('img'))
                img2.attr('src', CmtimgData);
                img2.attr('width', "1000px");
                img2.attr('style', "margin:0px 0px 100px 0px;");
                img2.appendTo($('#cmt'));


                var t5 = JSON.stringify($('#cmt').html().trim());

                //alert('t5');

                if (alpImgBase64 == null) {
                    alpImgBase64 = "";
                }


                $(this).after("<img src='/starteku/organisation/img/loading.gif' alt='loading' />").fadeIn();
                $(".tab").fadeOut();
                $(".loaderDiv").fadeIn();//show
                var incrementCount = 0;
                setInterval(function () {

                    incrementCount = 1 + incrementCount;
                    $(".progress-bar").css("width", incrementCount + "%");

                }, 1000); // 1000 means 1 sec 1 sec na icrement ma progressbar agad vadhe

                //alert('incrementCount : ' + incrementCount);
                debugger;
                $.ajax({
                    type: "POST",
                    cache: false,
                    async: true,
                    url: "PersonalDevelopmentPlan.aspx/SaveHtmlDataAsPDF",
                    data: '{ t1:' + t1 + ',t2:' + t2 + ',t3:' + t3 + ',t4:' + t4 + ',t5:' + t5 + ',styleData:' + styleData + '}',
                    //data: '{ t1:' + t1 + ',t2:' + '' + ',t3:' + '' + ',t4:' + '' + ',t5:' + '' + ',styleData:' + '' + '}',
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",

                    success: function (response) {
                        // alert('response');
                        $(".tab").fadeIn();
                        $(".loaderDiv").fadeOut();//show
                        var str = response.d;
                        //  alert(str);


                        window.open(str);
                        $("#cmt").hide();
                        $("#tab-1").hide();

                    },
                    failure: function (msg) {
                        alert('error');
                    }
                });
            }, 50000);
        }

        function ChangeDropdownSelectionAfteApproveByEmp() {
            var para = GetQueryParameterByName("sel", window.location.href);
            if (para == ('true' || 'true#')) {
                $('#ContentPlaceHolder1_ddActCate')[0].selectedIndex = 1;

                dateget(null);
            }
        }
    </script>
    <%--  <script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>--%>
    <script type="text/javascript" src="js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="js/dataTables.responsive.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function () {
            $(".gvGrid_ActivityStatus").dataTable({
                //"paging": false,
                "pageLength": 5000,
                responsive: true
            });




            //$('.dataTables_length').find('label').html($('.dataTables_length').find('label').html().replace('Show', $('#ContentPlaceHolder1_hdnShow').val()));
            //$('.dataTables_filter').find('label').html($('.dataTables_filter').find('label').html().replace('Search', $('#ContentPlaceHolder1_hdnSearch').val()));
            //$('.dataTables_length').find('label').html($('.dataTables_length').find('label').html().replace('entries', $('#ContentPlaceHolder1_hdnentries').val()));

            //$('.dataTables_info').html($('.dataTables_info').html().replace('Showing', $('#ContentPlaceHolder1_hdnShowing').val()));
            //$('.dataTables_info').html($('.dataTables_info').html().replace('to', $('#ContentPlaceHolder1_hdnto').val()));
            //$('.dataTables_info').html($('.dataTables_info').html().replace('of', $('#ContentPlaceHolder1_hdnof').val()));
            //$('.dataTables_info').html($('.dataTables_info').html().replace('entries', $('#ContentPlaceHolder1_hdnentrie').val()));


            //$('.paginate_button.previous').html($('.paginate_button.previous').html().replace('Previous', $('#ContentPlaceHolder1_hdnPrevious').val()));
            //$('.paginate_button.next').html($('.paginate_button.next').html().replace('Next', $('#ContentPlaceHolder1_hdnNext').val()));

        });
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            setTimeout(function () {
                SetDatePicker();
            }, 1500);

            var yesterday = new Date((new Date()).valueOf() - 1000 * 60 * 60 * 24);

            $(".txtActStartDate").pickadate({
                format: datePickerFormate, disable: [
                    { from: [0, 0, 0], to: yesterday }
                ]
            });


            $(".txtActEndDate").pickadate({
                format: datePickerFormate, disable: [
                    { from: [0, 0, 0], to: yesterday }
                ]
            });


            $("#txtdate").pickadate({
                format: datePickerFormate
            });

        });
        var datePickerFormate = 'dd/mm/yyyy';
        function SetDatePicker() {

            var yesterday = new Date((new Date()).valueOf() - 1000 * 60 * 60 * 24);

            $(".txtActStartDate").pickadate({
                format: datePickerFormate, disable: [
                    { from: [0, 0, 0], to: yesterday }
                ]
            });


            $(".txtActEndDate").pickadate({
                format: datePickerFormate, disable: [
                    { from: [0, 0, 0], to: yesterday }
                ]
            });


            $(".datepicker1").pickadate({
                format: datePickerFormate
            });

            ///


            $('.txtActStartDate').on('change', function () {
                $('.txtActEndDate').pickadate('picker').set('min', $(this).val());
            });
        }

        function parseJsonDate(jsonDateString) {


            //return new Date(parseInt(jsonDateString.replace('/Date(', '')));

            var formattedDate = new Date(parseInt(jsonDateString.substr(6)));
            var d = formattedDate.getDate();
            var m = formattedDate.getMonth();
            m += 1; // JavaScript months are 0-11
            var y = formattedDate.getFullYear();
            var min = formattedDate.getMinutes();
            var hour = formattedDate.getHours();
            var sec = formattedDate.getSeconds();

            //$("#txtDate").val(d + "." + m + "." + y);
            return d + "/" + m + "/" + y + " " + hour + ":" + min + ":" + sec;
        }

        function formatDate(d) {
            if (hasTime(d)) {
                var s = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
                s += ' ' + d.getHours() + ':' + zeroFill(d.getMinutes()) + ':' + zeroFill(d.getSeconds());
            } else {
                var s = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
            }

            return s;
        }
        function openPDF(url) {

            url = url.split("?")[0];
            var w = window.open(url, '_blank');
            w.focus();
        }
//        function getFileData(myFile) {
//            var file = myFile.files[0];
//            var filename = file.name;
        //        }


    </script>
    <script type="text/javascript">
        function getFileData(a) {

            //alert('hi');
            debugger;
           
            $('#ContentPlaceHolder1_Label23').text(a.value);

}

</script>
</asp:Content>

