﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master" AutoEventWireup="true" CodeFile="Setting.aspx.cs" Inherits="Organisation_Setting" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
       <link href="../assets/assets/css/plugins/jquery-ui.css" rel="stylesheet" />

    <script src="../assets/assets/js/libs/jquery-1.10.2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/themes/start/jquery-ui.css" />
    <script src="../plugins/bootstrap-inputmask/jquery.inputmask.min.js" type="text/javascript"></script>

    <link href="../Scripts/fullcalendor/dATEPICKER/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <script src="../Scripts/fullcalendor/dATEPICKER/bootstrap-datetimepicker.min.js"></script>
    <style type="text/css">
        .setting_table {
            color: #0B5D99;
            font-size: 16px;
            text-transform: uppercase;
        }

            .setting_table label {
                margin-left: 10px;
                font-weight: 100;
            }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="col-md-6">
        <div class="heading-sec">
            <h1><%--<%= CommonMessages.Settings%>--%> <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Settings" enableviewstate="false"/> <i><span runat="server" id="Settings"></span></i></h1>
        </div>
    </div>
    <div class="col-md-12">
        <ul class="your-message" style="margin-top: 30px;">
            <li style="margin-bottom: 10px; background: #fff; padding: 20px 0px;">
                <div class="setting_lable col-md-9">
                    <span class="col-md-3" style=" width: 100%;"><%--<%= CommonMessages.NewfileUploadwarning%> --%><asp:Literal ID="Literal2" runat="server" meta:resourcekey="NewfileUploadwarning" enableviewstate="false"/></span>
                    <div class="col-md-8"><%--Lorem ipsum dolor sit amet, conse adipiscing. dolor sit amet, conse adip lorem ipsum dolor sit amet--%></div>
                </div>

                <div class="col-md-3">
                    <asp:RadioButtonList ID="rl_Newfile" runat="server" class="setting_table" 
                        RepeatDirection="Horizontal" meta:resourcekey="rl_NewfileResource1">
                        <asp:ListItem Value="ON" Selected="True" meta:resourcekey="ListItemResource1"></asp:ListItem>
                        <asp:ListItem Value="OFF" meta:resourcekey="ListItemResource2"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </li>
            <li style="margin-bottom: 10px; background: #fff; padding: 20px 0px;">
                <div class="setting_lable col-md-9">
                   <%-- <span class="col-md-3"><%= CommonMessages.Competencelevelschange%></span>--%>
                    <span class="col-md-3" style=" width: 100%;"> <%--<%= CommonMessages.CompetenceNotification%>--%>  <asp:Literal ID="Literal3" runat="server" meta:resourcekey="CompetenceNotification" enableviewstate="false"/></span>
                    <div class="col-md-8"><%--Lorem ipsum dolor sit amet, conse adipiscing. dolor sit amet, conse adip lorem ipsum dolor sit amet--%></div>
                </div>

                <div class="col-md-3">
                    <asp:RadioButtonList ID="rl_levels_Change" runat="server" class="setting_table" 
                        RepeatDirection="Horizontal" meta:resourcekey="rl_levels_ChangeResource1">
                        <asp:ListItem Value="ON" Selected="True" meta:resourcekey="ListItemResource3"></asp:ListItem>
                        <asp:ListItem Value="OFF" meta:resourcekey="ListItemResource4"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </li>

            <li style="margin-bottom: 10px; background: #fff; padding: 20px 0px;">
                <div class="setting_lable col-md-9">
                    <span class="col-md-3" style=" width: 100%;"><%--<%= CommonMessages.Knowledgesharingrequests%> --%><asp:Literal ID="Literal4" runat="server" meta:resourcekey="Knowledgesharingrequests1" enableviewstate="false"/></span>
                    <div class="col-md-8"><%--Lorem ipsum dolor sit amet, conse adipiscing. dolor sit amet, conse adip lorem ipsum dolor sit amet--%></div>
                </div>

                <div class="col-md-3">
                    <asp:RadioButtonList ID="rl_Knowledge_sharing" runat="server" 
                        class="setting_table" RepeatDirection="Horizontal" 
                        meta:resourcekey="rl_Knowledge_sharingResource1">
                        <asp:ListItem Value="ON" Selected="True" meta:resourcekey="ListItemResource5"></asp:ListItem>
                        <asp:ListItem Value="OFF" meta:resourcekey="ListItemResource6"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </li>
             <li style="margin-bottom: 10px; background: #fff; padding: 20px 0px;">
                <div class="setting_lable col-md-9">
                   <%-- <span class="col-md-3"><%= CommonMessages.Knowledgesharingrequests%></span>--%>
                    <span class="col-md-3" style=" width: 100%;"> <%--Invitation <%= CommonMessages.Notification%> --%><asp:Literal ID="Literal5" runat="server" meta:resourcekey="Notification" enableviewstate="false"/></span>
                    <div class="col-md-8"><%--Lorem ipsum dolor sit amet, conse adipiscing. dolor sit amet, conse adip lorem ipsum dolor sit amet--%></div>
                </div>

                <div class="col-md-3">
                    <asp:RadioButtonList ID="rl_Invitation_Notification" runat="server" 
                        class="setting_table" RepeatDirection="Horizontal">
                        <asp:ListItem Value="ON" Selected="True" meta:resourcekey="ListItemResource5"></asp:ListItem>
                        <asp:ListItem Value="OFF" meta:resourcekey="ListItemResource6"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </li>
            <li style="margin-bottom: 10px; background: #fff; padding: 20px 0px;">
                <div class="setting_lable col-md-9">
                   <%-- <span class="col-md-3"><%= CommonMessages.Knowledgesharingrequests%></span>--%>
                    <span class="col-md-3" style=" width: 100%;"> <%--<%= CommonMessages.MessageNotification%>--%> <asp:Literal ID="Literal6" runat="server" meta:resourcekey="MessageNotification" enableviewstate="false"/></span>
                    <div class="col-md-8"><%--Lorem ipsum dolor sit amet, conse adipiscing. dolor sit amet, conse adip lorem ipsum dolor sit amet--%></div>
                </div>

                <div class="col-md-3">
                    <asp:RadioButtonList ID="rl_Message_Notification" runat="server" 
                        class="setting_table" RepeatDirection="Horizontal">
                        <asp:ListItem Value="ON" Selected="True" meta:resourcekey="ListItemResource5"></asp:ListItem>
                        <asp:ListItem Value="OFF" meta:resourcekey="ListItemResource6"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </li>
            
             <li style="margin-bottom: 10px; background: #fff; padding: 20px 0px;">
                <div class="setting_lable col-md-9">
                   <%-- <span class="col-md-3"><%= CommonMessages.Knowledgesharingrequests%></span>--%>
                    <span class="col-md-3" style=" width: 100%;"> <%--<%= CommonMessages.MessageNotification%>--%> 
                        <asp:Literal ID="Literal7" runat="server" meta:resourcekey="ActivityNotification" enableviewstate="false"/></span>
                    <div class="col-md-8"><%--Lorem ipsum dolor sit amet, conse adipiscing. dolor sit amet, conse adip lorem ipsum dolor sit amet--%></div>
                </div>

                <div class="col-md-3">
                    <asp:RadioButtonList ID="rl_Activity_Notificatoins" runat="server" 
                        class="setting_table" RepeatDirection="Horizontal">
                        <asp:ListItem Value="ON" Selected="True" meta:resourcekey="ListItemResource5"></asp:ListItem>
                        <asp:ListItem Value="OFF" meta:resourcekey="ListItemResource6"></asp:ListItem>
                    </asp:RadioButtonList>
                </div>
            </li>
            <div class="modal-footer" style="border: 0px;">
                <asp:Button runat="server" ID="btn_save_change" class="btn btn-primary yellow" 
                    Text="Save Changes" OnClick="btn_save_change_Click" 
                    meta:resourcekey="btn_save_changeResource1"></asp:Button>
                 <asp:Button runat="server" ID="btnCancel" class="btn btn-default black" 
                    Text="Cancel" OnClick="btnCancel_Click" meta:resourcekey="btnCancelResource1"></asp:Button>
               <%-- <button data-dismiss="modal" class="btn btn-default black" type="button">Cancel  </button>--%>
            </div>
        </ul>
    </div>

    <!-- TIME LINE -->

    <!-- Recent Post -->

    <div class="col-md-3"></div>
    <!-- Twitter Widget -->
    <!-- Weather Widget -->

    <script>
        SetExpandCollapse();
        </script>

</asp:Content>

