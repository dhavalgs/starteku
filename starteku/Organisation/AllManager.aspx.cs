﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using startetku.Business.Logic;
using starteku_BusinessLogic.Model;

public partial class Organisation_AllManager : System.Web.UI.Page
{


    #region Page Event

    protected void Page_Load(object sender, EventArgs e)
    {
       

        Page.ClientScript.RegisterStartupScript(Page.GetType(), "my", " SetExpandCollapse();", true);

        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        
        Session["OriginPage"] = "AllManager.aspx";
        if (!IsPostBack)
        {
            var db = new startetkuEntities1();
            var userId = Convert.ToInt32(Session["OrgUserId"]);
            var userCompanyID = Convert.ToInt32(Session["OrgUserId"]);

            var EnaData = db.GetItemsEnableList(userId, userCompanyID).FirstOrDefault();

            hdnCurrentLanguage.Value = Convert.ToString(Session["Language"]);

            Session["Aemcompid"] = EnaData.Aemcompid;
            Session["Cemcompid"] = EnaData.comdirCompID;
            Session["Compcompid"] = EnaData.compCompID;
            Session["Pointcompid"] = EnaData.pointCompID;
            Session["Quescompid"] = EnaData.quesemID;

            if (string.IsNullOrWhiteSpace(Convert.ToString(Session["Pointcompid"])))
            {
                pointPanel.Visible = false;
            }

            GetCheckBoxListData();
            GetAllJobtype();
            GetAllDepartments();
            GetAllTeam();

            ResourceLanguageBM obj = new ResourceLanguageBM();
            DataSet resds = obj.GetAllResourceLanguage();
            //ddlLanguage.DataSource = resds; 
            //ddlLanguage.DataTextField = "resLanguage";
            //ddlLanguage.DataValueField = "resCulture";
            //ddlLanguage.DataBind();

            //ddlLanguage.Items.Insert(0, new ListItem(hdnSelectLanguage.Value, CommonModule.dropDownZeroValue));

            //ddlLanguage.Items.FindByText("English").Selected = true;


            ddlLanguage1.DataSource = resds;
            ddlLanguage1.DataTextField = "resLanguage";
            ddlLanguage1.DataValueField = "resCulture";
            ddlLanguage1.DataBind();
            ddlLanguage1.Items.Insert(0, new ListItem(hdnSelectLanguage.Value, CommonModule.dropDownZeroValue));
           // ddlLanguage1.Items.FindByText("English").Selected = true;
            ddlLanguage1.Items.FindByText(Convert.ToString(Session["Culture"])).Selected = true;


            ddlgender.Items.Insert(0, new ListItem(hdnSelectGender.Value, CommonModule.dropDownZeroValue));


            // Employee.InnerHtml = CommonMessages.Welcome + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            Employee.InnerHtml = "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            GetAllEmployeeList();
            //EmployeeListArchiveAll();
            SetDefaultMessage();
           
        }
        if (string.IsNullOrWhiteSpace(Convert.ToString(Session["Aemcompid"])))
        {

            btnActOvp.Visible = false;
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region DropDown Event
    protected void ddlcompetence_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetCheckBoxListData();
    }
    protected void ddljobtype_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetCheckBoxListData();

    }
    #endregion

    #region method

    protected void txtEmail_TextChanged(object sender, EventArgs e)
    {
        UserBM obj2 = new UserBM();
        obj2.useremailCheckDuplication(txtEmail.Text, -1);
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        { lblemailvalid.Text = ""; }
        else
        {
            if (returnMsg == "userEmail")
            {
                lblemailvalid.Text = CommonModule.msgEmailAlreadyExists;
            }

        }
    }
    protected void GetCheckBoxListData()
    {
        CompetenceMasterBM obj = new CompetenceMasterBM();
        obj.catIsActive = true;
        obj.catIsDeleted = false;
        if (string.IsNullOrEmpty(ddlDepartment.SelectedValue))
        {
            NoRecords.Visible = true;
            return;

        }
        obj.comDivId = Convert.ToInt32(ddlDepartment.SelectedValue);
        obj.comJobtype = Convert.ToInt32(ddljobtype.SelectedValue);
        obj.GetAllCategoryByJobTypeId_DivId();
        // lblDivIdName.Text = ddlDepartment.SelectedItem.Text;

        DataSet ds = obj.ds;

        if (ds != null)
        {


            if (ds.Tables[0].Rows.Count > 0)
            {
                repCompCat.DataSource = ds.Tables[0];
                //chkList.DataTextField = "catName";
                //chkList.DataValueField = "catId";
                repCompCat.DataBind();
                //chkList.SelectedIndex = 0;
                NoRecords.Visible = false;

                //foreach (ListItem li in chkList.Items)
                //{
                //    li.Selected = true;
                //}
            }
            else
            {
                NoRecords.Visible = true;
                //chkList.Items.Clear();
                //ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownCategory, CommonModule.dropDownZeroValue));
            }

        }
        else
        {
            NoRecords.Visible = true;
            //chkList.Items.Clear();
        }
    }

    protected void SetDefaultMessage()
    {
        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["msg"])))
        {
            if (Convert.ToString(Request.QueryString["msg"]) == "del")
            {
                lblMsg.Text = CommonModule.msgRecordHasBeenDeletedSuccessfully;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "ins")
            {
                lblMsg.Text = CommonModule.msgRecordInsertedSuccessfully;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "upd")
            {
                lblMsg.Text = CommonModule.msgRecordUpdatedSuccss;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "arc")
            {
                lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
        }

    }
    protected void GetAllEmployeeList()
    {
        //Session["UserId"]
        //if (!String.IsNullOrEmpty(Request.QueryString["org"]))

        UserBM obj = new UserBM();
        obj.userIsActive = true;
        obj.userIsDeleted = false;
        obj.userType = 2;
        //obj.userCompanyId = Convert.ToInt32(Session["UserId"]);
        obj.userCompanyId = Convert.ToInt32(Session["OrgUserId"]);
        //obj.GetAllEmployee();
        obj.userCreateBy = Convert.ToString(Session["OrgUserId"]);
        obj.GetAllEmployeecreatebymanager();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvGrid.DataSource = ds.Tables[0];
                gvGrid.DataBind();

                gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvGrid.DataSource = null;
                gvGrid.DataBind();
            }
        }
        else
        {
            gvGrid.DataSource = null;
            gvGrid.DataBind();
        }
    }
    //protected void EmployeeListArchiveAll()
    //{
    //    UserBM obj = new UserBM();
    //    obj.userIsActive = false;
    //    obj.userIsDeleted = false;
    //    obj.userType = 3;
    //    obj.userCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
    //    obj.userCreateBy = Convert.ToString(Session["OrgUserId"]);
    //    obj.GetAllEmployeecreatebymanager();
    //   // obj.GetAllEmployee();
    //    DataSet ds = obj.ds;

    //    if (ds != null)
    //    {
    //        if (ds.Tables[0].Rows.Count > 0)
    //        {
    //            gvArchive.DataSource = ds.Tables[0];
    //            gvArchive.DataBind();

    //            gvArchive.HeaderRow.TableSection = TableRowSection.TableHeader;
    //        }
    //        else
    //        {
    //            gvArchive.DataSource = null;
    //            gvArchive.DataBind();
    //        }
    //    }
    //    else
    //    {
    //        gvArchive.DataSource = null;
    //        gvArchive.DataBind();
    //    }
    //}
    protected void GetAllJobtype()
    {
        JobTypeBM obj = new JobTypeBM();
        obj.jobIsActive = true;
        obj.jobIsDeleted = false;
        obj.jobCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllJobType();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (Convert.ToString(Session["Culture"]) == "Danish")
                {

                    ddljobtype.Items.Clear();

                    ddljobtype.DataSource = ds.Tables[0];
                    ddljobtype.DataTextField = "jobNameDN";
                    ddljobtype.DataValueField = "jobId";
                    ddljobtype.DataBind();

                    ddljobtype.Items.Insert(0, new ListItem(hdnSelectJob.Value, CommonModule.dropDownZeroValue));
                }

                else
                {
                    ddljobtype.Items.Clear();

                    ddljobtype.DataSource = ds.Tables[0];
                    ddljobtype.DataTextField = "jobName";
                    ddljobtype.DataValueField = "jobId";
                    ddljobtype.DataBind();

                    ddljobtype.Items.Insert(0, new ListItem(hdnSelectJob.Value, CommonModule.dropDownZeroValue));

                }
            }
            else
            {
                ddljobtype.Items.Clear();
                ddljobtype.Items.Insert(0, new ListItem(hdnSelectJob.Value, CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddljobtype.Items.Clear();
            ddljobtype.Items.Insert(0, new ListItem(hdnSelectJob.Value, CommonModule.dropDownZeroValue));
        }

    }
    protected void GetAllTeam()
    {
        JobTypeBM obj = new JobTypeBM();
        obj.TeamIDs = "0";
        obj.jobIsActive = true;
        obj.jobIsDeleted = false;
        obj.jobCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllTeam();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (Convert.ToString(Session["Culture"]) == "Danish")
                {

                    ddlTeam.Items.Clear();

                    ddlTeam.DataSource = ds.Tables[0];
                    ddlTeam.DataTextField = "TeamNameDN";
                    ddlTeam.DataValueField = "TeamID";
                    ddlTeam.DataBind();

                    ddlTeam.Items.Insert(0, new ListItem(hdnSelectTeam.Value, CommonModule.dropDownZeroValue));
                }

                else
                {
                    ddlTeam.Items.Clear();

                    ddlTeam.DataSource = ds.Tables[0];
                    ddlTeam.DataTextField = "TeamName";
                    ddlTeam.DataValueField = "TeamID";
                    ddlTeam.DataBind();

                    ddlTeam.Items.Insert(0, new ListItem(hdnSelectTeam.Value, CommonModule.dropDownZeroValue));

                }
            }
            else
            {
                ddlTeam.Items.Clear();
                ddlTeam.Items.Insert(0, new ListItem(hdnSelectTeam.Value, CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddlTeam.Items.Clear();
            ddlTeam.Items.Insert(0, new ListItem(hdnSelectTeam.Value, CommonModule.dropDownZeroValue));
        }

    }
    protected void GetAllDepartments()
    {
        //DepartmentsBM obj = new DepartmentsBM();
        //obj.depIsActive = true;
        //obj.depIsDeleted = false;
        //obj.depCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        //obj.GetAllDepartments();
        //DataSet ds = obj.ds;

        DivisionBM obj = new DivisionBM();
        obj.depIsActive = true;
        obj.depIsDeleted = false;
        obj.depCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllDivision();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {

                if (Convert.ToString(Session["Culture"]) == "Danish")
                {

                    ddlDepartment.Items.Clear();

                    ddlDepartment.DataSource = ds.Tables[0];
                    ddlDepartment.DataTextField = "divNameDN";
                    ddlDepartment.DataValueField = "divId";
                    ddlDepartment.DataBind();

                    ddlDepartment.Items.Insert(0, new ListItem(hdnSelectDivisions.Value, CommonModule.dropDownZeroValue));
                }

                else
                {
                    ddlDepartment.DataSource = ds.Tables[0];
                    ddlDepartment.DataTextField = "divName";
                    ddlDepartment.DataValueField = "divId";
                    ddlDepartment.DataBind();

                    ddlDepartment.Items.Insert(0, new ListItem(hdnSelectDivisions.Value, CommonModule.dropDownZeroValue));
                }
            }
            else
            {
                ddlDepartment.Items.Clear();
                ddlDepartment.Items.Insert(0, new ListItem(hdnSelectDivisions.Value, CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddlDepartment.Items.Clear();
            ddlDepartment.Items.Insert(0, new ListItem(hdnSelectDivisions.Value, CommonModule.dropDownZeroValue));
        }

    }

   

    protected void Insertuser()
    {
        string filePath = "";
        UserBM obj2 = new UserBM();
        obj2.useremailCheckDuplication(txtEmail.Text, -1);
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        {
            UserBM obj = new UserBM();
            obj.userFirstName = txtName.Text;
            obj.userLastName = txtLastName.Text;
            obj.userZip = "";
            obj.userAddress = txtaddress.Text;
            obj.userCountryId = Convert.ToInt32(0);
            obj.userStateId = Convert.ToInt32(0);
            //obj.userCityId = Convert.ToInt32(0);
            obj.userCityId = "";
            obj.comphone = txtMobile.Text;
            obj.usercontact = txtMobile.Text;
            obj.userEmail = txtEmail.Text;
            obj.userType = 2;

            //if (!String.IsNullOrEmpty(Request.QueryString["org"]))
            //{
            //    obj.userType = 2;
            //}
            //else if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            //{
            //    obj.userType = 3;
            //}
            // obj.userCityId = Convert.ToInt32(ddlcity.SelectedValue);
            obj.userCreatedDate = DateTime.Now;
            obj.userIsActive = true;
            obj.userIsDeleted = false;
            obj.userCompanyId = Convert.ToInt32(Session["OrgUserId"]);
            //if (!String.IsNullOrEmpty(Request.QueryString["org"]))
            //{
            //    obj.userCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            //}
            //else if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            //{
            //    obj.userCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            //}
            string pass = CommonModule.Generate_Random_password();
            obj.userPassword = CommonModule.encrypt(pass.Trim());

            //obj.userDOB = Convert.ToDateTime(CommonModule.FormatdtEnter(txtfromdate.Text));
            obj.userGender = ddlgender.SelectedValue;
            obj.userImage = "ofile_img.png";
            //if (flupload1.HasFile)
            //{
            //    filePath = System.DateTime.Now.ToString("MMddyyhhmmss") + "_" + flupload1.PostedFile.FileName;
            //    flupload1.SaveAs(Server.MapPath("../") + "Log/upload/Userimage/" + System.IO.Path.GetFileName(filePath.Replace(' ', '0')));
            //    obj.userImage = System.IO.Path.GetFileName(filePath.Replace(' ', '0'));

            //}
            //else
            //{
            //    if (ddlgender.SelectedValue == "Male")
            //    {
            //        obj.userImage = "male.png";
            //    }
            //    else
            //    {//ViewState["userImage"]
            //        obj.userImage = "female.png";
            //    }
            //}
            obj.userLevel = 1;
            obj.userdepId = Convert.ToString(ddlDepartment.SelectedValue);
            obj.userJobType = Convert.ToInt32(ddljobtype.SelectedValue);
            obj.userCreateBy = Convert.ToString(Session["OrgUserId"]);



            // Get Cattegory list comma sap list. |Saurin | 20141216 |
            var category = string.Empty;
            var aa = new List<string>();
            foreach (RepeaterItem aItem in repCompCat.Items)
            {
                var repCompAdd = (Repeater)aItem.FindControl("repCompAdd");
                foreach (RepeaterItem bItem in repCompAdd.Items)
                {
                    CheckBoxList chkDisplayTitle = (CheckBoxList)bItem.FindControl("chkList");
                    Label hdnChkboxValue = (Label)bItem.FindControl("hdnChkboxValue");

                    foreach (ListItem item in chkDisplayTitle.Items)
                    {
                        if (item.Selected)
                        {
                            aa.Add(item.Value);
                            //string selectedValue = item.Value;
                        }
                    }


                    //if (!string.IsNullOrWhiteSpace(chkDisplayTitle.SelectedValue))
                    //{

                    //}
                }



            }
            category = string.Join(",", aa.ToArray());
            obj.UserCategory = category;
            obj.TeamID = Convert.ToInt32(ddlTeam.SelectedValue);

            obj.InsertOrganisation();

            int UserId = 0;
            DataSet ds = obj.ds;
            if (ds != null)
            {
                if (ds.Tables.Count > 0)
                {
                    UserId = Convert.ToInt32(ds.Tables[0].Rows[0]["userId"]);
                    Updatelanguagemaster(UserId);

                    if (!string.IsNullOrEmpty(category))
                    {
                        //string[] str = category.Split(',');
                        //if (str.Length > 0)
                        //{
                        //    foreach (var comId in str)
                        //    {
                        //        Int32 CompId = 0;
                        //        if (!string.IsNullOrEmpty(comId))
                        //        {
                        //            CompId = Convert.ToInt32(comId);
                        //        }

                        //        EmployeeSkillBM skillobj = new EmployeeSkillBM();
                        //        skillobj.skillComId = CompId;
                        //        skillobj.skillStatus = 3;
                        //        skillobj.skillUserId = Convert.ToInt32(ds.Tables[0].Rows[0]["userId"]);
                        //        skillobj.skillIsActive = true;
                        //        skillobj.skillIsDeleted = false;
                        //        skillobj.skillCreatedDate = DateTime.Now;
                        //        skillobj.skillCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
                        //        skillobj.skillLocal = 0;
                        //        skillobj.skillAchive = 0;
                        //        skillobj.skilltarget = 0;
                        //        skillobj.skillComment = string.Empty;
                        //        skillobj.InsertSkillMaster();
                        //    }


                        //}


                        var db = new startetkuEntities1();
                        db.InsertIntoSkillMaster(3, Convert.ToInt32(ds.Tables[0].Rows[0]["userId"]), true, false, DateTime.Now, Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]), 0, 0, 0, string.Empty, true, category);



                    }
                }
            }
            if (obj.ReturnBoolean == true)
            {
                sendmail(txtEmail.Text, txtName.Text, pass, UserId);
                clear();
                //if (!String.IsNullOrEmpty(Request.QueryString["org"]))
                //{
                //    string msg = CommonModule.msgSuccessfullyRegistered;
                //    redirectpage(msg, "EmployerList.aspx");
                //}
                //else
                //{
                //    string msg = CommonModule.msgSuccessfullyRegistered;
                //    redirectpage(msg, "EmployeeList.aspx");
                //}

            }
        }
        else
        {
            if (returnMsg == "userEmail")
                lblMsg.Text = CommonModule.msgEmailAlreadyExists;
            else
                lblMsg.Text = CommonModule.msgProblemInsertRecord;
        }
    }

    protected void Updatelanguagemaster(int OrgUserId)
    {
        UserBM obj = new UserBM();
        obj.languageUserId = Convert.ToInt32(OrgUserId);
        String lag = Convert.ToString(ddlLanguage1.SelectedValue);

        obj.languageName = ddlLanguage1.SelectedItem.Text;
        obj.languageCulture = ddlLanguage1.SelectedItem.Value;
        //if (lag == "English")
        //{
        //    obj.languageName = "English";
        //    obj.languageCulture = "en-GB";


        //}
        //else
        //{
        //    obj.languageName = "Danish";
        //    obj.languageCulture = "da-DK";


        //}
        //obj.userGender = ddlgender.SelectedValue;
        obj.Updatelanguagemaster();

    }

    protected void sendmail(string email, string name, string pass, int userId)
    {
        try
        {
            // Common.WriteLog("start");
            //string subject = "Registration";
            string fromid = Convert.ToString(ConfigurationManager.AppSettings["fromid"]);

            //String confirmMail =
            //    CommonModule.getTemplatebyname("Registration", userId);
            Template template = CommonModule.getTemplatebyname1("Registration", userId);
            String confirmMail = template.TemplateName;
            if (!String.IsNullOrEmpty(confirmMail))
            {
                string subject = GetLocalResourceObject("RegistrationSubjectEng.Text").ToString();
                if (template.Language == "Danish")
                {
                    subject = GetLocalResourceObject("RegistrationSubjectDn.Text").ToString();
                }
                string tempString = confirmMail;
                tempString = tempString.Replace("###name###", name);
                tempString = tempString.Replace("###email###", email);
                tempString = tempString.Replace("###password###", pass);
                //SendMail(tempString, txtEmail.Text, txtFname.Text);
                CommonModule.SendMailToUser(email, subject, tempString, fromid, Convert.ToString(HttpContext.Current.Session["OrgCompanyId"]));
            }
        }
        catch (Exception ex)
        {
            Common.WriteLog("error in sendmail" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
        }

    }
    protected void GetAllEmployeebyid(int id)
    {
        UserBM obj = new UserBM();
        obj.userId = Convert.ToInt32(id);
        obj.GetAllEmployeebyid();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userFirstName"])))
                lblName.Text = Convert.ToString(ds.Tables[0].Rows[0]["userFirstName"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userLastName"])))
                lblLastName.Text = Convert.ToString(ds.Tables[0].Rows[0]["userLastName"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userAddress"])))
                lblAddress.Text = Convert.ToString(ds.Tables[0].Rows[0]["userAddress"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["usercontact"])))
                lblPhoneNo.Text = Convert.ToString(ds.Tables[0].Rows[0]["usercontact"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userEmail"])))
                lblEmail.Text = Convert.ToString(ds.Tables[0].Rows[0]["userEmail"]);

            //if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userPassword"])))
            //{
            //    string Password = CommonModule.decrypt(Convert.ToString(ds.Tables[0].Rows[0]["userPassword"]));
            //   // txtPassword.Attributes.Add("value", Password);
            //    lblPassword.Text = Password;
            //}

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userJobType"])))
            {
                try
                {
                    ddljobtype.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["userJobType"]);
                    if (ddljobtype.SelectedValue == "0")
                    {
                        lblJob.Text = "";
                    }
                    else
                    {
                        lblJob.Text = ddljobtype.SelectedItem.Text;
                    }
                    
                }

                catch
                {
                    lblJob.Text = "";
                }
            }


            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userdepId"])))
            {
                try
                {
                    ddlDepartment.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["userdepId"]);
                    if (ddlDepartment.SelectedValue == "0")
                    {
                        lblDepartments.Text = "";
                    }
                    else
                    {
                        lblDepartments.Text = ddlDepartment.SelectedItem.Text;
                    }
                    

                }
                catch
                {
                    lblDepartments.Text = "";
                }
            }

             
        }

    }
    protected void clear()
    {
        txtName.Text = "";
        txtaddress.Text = "";
        txtMobile.Text = "";
        txtEmail.Text = "";
        ddlDepartment.SelectedIndex = 0;
        ddljobtype.SelectedIndex = 0;
        lblMsg.Text = "";
    }

    private DataTable GetAllCompetenceAddbyComCatId(int categoryId)
    {
        CompetenceMasterBM obj = new CompetenceMasterBM();
        obj.catIsActive = true;
        obj.catIsDeleted = false;
        if (categoryId < 0) return null;
        obj.comDivId = Convert.ToInt32(ddlDepartment.SelectedValue);
        obj.comJobtype = Convert.ToInt32(ddljobtype.SelectedValue);
        obj.comCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllCompetenceAddbyComCatId(categoryId);
        // lblDivIdName.Text = ddlDepartment.SelectedItem.Text;

        DataSet ds = obj.ds;

        if (ds != null)
        {
            int selectTable = 0;

            if (ds.Tables[selectTable].Rows.Count > 0)
            {
                return ds.Tables[selectTable];

            }
            else
            {
                //chkList.Items.Clear();
                //ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownCategory, CommonModule.dropDownZeroValue));
            }

        }
        return null;
    }
    #endregion

    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        Insertuser();
        GetAllEmployeeList();
    }
    protected void btnclose_click1(object sender, EventArgs e)
    {
        clear();
    }
    #endregion

    #region grid Event
    protected void gvGrid_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "archive")
        {
            UserBM obj = new UserBM();
            obj.userId = Convert.ToInt32(e.CommandArgument);
            obj.userIsActive = false;
            obj.userIsDeleted = true;
            obj.EmployeeStatusUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    GetAllEmployeeList();
                    //EmployeeListArchiveAll();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
        else if (e.CommandName == "View")
        {
            GetAllEmployeebyid(Convert.ToInt32(e.CommandArgument));

            mpe.Show();

        }
        else if (e.CommandName == "Password")
        {
            GetEmployeePassword(Convert.ToInt32(e.CommandArgument));
            // Togle(false);
            Modal1.Show();
        }
    }
    public void Togle(bool value)
    {
        //lblAddress.Visible = value;
        //lblAddressLABEL.Visible = value;
        //lblEmail.Visible = value;
        //lblEmailLABEL.Visible = value;
        //lblJob.Visible = value;
        //lblJobLABEL.Visible = value;


    }
    protected void GetEmployeePassword(int id)
    {
        UserBM obj = new UserBM();
        obj.userId = Convert.ToInt32(id);
        obj.GetAllEmployeebyid();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            //if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userFirstName"])))
            //    txtName.Text = Convert.ToString(ds.Tables[0].Rows[0]["userFirstName"]);

            //if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userPassword"])))
            //    txtPassword.Text = Convert.ToString(ds.Tables[0].Rows[0]["userPassword"]);
            lblpassword.Text = CommonModule.decrypt(Convert.ToString(ds.Tables[0].Rows[0]["userPassword"]));
            //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>alert('" + ds.Tables[0].Rows[0]["userPassword"] + "')</script>", false);

            //if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userPassword"])))
            //{
            //    string Password = CommonModule.decrypt(Convert.ToString(ds.Tables[0].Rows[0]["userPassword"]));
            //   // txtPassword.Attributes.Add("value", Password);
            //    lblPassword.Text = Password;
            //}


        }

    }

    protected void gvArchive_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //Checking the RowType of the Row  
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Button btn = e.Row.FindControl("lnkBtnName") as Button;
            HiddenField hd_field = e.Row.FindControl("hdnConfirmArchive") as HiddenField;
            btn.Attributes.Add("OnClientClick", "return confirm('" + hd_field.Value + "');");
            //btn.OnClientClick = "return confirm(" + hd_field.Value + ");";

        }
    }

    protected void gvArchive_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "archive")
        {
            UserBM obj = new UserBM();
            obj.userId = Convert.ToInt32(e.CommandArgument);
            obj.userIsActive = true;
            obj.userIsDeleted = false;
            obj.EmployeeStatusUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {

                    GetAllEmployeeList();
                    //EmployeeListArchiveAll();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
        else if (e.CommandName == "permanentlydelete")
        {
            UserBM obj = new UserBM();
            obj.userId = Convert.ToInt32(e.CommandArgument);
            obj.userIsActive = false;
            obj.userIsDeleted = true;
            obj.EmployeeStatusUpdate();


            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    GetAllEmployeeList();
                    //EmployeeListArchiveAll();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }

    }
    protected void OnSelectedIndexChanged(object sender, EventArgs e)
    {
        //lblId.Text = GridView1.SelectedRow.Cells[0].Text;
        //lblName.Text = GridView1.SelectedRow.Cells[1].Text;
        //lblCountry.Text = (GridView1.SelectedRow.FindControl("lblCountry") as Label).Text;
        //mpe.Show();
    }


    protected void gvGrid_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //gvGrid.PageIndex = e.NewPageIndex;
        //GetAllEmployeeList();

        gvGrid.PageIndex = e.NewPageIndex;

        if (Session["SortedView"] != null)
        {
            gvGrid.DataSource = Session["SortedView"];
            gvGrid.DataBind();
        }
        else
        {
            UserBM obj = new UserBM();
            obj.userIsActive = true;
            obj.userIsDeleted = false;
            obj.userType = 2;
            //obj.userCompanyId = Convert.ToInt32(Session["UserId"]);
            obj.userCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            //obj.GetAllEmployee();
            // obj.userCreateBy = Convert.ToString(Session["OrgCompanyId"]);
            obj.GetAllEmployeecreatebymanager();
            DataSet ds = obj.ds;

            //gvGrid.DataSource = BindGridView();
            gvGrid.DataSource = ds;
            gvGrid.DataBind();
        }
    }

    // dharmesh 2015 04 30

    public SortDirection dir
    {
        get
        {
            if (ViewState["dirState"] == null)
            {
                ViewState["dirState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["dirState"];
        }
        set
        {
            ViewState["dirState"] = value;
        }

    }

    protected void gvGrid_Sorting(object sender, GridViewSortEventArgs e)
    {
        string sortingDirection = string.Empty;
        if (direction == SortDirection.Ascending)
        {
            direction = SortDirection.Descending;
            sortingDirection = "Desc";

        }
        else
        {
            direction = SortDirection.Ascending;
            sortingDirection = "Asc";

        }
        UserBM obj = new UserBM();
        obj.userIsActive = true;
        obj.userIsDeleted = false;
        obj.userType = 2;
        //obj.userCompanyId = Convert.ToInt32(Session["UserId"]);
        obj.userCompanyId = Convert.ToInt32(Session["OrgUserId"]);
        //obj.GetAllEmployee();
        obj.userCreateBy = Convert.ToString(Session["OrgUserId"]);
        obj.GetAllEmployeecreatebymanager();
        DataSet ds = obj.ds;

        //DataView dv = ds.Tables[0].DefaultView;
        //gvGrid.DataSource = BindGridView();
        //gvGrid.DataSource = ds;

        DataView sortedView = new DataView(ds.Tables[0]);
        sortedView.Sort = e.SortExpression + " " + sortingDirection;
        Session["SortedView"] = sortedView;
        gvGrid.DataSource = sortedView;
        gvGrid.DataBind();
    }

    public SortDirection direction
    {
        get
        {
            if (ViewState["directionState"] == null)
            {
                ViewState["directionState"] = SortDirection.Ascending;
            }
            return (SortDirection)ViewState["directionState"];
        }
        set
        {
            ViewState["directionState"] = value;
        }
    }


    #endregion

    #region repeater
    protected void Repeater1_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataRowView drv = (DataRowView)e.Item.DataItem;
            int categoryID = Convert.ToInt32(drv["catId"]);
            Repeater Repeater2 = (Repeater)e.Item.FindControl("repCompAdd");
            Label compCategoryName = (Label)e.Item.FindControl("lblCompCategoryName");
            var categoryName = Convert.ToString(drv["catName"]);
            var categoryNameDN = Convert.ToString(drv["catNameDN"]);

            if (Convert.ToString(Session["Culture"]) == "Danish")
            {
                compCategoryName.Text = categoryNameDN;

            }
            else
            {
                compCategoryName.Text = categoryName;
            }

            Repeater2.DataSource = GetAllCompetenceAddbyComCatId(categoryID);
            Repeater2.DataBind();



            foreach (RepeaterItem bItem in Repeater2.Items)
            {
                CheckBoxList chk = (CheckBoxList)bItem.FindControl("chkList");

                var data = GetAllCompetenceAddbyComCatId(categoryID);
                if (data == null) return;
                DataSet ds = data.DataSet;
                var catIds = string.Empty;
                for (int intCount = 0; intCount < ds.Tables[0].Rows.Count; intCount++)
                {

                    if (Convert.ToString(Session["Culture"]) == "Danish")
                    {
                        ds.Tables[0].Rows[intCount][3] += "  (" + categoryNameDN + ")";
                    }
                    else
                    {
                        ds.Tables[0].Rows[intCount][1] += "  (" + categoryName + ")";
                    }

                }
                ds.Tables[0].AcceptChanges();


                /////////////////
                if (Convert.ToInt32(ds.Tables[0].Rows[0]["catId"]) == categoryID)
                {
                    if (Convert.ToString(Session["Culture"]) == "Danish")
                    {
                        ds.Tables[0].Columns["catName"].ColumnName = "abcd";
                        ds.Tables[0].Columns["catNameDN"].ColumnName = "catName";

                    }
                    chk.DataSource = data;
                    chk.DataTextField = "catName";
                    chk.DataValueField = "comId";

                    chk.DataBind();


                    //chk1.DataSource = data;
                    //chk1.DataTextField = "catName";
                    //chk1.DataValueField = "comId";

                    //chk1.DataBind();

                    foreach (ListItem li in chk.Items)
                    {
                        if (string.IsNullOrWhiteSpace(catIds))
                        {
                          //  li.Selected = true;
                        }

                        else
                        {
                            foreach (var j in catIds.Split(','))
                            {

                                if (li.Value == j)
                                {
                                    li.Selected = true;
                                    break;
                                }
                                else
                                {
                                    li.Selected = false;
                                }
                            }

                        }

                    }

                    break;

                }
            }
        }
    }

    protected void Repeater2_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //{
        //    DataRowView drv = (DataRowView)e.Item.DataItem;
        //    int categoryID = Convert.ToInt32(drv["catId"]);
        //    CheckBoxList chk = (CheckBoxList)e.Item.FindControl("chkList");
        //    var data = GetAllCompetenceAddbyComCatId(categoryID);
        //    if (data == null) return;
        //    DataSet ds = data.DataSet;
        //    if (Convert.ToInt32(ds.Tables[0].Rows[0]["catId"]) == categoryID)
        //    {
        //        chk.DataSource = data;
        //        chk.DataTextField = "catName";
        //        chk.DataValueField = "catId";
        //        chk.DataBind();
        //    }
        //}
    }
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        //string language = "French";
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Danish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();
            //if (!string.IsNullOrEmpty(language))
            //{
            //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
            //    {
            //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
            //        {
            //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
            //        }
            //    }
            //    //if (language.EndsWith("Danish")) languageId = "da-DK";
            //    //else languageId = "en-GB";
            //    
            //}
            SetCulture(languageId);
        }

        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
}