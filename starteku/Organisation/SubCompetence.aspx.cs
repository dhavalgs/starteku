﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using startetku.Business.Logic;
using System.Threading;
using System.Globalization;

public partial class Organisation_SubCompetence : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            Competence.InnerHtml = CommonMessages.Welcome + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            subCompetenceSelectAll();

            SetDefaultMessage();

        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region method
    protected void clear()
    {
        txtName.Text = "";
        txtcname.Text = "";
        txteDescription.Text = "";
        txtDescription.Text = "";
        lblMsg.Text = "";
    }
    protected void SetDefaultMessage()
    {
        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["msg"])))
        {
            if (Convert.ToString(Request.QueryString["msg"]) == "del")
            {
                //lblMsg.Text = CommonModule.msgRecordHasBeenDeletedSuccessfully;
                lblMsg.Text = CommonMessages.msgRecordHasBeenDeletedSuccessfully;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "ins")
            {
                lblMsg.Text = CommonMessages.msgRecordInsertedSuccessfully;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "upd")
            {
                lblMsg.Text = CommonMessages.msgRecordUpdatedSuccss;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "arc")
            {
                lblMsg.Text = CommonMessages.msgRecordArchiveSucces;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
        }

    }
    protected void subCompetenceSelectAll()
    {
        SubCompetenceBM obj = new SubCompetenceBM();
        obj.subIsActive = true;
        obj.subIsDeleted = false;
        obj.subComId = Convert.ToInt32(Request.QueryString["id"]);
        obj.GetAllSubCompetenceMaster();
        DataSet ds = obj.ds;
        ViewState["ds"] = ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvGrid.DataSource = ds.Tables[0];
                gvGrid.DataBind();

                gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvGrid.DataSource = null;
                gvGrid.DataBind();
            }
        }
        else
        {
            gvGrid.DataSource = null;
            gvGrid.DataBind();
        }
    }
    protected void InsertsubCompetenceMaster()
    {
        SubCompetenceBM obj = new SubCompetenceBM();
        obj.subCompetence = txtName.Text;
        obj.subDescription= txtDescription.Text;
        obj.subUserId = Convert.ToInt32(Session["OrgUserId"]);
        obj.subComId = Convert.ToInt32(Request.QueryString["id"]);
        obj.subIsActive = true;
        obj.subIsDeleted = false;
        obj.subCreatedDate = DateTime.Now;
        obj.subCreateBy = 0;
        obj.InsertSubCompetence();
        if (obj.ReturnBoolean == true)
        {
            clear();
        }

        else
        {
            lblMsg.Text = CommonModule.msgProblemInsertRecord;
        }
    }
    protected void UpdateSubCompetence(int id)
    {
        SubCompetenceBM obj = new SubCompetenceBM();
        obj.subId = id;
        obj.subCompetence = txtcname.Text;
        obj.subUpdatedDate = DateTime.Now;
        obj.subDescription = txteDescription.Text;
        obj.UpdateSubCompetence();
        if (obj.ReturnBoolean == true)
        {
            clear();
        }

        else
        {
            lblMsg.Text = CommonModule.msgProblemInsertRecord;
        }
    }
    protected void GetAllsubCompetenceid(int id)
    {
        ViewState["id"] = Convert.ToString(id);
        DataSet ds1 = (DataSet)ViewState["ds"];
        DataView dv = new DataView();

        dv = ds1.Tables[0].DefaultView;
      
        dv.RowFilter = ("subId =  '" + Convert.ToInt32(id) + "'");
        DataTable dtitm = dv.ToTable();
        DataSet ds = new DataSet();
        ds.Tables.Add(dtitm);
        if (ds != null)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["subCompetence"])))
                txtcname.Text = Convert.ToString(ds.Tables[0].Rows[0]["subCompetence"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["subDescription"])))
                txteDescription.Text = Convert.ToString(ds.Tables[0].Rows[0]["subDescription"]);
        }

    }
    #endregion

    #region grid Event
    protected void gvGrid_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "archive")
        {
            SubCompetenceBM obj = new SubCompetenceBM();
            obj.subId = Convert.ToInt32(e.CommandArgument);
            obj.subIsActive = false;
            obj.subIsDeleted = false;
            obj.subCompetenceMasterUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    subCompetenceSelectAll();

                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
        else if (e.CommandName == "View")
        {
            GetAllsubCompetenceid(Convert.ToInt32(e.CommandArgument));
            mpe.Show();
        }
    }
    #endregion


    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        InsertsubCompetenceMaster();
        subCompetenceSelectAll();
    }
    protected void btnupdate_click(object sender, EventArgs e)
    {
        Int32 id = Convert.ToInt32(ViewState["id"]);
        UpdateSubCompetence(id);
        subCompetenceSelectAll();
    }
    protected void btnclose_click1(object sender, EventArgs e)
    {
        //clear();
    }
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Denish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
}