﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;

public partial class Organisation_OrgCms : System.Web.UI.Page
{

    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            CMS.InnerHtml = "Welcome   " + Convert.ToString(Session["OrgUserName"]) + "!";
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                GetAllCMSbyid();
            }
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region method
    protected void SetDefaultMessage()
    {

        string id = "?id=";
        string childurl = "child.aspx";
        if (Request.RawUrl.ToString().ToLower().Contains(childurl.ToLower()))
        {
            if (Request.RawUrl.ToString().ToLower().Contains(id.ToLower()))
            {
                lblDataDisplayTitle.Text = "Edit Children";

            }
            else
            {
                lblDataDisplayTitle.Text = "Add Children";
            }
        }
    }
    protected void InsertCMS()
    {
        try
        {
            CmsBM obj = new CmsBM();
            obj.cmsName = txtName.Text;
            obj.cmsDescription = txtDescription.Text;
            obj.cmsCompanyId = Convert.ToInt32(Session["OrgUserId"]);
            obj.cmsIsActive = true;
            obj.cmsIsDeleted = false;
            obj.cmsCreatedDate = DateTime.Now;
            obj.InsertCms();
            if (obj.ReturnBoolean == true)
            {
                Response.Redirect("OrgCmsList.aspx");
            }
        }
        catch (DataException ex)
        {
            throw ex;
        }

    }
    protected void updateCMS()
    {
        try
        {
            CmsBM obj = new CmsBM();
            obj.cmsId = Convert.ToInt32(Request.QueryString["id"]);
            obj.cmsName = txtName.Text;
            obj.cmsDescription = txtDescription.Text;
            obj.cmsUpdatedDate = DateTime.Now;
            obj.UpdateCms();
            //  DataSet ds = obj.ds;
            if (obj.ReturnBoolean == true)
            {
                Response.Redirect("OrgCmsList.aspx");
            }
        }
        catch (DataException ex)
        {
            throw ex;
        }

    }
    protected void GetAllCMSbyid()
    {
        try
        {
            CmsBM obj = new CmsBM();
            obj.cmsId = Convert.ToInt32(Request.QueryString["id"]);
            obj.GetAllcmsbyid();
            DataSet ds = obj.ds;

            if (ds != null)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["cmsName"])))
                    txtName.Text = Convert.ToString(ds.Tables[0].Rows[0]["cmsName"]);

                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["cmsDescription"])))
                    txtDescription.Text = Convert.ToString(ds.Tables[0].Rows[0]["cmsDescription"]);
            }
        }
        catch (DataException ex)
        {
            throw ex;
        }
    }

    #endregion


    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Request.QueryString["id"]))
        {
            updateCMS();
        }
        else
        {
            InsertCMS();
        }
    }
    protected void btnCancel_click(object sender, EventArgs e)
    {
        Response.Redirect("OrgCmsList.aspx");
    }
    #endregion
}