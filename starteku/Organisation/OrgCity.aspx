﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master" AutoEventWireup="true" CodeFile="OrgCity.aspx.cs" Inherits="Organisation_OrgCity" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg"></asp:Label>
    <asp:Label runat="server" ID="Label1" CssClass="commonerrormsg"></asp:Label>

    <div class="col-md-6">
        <div class="heading-sec">
            <h1>City   <i><span runat="server" id="City"></span></i></h1>
        </div>
    </div>
     <br /><br />

    <div class="col-md-6 range ">
        <%--<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
            <i class="fa fa-calendar-o icon-calendar icon-large"></i>
            <span>August 5, 2014 - September 3, 2014</span>  <b class="caret"></b>
        </div>--%>
    </div>
    <div class="col-md-12">
        <div class="chat-widget widget-body" style="background: #fff;">
            <asp:Label runat="server" ID="lblDataDisplayTitle"></asp:Label>
            <div class="chat-widget-head yellow" >
                <h4 style="margin: -6px 0px 0px;">City Information</h4>
            </div>
            <div class="indicatesRequireFiled">
                <i>* Indicates required field</i>
            </div>
            <div class="col-md-6" style="width:100%;">
                <div class="inline-form">
                    <label class="c-label" style="width:100%;">State Name:</label>
                    <asp:DropDownList ID="ddlstate" runat="server" Style="width: 100px; height: 32px; display: inline;">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="ddlstate"
                        ErrorMessage="Please select State." InitialValue="0" CssClass="commonerrormsg"
                        Display="Dynamic" ValidationGroup="chk"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label">City Name:</label>
                    <asp:TextBox runat="server" Text="" ID="txtcityName" MaxLength="50" CssClass="form-control" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtcityName"
                        ErrorMessage="Please enter City name." CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk"></asp:RequiredFieldValidator>
                </div>
            </div>



            <div class="col-xs-12 profile_bottom">
                <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="btn black pull-right" ValidationGroup="chk"
                    CausesValidation="false" OnClick="btnCancel_click" />                
                <asp:Button runat="server" ID="btnsubmit" Text="Save" CssClass="btn blue pull-right"
                    ValidationGroup="chk" OnClick="btnsubmit_click" />
            </div>
        </div>
    </div>

</asp:Content>

