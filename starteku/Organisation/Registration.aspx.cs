﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using System.Configuration;

public partial class Organisation_Registration : System.Web.UI.Page
{
    String temp = "";
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Registration.InnerHtml = "Welcome   " + Convert.ToString(Session["OrgUserName"]) + "!";
            if (!String.IsNullOrEmpty(Request.QueryString["org"]))
            {
                temp = Convert.ToString(Request.QueryString["org"]);
            }
            else if (!String.IsNullOrEmpty(Request.QueryString["emp"]))
            {
                temp = Convert.ToString(Request.QueryString["emp"]);
            }
            GetAllJobtype();
            GetAllCountry();
            if (!String.IsNullOrEmpty(temp))
            {
                //this.Master.FindControl("menu").Visible = false;
                state.Attributes.Add("style", "display:block");
                //city.Attributes.Add("style", "display:block");
                GetAllEmployeebyid();

            }
            //else if (!String.IsNullOrEmpty(Request.QueryString["emp"]))
            //{
            //    state.Attributes.Add("style", "display:block");
            //    city.Attributes.Add("style", "display:block");
            //    GetAllEmployeebyid();

            //}

        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region method
    protected void GetAllJobtype()
    {
        JobTypeBM obj = new JobTypeBM();
        obj.jobIsActive = true;
        obj.jobIsDeleted = false;
        obj.jobCompanyId = Convert.ToInt32(Session["OrgUserId"]);
        obj.GetAllJobType();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddljobtype.Items.Clear();

                ddljobtype.DataSource = ds.Tables[0];
                ddljobtype.DataTextField = "jobName";
                ddljobtype.DataValueField = "jobId";
                ddljobtype.DataBind();

                ddljobtype.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
            }
            else
            {
                ddljobtype.Items.Clear();
                ddljobtype.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddljobtype.Items.Clear();
            ddljobtype.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
        }

    }
    protected void SetDefaultMessage()
    {
        string id = "?id=";
        string childurl = "child.aspx";
        if (Request.RawUrl.ToString().ToLower().Contains(childurl.ToLower()))
        {
            if (Request.RawUrl.ToString().ToLower().Contains(id.ToLower()))
            {
                lblDataDisplayTitle.Text = "Edit Children";

            }
            else
            {
                lblDataDisplayTitle.Text = "Add Children";
            }
        }
    }
    protected void GetAllCountry()
    {
        CountryBM obj = new CountryBM();
        obj.couIsActive = true;
        obj.couIsDeleted = false;
        obj.GetAllCountry();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlcountry.Items.Clear();
                ddlcountry.DataSource = ds.Tables[0];
                ddlcountry.DataTextField = "couName";
                ddlcountry.DataValueField = "couId";
                ddlcountry.DataBind();
                if ((String.IsNullOrEmpty(temp)))
                {
                    ddlcountry.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));

                }
                else
                {
                    GetState(Convert.ToInt32(ddlcountry.SelectedValue));
                }
            }
            else
            {
                ddlcountry.Items.Clear();
                ddlcountry.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddlcountry.Items.Clear();
            ddlcountry.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
        }


    }
    protected void GetState(int contryid)
    {
        state.Attributes.Add("style", "display:block");

        StateBM obj = new StateBM();
        obj.staCountryId = contryid;
        obj.staIsActive = true;
        obj.staIsDeleted = false;
        obj.GetStatebyCountryid();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlstate.Items.Clear();
                ddlstate.DataSource = ds.Tables[0];
                ddlstate.DataTextField = "staName";
                ddlstate.DataValueField = "staId";
                ddlstate.DataBind();

                //  ddlstate.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
            }
            else
            {
                ddlstate.Items.Clear();
                ddlstate.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddlstate.Items.Clear();
            ddlstate.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
        }
        //Getcity(Convert.ToInt32(ddlstate.SelectedValue));
    }
    //protected void Getcity(int stateid)
    //{
    //    city.Attributes.Add("style", "display:block");

    //    CityBM obj = new CityBM();
    //    obj.citStateId = stateid;
    //    obj.citIsActive = true;
    //    obj.citIsDeleted = false;
    //    obj.Getcitybystateid();
    //    DataSet ds = obj.ds;

    //    if (ds != null)
    //    {
    //        if (ds.Tables[0].Rows.Count > 0)
    //        {
    //            ddlcity.Items.Clear();
    //            ddlcity.DataSource = ds.Tables[0];
    //            ddlcity.DataTextField = "citName";
    //            ddlcity.DataValueField = "citId";
    //            ddlcity.DataBind();

    //            //  ddlstate.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
    //        }
    //        else
    //        {
    //            ddlcity.Items.Clear();
    //            ddlcity.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
    //        }
    //    }
    //    else
    //    {
    //        ddlcity.Items.Clear();
    //        ddlcity.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
    //    }

    //}
    protected void redirectpage(string msg, string paage)
    {
        //string message = "You will now be redirected to ASPSnippets Home Page.";
        string message = msg;
        //string url = "Login.aspx?msg=ins";
        string url = paage;
        string script = "window.onload = function(){ alert('";
        script += message;
        script += "');";
        script += "window.location = '";
        script += url;
        script += "'; }";
        ClientScript.RegisterStartupScript(this.GetType(), "Redirect", script, true);

    }
    protected void Insertuser()
    {
        string filePath = "";
        UserBM obj2 = new UserBM();
        obj2.useremailCheckDuplication(txtEmail.Text, -1);
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        {
            UserBM obj = new UserBM();
            obj.userFirstName = txtfName.Text;
            obj.userLastName = txtlname.Text;
            obj.userZip = txtzip.Text;
            obj.userAddress = txtaddress.Text;
            obj.userCountryId = Convert.ToInt32(ddlcountry.SelectedValue);
            obj.userStateId = Convert.ToInt32(ddlstate.SelectedValue);
            //obj.userCityId = Convert.ToInt32(ddlcity.SelectedValue);
            obj.userCityId =txtcity.Text;
            obj.usercontact = txtMobile.Text;
            obj.userEmail = txtEmail.Text;
            if (!String.IsNullOrEmpty(Request.QueryString["org"]))
            {
                obj.userType = 2;
            }
            else if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                obj.userType = 3;
            }
            // obj.userCityId = Convert.ToInt32(ddlcity.SelectedValue);
            obj.userCreatedDate = DateTime.Now;
            obj.userIsActive = true;
            obj.userIsDeleted = false;
            if (!String.IsNullOrEmpty(Request.QueryString["org"]))
            {
                obj.userCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            }
            else if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                obj.userCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            }
            obj.userPassword = CommonModule.encrypt(txtPassword.Text.Trim());
            obj.userDOB = Convert.ToDateTime(CommonModule.FormatdtEnter(txtfromdate.Text));
            obj.userGender = ddlgender.SelectedValue;
            if (flupload1.HasFile)
            {
                filePath = System.DateTime.Now.ToString("MMddyyhhmmss") + "_" + flupload1.PostedFile.FileName;
                flupload1.SaveAs(Server.MapPath("../") + "Log/upload/Userimage/" + System.IO.Path.GetFileName(filePath.Replace(' ', '0')));
                obj.userImage = System.IO.Path.GetFileName(filePath.Replace(' ', '0'));

            }
            else
            {
                if (ddlgender.SelectedValue == "Male")
                {
                    obj.userImage = "male.png";
                }
                else
                {//ViewState["userImage"]
                    obj.userImage = "female.png";
                }
            }
            obj.userLevel = 1;
            obj.userJobType=Convert.ToInt32(ddljobtype.SelectedValue);
            obj.InsertOrganisation();
            //  DataSet ds = obj.ds;
            if (obj.ReturnBoolean == true)
            {
                sendmail(txtEmail.Text, txtfName.Text, txtPassword.Text);
                if (!String.IsNullOrEmpty(Request.QueryString["org"]))
                {
                    string msg = CommonModule.msgSuccessfullyRegistered;
                    redirectpage(msg, "EmployerList.aspx");
                }
                else
                {
                    string msg = CommonModule.msgSuccessfullyRegistered;
                    redirectpage(msg, "EmployeeList.aspx");
                }

            }
        }
        else
        {
            if (returnMsg == "userEmail")
                lblMsg.Text = CommonModule.msgEmailAlreadyExists;
            else
                lblMsg.Text = CommonModule.msgProblemInsertRecord;
        }
    }
    protected void UpdateUser()
    {
        string filePath = "";
        string QueryString = "0";
        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["emp"])))
        {
            QueryString = Convert.ToString(Request.QueryString["emp"]);
        }
        else
        {
            QueryString = Convert.ToString(Request.QueryString["org"]);
        }

        UserBM obj2 = new UserBM();
        obj2.useremailCheckDuplication(txtEmail.Text, Convert.ToInt32(QueryString));
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        {
            UserBM obj = new UserBM();
            obj.userId = Convert.ToInt32(QueryString);
            obj.userFirstName = txtfName.Text;
            obj.userLastName = txtlname.Text;
            obj.userZip = txtzip.Text;
            obj.userAddress = txtaddress.Text;
            obj.userCountryId = Convert.ToInt32(ddlcountry.SelectedValue);
            obj.userStateId = Convert.ToInt32(ddlstate.SelectedValue);
            //obj.userCityId = Convert.ToInt32(ddlcity.SelectedValue);
            obj.usercontact = txtMobile.Text;
            obj.userEmail = txtEmail.Text;
            obj.userCityId = txtcity.Text;
            obj.userUpdatedDate = DateTime.Now;
            obj.userPassword = CommonModule.encrypt(txtPassword.Text.Trim());
            obj.userDOB = Convert.ToDateTime(CommonModule.FormatdtEnter(txtfromdate.Text));
            obj.userGender = ddlgender.SelectedValue;
            if (flupload1.HasFile)
            {

                filePath = System.DateTime.Now.ToString("MMddyyhhmmss") + "_" + flupload1.PostedFile.FileName;
                flupload1.SaveAs(Server.MapPath("../") + "Log/upload/Userimage/" + System.IO.Path.GetFileName(filePath.Replace(' ', '0')));
                obj.userImage = System.IO.Path.GetFileName(filePath.Replace(' ', '0'));

            }
            else
            {
                obj.userImage = Convert.ToString(ViewState["userImage"]);

            }
            obj.userJobType = Convert.ToInt32(ddljobtype.SelectedValue);
            obj.UpdateOrganisation();
            if (obj.ReturnBoolean == true)
            {
                if (!String.IsNullOrEmpty(Request.QueryString["org"]))
                {
                    //string msg = CommonModule.msgSuccessfullyRegistered;
                    //redirectpage(msg, "EmployerList.aspx");
                    Response.Redirect("EmployerList.aspx?msg=upd");
                }
                else if (!String.IsNullOrEmpty(Request.QueryString["emp"]))
                {
                    Response.Redirect("EmployeeList.aspx?msg=upd");
                }
            }
        }
        else
        {
            if (returnMsg == "userEmail")
                lblMsg.Text = CommonModule.msgEmailAlreadyExists;
            else
                lblMsg.Text = CommonModule.msgProblemInsertRecord;
        }
    }
    protected void GetAllEmployeebyid()
    {
        UserBM obj = new UserBM();

        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["emp"])))
        {
            obj.userId = Convert.ToInt32(Request.QueryString["emp"]);
        }
        else
        {
            obj.userId = Convert.ToInt32(Request.QueryString["org"]);
        }
        obj.GetAllEmployeebyid();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userFirstName"])))
                txtfName.Text = Convert.ToString(ds.Tables[0].Rows[0]["userFirstName"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userLastName"])))
                txtlname.Text = Convert.ToString(ds.Tables[0].Rows[0]["userLastName"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userZip"])))
                txtzip.Text = Convert.ToString(ds.Tables[0].Rows[0]["userZip"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userAddress"])))
                txtaddress.Text = Convert.ToString(ds.Tables[0].Rows[0]["userAddress"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userCountryId"])))
                ddlcountry.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["userCountryId"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userStateId"])))
                ddlstate.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["userStateId"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userCityId"])))
                //ddlcity.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["userCityId"]);
               txtcity.Text = Convert.ToString(ds.Tables[0].Rows[0]["userCityId"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["usercontact"])))
                txtMobile.Text = Convert.ToString(ds.Tables[0].Rows[0]["usercontact"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userEmail"])))
                txtEmail.Text = Convert.ToString(ds.Tables[0].Rows[0]["userEmail"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userDOB"])))
                txtfromdate.Text = Convert.ToString(ds.Tables[0].Rows[0]["userDOB"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userPassword"])))
            {
                string Password = CommonModule.decrypt(Convert.ToString(ds.Tables[0].Rows[0]["userPassword"]));
                txtPassword.Attributes.Add("value", Password);
                txtConfirmPassword.Attributes.Add("value", Password);
            }

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userGender"])))
                ddlgender.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["userGender"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userImage"])))
                ViewState["userImage"] = Convert.ToString(ds.Tables[0].Rows[0]["userImage"]);
            img_profile.ImageUrl = "../Log/upload/Userimage/" + Convert.ToString(ds.Tables[0].Rows[0]["userImage"]) + "";


            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userJobType"])))
                ddljobtype.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["userJobType"]);

        }

    }
    protected void sendmail(string email, string name, string pass)
    {
        try
        {
           // Common.WriteLog("start");
            string subject = "Registration";
            string fromid = Convert.ToString(ConfigurationManager.AppSettings["fromid"]);
            CmsBM objMail = new CmsBM();
            objMail.cmsName = "Registration";
            objMail.SelectMailTemplateByName();
            DataSet dsMail = objMail.ds;
            if (dsMail.Tables[0].Rows.Count > 0)
            {
              //  Common.WriteLog("start_in");
                string confirmMail = dsMail.Tables[0].Rows[0]["cmsDescription"].ToString();
                string tempString = confirmMail;
                tempString = tempString.Replace("###name###", name);
                tempString = tempString.Replace("###email###", email);
                tempString = tempString.Replace("###password###", pass);
                //SendMail(tempString, txtEmail.Text, txtFname.Text);
                CommonModule.SendMailToUser(email, subject, tempString, fromid, Convert.ToString(HttpContext.Current.Session["OrgCompanyId"]));
            }
        }
        catch (Exception ex)
        {
            Common.WriteLog("error in sendmail" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
        }

    }
    #endregion

    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        if ((!String.IsNullOrEmpty(Request.QueryString["emp"])) || (!String.IsNullOrEmpty(Request.QueryString["org"])))
        {
            UpdateUser();
        }
        else
        {
            Insertuser();
        }

    }
    protected void btnCancel_click(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Request.QueryString["org"]))
        {
            Response.Redirect("EmployerList.aspx?msg=upd");
        }
        else if (!String.IsNullOrEmpty(Request.QueryString["emp"]))
        {
            Response.Redirect("EmployeeList.aspx?msg=upd");
        }
    }
    #endregion

    #region SelectedIndexChanged
    protected void ddlcountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetState(Convert.ToInt32(ddlcountry.SelectedValue));
    }
    protected void ddlstate_SelectedIndexChanged(object sender, EventArgs e)
    {
      //  Getcity(Convert.ToInt32(ddlstate.SelectedValue));
    }
    #endregion
}