﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="Category.aspx.cs" Inherits="Organisation_Category"
    Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/ajaxtab.css" rel="stylesheet" />
    <style type="text/css">
        .yellow
        {
            border-radius: 0px;
        }
        .treeNode input
        {
            width: auto;
            margin: 5px;
            float: left !important;
        }
        .table > thead > tr > th
        {
            vertical-align: middle;
        }
    </style>
  <script>
      function focusOn() {
          setTimeout(function () {
              //alert();
              $('#ContentPlaceHolder1_txtDepartmentName').focus();
          }, 1500);
      }
  </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="col-md-6">
        <div class="heading-sec">
            <h1 style="margin-left: 15px;">
                <%-- <%= CommonMessages.CATEGORY%> --%>
                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="CATEGORY" EnableViewState="false" /><i><span
                    runat="server" id="Category"></span></i>
            </h1>
        </div>
    </div>
    <%-- <div class="col-md-3">
        <div class="dropdown-example">
            <ul class="nav nav-pills">
                <li class="dropdown" style="float: left!important; width: 100%;">
                    <a class="skill_dropdown" id="drop7" role="button" data-toggle="dropdown" href="#">Account Department<b class="skill_caret"></b></a>
                    <ul id="menu7" class="dropdown-menu" role="menu" aria-labelledby="drop7" style="width: 100%;">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Account Department</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Sales Department</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Human Resources Department </a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Software Department</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Network Department</a></li>
                        <li role="presentation" class="catIder"></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="report_all.html">All</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
    <div class="col-md-3">
        <div class="dropdown-example">
            <ul class="nav nav-pills">
                <li class="dropdown" style="float: left!important; width: 100%;">
                    <a class="skill_dropdown" id="A1" role="button" data-toggle="dropdown" href="report_all.html">All<b class="skill_caret"></b></a>
                    <ul id="Ul1" class="dropdown-menu" role="menu" aria-labelledby="drop7" style="width: 100%;">
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="report.html">Original competence level</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">New competence level</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Development points</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Knowledge share</a></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">Potential for development</a></li>
                        <li role="presentation" class="catIder"></li>
                        <li role="presentation"><a role="menuitem" tabindex="-1" href="#">All</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>--%>
    <div class="col-md-12" style="margin-top: 20px;">
        <div id="graph-wrapper">
            <div class="col-md-12">
                <a href="#add-post-title" data-toggle="modal" title="" style="margin-bottom: 15px;" onclick="focusOn()">
                    <button style="border: 0px;" class="btn btn-primary yellow lrg-btn flat-btn add_user" type="button">
                        <%--   <%= CommonMessages.AddNew%> <%= CommonMessages.CATEGORY%>--%>
                        <asp:Literal ID="Literal2" runat="server" meta:resourcekey="AddNewcategory" EnableViewState="false" /></button>
                </a>
                <div aria-hidden="true" role="dialog" tabindex="-1" class="modal fade" id="add-post-title"
                    style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header blue yellow-radius" style="border-radius: 0px;">
                                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                                    ×
                                </button>
                                <h4 class="modal-title">
                                    <%--  <%= CommonMessages.AddNew%> <%= CommonMessages.CATEGORY%>--%>
                                    <asp:Literal ID="Literal3" runat="server" meta:resourcekey="AddNewcategory" EnableViewState="false" />
                                </h4>
                            </div>
                            <div class="modal-body">
                                <asp:TreeView ID="TreeView1" runat="server" ShowCheckBoxes="All" onclick="client_OnTreeNodeChecked(event)"
                                    OnTreeNodePopulate="TreeView_TreeNodePopulate" Style="cursor: pointer" ShowLines="True"
                                    NodeStyle-CssClass="treeNode" Visible="False" meta:resourcekey="TreeView1Resource1" />
                                <br />
                                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                    <ContentTemplate>
                                        <asp:TextBox runat="server" placeholder="Title :" ID="txtDepartmentName" MaxLength="50"
                                            meta:resourcekey="txtDepartmentNameResource1" AutoPostBack="True" OnTextChanged="txtDepartmentName_TextChanged" />
                                        <asp:Label runat="server" ID="lblDepartment" CssClass="commonerrormsg" meta:resourcekey="lblDepartmentResource1"></asp:Label>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtDepartmentName"
                                    ErrorMessage="Please enter Department name." CssClass="commonerrormsg" Display="Dynamic"
                                    ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator5Resource1"></asp:RequiredFieldValidator>
                                        <div style="clear:both;"></div>

                                        
                               
                                    <asp:TextBox runat="server" ID="txtDepartmentNameDN" MaxLength="50" placeholder="Category in danish"
                                         meta:resourcekey="txtDepartmentNameDNResource1" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtDepartmentNameDN"
                                        ErrorMessage="Please enter category name in danish." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator6Resource1"></asp:RequiredFieldValidator>
                              
                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <br />
                                <%--//Saurin |20141211| not use in  category page: Description--%>
                                <%-- <asp:TextBox runat="server" placeholder="DESCRIPTION :" ID="txtDESCRIPTION" MaxLength="500"
                                    TextMode="MultiLine" Rows="5" meta:resourcekey="txtDESCRIPTIONResource1" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDESCRIPTION"
                                    ErrorMessage="Please Enter DESCRIPTION." CssClass="commonerrormsg" Display="Dynamic"
                                    ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator1Resource1"></asp:RequiredFieldValidator>--%>
                            </div>
                            <div class="modal-footer">
                                <asp:Button runat="server" ID="btnsubmit" Text="Submit" CssClass="btn btn-primary yellow"
                                    ValidationGroup="chk" OnClick="btnsubmit_click" OnClientClick="CheckValidations('chk')"
                                    Style="border-radius: 5px;" meta:resourcekey="btnsubmitResource1" />
                                <button data-dismiss="modal" class="btn btn-default black" type="button">
                                    <%-- Close<%= CommonMessages.Close%>--%>
                                    <asp:Literal ID="Literal4" runat="server" meta:resourcekey="Close" EnableViewState="false" />
                                </button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                </div>
                <br /><br />
                <div style="clear:both;"></div>
                <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" meta:resourcekey="lblMsgResource1"></asp:Label>
                <br />
                 <asp:Label ID="hdnConfirmArchive" style="display:none;"   CssClass="abcde" meta:resourcekey="ConfirmArchive" runat="server" />
                <div class="chart-tab manager_table">
                    <asp:GridView ID="gvGrid" runat="server" AutoGenerateColumns="False" CellPadding="0"
                        Width="100%" GridLines="None" DataKeyNames="catId" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                        EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>' OnRowCommand="gvGrid_RowCommand"
                        OnRowDataBound="gvGrid_RowDataBound" meta:resourcekey="gvGridResource1">
                        <HeaderStyle CssClass="aa" />
                        <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:TemplateField HeaderText="Id" meta:resourcekey="TemplateFieldResource1">
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1 %>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" Font-Size="14px" />
                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                    Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="CATEGORY" meta:resourcekey="TemplateFieldResource2">
                                <ItemTemplate>
                                    <asp:Label ID="lblrNamer" runat="server" Text="<%# bind('catName') %>" meta:resourcekey="lblrNamerResource1"></asp:Label>
                                    <asp:HiddenField ID="catCompanyId" runat="server" Value="<%# bind('catCompanyId') %>" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Font-Size="14px" />
                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                    Width="50%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="" ItemStyle-Width="50px" meta:resourcekey="TemplateFieldResource3">
                                <ItemTemplate>
                                    <div class="vat" style="width: 70px" id="catEdit" runat="server">
                                        <p>
                                            <i class="fa fa-pencil"></i>
                                         
                                            <a href="<%# String.Format("EditCategory.aspx?id={0}", Eval("catId")) %>"
                                                title="Edit">
                                                <asp:Label runat="server" meta:resourcekey="btnEdit"></asp:Label>

                                            </a></asp:LinkButton>
                                        </p>
                                    </div>
                                    <div class="total" style="width: 70px;" id="catDeletet" runat="server">
                                        <p>
                                            <i class="fa fa-trash-o"></i>
                                            
                                            <asp:LinkButton ID="lnkBtnName" OnClientClick="return xyz();" CssClass="def" runat="server" CommandName="archive" CommandArgument='<%# Eval("catId") %>'
                                                ToolTip="Delete " meta:resourcekey="lnkBtnNameResource1">Delete </asp:LinkButton>
                                        </p>
                                    </div>
                                    <div class="total" style="width: 200px;" id="div1" runat="server">
                                        <p>
                                            <i>
                                                <img src="images/block.jpg" alt="" height="15px;" width="15px;" /></i>
                                            <%-- <%= CommonMessages.NotEditable%>--%>
                                            <asp:Literal ID="Literal1" runat="server" meta:resourcekey="NotEditable" EnableViewState="false" />
                                        </p>
                                    </div>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" Font-Size="14px" />
                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                    Width="15%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                    <%-- </div>
                                </ContentTemplate>
                            </cc1:TabPanel>
                            <cc1:TabPanel ID="tbpnlusrdetails" runat="server">
                                <HeaderTemplate>
                                    Archive
                                    <a></a>
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <div class="invoice" style="background-color: white; margin-top: 0px;">--%>
               <%--     <asp:GridView ID="gvArchive" runat="server" AutoGenerateColumns="False" CellPadding="0"
                        Width="100%" GridLines="None" Visible="False" EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'
                        DataKeyNames="catId" OnRowCommand="gvArchive_RowCommand" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                        meta:resourcekey="gvArchiveResource1">
                        <HeaderStyle CssClass="aa" />
                        <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:TemplateField HeaderText="ID" ItemStyle-Width="50px" meta:resourcekey="TemplateFieldResource4">
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1 %>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Category" meta:resourcekey="TemplateFieldResource5">
                                <ItemTemplate>
                                    <asp:Label ID="lblaNamer" runat="server" Text="<%# bind('catName') %>" meta:resourcekey="lblaNamerResource1"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                            </asp:TemplateField>
                            <%-- <asp:TemplateField HeaderText="State Name">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUserNamer" runat="server" Text="<%# bind('divName') %>">
                                                </asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        </asp:TemplateField>   --%>
                               <%-- <asp:TemplateField HeaderText="Action" ItemStyle-Width="50px" meta:resourcekey="TemplateFieldResource6">
                                <ItemTemplate>
                                    <span class="btn-group">
                                        <asp:LinkButton ID="lnkbtnrestore" runat="server" CommandName="archive" CommandArgument='<%# Eval("catId") %>'
                                            ToolTip="Restore" OnClientClick="return confirm('Are you sure you want to restore this record?');"
                                            Text="Restore" meta:resourcekey="lnkbtnrestoreResource1"></asp:LinkButton>&nbsp;|&nbsp;
                                        <asp:LinkButton ID="lnkBtnPermanentlydelete" runat="server" CommandName="permanentlydelete"
                                            CommandArgument='<%# Eval("catId") %>' Text="Delete Permanently" ToolTip="Delete Permanently"
                                            OnClientClick="return confirm('Are you sure you want to permanently delete this record?');"
                                            meta:resourcekey="lnkBtnPermanentlydeleteResource1"></asp:LinkButton>
                                    </span>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                <HeaderStyle HorizontalAlign="Left" Width="10%" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>--%>
                    <%--</div>
                                </ContentTemplate>
                            </cc1:TabPanel>
                        </cc1:TabContainer>--%>
                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- /no-padding -->
    <!--=== no-padding and table-tabletools ===-->
    <div class="row">
    </div>
    <!-- /no-padding and table-tabletools -->
    <!--=== no-padding and table-colvis ===-->
    <div class="row">
    </div>
    <!-- /no-padding and table-colvis -->
    <!--=== Horizontal Scrolling ===-->
    <div class="row">
    </div>
    <!-- /Normal -->
    <!-- /Page Content -->
        <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.js"></script>
       <script type="text/javascript" src="https://code.jquery.com/jquery.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            
            setTimeout(function () {
                SetExpandCollapse();
                //ExampanCollapsMenu();
                //ExampanCollapssubMenu();
                // tabMenuClick();
               // $(".Category").addClass("active_page");
            }, 500);
        });
        $(document).ready(function () {
            
        });

        function xyz() {
            return confirm($(".abcde").text());
            //$('.def').attr("onclick", "return confirm('" + $(".abcde").text() + "')");
        }
    </script>
</asp:Content>
