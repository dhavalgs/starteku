﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using starteku_BusinessLogic.Model;

public partial class Organisation_ActOvP : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            ActivityOverview();
            GetAllActivityCategory();
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
    }

    #region TAB1
    protected void ActivityOverview()
    {
        try
        {


            var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
            var userId = Convert.ToInt32(Session["OrgUserId"]);
            var actObj = new GetActivityLists_Result();


            actObj.ActCatRefId = 0;

            try
            {
                actObj.ActCatRefId = Convert.ToInt32(ddActCate.SelectedItem.Value);

            }
            catch (Exception)
            {


            }

            actObj.ActCompanyId = companyId;
            //actObj.ActReqUserId = userId;
            var actCatList = ActivityOverviewLogic.GetActivityOverviewList(actObj);


            gvActCat.DataSource = actCatList;
            gvActCat.DataBind();

        }
        catch (Exception ex)
        {

            ExceptionLogger.LogException(ex);
        }

    }
    protected void ActivityOverviewCount()
    {
        try
        {


            var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
            var userId = Convert.ToInt32(Session["OrgUserId"]);
            var actObj = new GetActivityLists_Result();


            actObj.ActCatRefId = 0;

            try
            {
                actObj.ActCatRefId = Convert.ToInt32(ddActCate.SelectedItem.Value);

            }
            catch (Exception)
            {


            }

            actObj.ActCompanyId = companyId;
            //actObj.ActReqUserId = userId;
            var actCatList = ActivityOverviewLogic.GetActivityOverviewList_Total(actObj);


            gvActCat.DataSource = actCatList;
            gvActCat.DataBind();

        }
        catch (Exception ex)
        {

            ExceptionLogger.LogException(ex);
        }

    }

    protected void OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string hdnActId = ((HiddenField)e.Row.FindControl("hdnActId")).Value;


            string id = gvActCat.DataKeys[e.Row.RowIndex].Value.ToString();

            var actObj = new GetActivityLists_Result();
            actObj.ActCompanyId = companyId;
            actObj.ActReqUserId = userId;
            actObj.ActId = Convert.ToInt32(hdnActId);



            var actCatList = ActivityOverviewLogic.GetPersonOverviewList(actObj);
            if (!actCatList.Any())
            {
                actCatList = new List<Overview_GetPersonStatus_Result>();

            }

            GridView gvPer = e.Row.FindControl("gvPer") as GridView;
            if (gvPer != null)
            {
                gvPer.DataSource = actCatList;
                gvPer.DataBind();

                //if (gvPer.HeaderRow != null) gvPer.HeaderRow.Visible = false;
            }
        }
    }

    #endregion



    #region TAB2
    protected void PersonOverviewTab2()
    {
        try
        {


            var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
            var userId = Convert.ToInt32(Session["OrgUserId"]);
            var actObj = new GetActivityLists_Result();


            actObj.ActCatRefId = 0;

            try
            {
                actObj.ActCatRefId = Convert.ToInt32(ddActCate.SelectedItem.Value);

            }
            catch (Exception)
            {


            }

            actObj.ActCompanyId = companyId;
            //actObj.ActReqUserId = userId;
            var actCatList = ActivityOverviewLogic.GetPersonOverviewListTab2(actObj);


            gv_Person_Master.DataSource = actCatList;
            gv_Person_Master.DataBind();

        }
        catch (Exception ex)
        {

            ExceptionLogger.LogException(ex);
        }

    }

    protected void PersonOverviewTab2_Count()
    {
        try
        {


            var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
            var userId = Convert.ToInt32(Session["OrgUserId"]);
            var actObj = new GetActivityLists_Result();


            actObj.ActCatRefId = 0;

            try
            {
                actObj.ActCatRefId = Convert.ToInt32(ddActCate.SelectedItem.Value);

            }
            catch (Exception)
            {


            }

            actObj.ActCompanyId = companyId;
            //actObj.ActReqUserId = userId;
            var actCatList = ActivityOverviewLogic.GetPersonOverviewListTab2_Total(actObj);


            gv_Person_Master.DataSource = actCatList;
            gv_Person_Master.DataBind();

        }
        catch (Exception ex)
        {

            ExceptionLogger.LogException(ex);
        }

    }
    protected void gv_Person_Master_OnRowDataBound(object sender, GridViewRowEventArgs e)
    {
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var userId = Convert.ToInt32(Session["OrgUserId"]);

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string hdnActId = ((HiddenField)e.Row.FindControl("hdnActId")).Value;


            string id = gv_Person_Master.DataKeys[e.Row.RowIndex].Value.ToString();

            var actObj = new GetActivityLists_Result();
            actObj.ActCompanyId = companyId;
            actObj.ActReqUserId = Convert.ToInt32(id);
            //actObj.ActReqActId = Convert.ToInt32(id);
            var actCatList = ActivityOverviewLogic.GetActivityOverviewList(actObj);
            if (!actCatList.Any())
            {
                actCatList = new List<Overview_GetActivityStatus_Result>();

            }

            GridView gv_Person_Child = e.Row.FindControl("gv_Person_Child") as GridView;
            if (gv_Person_Child != null)
            {
                gv_Person_Child.DataSource = actCatList;
                gv_Person_Child.DataBind();

                //if (gvPer.HeaderRow != null) gvPer.HeaderRow.Visible = false;
            }
        }
    }
    #endregion


    #region Tab3

    protected void Tab4_OnClick(object sender, EventArgs e)
    {
        GetActivityList_Tab4();

        ScriptManager.RegisterStartupScript(updtPnl, updtPnl.GetType()
                                                   , "TabVisiblity", "TabVisiblity(4);", true);


    }
    public void GetActivityList_Tab4()
    {
        var db = new startetkuEntities1();
        try
        {
            var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
            var userId = Convert.ToInt32(Session["OrgUserId"]);

            var actObj = new GetActivityLists_Result();
            actObj.ActCompanyId = companyId;
            actObj.ActReqUserId = userId;
            var activityList = ActivityMasterLogic.GetActivityList_Tab4(actObj);

            if (activityList.Any())
            {
                gvGrid_ActivityStatus_Tab4.DataSource = activityList;
                gvGrid_ActivityStatus_Tab4.DataBind();

                if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["OrguserType"])))
                {
                    var type = Convert.ToInt32(Session["OrguserType"]);

                    //if (type == 1)
                    //{
                    //    gvGrid_ActivityStatus_Tab4.Columns[10].Visible = false;
                    //}
                }
            }
            else
            {
                gvGrid_ActivityStatus_Tab4.DataSource = null;
                gvGrid_ActivityStatus_Tab4.DataBind();
            }

        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex);
        }
        finally
        {
            CommonAttributes.DisposeDBObject(ref db);
        }


    }
    protected void grd_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }

    protected void gvGrid_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            foreach (TableCell cell in e.Row.Cells)
            {
                cell.BorderStyle = BorderStyle.None;
            }
        }
    }

    public int total;
    protected void gvGrid_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {

            //string sponsorBonus = ((Label)e.Row.FindControl("Label2")).Text;

            //string count = ((HiddenField)e.Row.FindControl("hdnCost")).Value;
            string actReqStatus = ((HiddenField)e.Row.FindControl("hdnActReqStatus")).Value;
            Label lbl = (Label)e.Row.FindControl("lblActReqStatus");
            lbl.Text = ActivityRequestLogic.GetActivityStatusByStatusId(actReqStatus);



            /*Priority*/
            string hdnActPriority = ((HiddenField)e.Row.FindControl("hdnActPriority")).Value;
            Label lblActPriority = (Label)e.Row.FindControl("lblActPriority");


            lblActPriority.Text = ActivityRequestLogic.GetActivityPriorityStatusByNumber(hdnActPriority);


            //if (!string.IsNullOrWhiteSpace(count))
            //{
            //    if (actReqStatus == "2" || actReqStatus == "3" || actReqStatus == "5")
            //    {
            //        total = total + Int32.Parse(count);
            //    }

            //}

        }
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            var type = Convert.ToInt32(Session["OrguserType"]);
            Label lblTotalCost = (Label)e.Row.FindControl("lblTotalCost");
            lblTotalCost.Text = total.ToString();
            if (type != 1)
            {
                gvGrid_ActivityStatus_Tab4.ShowFooter = true;
            }
        }


    }
    protected void gvGrid_RowCommand(object sender, GridViewCommandEventArgs e)
    {


    }




    #endregion

    protected void GetAllActivityCategory()
    {

        var db = new startetkuEntities1();
        var getAllActCat = db.ActivityCategoryMasters.ToList();

        if (getAllActCat.Any())
        {

            ddActCate.DataSource = getAllActCat;
            ddActCate.DataTextField = "ActCatName";
            ddActCate.DataValueField = "ActCatId";
            ddActCate.DataBind();

            //ListItem li=new ListItem("All","0");
            ddActCate.Items.Insert(0, new ListItem("All", "0"));


        }
    }

    protected void ddActCate_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            ActivityOverview();
        }
        catch (Exception)
        {


        }
    }

    protected void ActivityVsActivityStatus_OnClick(object sender, EventArgs e)
    {
        ActivityOverview();
        ScriptManager.RegisterStartupScript(updtPnl, updtPnl.GetType()
                                                     , "TabVisiblity", "TabVisiblity(1);", true);
    }

    protected void PersonVsActivityStatus_OnClick(object sender, EventArgs e)
    {
        PersonOverviewTab2();
        ScriptManager.RegisterStartupScript(updtPnl, updtPnl.GetType()
                                                     , "alert", "TabVisiblity('2');", true);
    }

    protected void dd_Cout_Count_Tab1_OnSelectedIndexChanged(object sender, EventArgs e)
    {

        if (dd_Cout_Count_Tab1.SelectedItem.Value == "1")
        {
            ActivityOverview();
        }
        else
        {
            ActivityOverviewCount();

        }

    }

    protected void dd_Cout_Count_Tab2_OnSelectedIndexChanged(object sender, EventArgs e)
    {

        ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup2", "setTimeout(function(){Tab2Visible('Tab2');},500);", true);

        if (dd_Cout_Count_Tab2.SelectedItem.Value == "1")
        {
            PersonOverviewTab2();
        }
        else
        {
            PersonOverviewTab2_Count();

        }
        ScriptManager.RegisterStartupScript(updtPnl, updtPnl.GetType()
                                                     , "TabVisiblity", "TabVisiblity(2);", true);
    }


}