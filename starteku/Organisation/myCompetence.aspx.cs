﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Objects;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Web.Services;
using startetku.Business.Logic;
using starteku_BusinessLogic.Model;

public partial class Organisation_myCompetence : System.Web.UI.Page
{
    #region page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        

        Page.ClientScript.RegisterStartupScript(Page.GetType(), "my", " SetExpandCollapse();", true);

        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            Skills.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            subCompetenceSelectAll();
            //  getData();
            GetAllpoint();
            InsertUpdateLog();
            var userId = Convert.ToInt32(Session["OrgUserId"]);
            var userCompanyID = Convert.ToInt32(Session["OrgCompanyId"]);
            var db = new startetkuEntities1();
            var actEnaData = db.ActicityEnableMasters.FirstOrDefault(o => o.Aemcompid == userId || o.Aemcompid == userCompanyID);
            if (actEnaData != null)
            {
                Session["Aemcompid"] = actEnaData.Aemcompid;
            }
            else
            {
                Session["Aemcompid"] = null;
            }
        }
        /*Hide activity button if company has not permission for that*/
        if (string.IsNullOrWhiteSpace(Convert.ToString(Session["Aemcompid"])))
        {
            btnActivity.Visible = false;
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region method

   
    public void InsertUpdateLog()
    {
        Common.WriteLog("InsertUpdateLog in competence skill");

        //Lets log : current user goin to read doc.
        var userId = Convert.ToInt32(Session["OrgUserId"]);
        string cacheKey = string.Format("{0}-{1}", "OpenCompetence", userId);
        object cacheItem = HttpRuntime.Cache.Get(cacheKey) as string;

        var cookieVal =CookieMasterLogic.DbCookieNew(cacheKey, DateTime.Now.ToString(), DateTime.Now);

        try
        {
            if (!string.IsNullOrWhiteSpace(cookieVal))
            {
                return;
            }
            else
            {
                cacheItem = DateTime.Now.ToString();
                HttpRuntime.Cache.Insert(cacheKey, cacheItem, null, DateTime.Now.AddDays(1), TimeSpan.Zero);
            }
            var allowPoint = true;

            //if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["userDateTime"])))
            //{
            //    var lastlogintime = Convert.ToDateTime(Session["userDateTime"]);
            //    Common.WriteLog("lastlogintime" + lastlogintime);
            //    var timespan = DateTime.Now - lastlogintime;
            //    Common.WriteLog("timespan" + timespan);


            //    if (timespan.TotalHours > 24)
            //    {
            //        Common.WriteLog("allowPoint");
            //        allowPoint = true;
            //    }
            //    else
            //    {
            //        Common.WriteLog("notallowPoint");
            //        allowPoint = false;
            //    }
            //}
            // insert point
            var db = new startetkuEntities1();
            var username = (from user in db.UserMasters where user.userId == userId select new { user.userFirstName, user.userLastName }).FirstOrDefault();
            var point = LogMasterLogic.InsertUpdateLogParam(
                userId,
                Common.PLP(), Common.OpenCompetencesScreen(),
                string.Format("UserID : {0} Open Competences screen", username.userFirstName + " " + username.userLastName), allowPoint, 0, 0);
            Common.WriteLog("point" + point);
            if (allowPoint)
                PointBM.InsertUpdatePoint(userId, point, 0);
            //
        }
        catch (DataException ex)
        {
            Common.WriteLog("error in InsertUpdateLog  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            throw ex;
        }

        try
        {
            //return;
            //LogMaster obj = new LogMaster();
            //obj.LogIsActive = true;
            //obj.LogIsDeleted = false;
            //obj.LogCreateDate = DateTime.Now;
            //obj.LogIp = Common.GetVisitorIPAddress(true);
            //obj.LogUserId = Convert.ToInt32(Session["OrgUserId"]);
            //obj.LogCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            //obj.LogUserType = Convert.ToInt32(Session["OrguserType"]);
            //obj.LogDescription = obj.UserLogin();   // User log description..
            //obj.LogOutTime = new DateTime();
            //obj.LogCreateDate = DateTime.Now;
            //obj.LogBrowser = Common.GetWebBrowserName();
            //obj.LogPointInfo = string.Empty;
            //Common.WriteLog("obj.LogBrowser" + obj.LogBrowser);
            //// 
            //Common.WriteLog("obj.LogUserType" + obj.LogUserType);
            //if (obj.LogUserType == 3)
            //{
            //    Common.WriteLog("userDateTime" + Convert.ToString(Session["userDateTime"]));
            //    if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["userDateTime"])))
            //    {
            //        var lastlogintime = Convert.ToDateTime(Session["userDateTime"]);
            //        Common.WriteLog("lastlogintime" + lastlogintime);
            //        var timespan = DateTime.Now - lastlogintime;
            //        Common.WriteLog("timespan" + timespan);
            //        if (timespan.TotalHours > 24)
            //        {
            //            Common.WriteLog("SignIn");
            //            obj.LogPLP = LogMaster.GetPointByPointName("SignIn");
            //            Common.WriteLog("obj.LogPLP" + obj.LogPLP);
            //        }
            //        else
            //        {
            //            obj.LogPLP = 0;
            //        }
            //    }
            //    else
            //    {
            //        obj.LogPLP = LogMaster.GetPointByPointName("SignIn");
            //        Common.WriteLog("obj.LogPLP_else" + obj.LogPLP);
            //    }
            //}


            //obj.InsertUpdateLog();
        }
        catch (DataException ex)
        {
            Common.WriteLog("error in InsertUpdateLog  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }
    //public void getData()
    //{
    //    CompetenceMasterBM obj = new CompetenceMasterBM();
    //    obj.GetAllCategoryBypenddingrequest(Convert.ToInt32(Session["OrgUserId"]));
    //    DataSet ds = obj.ds;
    //    Accordion1.DataSource = ds.Tables[0].DefaultView;
    //    Accordion1.DataBind();
    //}
    //private DataTable GetAllCompetenceAddbyComCatId(int categoryId)
    //{
    //    SubCompetenceBM obj = new SubCompetenceBM();
    //    obj.subIsActive = true;
    //    obj.subIsDeleted = false;
    //    obj.GetCompotenceAddForDeshbordbyAccordion(Convert.ToInt32(Session["OrgUserId"]), categoryId);

    //    DataSet ds = obj.ds;

    //    if (ds != null)
    //    {
    //        int selectTable = 0;

    //        if (ds.Tables[selectTable].Rows.Count > 0)
    //        {
    //            return ds.Tables[selectTable];

    //        }
    //        else
    //        {

    //        }

    //    }
    //    return null;
    //}
    protected void GetAllpoint()
    {
        try
        {
            EmployeeSkillBM obj = new EmployeeSkillBM();
            obj.skillIsActive = true;
            obj.skillIsDeleted = false;
            obj.skillCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.GetEmpSkill();
            DataSet ds = obj.ds;
            ViewState["point"] = ds;
        }
        catch (Exception ex)
        {
            Common.WriteLog("error in GetAllpoint" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }

    protected void subCompetenceSelectAll()
    {
        try
        {
            SubCompetenceBM obj = new SubCompetenceBM();
            obj.subIsActive = true;
            obj.subIsDeleted = false;
            obj.GetCompotenceAddForDeshbord(Convert.ToInt32(Session["OrgUserId"]));
            DataSet ds = obj.ds;
            if (Convert.ToString(Session["Culture"]) == "Danish")
            {
                ds.Tables[0].Columns["comCompetence"].ColumnName = "abcd";
                ds.Tables[0].Columns["comCompetenceDN"].ColumnName = "comCompetence";

            }
            ViewState["data"] = ds;

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Int32 id = 3;
                    DataTable dtTop = ds.Tables[0].Rows.Cast<DataRow>().Take(id).CopyToDataTable();  //Take top 3
                    DataTable dtBottom = ds.Tables[0].Rows.Cast<DataRow>().Skip(ds.Tables[0].Rows.Count - id).CopyToDataTable(); // Take bottom 3

                    rptAll.DataSource = ds;
                    rptAll.DataBind();

                    rep_competence.DataSource = dtTop;
                    rep_competence.DataBind();

                    DataView dv = new DataView();
                    dv = dtBottom.DefaultView;
                    dv.Sort = "skillAchive asc";

                    rep_competencehigh.DataSource = dv;
                    rep_competencehigh.DataBind();
                }
                else
                {
                    rptAll.DataSource = null;
                    rptAll.DataBind();

                    rep_competence.DataSource = null;
                    rep_competence.DataBind();

                    rep_competencehigh.DataSource = null;
                    rep_competencehigh.DataBind();
                }
            }
            else
            {
                //rptAll.DataSource = null;
                //rptAll.DataBind();

                rep_competence.DataSource = null;
                rep_competence.DataBind();

                rep_competencehigh.DataSource = null;
                rep_competencehigh.DataBind();
            }
        }
        catch (Exception ex)
        {
            Common.WriteLog("error in subCompetenceSelectAll" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {

        string language = Convert.ToString(Session["Culture"]);

        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{

        //    if (language.EndsWith("Danish"))
        //    {
        //        languageId = "da-DK";
        //    }
        //    else
        //    {   
        //        languageId = "en-GB";
        //    }
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion

    #region Repeater Event
    //protected void Accordion1_ItemDataBound(object sender, AjaxControlToolkit.AccordionItemEventArgs e)
    //{
    //    if (e.ItemType == AjaxControlToolkit.AccordionItemType.Content)
    //    {
    //        Repeater rptAll = new Repeater();
    //        rptAll = (Repeater)e.AccordionItem.FindControl("rptAll");

    //        String categoryID = ((HiddenField)e.AccordionItem.FindControl("txt_categoryID")).Value;
    //        var data = GetAllCompetenceAddbyComCatId(Convert.ToInt32(categoryID));
    //        if (data == null)
    //        {
    //           // rptAll.DataBind();
    //            return;
    //        }
    //        DataSet ds = data.DataSet;
    //        ViewState["data"] = ds;

    //        rptAll.DataSource = ds;
    //        rptAll.DataBind();
    //    }
    //}
    protected void R1_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        try
        {
            //if (rep_competence.Items.Count < 1)
            //{
            //    if (e.Item.ItemType == ListItemType.Footer)
            //    {
            //        HtmlTableRow tr = (HtmlTableRow)e.Item.FindControl("Emptyrow");
            //        tr.Visible = true;
            //    }
            //}
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Button lbltarget = e.Item.FindControl("btnlocal") as Button;
                Button btnNeedhelp = e.Item.FindControl("btnNeedhelp") as Button;
                Label lblLtotal = e.Item.FindControl("lblLtotal") as Label;
                Label lblGtotal = e.Item.FindControl("lblGtotal") as Label;

                //Saurin: 20150108 

                HiddenField comId = e.Item.FindControl("comId") as HiddenField;

                //var obj = new InvitationBM();
                //obj.invStatus = 1;
                //if (comId != null) obj.invComId = Convert.ToInt32(comId.Value);
                //obj.invIsDeleted = false;
                //obj.invIsActive = true;
                //obj.invFromUserId = Convert.ToInt32(Convert.ToInt32(Session["orgUserId"]));
                //obj.invType = "Provide Help";
                //obj.GetInvitionStatusTotal();
                //DataSet ds = obj.ds;
                ////ViewState["data"] = ds;
                //if (ds != null)
                //{
                //    if (ds.Tables[0].Rows.Count > 0)
                //    {
                //        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                //        {
                //            if (lbltarget != null) lbltarget.Text = Convert.ToString(ds.Tables[0].Rows[i]["total"]);

                //        }
                //    //rep_tabmenu.DataSource = ds;
                //        //rep_tabmenu.DataBind();
                //        //rep_content.DataSource = ds;
                //        //rep_content.DataBind();

                //    }
                //}
                //if ((Convert.ToInt32(lbltarget.Text))<1)
                //{
                //    lbltarget.Visible = false;
                //    lblLtotal.Visible = true;
                //    lblLtotal.Text = "N/A";

                //}


                //GetInvitionStatusTotal();



                if (lbltarget.Text == "0")
                {
                    lbltarget.Visible = false;
                    lblLtotal.Visible = true;
                }
                else
                {
                    lbltarget.Text = View.Text + " (" + lbltarget.Text + ")";
                }
                if (btnNeedhelp.Text == "0")
                {
                    btnNeedhelp.Visible = false;
                    lblGtotal.Visible = true;
                }
                else
                {
                    btnNeedhelp.Text = RequestHelp.Text + " (" + btnNeedhelp.Text + ")";
                }


                //Label lbpoint = e.Item.FindControl("lbpoint") as Label;
                //Label lbllocal = e.Item.FindControl("lbllocal") as Label;
                //Label lblAchive = e.Item.FindControl("lblAchive") as Label;
                //Label lbltarget = e.Item.FindControl("lbltarget") as Label;
                //Label lblset = e.Item.FindControl("lblset") as Label;
                //DataSet ds = (DataSet)ViewState["data"];
                //if (ds != null)
                //{
                //if (ds.Tables[0].Rows.Count > 0)
                //{
                //    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                //    {                            //comId
                //        if (Convert.ToInt32(comId.Value) == Convert.ToInt32(ds.Tables[0].Rows[i]["skillComId"]))
                //        {
                //            if (!String.IsNullOrEmpty((Convert.ToString(ds.Tables[0].Rows[i]["skillLocal"]))))
                //            {
                //                lbllocal.Text = (Convert.ToString(ds.Tables[0].Rows[i]["skillLocal"]));
                //            }
                //            if (!String.IsNullOrEmpty((Convert.ToString(ds.Tables[0].Rows[i]["skillAchive"]))))
                //            {
                //                lblAchive.Text = (Convert.ToString(ds.Tables[0].Rows[i]["skillAchive"]));
                //            }
                //            if (!String.IsNullOrEmpty((Convert.ToString(ds.Tables[0].Rows[i]["skilltarget"]))))
                //            {
                //                lbltarget.Text = (Convert.ToString(ds.Tables[0].Rows[i]["skilltarget"]));
                //            }
                //            if (!String.IsNullOrEmpty((Convert.ToString(ds.Tables[0].Rows[i]["skillSet"]))))
                //            {
                //                //lblset.Text = (Convert.ToString(ds.Tables[0].Rows[i]["skillSet"]));
                //                lblset.Text = "Yes";
                //            }
                //        }
                //    }
                //}




                //
                // }
            }
            //DataSet dsdata = (DataSet)ViewState["data"];
            //if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            //{
            //    HiddenField hbsubId = e.Item.FindControl("subId") as HiddenField;
            //    Label lbpoint = e.Item.FindControl("lbpoint") as Label;
            //    //Label lblsubcom = e.Item.FindControl("lblsubcom") as Label;
            //    DataView dv = new DataView();
            //    dv = ds1.Tables[0].DefaultView;
            //    dv.RowFilter = "helpUserIdHelped = '" + Convert.ToInt32(Session["OrgUserId"]) + "'";
            //    DataTable dtitm = dv.ToTable();
            //    DataSet ds = new DataSet();
            //    ds.Tables.Add(dtitm);
            //    if (ds != null)
            //    {
            //        if (ds.Tables[0].Rows.Count > 0)
            //        {
            //            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            //            {
            //                if (Convert.ToInt32(hbsubId.Value) == Convert.ToInt32(ds.Tables[0].Rows[i]["helpsubComId"]))
            //                {
            //                    Button btnhelp = e.Item.FindControl("btnhelp") as Button;
            //                    btnhelp.Visible = false;
            //                    //lbpoint.Text = (Convert.ToString(ds.Tables[1].Rows[i]["skillPoint"]));
            //                    //lblsubcom.ForeColor = System.Drawing.Color.Green;
            //                    //((HtmlGenericControl)(e.Item.FindControl("myDiv"))).Attributes["class"] = "Completed_satus";
            //                    //((CheckBox)(e.Item.FindControl("checkbox"))).Visible = false;
            //                }
            //            }
            //        }
            //    }

            //DataView dvdata = new DataView();
            // dvdata = dsdata.Tables[1].DefaultView;
            // dvdata.RowFilter = "PointUserId = '" + Convert.ToInt32(Session["OrgUserId"]) + "'";
            // DataTable dtitmdata = dvdata.ToTable();
            // DataSet dsdatanew = new DataSet();
            // dsdatanew.Tables.Add(dtitmdata);  
            //if (dsdatanew != null)
            //{
            //    if (dsdatanew.Tables[0].Rows.Count > 0)
            //    {
            //        for (int i = 0; i < dsdatanew.Tables[0].Rows.Count; i++)
            //        {
            //            if (Convert.ToInt32(hbsubId.Value) == Convert.ToInt32(dsdatanew.Tables[0].Rows[i]["pintSubCompetance"]))
            //            {
            //                if (!String.IsNullOrEmpty(Convert.ToString(dsdatanew.Tables[0].Rows[i]["PointAnsCount"])))
            //                {
            //                    lbpoint.Text = (Convert.ToString(dsdatanew.Tables[0].Rows[i]["PointAnsCount"]));
            //                }
            //                else
            //                {
            //                    lbpoint.Text = "0";
            //                }

            //            }
            //        }
            //    }
            //}
            //    }
        }
        catch (Exception ex)
        {
            Common.WriteLog("error in R1_ItemDataBound" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }
    protected void Repeater1_OnItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "Local12")
        {
            try
            {
                string lblAchive = ((Label)e.Item.FindControl("lblAchive")).Text;
                Int32 skillComId = Convert.ToInt32(e.CommandArgument);
                DataSet dtlocal = new DataSet();
                dtlocal = (DataSet)ViewState["point"];
                DataView dv = new DataView();
                dv = dtlocal.Tables[0].DefaultView;
                //dv.RowFilter = "userJobType = '" + Convert.ToInt32(Session["userJobType"]) + "'";
                //dv.RowFilter = "userJobType = '" + Convert.ToInt32(Session["userJobType"]) + "' AND PointAnsCount < '" + Convert.ToInt32(col1) + "' AND pintSubCompetance = '" + Convert.ToInt32(sid) + "'";
                dv.RowFilter = "skillAchive < '" + Convert.ToInt32(lblAchive) + "' AND skillComId = '" + Convert.ToInt32(skillComId) + "'";
                DataTable dtitm = dv.ToTable();
                DataSet ds = new DataSet();
                ds.Tables.Add(dtitm);
                //if (Common.IsDatasetvalid(ds))
                //{
                //rptlocal.DataSource = ds;
                //rptlocal.DataBind();
                //mpe.Show();
                //}
                //else
                //{
                //    string res = "<script>alert('" + CommonModule.msgRecordNotfound + "');</script>";

                //}
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in Repeater1_OnItemCommand" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            }
        }

        //else if (e.CommandName == "All")
        //{
        //    string col1 = ((Label)e.Item.FindControl("lbpoint")).Text;
        //    Int32 sid = Convert.ToInt32(e.CommandArgument);

        //    DataSet dtlocal = new DataSet();
        //    dtlocal = (DataSet)ViewState["point"];
        //    DataView dv = new DataView();
        //    dv = dtlocal.Tables[0].DefaultView;
        //    dv.RowFilter = "PointAnsCount < '" + Convert.ToInt32(col1) + "' AND pintSubCompetance = '" + Convert.ToInt32(sid) + "'";
        //    DataTable dtitm = dv.ToTable();
        //    DataSet ds = new DataSet();
        //    ds.Tables.Add(dtitm);
        //    //if (Common.IsDatasetvalid(ds))
        //    //{
        //    //rptlocal.DataSource = ds;
        //    //rptlocal.DataBind();
        //    //mpe.Show();
        //    //}
        //    //else
        //    //{
        //    //   
        //    //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script>alert('" + CommonModule.msgRecordNotfound + "');</script>", false);

        //    //}
        //}
        //else if (e.CommandName == "catNameClick")
        //{
        //    string col1 = ((Label)e.Item.FindControl("lbpoint")).Text;
        //    LinkButton lk = (LinkButton)e.Item.FindControl("lnkCatName");
        //    Int32 sid = Convert.ToInt32(e.CommandArgument);
        //    Session["comId"] = sid;
        //    CompetenceMasterBM obj = new CompetenceMasterBM();
        //    obj.comIsActive = true;
        //    obj.comIsDeleted = false;
        //    obj.comchildcomID = Convert.ToInt32(sid);
        //    obj.GetAllchildCompetenceskill();
        //    DataSet ds = obj.ds;
        //    //ViewState["data"] = ds;
        //    if (ds != null)
        //    {
        //        if (ds.Tables[0].Rows.Count > 0)
        //        {
        //            //rep_tabmenu.DataSource = ds;
        //            //rep_tabmenu.DataBind();
        //            //rep_content.DataSource = ds;
        //            //rep_content.DataBind();

        //        }
        //    }
        //    //ScriptManager.GetCurrent(Page).RegisterPostBackControl(rep_tabmenu);
        //    // ScriptManager.GetCurrent(Page).RegisterPostBackControl(rep_content);
        //    //Timer1.Enabled = true;
        //}
    }
    protected void rep_competencehigh_OnItemCommand(object source, RepeaterCommandEventArgs e)
    {
        //if (e.CommandName == "Local")
        //{
        //    try
        //    {
        //        string col1 = ((Label)e.Item.FindControl("lbpoint")).Text;
        //        Int32 sid = Convert.ToInt32(e.CommandArgument);
        //        DataSet dtlocal = new DataSet();
        //        dtlocal = (DataSet)ViewState["point"];
        //        DataView dv = new DataView();
        //        dv = dtlocal.Tables[0].DefaultView;
        //        //dv.RowFilter = "userJobType = '" + Convert.ToInt32(Session["userJobType"]) + "'";
        //        dv.RowFilter = "userJobType = '" + Convert.ToInt32(Session["userJobType"]) + "' AND PointAnsCount > '" + Convert.ToInt32(col1) + "' AND pintSubCompetance = '" + Convert.ToInt32(sid) + "'";
        //        DataTable dtitm = dv.ToTable();
        //        DataSet ds = new DataSet();
        //        ds.Tables.Add(dtitm);
        //        //if (Common.IsDatasetvalid(ds))
        //        //{
        //        //rpllocalhigh.DataSource = ds;
        //        //rpllocalhigh.DataBind();
        //        ////mpe.Show();
        //        //ModalPopupExtender1.Show();
        //        //}


        //    }
        //    catch (Exception ex)
        //    {
        //        Common.WriteLog("error in Repeater1_OnItemCommand" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
        //    }
        //}

        //else if (e.CommandName == "All")
        //{
        //    string col1 = ((Label)e.Item.FindControl("lbpoint")).Text;
        //    Int32 sid = Convert.ToInt32(e.CommandArgument);

        //    DataSet dtlocal = new DataSet();
        //    dtlocal = (DataSet)ViewState["point"];
        //    DataView dv = new DataView();
        //    dv = dtlocal.Tables[0].DefaultView;
        //    dv.RowFilter = "PointAnsCount > '" + Convert.ToInt32(col1) + "' AND pintSubCompetance = '" + Convert.ToInt32(sid) + "'";
        //    DataTable dtitm = dv.ToTable();
        //    DataSet ds = new DataSet();
        //    ds.Tables.Add(dtitm);
        //    //if (Common.IsDatasetvalid(ds))
        //    //{
        //    //rpllocalhigh.DataSource = ds;
        //    //rpllocalhigh.DataBind();
        //    //ModalPopupExtender1.Show();
        //    //}

        //}
        //else if (e.CommandName == "catNameClick")
        //{
        //    string col1 = ((Label)e.Item.FindControl("lbpoint")).Text;
        //    LinkButton lk = (LinkButton)e.Item.FindControl("lnkCatName");
        //    Int32 sid = Convert.ToInt32(e.CommandArgument);
        //    Session["comId"] = sid;
        //    CompetenceMasterBM obj = new CompetenceMasterBM();
        //    obj.comIsActive = true;
        //    obj.comIsDeleted = false;
        //    obj.comchildcomID = Convert.ToInt32(sid);
        //    obj.GetAllchildCompetenceskill();
        //    DataSet ds = obj.ds;
        //    //ViewState["data"] = ds;
        //    if (ds != null)
        //    {
        //        if (ds.Tables[0].Rows.Count > 0)
        //        {
        //            //rep_tabmenu.DataSource = ds;
        //            //rep_tabmenu.DataBind();
        //            //rep_content.DataSource = ds;
        //            //rep_content.DataBind();

        //        }
        //    }
        //    //ScriptManager.GetCurrent(Page).RegisterPostBackControl(rep_tabmenu);
        //    // ScriptManager.GetCurrent(Page).RegisterPostBackControl(rep_content);
        //    // Timer1.Enabled = true;
        //}
    }
    protected void rep_competencehigh_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        try
        {
            if (rep_competencehigh.Items.Count < 1)
            {
                if (e.Item.ItemType == ListItemType.Footer)
                {
                    HtmlTableRow tr = (HtmlTableRow)e.Item.FindControl("Emptyrow");
                    tr.Visible = true;
                }
            }
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Button lbltarget = e.Item.FindControl("btnlocal") as Button;
                Button btnNeedhelp = e.Item.FindControl("btnNeedhelp") as Button;
                Label lblLtotal = e.Item.FindControl("lblLtotal") as Label;
                Label lblGtotal = e.Item.FindControl("lblGtotal") as Label;
                if (lbltarget.Text == "0")
                {
                    lbltarget.Visible = false;
                    lblLtotal.Visible = true;
                }
                else
                {
                    lbltarget.Text = View.Text + " (" + lbltarget.Text + ")";
                }
                if (btnNeedhelp.Text == "0")
                {
                    btnNeedhelp.Visible = false;
                    lblGtotal.Visible = true;
                }
                else
                {
                    btnNeedhelp.Text = RequestHelp.Text + " (" + btnNeedhelp.Text + ")";
                }
            }
        }
        catch (Exception ex)
        {
            Common.WriteLog("error in rep_competencehigh_ItemDataBound" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }
    protected void rptAll_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        try
        {
            //if (rep_competence.Items.Count < 1)
            //{
            //    if (e.Item.ItemType == ListItemType.Footer)
            //    {
            //        HtmlTableRow tr = (HtmlTableRow)e.Item.FindControl("Emptyrow");
            //        tr.Visible = true;
            //    }
            //}
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                Button lbltarget = e.Item.FindControl("btnlocal") as Button;
                Button btnNeedhelp = e.Item.FindControl("btnNeedhelp") as Button;
                Label lblLtotal = e.Item.FindControl("lblLtotal") as Label;
                Label lblGtotal = e.Item.FindControl("lblGtotal") as Label;
                if (lbltarget.Text == "0")
                {
                    lbltarget.Visible = false;
                    lblLtotal.Visible = true;
                }
                else
                {
                    lbltarget.Text = View.Text + " (" + lbltarget.Text + ")";
                }
                if (btnNeedhelp.Text == "0")
                {
                    btnNeedhelp.Visible = false;
                    lblGtotal.Visible = true;
                }
                else
                {
                    btnNeedhelp.Text = RequestHelp.Text + " (" + btnNeedhelp.Text + ")";

                }
            }

        }
        catch (Exception ex)
        {
            Common.WriteLog("error in R1_ItemDataBound" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }
    protected void rptAll_OnItemCommand(object source, RepeaterCommandEventArgs e)
    {
        //if (e.CommandName == "Local")
        //{
        //    try
        //    {
        //        string col1 = ((Label)e.Item.FindControl("lbpoint")).Text;
        //        Int32 sid = Convert.ToInt32(e.CommandArgument);
        //        DataSet dtlocal = new DataSet();
        //        dtlocal = (DataSet)ViewState["point"];
        //        DataView dv = new DataView();
        //        dv = dtlocal.Tables[0].DefaultView;
        //        //dv.RowFilter = "userJobType = '" + Convert.ToInt32(Session["userJobType"]) + "'";
        //        dv.RowFilter = "userJobType = '" + Convert.ToInt32(Session["userJobType"]) + "' AND PointAnsCount < '" + Convert.ToInt32(col1) + "' AND pintSubCompetance = '" + Convert.ToInt32(sid) + "'";
        //        DataTable dtitm = dv.ToTable();
        //        DataSet ds = new DataSet();
        //        ds.Tables.Add(dtitm);
        //        //if (Common.IsDatasetvalid(ds))
        //        //{
        //        //rptlocal.DataSource = ds;
        //        //rptlocal.DataBind();
        //        //mpe.Show();
        //        //}
        //        //else
        //        //{
        //        //string res = "<script>alert('" + CommonModule.msgRecordNotfound + "');</script>";

        //        //}
        //    }
        //    catch (Exception ex)
        //    {
        //        Common.WriteLog("error in Repeater1_OnItemCommand" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
        //    }
        //}

        //else if (e.CommandName == "All")
        //{
        //    string col1 = ((Label)e.Item.FindControl("lbpoint")).Text;
        //    Int32 sid = Convert.ToInt32(e.CommandArgument);

        //    DataSet dtlocal = new DataSet();
        //    dtlocal = (DataSet)ViewState["point"];
        //    DataView dv = new DataView();
        //    dv = dtlocal.Tables[0].DefaultView;
        //    dv.RowFilter = "PointAnsCount < '" + Convert.ToInt32(col1) + "' AND pintSubCompetance = '" + Convert.ToInt32(sid) + "'";
        //    DataTable dtitm = dv.ToTable();
        //    DataSet ds = new DataSet();
        //    ds.Tables.Add(dtitm);
        //    //if (Common.IsDatasetvalid(ds))
        //    //{
        //    //rptlocal.DataSource = ds;
        //    //rptlocal.DataBind();
        //    //mpe.Show();
        //    //}
        //    //else
        //    //{
        //    //   
        //    //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script>alert('" + CommonModule.msgRecordNotfound + "');</script>", false);

        //    //}
        //}
        //else if (e.CommandName == "catNameClick")
        //{
        //    string col1 = ((Label)e.Item.FindControl("lbpoint")).Text;
        //    LinkButton lk = (LinkButton)e.Item.FindControl("lnkCatName");
        //    Int32 sid = Convert.ToInt32(e.CommandArgument);
        //    Session["comId"] = sid;
        //    CompetenceMasterBM obj = new CompetenceMasterBM();
        //    obj.comIsActive = true;
        //    obj.comIsDeleted = false;
        //    obj.comchildcomID = Convert.ToInt32(sid);
        //    obj.GetAllchildCompetenceskill();
        //    DataSet ds = obj.ds;
        //    //ViewState["data"] = ds;
        //    if (ds != null)
        //    {
        //        if (ds.Tables[0].Rows.Count > 0)
        //        {
        //            //rep_tabmenu.DataSource = ds;
        //            //rep_tabmenu.DataBind();
        //            //rep_content.DataSource = ds;
        //            //rep_content.DataBind();

        //        }
        //    }
        //    //ScriptManager.GetCurrent(Page).RegisterPostBackControl(rep_tabmenu);
        //    // ScriptManager.GetCurrent(Page).RegisterPostBackControl(rep_content);
        //    //Timer1.Enabled = true;
        //}
    }
    protected void rptlocal_ItemDataBound(Object Sender, RepeaterItemEventArgs e)
    {
        try
        {
            //if (rptlocal.Items.Count < 1)
            //{
            //    if (e.Item.ItemType == ListItemType.Footer)
            //    {
            //        HtmlTableRow tr = (HtmlTableRow)e.Item.FindControl("Emptyrow");
            //        tr.Visible = true;

            //    }
            //}
            //DataSet ds = (DataSet)ViewState["point"];
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //HiddenField userId = e.Item.FindControl("userId") as HiddenField;
                //HiddenField pintSubCompetance = e.Item.FindControl("pintSubCompetance") as HiddenField;
                //HiddenField hdnhelpstatus = e.Item.FindControl("hdnhelpstatus") as HiddenField;
                //if (!String.IsNullOrEmpty(hdnhelpstatus.Value))
                //{
                //    if (Convert.ToBoolean(hdnhelpstatus.Value) == true)
                //    {
                //        HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("tdControl");
                //        td.Attributes.Add("style", "background-color:Greenyellow;");

                //    }
                //    else
                //    {
                //        HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("tdControl");
                //        td.Attributes.Add("style", "background-color:Red;");
                //    }
                //}
                //else
                //{
                //    HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("tdControl");
                //    td.Attributes.Add("style", "background-color:Red;");

                //}
                //DataView dv = new DataView();
                //dv = ds1.Tables[0].DefaultView;
                //dv.RowFilter = "helpUserIdHelped = '" + Convert.ToInt32(Session["OrgUserId"]) + "'";
                //DataTable dtitm = dv.ToTable();
                //DataSet ds = new DataSet();
                //ds.Tables.Add(dtitm);
                //if (ds != null)
                //{
                //    if (ds.Tables[0].Rows.Count > 0)
                //    {
                //        for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                //        {
                //            //if (Convert.ToInt32(userId.Value) == Convert.ToInt32(ds.Tables[0].Rows[i]["userId"]))
                //            if ((Convert.ToInt32(userId.Value) == Convert.ToInt32(ds.Tables[0].Rows[i]["userId"]))) //&& (Convert.ToInt32(pintSubCompetance.Value) == Convert.ToInt32(ds.Tables[1].Rows[i]["helpstatus"])))
                //            {
                //                if (ds.Tables[1].Rows.Count > 0)
                //                {
                //                    for (int j = 0; j < ds.Tables[1].Rows.Count; j++)
                //                    {
                //                        if ((Convert.ToInt32(userId.Value) == Convert.ToInt32(ds.Tables[1].Rows[j]["helpuserIdhelped"])) && (true == Convert.ToBoolean(ds.Tables[1].Rows[j]["helpstatus"])))
                //                        {
                //                            HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("tdControl");
                //                            td.Attributes.Add("style", "background-color:Greenyellow;");
                //                        }
                //                        else
                //                        {
                //                            HtmlTableCell td = (HtmlTableCell)e.Item.FindControl("tdControl");
                //                            td.Attributes.Add("style", "background-color:Red;");
                //                        }
                //                    }
                //                }

                //            }
                //        }
                //    }
                //}

            }
        }
        catch (Exception ex)
        {
            Common.WriteLog("error in rptlocal_ItemDataBound" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }
    protected void rptlocal_ItemCommand(object source, RepeaterCommandEventArgs e)
    {
        if (e.CommandName == "popupcal")
        {
            try
            {
                //HiddenField hdnhelpstatus = e.Item.FindControl("hdnhelpstatus") as HiddenField;
                //if (!String.IsNullOrEmpty(hdnhelpstatus.Value))
                //{
                //    if (Convert.ToBoolean(hdnhelpstatus.Value) == true)
                //    {
                //        HiddenField pintSubCompetance = e.Item.FindControl("pintSubCompetance") as HiddenField;
                //        HiddenField userFirstName = e.Item.FindControl("userFirstName") as HiddenField;
                //        Int32 userId = Convert.ToInt32(e.CommandArgument);
                //        Session["userId"] = Convert.ToInt32(userId);
                //        Session["sid"] = pintSubCompetance.Value;
                //        Session["chechtype"] = "0";
                //        txtcname.Text = userFirstName.Value;
                //        mpecal.Show();

                //    }
                //    else
                //    {

                //    }
                //}
                //else
                //{
                //    HiddenField pintSubCompetance = e.Item.FindControl("pintSubCompetance") as HiddenField;
                //    HiddenField userFirstName = e.Item.FindControl("userFirstName") as HiddenField;
                //    Int32 userId = Convert.ToInt32(e.CommandArgument);
                //    Session["userId"] = Convert.ToInt32(userId);
                //    Session["sid"] = pintSubCompetance.Value;
                //    txtcname.Text = userFirstName.Value;
                //    Session["chechtype"] = "0";
                //    mpecal.Show();
                //}
            }
            catch (Exception ex)
            {
                Common.WriteLog("error in rptlocal_ItemCommand" + "    error message " + ex.Message + "   error stack trace " + ex.StackTrace + " " + ex.Source + "    inner exception" + Convert.ToString(ex.InnerException));
            }
        }
    }
    #endregion

    protected void btnBack_click(object sender, EventArgs e)
    {
        switch (Convert.ToInt32(Session["OrguserType"]))
        {
            case 2:
                Response.Redirect("Manager-dashboard.aspx");
                break;
            case 1:
                Response.Redirect("Organisation-dashboard.aspx");
                break;
            default:
                Response.Redirect("Employee_dashboard.aspx");
                break;
        }
    }

    protected void RedirectToPDP(object sender, EventArgs e)
    {
        Response.Redirect("PersonalDevelopmentPlan.aspx?umId=" +  Convert.ToInt32(Session["OrgUserId"]));
    }
}