var data;
var userNameForChartDisplay;
//function GetGraphById(id, name) {
//    //    alert('getdata by id');

//    GetData(id);
//    if (name == '') {
//        //name = "Manager";
//        name = "All Employees"
//    }

//    $("#lblUserName").text($("#hdnGraphName").val() + " " + name);
//}

function GetGraphById(id, name) {
    
    $('#ContentPlaceHolder1_hdnGraphUserSelectedID').val(id);

    GetData(id);
    if (name == '') {
        // name = "Manager";
        //  name = "All Employees"
        name = $("#lblUserName").text();
    }

    $("#lblUserName").text($("#hdnGraphName").val() + " " + name);
}
function GetGraphByManagerId(id, name) {

    $('#ContentPlaceHolder1_hdnGraphUserSelectedID').val(id);
    GetData_Manager(id);
    if (name == '') {
        // name = "All Manager";
        name = $("#ContentPlaceHolder1_lblAllManager1").val();

        //name = "All Employees"
    }

    $("#lblUserName").text($("#hdnGraphName").val() + " " + name);
}


//call in System Owner
function GetDataByDivJobTypeID(dataNeedsOf, divid, jobtypeid) {
    // ie. : >>  var checkVideo = CommonAJSON("VideoList.asmx/IsItABadVideo", '{watchID:"' + watchID + '",platform:"Web"}'); 

    var ReturnData;
    $.ajax({
        type: "POST",
        async: false,
        url: "webservice1.asmx/GetCompetenceListByDivJobTypeId",
        data: "{'dataNeedsOf':'" + dataNeedsOf + "','divid':'" + divid + "','jobtypeid':'" + jobtypeid + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            $("#divResult").html(msg.d);
            data = msg.d;

            setTimeout(function () {

                DrawChart();
                drwaGraph();
                ForMainGraph();
                $("#bars").click();
            }, 500);

            //return msg.d;

        },
        error: function (e) {
            $("#divResult").html("WebSerivce unreachable");

        }
    });

    //  alert("aaa");
}




$('#chartType').change(function () {
    //var val = $(this).val();

    GetData("");

});


$('#ddlUserTarget').change(function () {
    //var val = $(this).val();

    var userid = $('#ContentPlaceHolder1_hdnGraphUserSelectedID').val();
 //   alert(userid);
    if (userid == '' || userid == null) {
        userid = '';
    }
    GetData(userid);

});
function GetData_Manager(dataNeedsOf) {
    // ie. : >>  var checkVideo = CommonAJSON("VideoList.asmx/IsItABadVideo", '{watchID:"' + watchID + '",platform:"Web"}'); 
   // alert('hi');
    var val = $('#chartType').val();
    var mul = $('#ddlCompetence').val();
    var TargetType = $('#ddlUserTarget').val();

    var TeamID = $('#ContentPlaceHolder1_ddlTeamFilter').val();
    if (TeamID == '' || TeamID == null || TeamID == 'undefined') {
        TeamID = 0;
    }
 //  alert(TeamID);
    var ReturnData;
    $.ajax({
        type: "POST",
        async: false,
        url: "webservice1.asmx/GetComListManager",
        data: "{'dataNeedsOf':'" + dataNeedsOf + "','val': '" + val + "','mul': '" + mul + "','TargetType': '" + TargetType + "','TeamID': '" + TeamID + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {

            $("#divResult").html(msg.d);
            data = msg.d;

            setTimeout(function () {


                drwaGraph();
                DrawChart();
                drGraph();
                ForMainGraph();

                if ($('#hdnChartType').val() == 'bars') {
                    $("#bars").click();
                } else if ($('#hdnChartType').val() == 'lines') {
                    $("#lines").click();
                } else if ($('#hdnChartType').val() == 'toggle') {
                    $("#toggle").click();
                }

            }, 500);

            //return msg.d;
        },
        error: function (e) {
            $("#divResult").html("WebSerivce unreachable");
        }
    });

    //  alert("aaa");
}

function GetData(dataNeedsOf) {
    // ie. : >>  var checkVideo = CommonAJSON("VideoList.asmx/IsItABadVideo", '{watchID:"' + watchID + '",platform:"Web"}'); 
   // alert('getdata');
    var val = $('#chartType').val();
    var mul = $('#ddlCompetence').val();
    var TargetType = $('#ddlUserTarget').val();

    var TeamID = $('#ContentPlaceHolder1_ddlTeamFilter').val();
    if (TeamID == '' || TeamID == null || TeamID == 'undefined') {
        TeamID = 0;
    }

    var ReturnData;
    $.ajax({
        type: "POST",
        async: false,
        url: "webservice1.asmx/GetComList",
        data: "{'dataNeedsOf':'" + dataNeedsOf + "','val': '" + val + "','mul': '" + mul + "','TargetType': '" + TargetType + "','TeamID': '" + TeamID + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {

            $("#divResult").html(msg.d);
            data = msg.d;

            setTimeout(function () {


                drwaGraph();
                DrawChart();
                drGraph();
                ForMainGraph();

                if ($('#hdnChartType').val() == 'bars') {
                    $("#bars").click();
                } else if ($('#hdnChartType').val() == 'lines') {
                    $("#lines").click();
                } else if ($('#hdnChartType').val() == 'toggle') {
                    $("#toggle").click();
                }

            }, 500);

            //return msg.d;
        },
        error: function (e) {
            $("#divResult").html("WebSerivce unreachable");
        }
    });

    //  alert("aaa");
}

var PDPdata;
function GetPDPData(dataNeedsOf,TeamID) {
    // ie. : >>  var checkVideo = CommonAJSON("VideoList.asmx/IsItABadVideo", '{watchID:"' + watchID + '",platform:"Web"}'); 
    var ReturnData;
    $.ajax({
        type: "POST",
        async: false,
        url: "webservice1.asmx/GetPDPpointByManagerId",
        data: "{'dataNeedsOf':'" + dataNeedsOf + "','TeamID':'" + TeamID + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            $("#divResult").html(msg.d);
            PDPdata = msg.d;

            setTimeout(function () {
                DrawChartPDP();
                drwaGraphPDP();
                ForPDPGraph();
                $("#bars").click();
            }, 500);

            //return msg.d;

        },
        error: function (e) {
            $("#divResult").html("WebSerivce unreachable");

        }
    });

    //  alert("aaa");
}
function ForPDPGraph() {
   // $('#graph-bars-pdp').hide();

    $('#PDPlines').on('click', function (e) {
        $('#PDPBars').removeClass('active');
        $('#graph-bars-pdp').fadeOut();
        $(this).addClass('active');
        $('#graph-lines-pdp').fadeIn();
        e.preventDefault();
    });

    $('#PDPBars').on('click', function (e) {
        $('#PDPlines').removeClass('active');
        $('#graph-lines-pdp').fadeOut();
        $(this).addClass('active');
        $('#graph-bars-pdp').fadeIn().removeClass('hidden');
        e.preventDefault();
    });



    var previousPoint = null;

    $('#graph-lines-pdp, #graph-bars-pdp').bind('plothover', function (event, pos, item) {
        if (item) {
            if (previousPoint != item.dataIndex) {
                previousPoint = item.dataIndex;
                $('#tooltip').remove();
                var x = item.datapoint[0],
                    y = item.datapoint[1];
                showTooltip(item.pageX, item.pageY, ' Point ' + y);
            }
        } else {
            $('#tooltip').remove();
            previousPoint = null;
        }
    });

}
function DrawChartPDP() {
    //var data1111a = dataa;


    var data1 = [["January", 10], ["February", 8], ["March", 4], ["April", 13], ["May", 17], ["June", 9]];
    var data11 = [["dfgr", 1], ["comp 10", 4], ["test", 5], []];
    var data1111 = [["dfgr", 2], ["comp 10", 3], ["test", 1], []];
    var x = [["dfgr", 2], ["comp 10", 3], ["test", 1], []];

    if (PDPdata != null) {

        var Achive = JSON.parse(PDPdata[0]);
       // alert(Achive);
        // var target = JSON.parse(data[2]);
        var graphData = [{
            // Returning Visits
            label: "PDP Points",
            data: Achive,
            color: '#282828',
            points: { radius: 4, fillColor: '#ffffff' }
        }
        ];



        setTimeout(function () {
            $.plot("#graph-lines-pdp", graphData, {
                series: {
                    points: {
                        show: true,
                        radius: 5
                    },
                    lines: {
                        show: true
                    },
                    shadowSize: 0
                },
                grid: {
                    color: '#282828',
                    borderColor: 'transparent',
                    borderWidth: 60,
                    hoverable: true
                },
                xaxis: {
                    mode: "categories",
                    tickLength: 0
                }
            });
        }, 1000);
    }

}
function drwaGraphPDP() {

    if (PDPdata != null) {
        var Achive = JSON.parse(PDPdata[0]);


        var graphData = [{
            // Returning Visits
            data: Achive,
            color: '#282828',
            points: { radius: 4, fillColor: '#282828' }
        }
        ];
        $.plot($('#graph-bars-pdp'), graphData, {
            series: {
                bars: {
                    show: true,
                    barWidth: .7,
                    align: 'center'
                },
                shadowSize: 0
            },
            grid: {

                borderColor: 'transparent',
                borderWidth: 40,
                hoverable: true
            },
            xaxis: {
                mode: "categories",
                tickLength: 0
            }
        });
    }

}

var alpPlpData;
function GetData_OLD(dataNeedsOf,TeamID) {
    // ie. : >>  var checkVideo = CommonAJSON("VideoList.asmx/IsItABadVideo", '{watchID:"' + watchID + '",platform:"Web"}'); 


    var ReturnData;
    $.ajax({
        type: "POST",
        async: false,
        url: "webservice1.asmx/GetTotalAlpPlpGroupByDateByUserId",
        data: "{'dataNeedsOf':'" + dataNeedsOf + "','TeamID':'" + TeamID + "'}",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            $("#divResult").html(msg.d);

           // alert('data:'+msg.d);
            alpPlpData = msg.d;

            setTimeout(function () {
                DrawChartAlpPlp();
                drwaGraphAlpPlp();
                ForAlpPlpGraph();
                $("#bars").click();
            }, 500);

            //return msg.d;

        },
        error: function (e) {
            $("#divResult").html("WebSerivce unreachable");
        }
    });

    //  alert("aaa");
}

function DrawChartAlpPlp() {
    //var data1111a = dataa;


    var data1 = [["January", 10], ["February", 8], ["March", 4], ["April", 13], ["May", 17], ["June", 9]];
    var data11 = [["dfgr", 1], ["comp 10", 4], ["test", 5], []];
    var data1111 = [["dfgr", 2], ["comp 10", 3], ["test", 1], []];
    var x = [["dfgr", 2], ["comp 10", 3], ["test", 1], []];

    //alert(alpPlpData[1]);
    //alert(alpPlpData[0]);
    var Achive = JSON.parse(alpPlpData[1]);
    var Local = JSON.parse(alpPlpData[0]);
    // var target = JSON.parse(data[2]);
    var graphData = [{
        // Visits
        label: "PLP Points",
        data: Achive,
        color: '#a6d87a'
    }, {
        // Returning Visits
        label: "ALP Points",
        data: Local,
        color: '#282828',
        points: { radius: 4, fillColor: '#ffffff' }
    }
    ];



    setTimeout(function () {
        $.plot("#graph-lines-alpplp", graphData, {
            series: {
                points: {
                    show: true,
                    radius: 5
                },
                lines: {
                    show: true
                },
                shadowSize: 0
            },
            grid: {
                color: '#282828',
                borderColor: 'transparent',
                borderWidth: 60,
                hoverable: true
            },
            xaxis: {
                mode: "categories",
                tickLength: 0
            }
        });
    }, 1000);

}
function drwaGraphAlpPlp() {
    var Achive = JSON.parse(alpPlpData[1]);
    var Local = JSON.parse(alpPlpData[0]);

    var graphData = [{
        // Visits
        data: Local,
        color: '#a6d87a'
    }, {
        // Returning Visits
        data: Achive,
        color: '#282828',
        points: { radius: 4, fillColor: '#282828' }
    }
    ];
    $.plot($('#graph-bars-alpplp'), graphData, {
        series: {
            bars: {
                show: true,
                barWidth: .7,
                align: 'center'
            },
            shadowSize: 0
        },
        grid: {

            borderColor: 'transparent',
            borderWidth: 40,
            hoverable: true
        },
        xaxis: {
            mode: "categories",
            tickLength: 0
        }
    });

}




//https://github.com/flot/flot/blob/master/API.md
//https://github.com/flot/flot/blob/master/API.md


function DrawChart() {
    //var data1111a = dataa;
    //alert('hi');

    var data1 = [["January", 10], ["February", 8], ["March", 4], ["April", 13], ["May", 17], ["June", 9]];
    var data11 = [["dfgr", 1], ["comp 10", 4], ["test", 5], []];
    var data1111 = [["dfgr", 2], ["comp 10", 3], ["test", 1], []];
    var x = [["dfgr", 2], ["comp 10", 3], ["test", 1], []];
    var isErr = false;
    try {
        var x = JSON.parse(data[1]);
    } catch (w) {
        //$("#dialog").dialog(); //show dilouge box.. it require jquery 1.11
        modelPopupDilouge("Graph Data", "No data availabel for graph.", false);
        //alert('Data not available'); //temp // 
        $("canvas").hide();
        isErr = true;

    }
    if (isErr) { $("canvas").hide(); } else {
        $("canvas").show();
    }


    var Achive = JSON.parse(data[1]);
    var Local = JSON.parse(data[0]);
    var target = JSON.parse(data[2]);

    var MaxLevel = JSON.parse(data[4]);
    var sc = [];

   

    for (var i = 0; i < target.length; i++) {
        sc.push(target[i][0]);
        //dataPoints.push({x:result[i].X, y:result[i].Y});
        //alert(sc);
    }

    var sv = $('#ContentPlaceHolder1_spiderchartname').val();
    var sp = $("#lblUserName").text();


    Highcharts.chart('container', {
        colors: ['#f45b5b', '#8085e9', '#8d4654', '#7798BF', '#aaeeee',
     '#ff0066', '#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
        chart: {
            polar: true,
            type: 'line'
        },
        credits: {
            enabled: false
        },

        title: {
            text: sv + ' ' + sp,
            x: -80
        },

        pane: {
            size: '100%'
        },
        scrollbar: {
            enabled: true

        },
        xAxis: {
            categories: sc,
            tickmarkPlacement: 'on',
            lineWidth: 0,

        },

        yAxis: {
            gridLineInterpolation: 'polygon',
            lineWidth: 0,
            min: 0,
        },

        tooltip: {
            shared: true,
            pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.0f}</b><br/>'
        },

        legend: {
            align: 'right',
            verticalAlign: 'top',
            y: 70,
            layout: 'vertical'
        },
        series: [{
            name: $('#ContentPlaceHolder1_hdnChartActual').val(),
            data: Achive,
            pointPlacement: 'on'
        }, {
            name: $('#ContentPlaceHolder1_hdnChartTarget').val(),
            data: target,
            pointPlacement: 'on'
        }
        , {
            name: $('#ContentPlaceHolder1_hdnChartMax').val(),
            data: MaxLevel,
            pointPlacement: 'on'
        }
        ]


    });

}
//Bar chart - Ubhi line wado chrat

function GetMaxValue() {
    var str = data[3];

    data[3] = data[3].replace('[', '');
    data[3] = data[3].replace(']', '');
    var mvt = (data[4].split(","));

    var hc = [];
    var max1 = 0;
    for (var j = 0; j < mvt.length; j++) {

        if (mvt[j] > max1) {
            max1 = mvt[j];

            //hc.push(max);
        }


    }

    return max1;
}


function drGraph() {

    var Achive = JSON.parse(data[1]);
    var Local = JSON.parse(data[0]);
    var target = JSON.parse(data[2]);

    var MaxLevel = JSON.parse(data[4]);
    var sc = [];

    for (var i = 0; i < target.length; i++) {
        sc.push(target[i][0]);
        //dataPoints.push({x:result[i].X, y:result[i].Y});
        //alert(sc);
    }


    var str = JSON.parse(data[4]);
    //data[2] = data[2].replace('[', '');
    //data[2] = data[2].replace(']', '');
    //var mvt = (data[2].split(","));
   
    var max1 = 0;
    for (var j = 0; j < str.length; j++) {
        
        if (str[j] > max1) {
            max1 = str[j];
            
        }

    }
    
    max1 = Math.ceil(max1);
    max1 = max1;
   // alert(max1);
    var totalcomp = 1;
    if (target.length > 0) {
        totalcomp = target.length / 2;
    }
    if (totalcomp > 5) {
        totalcomp = target.length / 5;
    }
    if (totalcomp > 8) {
        totalcomp = target.length / 8;
    }
    if (totalcomp > 10) {
        totalcomp = target.length / 10;
    }
    if (totalcomp > 15) {
        totalcomp = target.length / 15;
    }
    if (totalcomp > 20) {
        totalcomp = target.length / 20;
    }
    var sv = $('#ContentPlaceHolder1_columnchartname').val();
    var sp = $("#lblUserName").text();
    //if (sv == "Danish") {
    //    var thetitle = 'Alle medarbejdere'
    //}
    //else {
    //    var thetitle = 'All Employee'
    //}

    Highcharts.chart('graph-barEmp', {
        colors: ['#f45b5b', '#8085e9', '#8d4654', '#7798BF', '#aaeeee',
     '#ff0066', '#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
        chart: {
            type: 'column'
        },
        title: {
            text: sv + ' ' + sp
        },
        credits: {
            enabled: false
        },
        xAxis: {
            categories: sc,
            crosshair: true,
            min: 1,
            //  max: (totalcomp),
            max: totalcomp,
        },
        yAxis: {
            min: 0,
            max: max1,
            // tickInterval:1,
            title: {
                text: $('#ContentPlaceHolder1_hdnChartLevel').val()
            }
        },
        scrollbar: {
            enabled: true

        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} </b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0,
                dataLabels: {
                    enabled: true
                }
            }
        },
        series: [{

            name: $('#ContentPlaceHolder1_hdnChartActual').val(),
            data: Achive

        }, {

            name: $('#ContentPlaceHolder1_hdnChartTarget').val(),
            data: target

        }, {

            name: $('#ContentPlaceHolder1_hdnChartMax').val(),
            data: MaxLevel

        }]
    });


}



function drwaGraph() {

   // alert(JSON.parse(data[4]));

    var Achive = JSON.parse(data[1]);
    var Local = JSON.parse(data[0]);
    var target = JSON.parse(data[2]);

    var MaxLevel = JSON.parse(data[4]);
    var sc = [];

    for (var i = 0; i < target.length; i++) {
        sc.push(target[i][0]);
        //dataPoints.push({x:result[i].X, y:result[i].Y});
        //alert(sc);
    }



    var dataPoints = [];
    for (var i = 0; i < target.length; i++) {
        dataPoints.push({ label: target[i][0], y: target[i][1] });

        //dataPoints.push({x:result[i].X, y:result[i].Y});
    }

    var TblPoints = [];
    for (var i = 0; i < Achive.length; i++) {
        TblPoints.push({ label: Achive[i][0], y: Achive[i][1] });

        //dataPoints.push({x:result[i].X, y:result[i].Y});
    }
    var sv = $('#ContentPlaceHolder1_barchartname').val();
    
    var sp = $("#lblUserName").text();
    debugger;
   // alert(sp);
    //debugger;
    if (sp == '' || sp == null || sp == 'undefined') {
        sp = $('#ContentPlaceHolder1_lblAllManager1').val();
    }
    var totalcomp = 1;
    if (target.length > 0) {
        totalcomp = target.length / 2;
    }
    if (totalcomp > 5) {
        totalcomp = target.length / 5;
    }
    if (totalcomp > 8) {
        totalcomp = target.length / 8;
    }
    if (totalcomp > 10) {
        totalcomp = target.length / 10;
    }
    if (totalcomp > 15) {
        totalcomp = target.length / 15;
    }
    if (totalcomp > 20) {
        totalcomp = target.length / 20;
    }
    //alert(target.length);
    Highcharts.chart('graph-barsEmployee', {
        chart: {
            type: 'bar'
        },
        credits: {
            enabled: false
        },
        title: {
            text: sv + ' ' + sp
        },
        colors: ['#f45b5b', '#8085e9', '#8d4654', '#7798BF', '#aaeeee',
      '#ff0066', '#eeaaee', '#55BF3B', '#DF5353', '#7798BF', '#aaeeee'],
        xAxis: {
            allowDecimals: false,
            tickmarkPlacement: 'on',
            gridLineWidth: 1,
            categories: sc,
            min: 1,
            // max: (totalcomp),
            max: totalcomp,
            title: {
                text: null
            }
        },
        scrollbar: {
            enabled: true

        },
        yAxis: {
            gridLineWidth: 0,
            tickInterval: 1,
            min: 0,
            title: {
                text: $('#ContentPlaceHolder1_hdnChartLevel').val(),
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        //tooltip: {
        //    valueSuffix: ' millions'
        //},
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'right',
            verticalAlign: 'top',
            x: -40,
            y: 80,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: $('#ContentPlaceHolder1_hdnChartActual').val(),
            data: Achive
        }, {
            name: $('#ContentPlaceHolder1_hdnChartTarget').val(),
            data: target,
            pointPlacement: 'on',
        }, {
            name: $('#ContentPlaceHolder1_hdnChartMax').val(),
            data: MaxLevel,
            pointPlacement: 'on',
        }
        ]


    });
}




var dataDev;
function drwaGraphDevelopment() {
    console.log(dataDev);
   // alert(compNames);
   
    $(function () {
        $('#containerOriginal').highcharts({
            chart: {
                type: 'column',
                zoomType: 'x'
            },
            title: {
                text: 'Graph'
            },
            subtitle: {
                text: 'Click and drag mouse arrow to zoom chart'
            },

            xAxis: {

                categories: compNames
            },
            yAxis: {
                max: 6,  //to show y axis 5 level in graph weather data available or not. Saurin - 2015-11-21
                min: 0,
                title: {
                    text: $('#ContentPlaceHolder1_hdnChartLevel').val()
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    //'<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                    '<td style="padding:0"><b>{point.y:.1f} <span class="spanText">' + $('#ContentPlaceHolder1_hdnChartLevel').val() + '</span></b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: dataDev
        });
    });
}

function SubmitBtn() {
    GetPageName();
    var ReturnData;
    $.ajax({
        type: "POST",
        cache: false,
        async: true,
        url: pageName + ".aspx/BtnSubmitAJAX",
        data: '{txtName:"' + '' + '",txtDesc:"' + '' + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            dataDev = response.d;

            Developmentcomnames();
            setTimeout(function () {

                drwaGraphDevelopment();
                $("#wait").fadeOut();
                DataNewComp();
            }, 500);


        },
        failure: function (msg) {

            alert(msg);
            ReturnData = msg;
        }
    });

}
function DataNewComp() {

    var ReturnData;
    $.ajax({
        type: "POST",
        cache: false,
        async: true,
        url: pageName + ".aspx/DataNewComp",
        data: '{txtName:"' + '' + '",txtDesc:"' + '' + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            dataNewComp = response.d;

            // Developmentcomnames();
            setTimeout(function () {

                drwaChartNewCompLevel();
                DataDevelopmentComp();
                DataKnowledgeDevelopment();
                $("#waitNew").fadeOut();
            }, 500);


        },
        failure: function (msg) {

            alert(msg);
            ReturnData = msg;
        }
    });

}
function DataDevelopmentComp() {

    var ReturnData;
    $.ajax({
        type: "POST",
        cache: false,
        async: true,
        url: pageName + ".aspx/DataDevelopmentComp",
        data: '{txtName:"' + '' + '",txtDesc:"' + '' + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            dataDevelopmentComp = response.d;

            // Developmentcomnames();
            setTimeout(function () {

                drwaChartDevelopmentCompLevel();
                $("#waitDevelopment").fadeOut();
            }, 500);


        },
        failure: function (msg) {

            alert(msg);
            ReturnData = msg;
        }
    });

}
var compNames;
var dataNewComp;
var dataDevelopmentComp;
function Developmentcomnames() {

   
    var ReturnData;
    $.ajax({
        type: "POST",
        cache: false,
        async: true,
        url: pageName + ".aspx/Developmentcomnames",
        data: '{txtName:"' + '' + '",txtDesc:"' + '' + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            compNames = response.d;
           
            return compNames;

        },
        failure: function (msg) {

            alert(msg);
            ReturnData = msg;
        }
    });

}


function drwaChartNewCompLevel() {

    $(function () {
        $('#ContainerNew').highcharts({
            chart: {
                type: 'column', zoomType: 'x'
            },
            title: {
                text: 'Graph'
            },
            subtitle: {
                text: 'Click and drag mouse arrow to zoom chart'
            },
            xAxis: {
                categories: compNames
            },
            yAxis: {
                max: 6,  //to show y axis 5 level in graph weather data available or not. Saurin - 2015-11-21
                min: 0,
                title: {
                    text: $('#ContentPlaceHolder1_hdnChartLevel').val()
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    '<td style="padding:0"><b>{point.y:.1f} <span class="spanText">' + $('#ContentPlaceHolder1_hdnChartLevel').val() + '</span></b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            series: dataNewComp
        });
    });


}
function drwaChartDevelopmentCompLevel() {


    $(function () {


        $('#ContainerDevelopment').highcharts({
            chart: {
                type: 'column', zoomType: 'x'
            },
            title: {
                text: 'Graph'
            },
            subtitle: {
                text: 'Click and drag mouse arrow to zoom chart'
            },
            xAxis: {
                categories: compNames,
            },
            yAxis: {
                min: 0,
                title: {
                    text: $('#ContentPlaceHolder1_hdnChartLevel').val()
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    //'<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                '<td style="padding:0"><b>{point.y:.1f} <span class="spanText">' + $('#ContentPlaceHolder1_hdnChartLevel').val() + '</span></b></td></tr>',

                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            zoom: {
                interactive: true
            },
            pan: {
                interactive: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            selection: {
                mode: "x"
            }, mapNavigation: {
                enabled: true,
                enableDoubleClickZoomTo: true
            },

            series: dataDevelopmentComp
        });
    });


}

var pageName = "report"; //default
function GetPageName() {
    var url = window.location.pathname;
    var index = url.lastIndexOf("/") + 1;
    var filenameWithExtension = url.substr(index);
    var filename = filenameWithExtension.split(".")[0]; // <-- added this line

    pageName = filename;
    // <-- added this line

}
/*----------------KnowledgeDevelopment--------------------*/
var dataKnowledgeDevelopment;
function DataKnowledgeDevelopment() {

    var ReturnData;
    $.ajax({
        type: "POST",
        cache: false,
        async: true,
        url: pageName + ".aspx/DataKnowledgeDevelopment",
        data: '{txtName:"' + '' + '",txtDesc:"' + '' + '"}',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {

            dataKnowledgeDevelopment = response.d;

            // Developmentcomnames();
            setTimeout(function () {

                drwaKnowledgeDevelopment();
                $("#waitKnowledgeDevelopment").fadeOut();
            }, 500);


        },
        failure: function (msg) {

            alert(msg);
            ReturnData = msg;
        }
    });

}
function drwaKnowledgeDevelopment() {


    $(function () {
        $('#ContainerKnowledgeDevelopment').highcharts({
            chart: {
                type: 'column', zoomType: 'x'
            },
            title: {
                text: 'Graph'
            },
            subtitle: {
                text: 'Click and drag mouse arrow to zoom chart'
            },
            xAxis: {
                categories: compNames,
            },
            yAxis: {
                min: 0,
                title: {
                    text: $('#ContentPlaceHolder1_hdnChartLevel').val()
                }
            },
            tooltip: {
                headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                    //'<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                            '<td style="padding:0"><b>{point.y:.1f} <span class="spanText" >' + $('#ContentPlaceHolder1_hdnChartLevel').val() + '</span></b></td></tr>',
                footerFormat: '</table>',
                shared: true,
                useHTML: true
            },
            zoom: {
                interactive: true
            },
            pan: {
                interactive: true
            },
            plotOptions: {
                column: {
                    pointPadding: 0.2,
                    borderWidth: 0
                }
            },
            selection: {
                mode: "x"
            }, mapNavigation: {
                enabled: true,
                enableDoubleClickZoomTo: true
            },

            series: dataKnowledgeDevelopment
        });
    });


}
/**/
$(document).ready(function () {
   
    GetPageName();
    //  alert('document ready');
    // GetData("");
  //  alert($("#hdnLoginUserIDType").val());
    if ($("#hdnLoginUserIDType").val() == "1" || $("#hdnLoginUserIDType").val() == "2") {
        GetGraphByManagerId('', '');
    }
    else {
        GetData();
    }
    ForMainGraph();
    setTimeout(function () {
        $("#bars").click();
        //GetData_OLD("");
    }, 1500);

   // GetPDPData('');


   

});

function showTooltip(x, y, contents) {
    $('<div id="tooltip">' + contents + '</div>').css({
        top: y - 16,
        left: x + 20
    }).appendTo('body').fadeIn();
}
function ForAlpPlpGraph() {
    $('#graph-bars-alpplp').hide();

    $('#Alplines').on('click', function (e) {
        $('#AlpBars').removeClass('active');
        $('#graph-bars-alpplp').fadeOut();
        $(this).addClass('active');
        $('#graph-lines-alpplp').fadeIn();
        e.preventDefault();
    });

    $('#AlpBars').on('click', function (e) {
        $('#Alplines').removeClass('active');
        $('#graph-lines-alpplp').fadeOut();
        $(this).addClass('active');
        $('#graph-bars-alpplp').fadeIn().removeClass('hidden');
        e.preventDefault();
    });

    var previousPoint = null;

    $('#graph-lines-alpplp, #graph-bars-alpplp').bind('plothover', function (event, pos, item) {
        if (item) {
            if (previousPoint != item.dataIndex) {
                previousPoint = item.dataIndex;
                $('#tooltip').remove();
                var x = item.datapoint[0],
                    y = item.datapoint[1];
                showTooltip(item.pageX, item.pageY, ' Point ' + y);
            }
        } else {
            $('#tooltip').remove();
            previousPoint = null;
        }
    });

}

function ForMainGraph() {

    $('#graph-barsEmployee').hide();

    $('#container').hide();

    $('#lines').on('click', function (e) {
        $('#graph-barsEmployee').hide();
        $('#graph-barEmp').hide();
        $('#chartType').hide();
        $('#bars').removeClass('active');
        $('#graph-barsEmployee').fadeOut();
        $('#graph-barEmp').fadeOut();
        $(this).addClass('active');
        $('#container').fadeIn();
        e.preventDefault();
    });

    $('#bars').on('click', function (e) {
        $('#lines').removeClass('active');
        $('#chartType').show();
        $('#container').fadeOut();
        $('#graph-barEmp').fadeOut();
        $(this).addClass('active');
        $('#graph-barsEmployee').fadeIn().removeClass('hidden');

        e.preventDefault();
    });

    $('#toggle').on('click', function (e) {
        $('#lines').removeClass('active');
        $('#chartType').hide();
        $('#bars').removeClass('active');
        $('#container').fadeOut();
        $('#graph-barsEmployee').fadeOut();
        $('#chartType').show();
        $(this).addClass('active');
        $('#graph-barEmp').fadeIn().removeClass('hidden');
        e.preventDefault();
    });
    var previousPoint = null;

    $('#container, #graph-barsEmployee,#graph-barEmp').bind('plothover', function (event, pos, item) {
        if (item) {
            if (previousPoint != item.dataIndex) {
                previousPoint = item.dataIndex;
                $('#tooltip').remove();
                var x = item.datapoint[0],
                    y = item.datapoint[1];
                showTooltip(item.pageX, item.pageY, ' ' + $('#ContentPlaceHolder1_hdnChartLevel').val() + ' ' + y);
            }
        }
        else {
            $('#tooltip').remove();
            previousPoint = null;
        }
    });

}
