﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using System.Configuration;
using startetku.Business.Logic;
using System.Threading;
using System.Globalization;


public partial class Organisation_request : System.Web.UI.Page
{
    string temp = "0";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Eemployee.InnerHtml = CommonMessages.Welcome + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                temp = Convert.ToString(Request.QueryString["id"]);
                Label2.Visible = false;
                back.Visible = false;
            }
            else if (!String.IsNullOrEmpty(Request.QueryString["view"]))
            {
                temp = Convert.ToString(Request.QueryString["view"]);
                ddlstastus.Visible = false;
                submit.Visible = false;
            }
            GetAllEmployeebyid();
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }

    #region method
    protected void GetAllEmployeebyid()
    {
        EmployeeSkillBM obj = new EmployeeSkillBM();
        obj.skillId = Convert.ToInt32(temp);
        obj.GetAllSkillPendingRequestbyid();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["name"])))
            {
                lblName.Text = Convert.ToString(ds.Tables[0].Rows[0]["name"]);
            }
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comCompetence"])))
            {
                lblCompetence.Text = Convert.ToString(ds.Tables[0].Rows[0]["comCompetence"]);
            }
            //if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["subCompetence"])))
            //{
            //    lblsubCompetence.Text = Convert.ToString(ds.Tables[0].Rows[0]["subCompetence"]);
            //}
            //if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["skillDoc"])))
            //{
            //    if (ds.Tables[0].Rows.Count > 0)
            //    {
            //        rep_document.DataSource = ds;
            //        rep_document.DataBind();
            //    }
            //}
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["skillStatus"])))
            {
                Int32 status = Convert.ToInt32(ds.Tables[0].Rows[0]["skillStatus"]);
                if (status == 3)
                {
                    ddlstastus.SelectedValue = "3";
                    Label2.Text = "Pending";
                }
                else if (status == 2)
                {
                    ddlstastus.SelectedValue = "2";
                    Label2.Text = "Cancel";
                }
                else if (status == 1)
                {
                    ddlstastus.SelectedValue = "1";
                    Label2.Text = "Accept";
                }
            }

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[1].Rows[0]["skillLevel"])))
            {
                if (ds.Tables[1].Rows.Count > 0)
                {
                    Repeater1.DataSource = ds.Tables[1];
                    Repeater1.DataBind();
                }
            }
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[1].Rows[0]["skillPoint"])))
            {
                txtpoint.Text = Convert.ToString(ds.Tables[1].Rows[0]["skillPoint"]);
            }
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["skillUserId"])))
            {
                huserid.Value = Convert.ToString(ds.Tables[0].Rows[0]["skillUserId"]);
            }
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["skillComId"])))
            {
                hskillSubId.Value = Convert.ToString(ds.Tables[0].Rows[0]["skillComId"]);
            }
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["skillCompanyId"])))
            {
                hcmnyname.Value = Convert.ToString(ds.Tables[0].Rows[0]["skillCompanyId"]);
            }
        }

    }
    protected void UpdateUser()
    {
        string QueryString = "0";
        QueryString = Convert.ToString(Request.QueryString["id"]);
        EmployeeSkillBM obj = new EmployeeSkillBM();
        obj.skillId = Convert.ToInt32(QueryString);
        if (ddlstastus.SelectedValue == "1")
        {
            obj.skillStatus = 1;
            //obj.skillPoint = Convert.ToInt32(txtpoint.Text);
        }
        else if (ddlstastus.SelectedValue == "2")
        {
            obj.skillStatus = 2;
        }
        else if (ddlstastus.SelectedValue == "3")
        {
            obj.skillStatus = 3;
        }
        obj.UpdateSkillpendingRequest();
        if (obj.ReturnBoolean == true)
        {
            if (ddlstastus.SelectedValue == "1")
            {
                Updatepoint();
            }
            insertskillchild();
            Response.Redirect("pending_request.aspx?msg=upd");
        }

    }
    protected void insertskillchild()
    {
        string QueryString = "0";
        QueryString = Convert.ToString(Request.QueryString["id"]);
        foreach (RepeaterItem item in Repeater1.Items)
        {
            TextBox box = (TextBox)item.FindControl("TextBox1");
            HiddenField skillchildId = item.FindControl("skillchildId") as HiddenField;
            EmployeeSkillBM obj = new EmployeeSkillBM();
            obj.skillId = Convert.ToInt32(QueryString);
            obj.skillPoint = Convert.ToInt32(txtpoint.Text);
            obj.skillLevel = Convert.ToInt32(box.Text);
            obj.skillchildId = Convert.ToInt32(skillchildId.Value);
            obj.UpdateSkillchildpendingRequest();
        }
    }
    protected void Updatepoint()
    {
        PointBM obj = new PointBM();
        obj.PointUserId = Convert.ToInt32(huserid.Value);
        obj.PointCompanyId = Convert.ToInt32(hcmnyname.Value);
        obj.pintSubCompetance = Convert.ToInt32(hskillSubId.Value);
        if (!String.IsNullOrEmpty(txtpoint.Text))
        {
            obj.PointAnsCount = Convert.ToInt32(txtpoint.Text);
        }
        else
        {
            obj.PointAnsCount = 0;
        }
      
        obj.UpdatePointmaster();
        




    }
    #endregion

    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Request.QueryString["id"]))
        {
            UpdateUser();
        }

    }
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Denish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
}