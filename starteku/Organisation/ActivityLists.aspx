﻿<%@ Page Title="AskingCulture" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master" AutoEventWireup="true" meta:resourcekey="PageResource"
    CodeFile="ActivityLists.aspx.cs" Inherits="Organisation_ActivityLists" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        #ContentPlaceHolder1_txtActTags_tag {
            color: #d4d4d4;
        }
        .ActName {
            width:30% !important;
        }
        .yellow {
            border-radius: 0px;
        }

        .table > thead > tr > th {
            vertical-align: middle;
        }

        .radiobuttonlist {
            font: 12px Verdana, sans-serif;
            color: #000; /* non selected color */
        }

        .inline-rb input[type="radio"] {
            width: auto;
            margin-right: 10px;
            margin-left: 20px;
        }

        .inline-rb label {
            display: inline;
        }

        .ddpDivision {
            max-height: 150px;
            overflow: auto;
            border: 1px solid #d4d4d4;
            padding: 5px;
        }

        #rdoPrivatePublic label {
            display: inline;
        }

        #ContentPlaceHolder1_rdoPrivatePublic_0 {
            width: 0px;
            margin-right: 9px;
        }

        #ContentPlaceHolder1_rdoPrivatePublic_1 {
            width: 0px;
            margin-right: 9px;
        }

        .rdoBtn {
            width: 0px;
            margin-right: 9px;
        }

        .reply {
            clear: both;
        }

        #scrollbox6 {
            overflow-x: hidden;
            overflow-y: auto;
            background: #f4f4f4;
        }

        #txtCmt {
            overflow-x: hidden;
            overflow-y: auto;
            background: #f4f4f4;
            height: 315px;
        }

        .multiselect-container {
            height: 200px;
            overflow-x: hidden;
            overflow-y: scroll;
        }
    </style>

   <link href="../Scripts/pickadate.js-3.5.6/lib/themes/classic.css" rel="stylesheet" />
    <link href="../Scripts/fullcalendor/pickADate/classic.date.css" rel="stylesheet" />


    <%--joyride---------------------------------------------%>
    <link href="../plugins/joyride/joyride-2.1.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/multiselect.css" rel="stylesheet" />
    <link href="../Styles/UpdateCustom.css" rel="stylesheet" type="text/css" />
    <script src="../plugins/joyride/jquery.cookie.js" type="text/javascript"></script>
    <script src="../plugins/joyride/jquery.joyride-2.1.js" type="text/javascript"></script>
    <script src="../plugins/joyride/modernizr.mq.js" type="text/javascript"></script>

    <script src="../plugins/highchart/highcharts.js" type="text/javascript"></script>


    <script src="../plugins/highchart/highcharts-more.js" type="text/javascript"></script>
    <script src="../Scripts/plugins/exporting.js" type="text/javascript"></script>
    <script src="../Scripts/jquery321.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <%--<asp:UpdatePanel runat="server" ID="updtPnl">
        <ContentTemplate>--%>
    <div class="col-md-6">
        <div class="heading-sec">
            <h1 style="margin-left: 15px;">

                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="ActivityLists" EnableViewState="false" />
                <%-- <%= CommonMessages.Competence%>--%>
                <i>
                    <span>
                        <asp:Literal ID="Literal5" runat="server" meta:resourcekey="Welcome" EnableViewState="false" /></span><span runat="server" id="Competence"></span>
                </i>
            </h1>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="hdnLoginUserID" Value="0" />
    <asp:HiddenField runat="server" ID="hdnUserType" Value="0" />

    <div class="col-md-12" style="margin-top: 20px;">
        <div aria-hidden="true" role="dialog" tabindex="-1" class="modal fade col-md-12" id="popupAddNewDoc" style="display: none;">
            <div class="modal-dialog" style="width: 55%">
                <div class="modal-content">
                    <div class="modal-header blue yellow-radius">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                            ×
                        </button>
                        <h4 class="modal-title">
                            <%-- <%= CommonMessages.AddNewDocument%>--%>
                            <%--Add New Document<asp:Literal ID="Literal8" runat="server" meta:resourcekey="AddNewDocument"
                                    EnableViewState="false" />--%>

                            <asp:Label runat="server" ID="Label4" CssClass="" Style="color: white; font-size: 19px; margin-top: 4px;">
                                <asp:Literal ID="lblModelHeader" runat="server"
                                    EnableViewState="true" />
                            </asp:Label></h4>
                    </div>
                    <div class="modal-body">
                        <%--<div class="form-group" visible="true" id="errorDiv">
                            <div class="col-md-3" style="padding-top: 10px;" >
                                <asp:Label runat="server" ID="Label16" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">Error : </asp:Label>
                            </div>
                            <div class="col-md-9">

                                <asp:Label runat="server" ID="txtError" MaxLength="50" />
                                $1$meta:resourcekey="txttitleResource1"#1#
                                
                            </div>
                        </div>--%>
                        <div class="form-group">
                            <div id="errorDiv" runat="server" style="clear: both" visible="false">
                                <div class="col-md-3" style="padding-top: 10px;">
                                    <asp:Label runat="server" ID="Label17" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                        <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Error" EnableViewState="false" />
                                    </asp:Label>
                                </div>
                                <div class="col-md-9">
                                    <asp:Label runat="server" ID="txtError" ForeColor="red" />
                                    <%--meta:resourcekey="txttitleResource1"--%>
                                </div>
                            </div>
                        </div>
                        <br />

                        <div class="form-group">

                            <div class="col-md-3" style="padding-top: 10px;">
                                <asp:HiddenField runat="server" ID="hdnUmId" />
                                <asp:HiddenField runat="server" ID="hdnActId" />
                               
                                  <asp:HiddenField ID="hdnActReqIdInt" runat="server" Value="0" />
                                <asp:HiddenField ID="hdnActReqUserId" runat="server" Value="0" />
                                <asp:HiddenField ID="hdnActReqStatusActivity" runat="server" Value="0" />
                                 <asp:HiddenField ID="hdnPublicPrivateAct" runat="server" Value="0" />

                                <asp:HiddenField ID="hdnAcrCreaterId" runat="server" />
                                <asp:Label runat="server" ID="Label5" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                    <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Activity_Type" EnableViewState="false" />
                                </asp:Label>
                            </div>
                            <div class="col-md-9">
                                <div class="ddpDivision ">

                                    <asp:DropDownList Width="250px"
                                        ID="ddActCate" runat="server" CssClass="chkliststyle form-control" AppendDataBoundItems="False"
                                        RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description" OnSelectedIndexChanged="ddActCate_OnSelectedIndexChanged"
                                        DataValueField="value" AutoPostBack="True"
                                        Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; width: 100%; color: white; overflow: auto;">
                                    </asp:DropDownList>


                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddActCate"
                                        ErrorMessage="Please select activity category." InitialValue="0" CssClass="commonerrormsg"
                                        Display="Dynamic" ValidationGroup="chkdoc"></asp:RequiredFieldValidator>
                                </div>
                                <%--meta:resourcekey="txttitleResource1"--%>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3" style="padding-top: 10px;">
                                <asp:Label runat="server" ID="Label1" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                    <asp:Literal ID="Literal4" runat="server" meta:resourcekey="Activity_Name" EnableViewState="false" />
                                </asp:Label>
                            </div>
                            <div class="col-md-9" style="margin-top: 10px;">
                                <asp:TextBox runat="server" ID="txtActName" placeholder="Enter Name Of Activity" MaxLength="50" />
                                <%--meta:resourcekey="txttitleResource1"--%>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtActName"
                                    ErrorMessage="Please enter activity name." CssClass="commonerrormsg" Display="Dynamic"
                                    ValidationGroup="chkdoc"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-3" style="padding-top: 10px;">
                                <asp:Label runat="server" ID="Label2" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                    <asp:Literal ID="Literal6" runat="server" meta:resourcekey="Description" EnableViewState="false" />
                                </asp:Label>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox runat="server" ID="txtActDescription" placeholder="Enter Description for the Activity" TextMode="MultiLine" MaxLength="50" />
                                <%--meta:resourcekey="txttitleResource1"--%>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-3" style="padding-top: 10px;">
                                <asp:Label runat="server" ID="Label6" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                    <asp:Literal ID="Literal7" runat="server" meta:resourcekey="Requirement" EnableViewState="false" />
                                </asp:Label>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox runat="server" ID="txtActRequirements" placeholder="Enter Activity Requirements" MaxLength="50" />
                                <%--meta:resourcekey="txttitleResource1"--%>
                            </div>
                        </div>

                        <div class="form-group" style="padding-bottom: 105px; clear: both">
                            <div class="col-md-3" style="padding-top: 10px;">
                                <asp:Label runat="server" ID="Label7" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                    <asp:Literal ID="Literal8" runat="server" meta:resourcekey="Tags" EnableViewState="false" />
                                </asp:Label>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox runat="server" ID="txtActTags" CssClass="tagsinput" placeholder="Enter Activity tags" MaxLength="50" />
                                <asp:HiddenField runat="server" meta:resourcekey="AddATag" ID="hdnAddATag" />
                                <%--meta:resourcekey="txttitleResource1"--%>
                            </div>
                        </div>

                        <div class="form-group" style="clear: both">
                            <div class="col-md-3" style="padding-top: 10px;">
                                <asp:Label runat="server" ID="Label8" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                    <asp:Literal ID="Literal9" runat="server" meta:resourcekey="Start_Date" EnableViewState="false" />
                                </asp:Label>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox runat="server" ID="txtActStartDate" placeholder="Select Start" CssClass="txtActStartDate" MaxLength="50" onmouseover="SetDatePicker();" />
                                <%--meta:resourcekey="txttitleResource1"--%>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-3" style="padding-top: 10px;">
                                <asp:Label runat="server" ID="Label9" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                    <asp:Literal ID="Literal10" runat="server" meta:resourcekey="End_Date" EnableViewState="false" />
                                </asp:Label>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox runat="server" ID="txtActEndDate" placeholder="Select End" CssClass="txtActEndDate" MaxLength="50" onmouseover="SetDatePicker();" />
                                <%--meta:resourcekey="txttitleResource1"--%>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-3" style="padding-top: 10px;">
                                <asp:Label runat="server" ID="Label10" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                    <asp:Literal ID="Literal11" runat="server" meta:resourcekey="Cost111" EnableViewState="false" />
                                </asp:Label>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox runat="server" ID="txtActCost" MaxLength="50" min="0" type="number" max="99999" placeholder="" onkeypress="return isNumber(event,this);" />
                                <%--meta:resourcekey="txttitleResource1"--%>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-3" style="padding-top: 10px;">
                                <asp:Label runat="server" ID="Label19" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                    <asp:Literal ID="Literal23" runat="server" meta:resourcekey="Priority" EnableViewState="false" />
                                </asp:Label>
                            </div>
                            <div class="ddpDivision col-md-9" style="width: 71.4%; margin-left: 2%;">
                                <asp:DropDownList Width="250px" ID="dd_Priotiy" runat="server" CssClass="chkliststyle form-control select2List1"
                                    RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                                    DataValueField="value" Style="width: 100%; overflow: auto;">
                                </asp:DropDownList>

                                <%--meta:resourcekey="txttitleResource1"--%>
                            </div>
                        </div>

                        <div class="form-group" runat="server" id="enableDiv">
                            <div class="col-md-3" style="padding-top: 10px;">
                                <asp:Label runat="server" ID="Label12" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                    <asp:Literal ID="Literal14" runat="server" meta:resourcekey="Enabled" EnableViewState="false" /></asp:Label>
                            </div>
                            <div class="col-md-9" style="margin-top: 10px">
                                <asp:RadioButtonList runat="server" ID="rdoEnabled" RepeatDirection="Horizontal" CssClass="inline-rb" TextAlign="Right">
                                    <asp:ListItem Text="Yes" meta:resourcekey="Yes" Value="true" Selected="True" />
                                    <asp:ListItem Text="No" meta:resourcekey="No" Value="false" />
                                </asp:RadioButtonList>
                                <%--meta:resourcekey="txttitleResource1"--%>
                            </div>
                        </div>

                        <div class="form-group" runat="server" id="publicDiv">
                            <div class="col-md-3" style="padding-top: 10px;">
                                <asp:Label runat="server" ID="Label13" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                    <asp:Literal ID="Literal15" runat="server" meta:resourcekey="Public" EnableViewState="false" /></asp:Label>

                            </div>
                            <div class="col-md-9">
                                <div class="col-md-4" style="margin-left: -15px; margin-top: 10px; display: none;">
                                    <asp:RadioButtonList runat="server" ID="rdoPublic" RepeatDirection="Horizontal" CssClass="inline-rb" TextAlign="Right">
                                        <asp:ListItem Text="Yes" meta:resourcekey="Yes" Value="true" />
                                        <asp:ListItem Text="No" meta:resourcekey="No" Value="false" Selected="True" />
                                    </asp:RadioButtonList>
                                </div>

                                <div class="ddpDivision col-md-5">
                                    <asp:DropDownList Width="250px" ID="chkListJobtype" runat="server" CssClass="chkliststyle form-control select2List1"
                                        RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                                        DataValueField="value" Style="width: 100%; overflow: auto;">
                                    </asp:DropDownList>
                                </div>

                                <div class="ddpDivision col-md-6" style="margin-left: 10px;">
                                    <asp:DropDownList Width="250px" ID="chkListDivision" runat="server" CssClass="chkliststyle form-control select2List1"
                                        RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                                        DataValueField="value" Style="width: 100%; overflow: auto;">
                                    </asp:DropDownList>
                                </div>
                                <%--meta:resourcekey="txttitleResource1"--%>
                            </div>
                        </div>

                        <div class="form-group">
                        </div>

                        <div class="form-group">
                        </div>

                        <div class="form-group">
                            <div class="col-md-3" style="padding-top: 10px;">
                                <asp:Label runat="server" ID="Label14" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                    <asp:Literal ID="Literal16" runat="server" meta:resourcekey="CompDevelopment" EnableViewState="false" /></asp:Label>
                            </div>
                            <div class="col-md-9" style="margin-top: 10px;">

                                <asp:RadioButtonList runat="server" ID="chkIsCompDevDoc" RepeatDirection="Horizontal" CssClass="inline-rb" TextAlign="Right">
                                    <asp:ListItem Text="Yes" meta:resourcekey="Yes" Value="true" />
                                    <asp:ListItem Text="No" meta:resourcekey="No" Value="false" Selected="True" />
                                </asp:RadioButtonList>
                            </div>
                        </div>



                        <div class="form-group">
                            <div class="col-md-3" style="padding-top: 10px;">
                                <asp:Label runat="server" ID="Label11" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                    <asp:Literal ID="Literal17" runat="server" meta:resourcekey="Competence" EnableViewState="false" /></asp:Label>
                            </div>
                            <div class="col-md-9">
                                <div class="ddpDivision col-md-4 ">
                                    <asp:DropDownList Width="250px" ID="ddCompetence" runat="server" CssClass="chkliststyle form-control"
                                        RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                                        DataValueField="value" Style="width: 100%; overflow: auto;">
                                    </asp:DropDownList>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-3" style="padding-top: 10px;">
                                <asp:Label runat="server" ID="Label15" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                    <asp:Literal ID="Literal18" runat="server" meta:resourcekey="Level" EnableViewState="false" /></asp:Label>
                            </div>
                            <div class="col-md-9" style="margin-top: 10px;">

                                <asp:RadioButtonList runat="server" ID="rdoLevel" RepeatDirection="Horizontal" RepeatLayout="Table" CssClass="inline-rb form-control" TextAlign="Right">
                                    <asp:ListItem Text="1 to 2" Value="1 to 2" meta:resourcekey="OneToTwoResource" Selected="True">1 to 2</asp:ListItem>
                                    <asp:ListItem Text="2 to 3" Value="2 to 3" meta:resourcekey="TwoToThreeResource">2 to 3</asp:ListItem>
                                    <asp:ListItem Text="3 to 4" Value="3 to 4" meta:resourcekey="ThreeToFourResource">3 to 4</asp:ListItem>
                                    <asp:ListItem Text="4 to 5" Value="4 to 5" meta:resourcekey="FourToFiveResource">4 to 5</asp:ListItem>
                                </asp:RadioButtonList><br />
                            </div>
                        </div>

                        <div class="form-group" id="justificationDiv" runat="server" visible="False">
                            <div class="col-md-3" style="padding-top: 10px;">
                                <asp:Label runat="server" ID="Label16" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                    <asp:Literal ID="Literal12" runat="server" meta:resourcekey="Justification" EnableViewState="false" />
                                </asp:Label>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox runat="server" ID="txtJustification" MaxLength="450" />
                                <%--meta:resourcekey="txttitleResource1"--%>
                            </div>
                        </div>

                        <div class="form-group" id="Expected_Behaviour_Change_Div" runat="server" visible="False">
                            <div class="col-md-3" style="padding-top: 10px;">
                                <asp:Label runat="server" ID="Label18" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                    <asp:Literal ID="Literal13" runat="server" meta:resourcekey="Expected_Behaviour_Change" EnableViewState="false" />
                                </asp:Label>
                            </div>
                            <div class="col-md-9">
                                <asp:TextBox runat="server" ID="txtExpctBehavChange" TextMode="MultiLine" MaxLength="450" />
                                <%--meta:resourcekey="txttitleResource1"--%>
                            </div>
                        </div>

                        <div class="form-group" id="divAssignTo" runat="server" visible="False">
                            <div class="col-md-3" style="padding-top: 10px;">
                                <asp:Label runat="server" ID="Label20" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                    <asp:Literal ID="Literal26" runat="server" meta:resourcekey="AssignTo" EnableViewState="false" />
                                </asp:Label>
                            </div>
                            <div class="col-md-9">
                                <div class="ddpDivision col-md-4 ">
                                    <asp:DropDownList Width="250px" ID="ddAssignToLists" runat="server" CssClass="chkliststyle form-control"
                                        RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                                        DataValueField="value" Style="width: 100%; overflow: auto;">
                                    </asp:DropDownList>
                                </div>
                                <div class=" col-md-4 ">
                                    <asp:Button runat="server"
                                        ID="btnAssign" Text="Complete"
                                        meta:resourcekey="btnAssign"
                                        CssClass="btn btn-warning yellow"
                                        type="button"
                                        ValidationGroup="chkdoc"
                                        OnClick="AssignActivity"
                                        OnClientClick=""
                                        Style="border-radius: 5px;"
                                        Visible="True" />
                                </div>
                            </div>
                        </div>

                        <%----------------------------------------------------------------------------------------------------------------------------------%>
                        <div class="col-md-12">
                            <div class="row" id="commentDivServerSide" runat="server">
                                <div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <asp:Label runat="server" ID="Label3" CssClass="col-md-3" Style="color: black; font-weight: bold; margin-top: 4px;">
                                                <asp:Literal ID="Literal19" runat="server" meta:resourcekey="Comments" EnableViewState="false" />
                                            </asp:Label>
                                        </div>
                                    </div>
                                    <br />
                                    <div class="reply-sec">
                                        <div class="reply-sec-MESSAGE" style="margin-left: 20px;">

                                            <%--<textarea cols="3" rows="3" meta:resourcekey="TYPEYOURCOMMENTHERE" placeholder="TYPE YOUR COMMENT HERE" id="txtcom" class="txtcomment" style="padding: 10px;width: 557px;height: 54px;" ></textarea>--%>
                                            <asp:TextBox TextMode="MultiLine" cols="3" Rows="3"
                                                ID="txtcom"
                                                CssClass="txtcomment"
                                                Style="padding: 10px; width: 557px; height: 54px;"
                                                runat="server">

                                            </asp:TextBox><a href="#" title="" class="black" onclick="insertcomment(true)"><i class="fa fa-comments-o"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div id="txtCmtDiv">
                                    <ul id="txtCmt">
                                        <li class="reply">
                                            <asp:Literal ID="Literal22" runat="server" meta:resourcekey="RecordNotfoundResource" EnableViewState="false" /></li>
                                    </ul>
                                </div>
                                <br />
                                <asp:TextBox ID="txtComment" align="right" runat="server" AutoPostBack="false" Text=""
                                    MaxLength="1" TextMode="MultiLine" Style="float: left; margin-left: 50px; width: 557px; height: 196px; display: none"
                                    Enabled="false"></asp:TextBox><br />

                                <a href="#" id='btnChat' onclick="return GetActivityCommentById()" data-index="txtComment" class="black" style="display: none; float: left; margin-left: 310px; margin-top: 8px; padding-left: 10px; padding-top: 10px; height: 35px; width: 35px;"><i class="fa fa-comments-o"></i></a>
                                <%----%>
                              


                            </div>

                            <%--meta:resourcekey="txttitleResource1"--%>
                        </div>

                        <%---------------------------------------------------------------------------------------------------------------------------------------%>
                    </div>
                    <div class="modal-footer">

                        <asp:Button runat="server" ID="btnComplete" Text="Complete" meta:resourcekey="btnComplete" CssClass="btn btn-primary yellow"
                            type="button" ValidationGroup="chkdoc" OnClick="CompleteActivity" OnClientClick="closeModel();"
                            Style="border-radius: 5px; width: 130px; height: 38px; margin-left: 359px;" Visible="False" />


                        <asp:Button runat="server" ID="btnRequest" Text="Request" meta:resourcekey="btnRequestResource1" CssClass="btn btn-primary yellow"
                            type="button" ValidationGroup="chkdoc" OnClick="RequestActivity" OnClientClick="closeModel();"
                            Style="border-radius: 5px; width: 100px; height: 38px; margin-left: 359px;" Visible="False" />

                        <asp:Button runat="server" ID="btnUpdate" Text="Update" meta:resourcekey="btnsubmitResourceupdate" CssClass="btn btn-primary yellow"
                            type="button" ValidationGroup="chkdoc" OnClick="UpdateActivity" OnClientClick="CheckValidations('chkdoc');closeModel();"
                            Style="border-radius: 5px; width: 100px; height: 38px; margin-left: 359px;" Visible="False" />


                        <asp:Button runat="server" ID="btnCreate" meta:resourcekey="btnsubmitResource1"
                            Text="Create" CssClass="btn btn-primary yellow"
                            type="button" ValidationGroup="chkdoc" OnClick="CreateActivity" OnClientClick="CheckValidations('chkdoc');closeModel();"
                            Style="border-radius: 5px; width: 100px; height: 38px; margin-left: 359px;" />


                        <%--   <asp:Button runat="server" ID="Button1" Text="Close" CssClass="btn btn-default black "
                            type="button" ValidationGroup="chkdoc" OnClick="CloseActivity" OnClientClick="closeModel();"
                            style="border-radius: 5px; width: 100px; height: 38px; color: white;" />--%>

                        <asp:Button runat="server" ID="Button3"
                            Text="Close" CssClass="btn btn-default black"
                            type="button" ValidationGroup="chkdoc" OnClick="CloseActivity" OnClientClick="closeModel();"
                            Style="border-radius: 5px; width: 100px; height: 38px;" />

                        <%-- <input type="reset" value="Close" class="btn btn-default black " aria-hidden="true" data-dismiss="modal"
                            style="border-radius: 5px; width: 100px; height: 38px; color: white;" />--%>
                    </div>
                </div>
            </div>
        </div>



        <div id="graph-wrapper">
            <div class="col-md-12">
                <div class="add-btn1" style="float: left;">
                    <a href="#popupAddNewDoc" data-toggle="modal" title="" style="display: none">
                        <asp:Label runat="server" ID="lbl" CssClass="lblModel" meta:resourcekey="AddNewActivity" Style="font-size: 19px; margin-top: 4px;"></asp:Label>
                        <asp:HiddenField runat="server" ID="hdnCost" Value='<%#Eval("ActCost") %>' />
                    </a>

                    <asp:Button runat="server" ID="Button1" Text="Close" CssClass="btn btn-default yellow " meta:resourcekey="AddNewActivity"
                        type="button" OnClick="OpenCreateActivity"
                        Style="border-radius: 5px; height: 38px; color: white;" />

                </div>

                <div style="clear: both;"></div>

                <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg"
                    meta:resourcekey="lblMsgResource1"></asp:Label><br />
                <asp:Label ID="hdnConfirmArchive" Style="display: none;" CssClass="hdnConfirmArchive" meta:resourcekey="ConfirmArchive" runat="server" />


                <div class="chart-tab manager_table">
                    <div id="tabs-container manager_table">


                        <div class="chat-widget-head1 yellow yellow-radius col-md-12">
                            <div class="col-md-2 ">
                                <h4 style="color: white">
                                    <asp:Literal ID="Literal20" runat="server" meta:resourcekey="Activity_Public_Lists" EnableViewState="false" />
                                </h4>
                            </div>
                            <div class="col-md-4 col-md-offset-2" style="margin-top: 8px">
                                <asp:DropDownList Width="250px"
                                    ID="ddCat_Filter" runat="server" CssClass="chkliststyle form-control" AppendDataBoundItems="False"
                                    RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description" OnSelectedIndexChanged="ddCat_Filter_OnSelectedIndexChanged"
                                    DataValueField="value" Style="width: 100%; overflow: auto;" AutoPostBack="True">
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-2 " style="margin-top: 8px">
                                <button class="btn btn-primary black" onclick="$('.adncPrivateDiv').toggle(); return false;">
                                    <asp:Literal ID="Literal21" runat="server" meta:resourcekey="AdvanceSearch" EnableViewState="false" />
                                </button>
                            </div>
                            <div class="col-md-2 ">

                                <asp:TextBox ID="txtSearch_Public" runat="server" placeholder="Search" AutoPostBack="true"
                                    OnTextChanged="AdvanceFilter_Public" CssClass="library_search"></asp:TextBox>
                            </div>
                            <%-- <h4 style="width: 100%">
                                            $1$  <%= CommonMessages.NewUploaedfiles%>#1#
                                            <asp:Literal ID="Literal20" runat="server" meta:resourcekey="NewUploaedfiles" EnableViewState="false" />
                                           sdf
                                        </h4>--%>
                        </div>
                        <div class="chat-widget widget-body col-md-12 adncPrivateDiv" style="display: none; -ms-border-radius: 0px; border-radius: 0px">
                            <div class="col-md-3 ">
                                <asp:TextBox runat="server" CssClass="library_search datepicker1 col-md-2"
                                    OnTextChanged="AdvanceFilter_Public"
                                    ID="txtSearchStartDate_Public"
                                    placeholder="Start Date"
                                    data-mask="99/99/2099" AutoPostBack="False" Style="height: 42px;"
                                    onmouseover="SetDatePicker();"></asp:TextBox>
                            </div>
                            <div class="col-md-3 ">
                                <asp:TextBox runat="server" CssClass="library_search datepicker1 col-md-2"
                                    OnTextChanged="AdvanceFilter_Public"
                                    placeholder="End Date"
                                    ID="txtSearchEndDate_Public" data-mask="99/99/2099"
                                    AutoPostBack="False"
                                    onmouseover="SetDatePicker();"
                                    Style="height: 42px;"></asp:TextBox>
                            </div>
                            <div class="col-md-2 " style="margin-top: 5px">
                                <asp:Button runat="server" ID="btnFilter" OnClick="AdvanceFilter_Public" meta:resourcekey="btnFilter" Text="Filter Now" CssClass="btn btn-default black col-md-12"
                                    CausesValidation="False"></asp:Button>
                            </div>
                        </div>

                        <%--EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'--%>
                        <asp:GridView ID="gvGrid_Public" runat="server" AutoGenerateColumns="False" CellPadding="0"
                            Width="100%" GridLines="None" DataKeyNames="ActId" CssClass="table1 table-striped table-bordered table-hover table-checkable datatable"
                           
                            BackColor="White"
                            meta:resourcekey="GridRecordNotfound"
                            OnRowCommand="gvGrid_RowCommand"
                            OnRowDataBound="gvGrid_Public_RowDataBound"
                            OnRowCreated="gvGrid_RowCreated"
                            OnPreRender="gvGrid_PreRender">


                            <HeaderStyle CssClass="aa" />
                            <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:TemplateField meta:resourcekey="SrNo">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="3%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />


                                </asp:TemplateField>

                                <%--<asp:TemplateField meta:resourcekey="ActivityType">
                                    <ItemTemplate>
                                        <%#Eval("CatNameInLang") %>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                    <FooterTemplate>
                                        <asp:Label runat="server" ID="asdf">
                                            <asp:Literal ID="Literal17" runat="server" meta:resourcekey="TotalCount" EnableViewState="false" /></asp:Label>

                                    </FooterTemplate></asp:TemplateField>--%>
                                <asp:TemplateField meta:resourcekey="ActivityName">
                                    <ItemTemplate>
                                        <%#Eval("ActName") %>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow ActName"
                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>

                                <asp:TemplateField meta:resourcekey="Creater">
                                    <ItemTemplate>
                                        <%#Eval("userFirstName") %>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="15%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>


                                <asp:TemplateField meta:resourcekey="Requester">
                                    <ItemTemplate>
                                        <%#Eval("requesterFirstName") %>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="15%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>

                                <%-- <asp:TemplateField meta:resourcekey="Division">
                                    <ItemTemplate>
                                         <asp:HiddenField runat="server" ID="hdnDivisionValue" Value=' <%# ((Eval("divName") == string.Empty ) || (Eval("divName") == null ))? "All" : Convert.ToString(Eval("divName"))%>' />
                                         <asp:Label runat="server" ID="lbldivision"> </asp:Label>
                                    
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>

                                <asp:TemplateField meta:resourcekey="JobType">
                                    <ItemTemplate>
                                        <asp:HiddenField runat="server" ID="hdnJobTypeValue" Value='<%# ((Eval("jobName") == string.Empty ) || (Eval("jobName") == null ))? "All": Convert.ToString(Eval("jobName"))%>' />
                                         <asp:Label runat="server" ID="lbljobtype"> </asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>--%>

                                <asp:TemplateField meta:resourcekey="StartDate">
                                    <ItemTemplate>
                                        <%#Convert.ToDateTime(Eval("ActStartDate")).ToString("dd-MMM-yyyy") %>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="35%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>

                                <asp:TemplateField meta:resourcekey="EndDate">
                                    <ItemTemplate>
                                        <%#Convert.ToDateTime(Eval("ActEndDate")).ToString("dd-MMM-yyyy") %>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="35%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>

                                <asp:TemplateField meta:resourcekey="Cost1">
                                    <ItemTemplate>
                                        $
                                        <asp:Label runat="server" ID="lblCost">    <%#Eval("ActCost") %></asp:Label>
                                        <asp:HiddenField runat="server" ID="hdnCost_Public" Value='<%#Eval("ActCost") %>' />

                                    </ItemTemplate>
                                    <FooterTemplate>
                                        $
                                        <asp:Label runat="server" ID="lblTotalCost_Public">   </asp:Label>
                                    </FooterTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />

                                </asp:TemplateField>

                                <%-- <asp:TemplateField meta:resourcekey="IsEnabled">
                                    <ItemTemplate>
                                         <asp:HiddenField runat="server" ID="hdnstate" Value='<%#Eval("ActIsActive") %>' />
                                        <asp:Label runat="server" ID="lblstate"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>--%>

                                <asp:TemplateField meta:resourcekey="Status">
                                    <ItemTemplate>

                                        <%--<%# ((Eval("ActReqStatus") == string.Empty ) || (Eval("ActReqStatus") == null ))? "Available" : Convert.ToString(Eval("ActReqStatus"))%>--%>

                                        <asp:HiddenField runat="server" ID="hdnActReqStatus" Value=' <%#Eval("ActReqStatus") %>' />
                                        <asp:HiddenField runat="server" ID="hdnActStatus" Value=' <%#Eval("ActStatus") %>' />
                                        <asp:HiddenField runat="server" ID="hdnActIsPublic" Value=' <%#Eval("ActIsPublic") %>' />
                                        <asp:HiddenField runat="server" ID="hdnActIsComp" Value=' <%#Eval("ActIsComp") %>' />
                                        <asp:HiddenField runat="server" ID="hdnActPublicID" Value=' <%#Eval("ActId") %>' />
                                         <asp:HiddenField runat="server" ID="hdnRequestUserID_Num" Value=' <%#Eval("ActReqUserId") %>' />

                                        <asp:Label runat="server" ID="lblActReqStatus"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>



                                <asp:TemplateField HeaderText="" ItemStyle-Width="50px"
                                    meta:resourcekey="TemplateFieldResource3">
                                    <ItemTemplate>
                                        <asp:HiddenField runat="server" ID="hdnActUmId" Value='<%# Eval("ActCreatedBy") %>' />
                                        <asp:HiddenField runat="server" ID="hdnActReqIDNo" Value='<%# Eval("ActReqId") %>' />

                                        <asp:Panel runat="server" ID="editPanel">


                                            <i class="fa fa-pencil"></i>
                                            <asp:LinkButton ID="lnkEdit" CssClass="def" runat="server" CommandName="EditRow" CommandArgument='<%# Eval("ActId") %>'
                                                ToolTip="Edit" meta:resourcekey="Edit"></asp:LinkButton>

                                        </asp:Panel>
                                        <asp:Panel runat="server" ID="archivePanle">
                                            <i class="fa fa-trash-o"></i>
                                            <asp:LinkButton ID="lnkBtnName" CssClass="def" runat="server" CommandName="archive" CommandArgument='<%# Eval("ActId") %>'
                                                ToolTip="Archive" meta:resourcekey="Archive" OnClientClick="var ConfirmArchive = function() {  confirm($('.hdnConfirmArchive').text())};return ConfirmArchive();"></asp:LinkButton>
                                        </asp:Panel>
                                        <asp:Panel runat="server" ID="viewPanel">
                                            <i class="fa fa-info-circle"></i>

                                            <asp:LinkButton ID="lnkView" CssClass="def" runat="server" CommandName="ViewRow" CommandArgument='<%# Eval("ActId") %>'
                                                ToolTip="View" meta:resourcekey="View">View</asp:LinkButton>

                                        </asp:Panel>

                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#cccccc" Font-Bold="True" ForeColor="Black" HorizontalAlign="Left" />
                        </asp:GridView>


                        <br />
                        <div class="add-btn1" style="float: left;">
                            <asp:Button runat="server" ID="Button2" Text="Close" CssClass="btn btn-default yellow " meta:resourcekey="AddNewPublicActivity"
                                type="button" OnClick="OpenCreatePublicActivity"
                                Style="border-radius: 5px; height: 38px; color: white; margin-top: 15px" />
                        </div>
                        <div style="clear: both;"></div>
                        <br />
                        <div class="chat-widget-head1 yellow yellow-radius col-md-12">
                            <div class="col-md-2 ">
                                <h4 style="color: white">
                                    <asp:Literal ID="Literal24" runat="server" meta:resourcekey="Activity_Private_Lists" EnableViewState="false" />
                                </h4>
                            </div>
                            <div class="col-md-4 col-md-offset-2" style="margin-top: 8px">
                                <asp:DropDownList
                                    ID="dd_Private_Cat" runat="server" CssClass="chkliststyle form-control" AppendDataBoundItems="False"
                                    RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description" OnSelectedIndexChanged="dd_Private_Cat_OnSelectedIndexChanged"
                                    DataValueField="value" Style="width: 100%; overflow: auto;" AutoPostBack="True">
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-2 " style="margin-top: 8px">
                                <button class="btn btn-primary black" onclick="$('.adncDiv').toggle(); return false;">
                                    <asp:Literal ID="Literal25" runat="server" meta:resourcekey="AdvanceSearch" EnableViewState="false" />
                                </button>
                            </div>
                            <div class="col-md-2 ">

                                <asp:TextBox ID="txtSearch_Private" runat="server" placeholder="Search" AutoPostBack="true"
                                    OnTextChanged="AdvanceFilter_Private" CssClass="library_search"></asp:TextBox>
                            </div>
                            <%-- <h4 style="width: 100%">
                                            $1$  <%= CommonMessages.NewUploaedfiles%>#1#
                                            <asp:Literal ID="Literal20" runat="server" meta:resourcekey="NewUploaedfiles" EnableViewState="false" />
                                           sdf
                                        </h4>--%>
                        </div>
                        <div class="chat-widget widget-body col-md-12 adncDiv" style="display: none; -ms-border-radius: 0px; border-radius: 0px">
                            <div class="col-md-3 ">
                                <asp:TextBox runat="server" CssClass="library_search datepicker1 col-md-2"
                                    OnTextChanged="AdvanceFilter_Private"
                                    ID="txtStartDate_Private"
                                    placeholder="Start Date"
                                    data-mask="99/99/2099" AutoPostBack="False" Style="height: 42px;"
                                    onmouseover="SetDatePicker();"></asp:TextBox>
                            </div>
                            <div class="col-md-3 ">
                                <asp:TextBox runat="server" CssClass="library_search datepicker1 col-md-2"
                                    OnTextChanged="AdvanceFilter_Private"
                                    placeholder="End Date"
                                    ID="txtEndDate_Private" data-mask="99/99/2099"
                                    AutoPostBack="False"
                                    onmouseover="SetDatePicker();"
                                    Style="height: 42px;"></asp:TextBox>
                            </div>
                            <div class="col-md-2 " style="margin-top: 5px">
                                <asp:Button runat="server" ID="Button4" OnClick="AdvanceFilter_Private" meta:resourcekey="btnFilter" Text="Filter Now" CssClass="btn btn-default black col-md-12"
                                    CausesValidation="False"></asp:Button>
                            </div>
                        </div>

                        <asp:GridView ID="gvGrid_Private" runat="server" AutoGenerateColumns="False" CellPadding="0"
                            Width="100%" GridLines="None" DataKeyNames="ActId" CssClass="table1 table-striped table-bordered table-hover table-checkable datatable"
                            
                            BackColor="White"
                            meta:resourcekey="GridRecordNotfound"
                            OnRowCommand="gvGrid_RowCommand"
                            OnRowDataBound="gvGrid_Private_RowDataBound"
                            OnRowCreated="gvGrid_RowCreated"
                            OnPreRender="gvGrid_Private_PreRender">


                            <HeaderStyle CssClass="aa" />
                            <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:TemplateField meta:resourcekey="SrNo">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="3%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />


                                </asp:TemplateField>

                                <%--   <asp:TemplateField meta:resourcekey="ActivityType">
                                    <ItemTemplate>
                                        <%#Eval("CatNameInLang") %>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                    <FooterTemplate>
                                        <asp:Label runat="server" ID="asdf">
                                            <asp:Literal ID="Literal17" runat="server" meta:resourcekey="TotalCount" EnableViewState="false" /></asp:Label>

                                    </FooterTemplate>

                                </asp:TemplateField>--%>
                                <asp:TemplateField meta:resourcekey="ActivityName">
                                    <ItemTemplate>
                                        <%#Eval("ActName") %>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow ActName"
                                        Width="35%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>

                                <asp:TemplateField meta:resourcekey="Creater">
                                    <ItemTemplate>
                                        <%#Eval("userFirstName") %>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>

                                <%--  <asp:TemplateField meta:resourcekey="Division">
                                    <ItemTemplate>
                                         <asp:HiddenField runat="server" ID="hdnDivisionValue1" Value=' <%# ((Eval("divName") == string.Empty ) || (Eval("divName") == null ))? "All" : Convert.ToString(Eval("divName"))%>' />
                                         <asp:Label runat="server" ID="lbldivision1"> </asp:Label>

                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>

                                <asp:TemplateField meta:resourcekey="JobType">
                                    <ItemTemplate>

                                         <asp:HiddenField runat="server" ID="hdnJobTypeValue1" Value='<%# ((Eval("jobName") == string.Empty ) || (Eval("jobName") == null ))? "All": Convert.ToString(Eval("jobName"))%>' />
                                         <asp:Label runat="server" ID="lbljobtype1"> </asp:Label>

                                      
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>--%>

                                <asp:TemplateField meta:resourcekey="StartDate">
                                    <ItemTemplate>
                                        <%#Convert.ToDateTime(Eval("ActStartDate")).ToString("dd-MMM-yyyy") %>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="35%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>

                                <asp:TemplateField meta:resourcekey="EndDate">
                                    <ItemTemplate>
                                        <%#Convert.ToDateTime(Eval("ActEndDate")).ToString("dd-MMM-yyyy") %>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="35%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>

                                <asp:TemplateField meta:resourcekey="Cost1">
                                    <ItemTemplate>
                                        $
                                        <asp:Label runat="server" ID="lblCost">    <%#Eval("ActCost") %></asp:Label><asp:HiddenField runat="server" ID="hdnCost_Private" Value='<%#Eval("ActCost") %>' />

                                    </ItemTemplate>
                                    <FooterTemplate>
                                        $
                                        <asp:Label runat="server" ID="lblTotalCost_Private">   </asp:Label>
                                    </FooterTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />

                                </asp:TemplateField>

                                <asp:TemplateField meta:resourcekey="IsEnabled">
                                    <ItemTemplate>
                                        <asp:HiddenField runat="server" ID="hdnActReqStatus" Value=' <%#Eval("ActReqStatus") %>' />
                                        <asp:HiddenField runat="server" ID="hdnActStatus" Value=' <%#Eval("ActStatus") %>' />
                                        <asp:HiddenField runat="server" ID="hdnActIsPublic" Value=' <%#Eval("ActIsPublic") %>' />
                                        <asp:HiddenField runat="server" ID="hdnActIsComp" Value=' <%#Eval("ActIsComp") %>' />
                                         <asp:HiddenField runat="server" ID="hdnRequestUserID_Num" Value=' <%#Eval("ActReqUserId") %>' />
                                        <asp:HiddenField runat="server" ID="hdnstate1" Value='<%#Eval("ActIsActive") %>' />
                                        <asp:Label runat="server" ID="lblstate1"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>

                                <%-- <asp:TemplateField meta:resourcekey="Status">
                                    <ItemTemplate>

                                        <%# ((Eval("ActReqStatus") == string.Empty ) || (Eval("ActReqStatus") == null ))? "Available" : Convert.ToString(Eval("ActReqStatus"))%>

                                      

                                        <asp:Label runat="server" ID="lblActReqStatus">

                                        </asp:Label>

                                    </ItemTemplate><ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>--%>



                                <asp:TemplateField HeaderText="" ItemStyle-Width="50px"
                                    meta:resourcekey="TemplateFieldResource3">
                                    <ItemTemplate>
                                        <asp:HiddenField runat="server" ID="hdnActUmId" Value='<%# Eval("ActCreatedBy") %>' />
                                        <asp:HiddenField runat="server" ID="hdnActReqIDNo" Value='<%# Eval("ActReqId") %>' />
                                        <asp:Panel runat="server" ID="editPanel">


                                            <i class="fa fa-pencil"></i>
                                            <asp:LinkButton ID="lnkEdit" CssClass="def" runat="server" CommandName="EditRowPrivate" CommandArgument='<%# Eval("ActId") %>'
                                                ToolTip="Edit" meta:resourcekey="Edit"></asp:LinkButton>
                                        </asp:Panel>
                                        <asp:Panel runat="server" ID="archivePanle">
                                            <i class="fa fa-trash-o"></i>

                                            <asp:LinkButton ID="lnkBtnName" CssClass="def" runat="server" CommandName="archive" CommandArgument='<%# Eval("ActId") %>'
                                                ToolTip="Archive" meta:resourcekey="Archive" OnClientClick="var ConfirmArchive = function() {  confirm($('.hdnConfirmArchive').text())};return ConfirmArchive();"></asp:LinkButton>
                                        </asp:Panel>
                                        <asp:Panel runat="server" ID="viewPanel">
                                            <i class="fa fa-info-circle"></i>

                                            <asp:LinkButton ID="lnkView" CssClass="def" runat="server" CommandName="ViewRowPrivate" CommandArgument='<%# Eval("ActId") %>'
                                                ToolTip="View" meta:resourcekey="ViewRequest">   </asp:LinkButton>
                                        </asp:Panel>

                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                </asp:TemplateField>
                            </Columns>
                            <FooterStyle BackColor="#cccccc" Font-Bold="True" ForeColor="Black" HorizontalAlign="Left" />
                        </asp:GridView>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="modal-footer" style="border-top: 0px !important; margin-left: -1px;">
        <asp:Button runat="server" ID="btnBack" Text="Back" CssClass="btn black pull-right"
            OnClick="btnBack_click" Style="margin: 4px; display: none" />
        <%--  <a href="#" onclick="javascript:history.back(); return false;" class="btn black pull-right" style="border-radius: 5px;margin-left:0px;margin-top:5px;float:right;">Back</a>--%>
    </div>
    <div style="display: none">
        <asp:HiddenField ID="RequestSentSuccessful" meta:resourcekey="RequestSentSuccess" runat="server" />
        <asp:Literal ID="hdnAddNewActivity" meta:resourcekey="AddNewActivity" runat="server" />
        <asp:HiddenField ID="hdnUpdateActivity" meta:resourcekey="UpdateActivity" runat="server" />
        <asp:HiddenField ID="hdnViewActivity" meta:resourcekey="ViewActivity" runat="server" />


        <asp:HiddenField ID="hdnCurrentActivityStatus" Value="0" runat="server" />
    </div>



    <%--   </ContentTemplate>
    </asp:UpdatePanel>--%>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery.min.js"></script>


    <script src="../Scripts/pickadate.js-3.5.6/lib/legacy.js" type="text/javascript"></script>
    <script src="../Scripts/pickadate.js-3.5.6/lib/picker.js" type="text/javascript"></script>
    <script src="../Scripts/pickadate.js-3.5.6/lib/picker.date.js" type="text/javascript"></script>
    <script src="../Scripts/pickadate.js-3.5.6/lib/picker.time.js" type="text/javascript"></script>
    <script src="../assets/js/libs/tagsInput.js" type="text/javascript"></script>


  <%--  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>--%>
  <%--  <script src="../Scripts/select2-4.0.3/src/js/jquery.select2.js"></script>--%>

    <script type="text/javascript">
        $(document).ready(function () {




            setTimeout(function () {
                SetExpandCollapse();


            }, 500);

            setTimeout(function () {
                SetDatePicker();
                activateTagsInput();
                select2List();
            }, 1500);


            var yesterday = new Date((new Date()).valueOf() - 1000 * 60 * 60 * 24);

            $(".txtActStartDate").pickadate({
                format: datePickerFormate, disable: [
                    { from: [0, 0, 0], to: yesterday }
                ]
            });


            $(".txtActEndDate").pickadate({
                format: datePickerFormate, disable: [
                    { from: [0, 0, 0], to: yesterday }
                ]
            });


            $(".datepicker1").pickadate({
                format: datePickerFormate
            });

            ///


            $('.txtActStartDate').on('change', function () {
                $('.txtActEndDate').pickadate('picker').set('min', $(this).val());
            });
        });


        function select2List() {

            //$(".select2List").select2({
            //    placeholder: 'Select an option'
            //});
        }

        var datePickerFormate = 'dd/mm/yyyy';
        function SetDatePicker() {

            var yesterday = new Date((new Date()).valueOf() - 1000 * 60 * 60 * 24);

            $(".txtActStartDate").pickadate({
                format: datePickerFormate, disable: [
                    { from: [0, 0, 0], to: yesterday }
                ]
            });


            $(".txtActEndDate").pickadate({
                format: datePickerFormate, disable: [
                    { from: [0, 0, 0], to: yesterday }
                ]
            });


            $(".datepicker1").pickadate({
                format: datePickerFormate
            });

            ///


            $('.txtActStartDate').on('change', function () {
                $('.txtActEndDate').pickadate('picker').set('min', $(this).val());
            });
        }
        function UpdateResourceOfTag() {
            $("#ContentPlaceHolder1_txtActTags_tag").val($("#ContentPlaceHolder1_hdnAddATag").val());
        }
        function activateTagsInput() {
            setTimeout(function () {
                //  $('.tagsinput').tagsInput({ onAddTag: UpdateResourceOfTag });
                $('.tagsinput').tagsInput({});
                $(".tagsinput").css("width", "100%");
                $("#ContentPlaceHolder1_txtActTags_tag").val($("#ContentPlaceHolder1_hdnAddATag").val());
                $("#ContentPlaceHolder1_txtActTags_tag").css("width", "100%");
            }, 500);
        }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {
            // GetActivityCommentById(true);
        });
        function insertcomment(isFirstTime) {

            var message = $(".txtcomment").val();
            var ActReqId = $('#ContentPlaceHolder1_hdnActReqIdInt').val();
            var ActID = $('#ContentPlaceHolder1_hdnActId').val();
            var ActReqUserId = $('#ContentPlaceHolder1_hdnAcrCreaterId').val();

           
            if (ActReqId != '0') {
                if (message != '') {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",

                        //url: "View_request.aspx/insertmessage",

                        url: "ActivityLists.aspx/insertmessage",
                        data: "{'message':'" + message + "','ActReqId':'" + ActReqId + "','ActReqUserId':'" + ActReqUserId + "','ActID':'" + ActID + "'}",
                        //data: "{'message':'" + message + "','ActReqId':'" + ActReqId + ",'ActReqUserId':'" + ActReqUserId + "'}",
                        dataType: "json",
                        success: function (data) {
                            var obj = data.d;
                            if (obj == 'true') {

                                $(".txtcomment").val("");
                                // document.getElementById("txtcomment").value = '';
                                // GetmessgeById(touserid);
                                //GetmessgeById_afterinsert(touserid)
                                GetActivityCommentById(isFirstTime);

                                setTimeout(function () {
                                    $("#scrollbox6").scrollTop(99999999999999999999);
                                }, 122);
                                $("#" + aId).prev().prev().text(message); // copy text and paste on parent page's comment box
                            }
                        },
                        error: function (result) {
                            // alert("Error");
                        }
                    });
                }
                else {
                    generate("warning", "Oops! You have missed text to send a massage.", "bottomCenter");
                    return false;
                }
            }
            else {
                generate("warning", "Please Select Contact", "bottomCenter");

                document.getElementById("message").value = '';
                return false;
            }
        }


        var aId;
        var html = "";
        function GetActivityCommentById(isFirstTime) {


            var AcrCreaterId = $('#ContentPlaceHolder1_hdnActReqIdInt').val();
            var ActID = $('#ContentPlaceHolder1_hdnActId').val();
            var LoginUserID = $('#ContentPlaceHolder1_hdnLoginUserID').val();
            var UserType = $('#ContentPlaceHolder1_hdnUserType').val();
            //alert(AcrCreaterId);
            //alert(ActID);

            $("#scrollbox6").scrollTop(99999999999999999999);
            $("#txtCmt").scrollTop(99999999999999999999);
            //var comid = document.getElementById("hdncom").value;
            html = "";
            $("#scrollbox6").html('');
            $("#txtCmt").html('');



            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "ActivityLists.aspx/GetActivityCommentById",
                data: "{'AcrCreaterId':'" + AcrCreaterId + "','ActID':'" + ActID + "','LoginUserID':'" + LoginUserID + "','UserType':'" + UserType + "'}",
                dataType: "json",
                success: function (data) {

                    //$("#hdncom").val(a);

                    //cat = data.d;

                    for (var i = 0; i < data.d.length; i++) {

                        var cls = "reply";

                        html += "<li class='" + cls + "'><div class='chat-thumb'><img src='../Log/upload/Userimage/" + data.d[i].userImage + "' alt=''/></div>";
                        html += "<div class='chat-desc'><p>" + data.d[i].AcrCompComm + "</p><i class='chat-time'>" + parseJsonDate(data.d[i].AcrComCreateDate) + "</i>";
                        html += "</div></li>";
                    }

                    if (data.d.length == 0) {
                        $('#txtCmtDiv').fadeOut();

                        html += "No records found.";
                    }
                    else {
                        $('#txtCmtDiv').fadeIn();
                    }


                    $("#scrollbox6").append(html);
                    $("#txtCmt").append(html);

                    $("#scrollbox6").scrollTop(99999999999999999999);
                    $("#txtCmt").scrollTop(99999999999999999999);
                },


                error: function (result) {
                    //alert("Error");
                    $('#txtCmtDiv').fadeOut();

                }
            });
        }

        function parseJsonDate(jsonDateString) {


            //return new Date(parseInt(jsonDateString.replace('/Date(', '')));

            var formattedDate = new Date(parseInt(jsonDateString.substr(6)));
            var d = formattedDate.getDate();
            var m = formattedDate.getMonth();
            m += 1; // JavaScript months are 0-11
            var y = formattedDate.getFullYear();
            var min = formattedDate.getMinutes();
            var hour = formattedDate.getHours();
            var sec = formattedDate.getSeconds();

            //$("#txtDate").val(d + "." + m + "." + y);
            return d + "/" + m + "/" + y + " " + hour + ":" + min + ":" + sec;
        }

        function formatDate(d) {
            if (hasTime(d)) {
                var s = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
                s += ' ' + d.getHours() + ':' + zeroFill(d.getMinutes()) + ':' + zeroFill(d.getSeconds());
            } else {
                var s = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
            }

            return s;
        }
    </script>
 <%--   <script src="http://declanbright.awardspace.info/downloads/jquery.responsivetable.min.js" type="text/javascript"></script>--%>


    <%-- <!-- DataTables -->
    <script type="text/javascript" src="../plugins/datatables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../plugins/datatables/tabletools/TableTools.min.js"></script>
    <!-- optional -->
    <script type="text/javascript" src="../plugins/datatables/colvis/ColVis.min.js"></script>
    <!-- optional -->
    <script type="text/javascript" src="../plugins/datatables/DT_bootstrap.js"></script>--%>

   <%-- <script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>--%>

    <%--<script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/dataTables.jqueryui.min.js"></script>--%>

    <script type="text/javascript">
        $(document).ready(function () {
            //$("#ContentPlaceHolder1_gvGrid_Public").dataTable({
            //    //"paging": false,
            //    "pageLength": 5000,
            //    responsive: true
            //});
            //$("#ContentPlaceHolder1_gvGrid_Private").dataTable({
            //    //"paging": false,
            //    "pageLength": 5000,
            //    responsive: true
            //});
            $(".dataTables_filter").hide();
            $(".dataTables_length").hide();
            $(".dataTables_info").hide();
            $(".dataTables_paginate").hide();

        });
    </script>
</asp:Content>
