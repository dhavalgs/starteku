﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="Point.aspx.cs" Inherits="Organisation_Point" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="col-md-6">
        <div class="heading-sec">
            <h1>
              <%-- <%= CommonMessages.SetPoint%> --%>
             <asp:Literal ID="Literal1" runat="server" meta:resourcekey="SetPoint" enableviewstate="false"/> <i><span runat="server" id="Settings"></span></i>
            </h1>
        </div>
    </div>
    <%--Header Tab Button BIG----------------------------------------------------------------------20150109--%>
    <div class="col-md-12" style="margin-left: -10px; margin-top: 23px;">
        <div class="col-md-2">
            <div class="stat-boxes widget-body real-time pdp-point-round " style="border-radius: 5px;
                height: 75px;">
                <a href="#" onclick="showHideDiv('All');">
                    <h3>
                    <asp:Literal ID="Literal2" runat="server" meta:resourcekey="All" enableviewstate="false"/>
                         <%--<%= CommonMessages.All%> --%>
                    </h3>
                    <h4>
                        <i>PLP + ALP + PDP</i></h4>
                </a>
            </div>
        </div>
        <div class="col-md-2" style="height: 50px;">
            <div class="stat-boxes widget-body real-time alp-point-round" style="border-radius: 5px;">
                <a href="#" onclick="showHideDiv('PLP');">
                    <h3>
                        PLP
                    </h3>
                    <h4>
                        <i>  <asp:Literal ID="Literal3" runat="server" meta:resourcekey="PassiveLearningPoints" enableviewstate="false"/> <%--<%= CommonMessages.PassiveLearningPoints%>--%></i>
                    </h4>
                </a>
            </div>
        </div>
        <div class="col-md-2 ">
            <div class="stat-boxes widget-body real-time plp-point-round " style="border-radius: 5px;">
                <a href="#" onclick="showHideDiv('ALP');">
                    <h3>
                        ALP
                    </h3>
                    <h4>
                        <i>  <asp:Literal ID="Literal4" runat="server" meta:resourcekey="ActiveLearningPoints" enableviewstate="false"/> <%--<%= CommonMessages.ActiveLearningPoints%>--%></i></h4>
                </a>
            </div>
        </div>
         <div class="col-md-2 ">
            <div class="stat-boxes widget-body real-time plp-point-round " style="border-radius: 5px;">
                <a href="#" onclick="showHideDiv('PDP');">
                    <h3>
                        PDP
                    </h3>
                    <h4>
                        <i>  <asp:Literal ID="Literal20" runat="server" meta:resourcekey="PDPPoints" enableviewstate="false"/> <%--<%= CommonMessages.ActiveLearningPoints%>--%></i></h4>
                </a>
            </div>
        </div>
        <%--<div class="col-md-2">
                    <div class="stat-boxes widget-body real-time pdp-point-round" style="border-radius: 5px;">
                        <a href="pdp_points.html">
                            <h3>
                               <asp:Label ID="lblTotal" runat="server"></asp:Label></h3>
                            <i>PDP</i> </a>
                    </div>
                </div>--%>
        <%--   <div class="col-md-2">
                    <div class="stat-boxes widget-body real-time total-point-round " style="border-radius: 5px;">
                        <a href="apl_points.aspx?All=1">
                            <h3>
                                <asp:Label ID="lblTotal" runat="server" Text="0"></asp:Label>
                            </h3>
                            <i>Total</i> </a>
                    </div>
                </div>--%>
    </div>
    <%-- <div class="invoice col-md-12" style="width: 98%;margin: 19px 0 15px 13px;height: 53px;" id="ALP" runat="server">
                        <h2 class="pointtable_h2" style="margin-bottom: -11px;">
                              Active Learning Points
                          </h2> </div>--%>
    <ul class="your-message" style="margin-top: 30px;">
        <div class="col-md-12">
            <li style="margin-bottom: 10px; background: #fff; padding: 20px 0px;">
                <div class="setting_lable col-md-10" style="padding-top: 1px; padding-bottom: 0;">
                    <span class="col-md-3" id="myspan" style="margin-top: -4px; color: #0b5d99 !important;">
                        All (PLP + ALP + PDP) </span>
                </div>
            </li>
        </div>
    </ul>
    <ul class="your-message" style="margin-top: 0px;">
        <%--  margin-top: 30px;--%>
        <div class="col-md-12  PLP">
            <li style="margin-bottom: 10px; background: #fff; padding: 20px 0px;">
                <div class="setting_lable col-md-10">
                    <span class="col-md-3" style="margin-top: -4px;"> <asp:Literal ID="Literal5" runat="server" meta:resourcekey="Account" enableviewstate="false"/><%--<%= CommonMessages.Account%>--%> </span>
                    <div class="col-md-8">
                    <asp:Literal ID="Literal6" runat="server" meta:resourcekey="SignIn" enableviewstate="false"/>
                       <%-- <%= CommonMessages.SignIn%>--%></div>
                </div>
                <div class="col-md-21">
                    <asp:TextBox ID="txtplp" runat="server" Text="0" Style="margin: 9px; height: 31px;
                        width: 70px;" meta:resourcekey="txtplpResource1"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" CssClass="commonerrormsg"
                        Style="color: red;" ControlToValidate="txtplp" Display="Dynamic" ValidationGroup="chk"
                        ErrorMessage="Please Enter PLP." meta:resourcekey="rfvEmailResource1"></asp:RequiredFieldValidator>
                </div>
            </li>
            <li style="margin-bottom: 10px; background: #fff; padding: 20px 0px;">
                <div class="setting_lable col-md-10">
                    <span class="col-md-3" style="margin-top: -4px;"> <asp:Literal ID="Literal7" runat="server" meta:resourcekey="CompetencesSkill" enableviewstate="false"/><%--<%= CommonMessages.CompetencesSkill%> --%></span>
                    <div class="col-md-8">
                      <asp:Literal ID="Literal8" runat="server" meta:resourcekey="OpenCompetencesScreen" enableviewstate="false"/>  <%--<%= CommonMessages.OpenCompetencesScreen%>--%> 
                    </div>
                </div>
                <div class="col-md-21">
                    <asp:TextBox ID="txtCompetences" runat="server" Text="0" Style="margin: 9px; height: 31px;
                        width: 70px;" meta:resourcekey="txtCompetencesResource1"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" CssClass="commonerrormsg"
                        Style="color: red;" ControlToValidate="txtCompetences" Display="Dynamic" ValidationGroup="chk"
                        ErrorMessage="Please Enter Competences." 
                        meta:resourcekey="RequiredFieldValidator6Resource1"></asp:RequiredFieldValidator>
                </div>
            </li>
            <li style="margin-bottom: 10px; background: #fff; padding: 20px 0px;">
                <div class="setting_lable col-md-10">
                    <span class="col-md-3" style="margin-top: -4px;"><asp:Literal ID="Literal10" runat="server" meta:resourcekey="Competence" enableviewstate="false"/>  <%--<%= CommonMessages.Competence%>--%></span>
                    <div class="col-md-8">
                     <asp:Literal ID="Literal9" runat="server" meta:resourcekey="FinalFillCompetences" enableviewstate="false"/>  <%-- <%= CommonMessages.FinalFillCompetences%> --%>
                    </div>
                </div>
                <div class="col-md-21">
                    <asp:TextBox ID="txtsetCompetences" runat="server" Text="0" Style="margin: 9px; height: 31px;
                        width: 70px;" meta:resourcekey="txtsetCompetencesResource1"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" CssClass="commonerrormsg"
                        Style="color: red;" ControlToValidate="txtCompetences" Display="Dynamic" ValidationGroup="chk"
                        ErrorMessage="Please Enter Competences." 
                        meta:resourcekey="RequiredFieldValidator7Resource1"></asp:RequiredFieldValidator>
                </div>
            </li>
            <li style="margin-bottom: 10px; background: #fff; padding: 20px 0px;">
                <div class="setting_lable col-md-10">
                    <%-- <span class="col-md-3"><%= CommonMessages.Knowledgesharingrequests%></span>--%>
                    <span class="col-md-3" style="margin-top: -4px;"> <asp:Literal ID="Literal11" runat="server" meta:resourcekey="ReadDoc" enableviewstate="false"/><%-- <%= CommonMessages.ReadDoc%>--%></span>
                    <div class="col-md-8">
                   <asp:Literal ID="Literal12" runat="server" meta:resourcekey="ReadDocument" enableviewstate="false"/>   <%--<%= CommonMessages.ReadSuggestDoc%>--%>  
                    </div>
                </div>
                <div class="col-md-21">
                    <asp:TextBox ID="txtread" runat="server" Text="0" Style="margin: 9px; height: 31px;
                        width: 70px;" meta:resourcekey="txtreadResource1"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" CssClass="commonerrormsg"
                        Style="color: red;" ControlToValidate="txtread" Display="Dynamic" ValidationGroup="chk"
                        ErrorMessage="Please Enter Read Doc." 
                        meta:resourcekey="RequiredFieldValidator5Resource1"></asp:RequiredFieldValidator>
                </div>
            </li>
        </div>
        <div class="col-md-12  ALP">
            <%--  <ul class="your-message" style="margin-top: 30px;">--%>
            <li style="margin-bottom: 10px; background: #fff; padding: 20px 0px;">
                <div class="setting_lable col-md-10">
                    <%-- <span class="col-md-3"><%= CommonMessages.Competencelevelschange%></span>--%>
                    <span class="col-md-3" style="margin-top: -4px;"> <asp:Literal ID="Literal13" runat="server" meta:resourcekey="ReadSuggestDoc" enableviewstate="false"/> <%--<%= CommonMessages.ReadSuggestDoc%> --%></span>
                    <div class="col-md-8">
                     <asp:Literal ID="Literal14" runat="server" meta:resourcekey="NeedHelp" enableviewstate="false"/>  <%-- <%= CommonMessages.NeedHelp%>--%> </div>
                </div>
                <div class="col-md-21">
                    <asp:TextBox ID="txtRequestSession" runat="server" Text="0" Style="margin: 9px; height: 31px;
                        width: 70px;" meta:resourcekey="txtRequestSessionResource1"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" CssClass="commonerrormsg"
                        ControlToValidate="txtRequestSession" Display="Dynamic" ValidationGroup="chk"
                        Style="color: red;" ErrorMessage="Please Enter Request Session." 
                        meta:resourcekey="RequiredFieldValidator1Resource1"></asp:RequiredFieldValidator>
                </div>
            </li>
            <li style="margin-bottom: 10px; background: #fff; padding: 20px 0px;">
                <div class="setting_lable col-md-10">
                    <span class="col-md-3" style="margin-top: -4px;">  <asp:Literal ID="Literal15" runat="server" meta:resourcekey="SuggestSession" enableviewstate="false"/> <%-- <%= CommonMessages.SuggestSession%>--%> </span>
                    <div class="col-md-8">
                    <asp:Literal ID="Literal16" runat="server" meta:resourcekey="GiveHelp" enableviewstate="false"/>   <%-- <%= CommonMessages.GiveHelp%>--%>  </div>
                </div>
                <div class="col-md-21">
                    <asp:TextBox ID="txtSuggestSession" runat="server" Text="0" Style="margin: 9px; height: 31px;
                        width: 70px;" meta:resourcekey="txtSuggestSessionResource1"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" CssClass="commonerrormsg"
                        ControlToValidate="txtSuggestSession" Display="Dynamic" ValidationGroup="chk"
                        Style="color: red;" ErrorMessage="Please Enter Suggest Session." 
                        meta:resourcekey="RequiredFieldValidator2Resource1"></asp:RequiredFieldValidator>
                </div>
            </li>
            <li style="margin-bottom: 10px; background: #fff; padding: 20px 0px;">
                <div class="setting_lable col-md-10">
                    <%-- <span class="col-md-3"><%= CommonMessages.Knowledgesharingrequests%></span>--%>
                    <span class="col-md-3" style="margin-top: -4px;">  <asp:Literal ID="Literal17" runat="server" meta:resourcekey="SuggestDoc" enableviewstate="false"/> <%-- <%= CommonMessages.SuggestDoc%>--%></span>
                    <div class="col-md-8">
                 <asp:Literal ID="Literal18" runat="server" meta:resourcekey="SuggestDoctoother" enableviewstate="false"/>   <%--   <%= CommonMessages.SuggestDoctoother%> --%> </div>
                </div>
                <div class="col-md-21">
                    <asp:TextBox ID="txtSuggestdoc" runat="server" Text="0" Style="margin: 9px; height: 31px;
                        width: 70px;" meta:resourcekey="txtSuggestdocResource1"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" CssClass="commonerrormsg"
                        Style="color: red;" ControlToValidate="txtSuggestdoc" Display="Dynamic" ValidationGroup="chk"
                        ErrorMessage="Please Enter Suggest Doc." 
                        meta:resourcekey="RequiredFieldValidator4Resource1"></asp:RequiredFieldValidator>
                </div>
            </li>
             <li style="margin-bottom: 10px; background: #fff; padding: 20px 0px;">
                <div class="setting_lable col-md-10">
                    <%-- <span class="col-md-3"><%= CommonMessages.Knowledgesharingrequests%></span>--%>
                    <span class="col-md-3" style="margin-top: -4px;">  <asp:Literal ID="Literal29" runat="server" meta:resourcekey="AcceptHelp" enableviewstate="false"/> <%-- <%= CommonMessages.SuggestDoc%>--%></span>
                    <div class="col-md-8">
                 <asp:Literal ID="Literal30" runat="server" meta:resourcekey="AcceptTheHelp" enableviewstate="false"/>   <%--   <%= CommonMessages.SuggestDoctoother%> --%> </div>
                </div>
                <div class="col-md-21">
                    <asp:TextBox ID="txtAcceptHelp" runat="server" Text="0" Style="margin: 9px; height: 31px;
                        width: 70px;" meta:resourcekey="txtAcceptHelpResource1"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" CssClass="commonerrormsg"
                        Style="color: red;" ControlToValidate="txtAcceptHelp" Display="Dynamic" ValidationGroup="chk"
                        ErrorMessage="Please Enter Suggest Doc." 
                        meta:resourcekey="RequiredFieldValidator12esource1"></asp:RequiredFieldValidator>
                </div>
            </li>
            <li style="margin-bottom: 10px; background: #fff; padding: 20px 0px;">
                <div class="setting_lable col-md-10">
                    <%-- <span class="col-md-3"><%= CommonMessages.Knowledgesharingrequests%></span>--%>
                    <span class="col-md-3" style="margin-top: -4px;">  <asp:Literal ID="Literal19" runat="server" meta:resourcekey="PublicDoc" enableviewstate="false"/> <%--<%= CommonMessages.PublicDoc%>--%></span>
                    <div class="col-md-8">
                       <asp:Literal ID="Literal31" runat="server" meta:resourcekey="UploadDoc" enableviewstate="false"/></div>
                </div>
                <div class="col-md-21">
                    <asp:TextBox ID="txtUploadPublicDoc" runat="server" Text="0" Style="margin: 9px;
                        height: 31px; width: 70px;" meta:resourcekey="txtUploadPublicDocResource1"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" CssClass="commonerrormsg"
                        Style="color: red;" ControlToValidate="txtUploadPublicDoc" Display="Dynamic"
                        ValidationGroup="chk" ErrorMessage="Please Enter Upload Public Doc." 
                        meta:resourcekey="RequiredFieldValidator3Resource1"></asp:RequiredFieldValidator>
                </div>
            </li>
        </div>
            <div class="col-md-12  PDP">
            <%--  <ul class="your-message" style="margin-top: 30px;">--%>
            <li style="margin-bottom: 10px; background: #fff; padding: 20px 0px;">
                <div class="setting_lable col-md-10">
                    <%-- <span class="col-md-3"><%= CommonMessages.Competencelevelschange%></span>--%>
                    <span class="col-md-3" style="margin-top: -4px;"> <asp:Literal ID="Literal21" runat="server" meta:resourcekey="Level1_2" enableviewstate="false"/> <%--<%= CommonMessages.ReadSuggestDoc%> --%></span>
                    <div class="col-md-8">
                     <asp:Literal ID="Literal22" runat="server" meta:resourcekey="Level1_2_description" enableviewstate="false"/>  <%-- <%= CommonMessages.NeedHelp%>--%> </div>
                </div>
                <div class="col-md-21">
                    <asp:TextBox ID="Level1_2" runat="server" Text="0" Style="margin: 9px; height: 31px;
                        width: 70px;" meta:resourcekey="txtRequestSessionResource1"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" CssClass="commonerrormsg"
                        ControlToValidate="Level1_2" Display="Dynamic" ValidationGroup="chk"
                        Style="color: red;" ErrorMessage="Please Enter Point." 
                        meta:resourcekey="RequiredFieldValidator1Resource1"></asp:RequiredFieldValidator>
                </div>
            </li>
            <li style="margin-bottom: 10px; background: #fff; padding: 20px 0px;">
                <div class="setting_lable col-md-10">
                    <span class="col-md-3" style="margin-top: -4px;">  <asp:Literal ID="Literal23" runat="server" meta:resourcekey="Level2_3" enableviewstate="false"/> <%-- <%= CommonMessages.SuggestSession%>--%> </span>
                    <div class="col-md-8">
                    <asp:Literal ID="Literal24" runat="server" meta:resourcekey="Level2_3_description" enableviewstate="false"/>   <%-- <%= CommonMessages.GiveHelp%>--%>  </div>
                </div>
                <div class="col-md-21">
                    <asp:TextBox ID="Level2_3" runat="server" Text="0" Style="margin: 9px; height: 31px;
                        width: 70px;" meta:resourcekey="txtSuggestSessionResource1"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" CssClass="commonerrormsg"
                        ControlToValidate="Level2_3" Display="Dynamic" ValidationGroup="chk"
                        Style="color: red;" ErrorMessage="Please Enter Point." 
                        meta:resourcekey="RequiredFieldValidator2Resource1"></asp:RequiredFieldValidator>
                </div>
            </li>
            <li style="margin-bottom: 10px; background: #fff; padding: 20px 0px;">
                <div class="setting_lable col-md-10">
                    <%-- <span class="col-md-3"><%= CommonMessages.Knowledgesharingrequests%></span>--%>
                    <span class="col-md-3" style="margin-top: -4px;">  <asp:Literal ID="Literal25" runat="server" meta:resourcekey="Level3_4" enableviewstate="false"/> <%-- <%= CommonMessages.SuggestDoc%>--%></span>
                    <div class="col-md-8">
                 <asp:Literal ID="Literal26" runat="server" meta:resourcekey="Level3_4_description" enableviewstate="false"/>   <%--   <%= CommonMessages.SuggestDoctoother%> --%> </div>
                </div>
                <div class="col-md-21">
                    <asp:TextBox ID="Level3_4" runat="server" Text="0" Style="margin: 9px; height: 31px;
                        width: 70px;" meta:resourcekey="txtSuggestdocResource1"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" CssClass="commonerrormsg"
                        Style="color: red;" ControlToValidate="Level3_4" Display="Dynamic" ValidationGroup="chk"
                        ErrorMessage="Please Enter Point." 
                        meta:resourcekey="RequiredFieldValidator4Resource1"></asp:RequiredFieldValidator>
                </div>
            </li>
            <li style="margin-bottom: 10px; background: #fff; padding: 20px 0px;">
                <div class="setting_lable col-md-10">
                    <%-- <span class="col-md-3"><%= CommonMessages.Knowledgesharingrequests%></span>--%>
                    <span class="col-md-3" style="margin-top: -4px;">  <asp:Literal ID="Literal27" runat="server" meta:resourcekey="Level4_5" enableviewstate="false"/> <%--<%= CommonMessages.PublicDoc%>--%></span>
                    <div class="col-md-8">
                     <asp:Literal ID="Literal28" runat="server" meta:resourcekey="Level4_5_description" enableviewstate="false"/>   <%--   <%= CommonMessages.SuggestDoctoother%> --%> </div>
                </div>
                <div class="col-md-21">
                    <asp:TextBox ID="Level4_5" runat="server" Text="0" Style="margin: 9px;
                        height: 31px; width: 70px;" meta:resourcekey="txtUploadPublicDocResource1"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" CssClass="commonerrormsg"
                        Style="color: red;" ControlToValidate="Level4_5" Display="Dynamic"
                        ValidationGroup="chk" ErrorMessage="Please Enter Point." 
                        meta:resourcekey="RequiredFieldValidator3Resource1"></asp:RequiredFieldValidator>
                </div>
            </li>
        </div>
    </ul>
    <div class="modal-footer" style="border: 0px;">
        <asp:Button runat="server" ID="Button1" class="btn btn-primary yellow" Text="Save Changes"
            OnClick="btn_save_change_Click" ValidationGroup="chk" 
            meta:resourcekey="Button1Resource1"></asp:Button>
        <asp:Button runat="server" ID="Button2" class="btn btn-default black" Text="Cancel"
            OnClick="btnCancel_Click" meta:resourcekey="Button2Resource1"></asp:Button>
    </div>
    <!-- TIME LINE -->
    <!-- Recent Post -->
    <div class="col-md-3">
    </div>
    <!-- Twitter Widget -->
    <!-- Weather Widget -->
    <script type="text/jscript">
        function showHideDiv(divName) {
            if (divName == "PLP") {
                $('.PLP').show();
                $('.ALP').hide();
                $('.PDP').hide();
                document.getElementById("myspan").innerHTML = "PLP (Passive Learning Points)";
            } else if (divName == "ALP") {
                $('.PLP').hide();
                $('.ALP').show();
                $('.PDP').hide();
                document.getElementById("myspan").innerHTML = "ALP (Active Learning Points)";
            }
            else if (divName == "PDP") {
                $('.PLP').hide();
                $('.ALP').hide();
                $('.PDP').show();
                document.getElementById("myspan").innerHTML = "ALP (PDP Points)";
            }
            else {

                $('.PLP').show();
                $('.ALP').show();
                $('.PDP').show();
                document.getElementById("myspan").innerHTML = "All (PLP + ALP + PDP)";
            }
        }
    </script>
    
      <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            
            setTimeout(function () {
                //ExampanCollapsMenu();
                //ExampanCollapssubMenu();
                SetExpandCollapse();
                // tabMenuClick();
                //  $(".OrgJobTypeList").addClass("active_page");
            }, 500);
        });
    </script>

</asp:Content>
