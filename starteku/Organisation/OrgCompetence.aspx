﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="OrgCompetence.aspx.cs" Inherits="Organisation_OrgCompetence" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .treeNode {
        }

            .treeNode input {
                width: auto;
                margin: 5px;
                float: left !important;
            }
    </style>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script type="text/javascript">
       
    </script>
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg"
        meta:resourcekey="lblMsgResource1"></asp:Label>
    <div class="col-md-6">
        <div class="heading-sec">
            <h1 style="margin-left: 15px;">
                <%-- <%= CommonMessages.Competence%>--%>
                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Competence" EnableViewState="false" />
                <i><span runat="server" id="Competence"></span></i>
            </h1>
        </div>
    </div>
    <br />
    <br />


    <%-- <div class="col-md-6 range ">
    </div>--%>
    <div class="col-md-12">
        <div class="col-md-12">
            <div class="chat-widget widget-body" style="background: #fff;">
                <div class="chat-widget-head yellow yellow-radius" style="height: 55px">
                    <h4 style="margin: -6px 0px 0px;">
                        <asp:Literal ID="Literal2" runat="server" meta:resourcekey="lblCompetence" EnableViewState="false" />
                        <%--<%= CommonMessages.Competence%> --%>Information</h4>
                </div>
                <div class="indicatesRequireFiled">
                    <i>*
                        <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Indicatesrequiredfield" EnableViewState="false" />
                        <%--<%= CommonMessages.Indicatesrequiredfield%>--%></i>
                    <asp:Label runat="server" ID="lblDataDisplayTitle" Text="Competence"
                        meta:resourcekey="lblDataDisplayTitleResource1"></asp:Label>
                </div>

                <div id="first" runat="server">
                    <div class="col-md-6" style="width: 100%;">
                        <div class="inline-form">
                            <label class="c-label" style="width: 100%;">
                                <asp:Literal ID="Literal4" runat="server" meta:resourcekey="category" EnableViewState="false" />
                                <%-- <%= CommonMessages.CATEGORY%>--%> :</label>
                            <asp:DropDownList ID="ddlcategory" runat="server" Style="width: 100%; height: 32px; border: 1px solid #d4d4d4; display: inline;"
                                meta:resourcekey="ddlcategoryResource1" class="form-control">
                            </asp:DropDownList>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="ddlcategory"
                                ErrorMessage="Please select Category." InitialValue="0" CssClass="commonerrormsg"
                                Display="Dynamic" ValidationGroup="chk"
                                meta:resourcekey="RequiredFieldValidator14Resource1"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="col-md-6" style="width: 100%;">
                        <div class="inline-form">
                            <label class="c-label" style="width: 100%;">
                                <asp:Literal ID="Literal5" runat="server" meta:resourcekey="lblCompetenceres" EnableViewState="false" />
                                <%-- <%= CommonMessages.Competence%>--%>:</label>
                            <asp:TextBox runat="server" data-validation="length" data-validation-length="min20" ID="txtCompetence" MaxLength="50"
                                CssClass="form-control" meta:resourcekey="txtCompetenceResource1" required="required" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtCompetence"
                                ErrorMessage="Please enter competence." CssClass="commonerrormsg" Display="Dynamic"
                                ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator5Resource1"></asp:RequiredFieldValidator>
                        </div>
                    </div>

                    <div class="col-md-6" style="width: 100%;">
                        <div class="inline-form">
                            <label class="c-label" style="width: 100%;">
                                <asp:Literal ID="Literal6" runat="server" meta:resourcekey="Competenceindanish" EnableViewState="false" />
                                <%-- <%= CommonMessages.Competence%>--%>:</label>
                            <asp:TextBox runat="server" ID="txtCompetenceDN" MaxLength="50" required="required"
                                CssClass="form-control" meta:resourcekey="txtCompetenceResource1" />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtCompetenceDN"
                                ErrorMessage="Please enter competence in danish." CssClass="commonerrormsg" Display="Dynamic"
                                ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator6Resource1"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                </div>
                <div id="second" runat="server">
                    <div id="div_contener">
                        <asp:Repeater ID="Repeater1" runat="server">
                            <ItemTemplate>
                                <div class="col-md-6" style="width: 100%;">
                                    <div class="inline-form">
                                        <label class="c-label">
                                            <asp:Literal ID="Literal4" runat="server" meta:resourcekey="lavel" EnableViewState="false" /><%-- <%= CommonMessages.CATEGORY%>--%> :<label><%# Container.ItemIndex +1 %></label></label>
                                        <table style="width: 100%;">
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="TextBox1" Text='<%# Eval("comchildlevel") %>' runat="server"  required="required" CssClass="form-control ckeditor "
                                                        TextMode="MultiLine" meta:resourcekey="TextBox1Resource1"></asp:TextBox>
                                                </td>
                                            </tr>

                                        </table>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBox1"
                                            ErrorMessage="Please enter answers." CssClass="commonerrormsg" Display="Dynamic"
                                            ValidationGroup="chk" Height="50px"
                                            meta:resourcekey="RequiredFieldValidator2Resource1"></asp:RequiredFieldValidator>

                                    </div>
                                    <div class="inline-form">
                                        <label class="c-label">
                                            <asp:Literal ID="Literal7" runat="server" meta:resourcekey="lavelindanish" EnableViewState="false" />:<%# Container.ItemIndex +1 %></label>
                                        <table style="width: 100%;">
                                            <tr>
                                                <td>
                                                    <asp:TextBox ID="TextBoxDN" Text='<%# Eval("comchildlevelDN") %>' required="required" runat="server" CssClass="form-control ckeditor"
                                                        TextMode="MultiLine" meta:resourcekey="TextBoxDNResource1"></asp:TextBox>
                                                </td>

                                            </tr>
                                        </table>

                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="TextBoxDN"
                                            ErrorMessage="Please enter level in danish." CssClass="commonerrormsg" Display="Dynamic"
                                            ValidationGroup="chk" Height="50px"
                                            meta:resourcekey="RequiredFieldValidator1Resource1"></asp:RequiredFieldValidator>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <%-- <div class="col-xs-12 profile_bottom">--%>
                <%--   <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="btn black pull-right"
                    ValidationGroup="chk" CausesValidation="False" OnClick="btnCancel_click" 
                    meta:resourcekey="btnCancelResource1" />
                <asp:Button runat="server" ID="btnsubmit" Text="Save" CssClass="btn blue pull-right"
                    ValidationGroup="chk" OnClick="btnsubmit_click" 
                    meta:resourcekey="btnsubmitResource1" />--%>

                <%-- </div>--%>



                <div class="modal-footer" style="border: 0px;">
                    <asp:Button runat="server" ID="btnsubmit" Text="Save" CssClass="btn btn-primary yellow"
                        ValidationGroup="chk" OnClick="btnsubmit_click"
                        meta:resourcekey="btnsubmitResource1" />
                    <asp:Button runat="server" ID="btnCancel" Text="Cancel" OnClick="btnCancel_click" CssClass="btn btn-primary black"
                        meta:resourcekey="btnCancelResource1" />


                    <%-- <button data-dismiss="modal" class="btn btn-default black" type="button">Cancel  </button>--%>
                </div>

            </div>
        </div>
    </div>

    <script src="//cdn.ckeditor.com/4.7.0/full/ckeditor.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
    <script>

        setInterval(function () {

            updateEleCkEditor();
        }, 500);
        $(document).ready(function () {
            //alert();
            setInterval(function () {

                updateEleCkEditor();
            }, 500);
        });
        function updateEleCkEditor() {

            CKEDITOR.instances.ContentPlaceHolder1_Repeater1_TextBox1_0.updateElement();
            CKEDITOR.instances.ContentPlaceHolder1_Repeater1_TextBox1_1.updateElement();
            CKEDITOR.instances.ContentPlaceHolder1_Repeater1_TextBox1_2.updateElement();
            CKEDITOR.instances.ContentPlaceHolder1_Repeater1_TextBox1_3.updateElement();
            CKEDITOR.instances.ContentPlaceHolder1_Repeater1_TextBox1_4.updateElement();
            CKEDITOR.instances.ContentPlaceHolder1_Repeater1_TextBoxDN_0.updateElement();
            CKEDITOR.instances.ContentPlaceHolder1_Repeater1_TextBoxDN_1.updateElement();
            CKEDITOR.instances.ContentPlaceHolder1_Repeater1_TextBoxDN_2.updateElement();
            CKEDITOR.instances.ContentPlaceHolder1_Repeater1_TextBoxDN_3.updateElement();
            CKEDITOR.instances.ContentPlaceHolder1_Repeater1_TextBoxDN_4.updateElement();

            console.log("Test");
            $("#ContentPlaceHolder1_Repeater1_TextBox1_0").html(CKEDITOR.instances.ContentPlaceHolder1_Repeater1_TextBox1_0.getData());
            $("#ContentPlaceHolder1_Repeater1_TextBox1_1").html(CKEDITOR.instances.ContentPlaceHolder1_Repeater1_TextBox1_1.getData());
            $("#ContentPlaceHolder1_Repeater1_TextBox1_2").html(CKEDITOR.instances.ContentPlaceHolder1_Repeater1_TextBox1_2.getData());
            $("#ContentPlaceHolder1_Repeater1_TextBox1_3").html(CKEDITOR.instances.ContentPlaceHolder1_Repeater1_TextBox1_3.getData());
            $("#ContentPlaceHolder1_Repeater1_TextBox1_4").html(CKEDITOR.instances.ContentPlaceHolder1_Repeater1_TextBox1_4.getData());


            $("#ContentPlaceHolder1_Repeater1_TextBoxDN_4").html(CKEDITOR.instances.ContentPlaceHolder1_Repeater1_TextBoxDN_4.getData());
            $("#ContentPlaceHolder1_Repeater1_TextBoxDN_3").html(CKEDITOR.instances.ContentPlaceHolder1_Repeater1_TextBoxDN_3.getData());
            $("#ContentPlaceHolder1_Repeater1_TextBoxDN_2").html(CKEDITOR.instances.ContentPlaceHolder1_Repeater1_TextBoxDN_2.getData());
            $("#ContentPlaceHolder1_Repeater1_TextBoxDN_1").html(CKEDITOR.instances.ContentPlaceHolder1_Repeater1_TextBoxDN_1.getData());
            $("#ContentPlaceHolder1_Repeater1_TextBoxDN_0").html(CKEDITOR.instances.ContentPlaceHolder1_Repeater1_TextBoxDN_0.getData());




            return true;
        }
        $('.btn-primary').click(function () {

            return updateEleCkEditor();

            //CKEDITOR.instances.editor2.updateElement();
        });
    </script>

</asp:Content>

