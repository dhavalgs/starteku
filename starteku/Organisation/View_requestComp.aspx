﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="View_requestComp.aspx.cs" Inherits="Organisation_View_requestComp" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
   
    <style type="text/css">
        .table > thead > tr > th {
            vertical-align: middle;
        }
    </style>
    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 11pt;
        }
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=40);
            opacity: 0.4;
        }
        .modalPopup1
        {
            background-color: #FFFFFF;
            width: 300px;
            border: 3px solid #0DA9D0;
        }
        .modalPopup
        {
            background: none repeat scroll 0 0 #ffffff;
            display: table;
            float: none;
            margin: 96px auto;
            width: 600px !important;
            
            top: 10px !important;
        }
        .modalPopup .header
        {
            background-color: #2FBDF1;
            height: 30px;
            color: White;
            line-height: 30px;
            text-align: center;
            font-weight: bold;
        }
        .modalPopup .body
        {
            min-height: 50px;
            line-height: 30px;
            text-align: center;
            padding: 5px;
        }
        .modalPopup .footer
        {
            padding: 3px;
        }
        .modalPopup .button
        {
            height: 23px;
            color: White;
            line-height: 23px;
            text-align: center;
            font-weight: bold;
            cursor: pointer;
            background-color: #9F9F9F;
            border: 1px solid #5C5C5C;
        }
        .modalPopup td
        {
            text-align: left;
        }
        
        .button
        {
            background-color: transparent;
        }
        .yellow
        {
            border-radius: 0px;
        }

        .reply {
            clear:both;
        }

        #scrollbox6 {
            overflow-x:hidden;
            overflow-y:auto;
            background: #f4f4f4;
            height:350px !important;
        }
        
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg"
        meta:resourcekey="lblMsgResource1"></asp:Label>
    <div class="col-md-6">
        <div class="heading-sec">
            <h1 style="margin-left: 15px;">
                <%-- <%= CommonMessages.Viewrequest%>--%>
                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Viewrequest" EnableViewState="false" />
                <i><span runat="server" id="Employee"></span>
                </i>
            </h1>
        </div>
    </div>
    <div class="chat-widget widget-body" style="background: #fff;">
    </div>
    <div class="col-md-12" style="margin-top: 20px; width: 100%;">
        <%--<div id="graph-wrapper">--%>
        <div class="chat-widget-head yellow" style="width: 97.8%; margin-left: 15px; margin-bottom: -16px;">
            <h4 style="margin: -6px 0px 0px;">
                <span runat="server" id="spname"></span>'s  <%--<%= CommonMessages.Competencelevel%>--%>
                <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Competencelevel" EnableViewState="false" />
            </h4>
        </div>
        <div class="col-md-12">
            <div class="chart-tab">
                <div id="tabs-container">
                    <%-- <asp:Panel ID="pnlShowCountry" runat="server" Width="100%">
                        <cc1:Accordion ID="Accordion1" runat="server" TransitionDuration="100" SelectedIndex="-1"
                            FramesPerSecond="200" FadeTransitions="true" RequireOpenedPane="false" OnItemDataBound="Accordion1_ItemDataBound"
                            ContentCssClass="accordion-content" HeaderCssClass="accordion-header" HeaderSelectedCssClass="accordion-selected"
                            Width="100%">
                            <HeaderTemplate>
                                <%#DataBinder.Eval(Container.DataItem, "catName")%>
                            </HeaderTemplate>
                            <ContentTemplate>
                                <asp:HiddenField ID="txt_categoryID" runat="server" Value='<%#DataBinder.Eval(Container.DataItem,"catId") %>' />--%>
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>




                            <asp:GridView ID="gvGrid" runat="server" AutoGenerateColumns="False" CellPadding="0"
                                Width="100%" GridLines="None" DataKeyNames="skillUserId" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                                EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'
                                BackColor="White" OnRowDataBound="gvGrid_RowDataBound"
                                meta:resourcekey="gvGridResource1">
                                <HeaderStyle CssClass="aa" />
                                <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                    <asp:TemplateField HeaderText="ID" meta:resourcekey="TemplateFieldResource1">
                                        <ItemTemplate>
                                            <%# Container.DataItemIndex + 1 %>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="10px" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="2%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="COMPETENCE"
                                        meta:resourcekey="TemplateFieldResource2">
                                        <ItemTemplate>
                                             <%if(Convert.ToString(Session["Culture"]) == "Danish"){%>
                                            <asp:Label ID="lblUNamer" runat="server" Text='<%# Eval("comCompetenceDN") %>'
                                                meta:resourcekey="lblUNamerResource1"></asp:Label>
                                            <%} %>
                                            <%else{ %>
                                             <asp:Label ID="Label1" runat="server" Text='<%# Eval("comCompetence") %>'
                                                meta:resourcekey="lblUNamerResource1"></asp:Label>
                                            <%} %>
                                            <asp:HiddenField ID="skillComId" runat="server" Value="<%# bind('skillComId') %>" />
                                            <asp:HiddenField ID="skillId" runat="server" Value="<%# bind('skillId') %>" />
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="15%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Local" meta:resourcekey="TemplateFieldResource3">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtLocal" runat="server" Text="<%# bind('skillLocal') %>" onkeyup="checkNumber(this)"
                                                Width="50px" meta:resourcekey="txtLocalResource1"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Numbers"
                                                TargetControlID="txtLocal" Enabled="True">
                                            </cc1:FilteredTextBoxExtender>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Achieve"
                                        meta:resourcekey="TemplateFieldResource4">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtAchieve" runat="server" AutoPostBack="True" OnTextChanged="txtAchieve_TextChanged"
                                                MaxLength="1" Width="50px" meta:resourcekey="txtAchieveResource1"></asp:TextBox><br />
                                            <asp:RequiredFieldValidator ID="rfvEmail" runat="server" CssClass="commonerrormsg"
                                                ControlToValidate="txtAchieve" Display="Dynamic" ValidationGroup="chk"
                                                ErrorMessage="Please enter Achieve." meta:resourcekey="rfvEmailResource1"></asp:RequiredFieldValidator>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Numbers"
                                                TargetControlID="txtAchieve" Enabled="True">
                                            </cc1:FilteredTextBoxExtender>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Target"
                                        meta:resourcekey="TemplateFieldResource5">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtTarget" runat="server" AutoPostBack="True" OnTextChanged="txtTarget_TextChanged"
                                                MaxLength="1" Width="50px" meta:resourcekey="txtTargetResource1"></asp:TextBox><br />
                                            <asp:RequiredFieldValidator ID="txtTarget12" runat="server" CssClass="commonerrormsg"
                                                ControlToValidate="txtTarget" Display="Dynamic" ValidationGroup="chk"
                                                ErrorMessage="Please enter Target." meta:resourcekey="txtTarget12Resource1"></asp:RequiredFieldValidator>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers"
                                                TargetControlID="txtTarget" Enabled="True">
                                            </cc1:FilteredTextBoxExtender>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Long Term Target"
                                        meta:resourcekey="LongTermRes">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtLongTerm" runat="server" AutoPostBack="True" Text='<%#Eval("skillLongTermTarget" )%>'
                                                MaxLength="1" Width="50px" ></asp:TextBox><br />
                                            <asp:RequiredFieldValidator ID="txtTarget122" runat="server" CssClass="commonerrormsg"
                                                ControlToValidate="txtLongTerm" Display="Dynamic" ValidationGroup="chk"
                                                ErrorMessage="Please enter Target." meta:resourcekey="txtTarget122Resource1"></asp:RequiredFieldValidator>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender33" runat="server" FilterType="Numbers"
                                                TargetControlID="txtLongTerm" Enabled="True">
                                            </cc1:FilteredTextBoxExtender>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                    </asp:TemplateField>

                                     <asp:TemplateField HeaderText="Max Level"
                                        meta:resourcekey="MaxLevelRes">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtMaxLevel" runat="server" AutoPostBack="True"  Text='<%#Eval("skillMaxLevelTarget" )%>'
                                                MaxLength="1" Width="50px" ></asp:TextBox><br />
                                            <asp:RequiredFieldValidator ID="txtTarget1222" runat="server" CssClass="commonerrormsg"
                                                ControlToValidate="txtMaxLevel" Display="Dynamic" ValidationGroup="chk"
                                                ErrorMessage="Please enter Target." meta:resourcekey="txtTarget1222Resource1"></asp:RequiredFieldValidator>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender333" runat="server" FilterType="Numbers"
                                                TargetControlID="txtMaxLevel" Enabled="True">
                                            </cc1:FilteredTextBoxExtender>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                    </asp:TemplateField>

                                    <asp:TemplateField  meta:resourcekey="Comment">
                                        <ItemTemplate>
                                            <%-- <asp:TextBox ID="txtComment" runat="server" AutoPostBack="True"  Text='<%# bind("skillComment") %>'
                                                MaxLength="1" Width="200px" TextMode="MultiLine"></asp:TextBox><br />--%>

                                            <asp:TextBox ID="txtComment" runat="server" AutoPostBack="false" Text='<%# bind("skillComment") %>'
                                                MaxLength="1" Width="200px" TextMode="MultiLine" Style="float: left;" Enabled="false"></asp:TextBox><br />

                                            <a href="#" id="btnChat<%# Eval("comId") %>"  onclick="return GetcommentById('<%# Eval("comId") %>', this.id)" data-index="txtComment" class="black" style="float: left; margin-left: 10px; margin-top: 8px; padding-left: 10px; padding-top: 10px; height: 35px; width: 35px;"><i class="fa fa-comments-o"></i></a>

                                          <%--  <input type="hidden" id="hdncom" value='<%# Eval("comId") %>' />--%>


                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Status" Visible="False"
                                        meta:resourcekey="TemplateFieldResource6">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlstastus" runat="server" Style="width: 100px; height: 32px; display: inline;"
                                                meta:resourcekey="ddlstastusResource1">
                                                <asp:ListItem Value="1" meta:resourcekey="ListItemResource1">Accept</asp:ListItem>
                                                <asp:ListItem Value="2" meta:resourcekey="ListItemResource2">Cancel</asp:ListItem>
                                                <asp:ListItem Value="3" meta:resourcekey="ListItemResource3">pending</asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                            Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>



                        </ContentTemplate>
                        <%--<Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="txtTarget" EventName="TextChanged" />
                                    </Triggers>--%>
                    </asp:UpdatePanel>
                    <input type="hidden" id="hdncom" value=''/>
                    <asp:LinkButton ID="lnkFake" runat="server" meta:resourcekey="lnkFakeResource1" />
                    <cc1:ModalPopupExtender ID="mpe" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkFake"
                        CancelControlID="btnClose" BackgroundCssClass="modalBackground" DynamicServicePath=""
                        Enabled="True">
                    </cc1:ModalPopupExtender>
                    <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup modal-dialog" meta:resourcekey="pnlPopupResource1" Style="top: 0px !important;">
                        <div class="modal-header blue yellow-radius" style="color: #ffffff;">
                             <asp:Label runat="server" ID="Label2" CssClass="modal-title" style="font-size: 135%;"  meta:resourcekey="Comments"></asp:Label>
                            <%--<h4 class="modal-title">Comments</h4>--%>
                            <div style="float: right; margin-top: -10px;">
                                <asp:Button ID="btnClose" runat="server" Text="×" CssClass="close"  />
                            </div>
                        </div>
                        <div class="modal-body">
                            <ul id="scrollbox6">
                                <li class="reply">
                                    <asp:Literal ID="Literal3"  runat="server" meta:resourcekey="NoRecordsFound" EnableViewState="false" /> </li>
                            </ul>

                            <div class="reply-sec">
                                <div class="reply-sec-MESSAGE" style="margin-left: 20px;">

                                    <input type="text" meta:resourcekey="PlsEnterComments"  id="txtcomment" class="txtcomment" style="padding: 10px;" />



                                    <a href="#" title="" class="black" onclick="insertcomment()"><i class="fa fa-comments-o"></i></a>
                                </div>
                            </div>
                        </div>
                    </asp:Panel>
                    <div class="chat-widget widget-body" style="background: #fff;">
                        <%--<div class="col-md-6" style="width: 100%;float:right;">
                            <div class="inline-form" style="width: 100%;">
                                 <label class="c-label" >
                                    Comment:</label>
                            <asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" ID="txtComment"></asp:TextBox>
                               
                               
                            </div>
                        </div>--%>
                        <asp:HiddenField runat="server" ID="hdnStatus12"/>
                        <div class="col-md-6" style="width: 100%; float: right;">
                            <div class="inline-form" style="width: 100%; float: right;">
                                <asp:DropDownList ID="ddlsttus" runat="server"  Style="width: 142px; height: 32px; display: inline; float: right;"
                                    meta:resourcekey="ddlsttusResource1">
                                    <asp:ListItem Value="1" meta:resourcekey="ListItemResource1">Accept</asp:ListItem>
                                    <asp:ListItem Value="2" meta:resourcekey="ListItemResource2">Cancel</asp:ListItem>
                                    <asp:ListItem Value="3" meta:resourcekey="ListItemResource3">Pending</asp:ListItem>
                                </asp:DropDownList>

                               <%-- <label class="c-label" style="width: 7%; float: right;" id="lblStatus">
                                    Status:</label>--%>
                                 <asp:Label runat="server" ID="lblStatus" CssClass="c-label" style="width: 7%; float: right;" meta:resourcekey="lblStatus"></asp:Label>
                            </div>
                        </div>
                    </div>
                    <%-- <div style="float:right;">
                        <span runat="server" id="Span1" style="background: white;width: 100px;">Status:</span>
                        <asp:DropDownList ID="ddlstastus" runat="server" runat="server" Style="width: 100px;
                            height: 32px; display: inline;">
                            <asp:ListItem Value="1" meta:resourcekey="ListItemResource1">Accept</asp:ListItem>
                            <asp:ListItem Value="2" meta:resourcekey="ListItemResource2">Cancel</asp:ListItem>
                            <asp:ListItem Value="3" meta:resourcekey="ListItemResource3">pendding</asp:ListItem>
                        </asp:DropDownList>
                    </div>--%>
                    <%--</ContentTemplate>
                        </cc1:Accordion>
                    </asp:Panel>--%>
                </div>
                <div class="col-xs-12" id="submit" runat="server" style="margin: 10px; padding: 10px; width: 98%;">
                    <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="btn black pull-right"
                        ValidationGroup="chk" CausesValidation="False" OnClick="btnCancel_click"
                        Style="color: white;" meta:resourcekey="btnCancelResource1" />
                    <asp:Button runat="server" ID="btnsubmit" Text="Save" CssClass="btn blue pull-right"
                        ValidationGroup="chk" OnClick="btnsubmit_click" Style="color: white;"
                        meta:resourcekey="btnsubmitResource1" />
                    <asp:Button runat="server" ID="btnback" Text="Back" CssClass="btn black pull-right"
                        CausesValidation="False" OnClick="btnback_click" Visible="False"
                        Style="color: white;" meta:resourcekey="btnbackResource1" />
                </div>
            </div>
        </div>
        <%--</div>--%>
    </div>
    <div style="display:none">
        <asp:Label runat="server" ID="lblNoRecord" CssClass="lblNoRecords" meta:resourcekey="NoRecordsFound"></asp:Label>
        <asp:Label runat="server" ID="lblTypeYourComments" CssClass="lblTypeYourComments" meta:resourcekey="PlsEnterComments"></asp:Label>
    </div>
    <style type="text/css">
        .yellow {
            border-radius: 0px;
        }
    </style>
    <script src="../assets/assets/js/libs/jquery-1.10.2.min.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            setTimeout(function () {
                $(".active").click();
                onLoad();
               
                // tabMenuClick();
                // $(".myCompetence").addClass("active_page");
            }, 500);
            $(".pending_helprequest").addClass("active_page");
        });
        
      
        $(document).keypress(function (e) {
            if (e.keyCode === 13) {
                insertcomment();
                e.preventDefault();
                return false;
            }
        });
        function insertcomment() {

            var message = document.getElementById("txtcomment").value;
            var comid = $("#hdncom").val();

            if (comid != '0') {
                if (message != '') {

                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "View_requestComp.aspx/insertmessage",
                        data: "{'message':'" + message + "','comid':'" + comid + "'}",
                        dataType: "json",
                        success: function (data) {
                            var obj = data.d;
                            if (obj == 'true') {
                                document.getElementById("txtcomment").value = '';
                                // GetmessgeById(touserid);
                                //GetmessgeById_afterinsert(touserid)
                                GetcommentById(comid, aId);
                                $("#" + aId).prev().prev().text(message); // copy text and paste on parent page's comment box
                                $("#scrollbox6").scrollTop(99999999999999999999);
                            }
                        },
                        error: function (result) {
                            // alert("Error");
                        }
                    });
                }
                else {
                    generate("warning", "Oops! You have missed text to send a massage.", "bottomCenter");
                    return false;
                }
            }
            else {
                generate("warning", "Please Select Contact", "bottomCenter");

                document.getElementById("message").value = '';
                return false;
            }
        }

        var aId;
        function GetcommentById(a, id) {
            aId = id;
            $(".txtcomment").attr("placeholder", $(".lblTypeYourComments").attr("placeholder"));
            $("#scrollbox6").scrollTop(99999999999999999999);
            //var comid = document.getElementById("hdncom").value;
            var html = "";
            $("#scrollbox6").html('');
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "View_requestComp.aspx/GetcommentById",
                data: "{'comId':'" + a + "'}",
                dataType: "json",
                success: function (data) {
                    $("#hdncom").val(a);
                    
                        cat = data.d;
                        for (var i = 0; i < data.d.length; i++) {
                            var cls = "reply";

                            html += "<li class='" + cls + "'><div class='chat-thumb'><img src='../Log/upload/Userimage/" + data.d[i].Image + "' alt=''/></div>";
                            html += "<div class='chat-desc'><p>" + data.d[i].compcomment + "</p><i class='chat-time'>" + data.d[i].comcreatedate + "</i>";
                            html += "</div></li>";
                        }
                        //setInterval(function () {
                        //    GetmessgeById_afterinsert(touserid);
                        //}, 10000);

                        if (data.d.length == 0) {
                            html += $(".lblNoRecords").text();// "No records found.";
                        }
                        $("#scrollbox6").append(html);
                        var modalPopup = $find('<%=mpe.ClientID %>');

                        if (modalPopup != null) {

                            modalPopup.show();

                        }
                    },
                    error: function (result) {
                        //alert("Error");

                    }
                });
            }
        function checkNumber(ele) {
            var key = window.event ? event.keyCode : event.which;
            var keychar = String.fromCharCode(key);
            var el = ele.value;
            var input = el + "" + keychar;
            alert(input);
            //your code to valide it.........

        }
        function onLoad() {

            SetExpandCollapse();
            setTimeout(function () {
                //ExampanCollapsMenu();

                // tabMenuClick();
                $(".pending_helprequest").addClass("active_page");
            }, 500);
        }
    </script>
</asp:Content>
