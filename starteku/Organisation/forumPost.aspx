﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master" AutoEventWireup="true" CodeFile="forumPost.aspx.cs" Inherits="Organisation_forumPost" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" src="../Scripts/ckeditor/ckeditor.js"> </script>
    <link href="css/forumStyle.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    
    <div class="col-md-6">
        <div class="heading-sec">
            <h1>
                <asp:Label ID="Label5" runat="server" Text="Label" meta:resourcekey="Forum"></asp:Label> <i><span runat="server" id="Conatct"></span></i>
            </h1>
        </div>
    </div>
    <div style="height: 47px">
        
    </div>
     
    <div class="col-md-12">
        <div class="chat-widget widget-body" style="background: #fff;">
            <div class="chat-widget-head yellow yellow-radius">
                <h4 style="margin: -5px 0px 0px;">
                   <asp:Label ID="Label6" runat="server" Text="Label" meta:resourcekey="Forum"></asp:Label>
                </h4>
            </div>
            <div class="indicatesRequireFiled">
                <i>* <asp:Literal ID="Literal5" runat="server" meta:resourcekey="Indicatesrequiredfield" EnableViewState="false" /></i>
            </div>
            
            
             <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                <ContentTemplate>
                    
                    
                    <div class="sppostContent flLeft" id="fullResponseContainer_2266000002831596" authorId="4303554" style="background: #ffffff">
                        <div class="sppostContentWrapper">
                            <div class="sppostHeader">
                                <h3 id="DocumentTitle" forumId="2266000000623864" categoryId="2266000000623866" style="">
                                
                                <a href="#" onclick="return false;">
                                      <asp:Label runat="server" ID="txtName" 
                                        placeholder="What's your question? Be specific." ReadOnly="True" 
                                        meta:resourcekey="txtNameResource1"/>
                                </a></h3>

                                
                                <div class="clearBoth overAuto dimText">
                                    <asp:Label ID="Label3" runat="server" Text="Label" meta:resourcekey="in"></asp:Label> <a id="singlePostForumLink" href="#"  title=" ">
                                           
                                        <asp:Label runat="server" ID="lblForumCategory" 
                                        meta:resourcekey="lblForumCategoryResource1"></asp:Label>
                                          <asp:Label runat="server" ID="lblForumCategoryId" style="display: none" 
                                        meta:resourcekey="lblForumCategoryIdResource1"></asp:Label>
                                       </a> &nbsp;&#8226;&nbsp;
                                     <asp:Label ID="Label4" runat="server" Text="Label" meta:resourcekey="tags"> </asp:Label>
                                    <a  title="General" href="#">
                                                                    <asp:Label runat="server" ID="lblTags" 
                                        meta:resourcekey="lblTagsResource1"></asp:Label>
                                                               </a>

                                    <span class="ndsep ">&nbsp;&#8226;&nbsp;</span>
                                    
                                    <em class="ndboldem" title="15-Nov-2011 07:06 AM">
                                        
                                    </em>

                                    &nbsp;
                                    
                                </div>
                            </div>
                            <div class="responseHeight" id="responseContentContainer_2266000002831596" style=" max-height: 300px;overflow: auto;width: 111%;">
                                <asp:Label runat="server" ID="txtDescription" TextMode="MultiLine" 
                                     CssClass="ckeditor" Width="100%" 
                                     meta:resourcekey="txtDescriptionResource1" />
                            </div>
                            
                            <a target="_blank" href="/html/blank.html" class="lightbox" id="zoomInLineImage"></a>
                            <div class="sppostTypeActions" id="postTypeActions_2266000002831596">
                                
                                <div class="flRight" id="postActions_2266000002831596">
                                    
                                    <a href="javascript:;" replyContainer="" serverURL="https://forums.zoho.com/topic/is-there-a-sandbox-environment" iamURL ="https://accounts.zoho.com/login?hide_signup=true&hide_title=true&hide_gsignup=true&service_language=en&servicename=ZohoDiscussions&serviceurl=https%3A%2F%2Fforums.zoho.com%2Ftopic%2Fis-there-a-sandbox-environment%3Freply%3Dtrue" canUserReplyThis="false" id="replyToTopicBtn_2266000002831595" topicTypeString = "zohodiscussions.dashBoard.question" postauthor = "erento" forumTopicId="2266000002831595" responseId="-1" forumId="2266000000623864" categoryId="2266000000623866" postTitle="Re: Is there a sandbox environment?" isPrivate="false" isThread="false"></a>
                                    
                                </div>
                            
                            </div>
                        </div>
                    </div>
            <div class="col-md-6" style="width: 100%;display: none">
                <div class="inline-form">
                    <label class="c-label">
                        Tags:<span class="starValidation">*</span></label>
                    <asp:HiddenField ID="HiddenField2" Value="0" runat="server" />
                               
                                    
                    <asp:TextBox runat="server" ID="tag1"  MaxLength="80"
                                 CssClass="form-control" meta:resourcekey="txtNameResource1" />
                                   
                                   
                    <br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="tag1"
                                                ErrorMessage="Please enter tag." 
                        CssClass="commonerrormsg" Display="Dynamic"
                                                ValidationGroup="chk" 
                        meta:resourcekey="RequiredFieldValidator1Resource1" ></asp:RequiredFieldValidator>
                                
                </div></div>
                

              
                <div class="col-md-6" style="width: 100%;">
                   
                    <div class="inline-form replyDiv"  style="display: none">
                        <label class="c-label">
                            Reply:<span class="starValidation">*</span></label>
                        <div style="margin-top: 31px;">        
                    
                            <asp:TextBox runat="server" ID="txtReply" TextMode="MultiLine" Height="300px"
                                         MaxLength="500" CssClass="ckeditor" Width="90%" meta:resourcekey="txtReplyResource1"
                                          />
                            <asp:Label runat="server" ID="Label1" CssClass="commonerrormsg" meta:resourcekey="Label1Resource1" 
                                       ></asp:Label>
                             
                                    
                              
                        </div> </div>
                 
                </div>    
                 

            </div>
            <div class="modal-footer " style="border: 0px;">
                <%--  <asp:Button runat="server" ID="btnReplyShowHide" class="btn btn-primary yellow" 
                            Text="Reply" style="border-radius: 5px;" meta:resourcekey="btnReplyResource">
                </asp:Button>--%>
                <a href="#" id="btnReplyShowHide" class="btn btn-primary yellow"
                    style="border-radius: 5px;" onclick="return fnShowReplyDiv();">
                    <asp:Label ID="Label2" runat="server" meta:resourcekey="btnReplyResource" Text="Reply"></asp:Label>
                </a>
                <%--<input type="button" value="Reply" id="btnReplyShowHide" class="btn btn-primary yellow"/>--%>
                <%--<asp:Button runat="server" ID="btnsubmit" class="btn btn-primary yellow replyDiv" 
                            Text="Post Your Reply" OnClick="btnsubmit_click"  ValidationGroup="chk" 
                            style="border-radius: 5px;display: none" meta:resourcekey="btnsubmitResource1">
                </asp:Button>--%>
                  <asp:Button runat="server" ID="btnsubmit" class="btn btn-primary yellow replyDiv"  
                    Text="Post Your Reply"  Style="border-radius: 5px; display: none;" meta:resourcekey="btnsubmitResource1" 
                      OnClick="btnsubmit_Click"></asp:Button>
                <asp:Button runat="server" ID="btnCancel" class="btn btn-primary black" 
                            Text="Cancel" PostBackUrl="ForumAll.aspx" 
                    meta:resourcekey="btnCancelResource1"></asp:Button>
            </div>
               <script type="text/javascript">
                   $('#btnReplyShowHide').click(function () {
                       $('.replyDiv').toggle();
                       $('#btnReplyShowHide').hide();
                   });
                    </script>
           
                <div class="col-md-6" style="width: 100%;">
                    <div class="inline-form">
                        <label class="c-label">
                       <asp:Label runat="server" ID="lblRecentReply" 
                            meta:resourcekey="lblRecentReplyResource1" Text="s"> </asp:Label> 
                            <span class="starValidation"></span></label>
                        <div style="margin-top: 31px;">  
                            <asp:Repeater ID="rptReply" runat="server">
                                <ItemTemplate>
                                   
                  <li class="ndsingleList ndquestion" id="eachTopic_2266000006396567" >
                    
                    <div class="ndflLeft ndphoto">
                       
                        <img purpose="authorProfile" authorname='<%# Eval("usreName") %>'  
                            alt='<%# Eval("usreName") %>' 
                            src='../Log/upload/Userimage/<%# Eval("userImage") %>'>
                    </div>
                        	
                    <div class="ndflLeft ndtypeIcon"></div>
                    <div class="ndhideOverflow">
                        <div class="ndhideOverflow">
                                <div class="ndflLeft width89">
                                
                                    <h3>
                                        <a class="topicText mediumUnderlineLink" purpose="postTitle" id=""  
                                            href='forumpost.aspx?fmid=<%# Eval("forumMasterId") %>' isresponse="true" 
                                            target="_parent" style="color:blue;">
                                             <div style="max-height: 200px; overflow: auto">
                                    <asp:Label runat="server" ID="txtRecentReply" TextMode="MultiLine" Width="90%" 
                                                     Text='<%# Eval("frDescription") %>' meta:resourcekey="txtRecentReplyResource1"
                                        />
                                        </div>
                                        </a> 
                                	
                                        <a class="orangeLink" onclick='topiconClickAction("2266000000002054","2266000006396567",true, "migration-failure-user-id-not-coming-across");' href="#" isresponse="true" target="_parent"> </a> 
                                    
                                    </h3>
                                </div>
                                
                                    <div class="ndflRight ndstatusCont" votetopic="true" firstresponseid="2266000006428103">
                                            <div class="ndthumbsUpIcon flLeft" title=" 1 user has this question"></div>
                                            <span class="ndvoteCount"> </span>
                                    </div>
                        </div>
                        <p class="flLeft ndPostMeta width78">
                            
                                
                                <span><asp:Label ID="Label7" runat="server" Text="Label" meta:resourcekey="replyby"></asp:Label></span>
                                
                                <a authorprof="show" href="#" ><%#Eval("usreName") %>  </a>
                                
                                <span> <asp:Label ID="Label3" runat="server" Text="Label" meta:resourcekey="in"></asp:Label></span>&nbsp;<a purpose="" hashpurpose="Forum/zoho-mail/1" title="Zoho Mail">
                                                        
                                    <%#Eval("frCategoryName") %>                  
                                   </a>
                                
                                <span class="ndsep">&nbsp;&nbsp;</span>
                                
                                <em class="ndboldem" >
                                
                                <%#Eval("MinAgo") %>      
                                
                                </em>
                                
                                <span class="ndsep">&nbsp;&nbsp;</span>
                                
                                
                                </p>
                        &nbsp;
                        
                    </div>
                     
          </li> 
                                </ItemTemplate>
                            </asp:Repeater>
                        </div> 
                    </div>
                </div>       
            
            </ContentTemplate>
            </asp:UpdatePanel>  

        </div>
    </div>
    
    
    <%--Use for tag jquery plugin....... SAURIN :' 2015 01 27--
    
    Source : http://xoxco.com/projects/code/tagsinput/
    

      --%>
   <%-- <link rel="stylesheet" type="text/css" href="https://xoxco.com/projects/code/tagsinput/jquery.tagsinput.css" />--%>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"> </script>
  <%--  <script type="text/javascript" src="https://xoxco.com/projects/code/tagsinput/jquery.tagsinput.js"> </script>--%>
    <!-- To test using the original jQuery.autocomplete, uncomment the following -->
    <!--
        <script type='text/javascript' src='http://xoxco.com/x/tagsinput/jquery-autocomplete/jquery.autocomplete.min.js'></script>
        <link rel="stylesheet" type="text/css" href="http://xoxco.com/x/tagsinput/jquery-autocomplete/jquery.autocomplete.css" />
        -->
    <script type='text/javascript' src='https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/jquery-ui.min.js'> </script>
    <link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.13/themes/start/jquery-ui.css" />
  
   
      <script type="text/javascript">

        $(document).ready(function() {
            GetTagsForAutoComplete();
            var x;
            setTimeout(function() {
                var x = document.getElementById("<%= tag1.ClientID %>");
                // alert(x);
               // $("<%= tag1.ClientID %>").tagsInput({
                    
                    // my parameters here
               // });
            }, 1500);
            //$('#ContentPlaceHolder1_tag1').tagsInput({
            //    'width': '100%',
            //    autocomplete_url: data,
            //    autocomplete: { selectFirst: true, width: '100px', autoFill: true }
            //    // my parameters here
            //});
        });
        var data;

        function GetTagsForAutoComplete() {

            $.ajax({
                type: "POST",
                async: false,
                url: "ForumNewTopic.aspx/GetForumTagsByforumCompanyId",

                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(msg) {
                    $("#divResult").html(msg.d);
                    data = msg.d;
                    // alert(data);
                    setTimeout(function() {

                    }, 500);

                    //return msg.d;

                },
                error: function(e) {
                    $("#divResult").html("WebSerivce unreachable");
                }
            });
        }
    </script>
    
</asp:Content>