﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using starteku_BusinessLogic.Model;
using startetku.Business.Logic;
using System.Threading;
using System.Web.Services;
using System.Data.Entity.Validation;


public partial class Organisation_ActivityLists : Page
{

    public void SetPublicVaribale()
    {
        CompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        LoggedUserId = Convert.ToInt32(Session["OrgUserId"]);
        OrguserType = Convert.ToInt32(Session["OrguserType"]);

    }

    public static int LoggedUserId { get; set; }
    public static int OrguserType { get; set; }
    public static int ResLangId = 0;
    //public int ResLangId = 0;
    private static int CompanyId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        ResLangId = Convert.ToInt32(Session["resLangID"]);

        if (string.IsNullOrWhiteSpace(Convert.ToString(Session["Aemcompid"])))
        {
            Response.Redirect("login.aspx");
        }
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
      
        if (!IsPostBack)
        {
            hdnLoginUserID.Value = Convert.ToString(Session["OrgUserId"]);
            hdnUserType.Value = Convert.ToString(Session["OrguserType"]);
            //if (string.IsNullOrWhiteSpace(Convert.ToString(Session["Aemcompid"])))
            //{
            //    Response.Redirect("Login.aspx");
            //}

            SetPublicVaribale();
            CompetenceSelectAll();
            try
            {
                txtActName.Attributes["placeholder"] = GetLocalResourceObject("txtActName.Text").ToString();
                txtActDescription.Attributes["placeholder"] = GetLocalResourceObject("txtActDescription.Text").ToString();
                txtActRequirements.Attributes["placeholder"] = GetLocalResourceObject("txtActRequirements.Text").ToString();
                txtActTags.Attributes["placeholder"] = GetLocalResourceObject("txtActTags.Text").ToString();
                txtActStartDate.Attributes["placeholder"] = GetLocalResourceObject("txtActStartDate.Text").ToString();
                txtActEndDate.Attributes["placeholder"] = GetLocalResourceObject("txtActEndDate.Text").ToString();
                txtActCost.Attributes["placeholder"] = GetLocalResourceObject("txtActCost.Text").ToString();
                txtcom.Attributes["placeholder"] = GetLocalResourceObject("TYPEYOURCOMMENTHERE.Text").ToString();
            }
            catch (Exception ex)
            {

                ExceptionLogger.LogException(ex);
            }

            // GetAllJobType();
            //Competence.InnerHtml = CommonMessages.Welcome + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            Competence.InnerHtml = "  " + Convert.ToString(Session["OrgUserName"]) + "!";

            GetPriorityList();
            SetDefaultMessage();
            GetAllJobTypeByCompanyId();
            GetAllDivisionByCompanyId();
            GetAllActivityCategory();

            GetPrivateActivityList();
            GetPublicActivityList();

            GetResource();

            if (!String.IsNullOrEmpty(Request.QueryString["notif"]))
            {
                UpdateNotification();
            }


        }
    }
    #region Notification
    private void UpdateNotification()
    {
        var db = new startetkuEntities1();
        try
        {
            if (!String.IsNullOrEmpty(Request.QueryString["notif"]))
            {
                var notId = Convert.ToInt32(Request.QueryString["notif"]);
                var notObj = db.Notifications.FirstOrDefault(o => o.notId == notId);
                if (notObj != null)
                {
                    notObj.notIsActive = false;
                    db.SaveChanges();
                }
            }

        }
        catch (Exception)
        {

            throw;
        }
    }
    #endregion
    private void GetAssignToUserListsInDropDown(int actId)
    {
        var db = new startetkuEntities1();
        try
        {
            var assignType = 3;
            switch (OrguserType)
            {
                case 2:
                    assignType = 3;
                    break;
                case 1:
                    assignType = 2;
                    break;
                default:
                    divAssignTo.Visible = false;
                    break;
            }
            var userLists = db.GetAllEmployee(false, true, assignType, CompanyId, LoggedUserId).ToList();

            ddAssignToLists.Items.Clear();
            ddAssignToLists.ClearSelection();
            ddAssignToLists.DataSource = userLists;
            ddAssignToLists.DataTextField = "name";
            ddAssignToLists.DataValueField = "userId";
            ddAssignToLists.DataBind();
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex);

        }
    }

    private void GetPriorityList()
    {
        try
        {
            string High = GetLocalResourceObject("High.Text").ToString();
            string Medium = GetLocalResourceObject("Medium.Text").ToString();
            string Low = GetLocalResourceObject("Low.Text").ToString();

            ListItem li = new ListItem(High, "1");
            dd_Priotiy.Items.Add(li);
            dd_Priotiy.Items.Add(new ListItem(Medium, "2"));
            dd_Priotiy.Items.Add(new ListItem(Low, "3"));
            dd_Priotiy.SelectedValue = "1";

        }
        catch (Exception)
        {


        }
    }


    protected void GetAllActivityCategory(int isPublic = -1, int isActCompEnabled = -1)
    {
        try
        {
            var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
            var logginUserType = Convert.ToInt32(Session["OrguserType"]);
            var getAllActCat = ActivityCategoryLogic.GetActivityCategoryList(ResLangId, companyId, 1, isPublic, 0, isActCompEnabled);// db.ActivityCategoryMasters.ToList();
            if (logginUserType != 1)
            {
                //Activities categories taht have Competence enabled = Yes should not be listed in Manager/Employees dropdown listboxes 
                getAllActCat = getAllActCat.Where(o => o.ActCompEnabled != true).ToList();
            }

            ddActCate.Items.Clear();
            ddActCate.ClearSelection();
            // ddActCate.SelectedIndex = -1;
            // ddActCate.SelectedValue = null;

            ListItem li = new ListItem();
            li.Text = GetLocalResourceObject("SelectActivitytype.Text").ToString();
            li.Value = @"0";

            if (getAllActCat.Any())
            {

                ddActCate.DataSource = getAllActCat;
                ddActCate.DataTextField = "CatNameInLang";
                ddActCate.DataValueField = "ActCatId";
                ddActCate.DataBind();






                if (isPublic == -1)
                {
                    ddCat_Filter.Items.Clear();
                    ddCat_Filter.ClearSelection();
                    ddCat_Filter.SelectedIndex = -1;
                    ddCat_Filter.SelectedValue = null;

                    //Commented by Swati on 23/1/18
                    ddCat_Filter.DataSource = getAllActCat;
                    ddCat_Filter.DataTextField = "CatNameInLang";
                    ddCat_Filter.DataValueField = "ActCatId";
                    ddCat_Filter.DataBind();
                    ddCat_Filter.Items.Insert(0, li);

                    /*Private--Bottom table*/

                    dd_Private_Cat.Items.Clear();
                    dd_Private_Cat.ClearSelection();
                    dd_Private_Cat.SelectedIndex = -1;
                    dd_Private_Cat.SelectedValue = null;

                    dd_Private_Cat.DataSource = getAllActCat.Where(o => o.ActCatPublic != false && o.ActCompEnabled == false);
                    dd_Private_Cat.DataTextField = "CatNameInLang";
                    dd_Private_Cat.DataValueField = "ActCatId";
                    dd_Private_Cat.DataBind();
                    dd_Private_Cat.Items.Insert(0, li);



                }
            }
            else
            {
                ddCat_Filter.DataSource = null;
                ddCat_Filter.DataTextField = "CatNameInLang";
                ddCat_Filter.DataValueField = "ActCatId";
                ddCat_Filter.DataBind();

                dd_Private_Cat.DataSource = null;
                dd_Private_Cat.DataTextField = "CatNameInLang";
                dd_Private_Cat.DataValueField = "ActCatId";
                dd_Private_Cat.DataBind();
            }


            ddActCate.Items.Insert(0, li);

        }
        catch (Exception ex)
        {

            ExceptionLogger.LogException(ex);
        }
    }

    protected void GetAllJobTypeByCompanyId()
    {
        JobTypeBM obj = new JobTypeBM();
        obj.jobIsActive = true;
        obj.jobIsDeleted = false;
        obj.jobCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);


        obj.GetAllJobTypeByCompanyId();


        DataSet ds = obj.ds;
        //if (Convert.ToString(Session["Culture"]) == "Danish")
        //{
        //    ds.Tables[0].Columns["jobName"].ColumnName = "abcd1";
        //    ds.Tables[0].Columns["jobNameDN"].ColumnName = "jobName";

        //}
        ViewState["ds"] = ds;

        if (ds == null) return;
        if (ds.Tables[0].Rows.Count <= 0) return;
        chkListJobtype.DataSource = ds.Tables[0];
        chkListJobtype.DataTextField = "jobName";
        chkListJobtype.DataValueField = "jobId";
        chkListJobtype.DataBind();
        chkListJobtype.Items.Insert(0, new ListItem(GetLocalResourceObject("chkListJobtype.Text").ToString(), "0"));

        // chkListDivision.Items.Insert(0, "All");
    }

    protected void GetAllDivisionByCompanyId()
    {
        DivisionBM obj = new DivisionBM();
        obj.depIsActive = true;
        obj.depIsDeleted = false;
        obj.depCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);


        obj.GetAllDivisionsbyCompanyId();


        DataSet ds = obj.ds;
        //if (Convert.ToString(Session["Culture"]) == "Danish")
        //{
        //    ds.Tables[0].Columns["divName"].ColumnName = "abcd";
        //    ds.Tables[0].Columns["divNameDN"].ColumnName = "divName";

        //}
        //ViewState["ds"] = ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                chkListDivision.DataSource = ds.Tables[0];
                chkListDivision.DataTextField = "divName";
                chkListDivision.DataValueField = "divId";
                chkListDivision.DataBind();
                chkListDivision.Items.Insert(0, new ListItem(GetLocalResourceObject("SelectDivision.Text").ToString(), "0"));



            }

        }
    }

    /*Bottom Table*/
    private void GetPrivateActivityList()
    {
        var db = new startetkuEntities1();
        try
        {
            var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
            var userId = Convert.ToInt32(Session["OrgUserId"]);

            var actObj = new GetActivityLists_Result();
            actObj.ActCompanyId = companyId;
            actObj.ActReqUserId = userId;
            if (!String.IsNullOrEmpty(dd_Private_Cat.SelectedValue))
            {
                actObj.ActCatRefId = Convert.ToInt32(dd_Private_Cat.SelectedValue);
            }
            else
            {
                actObj.ActCatRefId = 0;
            }
            if (!String.IsNullOrWhiteSpace(txtStartDate_Private.Text))
            {
                try
                {
                    actObj.ActStartDate = Convert.ToDateTime(txtStartDate_Private.Text);
                }
                catch (Exception)
                {
                }
            }
            if (!String.IsNullOrWhiteSpace(txtEndDate_Private.Text))
            {
                try
                {
                    actObj.ActEndDate = Convert.ToDateTime(txtEndDate_Private.Text);
                }
                catch (Exception)
                {
                }
            }
            var activityList = ActivityMasterLogic.GetActivityList(actObj, ResLangId, -1);

            //Get user type
            var logginUserType = Convert.ToInt32(Session["OrguserType"]);

            if (logginUserType == 3)
            {

                var emplsManagerId = Convert.ToInt32(Session["OrgCreatedBy"]);//Mang Id

                // activityList = activityList.Where(o => o.ActCreatedBy == emplsManagerId || o.ActCreatedBy == userId || o.ActCreatedBy == companyId).ToList();


            }
            if (logginUserType == 2)
            {

                var managerCompId = Convert.ToInt32(Session["OrgCreatedBy"]); //compyId

                // activityList = activityList.Where(o => o.ActCreatedBy == managerCompId || o.ActCreatedBy == userId || o.ActCreatedBy == companyId).ToList();


            }

            if (!String.IsNullOrWhiteSpace(txtSearch_Private.Text))
            {
                txtSearch_Private.Text = txtSearch_Private.Text.ToLower();
                activityList = activityList.Where(o => (o.userFirstName != null && o.userFirstName.ToLower().Contains(txtSearch_Private.Text))

                        || o.ActName.ToLower().Contains(txtSearch_Private.Text)
                        || (o.divName != null && o.divName.ToLower().Contains(txtSearch_Private.Text))
                        || (o.jobName != null && o.jobName.ToLower().Contains(txtSearch_Private.Text))
                        || (o.ActReqStatus != null && o.ActReqStatus.ToLower().Contains(txtSearch_Private.Text))
                        || (o.ActTags != null && o.ActTags.ToLower().Contains(txtSearch_Private.Text))
                        || (o.ActCost != null && o.ActCost.ToString().ToLower().Contains(txtSearch_Private.Text))

                     ).ToList();
                /* activityList = activityList.Where(o => o.userFirstName.Contains(txtSearch.Text) || o.ActName.Contains(txtSearch.Text)
                     || o.divName.Contains(txtSearch.Text)
                      || o.jobName.Contains(txtSearch.Text)
                        || o.ActReqStatus.ToLower().Contains(txtSearch.Text)
                         || o.ActTags.Contains(txtSearch.Text)
                         ).ToList();*/

            }

            if (activityList.Any())
            {
                /*Gridview 2*/
                //activityList = activityList.Where(o => o.ActIsPublic == true && o.ActCompEnabled == false && o.ActReqUserId!=userId && (string.IsNullOrWhiteSpace(o.ActReqStatus) || o.ActReqStatus=="4")).ToList();
                //Due to ticket no AC-151 Comp Enable Catagloue should be displayed in bottom list
                // activityList = activityList.Where(o => o.ActIsPublic == true  && o.ActReqUserId != userId && (string.IsNullOrWhiteSpace(o.ActReqStatus) || o.ActReqStatus == "4")).ToList();

                //as per johan all public activity should be in bottom list
                activityList = activityList.Where(o => o.ActIsPublic == true && o.ActReqUserId != userId).ToList();



                //activityList = activityList.Where(o => o.ActIsPublic == false && o.ActCompEnabled == false && string.IsNullOrWhiteSpace(o.ActReqStatus)).ToList();
                // activityList = activityList.Where(o => o.ActCompEnabled == false && string.IsNullOrWhiteSpace(o.ActReqStatus)).ToList();


                gvGrid_Private.DataSource = activityList;
                gvGrid_Private.DataBind();
                gvGrid_Private.UseAccessibleHeader = true;
                gvGrid_Private.HeaderRow.TableSection = TableRowSection.TableHeader;
                gvGrid_Private.FooterRow.TableSection = TableRowSection.TableFooter;

                if (!String.IsNullOrWhiteSpace(Convert.ToString(Session["OrguserType"])))
                {
                    var type = Convert.ToInt32(Session["OrguserType"]);

                    if (type == 1)
                    {
                        // gvGrid_Private.Columns[10].Visible = false;
                    }
                }

            }
            else
            {
                gvGrid_Private.DataSource = null;
                gvGrid_Private.DataBind();

            }

        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex);
        }
        finally
        {
            CommonAttributes.DisposeDBObject(ref db);
        }
        btnCreate.Visible = true;

    }

    //Top List
    private void GetPublicActivityList()
    {
        var db = new startetkuEntities1();
        try
        {
            var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
            var userId = Convert.ToInt32(Session["OrgUserId"]);
            var logginUserType = Convert.ToInt32(Session["OrguserType"]);

            var actObj = new GetActivityLists_Result();
            actObj.ActCompanyId = companyId;
            if (logginUserType == 3)
            {
                actObj.ActReqUserId = userId;
            }
            actObj.ActCatRefId = Convert.ToInt32(ddCat_Filter.SelectedValue);


            if (!String.IsNullOrWhiteSpace(txtSearchStartDate_Public.Text))
            {
                try
                {
                    actObj.ActStartDate = Convert.ToDateTime(txtSearchStartDate_Public.Text);
                }
                catch (Exception)
                {

                }

            }
            if (!String.IsNullOrWhiteSpace(txtSearchEndDate_Public.Text))
            {
                try
                {
                    actObj.ActEndDate = Convert.ToDateTime(txtSearchEndDate_Public.Text);
                }
                catch (Exception)
                {
                    //
                }

            }

            string struserid = Convert.ToString(userId);
            var activityList = ActivityMasterLogic.GetActivityList(actObj, ResLangId, 0).ToList();
            //Common.WriteLog("logginUserType:" + Convert.ToString(logginUserType));
            //Common.WriteLog("userId:" + Convert.ToString(userId));
            //Common.WriteLog("Total Act :" + Convert.ToString(activityList.Count));
            if (logginUserType == 1)
            {
                activityList = activityList.Where(o => (o.ActCompEnabled == true || o.ActCompanyId == userId || o.ActCreatedBy == userId || o.ActIsPublic == true) && (o.ActReqStatus != null || o.ActCreatedBy == userId || o.ActCompanyId == userId)).ToList();
            }
            else if (logginUserType == 2)
            {
                activityList = activityList.Where(o => (o.ActCompEnabled == true || o.userCreateBy == struserid || o.ActCreatedBy == userId || o.ActIsPublic == true || o.ActReqUserId == userId) && (o.ActReqStatus != null || o.ActCreatedBy == userId || o.userCreateBy == struserid)).ToList();
            }
            else
            {
                activityList = activityList.Where(o => (o.ActCompEnabled == true || o.ActCreatedBy == userId || o.ActIsPublic == true || o.ActReqUserId == userId) && (o.ActReqStatus != null || o.ActCreatedBy == userId)).ToList();
            }
            //Common.WriteLog("Total Act :" + Convert.ToString(activityList.Count));
            //activityList = activityList.Where(o => (o.ActCreatedBy == userId && o.ActIsPublic == true)).ToList();

            //activityList = activityList.Where(o => (o.ActCompEnabled == true && o.ActCreatedBy != userId)).ToList();

            //Get user type
            if (logginUserType == 3)
            {
                var emplsManagerId = Convert.ToInt32(Session["OrgCreatedBy"]);//Mang Id
                //  activityList = activityList.Where(o => o.ActCreatedBy == emplsManagerId || o.ActCreatedBy == userId || o.ActCreatedBy == companyId).ToList();
            }
            if (logginUserType == 2)
            {
                var managerCompId = Convert.ToInt32(Session["OrgCreatedBy"]); //compyId
                // activityList = activityList.Where(o => o.ActCreatedBy == managerCompId || o.ActCreatedBy == userId || o.ActCreatedBy == companyId).ToList();
            }
            if (!String.IsNullOrWhiteSpace(txtSearch_Public.Text))
            {
                txtSearch_Public.Text = txtSearch_Public.Text.ToLower();
                activityList = activityList.Where(o => (o.userFirstName != null && o.userFirstName.ToLower().Contains(txtSearch_Public.Text))

                        || (o.ActName.ToLower().Contains(txtSearch_Public.Text))
                        || (o.divName != null && o.divName.ToLower().Contains(txtSearch_Public.Text))
                        || (o.jobName != null && o.jobName.ToLower().Contains(txtSearch_Public.Text))
                        || (o.ActReqStatus != null && o.ActReqStatus.ToLower().Contains(txtSearch_Public.Text))
                        || (o.ActTags != null && o.ActTags.ToLower().Contains(txtSearch_Public.Text))
                        || (o.requesterFirstName != null && o.requesterFirstName.ToLower().Contains(txtSearch_Public.Text))
                        || (o.ActCost != null && o.ActCost.ToString().ToLower().Contains(txtSearch_Public.Text))

                     ).ToList();
                /* activityList = activityList.Where(o => o.userFirstName.Contains(txtSearch.Text) || o.ActName.Contains(txtSearch.Text)
                     || o.divName.Contains(txtSearch.Text)
                      || o.jobName.Contains(txtSearch.Text)
                        || o.ActReqStatus.ToLower().Contains(txtSearch.Text)
                         || o.ActTags.Contains(txtSearch.Text)
                         ).ToList();*/

            }

            if (activityList.Any())
            {
                activityList = activityList.Where(o => (o.ActIsPublic == false || (!string.IsNullOrWhiteSpace(o.ActReqStatus)) || o.ActCatDeveplan == true)).ToList();
                // activityList = activityList.Where(o => (o.ActIsPublic == false || (!string.IsNullOrWhiteSpace(o.ActReqStatus) && o.ActIsPublic == true)) || o.ActCatDeveplan == true)).ToList();


                var newActivityLists = new List<GetActivityLists_Result>();
                if (logginUserType != 1)
                {

                    foreach (var i in activityList)
                    {
                        //Commented by Swati on 20/01/2018 as User requested activity was not coming on top list
                        //if (i.ActCreatedBy != userId || !i.ActIsPublic)
                        {

                            if (logginUserType == 2)
                            {
                                if (i.requesterUserCreateBy == Convert.ToString(userId) || (i.ActReqUserId == userId && i.requesterUserType == 2) || (i.ActCreatedBy == userId))
                                {
                                    newActivityLists.Add(i);
                                }

                            }
                            else
                            {
                                newActivityLists.Add(i);
                            }
                        }

                    }
                }
                else
                {
                    newActivityLists = activityList;
                }

                gvGrid_Public.DataSource = newActivityLists;
                gvGrid_Public.DataBind();

                //This replaces <td> with <th> and adds the scope attribute
                gvGrid_Public.UseAccessibleHeader = true;

                //This will add the <thead> and <tbody> elements
                gvGrid_Public.HeaderRow.TableSection = TableRowSection.TableHeader;

                //This adds the <tfoot> element. 
                //Remove if you don't have a footer row
                gvGrid_Public.FooterRow.TableSection = TableRowSection.TableFooter;

                if (!String.IsNullOrWhiteSpace(Convert.ToString(Session["OrguserType"])))
                {
                    var type = Convert.ToInt32(Session["OrguserType"]);

                    if (type == 1)
                    {
                        //gvGrid_Public.Columns[10].Visible = false;
                    }
                }
            }
            else
            {
                gvGrid_Public.DataSource = null;
                gvGrid_Public.DataBind();
            }

        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex);
        }
        finally
        {
            CommonAttributes.DisposeDBObject(ref db);
        }
        btnCreate.Visible = true;

    }
    protected void CompetenceSelectAll()
    {
        CompetenceMasterBM obj = new CompetenceMasterBM();
        obj.comIsActive = true;
        obj.comIsDeleted = false;
        obj.comCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllCompetenceMasterAdd();
        DataSet ds = obj.ds;
        //if (Convert.ToString(Session["Culture"]) == "Danish")
        //{
        //    ds.Tables[0].Columns["catName"].ColumnName = "abcd";
        //    ds.Tables[0].Columns["catNameDN"].ColumnName = "catName";

        //}
        ViewState["ds"] = ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddCompetence.DataSource = ds.Tables[0];
                ddCompetence.DataTextField = "comCompetence";
                ddCompetence.DataValueField = "comId";
                ddCompetence.DataBind();
                ddCompetence.Items.Insert(0, new ListItem(GetLocalResourceObject("SelectCompetence.Text").ToString(), "0"));
                ddCompetence.SelectedValue = "0";


            }
            else
            {
                ddCompetence.DataSource = null;
                ddCompetence.DataBind();
            }
        }
        else
        {
            ddCompetence.DataSource = null;
            ddCompetence.DataBind();
        }
    }

    protected void grd_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {

    }

    protected void gvGrid_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Footer)
        {
            foreach (TableCell cell in e.Row.Cells)
            {
                cell.BorderStyle = BorderStyle.None;
            }
        }
       
    }

    public int total=0;
    //Bottom List
    protected void gvGrid_Private_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {


            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                //string sponsorBonus = ((Label)e.Row.FindControl("Label2")).Text;

                string count = ((HiddenField)e.Row.FindControl("hdnCost_Private")).Value;
                string actReqStatus = ((HiddenField)e.Row.FindControl("hdnActReqStatus")).Value;
                string isPublicActivity = ((HiddenField)e.Row.FindControl("hdnActIsPublic")).Value;

                //Label lbl = (Label)e.Row.FindControl("lblActReqStatus");
                //lbl = (Label)e.Row.FindControl("lblActReqStatus");
                // lbl.Text = GetActivityStatusByStatusId(actReqStatus, !Convert.ToBoolean(isPublicActivity));

                var editPanel = (Panel)e.Row.FindControl("editPanel");
                var archivePanle = (Panel)e.Row.FindControl("archivePanle");
                var hdnActUmId = (HiddenField)e.Row.FindControl("hdnActUmId");
                var hdnActReqID = (HiddenField)e.Row.FindControl("hdnActReqIDNo");




                //string hdnDivisionValue = ((HiddenField)e.Row.FindControl("hdnDivisionValue1")).Value;
                //string hdnJobTypeValue = ((HiddenField)e.Row.FindControl("hdnJobTypeValue1")).Value;
                //Label lbljobtype = (Label)e.Row.FindControl("lbljobtype1");
                //Label lbldivision = (Label)e.Row.FindControl("lbldivision1");
                string actstate = ((HiddenField)e.Row.FindControl("hdnstate1")).Value;
                Label lblstate = (Label)e.Row.FindControl("lblstate1");
                lblstate.Text = GetActivityState(actstate);

                //if (hdnDivisionValue == "All")
                //{
                //    lbldivision.Text = GetLocalResourceObject("All.Text").ToString();
                //}
                //else
                //{
                //    lbldivision.Text = hdnDivisionValue;
                //}

                //if (hdnJobTypeValue == "All")
                //{
                //    lbljobtype.Text = GetLocalResourceObject("All.Text").ToString();
                //}
                //else
                //{
                //    lbljobtype.Text = hdnJobTypeValue;
                //}


                if (!String.IsNullOrWhiteSpace(count))
                {
                    if (actReqStatus == "2" || actReqStatus == "3" || actReqStatus == "5" || actReqStatus == "4")
                    {
                       // total = total + Int32.Parse(count);

                    }
                    else
                    {
                        editPanel.Visible = true;
                    }

                }
                var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
                var userId = Convert.ToInt32(Session["OrgUserId"]);
                var type = Convert.ToInt32(Session["OrguserType"]);


                if (hdnActUmId.Value == userId.ToString() || type == 1)
                {
                    editPanel.Visible = true;
                    archivePanle.Visible = true;
                }
                else
                {
                    editPanel.Visible = false;
                    archivePanle.Visible = false;
                }

            }
            //if (e.Row.RowType == DataControlRowType.Footer)
            //{
            //    var type = Convert.ToInt32(Session["OrguserType"]);
            //    Label lblTotalCost = (Label)e.Row.FindControl("lblTotalCost_Private");
            //    lblTotalCost.Text = total.ToString();
            //    if (type != 1)
            //    {
            //        gvGrid_Private.ShowFooter = false;

            //    }
            //}
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "gvGrid_Private_RowDataBound");
        }

    }
    //Top List
       public int  totalPrivate=0;
    protected void gvGrid_Public_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {


            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                //string sponsorBonus = ((Label)e.Row.FindControl("Label2")).Text;

                string count = ((HiddenField)e.Row.FindControl("hdnCost_Public")).Value;
                string actReqStatus = ((HiddenField)e.Row.FindControl("hdnActReqStatus")).Value;
                string isPublicActivity = ((HiddenField)e.Row.FindControl("hdnActIsPublic")).Value;
                string actID = ((HiddenField)e.Row.FindControl("hdnActPublicID")).Value;

                Label lbl = (Label)e.Row.FindControl("lblActReqStatus");
                lbl = (Label)e.Row.FindControl("lblActReqStatus");


                var editPanel = (Panel)e.Row.FindControl("editPanel");
                var viewPanel = (Panel)e.Row.FindControl("viewPanel");
                var archivePanle = (Panel)e.Row.FindControl("archivePanle");
                var hdnActUmId = (HiddenField)e.Row.FindControl("hdnActUmId");


                lbl.Text = GetActivityStatusByStatusId(actReqStatus, !Convert.ToBoolean(isPublicActivity));

                //string hdnDivisionValue = ((HiddenField)e.Row.FindControl("hdnDivisionValue")).Value;
                //string hdnJobTypeValue = ((HiddenField)e.Row.FindControl("hdnJobTypeValue")).Value;
                //string actstate = ((HiddenField)e.Row.FindControl("hdnstate")).Value;
                //Label lblstate = (Label)e.Row.FindControl("lblstate");
                //lblstate.Text = GetActivityState(actstate);

                //Label lbljobtype = (Label)e.Row.FindControl("lbljobtype");
                //Label lbldivision = (Label)e.Row.FindControl("lbldivision");

                //if (hdnDivisionValue == "All")
                //{
                //    lbldivision.Text = GetLocalResourceObject("All.Text").ToString();
                //}
                //else
                //{
                //    lbldivision.Text = hdnDivisionValue;
                //}

                //if (hdnJobTypeValue == "All")
                //{
                //    lbljobtype.Text = GetLocalResourceObject("All.Text").ToString();
                //}
                //else
                //{
                //    lbljobtype.Text = hdnJobTypeValue;
                //}




                if (!String.IsNullOrWhiteSpace(count))
                {
                    if (actReqStatus == "2" || actReqStatus == "3" || actReqStatus == "5" || actReqStatus=="4")
                    {
                        totalPrivate = totalPrivate + Int32.Parse(count);

                    }
                    else
                    {
                        editPanel.Visible = true;
                    }

                }
                var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
                var userId = Convert.ToInt32(Session["OrgUserId"]);
                var type = Convert.ToInt32(Session["OrguserType"]);



                if (hdnActUmId.Value == userId.ToString() || type == 1)
                {
                    editPanel.Visible = true;
                    archivePanle.Visible = true;

                }
                else
                {
                    editPanel.Visible = false;
                    archivePanle.Visible = false;
                }

                //if (Convert.ToBoolean(isPublicActivity) == false)
                //{
                //    viewPanel.Visible = false;
                //}
                if (actReqStatus == "5")
                {
                    editPanel.Visible = false;
                    archivePanle.Visible = true;
                    viewPanel.Visible = true;
                }


            }
            if (e.Row.RowType == DataControlRowType.Footer)
            {
                var type = Convert.ToInt32(Session["OrguserType"]);
                Label lblTotalCost = (Label)e.Row.FindControl("lblTotalCost_Public");
                lblTotalCost.Text = totalPrivate.ToString();
                if (type != 1)
                {
                    gvGrid_Public.ShowFooter = true;
                }
            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "gvGrid_Public_RowDataBound");
        }

    }
    public string GetActivityState(string actstate)
    {
        GetResource();
        string state = "";
        // var state = isenable ? ENABLED : DISABLED;
        switch (actstate)
        {
            case "True":
                {
                    state = AVAILABLE;//getResource.GetResource("REQUESTED.Text");//StringFor.REQUESTED;
                    break;
                }
            case "False":
                {
                    state = DISABLED;// getResource.GetResource("REQUESTED.Text"); //StringFor.APPROVED;
                    break;
                }
        }
        return state;
    }
    protected void gvGrid_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        justificationDiv.Visible = false;
        Expected_Behaviour_Change_Div.Visible = false;
        btnRequest.Visible = false;
        btnUpdate.Visible = false;
        btnComplete.Visible = false;
        EnableFields(true);


        var db = new startetkuEntities1();
        string ActId = e.CommandArgument.ToString();
        var ac = Convert.ToInt32(ActId);
          var userId = Convert.ToInt32(Session["OrgUserId"]);
          var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var checkIsReq = db.ActReqMasters.FirstOrDefault(o => o.ActReqActId == ac);
        var checkIsReqUser = db.ActReqMasters.FirstOrDefault(o => o.ActReqActId == ac && o.ActReqUserId == userId);

       

        if (checkIsReqUser == null)
        {
            db.UpdateRequestComment(ac, 0, userId, 0, companyId);
        }
        if (e.CommandName == "EditRow")
        {
            btnUpdate.Visible = true;
            btnCreate.Visible = false;


            GetActivityData(e, "EditRow", "EditRow");

            if (checkIsReq != null)
            {
                //txtActCost.Enabled = false;
                //txtActStartDate.Enabled = false;
                //txtActEndDate.Enabled = false;
                divAssignTo.Visible = true;
                GetAssignToUserListsInDropDown(0);
                if (checkIsReq != null)
                {
                    try
                    {
                        ddAssignToLists.SelectedValue = Convert.ToString(checkIsReq.ActReqUserId);
                    }
                    catch (Exception ex)
                    {

                        ExceptionLogger.LogException(ex, 0, " ddAssignToLists.SelectedValue->" + Convert.ToString(checkIsReq.ActReqUserId));
                    }

                }
                btnAssign.Enabled = false;
                ddAssignToLists.Enabled = false;
            }

        }
        else if (e.CommandName == "EditRowPrivate")
        {
            btnUpdate.Visible = true;
            btnCreate.Visible = false;


            GetActivityData(e, "EditRow", "EditRowPrivate");
            divAssignTo.Visible = false;
            commentDivServerSide.Visible = false;
            //if (checkIsReq != null)
            //{
            //    txtActCost.Enabled = false;
            //    txtActStartDate.Enabled = false;
            //    txtActEndDate.Enabled = false;
            //    divAssignTo.Visible = true;
            //    GetAssignToUserListsInDropDown(0);
            //    if (checkIsReq != null)
            //    {
            //        try
            //        {
            //            ddAssignToLists.SelectedValue = Convert.ToString(checkIsReq.ActReqUserId);
            //        }
            //        catch (Exception ex)
            //        {

            //            ExceptionLogger.LogException(ex, 0, " ddAssignToLists.SelectedValue->" + Convert.ToString(checkIsReq.ActReqUserId));
            //        }

            //    }
            //    btnAssign.Enabled = false;
            //    ddAssignToLists.Enabled = false;
            //}

        }

        else if (e.CommandName == "ViewRow")
        {
            justificationDiv.Visible = true;
            Expected_Behaviour_Change_Div.Visible = true;
            btnCreate.Visible = false;
            btnRequest.Visible = true;

            GetActivityData(e, "ViewRow", "ViewRow");
            EnableFields();
            //txtExpctBehavChange.Enabled = true;

            //txtJustification.Enabled = false;
            //divAssignTo.Visible = false;
            //btnAssign.Visible = false;
            //txtcom.Enabled = false;
            //txtExpctBehavChange.Enabled = false;

            if (hdnActReqStatusActivity.Value == "3")  //swati
            {
                btnComplete.Visible = true;
                commentDivServerSide.Visible = true;
                btnUpdate.Visible = false;
                txtcom.Enabled = true;
            }
            divAssignTo.Visible = false;
            btnAssign.Visible = false;
            GetAssignToUserListsInDropDown(0);
            if (checkIsReq != null)
            {
                try
                {
                    ddAssignToLists.SelectedValue = Convert.ToString(checkIsReq.ActReqUserId);
                }
                catch (Exception ex)
                {

                    ExceptionLogger.LogException(ex, 0, " ddAssignToLists.SelectedValue->" + Convert.ToString(checkIsReq.ActReqUserId));
                }

            }

        }
        else if (e.CommandName == "ViewRowPrivate")
        {
            justificationDiv.Visible = true;
            Expected_Behaviour_Change_Div.Visible = true;
            btnCreate.Visible = false;
            btnRequest.Visible = true;
            GetActivityData(e, "ViewRow", "ViewRowPrivate");
            EnableFields();
            txtExpctBehavChange.Enabled = true;
            txtJustification.Enabled = true;

            commentDivServerSide.Visible = true;

            divAssignTo.Visible = true;
            btnAssign.Visible = true;
            GetAssignToUserListsInDropDown(0);
            if (checkIsReq != null)
            {
                try
                {
                    ddAssignToLists.SelectedValue = Convert.ToString(checkIsReq.ActReqUserId);
                }
                catch (Exception ex)
                {

                    ExceptionLogger.LogException(ex, 0, " ddAssignToLists.SelectedValue->" + Convert.ToString(checkIsReq.ActReqUserId));
                }

            }
        }
        else if (e.CommandName == "archive")
        {
            errorDiv.Visible = true;

            if (checkIsReq != null)
            {
                lblMsg.Text = @"Oops! You can not archive this activity. This activity is used in child activity,";
                lblMsg.ForeColor = Color.White;
                return;
            }

            var isDup = db.ActivityMasters.FirstOrDefault(o => o.ActId == ac);

            if (isDup != null)
            {
                isDup.ActIsDeleted = true;
                //isDup.ActIsActive = false;
                db.SaveChanges();

                //var getActReq = db.ActReqMasters.Where(o => o.ActReqActId == ac).ToList();
                //foreach (var j in getActReq)
                //{
                //    db.ActReqMasters.Remove(j);
                //    db.SaveChanges();
                //}

                Response.Redirect("ActivityLists.aspx?msg=del");


            }



        }

    }

    private void EnableFields(bool isTrue = false)
    {
        txtActName.Enabled = isTrue;
        txtActName.Enabled = isTrue;
        txtActRequirements.Enabled = isTrue;
        txtActDescription.Enabled = isTrue;
        txtActRequirements.Enabled = isTrue;
        txtActTags.Enabled = isTrue;

        txtActStartDate.Enabled = isTrue;
        txtActEndDate.Enabled = isTrue;
        txtActCost.Enabled = isTrue;

        ddCompetence.Enabled = isTrue;

        chkListDivision.Enabled = isTrue;
        chkListJobtype.Enabled = isTrue;
        ddActCate.Enabled = isTrue;
        txtError.Enabled = isTrue;
        //txtJustification.Enabled = isTrue;
        //  txtExpctBehavChange.Enabled = isTrue;
        dd_Priotiy.Enabled = isTrue;
        rdoEnabled.Enabled = isTrue;
        //errorDiv.Visible = false;
        //commentDivServerSide.Visible = false;
    }

    public void GetActivityData(GridViewCommandEventArgs e, string eventName, string SUBEVENT)
    {
        try
        {
           
            int IsPublic = 0;
            hdnPublicPrivateAct.Value = "0";
            var id = Convert.ToInt32(e.CommandArgument);
            hdnActId.Value = Convert.ToString(e.CommandArgument);

            GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);

            var hdnActReqID = (HiddenField)row.FindControl("hdnActReqIDNo");


           // GridViewRow row = (GridViewRow)(((Control)e.CommandSource).NamingContainer);

            var hdnRequestUserID_Num = (HiddenField)row.FindControl("hdnRequestUserID_Num");
            //ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lbl').click();", true);
            //ScriptManager.RegisterStartupScript(updtPnl, updtPnl.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lbl').click();", true);

            ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lbl').click();GetActivityCommentById('');", true);
            //Get Data
            var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
            var userId = Convert.ToInt32(Session["OrgUserId"]);
            var actObj = new GetActivityLists_Result();
            actObj.ActId = id;
            actObj.ActCompanyId = companyId;
            actObj.ActReqUserId = userId;
            actObj.ActIsDeleted = false;


            if (eventName != SUBEVENT)
            {
                IsPublic = -1;
                hdnPublicPrivateAct.Value = "1";
                try
                {
                    if (!String.IsNullOrEmpty(hdnActReqID.Value))
                    {
                        actObj.ActReqId = Convert.ToInt32(hdnActReqID.Value);
                    }
                }
                catch (Exception ex)
                {
                    ExceptionLogger.LogException(ex, 0, "");
                }
            }
            else
            {
                try
                {
                    if (!String.IsNullOrEmpty(hdnActReqID.Value))
                    {
                        actObj.ActReqId = Convert.ToInt32(hdnActReqID.Value);
                    }
                    if (!String.IsNullOrEmpty(hdnActReqID.Value))
                    {
                        actObj.ActReqUserId = Convert.ToInt32(hdnRequestUserID_Num.Value);
                    }
                }
                catch (Exception ex)
                {
                    ExceptionLogger.LogException(ex, 0, "");
                }
            }

            var getActData = ActivityMasterLogic.GetActivityList(actObj, 1, IsPublic).FirstOrDefault();// db.ActivityMasters.FirstOrDefault(o => o.ActId == id);

           
            //
            if (getActData == null)
            {
                lblMsg.Text = @"Sorry, Record not exists any more";
                lblMsg.ForeColor = Color.White;
                return;
            }

            //if (getActData.ActReqId == null)
            //{
            //    getActData.ActReqId = 0;
            //}

            hdnActReqIdInt.Value = Convert.ToString(getActData.ActReqId);

            hdnActReqStatusActivity.Value = Convert.ToString(getActData.ActReqStatus);

            hdnAcrCreaterId.Value = Convert.ToString(getActData.ActCreatedBy);
            hdnCurrentActivityStatus.Value = Convert.ToString(getActData.ActReqStatus);
            //CHeck Row Event

            if (eventName == "ViewRow")
            {
                lblModelHeader.Text = hdnViewActivity.Value;
                btnCreate.Visible = false;
                btnUpdate.Visible = false;
                //if login user & creater of activity is same then requet button will visible =false
                if (getActData.ActIsPublic == false)
                {
                    btnRequest.Visible = false;
                    commentDivServerSide.Visible = false;//that means activity is private and user cant comment on provate activity
                }

            }
            if (eventName == "EditRow")
            {
                lblModelHeader.Text = hdnUpdateActivity.Value;
                btnCreate.Visible = false;
                btnRequest.Visible = false;

                //Check User is the same user who created this activity, if not then do not allow him to edit

                if (userId != getActData.ActCreatedBy)
                {
                    btnUpdate.Visible = false;
                }

                if (Convert.ToString(Session["OrguserType"]) == "1")
                {
                    btnUpdate.Visible = true;
                }
                justificationDiv.Visible = false;
                Expected_Behaviour_Change_Div.Visible = false;
            }

            if (userId == getActData.ActCreatedBy)
            {
                //if user is creater of activity then comment box should be invisible
                commentDivServerSide.Visible = false;
                justificationDiv.Visible = false;
                txtActStartDate.Enabled = true;
                txtActEndDate.Enabled = true;
                txtActCost.Enabled = true;
            }
            else
            {
                txtActStartDate.Enabled = false;
                txtActEndDate.Enabled = false;
                txtActCost.Enabled = false;
            }

            //$("#txtCmtDiv").fadeOut();

            if (getActData != null)
            {

                if (getActData.ActReqStatus == "5" || getActData.ActReqStatus == "4" || getActData.ActReqStatus == "3" || getActData.ActReqStatus == "2" || Convert.ToInt32(Session["OrguserType"]) == 1)
                {
                    btnRequest.Visible = false;
                    txtJustification.Enabled = false;

                    txtcom.Enabled = false;
                    txtExpctBehavChange.Enabled = false;

                    // Expected_Behaviour_Change_Div.Visible = false;
                }
                if (getActData.ActReqStatus == "4" && eventName == "ViewRow" && (getActData.ActCreatedBy == userId || getActData.ActReqUserId == userId || getActData.ActReqToUserId == userId))
                {
                    txtcom.Enabled = true;
                    btnComplete.Visible = true;
                }
                if (getActData.ActReqStatus == "4" && eventName == "ViewRow" && getActData.ActCreatedBy != userId)
                {
                    btnRequest.Visible = true;
                }
                if ((getActData.ActReqStatus == "4" || getActData.ActReqStatus == "3") && eventName == "ViewRow" && (getActData.ActReqToUserId == userId || getActData.ActReqUserId == userId))
                {
                    btnRequest.Visible = false;
                }
                if ((getActData.ActReqStatus == "7") && eventName == "ViewRow" && (getActData.ActReqUserId == userId))
                {
                    btnRequest.Visible = true;

                }
                else
                {
                    btnRequest.Visible = false;
                }
                if ((getActData.ActReqStatus == "4" || getActData.ActReqStatus == "3") && eventName == "EditRow")
                {
                    btnComplete.Visible = true;
                    btnUpdate.Visible = false;
                }
                if ((getActData.ActReqStatus == "3") && eventName == "EditRow" && userId == getActData.ActCreatedBy) //ACT IS APPROVED BUT YET NOT ONGOING SO ALLOW TO EDIT
                {
                    btnComplete.Visible = false;
                    btnUpdate.Visible = true;
                }
                if (SUBEVENT == "ViewRowPrivate")
                {
                    if (userId != getActData.ActCreatedBy)
                    {
                        txtcom.Enabled = true;
                    }
                    btnRequest.Visible = true;
                    btnComplete.Visible = false;
                    btnUpdate.Visible = false;
                    justificationDiv.Visible = true;
                }
                if (SUBEVENT == "EditRowPrivate")
                {
                    btnRequest.Visible = false;
                    btnComplete.Visible = false;
                    btnUpdate.Visible = true;
                }
                if (getActData.ActIsPublic)
                {
                    // GetAllActivityCategory(1);
                    publicDiv.Visible = true;
                    enableDiv.Visible = true;
                }
                else
                {
                    // GetAllActivityCategory(-1);
                    publicDiv.Visible = false;
                    enableDiv.Visible = false;
                    commentDivServerSide.Visible = false;
                }
                if (getActData.ActReqId == null)
                {
                    commentDivServerSide.Visible = false;//that means activity is private and user cant comment on provate activity
                }
                {
                    commentDivServerSide.Visible = true;//that means activity is private and user cant comment on provate activity
                }
                txtActName.Text = getActData.ActName;
                txtActCost.Text = Convert.ToString(getActData.ActCost);

                txtActEndDate.Text = getActData.ActEndDate.ToString("dd/MM/yyyy");
                txtActStartDate.Text = getActData.ActStartDate.ToString("dd/MM/yyyy");

                if (Convert.ToBoolean(getActData.ActIsPublic) == true)
                {
                    rdoPublic.SelectedValue = "true";

                    // rdoGender.SelectedIndex = 0;
                }
                else
                {
                    rdoPublic.SelectedValue = "false";

                }

                if (Convert.ToBoolean(getActData.ActIsActive) == false)
                {
                    rdoEnabled.SelectedValue = "false";

                    // rdoGender.SelectedIndex = 0;
                }
                else
                {
                    rdoEnabled.SelectedValue = "true";
                }

                if (Convert.ToBoolean(getActData.ActCompEnabled) == true)
                {
                    chkIsCompDevDoc.Items[0].Selected = true;
                    chkIsCompDevDoc.Items[1].Selected = false;
                    // rdoGender.SelectedIndex = 0;
                    ddCompetence.Enabled = true;
                    chkIsCompDevDoc.Enabled = true;
                    rdoLevel.Enabled = true;
                }
                else
                {
                    ddCompetence.Enabled = false;
                    chkIsCompDevDoc.Enabled = false;
                    rdoLevel.Enabled = false;
                    chkIsCompDevDoc.SelectedValue = "false";

                }

                if (Convert.ToBoolean(getActData.ActIsComp) == true)
                {
                    chkIsCompDevDoc.SelectedValue = "true";
                }
                else
                {

                    chkIsCompDevDoc.SelectedValue = "false";

                }


                try
                {
                    dd_Priotiy.SelectedValue = Convert.ToString(getActData.ActPriority);
                }
                catch (Exception)
                {

                    dd_Priotiy.SelectedValue = "1";
                }


                if (!Convert.ToBoolean(getActData.ActCatDeveplan))
                {
                    // ddCompetence.Enabled = false;
                    //chkIsCompDevDoc.Enabled = false;
                    //rdoLevel.Enabled = false;
                }
                else
                {
                    //ddCompetence.Enabled = true;
                    //chkIsCompDevDoc.Enabled = true;
                    //rdoLevel.Enabled = true;
                }

                chkListDivision.SelectedValue = Convert.ToString(getActData.ActDivId);
                chkListJobtype.SelectedValue = Convert.ToString(getActData.ActJobId);

                ddCompetence.SelectedValue = Convert.ToString(Convert.ToInt32(getActData.ActCompId));

                try
                {
                    ddActCate.ClearSelection();
                    ddActCate.SelectedIndex = -1;
                    //  ddActCate.SelectedValue = "";
                    ddActCate.SelectedValue = Convert.ToString(getActData.ActCatRefId);

                }
                catch (Exception ex)
                {
                    ExceptionLogger.LogException(ex, 0, "ddActCate");
                }

                try
                {


                    txtActTags.Text = getActData.ActTags;
                    txtActRequirements.Text = getActData.ActRequirements;

                    txtActDescription.Text = getActData.ActDescription;
                    try
                    {
                        rdoLevel.SelectedValue = getActData.ActCompLevel;
                    }
                    catch (Exception ex)
                    {
                        ExceptionLogger.LogException(ex, 0, "rdoLevel.SelectedValue 1");
                    }


                    txtExpctBehavChange.Text = getActData.ActReqExpJustBehav;
                    txtJustification.Text = getActData.ActReqJustification;


                }
                catch (Exception ex)
                {
                    ExceptionLogger.LogException(ex, 0, "");
                }



            }
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "GetActivityData");
        }
    }

    protected void SetDefaultMessage()
    {
        // lblMsg.Text = GetLocalResourceObject("lblHelloWorld.Text").ToString();

        if (!String.IsNullOrEmpty(Convert.ToString(Request.QueryString["msg"])))
        {
            if (Convert.ToString(Request.QueryString["msg"]) == "CompleteActivity")
            {
                //lblMsg.Text = CommonModule.msgRecordHasBeenDeletedSuccessfully;
                //lblMsg.Text = CommonMessages.msgRecordHasBeenDeletedSuccessfully;
                lblMsg.Text = GetLocalResourceObject("CompleteActivity_Successfull.Text").ToString();
                lblMsg.ForeColor = Color.White;
            }
            if (Convert.ToString(Request.QueryString["msg"]) == "RequestActivity")
            {
                //lblMsg.Text = CommonModule.msgRecordHasBeenDeletedSuccessfully;
                //lblMsg.Text = CommonMessages.msgRecordHasBeenDeletedSuccessfully;
                lblMsg.Text = GetLocalResourceObject("RequestActivity_Successfull.Text").ToString();
                lblMsg.ForeColor = Color.White;
            }


            if (Convert.ToString(Request.QueryString["msg"]) == "del")
            {
                //lblMsg.Text = CommonModule.msgRecordHasBeenDeletedSuccessfully;
                //lblMsg.Text = CommonMessages.msgRecordHasBeenDeletedSuccessfully;
                lblMsg.Text = GetLocalResourceObject("msgRecordHasBeenDeletedSuccessfully.Text").ToString();
                lblMsg.ForeColor = Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "ins")
            {
                //lblMsg.Text = CommonMessages.msgRecordInsertedSuccessfully;
                lblMsg.Text = GetLocalResourceObject("msgRecordInsertedSuccessfully.Text").ToString();
                lblMsg.ForeColor = Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "upd")
            {
                //lblMsg.Text = CommonMessages.msgRecordUpdatedSuccss;
                lblMsg.Text = GetLocalResourceObject("msgRecordUpdatedSuccss.Text").ToString();
                lblMsg.ForeColor = Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "AssignActivity")
            {
                lblMsg.Text = GetLocalResourceObject("AssignActivitySuccssMsg.Text").ToString();
                lblMsg.ForeColor = Color.White;
            }
        }

    }

    protected void btnBack_click(object sender, EventArgs e)
    {
        switch (Convert.ToInt32(Session["OrguserType"]))
        {
            case 2:
                Response.Redirect("Manager-dashboard.aspx");
                break;
            case 1:
                Response.Redirect("Organisation-dashboard.aspx");
                break;
            default:
                Response.Redirect("Employee_dashboard.aspx");
                break;
        }
    }


    protected void InsertNotification(int reqId, string subject = "Has Set Reqest.", string templateName = "ActivityRequest")
    {
        NotificationBM obj = new NotificationBM();
        obj.notsubject = subject;//"Has Set Reqest.";
        obj.notUserId = Convert.ToInt32(Session["OrgUserId"]);
        obj.notIsActive = true;
        obj.notIsDeleted = false;
        obj.notCreatedDate = DateTime.Now;
        obj.notCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.notpage = "ActivityRequestView.aspx?actreqid=" + reqId;

        if (templateName == "ActivityRequest")
        {
            obj.nottype = "ActReq";
        }
        else if (templateName == "ActivityComplete")
        {
            obj.nottype = "ActCom";
        }
        else if (templateName == "ActivityReRequest")
        {
            obj.nottype = "ActReReq";
        }
        obj.notToUserId = Convert.ToInt32(Session["OrgCreatedBy"]);
        obj.InsertNotification();


        sendmail(Convert.ToInt32(Session["OrgCreatedBy"]), templateName);
    }

    protected void sendmail(Int32 touserid, string templateName = "ActivityRequest")
    {
        #region Send mail
        // String subject = "Set Competence for " + Convert.ToString(Session["OrgUserName"]);
        Template template = CommonModule.getTemplatebyname1(templateName, Convert.ToInt32(Session["OrgCreatedBy"]));
        //String confirmMail = CommonModule.getTemplatebyname("Set Competence", touserid);
        String confirmMail = template.TemplateName;
        if (!String.IsNullOrEmpty(confirmMail))
        {
            var localResourceObject = GetLocalResourceObject("Activity_Request_Email_Subject.Text");
            if (templateName == "ActivityComplete")
            {
                localResourceObject = GetLocalResourceObject("Activity_Complete_Email_Subject.Text");
            }
            else if (templateName == "ActivityReRequest")
            {
                localResourceObject = HttpContext.GetLocalResourceObject("~/Organisation/ActivityLists.aspx", "ActivityReRequest_Email_Subject.Text");
            }
            if (localResourceObject != null)
            {
                string subject = localResourceObject.ToString();


                UserBM Cust = new UserBM();
                Cust.userId = Convert.ToInt32(Session["OrgCreatedBy"]);
                Cust.SelectmanagerByUserId();
                //  Cust.SelectPasswordByUserId();
                DataSet ds = Cust.ds;
                if (ds != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        String name = Convert.ToString(ds.Tables[0].Rows[0]["userFirstName"]);
                        string fromid = Convert.ToString(ConfigurationManager.AppSettings["fromid"]);
                        String empname = Convert.ToString(Session["OrgUserName"]);
                        string tempString = confirmMail;
                        tempString = tempString.Replace("###name###", name);
                        tempString = tempString.Replace("###empname###", empname);
                        CommonModule.SendMailToUser(Convert.ToString(ds.Tables[0].Rows[0]["userEmail"]), subject, tempString, fromid, Convert.ToString(HttpContext.Current.Session["OrgCompanyId"]));

                        ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'> generate('information', '<b>'" + RequestSentSuccessful.Value + "'</b><br> ', 'bottomCenter');</script>", false);
                    }
                }
            }
        }

        #endregion
    }

    protected void OpenCreateActivity(object sender, EventArgs e)
    {
        Clear();

        lblModelHeader.Text = hdnAddNewActivity.Text;
        justificationDiv.Visible = false;
        Expected_Behaviour_Change_Div.Visible = false;
        btnCreate.Visible = true;
        btnRequest.Visible = false;
        btnUpdate.Visible = false;
        commentDivServerSide.Visible = false;
        rdoPublic.SelectedValue = "false";
        //ScriptManager.RegisterStartupScript(updtPnl, updtPnl.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lbl').click();", true);

        ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lbl').click();", true);
        GetAllActivityCategory(0, 0);
        publicDiv.Visible = false;
        enableDiv.Visible = false;
        /////////////////Added on 17/01/2018////////////////
        divAssignTo.Visible = true;
        hdnActId.Value = "0";
        GetAssignToUserListsInDropDown(0);
        btnComplete.Visible = false;
        //  rdoPublic.SelectedValue = "true";

    }

    protected void OpenCreatePublicActivity(object sender, EventArgs e)
    {
        Clear();
        publicDiv.Visible = true;
        enableDiv.Visible = true;

        lblModelHeader.Text = GetLocalResourceObject("AddNewPublicActivity.Text").ToString();

        justificationDiv.Visible = false;
        Expected_Behaviour_Change_Div.Visible = false;

        btnCreate.Visible = true;
        btnRequest.Visible = false;
        btnComplete.Visible = false;
        btnUpdate.Visible = false;
        GetAllActivityCategory(1, -1);
        GetAssignToUserListsInDropDown(0);
        commentDivServerSide.Visible = false;
        divAssignTo.Visible = false;
        //ScriptManager.RegisterStartupScript(updtPnl, updtPnl.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lbl').click();", true);
        hdnActId.Value = "0";
        ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lbl').click();", true);
        rdoPublic.SelectedValue = "true";

    }


    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }

    #region Culture
    protected override void InitializeCulture()
    {
        // string language = "French";
        string language = Convert.ToString(Session["Culture"]);

        string languageId = "";

        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!String.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }

        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
    protected void Clear()
    {

        // ddActCate.Items.Clear();
        EnableFields(true);
        txtActName.Text = String.Empty;
        txtActRequirements.Text = String.Empty;
        txtActDescription.Text = String.Empty;
        txtActRequirements.Text = String.Empty;
        txtActTags.Text = String.Empty;

        txtActStartDate.Text = String.Empty;
        txtActEndDate.Text = String.Empty;
        txtActCost.Text = String.Empty;

        ddCompetence.SelectedValue = "0";

        chkListDivision.SelectedValue = "0";
        chkListJobtype.SelectedValue = "0";
        // ddActCate.SelectedValue = "0";
        txtError.Text = "";
        errorDiv.Visible = false;
        commentDivServerSide.Visible = false;

    }

    #region Comment-Webservice
    [WebMethod(EnableSession = true)]
    public static GetActivityCommentById_Result[] GetActivityCommentById(string AcrCreaterId, int ActID, int LoginUserID, int UserType)
    {
        int reqID = 0;
        if (String.IsNullOrEmpty(AcrCreaterId))
        {
            reqID = 0;
        }
        else
        {
            reqID = Convert.ToInt32(AcrCreaterId);
        }
        var actReqList = ActReqComment.GetActivityCommentById(reqID, ActID, LoginUserID, UserType);


        return actReqList.ToArray();
    }

    [WebMethod(EnableSession = true)]
    public static string insertmessage(string message, string ActReqId, string ActReqUserId, string ActID)
    {



        string msg = String.Empty;
        CompetenceBM obj = new CompetenceBM();


        var db = new startetkuEntities1();


        var actCat = new ActvityReqComment();
        actCat.AcrCompanyID = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        actCat.AcrCompComm = message;

        actCat.AcrCreaterId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        actCat.AcrComToUserID = Convert.ToInt32(ActReqUserId);
        if (String.IsNullOrEmpty(ActReqId))
        {
            actCat.ActComActReqId = 0;
        }
        else
        {
            actCat.ActComActReqId = Convert.ToInt32(ActReqId);
        }
        if (String.IsNullOrEmpty(ActID))
        {
            actCat.ActID = 0;
        }
        else
        {
            actCat.ActID = Convert.ToInt32(ActID);
        }
        //  actCat.ActID = Convert.ToInt32(ActID);  //swati on 24/01/2018

        actCat.AcrIsActive = true;
        actCat.AcrIsdeleted = false;
        actCat.AcrComCreateDate = CommonUtilities.GetCurrentDateTime();
        actCat.AcrComUpdateDate = CommonUtilities.GetCurrentDateTime();

        actCat.ActUserTypeId = Convert.ToInt32(HttpContext.Current.Session["OrguserType"]);
        actCat.ActUserCreatedById = Convert.ToInt32(HttpContext.Current.Session["OrgCreatedBy"]);
        //

        db.ActvityReqComments.Add(actCat);
        db.SaveChanges();
        msg = "true";


        return msg;
    }
    public class comment
    {
        public string compId { get; set; }

        public string name { get; set; }
        public string comuserid { get; set; }
        public string Image { get; set; }
        public string compcomment { get; set; }
        public string comcreatedate { get; set; }

    }
    #endregion

    protected void AdvanceFilter_Public(object sender, EventArgs e)
    {

        GetPublicActivityList();

    }

    protected void AdvanceFilter_Private(object sender, EventArgs e)
    {
        GetPrivateActivityList();


    }

    protected void ddActCate_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            //ScriptManager.RegisterStartupScript(updtPnl, updtPnl.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lbl').click();", true);

            ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lbl').click();", true);
            var selectedCatId = ddActCate.SelectedValue;

            var getCatObj = ActivityCategoryLogic.GetActivityCategoryList(ResLangId, 0, -1, -1, Convert.ToInt32(selectedCatId));

            if (getCatObj.Any())
            {
                var activityCategoryGetResult = getCatObj.FirstOrDefault();
                var isCompetenceCat = activityCategoryGetResult != null && Convert.ToBoolean(activityCategoryGetResult.ActCompEnabled);
                chkIsCompDevDoc.SelectedValue = isCompetenceCat ? "true" : "false";

                if (!isCompetenceCat)
                {
                    ddCompetence.Enabled = false;
                    chkIsCompDevDoc.Enabled = false;
                    rdoLevel.Enabled = false;
                }
                else
                {
                    ddCompetence.Enabled = true;
                    chkIsCompDevDoc.Enabled = true;
                    rdoLevel.Enabled = true;
                }
            }
        }
        catch (Exception)
        {


        }
    }


    protected void dd_Private_Cat_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        GetPrivateActivityList();
    }

    protected void ddCat_Filter_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        GetPublicActivityList();
    }

    protected void gvGrid_PreRender(object sender, EventArgs e)
    {
        // You only need the following 2 lines of code if you are not 
        // using an ObjectDataSource of SqlDataSource
        //gvGrid.DataSource = Sample.GetData();
        //gvGrid.DataBind();

        if (gvGrid_Public.Rows.Count > 0)
        {
            //This replaces <td> with <th> and adds the scope attribute
            gvGrid_Public.UseAccessibleHeader = true;

            //This will add the <thead> and <tbody> elements
            gvGrid_Public.HeaderRow.TableSection = TableRowSection.TableHeader;

            //This adds the <tfoot> element. 
            //Remove if you don't have a footer row
            gvGrid_Public.FooterRow.TableSection = TableRowSection.TableFooter;
        }
    }

    protected void gvGrid_Private_PreRender(object sender, EventArgs e)
    {
        // You only need the following 2 lines of code if you are not 
        // using an ObjectDataSource of SqlDataSource
        //gvGrid.DataSource = Sample.GetData();
        //gvGrid.DataBind();

        if (gvGrid_Private.Rows.Count > 0)
        {
            //This replaces <td> with <th> and adds the scope attribute
            gvGrid_Private.UseAccessibleHeader = true;

            //This will add the <thead> and <tbody> elements
            gvGrid_Private.HeaderRow.TableSection = TableRowSection.TableHeader;

            //This adds the <tfoot> element. 
            //Remove if you don't have a footer row
            gvGrid_Private.FooterRow.TableSection = TableRowSection.TableFooter;
        }
    }

    protected void CloseActivity(object sender, EventArgs e)
    {
        var db = new startetkuEntities1();
        var ac = Convert.ToInt32(hdnActId.Value);
        var userId = Convert.ToInt32(Session["OrgUserId"]);
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        var checkIsReqUser = db.ActReqMasters.FirstOrDefault(o => o.ActReqActId == ac && o.ActReqUserId == userId);
        if (checkIsReqUser == null)
        {
            db.UpdateRequestComment(ac, 0, userId, 0, companyId);
        }

        Response.Redirect("ActivityLists.aspx");
    }

    public string GetActivityStatusByStatusId(string actReqStatus, bool isPrivate)
    {
        /*
     * About Activity Request Stauts:  2017 04 01
     
     * We need to change status on an activity
    Available” ==1
            Activity can be requested
            Should be auto set when creating a public activity
    Requested   ==2
            Activity have been requested by user
            Should be auto set when user request a public activity
    Request approved ==3
            Manager have approved requested activity
            Should be auto set when manager approve users activity request
    Ongoing ==4
            Should be auto set when start date have been reached
    Completed ==5
            employee flag activity has been completed
            Should be set manuel by user (or manager)
    Completed approval ==6
            Manager have approved that activity have been completed by user
            Should be set manual by manager (or system owner)
     * 
     * REJECTED == 7
    */
        //var status = isPrivate ? StringFor.CREATED : StringFor.Available;

        GetResource();
        //  var status = isPrivate ? StringFor.CREATED : StringFor.Available;
        var status = isPrivate ? CREATED : AVAILABLE;

        if (isPrivate == true)
        {
            switch (actReqStatus)
            {
                case "2":
                    {
                        status = REQUESTED;//getResource.GetResource("REQUESTED.Text");//StringFor.REQUESTED;
                        break;
                    }
                case "3":
                    {
                        status = ASSIGNED;// getResource.GetResource("REQUESTED.Text"); //StringFor.APPROVED;
                        break;
                    }
                case "4":
                    {
                        status = ONGOING;// getResource.GetResource("REQUESTED.Text"); //StringFor.ONGOING;
                        break;
                    }
                case "5":
                    {
                        status = COMPLETED;// getResource.GetResource("REQUESTED.Text"); //StringFor.COMPLETED;
                        break;
                    }
                case "6":
                    {
                        status = COMPLETED_APPROVE;//getResource.GetResource("REQUESTED.Text"); //StringFor.COMPLETED_APPROVE;
                        break;
                    }
                case "7":
                    {
                        status = REJECTED;//getResource.GetResource("REQUESTED.Text"); //StringFor.REJECTED;
                        break;
                    }
            }
        }
        else
        {
            switch (actReqStatus)
            {
                case "2":
                    {
                        status = REQUESTED;//getResource.GetResource("REQUESTED.Text");//StringFor.REQUESTED;
                        break;
                    }
                case "3":
                    {
                        status = APPROVED;// getResource.GetResource("REQUESTED.Text"); //StringFor.APPROVED;
                        break;
                    }
                case "4":
                    {
                        status = ONGOING;// getResource.GetResource("REQUESTED.Text"); //StringFor.ONGOING;
                        break;
                    }
                case "5":
                    {
                        status = COMPLETED;// getResource.GetResource("REQUESTED.Text"); //StringFor.COMPLETED;
                        break;
                    }
                case "6":
                    {
                        status = COMPLETED_APPROVE;//getResource.GetResource("REQUESTED.Text"); //StringFor.COMPLETED_APPROVE;
                        break;
                    }
                case "7":
                    {
                        status = REJECTED;//getResource.GetResource("REQUESTED.Text"); //StringFor.REJECTED;
                        break;
                    }

                case "8":
                    {
                        status = CREATED;//getResource.GetResource("CREATED.Text"); //StringFor.CREATED;
                        break;
                    }
                case "9":
                    {
                        status = AVAILABLE;//getResource.GetResource("AVAILABLE.Text"); //StringFor.AVAILABLE;
                        break;
                    }


            }
        }
        return status;
    }

    public string REQUESTED { get; set; }
    public string APPROVED { get; set; }
    public string ONGOING { get; set; }

    public string COMPLETED { get; set; }
    public string COMPLETED_APPROVE { get; set; }
    public string REJECTED { get; set; }

    public string CREATED { get; set; }
    public string AVAILABLE { get; set; }
    public string ENABLED { get; set; }
    public string DISABLED { get; set; }

    public string ASSIGNED { get; set; }

    private void GetResource()
    {
        REQUESTED = GetLocalResourceObject("REQUESTED.Text").ToString();
        APPROVED = GetLocalResourceObject("APPROVED.Text").ToString();
        ONGOING = GetLocalResourceObject("ONGOING.Text").ToString();

        COMPLETED = GetLocalResourceObject("COMPLETED.Text").ToString();
        COMPLETED_APPROVE = GetLocalResourceObject("COMPLETED_APPROVE.Text").ToString();
        REJECTED = GetLocalResourceObject("REJECTED.Text").ToString();

        CREATED = GetLocalResourceObject("CREATED.Text").ToString();
        AVAILABLE = GetLocalResourceObject("AVAILABLE.Text").ToString();
        ENABLED = GetLocalResourceObject("ENABLED.Text").ToString();
        DISABLED = GetLocalResourceObject("DISABLED.Text").ToString();

        ASSIGNED = GetLocalResourceObject("ASSIGNED.Text").ToString();
    }

    //Create Activity
    #region Create-Update-Request-Complete-Activity

    protected void CreateActivity(object sender, EventArgs e)
    {
        CreateActivityMethod();
    }


    protected void CreateActivityMethod()
    {
        var db = new startetkuEntities1();
        try
        {
            // check Duplicate
            var companyId = Convert.ToInt32(Session["OrgCompanyId"]);

            var isDup = db.ActivityMasters.FirstOrDefault(o => o.ActName == txtActName.Text && o.ActCompanyId == companyId);
            if (isDup != null && hdnActId.Value != "0")
            {
                //Already created when activity assigned to user
            }
            else if (isDup != null)
            {
                //Opps there is duplicate records
                errorDiv.Visible = true;
                txtError.Text = @"Opps! Records already found";
                //ScriptManager.RegisterStartupScript(updtPnl, updtPnl.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lbl').click();", true);

                ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lbl').click();", true);

            }
            else
            {
                errorDiv.Visible = false;
                //Save record
                var actObj = new ActivityMaster();

                actObj.ActName = txtActName.Text;
                actObj.ActBehaviouralChange = "";

                if (String.IsNullOrWhiteSpace(txtActCost.Text))
                {
                    txtActCost.Text = @"0";
                }
                actObj.ActCost = Convert.ToInt32(txtActCost.Text);
                actObj.ActCreatedBy = Convert.ToInt32(Session["OrgUserId"]);


                actObj.ActEndDate = CommonUtilities.ConvertStringToDateTime(txtActEndDate.Text);
                actObj.ActStartDate = CommonUtilities.ConvertStringToDateTime(txtActStartDate.Text);

                actObj.ActDescription = txtActDescription.Text;
                actObj.ActTags = txtActTags.Text;


                actObj.ActIsActive = rdoEnabled.SelectedItem.Value == @"true";
                actObj.ActPriority = Convert.ToInt32(dd_Priotiy.SelectedValue);

                var actDivId = 0;
                if (!String.IsNullOrWhiteSpace(chkListDivision.SelectedItem.Value))
                {
                    try
                    {
                        actDivId = Convert.ToInt32(chkListDivision.SelectedItem.Value);
                    }
                    catch (Exception)
                    {


                    }

                }

                var actJobId = 0;
                if (!String.IsNullOrWhiteSpace(chkListJobtype.SelectedItem.Value))
                {
                    try
                    {
                        actJobId = Convert.ToInt32(chkListJobtype.SelectedItem.Value);

                    }
                    catch (Exception)
                    {


                    }

                }



                var actCompId = 0;
                if (!String.IsNullOrWhiteSpace(ddCompetence.SelectedItem.Value))
                {
                    try
                    {
                        actCompId = Convert.ToInt32(ddCompetence.SelectedItem.Value);
                    }
                    catch (Exception)
                    {


                    }

                }

                var actCatRefId = 0;
                if (!String.IsNullOrWhiteSpace(chkListJobtype.SelectedItem.Value))
                {
                    try
                    {
                        actCatRefId = Convert.ToInt32(ddActCate.SelectedItem.Value);
                    }
                    catch (Exception)
                    {


                    }

                }


                actObj.ActDivId = actDivId;
                actObj.ActJobId = actJobId;
                actObj.ActCompId = actCompId;
                actObj.ActIsComp = Convert.ToBoolean(chkIsCompDevDoc.SelectedValue);
                actObj.ActCatRefId = actCatRefId;

                //if (Convert.ToInt32(Session["OrguserType"]) == 3)
                //{
                //    actObj.ActIsPublic = true;
                //}
                //else
                //{
                //    actObj.ActIsPublic = rdoPublic.SelectedItem.Value == @"true";
                //}

                actObj.ActIsPublic = rdoPublic.SelectedItem.Value == @"true";
                actObj.ActStatus = "";

                actObj.ActJustification = String.Empty;
                actObj.ActRequirements = txtActRequirements.Text;

                actObj.ActCompLevel = rdoLevel.SelectedItem.Text;

                actObj.ActCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);

                actObj.ActCreatedDate = CommonUtilities.GetCurrentDateTime();
                actObj.ActUpdateDate = CommonUtilities.GetCurrentDateTime();

                actObj.ActIsDeleted = false;

                db.ActivityMasters.Add(actObj);
                db.SaveChanges();
                hdnActId.Value = Convert.ToString(actObj.ActId);

                GetPrivateActivityList();
                GetPublicActivityList();

            }

        }
        catch (Exception ex)
        {

            ExceptionLogger.LogException(ex);
        }
        finally
        {
            CommonAttributes.DisposeDBObject(ref db);
        }
    }

    protected void CompleteActivity(object sender, EventArgs e)
    {
        var db = new startetkuEntities1();
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        try
        {
            Int32 reqId = 0;
            if (String.IsNullOrEmpty(hdnActReqIdInt.Value))
            {
                reqId = Convert.ToInt32(hdnActId.Value);
                var getActReqObj = db.ActivityMasters.FirstOrDefault(o => o.ActId == reqId);
                var chkDup = db.ActivityMasters.FirstOrDefault(o => o.ActId != reqId && o.ActName == txtActName.Text && o.ActCompanyId == companyId);
                if (chkDup != null)
                {
                    //Opps there is duplicate records
                    errorDiv.Visible = true;
                    txtError.Text = @"Record Already Found";
                    //ScriptManager.RegisterStartupScript(updtPnl, updtPnl.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lbl').click();", true);

                    ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lbl').click();", true);
                    return;

                }
                else
                {
                    if (getActReqObj != null)
                    {
                        errorDiv.Visible = false;
                        getActReqObj.ActStatus = "5";
                        getActReqObj.ActName = txtActName.Text;
                        getActReqObj.ActCost = Convert.ToInt32(txtActCost.Text);
                        getActReqObj.ActEndDate = CommonUtilities.ConvertStringToDateTime(txtActEndDate.Text);
                        getActReqObj.ActStartDate = CommonUtilities.ConvertStringToDateTime(txtActStartDate.Text);
                        getActReqObj.ActDescription = txtActDescription.Text;
                        getActReqObj.ActTags = txtActTags.Text;

                        getActReqObj.ActPriority = Convert.ToInt32(dd_Priotiy.SelectedValue);
                        getActReqObj.ActIsActive = rdoEnabled.SelectedItem.Value == @"true";

                        var actDivId = 0;
                        if (!String.IsNullOrWhiteSpace(chkListDivision.SelectedItem.Value))
                        {
                            try
                            {
                                actDivId = Convert.ToInt32(chkListDivision.SelectedItem.Value);
                            }
                            catch (Exception)
                            {
                            }

                        }

                        var actJobId = 0;
                        if (!String.IsNullOrWhiteSpace(chkListDivision.SelectedItem.Value))
                        {
                            try
                            {
                                actJobId = Convert.ToInt32(chkListJobtype.SelectedItem.Value);

                            }
                            catch (Exception)
                            {
                            }
                        }

                        var actCompId = 0;
                        if (!String.IsNullOrWhiteSpace(ddCompetence.SelectedItem.Value))
                        {
                            try
                            {
                                actCompId = Convert.ToInt32(ddCompetence.SelectedItem.Value);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        var actCatRefId = 0;
                        if (!String.IsNullOrWhiteSpace(chkListDivision.SelectedItem.Value))
                        {
                            try
                            {
                                actCatRefId = Convert.ToInt32(ddActCate.SelectedItem.Value);
                            }
                            catch (Exception)
                            {
                            }
                        }
                        getActReqObj.ActDivId = actDivId;
                        getActReqObj.ActJobId = actJobId;
                        getActReqObj.ActCompId = actCompId;
                        getActReqObj.ActIsComp = Convert.ToBoolean(chkIsCompDevDoc.SelectedValue);
                        getActReqObj.ActCatRefId = actCatRefId;

                        getActReqObj.ActIsPublic = rdoPublic.SelectedItem.Value == @"true";
                        getActReqObj.ActRequirements = txtActRequirements.Text;

                        getActReqObj.ActCompLevel = rdoLevel.SelectedItem.Text;
                        getActReqObj.ActUpdateDate = CommonUtilities.GetCurrentDateTime();

                        db.SaveChanges();
                        InsertNotification(reqId, "Has Completed Activity", "ActivityComplete");

                        Thread.Sleep(2000);
                        Response.Redirect("ActivityLists.aspx?msg=CompleteActivity", false);
                        HttpContext.Current.ApplicationInstance.CompleteRequest();
                    }
                }
            }
            else
            {
                reqId = Convert.ToInt32(hdnActReqIdInt.Value);
                var getActReqObj = db.ActReqMasters.FirstOrDefault(o => o.ActReqId == reqId);
                if (getActReqObj != null)
                {
                    getActReqObj.ActReqStatus = "5";
                    Int32 actID = Convert.ToInt32(getActReqObj.ActReqActId);
                    var getActObj = db.ActivityMasters.FirstOrDefault(o => o.ActId == actID);

                    getActObj.ActName = txtActName.Text;
                    getActObj.ActDescription = txtActDescription.Text;
                    getActObj.ActRequirements = txtActRequirements.Text;
                    getActObj.ActTags = txtActTags.Text;
                    getActObj.ActPriority = Convert.ToInt32(dd_Priotiy.SelectedValue);
                    db.SaveChanges();

                    InsertNotification(reqId, "Has Completed Activity", "ActivityComplete");

                    Thread.Sleep(2000);
                    Response.Redirect("ActivityLists.aspx?msg=CompleteActivity", false);
                    HttpContext.Current.ApplicationInstance.CompleteRequest();
                }
            }


        }
        catch (Exception)
        {


        }
    }
    #endregion
    protected void AssignActivity(object sender, EventArgs e)
    {
        try
        {
            CreateActivityMethod();
            InsertActivity(true);
            //  ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lbl').click();", true);
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "AssignActivity");

        }
    }

    protected void RequestActivity(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(hdnActReqStatusActivity.Value) && hdnActReqStatusActivity.Value == "7")
        {
            var db = new startetkuEntities1();
            int RequestID = 0;
            int ActID = 0;
            if (!String.IsNullOrEmpty(hdnActReqIdInt.Value))
            {
                RequestID = Convert.ToInt32(hdnActReqIdInt.Value);
            }
            var actreqData = db.ActReqMasters.Find(RequestID);
            if (actreqData != null)
            {
                ActID = Convert.ToInt32(actreqData.ActReqActId);
                actreqData.ActReqStatus = "2";
                actreqData.ActReqJustification = txtJustification.Text;
                actreqData.ActReqExpJustBehav = txtExpctBehavChange.Text;
                db.SaveChanges();

                var actData = db.ActivityMasters.Find(ActID);
                actData.ActJustification = txtJustification.Text;
                actData.ActBehaviouralChange = txtExpctBehavChange.Text;
                db.SaveChanges();

                try
                {
                    Common.WriteLog("UpdateRequestComment :" + Convert.ToString(actreqData.ActReqId) + " actID:" + Convert.ToString(actreqData.ActReqActId) + " ActReqUserId:" + Convert.ToString(actreqData.ActReqUserId) + " toUserId:" + Convert.ToString(actreqData.ActReqOrgId));
                    db.UpdateRequestComment(actreqData.ActReqActId, actreqData.ActReqId, actreqData.ActReqUserId, 0, actreqData.ActReqOrgId);
                }
                catch (Exception ex)
                {
                    ExceptionLogger.LogException(ex, 0, "UpdateRequestComment");
                }
            

                InsertNotification(actreqData.ActReqId,"Has Re-requested","ActivityReRequest");
            }

            Response.Redirect("ActivityLists.aspx?msg=AssignActivity", false);
        }
        else
        {
            InsertActivity(false);
        }
    }

    public void InsertActivity(bool isAssign)
    {
        var db = new startetkuEntities1();
        var isPointAllow = false;
        int ActReqUserId = 0;
        bool flagAlreadyReq = true;
        Int32 CompanyID = 0;

        try
        {
            var req = new ActReqMaster();
            int actID = Convert.ToInt32(hdnActId.Value);
            if (isAssign)
            {
                ActReqUserId = Convert.ToInt32(ddAssignToLists.SelectedValue);
            }
            else
            {
                ActReqUserId = Convert.ToInt32(Session["OrgUserId"]);
            }
            var toUserId = 0;
            if (!String.IsNullOrWhiteSpace(Convert.ToString(Session["OrgCreatedBy"])))
            {
                //SWati added as If manager assign Act to Emp ToUserID goes of Owner
                if (Convert.ToInt32(Session["OrguserType"]) == 2 && isAssign == true)
                {
                    toUserId = Convert.ToInt32(Session["OrgUserId"]);

                }
                else
                {
                    toUserId = Convert.ToInt32(Session["OrgCreatedBy"]);
                }
            }

            req = db.ActReqMasters.FirstOrDefault(o => o.ActReqUserId == ActReqUserId && o.ActReqToUserId == toUserId && o.ActReqActId == actID);
            if (req == null)
            {
                req = new ActReqMaster();
                flagAlreadyReq = false;
            }
            req.ActReqCreateDate = CommonUtilities.GetCurrentDateTime();
            req.ActReqUpdateDate = CommonUtilities.GetCurrentDateTime();
            req.ActReqIsActive = true;
            //if (!String.IsNullOrEmpty(hdnCurrentActivityStatus.Value) && (Convert.ToInt32(hdnCurrentActivityStatus.Value) == 7))  // If old request is rejected than new request data will be empty
            //{
            //    req.ActReqJustification = "";
            //    req.ActReqExpJustBehav = "";
            //}
            //else
            {
                req.ActReqJustification = txtJustification.Text;
                req.ActReqExpJustBehav = txtExpctBehavChange.Text;
            }
            req.ActReqStatus = "2";
            if (isAssign)
            {
                req.ActReqUserId = Convert.ToInt32(ddAssignToLists.SelectedValue);
                req.ActReqStatus = "3"; //Approve
            }
            else
            {
                req.ActReqUserId = Convert.ToInt32(Session["OrgUserId"]);
                req.ActReqStatus = "2"; //Request

            }


            req.ActReqToUserId = toUserId;


            req.ActReqActId = Convert.ToInt32(hdnActId.Value);
            CompanyID = Convert.ToInt32(Session["OrgCompanyId"]);
            req.ActReqOrgId = Convert.ToInt32(Session["OrgCompanyId"]);

            //New Added------------------------
            req.ActReqEndDate = CommonUtilities.ConvertStringToDateTime(txtActEndDate.Text);
            req.ActReqStartDate = CommonUtilities.ConvertStringToDateTime(txtActStartDate.Text);
            req.ActReqCost = Convert.ToInt32(txtActCost.Text);
            req.ActReqName = Convert.ToString(txtActName.Text);
            var actDivId = 0;
            if (!String.IsNullOrWhiteSpace(chkListDivision.SelectedItem.Value))
            {
                try
                {
                    actDivId = Convert.ToInt32(chkListDivision.SelectedItem.Value);
                }
                catch (Exception)
                {
                }
            }

            var actJobId = 0;
            if (!String.IsNullOrWhiteSpace(chkListJobtype.SelectedItem.Value))
            {
                try
                {
                    actJobId = Convert.ToInt32(chkListJobtype.SelectedItem.Value);
                }
                catch (Exception)
                {
                }
            }
            var actCompId = 0;
            if (!String.IsNullOrWhiteSpace(ddCompetence.SelectedItem.Value))
            {
                try
                {
                    actCompId = Convert.ToInt32(ddCompetence.SelectedItem.Value);
                }
                catch (Exception)
                {
                }
            }

            var actCatRefId = 0;
            if (!String.IsNullOrWhiteSpace(chkListJobtype.SelectedItem.Value))
            {
                try
                {
                    actCatRefId = Convert.ToInt32(ddActCate.SelectedItem.Value);
                }
                catch (Exception)
                {
                }

            }
            req.ActReqCatRefId = actCatRefId;
            req.ActReqDivId = actDivId;
            req.ActReqJobId = actJobId;
            req.ActReqDescription = txtActDescription.Text;
            req.ActReqRequirements = txtActRequirements.Text;
            req.ActReqTags = txtActTags.Text;
            req.ActReqIsComp = Convert.ToBoolean(chkIsCompDevDoc.SelectedValue);
            req.ActReqCompLevel = rdoLevel.SelectedItem.Text;
            req.ActReqPriority = Convert.ToInt32(dd_Priotiy.SelectedValue);

            //---------------------------------
            if (flagAlreadyReq == false)
            {
                db.ActReqMasters.Add(req);
            }
            db.SaveChanges();

            try
            {
                Common.WriteLog("UpdateRequestComment :" + Convert.ToString(req.ActReqId) + " actID:" + Convert.ToString(req.ActReqActId) + " ActReqUserId:" + Convert.ToString(ActReqUserId) + " toUserId:" + Convert.ToString(CompanyID));
                db.UpdateRequestComment(req.ActReqActId, req.ActReqId, ActReqUserId, 0, CompanyID);
            }
            catch (Exception ex)
            {
                ExceptionLogger.LogException(ex, 0, "UpdateRequestComment");
            }
            

            if (!isAssign)
            {
                InsertNotification(req.ActReqId);
            }

            // Commented by Swati on 17/01/18
            Response.Redirect("ActivityLists.aspx?msg=AssignActivity", false);
            //HttpContext.Current.ApplicationInstance.CompleteRequest();
            //GetActivityList();

        }
        catch (DbEntityValidationException ex)
        {


            foreach (var eve in ex.EntityValidationErrors)
            {
                ExceptionLogger.LogException(ex, 0, String.Format("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                    eve.Entry.Entity.GetType().Name, eve.Entry.State));
                foreach (var ve in eve.ValidationErrors)
                {
                    ExceptionLogger.LogException(ex, 0, String.Format("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage));
                }
            }

        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "InsertActivity");

        }
    }
    protected void UpdateActivity(object sender, EventArgs e)
    {
        var db = new startetkuEntities1();
        ActReqMaster actReqData = null;
        var ActReqId = 0;
        try
        {
            var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
            // check Duplicate
            var id = Convert.ToInt32(hdnActId.Value);
            if (!String.IsNullOrEmpty(hdnActReqIdInt.Value))
            {
                ActReqId = Convert.ToInt32(hdnActReqIdInt.Value);
            }
            var IsPublicPrivate = Convert.ToInt32(hdnPublicPrivateAct.Value);

            var actObj = db.ActivityMasters.FirstOrDefault(o => o.ActId == id);
            if (actObj == null)
            {
                //Opps there is duplicate records
                errorDiv.Visible = true;
                txtError.Text = @"Opps! Records not found";
                //ScriptManager.RegisterStartupScript(updtPnl, updtPnl.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lbl').click();", true);

                ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lbl').click();", true);

            }
            else
            {
                var chkDup = db.ActivityMasters.FirstOrDefault(o => o.ActId != id && o.ActName == txtActName.Text && o.ActCompanyId == companyId);
                if (chkDup != null)
                {
                    //Opps there is duplicate records
                    errorDiv.Visible = true;
                    txtError.Text = @"Record Already Found";
                    //ScriptManager.RegisterStartupScript(updtPnl, updtPnl.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lbl').click();", true);

                    ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lbl').click();", true);
                    return;

                }

                if (IsPublicPrivate == 0)//Top Activity
                {
                    actReqData = db.ActReqMasters.Find(ActReqId);
                }
                //Lets check is there any entry in request master table : 

                errorDiv.Visible = false;
                //Save record

                actObj.ActBehaviouralChange = "";
                actObj.ActCreatedBy = Convert.ToInt32(Session["OrgUserId"]);
                var actDivId = 0;
                if (!String.IsNullOrWhiteSpace(chkListDivision.SelectedItem.Value))
                {
                    try
                    {
                        actDivId = Convert.ToInt32(chkListDivision.SelectedItem.Value);
                    }
                    catch (Exception)
                    {

                    }
                }
                var actJobId = 0;
                if (!String.IsNullOrWhiteSpace(chkListJobtype.SelectedItem.Value))
                {
                    try
                    {
                        actJobId = Convert.ToInt32(chkListJobtype.SelectedItem.Value);

                    }
                    catch (Exception)
                    {
                    }
                }

                var actCompId = 0;
                if (!String.IsNullOrWhiteSpace(ddCompetence.SelectedItem.Value))
                {
                    try
                    {
                        actCompId = Convert.ToInt32(ddCompetence.SelectedItem.Value);
                    }
                    catch (Exception)
                    {
                    }
                }

                var actCatRefId = 0;
                if (!String.IsNullOrWhiteSpace(ddActCate.SelectedItem.Value))
                {
                    try
                    {
                        actCatRefId = Convert.ToInt32(ddActCate.SelectedItem.Value);
                    }
                    catch (Exception)
                    {
                    }
                }
                //if (actReqData != null)
                //{
                //    actReqData.ActReqCost = Convert.ToInt32(txtActCost.Text);
                //    actReqData.ActReqEndDate = CommonUtilities.ConvertStringToDateTime(txtActEndDate.Text);
                //    actReqData.ActReqStartDate = CommonUtilities.ConvertStringToDateTime(txtActStartDate.Text);
                //    actReqData.ActReqName = txtActName.Text;
                //    actReqData.ActReqCatRefId = actCatRefId;
                //    actReqData.ActReqDivId = actDivId;
                //    actReqData.ActReqJobId = actJobId;
                //    actReqData.ActReqDescription = txtActDescription.Text;
                //    actReqData.ActReqRequirements = txtActRequirements.Text;
                //    actReqData.ActReqTags = txtActTags.Text;
                //    actReqData.ActReqIsComp = Convert.ToBoolean(chkIsCompDevDoc.SelectedValue);
                //    actReqData.ActReqCompId = actCompId;
                //    actReqData.ActReqCompLevel = rdoLevel.SelectedItem.Text;
                //    actReqData.ActReqPriority = Convert.ToInt32(dd_Priotiy.SelectedValue);

                //}
                //else
                //{
                actObj.ActCost = Convert.ToInt32(txtActCost.Text);
                actObj.ActEndDate = CommonUtilities.ConvertStringToDateTime(txtActEndDate.Text);
                actObj.ActStartDate = CommonUtilities.ConvertStringToDateTime(txtActStartDate.Text);
                actObj.ActName = txtActName.Text;
                actObj.ActCatRefId = actCatRefId;
                actObj.ActDivId = actDivId;
                actObj.ActJobId = actJobId;
                actObj.ActDescription = txtActDescription.Text;
                actObj.ActRequirements = txtActRequirements.Text;
                actObj.ActTags = txtActTags.Text;
                actObj.ActIsComp = Convert.ToBoolean(chkIsCompDevDoc.SelectedValue);
                actObj.ActCompId = actCompId;
                actObj.ActCompLevel = rdoLevel.SelectedItem.Text;
                actObj.ActPriority = Convert.ToInt32(dd_Priotiy.SelectedValue);
                // }
                actObj.ActIsActive = rdoEnabled.SelectedItem.Value == @"true";
                actObj.ActCompId = actCompId;
                actObj.ActIsPublic = rdoPublic.SelectedItem.Value == @"true";
                actObj.ActStatus = "";
                actObj.ActJustification = String.Empty;
                actObj.ActCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
                actObj.ActUpdateDate = CommonUtilities.GetCurrentDateTime();


                db.SaveChanges();



                Response.Redirect("ActivityLists.aspx?msg=upd", false);
                // GetActivityList();


            }

        }
        catch (Exception ex)
        {

            ExceptionLogger.LogException(ex);
        }
        finally
        {
            CommonAttributes.DisposeDBObject(ref db);
        }
    }
}