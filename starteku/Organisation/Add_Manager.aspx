﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="Add_Manager.aspx.cs" Inherits="Organisation_Add_Manager" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .treeNode
        {
        }
        .treeNode input
        {
            width: auto;
            margin: 5px;
            float: left !important;
        }
    </style>
    <style type="text/css">
        body
        {
            font-family: Geneva,Arial,Helvetica,sans-serif;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="col-md-6">
        <div class="heading-sec">
            <h1>
                Manager
                <%-- <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Employee" enableviewstate="false"/>--%>
                <i><span runat="server" id="Employee"></span></i>
            </h1>
        </div>
    </div>
    <asp:UpdatePanel runat="server" UpdateMode="Conditional" ID="lll">
        <ContentTemplate>
            <div class="col-md-12" style="margin-top: 20px;">
                <div id="graph-wrapper">
                    <div class="col-md-12">
                        <asp:Button runat="server" ID="Button1" PostBackUrl="Registration.aspx?id=1" Text="Add employee"
                            CssClass="btn btn-primary yellow" Visible="False" />
                        <a href="#add-post-title" data-toggle="modal" title="" onclick="javascript:showpopup1();">
                            <button type="button" class="btns  yellow  lrg-btn flat-btn add_user" style="border: 0px;">
                                <%--  <%= CommonMessages.AddNewEmployee%>--%>
                                <asp:Literal ID="Literal2" runat="server"  Text="Add New MAnager" EnableViewState="false" />
                            </button>
                        </a>
                        <br />
                        <br />
                        <br />
                        <div class="chart-tab">
                            <div id="tabs-container">
                                <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg"></asp:Label>
                               
                                <asp:GridView ID="gvGrid" runat="server" AutoGenerateColumns="False" CellPadding="0"
                                    Width="100%" GridLines="None" DataKeyNames="userId" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                                    EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>' OnRowCommand="gvGrid_RowCommand"
                                    AllowPaging="True" PageSize="5" OnPageIndexChanging="gvGrid_PageIndexChanging"
                                    BackColor="White" >
                                    <HeaderStyle CssClass="aa" />
                                    <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                    <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="ID">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="aa" Style="max-width: 15px"> <%# Container.DataItemIndex + 1 %></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                Width="3%" ForeColor="White" BorderWidth="0px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="EMPLOYEE NAME">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUNamer" runat="server" Text="<%# bind('name') %>" ></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                Width="10%" ForeColor="White" BorderWidth="0px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="JOB TYPE" >
                                            <ItemTemplate>
                                                <asp:Label ID="lblUserNamer" runat="server" Text="<%# bind('jobName') %>" ></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                Width="10%" ForeColor="White" BorderWidth="0px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="DIVISION">
                                            <ItemTemplate>
                                                <asp:Label ID="lblUserNamer1" runat="server" Text="<%# bind('divName') %>" ></asp:Label>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                Width="10%" ForeColor="White" BorderWidth="0px" />
                                        </asp:TemplateField>
                                        <%--Show Password----------------------------------%>
                                        <asp:TemplateField HeaderText="PASSWORD">
                                            <ItemTemplate>
                                                <div class="vat" style="width: 30%;">
                                                    <p>
                                                        <asp:LinkButton ID="LinkButton12" runat="server" CommandName="Password" CommandArgument='<%# Eval("userId") %>'
                                                            ToolTip="View">View</asp:LinkButton>
                                                    </p>
                                                </div>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                Width="3%" ForeColor="White" BorderWidth="0px" />
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="" >
                                            <ItemTemplate>
                                                <div class="vat" style="width: 30%;">
                                                    <p>
                                                        <asp:LinkButton ID="LinkButton1" runat="server" CommandName="View" CommandArgument='<%# Eval("userId") %>'
                                                            ToolTip="View" >View</asp:LinkButton>
                                                    </p>
                                                </div>
                                                <div class="vat" style="width: 30%;">
                                                    <p>
                                                        <i class="fa fa-pencil"></i><a href="<%# String.Format("Eemployee.aspx?id={0}", Eval("userId")) %>"
                                                            title="Edit">Edit</a>
                                                    </p>
                                                </div>
                                                <div class="total" style="width: 30%;">
                                                    <p>
                                                        <i class="fa fa-trash-o"></i>
                                                        <asp:LinkButton ID="lnkBtnName" runat="server" CommandName="archive" CommandArgument='<%# Eval("userId") %>'
                                                            ToolTip="Delete" OnClientClick="return confirm('Are you sure you want to archive this record?');"
                                                            >Delete</asp:LinkButton>
                                                    </p>
                                                </div>
                                            </ItemTemplate>
                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                Width="20%" ForeColor="White" BorderWidth="0px" />
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                           
                        </div>
                    </div>
                </div>
            </div>
            <asp:LinkButton ID="lnkFake" runat="server" />
            <cc1:ModalPopupExtender ID="mpe" runat="server" PopupControlID="pnlPopup" TargetControlID="lnkFake"
                CancelControlID="btnClose" BackgroundCssClass="modalBackground" DynamicServicePath=""
                Enabled="True">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlPopup" runat="server" CssClass="modalPopup modal-dialog" >
                <div class="modal-header blue yellow-radius" style="color: #ffffff;">
                    <h4 class="modal-title">
                        Details</h4>
                    <div style="float: right; margin-top: -20px;">
                        <asp:Button ID="btnClose" runat="server" Text="×" CssClass="close" />
                    </div>
                </div>
                <div class="modal-body">
                    <table border="0" cellpadding="3" cellspacing="0">
                        <tr>
                            <td style="width: 135px">
                                <b>
                                    <%-- <%= CommonMessages.Name%>--%>
                                    <asp:Literal ID="Literal3" runat="server"  Text="Name" EnableViewState="false" />:
                                </b>
                            </td>
                            <td>
                                <asp:Label ID="lblName" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <%-- <%= CommonMessages.Address%>--%>
                                    <asp:Literal ID="Literal4" runat="server"  Text="Address" EnableViewState="false" />:
                                </b>
                            </td>
                            <td>
                                <asp:Label ID="lblAddress" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Email: </b>
                            </td>
                            <td>
                                <asp:Label ID="lblEmail" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>
                                    <%--<%= CommonMessages.PhoneNo%>--%>
                                    <asp:Literal ID="Literal5" runat="server"  Text="Phone No" EnableViewState="false" />:
                                </b>
                            </td>
                            <td>
                                <asp:Label ID="lblPhoneNo" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Division: </b>
                            </td>
                            <td>
                                <asp:Label ID="lblDepartments" runat="server"/>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <b>Job Type: </b>
                            </td>
                            <td>
                                <asp:Label ID="lblJob" runat="server"/>
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
            <asp:LinkButton ID="LinkButton2" runat="server" />
            <cc1:ModalPopupExtender ID="Modal1" runat="server" PopupControlID="Panel1" TargetControlID="LinkButton2"
                CancelControlID="Button3" BackgroundCssClass="modalBackground" DynamicServicePath=""
                Enabled="True">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup modal-dialog">
                <div class="modal-header blue yellow-radius" style="color: #ffffff;">
                    <h4 class="modal-title">
                        Password</h4>
                    <div style="float: right; margin-top: -20px;">
                        <asp:Button ID="Button3" runat="server" Text="×" CssClass="close" />
                    </div>
                </div>
                <div class="modal-body">
                    <table border="0" cellpadding="3" cellspacing="0">
                        <tr>
                            <td style="width: 135px">
                                <b>Password: </b>
                            </td>
                            <td>
                                <asp:Label ID="lblpassword" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
            </asp:Panel>
        </ContentTemplate>
        <Triggers>
        </Triggers>
    </asp:UpdatePanel>
    <%----------------------------Add new employee popup-----------------------------------------------------------------------------%>
    <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 11pt;
        }
        .modalBackground
        {
            background-color: Black;
            filter: alpha(opacity=40);
            opacity: 0.4;
        }
        .modalPopup1
        {
            background-color: #FFFFFF;
            width: 300px;
            border: 3px solid #0DA9D0;
        }
        .modalPopup
        {
            background: none repeat scroll 0 0 #ffffff;
            display: table;
            float: none;
            margin: 96px auto;
            width: 450px;
            top: 60px !important;
        }
        .modalPopup .header
        {
            background-color: #2FBDF1;
            height: 30px;
            color: White;
            line-height: 30px;
            text-align: center;
            font-weight: bold;
        }
        .modalPopup .body
        {
            min-height: 50px;
            line-height: 30px;
            text-align: center;
            padding: 5px;
        }
        .modalPopup .footer
        {
            padding: 3px;
        }
        .modalPopup .button
        {
            height: 23px;
            color: White;
            line-height: 23px;
            text-align: center;
            font-weight: bold;
            cursor: pointer;
            background-color: #9F9F9F;
            border: 1px solid #5C5C5C;
        }
        .modalPopup td
        {
            text-align: left;
        }
        
        .button
        {
            background-color: transparent;
        }
        .yellow
        {
            border-radius: 0px;
        }
    </style>
    <script type="text/javascript">
        function showpopup1() {
            document.getElementById("ContentPlaceHolder1_txtName").value = "";
            document.getElementById("ContentPlaceHolder1_txtaddress").value = "";
            document.getElementById("ContentPlaceHolder1_txtEmail").value = "";
            document.getElementById("ContentPlaceHolder1_txtMobile").value = "";
        }
         
    </script>
</asp:Content>
