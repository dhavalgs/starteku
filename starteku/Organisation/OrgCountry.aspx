﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="OrgCountry.aspx.cs" Inherits="Organisation_OrgCountry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg"></asp:Label>
    <div class="col-md-6">
        <div class="heading-sec">
            <%--<h1>--%>
            <%--  <asp:Label runat="server" ID="lblDataDisplayTitle" Text="Country"></asp:Label>--%>
            <%-- <i>Welcome to Flat Lab </i></h1>--%>
            <h1>
                Country <i>
                    <span runat="server" id="Country"></span></i></h1>
        </div>
    </div>
    <br />
    <br />
    <div class="col-md-6 range ">
        <%--<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
            <i class="fa fa-calendar-o icon-calendar icon-large"></i>
            <span>August 5, 2014 - September 3, 2014</span>  <b class="caret"></b>
        </div>--%>
    </div>
    <div class="col-md-12">
        <div class="chat-widget widget-body" style="background: #fff;">
            <div class="chat-widget-head yellow">
               
                 <h4 style="margin: -6px 0px 0px;">Country Information</h4>
            </div>
            <div class="indicatesRequireFiled">
                <i>* Indicates required field</i>
            </div>
            <div class="col-md-6">
                <div class="inline-form" style="width: 100%;">
                    <label class="c-label">
                        Country Name:</label>
                    <asp:TextBox runat="server" Text="" ID="txtcountryName" MaxLength="50" CssClass="form-control" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtcountryName"
                        ErrorMessage="Please enter first name." CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="col-xs-12 profile_bottom">
                <asp:Button runat="server" ID="btnsubmit" Text="Save" CssClass="btn blue pull-right"
                    ValidationGroup="chk" OnClick="btnsubmit_click" />
                <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="btn black pull-right"
                    ValidationGroup="chk" CausesValidation="false" OnClick="btnCancel_click" />
            </div>
        </div>
    </div>
</asp:Content>
