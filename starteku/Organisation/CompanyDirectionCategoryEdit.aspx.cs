﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using startetku.Business.Logic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Threading;

public partial class Organisation_CompanyDirectionCategoryEdit : System.Web.UI.Page
{
    public static int ResLangId = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                GetCompanyCategoryById();
            }
        }
    }

    protected void btnCancel_click(object sender, EventArgs e)
    {
        Response.Redirect("CompanyDirectionCategoryList.aspx");
    }
    protected void btnSave_click(object sender, EventArgs e)
    {

        string msg = "";
        if (!String.IsNullOrEmpty(Request.QueryString["id"]))
        {
            msg = UpdateCategory();
        }
        if (!String.IsNullOrEmpty(msg))
        {
            if (msg == "success")
            {
                lblMsg.Text = CommonModule.msgRecordInsertedSuccessfully;
                lblMsg.ForeColor = System.Drawing.Color.White;

                Response.Redirect("CompanyDirectionCategoryList.aspx");
            }
            else
            {
                lblMsg.Text = CommonModule.msgAlreadyExists;
            }
        }
        else
        {
            lblMsg.Text = CommonModule.msgSomeProblemOccure;
        }
        
    }
    public string UpdateCategory()
    {
        string returnMsg = "";
        try
        {
            DirectionCategory obj = new DirectionCategory();

            var CompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            var LoggedUserId = Convert.ToInt32(Session["OrgUserId"]);

            obj.catID = hdcatID.Value;
            obj.catName = tbcatName.Text;
            obj.catNameDN = tbcatNameDN.Text;
            obj.catDescription = tbDESCRIPTION.Text;
            obj.catIndex = Convert.ToInt32(tbsqno.Text);
            obj.catCreatedBy = LoggedUserId;
            obj.catCreateDate = DateTime.Now;
            obj.catUpdateDate = DateTime.Now;
            obj.catIsActive = true;
            obj.catCompanyID = CompanyId;
            obj.catIsDelete = false;

            obj.UpdateDirectionCategory();

            DataSet ds = obj.ds;


            LangTranslationMasterBM lobj = new LangTranslationMasterBM();
            string resLangID, LangText;
            for (int i = 0; i < gridTranslate.Rows.Count; i++)
            {
                resLangID = ((HiddenField)gridTranslate.Rows[i].Cells[0].FindControl("hdnResLangID")).Value;
                LangText = ((TextBox)gridTranslate.Rows[i].Cells[0].FindControl("txtTranValue")).Text;
                lobj.LangType = "compcat";
                lobj.LangActCatId =Convert.ToInt32(hdcatID.Value);
                lobj.ResLangID = Convert.ToInt32(resLangID);
                lobj.LangText = LangText;
                lobj.LangCompanyID = LoggedUserId;
                lobj.InsertLangResource();
            }

            returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
           
        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex);
        }

        return returnMsg;
    }
    #region Culture
    protected override void InitializeCulture()
    {
        // string language = "French";
        string language = Convert.ToString(Session["Culture"]);

        string languageId = "";

        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!String.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }

        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion

    protected void GetCompanyCategoryById()
    {
        //ddlQuesCat
        ResLangId = Convert.ToInt32(Session["resLangID"]);
        var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
        DirectionCategory obj = new DirectionCategory();
        obj.catIsActive = true;
        obj.catIsDelete = false;
        obj.catID = Convert.ToString(Request.QueryString["id"]);
        obj.LangID = ResLangId;
        obj.catCompanyID = companyId;
        obj.GetAllCompanyCategoryName();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["catID"])))
                hdcatID.Value = Convert.ToString(ds.Tables[0].Rows[0]["catID"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["catName"])))
                tbcatName.Text = Convert.ToString(ds.Tables[0].Rows[0]["catName"]);


            //if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["catNameDN"])))
            //    tbcatNameDN.Text = Convert.ToString(ds.Tables[0].Rows[0]["catNameDN"]);


            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["catDescription"])))
                tbDESCRIPTION.Text = Convert.ToString(ds.Tables[0].Rows[0]["catDescription"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["catIndex"])))
                tbsqno.Text = Convert.ToString(ds.Tables[0].Rows[0]["catIndex"]);

            Session["ActID"] = Convert.ToInt32(hdcatID.Value);

            ResourceLanguageBM obj1 = new ResourceLanguageBM();
            DataSet ds1 = new DataSet();
            obj1.LangCompanyID = Convert.ToInt32(Session["OrgUserId"]);
            obj1.LangActCatId = Convert.ToInt32(Session["ActID"]);
            obj1.LangType = "compcat";
            ds1 = obj1.GetAllResourceLangTran();

            gridTranslate.DataSource = ds1.Tables[0];
            gridTranslate.DataBind();

        }
    }
}