﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;


public partial class Organisation_OrgState : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            State.InnerHtml = "Welcome   " + Convert.ToString(Session["OrgUserName"]) + "!";
            GetAllCountry();
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                GetAllStatebyid();
            }
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region method
    protected void SetDefaultMessage()
    {

        string id = "?id=";
        string childurl = "child.aspx";
        //if (Request.RawUrl.ToString().ToLower().Contains(childurl.ToLower()))
        //{
        //    if (Request.RawUrl.ToString().ToLower().Contains(id.ToLower()))
        //    {
        //        lblDataDisplayTitle.Text = "Edit Children";

        //    }
        //    else
        //    {
        //        lblDataDisplayTitle.Text = "Add Children";
        //    }
        //}
    }
    protected void GetAllCountry()
    {
        CountryBM obj = new CountryBM();
        obj.couIsActive = true;
        obj.couIsDeleted = false;
        obj.couCompanyId = Convert.ToInt32(Session["OrgUserId"]);
        obj.GetAllCountry();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlcountry.Items.Clear();

                ddlcountry.DataSource = ds.Tables[0];
                ddlcountry.DataTextField = "couName";
                ddlcountry.DataValueField = "couId";
                ddlcountry.DataBind();

                ddlcountry.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
            }
            else
            {
                ddlcountry.Items.Clear();
                ddlcountry.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddlcountry.Items.Clear();
            ddlcountry.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
        }

    }
    protected void Insertstate()
    {
        StateBM obj2 = new StateBM();
        obj2.stateCheckDuplication(txtstateName.Text, -1);
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        {
            StateBM obj = new StateBM();
            obj.staName = txtstateName.Text;
            obj.staCountryId = Convert.ToInt32(ddlcountry.SelectedValue);
            obj.staCompanyId = Convert.ToInt32(Session["OrgUserId"]);
            obj.staIsActive = true;
            obj.staIsDeleted = false;
            obj.staCreatedDate = DateTime.Now;
            obj.InsertState();
            //  DataSet ds = obj.ds;
            if (obj.ReturnBoolean == true)
            {
                Response.Redirect("OrgStateList.aspx?msg=ins");
            }
        }
        else
        {
            if (returnMsg == "stateName")
                lblMsg.Text = CommonModule.msgNameAlreadyExists;
            else
                lblMsg.Text = CommonModule.msgProblemInsertRecord;
        }
    }
    protected void updatestate()
    {
        StateBM obj2 = new StateBM();
        obj2.stateCheckDuplication(txtstateName.Text, Convert.ToInt32(Request.QueryString["id"]));
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        {
            StateBM obj = new StateBM();
            obj.staId = Convert.ToInt32(Request.QueryString["id"]);
            obj.staName = txtstateName.Text;
            obj.staCountryId = Convert.ToInt32(ddlcountry.SelectedValue);
            obj.staUpdatedDate = DateTime.Now;
            obj.UpdateState();
            //  DataSet ds = obj.ds;
            if (obj.ReturnBoolean == true)
            {
                Response.Redirect("OrgStateList.aspx?msg=upd");
            }
        }
        else
        {
            if (returnMsg == "stateName")
                lblMsg.Text = CommonModule.msgNameAlreadyExists;
            else
                lblMsg.Text = CommonModule.msgProblemInsertRecord;
        }

    }
    protected void GetAllStatebyid()
    {
        StateBM obj = new StateBM();
        obj.staId = Convert.ToInt32(Request.QueryString["id"]);
        obj.GetAllstatebyid();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["staName"])))
                txtstateName.Text = Convert.ToString(ds.Tables[0].Rows[0]["staName"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["staCountryId"])))
                ddlcountry.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["staCountryId"]);
        }

    }
    #endregion

    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Request.QueryString["id"]))
        {
            updatestate();
        }
        else
        {
            Insertstate();

        }
    }
    protected void btnCancel_click(object sender, EventArgs e)
    {
        Response.Redirect("OrgStateList.aspx");
    }
    #endregion

}