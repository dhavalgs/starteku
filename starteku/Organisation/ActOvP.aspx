﻿<%@ Page Title="AskingCulture" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"  
    AutoEventWireup="true" CodeFile="ActOvP_.aspx.cs" Inherits="Organisation_ActOvP" %>
<%@ Reference Page="~/Organisation/ActivityLists.aspx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <style type="text/css">
        body {
            font-family: Arial;
            font-size: 10pt;
        }

        .Grid td {
            background-color: #A1DCF2;
            color: black;
            font-size: 10pt;
            line-height: 200%;
        }

        .Grid th {
            background-color: #3AC0F2;
            color: White;
            font-size: 10pt;
            line-height: 200%;
        }

        .ChildGrid td {
            background-color: #eee !important;
            color: black;
            font-size: 10pt;
            line-height: 200%;
        }

        .ChildGrid th {
            background-color: #6C6C6C !important;
            color: White;
            font-size: 10pt;
            line-height: 200%;
        }

        .doller {
            display: none;
        }
    </style>
    <script src="../Scripts/jquery_1_8_3.min.js" type="text/javascript"></script>

    <%--<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            setTimeout(function () {
                $("[src*=plus]").live("click", function () {
                    $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>");
                    $(this).attr("src", "../images/minus.png");
                });
                $("[src*=minus]").live("click", function () {
                    $(this).attr("src", "../images/plus.png");
                    $(this).closest("tr").next().remove();
                });
            }, 500);
        });
    </script>

    <link href="../Scripts/pickadate.js-3.5.6/lib/themes/classic.css" rel="stylesheet" />
    <link href="../Scripts/fullcalendor/pickADate/classic.date.css" rel="stylesheet" />

</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">


    <asp:UpdatePanel runat="server" ID="updtPnl">
        <ContentTemplate>

            <asp:HiddenField runat="server" ID="GraphSubTitle" meta:resourcekey="ResourceGraphSubTitle"></asp:HiddenField>
            <asp:HiddenField runat="server" ID="GraphAxisLevel" meta:resourcekey="ResourceGraphAxisLevel"></asp:HiddenField>
            <asp:HiddenField runat="server" ID="GraphToolTip" meta:resourcekey="ResourceGraphToolTip"></asp:HiddenField>
            <asp:HiddenField ID="Hdntaginfo" runat="Server" Value="1" />

            <div class="col-md-6">
                <div class="heading-sec">
                    <h1 style="margin-left: 15px;">

                        <asp:Literal ID="Literal2" runat="server" meta:resourcekey="ActivityOverview" EnableViewState="false" />
                        <%-- <%= CommonMessages.Competence%>--%>
                        <i><span>
                            <asp:Literal ID="Literal5" runat="server" meta:resourcekey="Welcome" EnableViewState="false" /></span><span runat="server" id="Competence"></span></i>
                    </h1>
                </div>
            </div>

            <div class="">
                <div class="container">
                    <div class="col-md-12" style="margin-top:2%">
                        <div class="heading-sec">
                            <h1>
                                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Report" EnableViewState="false" />
                                <i><span runat="server" id="Settings"></span></i>
                            </h1>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="dropdown-example" style="margin-left:4%">

                            <asp:DropDownList Width="250px" ID="ddActCate" runat="server" CssClass="chkliststyle form-control"
                                AutoPostBack="True"
                                RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description" OnSelectedIndexChanged="ddActCate_OnSelectedIndexChanged"
                                DataValueField="value" Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; color: white; overflow: auto;">
                            </asp:DropDownList>

                        </div>
                    </div>

                    <div class="col-md-8">
                        <asp:DropDownList ID="ddlTeam" runat="server" Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; color: white; overflow: auto; width: 25%; height: 32px; float: right; display: inline; margin-bottom: 20px; margin-Top: 6px; margin-right: 20px;"
                            CssClass="chkliststyle form-control"
                            AutoPostBack="True"
                            OnSelectedIndexChanged="ddActCate_OnSelectedIndexChanged">
                        </asp:DropDownList>

                        <asp:DropDownList ID="ddlDepartment" runat="server"
                            Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; color: white; overflow: auto; width: 25%; height: 32px; float: right; display: inline; margin-bottom: 20px; margin-Top: 6px; margin-right: 20px;"
                            CssClass="chkliststyle form-control"
                            AutoPostBack="True"
                            OnSelectedIndexChanged="ddActCate_OnSelectedIndexChanged">
                        </asp:DropDownList>

                        <asp:DropDownList ID="ddlJobType" runat="server" Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; color: white; overflow: auto; width: 25%; height: 32px; float: right; display: inline; margin-bottom: 20px; margin-Top: 6px; margin-right: 20px;"
                            CssClass="chkliststyle form-control"
                            AutoPostBack="True"
                            OnSelectedIndexChanged="ddActCate_OnSelectedIndexChanged">
                        </asp:DropDownList>

                    </div>


                    <div class="col-md-12" style="margin-top: 20px;">
                        <div id="graph-wrapper">
                            <div class="col-md-12">
                                <ul class="tabs-menu" >

                                    <li class="current tabs-menu1" style="width: 33.3%;">
                                        <a href="#" onclick="$('.ActivityVsActivityStatus_OnClick').click();TabVisiblity(1);" class="">
                                            <asp:Literal ID="Literal7" runat="server" Text="Activity Vs Activity status" meta:resourcekey="ActivityVsActivityStatus"
                                                EnableViewState="false" /></a>
                                        <asp:Button runat="server" OnClick="ActivityVsActivityStatus_OnClick" Text="Activity Vs Activity status" CssClass="ActivityVsActivityStatus_OnClick" Style="display: none" />
                                    </li>

                                    <li class="tabs-menu2" style="width: 33.3%;"><a href="#" onclick="$('.PersonVsPersonStatus_OnClick').click();TabVisiblity(2);" class="tab2">
                                        <asp:Literal ID="Literal8" runat="server" Text="Person Vs Activity status" meta:resourcekey="PersonVsActivityStatus" EnableViewState="false" /></a>

                                        <asp:Button ID="Button1" runat="server" OnClick="PersonVsActivityStatus_OnClick" Text="Activity Vs Activity status" CssClass="PersonVsPersonStatus_OnClick" Style="display: none" />

                                    </li>



                                    <li class="tabs-menu4" style="width: 33.3%;"><a href="#" onclick="$('.Tab4_OnClick').click();TabVisiblity(4);">
                                        <asp:Literal ID="Literal10" runat="server" Text="ActivityDetails" meta:resourcekey="ActivityDetails" EnableViewState="false" /></a>

                                        <asp:Button ID="Button4" runat="server" OnClick="Tab4_OnClick" Text="Activity Vs Activity status" CssClass="Tab4_OnClick" Style="display: none" />

                                    </li>

                                </ul>
                                <div class="chart-tab">


                                    <div id="tabs-container">
                                        <div class="tab">
                                            <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="updtPnl">
                                                <ProgressTemplate>
                                                    <div class="progress small-progress">
                                                        <div class="progress-bar blue" style="width: 0%;" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40" role="progressbar"></div>
                                                    </div>
                                                </ProgressTemplate>
                                            </asp:UpdateProgress>
                                            <div id="tab-1" class="tab-content tab1Div">

                                                <div class="col-md-4">
                                                    <div class="dropdown-example">

                                                        <asp:DropDownList Width="250px" ID="dd_Cout_Count_Tab1" runat="server" CssClass="chkliststyle form-control" AutoPostBack="True"
                                                            RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description" OnSelectedIndexChanged="dd_Cout_Count_Tab1_OnSelectedIndexChanged"
                                                            DataValueField="value" Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; color: white; overflow: auto;">

                                                            <asp:ListItem Text="Cost" Value="1" meta:resourcekey="costres"/>
                                                            <asp:ListItem Text="Count" Value="2" meta:resourcekey="countres"/>


                                                        </asp:DropDownList>




                                                    </div>
                                                </div>

                                                <div class="col-md-4">

                                                    <asp:TextBox ID="txtOvpStartDate" runat="server" placeholder="Start Date" AutoPostBack="true" OnTextChanged="txtOvpStartDate_TextChanged" CssClass="form-control txtActStartDate" meta:resourcekey="startdateres"></asp:TextBox>
                                                </div>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="txtOvpEndDate" runat="server" placeholder="End Date" OnTextChanged="txtOvpStartDate_TextChanged" CssClass="form-control txtActEndDate" meta:resourcekey="enddateres"></asp:TextBox>

                                                </div>






                                                <div>
                                                    <asp:GridView ID="gvActCat" runat="server" AutoGenerateColumns="false"
                                                         CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                                                        DataKeyNames="ActId" OnRowDataBound="OnRowDataBound"
                                                        meta:resourcekey="NoRecordFound">
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <img alt="PLUS" style="cursor: pointer" src="../images/plus.png" />
                                                                    <asp:Panel ID="pnlOrders" runat="server" Style="display: none">
                                                                        <asp:HiddenField runat="server" ID="hdnActId" Value='<%#Eval("ActId") %>' />
                                                                        <asp:GridView ID="gvPer" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover table-checkable datatable">
                                                                            <Columns>
                                                                                <asp:TemplateField meta:resourcekey="Firstname">
                                                                                    <ItemTemplate>
                                                                                        <%#Eval("userFirstName") %>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                                    <FooterTemplate>
                                                                                        <asp:Label runat="server" ID="asdf1"> Requested   </asp:Label>
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField meta:resourcekey="Requested">
                                                                                    <ItemTemplate>
                                                                                        <span class="doller">$</span>
                                                                                        <%# (Eval("RequestCount")=="" || Eval("RequestCount")==null) ? "0" : Eval("RequestCount")%>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                                    <FooterTemplate>
                                                                                        <asp:Label runat="server" ID="asdf11"> Requested   </asp:Label>
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField meta:resourcekey="Approved">
                                                                                    <ItemTemplate>
                                                                                        <span class="doller">$</span>
                                                                                        <%# (Eval("ApprovedCount")=="" || Eval("ApprovedCount")==null) ? "0" : Eval("ApprovedCount")%>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                                    <FooterTemplate>
                                                                                        <asp:Label runat="server" ID="asdf111"> Requested   </asp:Label>
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField meta:resourcekey="Ongoing">
                                                                                    <ItemTemplate>
                                                                                        <span class="doller">$</span>
                                                                                        <%# (Eval("OngoingCount")=="" || Eval("OngoingCount")==null) ? "0" : Eval("OngoingCount")%>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                                    <FooterTemplate>
                                                                                        <asp:Label runat="server" ID="asdf22"> Requested   </asp:Label>
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>

                                                                                <asp:TemplateField meta:resourcekey="Completed">
                                                                                    <ItemTemplate>
                                                                                        <span class="doller">$</span>
                                                                                        <%# (Eval("CompletedCount")=="" || Eval("CompletedCount")==null) ? "0" : Eval("CompletedCount")%>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                                    <FooterTemplate>
                                                                                        <asp:Label runat="server" ID="lblCompleted"> Requested   </asp:Label>
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>


                                                                                <asp:TemplateField meta:resourcekey="Total">
                                                                                    <ItemTemplate>
                                                                                        <span class="doller">$</span>
                                                                                        <%# (Eval("Total")=="" || Eval("Total")==null) ? "0" : Eval("Total")%>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                                    <FooterTemplate>
                                                                                        <asp:Label runat="server" ID="asdf3"> Requested   </asp:Label>
                                                                                    </FooterTemplate>
                                                                                </asp:TemplateField>
                                                                                <%--    <asp:BoundField ItemStyle-Width="150px" DataField="" HeaderText="" />--%>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </asp:Panel>
                                                                </ItemTemplate>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField meta:resourcekey="Activity">
                                                                <ItemTemplate>

                                                                    <%# (Eval("ActName")=="" || Eval("ActName")==null) ? "0" : Eval("ActName")%>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                <FooterTemplate>
                                                                    <asp:Label runat="server" ID="lblActivity"> Total   </asp:Label>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField meta:resourcekey="Requested">
                                                                <ItemTemplate>
                                                                    <span class="doller">$</span> <%# (Eval("RequestCount")=="" || Eval("RequestCount")==null) ? "0" : Eval("RequestCount")%>


                                                                    <asp:HiddenField runat="server" ID="hdnRequestCount" Value='<%#Eval("RequestCount") %>' />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                <FooterTemplate>
                                                                    <span class="doller">$</span>
                                                                    <asp:Label runat="server" ID="lblRequested"> Requested   </asp:Label>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField meta:resourcekey="Approved">
                                                                <ItemTemplate>
                                                                    <span class="doller">$</span>  <%# (Eval("ApprovedCount")=="" || Eval("ApprovedCount")==null) ? "0" : Eval("ApprovedCount")%>


                                                                    <asp:HiddenField runat="server" ID="hdnApprovedCount" Value='<%#Eval("ApprovedCount") %>' />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                <FooterTemplate>
                                                                    <span class="doller">$</span>
                                                                    <asp:Label runat="server" ID="lblApproved"> Requested   </asp:Label>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField meta:resourcekey="Ongoing">
                                                                <ItemTemplate>

                                                                    <span class="doller">$</span>    <%# (Eval("OngoingCount")=="" || Eval("OngoingCount")==null) ? "0" : Eval("OngoingCount")%>



                                                                    <asp:HiddenField runat="server" ID="hdnOngoingCount" Value='<%#Eval("OngoingCount") %>' />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                <FooterTemplate>
                                                                    <span class="doller">$</span>
                                                                    <asp:Label runat="server" ID="lblhdnOngoingCount"> Requested   </asp:Label>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>

                                                            <asp:TemplateField meta:resourcekey="Completed">
                                                                <ItemTemplate>

                                                                    <span class="doller">$</span>   <%# (Eval("CompletedCount")=="" || Eval("CompletedCount")==null) ? "0" : Eval("CompletedCount")%>

                                                                    <asp:HiddenField runat="server" ID="hdnCompletedCount" Value='<%#Eval("CompletedCount") %>' />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                <FooterTemplate>
                                                                    <span class="doller">$</span>
                                                                    <asp:Label runat="server" ID="lglhdnCompletedCount"> Requested   </asp:Label>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField meta:resourcekey="Total">
                                                                <ItemTemplate>


                                                                    <span class="doller">$</span>      <%# (Eval("Total")=="" || Eval("Total")==null) ? "0" : Eval("Total")%>
                                                                    <asp:HiddenField runat="server" ID="hdnTotal" Value='<%#Eval("Total") %>' />
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                <FooterTemplate>
                                                                    <span class="doller">$</span>
                                                                    <asp:Label runat="server" ID="lblTotalCount"> Requested   </asp:Label>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>



                                                            <%-- <asp:BoundField ItemStyle-Width="150px" DataField="" HeaderText="City" />--%>
                                                        </Columns>
                                                    </asp:GridView>


                                                </div>


                                            </div>





                                            <div id="tab-2" class="tab-content tab2Div">
                                                <div class="col-md-4">
                                                    <asp:DropDownList Width="250px" ID="dd_Cout_Count_Tab2" runat="server" CssClass="chkliststyle form-control" AutoPostBack="True"
                                                        RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description" OnSelectedIndexChanged="dd_Cout_Count_Tab2_OnSelectedIndexChanged"
                                                        DataValueField="value" Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; color: white; overflow: auto;">

                                                        <asp:ListItem Text="Cost" Value="1" meta:resourcekey="costres1" />
                                                        <asp:ListItem Text="Count" Value="2" meta:resourcekey="countres1"/>







                                                    </asp:DropDownList>
                                                </div>
                                                <div class="col-md-4">

                                                    <asp:TextBox ID="txtOvpStartDateTab2" runat="server" placeholder="Start Date" AutoPostBack="true" OnTextChanged="txtOvpStartDate_TextChanged" CssClass="form-control txtActStartDate" meta:resourcekey="startdateres1"></asp:TextBox>
                                                </div>
                                                <div class="col-md-4">
                                                    <asp:TextBox ID="txtOvpEndDateTab2" runat="server" placeholder="End Date" OnTextChanged="txtOvpStartDate_TextChanged" CssClass="form-control txtActEndDate" meta:resourcekey="enddateres1"></asp:TextBox>

                                                </div>










                                                <asp:GridView ID="gv_Person_Master" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                                                    DataKeyNames="userId" OnRowDataBound="gv_Person_Master_OnRowDataBound"
                                                    meta:resourcekey="NoRecordFound">
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <img alt="PLUS" style="cursor: pointer" src="../images/plus.png" />
                                                                <asp:Panel ID="pnlOrders" runat="server" Style="display: none">
                                                                    <asp:HiddenField runat="server" ID="hdnActId" Value='<%#Eval("ActReqActId") %>' />
                                                                    <asp:GridView ID="gv_Person_Child" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover table-checkable datatable">
                                                                        <Columns>
                                                                            <asp:TemplateField meta:resourcekey="Person">
                                                                                <ItemTemplate>

                                                                                    <span class="doller">$</span> 
                                                                                    <%# (Eval("actName")=="" || Eval("actName")==null) ? "0" : Eval("actName")%>
                                                                                </ItemTemplate>
                                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                    Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                                <FooterTemplate>
                                                                                    <asp:Label runat="server" ID="asdf4"> Requested 1  </asp:Label>
                                                                                </FooterTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField meta:resourcekey="Requested">
                                                                                <ItemTemplate>


                                                                                    <span class="doller">$</span>  <%# (Eval("RequestCount")=="" || Eval("RequestCount")==null) ? "0" : Eval("RequestCount")%>
                                                                                </ItemTemplate>
                                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                    Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                                <FooterTemplate>
                                                                                    <asp:Label runat="server" ID="asdf5"> Requested   </asp:Label>
                                                                                </FooterTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField meta:resourcekey="Approved">
                                                                                <ItemTemplate>


                                                                                    <span class="doller">$</span>   <%# (Eval("ApprovedCount")=="" || Eval("ApprovedCount")==null) ? "0" : Eval("ApprovedCount")%>
                                                                                </ItemTemplate>
                                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                    Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                                <FooterTemplate>
                                                                                    <asp:Label runat="server" ID="asdf6"> Requested   </asp:Label>
                                                                                </FooterTemplate>
                                                                            </asp:TemplateField>

                                                                            <asp:TemplateField meta:resourcekey="Ongoing">
                                                                                <ItemTemplate>


                                                                                    <span class="doller">$</span>   <%# (Eval("OngoingCount")=="" || Eval("OngoingCount")==null) ? "0" : Eval("OngoingCount")%>
                                                                                </ItemTemplate>
                                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                    Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                                <FooterTemplate>
                                                                                    <asp:Label runat="server" ID="asdf7"> OngoingCount   </asp:Label>
                                                                                </FooterTemplate>
                                                                            </asp:TemplateField>


                                                                            <asp:TemplateField meta:resourcekey="Completed">
                                                                                <ItemTemplate>


                                                                                    <span class="doller">$</span> <%# (Eval("CompletedCount")=="" || Eval("CompletedCount")==null) ? "0" : Eval("CompletedCount")%>
                                                                                </ItemTemplate>
                                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                    Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                                <FooterTemplate>
                                                                                    <asp:Label runat="server" ID="asdf8"> Requested   </asp:Label>
                                                                                </FooterTemplate>
                                                                            </asp:TemplateField>


                                                                            <asp:TemplateField meta:resourcekey="Total">
                                                                                <ItemTemplate>


                                                                                    <span class="doller">$</span>  <%# (Eval("Total")=="" || Eval("Total")==null) ? "0" : Eval("Total")%>
                                                                                </ItemTemplate>
                                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                    Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                                <FooterTemplate>
                                                                                    <asp:Label runat="server" ID="asdf9"> Requested   </asp:Label>
                                                                                </FooterTemplate>
                                                                            </asp:TemplateField>
                                                                            <%--    <asp:BoundField ItemStyle-Width="150px" DataField="" HeaderText="" />--%>
                                                                        </Columns>
                                                                    </asp:GridView>
                                                                </asp:Panel>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField meta:resourcekey="Person">
                                                            <ItemTemplate>

                                                                <%# (Eval("userFirstName")=="" || Eval("userFirstName")==null) ? "0" : Eval("userFirstName")%>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                            <FooterTemplate>
                                                                <asp:Label runat="server" ID="asdf10"> Total   </asp:Label>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField meta:resourcekey="Requested">
                                                            <ItemTemplate>

                                                                <%# (Eval("RequestCount")=="" || Eval("RequestCount")==null) ? "0" : Eval("RequestCount")%>

                                                                <asp:HiddenField runat="server" ID="hdnRequestCountp" Value='<%#Eval("RequestCount") %>' />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                            <FooterTemplate>
                                                                <span class="doller">$</span>
                                                                <asp:Label runat="server" ID="lbRequestCountp"> Requested   </asp:Label>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField meta:resourcekey="Approved">
                                                            <ItemTemplate>

                                                                <%# (Eval("ApprovedCount")=="" || Eval("ApprovedCount")==null) ? "0" : Eval("ApprovedCount")%>

                                                                <asp:HiddenField runat="server" ID="hdnApprovedCountp" Value='<%#Eval("ApprovedCount") %>' />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                            <FooterTemplate>
                                                                <span class="doller">$</span>
                                                                <asp:Label runat="server" ID="lblApprovedCountp"> Requested   </asp:Label>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField meta:resourcekey="Ongoing">
                                                            <ItemTemplate>


                                                                <%# (Eval("OngoingCount")=="" || Eval("OngoingCount")==null) ? "0" : Eval("OngoingCount")%>
                                                                <asp:HiddenField runat="server" ID="hdnOngoingCountp" Value='<%#Eval("OngoingCount") %>' />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                            <FooterTemplate>
                                                                <span class="doller">$</span>
                                                                <asp:Label runat="server" ID="lblOngoingCountp"> Requested   </asp:Label>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>



                                                        <asp:TemplateField meta:resourcekey="Completed">
                                                            <ItemTemplate>


                                                                <%# (Eval("CompletedCount")=="" || Eval("CompletedCount")==null) ? "0" : Eval("CompletedCount")%>
                                                                <asp:HiddenField runat="server" ID="hdnCompletedCountp" Value='<%#Eval("CompletedCount") %>' />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                            <FooterTemplate>
                                                                <span class="doller">$</span>
                                                                <asp:Label runat="server" ID="lblCompletedCountp"> Requested   </asp:Label>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField meta:resourcekey="Total">
                                                            <ItemTemplate>


                                                                <%# (Eval("Total")=="" || Eval("Total")==null) ? "0" : Eval("Total")%>

                                                                <asp:HiddenField runat="server" ID="hdnTotalp" Value='<%#Eval("Total") %>' />
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                            <FooterTemplate>
                                                                <span class="doller">$</span>
                                                                <asp:Label runat="server" ID="lblTotalp"> Requested   </asp:Label>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>



                                                        <%-- <asp:BoundField ItemStyle-Width="150px" DataField="" HeaderText="City" />--%>
                                                    </Columns>
                                                </asp:GridView>




                                            </div>




                                            <div id="tab-3" class="tab-content tab3Div">
                                                <p>
                                                    <asp:Literal ID="Developmentpoints" runat="server" meta:resourcekey="Developmentpoints"
                                                        EnableViewState="false" />
                                                    <asp:Button ID="btn" runat="server" CommandArgument="Developmentpoints" CommandName="Developmentpoints"
                                                        Text="Export to PDF" CssClass="btns  yellow  col-md-11 libray_btn" Style="width: 130px; font-size: 15px; display: none;" />
                                                </p>
                                                <%-- <asp:Literal ID="Developmentpoints" runat="server" meta:resourcekey="Developmentpoints"
                                            EnableViewState="false" />--%>
                                                <div class="progress small-progress">
                                                    <div style="width: 40%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40"
                                                        role="progressbar" class="progress-bar blue">
                                                    </div>
                                                </div>

                                                <div class="home_grap">
                                                    <div id="ContainerDevelopment" style="min-width: 310px; height: 400px; margin: 0 auto">
                                                        <div id="waitDevelopment" style="margin: 190px 612px">
                                                            <img src="../images/wait.gif" />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="tab-4" class="tab-content tab4Div ">

                                                <p>
                                                    <asp:Literal ID="Knowledgeshare" runat="server" meta:resourcekey="Knowledgeshare"
                                                        EnableViewState="false" />

                                                </p>
                                                <%-- <asp:Literal ID="Knowledgeshare" runat="server" meta:resourcekey="Knowledgeshare"
                                            EnableViewState="false" />--%>

                                                <div class="col-md-12 comp_table">
                                                    <asp:GridView ID="gvGrid_ActivityStatus_Tab4" runat="server" AutoGenerateColumns="False" CellPadding="0"
                                                        Width="100%" GridLines="None" DataKeyNames="ActId" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                                                          EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'
                                                        BackColor="White"
                                                        meta:resourcekey="No_Record_Found"
                                                        OnRowCommand="gvGrid_RowCommand"
                                                        OnRowDataBound="gvGrid_RowDataBound"
                                                        OnRowCreated="gvGrid_RowCreated">


                                                        <HeaderStyle CssClass="aa" />
                                                        <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                                        <AlternatingRowStyle BackColor="White" />
                                                        <Columns>
                                                            <asp:TemplateField meta:resourcekey="SrNo">
                                                                <ItemTemplate>
                                                                    <%# Container.DataItemIndex + 1 %>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="3%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />


                                                            </asp:TemplateField>

                                                            <asp:TemplateField meta:resourcekey="Creator">
                                                                <ItemTemplate>
                                                                    <%#Eval("CreaterFullName") %>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                <FooterTemplate>
                                                                    <%-- <asp:Label runat="server" ID="asdf">--%>
                                                                    <%-- <asp:Literal ID="Literal17" runat="server" meta:resourcekey="TotalCount" EnableViewState="false" /></asp:Label>--%>
                                                                </FooterTemplate>
                                                            </asp:TemplateField>


                                                            <asp:TemplateField meta:resourcekey="Assigned">
                                                                <ItemTemplate>
                                                                    <%#Eval("assignFullName") %>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField meta:resourcekey="ActivityName">
                                                                <ItemTemplate>
                                                                    <%#Eval("ActName") %>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField meta:resourcekey="ActivityType">
                                                                <ItemTemplate>

                                                                    <%#Eval("ActCatName") %>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField meta:resourcekey="CompetenceDev">
                                                                <ItemTemplate>
                                                                    
                                                                   
                                                                     <%# (Boolean.Parse(Eval("ActCatDeveplan").ToString())) ? "Yes" : "No" %>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField meta:resourcekey="Priority">
                                                                <ItemTemplate>
                                                                    <asp:HiddenField runat="server" ID="hdnActPriority" Value='<%#Eval("ActPriority") %>' />
                                                                    <asp:Label runat="server" ID="lblActPriority"></asp:Label>

                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField meta:resourcekey="CreatedDate">
                                                                <ItemTemplate>

                                                                    <%# (Eval("ActCreatedDate")==null)? "--" :  Convert.ToDateTime(Eval("ActCreatedDate")).ToString("dd-MMM-yyyy")%>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                            </asp:TemplateField>




                                                            <asp:TemplateField meta:resourcekey="Status">
                                                                <ItemTemplate>



                                                                    <asp:HiddenField runat="server" ID="hdnActReqStatus" Value='<%#Eval("ActReqStatus") %>' />
                                                                    <asp:Label runat="server" ID="lblActReqStatus"></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                            </asp:TemplateField>




                                                        </Columns>
                                                        <FooterStyle BackColor="#cccccc" Font-Bold="True" ForeColor="Black" HorizontalAlign="Left" />
                                                    </asp:GridView>
                                                </div>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--  <h3 class="custom-heading darkblue">Original competence level</h3>
				 <div class="visitor-stats widget-body blue">
					
					<div class="home_grap"><div class="graph-info">
								
								 <a href="#" id="bars"><span><i class="fa fa-bar-chart-o"></i></span></a>
								 <a href="#" id="lines" class="active"><span><i class="fa fa-code-fork"></i></span></a>
							 </div>
							 <div class="graph-container">
							
								 <div id="graph-lines"></div>
								 <div id="graph-bars"></div>
							 </div></div>
							 <div class="col-md-12" style="background:#f4f4f4; padding-left:0px !important;" >
							 <div class="col-md-1 profile-details" style="padding-left:0px !important;" >
							 
							 
							 </div>
					
					
				 </div>
					 -->
                        </div>
                    </div>
                    <div style="clear: both">
                    </div>
                    <!-- TIME LINE -->
                </div>
            </div>
            <!-- Chat Widget -->
            <!-- Recent Post -->
            <div class="col-md-3">
            </div>
            <!-- Twitter Widget -->
            <!-- Weather Widget -->
            </div>
    <!-- Container -->
            </div><!-- Wrapper -->
            <!-- RAIn ANIMATED ICON-->
            <%--Chart--%>
        </ContentTemplate>
    </asp:UpdatePanel>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.2/jquery.min.js" type="text/javascript"></script>
    <script src="../Scripts/Scripts/exporting.js" type="text/javascript"></script>
    <script src="../Scripts/pickadate.js-3.5.6/lib/legacy.js" type="text/javascript"></script>
    <script src="../Scripts/pickadate.js-3.5.6/lib/picker.js" type="text/javascript"></script>
    <script src="../Scripts/pickadate.js-3.5.6/lib/picker.date.js" type="text/javascript"></script>
    <script src="../Scripts/pickadate.js-3.5.6/lib/picker.time.js" type="text/javascript"></script>
    <script src="../Scripts/highcharts.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/chart-line-and-graph.js"></script>
    <script type="text/javascript">
        var icons = new Skycons();
        icons.set("rain", Skycons.RAIN);
        icons.play();

        $(document).ready(function () {

        }, 500);
    </script>


    <script type="text/javascript">
        $(document).ready(function () {


            $('.doller').hide();

            setTimeout(function () {
                SetExpandCollapse();
            }, 500);

            ActivateDateTime();
        });


        function ActivateDateTime() {
            var yesterday = new Date((new Date()).valueOf() - 1000 * 60 * 60 * 24);
            $(".txtActStartDate").pickadate({
                format: 'dddd, dd mmm, yyyy'
            });


            $(".txtActEndDate").pickadate({
                format: 'dddd, dd mmm, yyyy'
            });


            ///


            $('.txtActStartDate').on('change', function () {

                $('.txtActEndDate').pickadate('picker').set('min', $(this).val());

            });

        }

    </script>



    <script type="text/javascript">

        function ChangeColor(a) {
            $("#" + a).parent().css('background', 'ActiveBorder');
        }

        function TabVisiblity(tabNo) {
            $("#ContentPlaceHolder1_Hdntaginfo").val(tabNo);

            $(".tab-content").hide();
            $('li').removeClass("current");


            if (tabNo == 2) {
                // Tab2Visible(tabNo);
            }
            //if (tabNo == 3) {


            //    $(".tabs-menu2").addClass("current");
            //}
            $(".tab" + tabNo + "Div").show();
            $(".tabs-menu" + tabNo).addClass("current");

            ProgressBarr(5);
            ActivateDateTime();

        }


        function Tab2Visible(tabName) {
            $('li').removeClass("current");
            $(".tab-content").hide();
            $(".tab2Div").show();
            $(".tabs-menu2").addClass("current");
            setTimeout(function () {

                $('li').removeClass("current");
                $(".tab-content").hide();
                $(".tab2Div").show();
                $(".tabs-menu2").addClass("current");
            }, 200);

            $('li').removeClass("current");
            $(".tab-content").hide();
            $(".tab2Div").show();
            $(".tabs-menu2").addClass("current");
            setTimeout(function () {

                $('li').removeClass("current");
                $(".tab-content").hide();
                $(".tab2Div").show();
                $(".tabs-menu2").addClass("current");
            }, 400);
        }


        var subtext = $('#<%=GraphSubTitle.ClientID %>').val();
        var AxisText = $('#<%=GraphAxisLevel.ClientID %>').val();
        ToolTipText = $('#<%=GraphToolTip.ClientID %>').val();

        if (subtext == "") {
            subtext = "Click and drag mouse arrow to zoom chart";
        }
        if (text == "") {
            text = $("#ddLabel").text();
            if (text == "") {
                text = "Original Competence Level";
            }
        }
        if (AxisText == "") {

            text = "Level";
        }
        if (ToolTipText == "") {

            ToolTipText = "Level";
        }


        $(".highcharts-title").text(text);
        $(".highcharts-subtitle").text(subtext);
        $(".highcharts-yaxis-title").text(AxisText);
        $(".spanText").text(ToolTipText);





        $(document).ready(function () {
            $('.doller').hide();
            SetExpandCollapse();
            setTimeout(function () {

                var subtext = $('#<%=GraphSubTitle.ClientID %>').val();
                var AxisText = $('#<%=GraphAxisLevel.ClientID %>').val();
                ToolTipText = $('#<%=GraphToolTip.ClientID %>').val();

                if (subtext == "") {
                    subtext = "Click and drag mouse arrow to zoom chart";
                }
                if (text == "") {
                    text = $("#ddLabel").text();
                    if (text == "") {
                        text = "Original Competence Level";
                    }
                }
                if (AxisText == "") {

                    text = "Level";
                }
                if (ToolTipText == "") {

                    ToolTipText = "Level";
                }

                $(".highcharts-title").text(text);
                $(".highcharts-subtitle").text(subtext);
                $(".highcharts-yaxis-title").text(AxisText);
                $(".spanText").text(ToolTipText);
                $('.doller').hide();
            }, 3000);
        });

    </script>
    <script>
        var totalLevel;
        var dataLP;
        function SubmitBtn1() {

            GetPageName();
            var ReturnDataLP;
            $.ajax({
                type: "POST",
                cache: false,
                async: true,
                url: "ReportALL.aspx/BtnSubmitAJAXLP",
                data: '{txtName:"' + '' + '",txtDesc:"' + '' + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    dataLP = response.d;

                    Developmentcomnames1();
                    setTimeout(function () {


                        //$("#waitLearninipoint").fadeOut();

                    }, 500);


                },
                failure: function (msg) {

                    alert(msg);
                    ReturnDataLP = msg;
                }
            });

        }

        function Developmentcomnames1() {

            var ReturnDataLP;
            $.ajax({
                type: "POST",
                cache: false,
                async: true,
                url: "ReportALL.aspx/DevelopmentcomnamesLP",
                data: '{txtName:"' + '' + '",txtDesc:"' + '' + '"}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {

                    totalLevel = response.d;
                    return totalLevel;

                },
                failure: function (msg) {

                    alert(msg);
                    ReturnDataLP = msg;
                }
            });

        }

        var iCount = 10;
        function ProgressBarr(curVal) {
            iCount = curVal;
            $(".progress").show("slow");
            setInterval(function () {
                iCount = iCount + 5;
                $(".progress-bar").width(iCount + "%");
                if (iCount > 110) {
                    iCount = 0;
                    $(".progress").hide("slow");
                }
            }, 1200);

        }


        function drwaLearningpointGraph() {
            console.log(totalLevel);
            console.log(dataLP);

            $("#waitLearninipoint").fadeOut();
            $(function () {
                $('#ContainerLearninipoint').highcharts({
                    chart: {
                        type: 'column',
                        zoomType: 'x'
                    },
                    title: {
                        text: 'Graph'
                    },
                    subtitle: {
                        text: 'Click and drag mouse arrow to zoom chart'
                    },

                    xAxis: {
                        categories: totalLevel

                    },
                    yAxis: {

                        min: 0,
                        title: {
                            text: 'Level'
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
                        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                            //'<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
                            '<td style="padding:0"><b>{point.y:.1f} <span class="spanText">Level</span></b></td></tr>',
                        footerFormat: '</table>',
                        useHTML: true
                    },
                    plotOptions: {
                        column: {
                            pointPadding: 0.2,
                            borderWidth: 0
                        }
                    },
                    series: dataLP
                });
            });
        }





    </script>




</asp:Content>

