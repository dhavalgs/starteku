﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;

public partial class Organisation_OrgCountry : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            Country.InnerHtml = "Welcome   " + Convert.ToString(Session["OrgUserName"]) + "!";
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                GetAllCountrybyid();
            }
        }
    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    #endregion

    #region method
    protected void SetDefaultMessage()
    {

        string id = "?id=";
        string childurl = "child.aspx";
        if (Request.RawUrl.ToString().ToLower().Contains(childurl.ToLower()))
        {
            if (Request.RawUrl.ToString().ToLower().Contains(id.ToLower()))
            {
                //lblDataDisplayTitle.Text = "Edit Children";

            }
            else
            {
                //lblDataDisplayTitle.Text = "Add Children";
            }
        }
    }
    protected void InsertCountry()
    {
        CountryBM obj2 = new CountryBM();
        obj2.CountryCheckDuplication(txtcountryName.Text, -1);
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        {

            CountryBM obj = new CountryBM();
            obj.couName = txtcountryName.Text;
            obj.couCompanyId = Convert.ToInt32(Session["OrgUserId"]);
            obj.couIsActive = true;
            obj.couIsDeleted = false;
            obj.couCreatedDate = DateTime.Now;
            obj.InsertCountry();
            //  DataSet ds = obj.ds;
            if (obj.ReturnBoolean == true)
            {
                Response.Redirect("OrgCountryList.aspx");
            }
        }
        else
        {
            if (returnMsg == "CountryName")
                lblMsg.Text = CommonModule.msgNameAlreadyExists;
            else
                lblMsg.Text = CommonModule.msgProblemInsertRecord;

        }

    }
    protected void updateCountry()
    {
        CountryBM obj2 = new CountryBM();
        obj2.CountryCheckDuplication(txtcountryName.Text, Convert.ToInt32(Request.QueryString["id"]));
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        {
            CountryBM obj = new CountryBM();
            obj.couId = Convert.ToInt32(Request.QueryString["id"]);
            obj.couName = txtcountryName.Text;
            obj.couUpdatedDate = DateTime.Now;
            obj.UpdateCountry();
            //  DataSet ds = obj.ds;
            if (obj.ReturnBoolean == true)
            {
                Response.Redirect("OrgCountryList.aspx");
            }
        }
        else
        {
            if (returnMsg == "CountryName")
                lblMsg.Text = CommonModule.msgNameAlreadyExists;
            else
                lblMsg.Text = CommonModule.msgProblemInsertRecord;
        }
    }
    protected void GetAllCountrybyid()
    {
        CountryBM obj = new CountryBM();
        obj.couId = Convert.ToInt32(Request.QueryString["id"]);
        obj.GetAllCountrybyid();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["couName"])))
                txtcountryName.Text = Convert.ToString(ds.Tables[0].Rows[0]["couName"]);
        }

    }

    #endregion

    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Request.QueryString["id"]))
        {
            updateCountry();
        }
        else
        {
            InsertCountry();
        }
    }
    protected void btnCancel_click(object sender, EventArgs e)
    {
        Response.Redirect("OrgCountryList.aspx");
    }
    #endregion
}