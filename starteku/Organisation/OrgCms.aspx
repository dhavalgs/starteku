﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master" AutoEventWireup="true" CodeFile="OrgCms.aspx.cs" Inherits="Organisation_OrgCms" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript" src="../Scripts/ckeditor/ckeditor.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg"></asp:Label>
    <div class="col-md-6">
        <div class="heading-sec">
            <h1>CMS <i><span runat="server" id="CMS"></span> </i></h1>
        </div>
    </div>
     <br /><br />
    <div class="col-md-6 range ">
        <%--<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
            <i class="fa fa-calendar-o icon-calendar icon-large"></i>
            <span>August 5, 2014 - September 3, 2014</span>  <b class="caret"></b>
        </div>--%>
    </div>
    <div class="col-md-12">
        <div class="chat-widget widget-body" style="background: #fff;">
            <div class="chat-widget-head yellow">
                 <h4 style="margin: -6px 0px 0px;">CMS Information</h4>
            </div>
            <asp:Label runat="server" ID="lblDataDisplayTitle"></asp:Label>
            <div class="indicatesRequireFiled">
                <i>* Indicates required field</i>
            </div>
            <asp:HiddenField ID="hfTagId" Value="0" runat="server" />
            <div class="col-md-6" style="width: 80%;">
                <div class="inline-form" >
                    <label class="c-label" style="width: 100%;">Name:</label>
                    <asp:TextBox runat="server" Text="" ID="txtName" onkeyup="run(this)" MaxLength="80"
                        CssClass="form-control" />
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="UppercaseLetters,LowercaseLetters,Numbers,Custom"
                        TargetControlID="txtName">
                    </cc1:FilteredTextBoxExtender>
                    <br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtName"
                        ErrorMessage="Please enter Name." CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk"></asp:RequiredFieldValidator>
                </div>

                <label class="c-label" style="width: 100%;">Description:</label>
                <div class="inline-form" >
                    
                    <asp:TextBox runat="server" Text="" ID="txtDescription" TextMode="MultiLine" Height="300"
                        MaxLength="500" CssClass="ckeditor" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDescription"
                        ErrorMessage="Please enter Description." CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk"></asp:RequiredFieldValidator>
                </div>



            </div>
            <div class="col-xs-12 profile_bottom">
                <asp:Button runat="server" ID="btnsubmit" Text="Save" CssClass="btn blue pull-right"
                    ValidationGroup="chk" OnClick="btnsubmit_click" />
                <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="btn black pull-right" ValidationGroup="chk"
                    CausesValidation="false" OnClick="btnCancel_click" />
            </div>
        </div>
    </div>
</asp:Content>

