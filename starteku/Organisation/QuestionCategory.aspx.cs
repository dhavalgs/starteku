﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using starteku_BusinessLogic.Model;
using startetku.Business.Logic;
using System.Threading;
using System.Web.Services;

public partial class Organisation_QuestionCategory : System.Web.UI.Page
{
    public void SetPublicVaribale()
    {
        CompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        LoggedUserId = Convert.ToInt32(Session["OrgUserId"]);
        OrguserType = Convert.ToInt32(Session["OrguserType"]);

    }

    public static int LoggedUserId { get; set; }
    public static int OrguserType { get; set; }
    public static int ResLangId = 0;
    //public int ResLangId = 0;
    private static int CompanyId = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        ResLangId = Convert.ToInt32(Session["resLangID"]);
        GetAllQuestionCategory();
    }
    protected void GetAllQuestionCategory()
    {
        //ddlQuesCat
        QuestionBM obj = new QuestionBM();
        obj.queIsActive = true;
        obj.queIsDeleted = false;
        obj.quesCatID = 0;

        obj.GetAllQuestionCategoryList();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvGrid_QuestionCat.DataSource = ds.Tables[0];
                gvGrid_QuestionCat.DataBind();

                gvGrid_QuestionCat.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvGrid_QuestionCat.DataSource = null;
                gvGrid_QuestionCat.DataBind();
            }
        }
        else
        {
            gvGrid_QuestionCat.DataSource = null;
            gvGrid_QuestionCat.DataBind();
        }

    }
    #region Culture
    protected override void InitializeCulture()
    {
        // string language = "French";
        string language = Convert.ToString(Session["Culture"]);

        string languageId = "";

        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }

        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion

    protected void OpenCreateQuestionCat(object sender, EventArgs e)
    {
        ClientScript.RegisterStartupScript(this.GetType(), "ModelPopup", "$('#ContentPlaceHolder1_lblQuestionCat').click();", true);
        ClearData();
    }

    protected void ClearData()
    {
        txtqcatName.Text = "";
        txtqcatNameDN.Text = "";
        txtqcatDescription.Text = "";
    }
    #region Method
    protected void CreateQuestionCategory(object sender, EventArgs e)
    {
        CreateQuestionCatMethod();
        GetAllQuestionCategory();
    }
    protected void CreateQuestionCatMethod()
    {
        try
        {
            // check Duplicate
            var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
            var userId = Convert.ToInt32(Session["OrgUserId"]);
            var logginUserType = Convert.ToInt32(Session["OrguserType"]);
            QuestionBM obj = new QuestionBM();

            obj.quesCategoryName = txtqcatName.Text;
            obj.quesCategoryNameDN = txtqcatNameDN.Text;
            obj.quesDescription = txtqcatDescription.Text;

            obj.CreatedByID = userId;
            obj.queCompanyId = companyId;

            obj.queIsActive = true;
            obj.queIsDeleted = false;
            obj.queCreatedDate = DateTime.Now;
            obj.InsertQuestionCategoryData();
            DataSet ds = obj.ds;

        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex);
        }
    }
    #endregion
    #region Grid Method
    protected void gvGrid_QuestionCat_RowDataBound(object sender, GridViewRowEventArgs e)
    {

    }

    protected void gvGrid_QuestionCat_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "archive")
        {
            QuestionBM obj = new QuestionBM();
            obj.quesCatID = Convert.ToInt32(e.CommandArgument);
            obj.queIsActive = false;
            obj.queIsDeleted = true;
            obj.QuestionCategoryStatusUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    GetAllQuestionCategory();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
    }
    #endregion
}