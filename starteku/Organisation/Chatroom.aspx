﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="Chatroom.aspx.cs" Inherits="Organisation_Chatroom"
    Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <script type="text/javascript">
        var a;
        $(window).load(function () {
            $('#scrollbox6').scrollTop($('#scrollbox6').height());

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="col-md-6">
        <div class="heading-sec">
            <h1>
              <asp:Literal ID="Literal5" runat="server" meta:resourcekey="ChatRooms" EnableViewState="false" /><i><span runat="server" id="Conatct"></span></i>
            </h1>
        </div>
    </div>
    <div style="height: 47px">
    </div>
    <asp:UpdatePanel runat="server" ID="UpdatePanellist" UpdateMode="Conditional">
        <ContentTemplate>
            <table style="width: 100%;">
                <tr>
                    <td style="width: 70%;">
                        <div class="chat-widget widget-body" style="width: 100%;height: 547px;">
                            <div class="chat-widget-head yellow yellow-radius">
                                <h4 style="margin: -5px 0px 0px;">
                                    <span runat="server" id="chat" />
                                </h4>
                                <div class="add-btn">
                                </div>
                            </div>
                            <asp:UpdatePanel runat="server" ID="timerUpdate">
                                <ContentTemplate>
                                    <asp:Timer runat="server" Enabled="True" OnTick="OnTick_RefreshChatList" ID="chatrefreshTick"
                                        Interval="5000">
                                    </asp:Timer>
                                    <ul id="scrollbox6">
                                        <asp:Repeater ID="rpt_chat" runat="server" OnItemDataBound="rpt_chat_ItemDataBound">
                                            <ItemTemplate>
                                                <li runat="server" id="listItem" class="reply">
                                                    <div class="chat-thumb">
                                                        <img src='../Log/upload/Userimage/<%# Eval("img1") %>' alt="img" onerror='../Organisation/images/sign-in.jpg'/>
                                                    </div>
                                                    <div class="chat-desc">
                                                        <p>
                                                            <%#Eval("messubject")%>
                                                            <asp:HiddenField ID="mesfromUserId" runat="server" Value="<%# bind('mesfromUserId') %>" />
                                                        </p>
                                                        <i class="chat-time">
                                                            <%# Eval("masCreatedDate") %></i>
                                                    </div>
                                                </li>
                                            </ItemTemplate>
                                        </asp:Repeater>
                                    </ul>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <div class="reply-sec" style="margin-left: 20px;">
                                <asp:TextBox ID="txtmessage" runat="server" placeholder="Type Your Message Here"
                                    Style="border: 1px solid #c6c6c6; border-radius: 2px; float: left; 
                                    height: 37px; letter-spacing: 0.3px; margin: 10px 0; padding: 0 10px; width: 85%;"
                                    meta:resourcekey="txtmessageResource1"></asp:TextBox>
                                <asp:LinkButton ID="lnkCatName" runat="server" CssClass="fa fa-comments-o black"
                                    Style="font-size: 22px; height: 37px; line-height: 33px; margin: 10px 0 10px 10px;
                                    text-align: center; width: 52px; -webkit-border-radius: 2px; -moz-border-radius: 2px;
                                    -ms-border-radius: 2px; -o-border-radius: 2px; border-radius: 2px;" OnClick="Button1_Click"
                                    ValidationGroup="chk" OnClientClick="highlightOnSend();" meta:resourcekey="lnkCatNameResource1"></asp:LinkButton><br />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtmessage"
                                    ErrorMessage="Please Enter Messge." CssClass="commonerrormsg" Display="Dynamic"
                                    ValidationGroup="chk" Style="float: left;margin-top: -69px !important;" meta:resourcekey="RequiredFieldValidator5Resource1"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </td>
                    <td style="width: 70%;">
                        <div class="chat-widget widget-body" style="height: 547px; width: 300px;">
                            <div class="chat-widget-head yellow yellow-radius">
                                <h4 style="margin: -5px 0px 0px;">
                                    Contacts</h4>
                                <div class="add-btn">
                                </div>
                            </div>
                            <ul id="scrollbox6">
                                <asp:Repeater ID="lst_contacts" runat="server" OnItemCommand="lst_contacts_OnItemCommand">
                                    <ItemTemplate>
                                        <li style="margin-bottom: 0px;" id="li" runat="server">
                                            <label style="width: 100%;">
                                                <asp:LinkButton ID="lb_contact" runat="server" Text='<%# Eval("name") %>' Style="color: Black;
                                                    font-weight: normal" CommandName="Message" CommandArgument='<%# Eval("userId") %>'
                                                    OnClientClick="HighlighThis();scrollBottom();" meta:resourcekey="lb_contactResource1"></asp:LinkButton>
                                            </label>
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </div>
                    </td>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%-- <script type="text/jscript">
        function switchColors(element) {
            debugger;
            links = document.getElementsByTagName("li"); 
        for (var i = 0; i < links.length; i++)
         links.item(i).style.color = 'red';
      element.style.color='orange'
    ; }
    

    </script>--%>
    <style type="text/css">
        .highlightContactName
        {
            color: green !important; /*  font-style: oblique;*/
        }
        #scrollbox6 li a:hover
        {
            /*font-size: 25px;*/
            color: green !important;
        }
    </style>
    <script src="js/jquery.js" type="text/javascript"></script>
    <script src="js/jquery-1.10.2.js" type="text/javascript"></script>
    <script type="text/jscript">
        $(document).ready(function () {

            $("#scrollbox6 li a").first().addClass("highlightContactName");
            HighlighThis();
            $("#scrollbox6 li a").first().addClass("highlightContactName");
            setTimeout(function () { $("#scrollbox6 li a").first().click(); scrollBottom(); }, 2000); //this to make sure that 1st li has been selected adn color of text is green.
            $("#ContentPlaceHolder1_lnkCatName").mouseover(function () {
                scrollBottom();
            });
        });
        function scrollBottom() {
            return;
            $("#scrollbox6").animate({ scrollTop: 100000000000000000 }, "slow");
            setTimeout(function () {
                $("#scrollbox6").animate({ scrollTop: 100000000000000000 }, "slow");
                console.log("scroll down");
                //return false;
            }, 500);
        }
        function highlightOnSend() {
            var id = $(".highlightContactName").attr("id");
            $("#" + id).addClass("highlightContactName");
            setTimeout(function () { $("#" + id).addClass("highlightContactName"); HighlighThis(); }, 500);
            setTimeout(function () { $("#" + id).addClass("highlightContactName"); HighlighThis(); }, 1000);
            HighlighThis();
        }

        function HighlighThis() {

            $("#scrollbox6 li a").click(function () {
                $("#scrollbox6 li a").css("color", "black");
                $("#scrollbox6 li a").removeClass("highlightContactName");
                a = jQuery(this);
                $("#" + a.attr("id")).addClass("highlightContactName");
                setTimeout(function () { $("#" + a.attr("id")).addClass("highlightContactName"); HighlighThis(); }, 500);
                setTimeout(function () { $("#" + a.attr("id")).addClass("highlightContactName"); HighlighThis(); }, 1000);
                setTimeout(function () { $("#" + a.attr("id")).addClass("highlightContactName"); }, 2000);
            });

        }
        

    </script>
</asp:Content>
