﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="DocumentList.aspx.cs" Inherits="Organisation_DocumentList" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="../Styles/UpdateCustom.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .document inbox-hover black a i
        {
            margin-top: 9px;
        }
        .inbox-hover {
    right: -130px;
    width:130px;
}
    </style>
 
      <script src="../Scripts/sMain.js?version=1.0" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="col-md-6">
        <div class="heading-sec">
                <h1>
               <%-- <%= CommonMessages.Documents%> --%>
                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Documents" enableviewstate="false"/><i><span runat="server" id="Documents"></span></i>
            </h1>
        </div>
    </div>
    <div class="col-md-12" style="margin-top: 20px;">
        <a href="#add-post-title" data-toggle="modal" title="">
            <button type="button" class="btns  yellow  lrg-btn flat-btn add_user" style="border: 0px;">
<%--                <%= CommonMessages.AddNewDocument%>--%>  <asp:Literal ID="Literal2" runat="server" meta:resourcekey="AddNewDocument" enableviewstate="false"/></button></a>
        <div class="col-md-3" style="float: right; margin-right: 40px;">
            <%-- <input class="library_search" name="" type="text">--%>
            <asp:UpdatePanel runat="server" ID="UpdatePanellist" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:TextBox ID="txtsearch" class="library_search" runat="server" AutoPostBack="True"
                        OnTextChanged="txtsearch_TextChanged" 
                        meta:resourcekey="txtsearchResource1"></asp:TextBox>
                </ContentTemplate>
                <%--<Triggers>
                        <asp:AsyncPostBackTrigger ControlID="txtsearch" />
                    </Triggers>--%>
            </asp:UpdatePanel>
        </div>
        
        
 <%-------Pop up Add new document-------------------------------------------%>

        <div aria-hidden="true" role="dialog" tabindex="-1" class="modal fade" id="add-post-title"
            style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header blue yellow-radius" >
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                            ×
                        </button>
                        <h4 class="modal-title">
                            <%-- <%= CommonMessages.AddNewDocument%>--%> <asp:Literal ID="Literal3" runat="server" meta:resourcekey="AddNewDocument" enableviewstate="false"/></h4>
                    </div>
                        <div class="modal-body">
                        <asp:TextBox runat="server" placeholder="FILE NAME :" ID="txtFileName" MaxLength="50"  /> <%--meta:resourcekey="txttitleResource1"--%>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtFileName"
                            ErrorMessage="Please enter file name." CssClass="commonerrormsg" Display="Dynamic"
                            ValidationGroup="chk" ></asp:RequiredFieldValidator>
                        <br />
<%--File upload------------------------------------------------%>
                     <label for="exampleInputFile">
                            <i class="fa fa-paperclip attch_file"></i><asp:Literal ID="Literal4" runat="server" meta:resourcekey="Attachments" enableviewstate="false"/> <%--<%= CommonMessages.Attachments%>--%>...</label>
                        <asp:FileUpload ID="FileUpload1" runat="server" 
                            meta:resourcekey="FileUpload1Resource1" />
                        <asp:RegularExpressionValidator ID="reFile1" runat="server" ControlToValidate="FileUpload1"
                            CssClass="legend" Display="Dynamic" ErrorMessage="Please upload only .pdf File."
                            ValidationExpression="^.*\.(PDF|pdf)$" ForeColor="Red" 
                            ValidationGroup="chk" meta:resourcekey="reFile1Resource1"></asp:RegularExpressionValidator>
                        <br />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="FileUpload1"
                            ErrorMessage="Please select file ." CssClass="commonerrormsg" Display="Dynamic"
                            ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator5Resource1"></asp:RequiredFieldValidator>
                        <br />
                        
                        <%--Public private radio-------------------------------------%>    
 <br/> <br/>       
                       <div class="margin-top-10px" >
                          
                              
                        <asp:RadioButtonList runat="server" ID="rdoPrivatePublic" RepeatDirection="Horizontal" TextAlign="Left" CssClass="radioButtonList " style="display:inline;width: 5px">
                            <asp:ListItem Selected="True">Public</asp:ListItem>
                            <asp:ListItem>Private</asp:ListItem>
                        </asp:RadioButtonList>
                      </div>
                        
<%--Checkboxes-----------------------------------------%>
    <style type="text/css">.ddpDivision{ max-height: 150px;overflow: auto;} 
        #rdoPrivatePublic label{
    display:inline;
}#ContentPlaceHolder1_rdoPrivatePublic_0{ width: 0px; margin-right: 9px;}#ContentPlaceHolder1_rdoPrivatePublic_1{ width: 0px; margin-right: 9px;}
        </style>
         <label> Division : </label>
            
            
            <div class="ddpDivision modal-body">
    
                            <asp:CheckBoxList Width="250px" ID="chkListDivision" runat="server" CssClass="chkliststyle"
                                    RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                                    DataValueField="value" Style="width: 100%; overflow: auto; ">
                                </asp:CheckBoxList>
               </div>
                                <br/> <br/> <br/> <br/> <br/> <br/> <br/> <br/>
                  <div  class="margin-top-10px" style="margin-top: 34px"><label> Job Type : </label></div>
                                <div class="ddpDivision modal-body">
                                    
                            <asp:CheckBoxList Width="250px" ID="chkListJobtype" runat="server" CssClass="chkliststyle"
                                RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                                DataValueField="value" Style="width: 100%; overflow: auto;" >
                            </asp:CheckBoxList>
                            
                        <%--<asp:TextBox runat="server" placeholder="DESCRIPTION :" ID="txtDESCRIPTION" MaxLength="500"
                            TextMode="MultiLine" meta:resourcekey="txtDESCRIPTIONResource1" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDESCRIPTION"
                            ErrorMessage="Please enter DESCRIPTION." CssClass="commonerrormsg" Display="Dynamic"
                            ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator1Resource1"></asp:RequiredFieldValidator>
                        <br />--%>
                        </div>
                        

                     
 <%--Keyword Textbox ----------------------------------------------------%>
                     
                      <div  class="margin-top-10px" style="margin-top: 168px;">
                      <asp:TextBox runat="server" ID="txtKeywords" placeholder="KEYWORDS &nbsp; (Usefull in search document)"></asp:TextBox>
                     
                      </div>
                       <div class="margin-top-10px" style="margin-left: 9px"> 
                   
                   <label>REPOSITORY :</label>    <asp:DropDownList runat="server" ID="ddRepository" >
                          <asp:ListItem>Please select Repository</asp:ListItem>
                            <asp:ListItem>Document</asp:ListItem>
                            <asp:ListItem>HR Rules</asp:ListItem>
                            <asp:ListItem>Employee Rules</asp:ListItem>
                            <asp:ListItem>Manpower Rules</asp:ListItem>
                            <asp:ListItem>Marketing</asp:ListItem>
                      </asp:DropDownList>
                      <asp:CompareValidator
                                        ID="val14" runat="server" ControlToValidate="ddRepository"
                                        ErrorMessage=" Required." Operator="NotEqual"
                                        ValueToCompare="Please select Repository"
                                        ForeColor="Red" SetFocusOnError="true" />
                     
                      
<%--Description--------------------------------------------------------%>
                       
                    </div>
                    <asp:TextBox runat="server" placeholder="Description :" ID="txtDescription" style="margin-top: 30px" /> <%--meta:resourcekey="txttitleResource1"--%>
                       <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtFileName"
                            ErrorMessage="Please enter file name." CssClass="commonerrormsg" Display="Dynamic"
                            ValidationGroup="chk" ></asp:RequiredFieldValidator>--%>
                        <br />
<%--Buttons--------------------------------------------------------%>
                    <div style="clear: both"></div>
                    <div class="modal-footer">
                          <%-- <button class="btn btn-primary yellow" type="button">Submit</button>
                        <button data-dismiss="modal" class="btn btn-default black" type="button">Close </button>--%>
                       
                        <asp:Button runat="server" ID="btnsubmit" Text="Submit" CssClass="btn btn-primary yellow" type="button"
                            ValidationGroup="chk" OnClick="btnsubmit_click" OnClientClick="CheckValidations('chk')"
                            style="border-radius: 5px; width: 100px;height: 38px;margin-left: 359px;" meta:resourcekey="btnsubmitResource1"/>
                        <%--<input type="button" onclick="myFunction()" id="btnsubmit" value="Submit" class="btn btn-primary yellow">--%>
                        <%--<button data-dismiss="modal" class="btn btn-default black" type="reset" onclick="clearFields();" style="height: 39px;">
                            <%= CommonMessages.Close%>
                        </button>--%>
                        <input type="reset"  value="Close"  class="btn btn-default black" style="border-radius: 5px; width: 100px;height: 38px;margin-left: 466px;margin-top: -58px;color: white;" onclick="hideAddNewDoc()"/>
                    </div>
                </div>
                <!-- /.modal-content ---------------------------------------------------->
            </div>
        </div>
        </div>
        <asp:UpdatePanel runat="server" ID="UpdatePanellist1" UpdateMode="Conditional">
            <ContentTemplate>
                <ul class="document">
                    <asp:Repeater ID="rep_document" runat="server">
                        <ItemTemplate>
                            <li>
                                <div class="document1">
                                    <img src="images/documents_icon.png">
                                </div>
                                <h1>
                                    <%# Eval("docFileName_Friendly")%></h1>
                                <span>
                                    <%# Eval("MinAgo") %>
                                </span><i>
                                    <%# Eval("FullName") %>
                                </i>
                                <p style="height: 60px;">
                                    <%# Eval("docDescription")%></p>
                                <div class="inbox-hover black">
                                    <a href="../Log/upload/Document/<%# Eval("docattachment") %>" title="" data-tooltip="Download"
                                        data-placement="bottom" download><i class="fa fa-download" style="margin-top: 9px;">
                                        </i></a>
                                        <a href="#" title="" data-tooltip="Read" data-placement="bottom" onclick="openPDF('../Log/upload/Document/<%# Eval("docattachment") %>?UID=<%# Eval("docUserID") %>&TOID=0&docId=<%# Eval("docID") %>');">
                                            <i class="fa fa-eye" style="margin-top: 9px;"></i></a>
                                    <asp:LinkButton ID="btn_delete" runat="server" CommandName='<%# Eval("docId") %>'
                                        data-tooltip="Delete" data-placement="bottom" OnClick="btn_delete_Click" 
                                        meta:resourcekey="btn_deleteResource1"><i class="fa fa-trash-o" style="margin-top: 9px;"></i></asp:LinkButton>
                                </div>
                            </li>
                        </ItemTemplate>
                    </asp:Repeater>
                </ul>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger ControlID="txtsearch" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
      <script type="text/jscript">

        $(document).ready(function () {
            setTimeout(function () {
                $(".btn.btn-default.black").click(function () { $(".modal").modal("hide"); });
            },5000);
        });

        function openPDF(url) {

            //url shoudl contain querystrings  >>  UID and TOID
            var uId = GetQueryParameterByName("UID",url);
            var toId = GetQueryParameterByName("TOID", url);
            var docId = GetQueryParameterByName("docId", url);
            //ajax call that give point to originator of document
            $.ajax({
                type: "POST",
                url: 'WebService1.asmx/InsertUpdatePoint',
                data: "{'uId':'" + uId + "', 'toId' :'" + toId + "', 'url' : '" + url + "','docId':'" + docId + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#divResult").html(msg.d);
                    //alert(msg.d);
                },
                error: function (e) {
                    $("#divResult").html("WebSerivce unreachable");
                }
            });
            
            
            //
            

            url = url.split("?")[0];
            var w = window.open(url, '_blank');
            w.focus();
        }
        function clearFields() {
           // document.getElementById("txtFileName").text("");
            document.getElementById("<%=txtFileName.ClientID %>").value = " ";
        }
        function hideAddNewDoc() {
            $(".btn.btn-default.black").click(function () { $(".modal").modal("hide"); });
            $(".modal").modal("hide");
           // document.getElementById('<%= txtFileName.ClientID %>').text = ""
            
        }
    
    </script>
</asp:Content>
