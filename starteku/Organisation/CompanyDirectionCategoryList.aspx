﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master" AutoEventWireup="true" CodeFile="CompanyDirectionCategoryList.aspx.cs" Inherits="Organisation_CompanyDirectionCategoryList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style>
        .tabs-menu li {
            width: 33.33%;
        }

        .tab {
            padding-top: 0px !important;
        }

        .tab-content {
            padding: 0px !important;
        }



        .dropdown-menu > li > a {
            display: block;
            padding: 8px 65px !important;
            padding-bottom: 8px;
            clear: both;
            font-weight: normal;
            line-height: 1.428571429;
            color: #333;
            white-space: nowrap;
        }

        .radio input[type="radio"], .radio-inline input[type="radio"], .checkbox input[type="checkbox"], .checkbox-inline input[type="checkbox"] {
            float: left;
            margin-left: -80px !important;
        }

        .radiobuttonlist {
            font: 12px Verdana, sans-serif;
            color: #000; /* non selected color */
        }

        .inline-rb input[type="radio"] {
            width: auto;
            margin-right: 10px;
            margin-left: 20px;
        }

        .inline-rb label {
            display: inline;
        }
    </style>



    <%--joyride---------------------------------------------%>
    <link href="../plugins/joyride/joyride-2.1.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/multiselect.css" rel="stylesheet" />
    <link href="../Styles/UpdateCustom.css" rel="stylesheet" type="text/css" />
    <script src="../plugins/joyride/jquery.cookie.js" type="text/javascript"></script>
    <script src="../plugins/joyride/jquery.joyride-2.1.js" type="text/javascript"></script>
    <script src="../plugins/joyride/modernizr.mq.js" type="text/javascript"></script>

    <script src="../plugins/highchart/highcharts.js" type="text/javascript"></script>


    <script src="../plugins/highchart/highcharts-more.js" type="text/javascript"></script>
    <script src="../Scripts/plugins/exporting.js" type="text/javascript"></script>
    <script src="../Scripts/jquery321.js"></script>

    <script src="../assets/js/multiselect.js" type="text/javascript"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            setTimeout(function () {
                SetExpandCollapse();
                //ExampanCollapsMenu();
                //ExampanCollapssubMenu();
                // tabMenuClick();
                $(".OrgDivisionList").addClass("active_page");
            }, 500);
        });
    </script>
    <div class="col-md-6">
        <div class="heading-sec">
            <asp:Label runat="server" ID="Label12" CssClass="lblModel" meta:resourcekey="comdirectioncat" Font-Bold="True" ForeColor="white" Font-Size="22px"></asp:Label>

            <%--            <h1 style="margin-left: 15px;">Company Direction Category <i><span runat="server" id="Division"></span></i>--%>
            <%--            </h1>--%>
        </div>
    </div>

    <div class="col-md-12" style="margin-top: 20px;">

         <asp:HiddenField ID="hdnRecordAlreadyFound" runat="server" Value="Select Template" meta:resourcekey="RecordAlreadyFoundRes" />

        <asp:HiddenField ID="hdnSelectQues" runat="server" Value="Select Question Category" meta:resourcekey="SelectQuesCatResource" />
        <asp:HiddenField ID="hdnSelectQuesTemplate" runat="server" Value="Select Template" meta:resourcekey="SelectQuesTemplateResource" />
        <div id="Div1">
            <div class="col-md-12">
                <div class="add-btn1" style="float: right;">


                    <a href="#popupAddNewQuesCat" data-toggle="modal" title="" style="display: none">
                        <asp:Label runat="server" ID="lblQuestionCat" CssClass="lblModel" meta:resourcekey="AddQuestionCat" Style="font-size: 19px; margin-top: 4px;"></asp:Label>
                    </a>
                    <%--<asp:Button runat="server" ID="btnAddQuesCat" Text="Add Question Category" CssClass="btn btn-default yellow " meta:resourcekey="AddQuestionCat"
                        type="button" Style="border-radius: 5px; height: 38px; color: white;" />--%>


                    <%--  <a href="myBtn" data-toggle="modal" id="myBtn" class="btn btn-default yellow">
                             <asp:Label runat="server" ID="Label1234" CssClass="lblModel" meta:resourcekey="Addcompdireccat" Style="font-size: 19px; margin-top: 4px;"></asp:Label>
                 
                        </a>--%>
                    <a href="#popupAddNewQuesCat" data-toggle="modal" title="" style="display: none;">
                        <asp:Label runat="server" ID="Label13" CssClass="lblModel" meta:resourcekey="AddQuestionCat" Style="font-size: 19px; margin-top: 4px;"></asp:Label>
                    </a>
                    <asp:Button runat="server" ID="btnAddQuesCat" Text="Add Company Direction Category" CssClass="btn btn-default yellow " meta:resourcekey="Addcompdireccat"
                        type="button" Style="border-radius: 5px; height: 38px; color: white;" OnClick="OpenCreateCompanyDirCat" />


                </div>

                <div style="clear: both;"></div>
            </div>
        </div>
    </div>

    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" meta:resourcekey="lblMsgResource1"></asp:Label>
    <%--Category--%>
    <div>
        <div aria-hidden="true" role="dialog" tabindex="-1" class="modal fade col-md-12" id="popupAddNewQuesCat" style="display: none;">
            <div class="modal-dialog" style="width: 70%">
                <div class="modal-content">
                    <div class="modal-header blue yellow-radius">
                        <h4>
                            <asp:Label runat="server" meta:resourcekey="addcomdirectioncat"></asp:Label></h4>
                    </div>
                    <div class="modal-body">


                        <div class="col-md-12">
                            <div class="col-md-4" style="padding-top: 20px;">
                                <%--                        <label style="float:left">Company Direction Category Name</label>--%>
                                <asp:Label runat="server" ID="Lable2" CssClass="lblModel" meta:resourcekey="comdireccatname" Font-Bold="True">
                         <label style="color: red; float:left">*</label> 
                                </asp:Label>
                            </div>
                            <div class="col-md-5" style="padding-top: 10px;">
                                <asp:TextBox ID="txtname" runat="server" placeholder="Category Name(English)" meta:resourcekey="catname"></asp:TextBox>
                            </div>
                            <div class="col-md-3" style="padding-top: 10px;">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtname"
                                    ErrorMessage="Please Enter Category Name" CssClass="commonerrormsg" ValidationGroup="grpcat"
                                    Display="Dynamic" meta:resourcekey="catnameres">

                                </asp:RequiredFieldValidator>
                            </div>
                        </div>
                        <div class="col-md-12" style="display: none;">
                            <div class="col-md-4" style="padding-top: 20px;">
                                <asp:Label runat="server" ID="Label4" CssClass="lblModel" meta:resourcekey="comdireccatname" Font-Bold="True">
                        <label style="color: red; float:left">*</label>
                                </asp:Label>
                            </div>
                            <div class="col-md-5" style="padding-top: 10px; display: none;">
                                <asp:TextBox ID="txtnameDN" runat="server" placeholder="Category Name(Denish)" meta:resourcekey="catname"></asp:TextBox>
                            </div>
                            <div class="col-md-3" style="padding-top: 10px;">
                                <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtnameDN"
                            ErrorMessage="Please Enter Category Name" CssClass="commonerrormsg" ValidationGroup="grpcat"
                            Display="Dynamic">

                        </asp:RequiredFieldValidator>--%>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="col-md-4" style="padding-top: 10px;">
                                    <asp:Label runat="server" ID="Label5" CssClass="lblModel" meta:resourcekey="comdireccatdesc" Font-Bold="True"></asp:Label>

                                </div>
                                <div class="col-md-5" style="padding-top: 10px;">
                                    <asp:TextBox runat="server" ID="txtdescription" placeholder="Category Description" TextMode="MultiLine" meta:resourcekey="catdescription" />
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="col-md-4" style="padding-top: 20px;">
                                    <asp:Label runat="server" ID="Label10" CssClass="lblModel" meta:resourcekey="comdirecseqno" Font-Bold="True">
                              <label style="color: red; float: left">*</label>
                                    </asp:Label>
                                </div>
                                <div class="col-md-3" style="padding-top: 10px;">
                                    <asp:TextBox runat="server" ID="txtno" MaxLength="3" min="0" type="number" max="999" placeholder="Sequence Number" onkeypress="return isNumber(event)" meta:resourcekey="Seqno" />
                                </div>
                                <div class="col-md-4" style="padding-top: 10px;">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtno"
                                        ErrorMessage="Please Enter Sequence Number" CssClass="commonerrormsg" ValidationGroup="grpcat"
                                        Display="Dynamic" meta:resourcekey="seqnores">

                                    </asp:RequiredFieldValidator>
                                </div>

                            </div>

                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="col-md-4" style="margin-top: 20px; margin-left: -10px; color: black; margin-top: 50px;">
                                    <%-- <asp:Literal ID="Literal12" runat="server"  meta:resourcekey="Translations" Font-Bold="True"> </asp:Literal>--%>
                                    <asp:Label runat="server" ID="Literal12" CssClass="lblModel" meta:resourcekey="Translations" Font-Bold="True"></asp:Label>
                                </div>

                                <div class="col-md-8" style="overflow-y: scroll; height: 150px; margin-left: 10px;">
                                    <asp:GridView ID="gridTranslate" runat="server" AutoGenerateColumns="False" CellPadding="0" Style="background-color: #f9f9f9; margin-bottom: 0px !important;"
                                        Width="100%" GridLines="None" CssClass="table table-striped table-bordered table-hover table-checkable datatable">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Language" meta:resourcekey="language">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("resLanguage") %>'></asp:Label>
                                                    <asp:HiddenField ID="hdnResLangID" runat="server" Value='<%# Eval("resLangID") %>'></asp:HiddenField>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" Font-Size="14px" />
                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                    Width="10%" ForeColor="White" Font-Size="15px" BorderWidth="0px" Height="20px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Value" meta:resourcekey="Value">
                                                <ItemTemplate>
                                                    <%--<asp:TextBox ID="txtTranValue" Value='<%# Eval("LangText") %>' runat="server"></asp:TextBox>--%>
                                                    <asp:TextBox ID="txtTranValue"  runat="server"></asp:TextBox>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" Font-Size="14px" />
                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                    Width="10%" ForeColor="White" Font-Size="15px" BorderWidth="0px" Height="20px" />
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="col-md-4" style="padding-top: 10px;">
                                </div>
                                <div class="col-md-2" style="padding-top: 10px;">
                                    <asp:Button ID="btnsave" runat="server" Text="Save" OnClick="btnsave_Click" ValidationGroup="grpcat" CssClass="btn btn-primary yellow" meta:resourcekey="btnsaveres" />
                                </div>

                                <div class="col-md-2" style="margin-top: 10px;">
                                    <asp:Button ID="Button1" runat="server" Text="Close" ForeColor="White" BackColor="Black" CssClass="Close btn btn-primary black" ValidationGroup="chkdoc3" meta:resourcekey="btncloseres" />
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


    <div class="col-md-12" style="margin-top: 20px;" id="htmlData">
        <div id="graph-wrapper">
            <div class="col-md-12">

                <div class="chart-tab">
                    <div id="tabs-container">
                        <div class="loaderDiv" style="display: none">
                            <div class="progress small-progress">
                                <div style="width: 40%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40"
                                    role="progressbar" class="progress-bar blue">
                                </div>
                            </div>

                            <div class="home_grap">
                                <div id="ContainerDevelopment" style="min-width: 310px; height: 400px; margin: 0 auto">
                                    <div id="waitDevelopment" style="margin: 190px 602px">
                                        <img src="../images/wait.gif" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab">

                            <div id="tab-1" class="tab-content">

                                <div class="chart-tab manager_table">
                                    <div id="tabs-container manager_table">

                                        <asp:GridView ID="gvGrid_DirCat" runat="server" AutoGenerateColumns="False" CellPadding="0"
                                            Width="100%" GridLines="None" DataKeyNames="catID" CssClass="table1 table-striped table-bordered table-hover table-checkable datatable"
                                            EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'
                                            BackColor="White"
                                            meta:resourcekey="GridRecordNotfound"
                                            OnRowCommand="gvGrid_DirCat_RowCommand">


                                            <HeaderStyle CssClass="aa" />
                                            <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                            <AlternatingRowStyle BackColor="White" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="SrNo" meta:resourcekey="SrNo">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="center" CssClass="hidden-xs" Width="50px" />
                                                    <HeaderStyle HorizontalAlign="center" CssClass="invoice-head chat-widget-head yellow"
                                                        Width="1%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />


                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Direction Category" meta:resourcekey="TemplateFieldResource2">
                                                    <ItemTemplate>
                                                        <asp:Label ID="lblrNamer" runat="server" Text='<%# Eval("catNameInLang") %>'></asp:Label>
                                                        <%--<asp:HiddenField ID="divCompanyId" runat="server" Value='<%# Eval("qcatCompanyID") %>' />--%>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="left" CssClass="hidden-xs" Font-Size="14px" />
                                                    <HeaderStyle HorizontalAlign="left" CssClass="invoice-head chat-widget-head yellow"
                                                        Width="31%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                </asp:TemplateField>


                                                <asp:TemplateField HeaderText="" ItemStyle-Width="50px" meta:resourcekey="TemplateFieldResource3">
                                                    <ItemTemplate>
                                                        <div class="vat" style="width: 90px;" id="divEdit" runat="server">

                                                            <i class="fa fa-pencil"></i>
                                                            <a href="<%# String.Format("CompanyDirectionCategoryEdit.aspx?id={0}", Eval("catID")) %>"
                                                                title="Edit">
                                                                <asp:Label runat="server" meta:resourcekey="btnEdit"></asp:Label></a>

                                                        </div>
                                                        <div class="total" style="width: 90px;" id="divDeletet" runat="server">

                                                            <i class="fa fa-trash-o"></i>
                                                            <asp:LinkButton ID="lnkBtnName" runat="server" CommandName="archive" CommandArgument='<%# Eval("catID") %>'
                                                                ToolTip="Delete " OnClientClick="return confirm('Are you sure you want to Delete  this record?');"
                                                                meta:resourcekey="lnkBtnNameResource1" CausesValidation="false"><asp:Label runat="server" meta:resourcekey="btnDelete"></asp:Label></asp:LinkButton>

                                                        </div>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" Font-Size="14px" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                        Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <FooterStyle BackColor="#cccccc" Font-Bold="True" ForeColor="Black" HorizontalAlign="Left" />
                                        </asp:GridView>


                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal content -->



    <!-- Modal -->

    <script type="text/javascript">
        // Get the modal
        var modal = document.getElementById('myModal');

        // Get the button that opens the modal
        var btn = document.getElementById("myBtn");

        // Get the <span> element that closes the modal
        var span = document.getElementsByClassName("close")[0];

        // When the user clicks the button, open the modal 
        btn.onclick = function () {
            modal.style.display = "block";

            $('#ContentPlaceHolder1_txtname').val('');
            $('#ContentPlaceHolder1_txtnameDN').val('');
            $('#ContentPlaceHolder1_txtdescription').val('');
            $('#ContentPlaceHolder1_txtno').val('');
        }

        // When the user clicks on <span> (x), close the modal
        span.onclick = function () {
            modal.style.display = "none";
        }

        // When the user clicks anywhere outside of the modal, close it
        window.onclick = function (event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }
    </script>

    <script>
        function closeModel(a) {

            if (CheckValidations('chkdoc')) { $('#' + a).modal('hide'); }

        }


        function closeModelCancel(a) { $('#' + a).modal('hide'); }
    </script>

</asp:Content>

