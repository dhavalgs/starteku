﻿using starteku_BusinessLogic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class Organisation_EmpCompetence : System.Web.UI.Page
{
    #region Variable Declare
    public string YourScript = "";
    #endregion

    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            GetAllCompetence();
        }
    }
    #endregion

    #region Method
    public void GetAllCompetence()
    {
        CompetenceBM obj = new CompetenceBM();
        obj.comIsDeleted = false;
        obj.comIsActive = true;
        obj.comCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllCompetence();
        DataSet ds = obj.ds;

        if (ds.Tables[0].Rows.Count > 0)
        {
            rep_Competence.DataSource = ds;
            rep_Competence.DataBind();
        }

        //if (ds.Tables[0].Rows.Count == 1)
        //{
        //    YourScript = "ReadyQuestions();";
        //}
    }
    public void GetAllCompetencebyid(int comId)
    {
        CompetenceBM obj = new CompetenceBM();
        obj.comCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllCompetencebyid();
        DataSet ds = obj.ds;

        if (ds.Tables[0].Rows.Count > 0)
        {
            rep_Competence.DataSource = ds;
            rep_Competence.DataBind();
        }

        //if (ds.Tables[0].Rows.Count == 1)
        //{
        //    YourScript = "ReadyQuestions();";
        //}
    }
    #endregion

    #region Events

    #endregion


    protected void rep_qua_list_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        //if (e.Item.ItemType == RepeaterItem)
        {

            HiddenField hf_comId = (HiddenField)e.Item.FindControl("hf_comId");

            CompetenceBM obj = new CompetenceBM();
            obj.comId = Convert.ToInt32(hf_comId.Value);
            obj.GetAllCompetencebyid();
            DataSet ds = obj.ds;

            if (ds.Tables[0].Rows.Count > 0)
            {
                RadioButtonList rbl_answears = (RadioButtonList)e.Item.FindControl("rbl_answears");
                for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                {
                    rbl_answears.Items.Add(
                        new ListItem()
                        {
                            Text = Convert.ToString(ds.Tables[1].Rows[i]["answer"]),
                            Value = Convert.ToString(ds.Tables[1].Rows[i]["aId"])
                        });
                }

            }
        }
    }

    protected void btn_submit_answear_Click(object sender, EventArgs e)
    {
        Boolean status = false;
        foreach (RepeaterItem itme in rep_qua_list.Items)
        {
            RadioButtonList rbl = (RadioButtonList)itme.FindControl("rbl_answears");
            HiddenField hf_comId = (HiddenField)itme.FindControl("hf_comId");
            HiddenField trueAns = (HiddenField)itme.FindControl("trueAns");
            int userid = Convert.ToInt32(Session["OrgUserId"]);
            Boolean ans_status = true;

            PointBM obj = new PointBM();
            obj.PointUserId = userid;
            obj.PointCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.PointComId = Convert.ToInt32(hf_comId.Value);
            obj.PointAnsId = Convert.ToInt32(rbl.SelectedValue);
            if (Convert.ToInt32(trueAns.Value) == Convert.ToInt32(rbl.SelectedValue))
            {
                obj.PointAnsCount = 1;
            }
            else
            {
                obj.PointAnsCount = 0;
            }
            obj.PointIsDeleted = false;
            obj.PointIsStatus = true;
            status = obj.InsertPoint();
            if (status)
            {
                status = true;
            }
            else
            {
                break;
                status = false;
            }
        }
        if (status)
        {
            Response.Redirect("Employee_dashboard.aspx");
        }
    }


    protected void rep_Competence_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {        
        HiddenField hf = (HiddenField)e.Item.FindControl("hf_point");
        HtmlGenericControl div = (HtmlGenericControl)e.Item.FindControl("pnltest");
        LinkButton lb = (LinkButton)e.Item.FindControl("txt_Competence");

        if (!string.IsNullOrEmpty(hf.Value))
        {
            //div.Attributes.Add("class", "active_satus");            
        }
        else
        {
            div.Attributes.Add("class", "active_satus");
            lb.ForeColor = Color.Black;
        }
    }
    protected void txt_Competence_Click(object sender, EventArgs e)
    {

    }
}