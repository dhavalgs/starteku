﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master" AutoEventWireup="true" CodeFile="CompanyDirection.aspx.cs" Inherits="Organisation_CompanyDirection" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="js/jquery-1.10.2.js"></script>

    <link href="../Scripts/pickadate.js-3.5.6/lib/themes/classic.css" rel="stylesheet" />
    <link href="../Scripts/fullcalendor/pickADate/classic.date.css" rel="stylesheet" />

    <script type="text/javascript" src="https://cdn.datatables.net/1.10.15/js/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/responsive/2.1.1/js/dataTables.responsive.min.js"></script>
    <link href="https://cdn.datatables.net/1.10.15/css/dataTables.jqueryui.min.css" rel="stylesheet" />
    <link href="https://cdn.datatables.net/responsive/2.1.1/css/responsive.dataTables.min.css" rel="stylesheet" />


    <style type="text/css">
        .hover_bkgr_fricc {
            background: rgba(0,0,0,.4);
            cursor: pointer;
            display: none;
            height: 100%;
            position: fixed;
            text-align: center;
            top: 0;
            width: 100%;
            z-index: 10000;
        }


            .hover_bkgr_fricc .helper {
                display: inline-block;
                height: 100%;
                vertical-align: middle;
            }

            .hover_bkgr_fricc > div {
                background-color: #fff;
                box-shadow: 10px 10px 60px #555;
                display: inline-block;
                height: auto;
                max-width: 1100px;
                min-height: 100px;
                vertical-align: middle;
                width: 100%;
                position: relative;
                border-radius: 8px;
                margin-right: 150px;
                z-index: 99;
                margin-top: 80px;
                -webkit-animation-name: animatetop;
                -webkit-animation-duration: 0.4s;
                animation-name: animatetop;
                animation-duration: 0.4s;
            }

        .popupCloseButton {
            color: white;
            float: right;
            font-size: 27px;
            font-weight: 100;
            opacity: 0.9;
            margin-right: 10px;
            margin-top: -20px;
        }

            .popupCloseButton:hover {
                color: #000;
                font-weight: 100;
                text-decoration: none;
                cursor: pointer;
            }

        .trigger_popup_fricc {
            cursor: pointer;
            font-size: 20px;
            margin: 20px;
            display: inline-block;
            font-weight: bold;
        }

        .header2 {
            margin-top: -25px;
            width: 100%;
            color: white;
            height: 50px;
            text-align: left;
        }

        .tabs-menu li {
            width: 33.333% !important;
        }

        .inline-rb input[type="radio"] {
            width: auto;
            margin-right: 10px;
            margin-left: 0px;
        }

        .inline-rb label {
            display: inline;
        }

        .radio input[type="radio"], .radio-inline input[type="radio"], .checkbox input[type="checkbox"], .checkbox-inline input[type="checkbox"] {
            float: left;
            margin-left: -80px !important;
        }

        .radiobuttonlist {
            font: 12px Verdana, sans-serif;
            color: #000; /* non selected color */
        }
    </style>
    <style type="text/css">
        .picker {
            font-size: 15px;
        }
    </style>


    <script src="../Scripts/pickadate.js-3.5.6/lib/legacy.js" type="text/javascript"></script>
    <script src="../Scripts/pickadate.js-3.5.6/lib/picker.js" type="text/javascript"></script>
    <script src="../Scripts/pickadate.js-3.5.6/lib/picker.date.js" type="text/javascript"></script>
    <script src="../Scripts/pickadate.js-3.5.6/lib/picker.time.js" type="text/javascript"></script>
    <script src="../assets/js/libs/tagsInput.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

     <asp:HiddenField ID="Label8" runat="server" Value="Add Company Direction Element" meta:resourcekey="AddComDirElemres1" ></asp:HiddenField>
     <asp:Label ID="Label9" runat="server" Text="Edit Company Direction Element" meta:resourcekey="EditComDirElemres" Visible="false"></asp:Label>
    <asp:HiddenField ID="hdnSelectCat" runat="server" Value="Select Company Category" meta:resourcekey="SelectCompanyCategoryResource" />
    <asp:HiddenField ID="hdnSelectCat2" runat="server" Value="Company Category" meta:resourcekey="Companycategoryres" />
    <asp:HiddenField ID="hdnAll" runat="server" Value="All" meta:resourcekey="allcategoryres"/>
    <asp:HiddenField ID="hdnSelectdirele" runat="server" Value="Direction Element" meta:resourcekey="directionelementres1" />
    <asp:HiddenField ID="hdnSelectTeam" runat="server" Value="All" meta:resourcekey="SelectTeamResource" />
    <asp:HiddenField ID="hdnSelectUser" runat="server" Value="Select User" meta:resourcekey="SelectUserResource" />

    <asp:HiddenField ID="hdnSelectedCatIDs" runat="server" Value="0" />
    <asp:HiddenField ID="hdnSelectedDirIDs" runat="server" Value="0" />

    <asp:HiddenField ID="hdnSelectedTeamIDs" runat="server" Value="0" />
    <asp:HiddenField ID="hdnSelectedUserIDs" runat="server" Value="0" />


    <asp:HiddenField ID="hdnAllTeamIDs" runat="server" Value="0" />


    <asp:HiddenField ID="hdnPublishTeamUserID" runat="server" Value="0" />

    <asp:Label runat="server" ID="Label1" CssClass="commonerrormsg" meta:resourcekey="lblMsgResource1"></asp:Label>
    <asp:Label ID="hdnConfirmArchive" Style="display: none;" CssClass="abcde" meta:resourcekey="ConfirmArchive" runat="server" />
    
    <%--joyride---------------------------------------------%>

    <link href="../plugins/joyride/joyride-2.1.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/multiselect.css" rel="stylesheet" />
    <link href="../Styles/UpdateCustom.css" rel="stylesheet" type="text/css" />
    <script src="../plugins/joyride/jquery.cookie.js" type="text/javascript"></script>
    <script src="../plugins/joyride/jquery.joyride-2.1.js" type="text/javascript"></script>
    <script src="../plugins/joyride/modernizr.mq.js" type="text/javascript"></script>


    <%--    <script src="../Scripts/jquery_1_8_3.min.js" type="text/javascript"></script>--%>

    <script type="text/javascript">
       
        function plussign1(a)
        {
            if ($(a).hasClass('plus')) {
                $(a).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(a).next().html() + "</td></tr>");
                $(a).attr("src", "../images/minus.png");
                $(a).removeClass('plus');
                $(a).addClass('minus');
            }
            else {
                $(a).attr("src", "../images/plus.png");
                $(a).closest("tr").next().remove();
                $(a).removeClass('minus');
                $(a).addClass('plus');
            }
        }
        // $('.teamplussign').on("click", function () {
        function teamplussign1(a){

            if ($(a).hasClass('teamplus')) {
                $(a).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(a).next().html() + "</td></tr>");
                $(a).attr("src", "../images/minus.png");
                $(a).removeClass('teamplus');
                $(a).addClass('teamminus');
            }
            else {
                $(a).attr("src", "../images/plus.png");
                $(a).closest("tr").next().remove();
                $(a).removeClass('teamminus');
                $(a).addClass('teamplus');
            }

            //  });
        }

            
        function assignplussign1(a)
        {
         
            if ($(a).hasClass('assignplus')) {
                $(a).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(a).next().html() + "</td></tr>");
                $(a).attr("src", "../images/minus.png");
                $(a).removeClass('assignplus');
                $(a).addClass('assignminus');
            }
            else {
                $(a).attr("src", "../images/plus.png");
                $(a).closest("tr").next().remove();
                $(a).removeClass('assignminus');
                $(a).addClass('assignplus');
            }
             
            //  });
        }
        
    </script>
    <div class="">
        <div class="container">
            <div class="col-md-12">
                <div class="heading-sec">
                    <h1 style="color: white;">
                        <asp:Literal ID="Literal1" runat="server" EnableViewState="false"  />
                        <asp:Label runat="server" meta:resourcekey="companydirectionres"></asp:Label><i><span style="color: white;"
                            runat="server" id="Personaldevelopment"></span></i>
                        <i style="color: white;"><span runat="server" id="Settings"></span></i>
                    </h1>
                </div>
            </div>


            <div class="col-md-4">
                <div class="dropdown-example" style="margin-left: 15px;">
                    <br />

                    <%--  <asp:DropDownList Width="250px" ID="ddActCate" AutoPostBack="false" runat="server" CssClass="chkliststyle form-control" onchange="dateget(this)"
                        RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                        DataValueField="value" Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; color: white; overflow: auto;" Visible="True">
                    </asp:DropDownList>--%>
                </div>
            </div>

            <div class="col-md-8">
            </div>



            <div class="col-md-12" style="margin-top: 20px;" id="htmlData">
                <div id="graph-wrapper">
                    <div style="float: left; margin-top: 40px; margin-left: 20px">
                        <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg"
                            meta:resourcekey="lblMsgResource1"></asp:Label>

                    </div>
                    <div class="add-btn1" style="float: right; margin-right: 20px">

                         <a class="trigger_popup_fricc btn btn-default yellow" style="font-size: 15px; font-weight: normal;height: 38px;">
                           
                             <asp:Label runat="server" ID="Label6" CssClass="lblModel" meta:resourcekey="AddCompanyDirElementRes" 
                                Text="Add Company Direction Element" Style="font-size: 15px; margin-top: 4px;"></asp:Label>
                        </a>
                    </div>

                    <%--<button id="myBtn">Open Modal</button>--%>

                    <div class="add-btn1" style="float: right; margin-right: 10px; margin-top: 20px;">

                        <%-- <a href="#myModal" data-toggle="modal" title="" style="display: none;">
                            <asp:Label runat="server" ID="lblCompanyDirection" CssClass="lblModel" meta:resourcekey="CompanyDirection" Style="font-size: 19px; margin-top: 4px;"></asp:Label>
                        </a>
                        <asp:Button runat="server" ID="btnCompDir" Text="Add Company Direction Category" CssClass="btn btn-default yellow "
                            meta:resourcekey="AddQuestion" type="button" OnClick="myBtn_Click" Style="border-radius: 5px; height: 38px; color: white;" />--%>

                        <%-- <a href="myBtn" runat="server" onclick="" id="myBtn" class="btn btn-default yellow ">Add Company Direction Category</a>--%>
                        <%-- <asp:Button runat="server" ID="btncdc" Text="Add Company Direction Category" CssClass="btn btn-primary yellow"
                          Style="border-radius: 5px;" OnClick="Test_click" />

                        <a href="#myModal" data-toggle="modal" title="" style="margin-bottom: 15px; display: none;">
                            <button id="myBtn" style="border: 0px;" class="btn btn-primary yellow lrg-btn flat-btn" type="button">
                                <asp:Literal ID="Literal12" runat="server" EnableViewState="false" />
                            </button>
                        </a>--%>
                        <a href="#popupAddNewQuesCat" data-toggle="modal" title="" style="display: none;">
                            <asp:Label runat="server" ID="lblQuestionCat" CssClass="lblModel" meta:resourcekey="AddCompanyDirectionlbl" Style="font-size: 19px; margin-top: 4px;"></asp:Label>
                        </a>
                        <asp:Button runat="server" ID="btnAddQuesCat" Text="Add Company Direction Category" CssClass="btn btn-default yellow " meta:resourcekey="AddComDirectionres"
                            type="button" Style="border-radius: 5px; height: 38px; color: white;" OnClick="OpenCreateCompanyDirCat" />

                    </div>
                    <div class="col-md-12" style="margin-top: 10px">
                        <ul class="tabs-menu maintab" style="width: 100%">
                            <li class="current" id="t1"><a href="#" onclick="DisplayTab(1);">
                                <asp:Literal ID="Literal7" runat="server" Text="Manage Company Direction" meta:resourcekey="ManageCompanyDirection"
                                    EnableViewState="false" /></a></li>
                            <li id="t2"><a href="#" onclick="DisplayTab(2);">
                                <asp:Literal ID="Literal8" runat="server" Text="Assign Company Direction" meta:resourcekey="AssignCompanyDirection" EnableViewState="false" /></a></li>
                            <li id="t3"><a href="#" onclick="DisplayTab(3);">
                                <asp:Literal ID="Literal9" runat="server" Text="Publish Company Direction " meta:resourcekey="PublishCompanyDirection" EnableViewState="false" /></a></li>

                            <%-- <li id="t4"><a href="#" onclick="DisplayTab(4);  ">
                                    <asp:Literal ID="Literal10" runat="server" Text="Activitites " meta:resourcekey="Activitites" EnableViewState="false" /></a></li>                           

                            <li id="t5"><a href="#" onclick=" DisplayTab(5);">
                                <asp:Literal ID="Literal2" runat="server" Text="Comments" meta:resourcekey="Comments" EnableViewState="false" /></a></li>--%>
                        </ul>

                        <div class="chart-tab">
                            <div id="tabs-container">
                                <div class="loaderDiv" style="display: none">
                                    <div class="progress small-progress">
                                        <div style="width: 40%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40"
                                            role="progressbar" class="progress-bar blue">
                                        </div>
                                    </div>

                                    <div class="home_grap">
                                        <div id="ContainerDevelopment" style="min-width: 310px; height: 400px; margin: 0 auto">
                                            <div id="waitDevelopment" style="margin: 190px 602px">
                                                <img src="../images/wait.gif" />
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="tab">

                                    <div id="tab-1" class="tab-content">

                                        <div class="form-group col-md-12">
                                            <div class="col-md-4">
                                                <asp:Label runat="server" ID="Label2" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                                    <asp:Literal ID="Literal2" runat="server" Text="Filter on Company Direction Category" meta:resourcekey="FilterDirectionCategory" EnableViewState="false" />
                                                </asp:Label></div><div class="col-md-6">
                                                <div class="ddpDivision ">

                                                    <asp:DropDownList Width="250px"
                                                        ID="ddlFilterDirectionCategory" runat="server" CssClass="chkliststyle form-control" AppendDataBoundItems="False"
                                                        RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                                                        DataValueField="value" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterDirectionCategory_OnSelectedIndexChanged"
                                                        Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; width: 100%; color: white; overflow: auto;">
                                                    </asp:DropDownList>
                                                </div>

                                            </div>

                                        </div>
                                        <div id="tabs-container manager_table">
                                            <asp:GridView ID="gvGrid_Element" runat="server" AutoGenerateColumns="False" CellPadding="0"
                                                Width="100%" GridLines="None" DataKeyNames="dirID" CssClass="table1 table-striped table-bordered table-hover table-checkable datatable"
                                                  EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'
                                                BackColor="White"
                                                OnRowCommand="gvGrid_Element_RowCommand">

                                                <HeaderStyle CssClass="aa" />
                                                <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                                <AlternatingRowStyle BackColor="White" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="SrNo" meta:resourcekey="SrNo">
                                                        <ItemTemplate>
                                                            <%# Container.DataItemIndex + 1 %>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="center" CssClass="hidden-xs" Width="50px" />
                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                            Width="3%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />


                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="Company Direction Elements" meta:resourcekey="comdireleres">
                                                        <ItemTemplate>
                                                            <%if (Convert.ToString(Session["Culture"]) == "Danish")
                                                              {%>
                                                            <%#Eval("dirNameDN") %>
                                                            <%} %>
                                                            <%else
                                                              { %>
                                                            <%#Eval("dirName") %>
                                                            <%} %>
                                                           
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                            Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                        <FooterTemplate>
                                                            <asp:Label runat="server" ID="asdf">
                                                                <asp:Literal ID="Literal17" runat="server" EnableViewState="false" /></asp:Label></FooterTemplate></asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Company Direction Catetgory" meta:resourcekey="comdirecatres">
                                                        <ItemTemplate>
<%--                                                            <%if (Convert.ToString(Session["Culture"]) == "Danish")--%>
<%--                                                              {%>--%>
<%--                                                            <%#Eval("catNameDN") %>--%>
<%--                                                            <%} %>--%>
<%--                                                            <%else--%>
<%--                                                              { %>--%>
<%--                                                            <%#Eval("catName") %>--%>
<%--                                                            <%} %>--%>
                                                            <%#Eval("catNameInLang") %>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                            Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Description" meta:resourcekey="descriptionresource">
                                                        <ItemTemplate>
                                                            <%#Eval("dirDescription") %>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                            Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Seq. No" meta:resourcekey="seqnoresource">
                                                        <ItemTemplate>
                                                            <%#Eval("dirIndex") %>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="hidden-xs" />
                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                            Width="5%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="" ItemStyle-Width="50px">
                                                        <ItemTemplate>
                                                            <asp:HiddenField runat="server" ID="hdnActUmId" Value='<%# Eval("dirCreatedBy") %>' />
                                                            <asp:Panel runat="server" ID="viewPanel">
                                                                <i class="fa fa-pencil"></i>
                                                                <asp:LinkButton ID="lnkEdit" CausesValidation="false" CssClass="def" runat="server" CommandName="EditRow" CommandArgument='<%# Eval("dirID") %>'
                                                                    ToolTip="Edit" meta:resourcekey="btneditres"><asp:Label runat="server" meta:resourcekey="btneditres"></asp:Label></asp:LinkButton>&nbsp;&nbsp;<i class="fa fa-trash-o"></i> 
                                                                
                                                                <asp:LinkButton ID="lnkBtnName" CausesValidation="false" CssClass="def" runat="server"
                                                                    OnClientClick="var ConfirmArchive = function() { return confirm($('.hdnConfirmArchive').text())};return ConfirmArchive();"
                                                                    CommandName="archive" CommandArgument='<%# Eval("dirID") %>'
                                                                    ToolTip="Archive" meta:resourcekey="btndeleteres"><asp:Label runat="server" meta:resourcekey="btndeleteres"></asp:Label></asp:LinkButton>&nbsp;&nbsp;<i class="fa fa-info-circle"></i> 
                                                                
                                                                <asp:LinkButton ID="lnkView" CausesValidation="false" CssClass="def" runat="server" CommandName="ViewRow" CommandArgument='<%# Eval("dirID") %>'
                                                                    ToolTip="View" meta:resourcekey="btnviewres"><asp:Label runat="server" meta:resourcekey="btnviewres"></asp:Label></asp:LinkButton></asp:Panel></ItemTemplate><ItemStyle HorizontalAlign="center" CssClass="hidden-xs" Width="50px" />
                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                            Width="15%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                    </asp:TemplateField>
                                                    <%--<asp:CommandField ShowDeleteButton="True" ButtonType="Link" ShowEditButton="true" showheader="false" >
                                                   
                                                  </asp:CommandField> --%>
                                                </Columns>

                                                <FooterStyle BackColor="#cccccc" Font-Bold="True" ForeColor="Black" HorizontalAlign="Left" />
                                                <RowStyle HorizontalAlign="Center" />

                                            </asp:GridView>
                                        </div>
                                    </div>
                                    <div id="tab-2" class="tab-content">

                                        <div class="col-md-12">
                                            <div class="col-md-6" style="margin-top: 2%;">
                                                <asp:UpdatePanel ID="UpdatePanel3" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <div style="height: 100px;">
                                                            <div class="form-group col-md-12">
                                                                <div class="col-md-2">
                                                                    <asp:Label runat="server" ID="Label28" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                                                        <asp:Literal ID="Literal29" runat="server" Text="Direction Category" meta:resourcekey="DirectionCategoryres" EnableViewState="false" />
                                                                    </asp:Label></div><div class="col-md-6">
                                                                    <div class="ddpDivision ">

                                                                        <asp:DropDownList Width="250px"
                                                                            ID="ddldirCat2" runat="server" CssClass="chkliststyle form-control" AppendDataBoundItems="False"
                                                                            RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                                                                            DataValueField="value" AutoPostBack="true" OnSelectedIndexChanged="ddlCatEle_OnSelectedIndexChanged"
                                                                            Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; width: 100%; color: white; overflow: auto;">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <asp:Button runat="server" ID="btnAssignSelectCatTemplate" meta:resourcekey="btnSelect" Text="Select"
                                                                        CssClass="btn black" OnClick="btnAssignSelectCat_Click"
                                                                        CausesValidation="False"></asp:Button>

                                                                    <asp:Button runat="server" ID="btnAssignResetCatTemplate" Text="Reset" meta:resourcekey="btnreset"
                                                                        CssClass="btn black" OnClick="btnAssignResetCatTemplate_Click"
                                                                        CausesValidation="False"></asp:Button>
                                                                </div>
                                                            </div>
                                                            <div class="form-group  col-md-12">
                                                                <div class="col-md-2">
                                                                    <asp:Label runat="server" ID="Label29" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                                                        <asp:Literal ID="Literal30" runat="server" Text="Direction Element" meta:resourcekey="DirectionElementresource1" EnableViewState="false" />
                                                                    </asp:Label></div><div class="col-md-6">
                                                                    <div class="ddpDivision ">

                                                                        <asp:DropDownList Width="250px"
                                                                            ID="ddldirele" runat="server" CssClass="chkliststyle form-control" AppendDataBoundItems="False"
                                                                            RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="dirName"
                                                                            DataValueField="value" AutoPostBack="false"
                                                                            Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; width: 100%; color: white; overflow: auto;">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <asp:Button runat="server" ID="btnAssignSelectElement" meta:resourcekey="btnSelectres" Text="Select"
                                                                        CssClass="btn btn-default black col-md-12" OnClick="btnAssignSelectElement_Click"
                                                                        CausesValidation="False"></asp:Button>
                                                                </div>
                                                            </div>
                                                        </div>

                                                        <div style="height: 400px; overflow-y: scroll; margin-top: 2%; width: 100%;">
                                                            <asp:GridView ID="gvCatTemplate" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                                                                DataKeyNames="catID" OnRowDataBound="gvCatTemplate_OnRowDataBound"
                                                                meta:resourcekey="NoRecordFound">
                                                                <Columns>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <img alt="PLUS" style="cursor: pointer" class="plussign plus" src="../images/plus.png" onclick="plussign1(this);" />
                                                                            <asp:Panel ID="pnlOrders" runat="server" Style="display: none">

                                                                                <asp:HiddenField runat="server" ID="hdndirID" Value='<%#Eval("catID") %>' />
                                                                                <%--<asp:HiddenField runat="server" ID="hdnIsTmpMandatory" Value='<%#Eval("IsMandatory") %>' />--%>
                                                                                <asp:Label ID="lblNoRecordFoundCat" Visible="false" ForeColor="Red" runat="server" Text="No record found" EnableViewState="false" />
                                                                                <asp:GridView ID="gvelement" runat="server" AutoGenerateColumns="false"
                                                                                    CssClass="table table-striped table-bordered table-hover table-checkable datatable">
                                                                                    <Columns>
                                                                                        <%--  <asp:TemplateField  HeaderText="Category Element">
                                                                        <ItemTemplate>
                                                                           <asp:HiddenField runat="server" ID="hdnID" Value='<%#Eval("dirID") %>' />
                                                                        </ItemTemplate>
                                                                     </asp:TemplateField>--%>
                                                                                        <asp:TemplateField HeaderText="Category Element" meta:resourcekey="catelementres1">
                                                                                            <ItemTemplate>
                                                                                                <%#Eval("dirName") %>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                                Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                                            <%-- <FooterTemplate>
                                                                        <asp:Label runat="server" ID="asdf"> Requested   </asp:Label>
                                                                    </FooterTemplate>--%>
                                                                                        </asp:TemplateField>

                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>
                                                                                                <input type="checkbox" onclick="SelectQuesTemplate(this,'<%# Eval("catID") %>    ','<%# Eval("dirID") %>    ')"
                                                                                                    class='<%# String.Format("Ques{0}", Eval("catID").ToString()) %>' checked="checked"
                                                                                                    id='<%# String.Format("Ques{0}{1}", Eval("catID").ToString(),Eval("dirID").ToString()) %>' />
                                                                                                <asp:HiddenField runat="server" ID="hdnChkQuesTemp" Value="1" EnableViewState="true" />
                                                                                                <asp:HiddenField runat="server" ID="hdnID" Value='<%#Eval("dirID") %>' />
                                                                                                <asp:HiddenField runat="server" ID="txthdnTmpID" Value='<%#Eval("catID") %>' />
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                                Width="5%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </asp:Panel>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" CssClass="hidden-xs" />
                                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                            Width="5%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                    </asp:TemplateField>


                                                                    <asp:TemplateField HeaderText="Category Name" meta:resourcekey="catnameres1">
                                                                        <ItemTemplate>
                                                                            <%#Eval("catNameInLang") %>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                            Width="80%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                        <%-- <FooterTemplate>
                                                    <asp:Label runat="server" ID="lblActivity"> Total   </asp:Label>
                                                </FooterTemplate>--%>
                                                                    </asp:TemplateField>


                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <%-- <asp:CheckBox Width="250px" ID="chkList" runat="server" CssClass="chkliststyle"
                                                        BorderWidth="0" Style="width: 100%;"></asp:CheckBox>--%>
                                                                            <input type="checkbox" name="chekQues" checked="checked"
                                                                                class='<%# String.Format("MainQues{0}", Eval("catID").ToString()) %>'
                                                                                id='<%# String.Format("MainQues{0}", Eval("catID").ToString()) %>'
                                                                                onclick="SelectMainQuesTemplate(this, '<%# Eval("catID") %>' );" />

                                                                            <asp:HiddenField runat="server" ID="hdnChkMainQuesTemp" Value="1" EnableViewState="true" />
                                                                            <%--  <asp:HiddenField runat="server" ID="hdnID" Value='<%#Eval("dirID") %>' />--%>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                            Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                    </asp:TemplateField>
                                                                    <%-- <asp:BoundField ItemStyle-Width="150px" DataField="" HeaderText="City" />--%>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="gvCatTemplate" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </div>
                                            <div class="col-md-6" style="margin-top: 2%;" >
                                                <asp:UpdatePanel ID="UpdatePanel4" runat="server" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <div style="height: 100px;">
                                                            <div class="form-group col-md-12">
                                                                <div class="col-md-2">
                                                                    <asp:Label runat="server" ID="Label23" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                                                        <asp:Literal ID="Literal23" runat="server" Text="Target Department" meta:resourcekey="TargetDepartmentres1" EnableViewState="false" />
                                                                    </asp:Label></div><div class="col-md-6">
                                                                    <div class="ddpDivision ">

                                                                        <asp:DropDownList Width="250px"
                                                                            ID="ddlAssignTeam" runat="server" CssClass="chkliststyle form-control" AppendDataBoundItems="False"
                                                                            RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description" OnSelectedIndexChanged="ddlAssignTeam_OnSelectedIndexChanged"
                                                                            DataValueField="value" AutoPostBack="true"
                                                                            Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; width: 100%; color: white; overflow: auto;">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <asp:Button runat="server" ID="btnAssignSelectTeam" meta:resourcekey="btnSelectres1" Text="Select"
                                                                        CssClass="btn black" OnClick="btnAssignSelectTeam_Click"
                                                                        CausesValidation="False"></asp:Button>

                                                                    <asp:Button runat="server" ID="btnAssignResetTeam" Text="Reset" meta:resourcekey="btnResetRes1"
                                                                        CssClass="btn black" OnClick="btnAssignResetTeam_Click"
                                                                        CausesValidation="False"></asp:Button>
                                                                </div>
                                                            </div>
                                                            <div class="form-group  col-md-12">
                                                                <div class="col-md-2">
                                                                    <asp:Label runat="server" ID="Label25" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                                                        <asp:Literal ID="Literal25" runat="server" Text="Target Individual" meta:resourcekey="TargetIndividualres1" EnableViewState="false" />
                                                                    </asp:Label></div><div class="col-md-6">
                                                                    <div class="ddpDivision ">

                                                                        <asp:DropDownList Width="250px"
                                                                            ID="ddlAssignUser" runat="server" CssClass="chkliststyle form-control" AppendDataBoundItems="False"
                                                                            RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                                                                            DataValueField="value" AutoPostBack="false"
                                                                            Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; width: 100%; color: white; overflow: auto;">
                                                                        </asp:DropDownList>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-4">
                                                                    <asp:Button runat="server" ID="btnAssignSelectTeamUser" meta:resourcekey="Selectbtnres" Text="Select"
                                                                        CssClass="btn btn-default black col-md-12" OnClick="btnAssignSelectTeamUser_Click"
                                                                        CausesValidation="False"></asp:Button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div style="height: 400px; overflow-y: scroll; margin-top: 2%; width: 100%;">
                                                            <asp:GridView ID="gridTeamList" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                                                                DataKeyNames="TeamID" OnRowDataBound="gridTeamList_OnRowDataBoundTeam"
                                                                meta:resourcekey="NoRecordFound">
                                                                <Columns>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <img alt="PLUS" style="cursor: pointer" class="teamplussign teamplus" src="../images/plus.png" onclick="teamplussign1(this);" />
                                                                            <asp:Panel ID="pnlTeamUser" runat="server" Style="display: none">
                                                                                <asp:HiddenField runat="server" ID="hdnTeamID" Value='<%#Eval("TeamID") %>' />
                                                                                <asp:Label ID="lblNoRecordFound" ForeColor="Red" Visible="false" runat="server" Text="No record found" meta:resourcekey="RecordNotfoundResource" EnableViewState="false" />
                                                                                <asp:GridView ID="gvTeamUser" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover table-checkable datatable">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="User Name" meta:resourcekey="usernameres">
                                                                                            <ItemTemplate>
                                                                                                <%#Eval("userFirstName") %>  <%#Eval("userLastName") %>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                                Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />

                                                                                        </asp:TemplateField>
                                                                                        <asp:TemplateField>
                                                                                            <ItemTemplate>

                                                                                                <input type="checkbox"
                                                                                                    onclick="SelectUserTemplate(this,'<%# Eval("TeamID") %>    ','<%# Eval("userId") %>    ')"
                                                                                                    class='<%# String.Format("User{0}", Eval("TeamID").ToString()) %>' checked="checked"
                                                                                                    id='<%# String.Format("User{0}{1}", Eval("TeamID").ToString(),Eval("userId").ToString()) %>' />
                                                                                                <asp:HiddenField runat="server" ID="hdnChkTeamUser" Value="1" EnableViewState="true" />

                                                                                                <asp:HiddenField runat="server" ID="hdnUsernoID" Value='<%#Eval("userId") %>' />
                                                                                                <asp:HiddenField runat="server" ID="txthdnTeamNoID" Value='<%#Eval("TeamID") %>' />

                                                                                            </ItemTemplate>
                                                                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                                Width="5%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />

                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </asp:Panel>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Center" CssClass="hidden-xs" />
                                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                            Width="5%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                    </asp:TemplateField>


                                                                    <asp:TemplateField meta:resourcekey="TeamNameres1" HeaderText="Team Name">
                                                                        <ItemTemplate>
                                                                            <%#Eval("TeamName") %>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                            Width="80%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField>
                                                                        <ItemTemplate>
                                                                            <input type="checkbox" name="chekQues"
                                                                                class='<%# String.Format("TeamUser{0}", Eval("TeamID").ToString()) %>'
                                                                                id='<%# String.Format("TeamUser{0}", Eval("TeamID").ToString()) %>' checked="checked"
                                                                                onclick="SelectTeamUserTemplate(this, '<%# Eval("TeamID") %>');" />

                                                                            <asp:HiddenField runat="server" ID="hdnChkMainTeam" Value="1" EnableViewState="true" />
                                                                            <%-- <input type="checkbox" name="chekQues" class='<%# String.Format("Team{0}", Eval("TeamID").ToString()) %>'
                                                            onclick="SelectTeamTemplate(this, '<%# Eval("TeamID") %>    ');" />--%>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                            Width="5%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </ContentTemplate>
                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="gridTeamList" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </div>
                                        </div>

                                        <div class="col-md-12" style="margin-top: 5%;">
                                            <div class="form-group col-md-12">
                                                <div class="col-md-3" style="padding-top: 10px; text-align: right;">
                                                    <asp:Label runat="server" ID="Label24" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                                        <asp:Literal ID="Literal24" runat="server" Text="Assign Company Direction To Departments/Employees" EnableViewState="false" meta:resourcekey="Assigncomdirtodepartmentres" />
                                                    </asp:Label></div><div class="col-md-9">
                                                    <div class="chat-widget widget-body col-md-12 adncPrivateDiv" style="display: block; -ms-border-radius: 0px; border-radius: 0px">
                                                        <div class="col-md-3 ">
                                                            <asp:TextBox runat="server" CssClass="library_search datepicker1 col-md-2"
                                                                ID="txtSearchStartDate_Public"
                                                                placeholder="Start Date" meta:resourcekey="startdateres1"
                                                                data-mask="99/99/2099" AutoPostBack="False" Style="height: 42px;"
                                                                onmouseover="SetDatePicker();"></asp:TextBox></div><div class="col-md-3 ">
                                                            <asp:TextBox runat="server" CssClass="library_search datepicker1 col-md-2"
                                                                placeholder="End Date" meta:resourcekey="enddaterres1"
                                                                ID="txtSearchEndDate_Public" data-mask="99/99/2099"
                                                                AutoPostBack="False"
                                                                onmouseover="SetDatePicker();"
                                                                Style="height: 42px;"></asp:TextBox></div><div class="col-md-2 " style="margin-top: 5px">
                                                            <asp:Button runat="server" ID="btnAssign" meta:resourcekey="btnAssignres1" Text="Assign" CssClass="btn btn-default black col-md-12"
                                                                CausesValidation="False" OnClick="btnAssign_click" OnClientClick="return GetAllValues();"></asp:Button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="col-md-12">
                                            <asp:GridView ID="grid_DirectionAssign" runat="server" AutoGenerateColumns="False" CellPadding="0"
                                                Width="100%" GridLines="None" DataKeyNames="dassignID" CssClass="table1 table-striped table-bordered table-hover table-checkable datatable"
                                                  EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'
                                                BackColor="White"
                                                meta:resourcekey="GridRecordNotfound"
                                                OnRowDataBound="grid_DirectionAssign_RowDataBound"
                                                OnRowCommand="grid_DirectionAssign_RowCommand">

                                                <HeaderStyle CssClass="aa" />
                                                <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                                <AlternatingRowStyle BackColor="White" />
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <img alt="PLUS" style="cursor: pointer" class="assignplussign assignplus" src="../images/plus.png" onclick="assignplussign1(this);" />
                                                            <asp:Panel ID="pnlOrders" runat="server" Style="display: none">
                                                                <asp:HiddenField runat="server" ID="hdngriddassignID" Value='<%#Eval("dassignID") %>' />
                                                                <asp:HiddenField runat="server" ID="hdngridAssignDirCatID" Value='<%#Eval("DirCatID") %>' />
                                                                <asp:HiddenField runat="server" ID="hdngridAssignTeamID" Value='<%#Eval("TeamID") %>' />

                                                                <asp:HiddenField runat="server" ID="hdngridAssignElementIDs" Value='<%#Eval("ElementIDs") %>' />
                                                                <asp:HiddenField runat="server" ID="hdngridAssignUserIDs" Value='<%#Eval("UserIDs") %>' />

                                                                <asp:Label ID="lblNoRecordFoundQues" Visible="false" ForeColor="Red" runat="server" Text="No record found" meta:resourcekey="RecordNotfoundResource" EnableViewState="false" />
                                                                <div class="col-md-12">
                                                                    <div class="col-md-6">
                                                                        <asp:GridView ID="gvQues_AssignList" runat="server" AutoGenerateColumns="false"
                                                                            CssClass="table table-striped table-bordered table-hover table-checkable datatable">
                                                                            <Columns>
                                                                                <asp:TemplateField meta:resourcekey="elementres1" HeaderText="Element">
                                                                                    <ItemTemplate>
                                                                                        <%#Eval("dirName") %>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />

                                                                                </asp:TemplateField>
                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <asp:GridView ID="gvTeamUser_AssignList" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover table-checkable datatable">
                                                                            <Columns>
                                                                                <asp:TemplateField HeaderText="User Name" meta:resourcekey="usernameres1">
                                                                                    <ItemTemplate>
                                                                                        <%#Eval("userFirstName") %>  <%#Eval("userLastName") %>
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                        Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />

                                                                                </asp:TemplateField>

                                                                            </Columns>
                                                                        </asp:GridView>
                                                                    </div>
                                                                </div>
                                                            </asp:Panel>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Center" CssClass="hidden-xs" />
                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                            Width="5%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                    </asp:TemplateField>

                                                    <asp:TemplateField HeaderText="SrNo" meta:resourcekey="SrNores1">
                                                        <ItemTemplate>
                                                            <%# Container.DataItemIndex + 1 %>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs ItemStyle" Width="50px" />
                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                            Width="2%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Category Name" meta:resourcekey="categorynameres1">
                                                        <ItemTemplate>
                                                            
                                                            <%if (Convert.ToString(Session["Culture"]) == "Danish")
                                                              {%>
                                                            <%#Eval("catNameInLang") %>
                                                            <%} %>
                                                            <%else
                                                              { %>
                                                            <%#Eval("catNameInLang") %>
                                                            <%} %>
<%--                                                            <%#Eval("catName") %>--%>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs ItemStyle" />
                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                            Width="15%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                        <FooterTemplate>
                                                            <asp:Label runat="server" ID="asdf">
                                                                <asp:Literal ID="Literal17" runat="server" meta:resourcekey="TotalCountres1" EnableViewState="false" /></asp:Label></FooterTemplate></asp:TemplateField>
                                                    <asp:TemplateField HeaderText="User" meta:resourcekey="UserResource1">
                                                        <ItemTemplate>
                                                            <%#Eval("TeamName") %>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs ItemStyle" />
                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                            Width="15%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Start Date-End Date" meta:resourcekey="startdateenddateres1" >
                                                        <ItemTemplate>
                                                            <%#Eval("StartDate") %> - <%#Eval("EndDate") %></ItemTemplate><ItemStyle HorizontalAlign="Center" CssClass="hidden-xs" />
                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                            Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                    </asp:TemplateField>


                                                    <asp:TemplateField HeaderText="" ItemStyle-Width="50px"
                                                        meta:resourcekey="TemplateFieldResource3">
                                                        <ItemTemplate>
                                                            <asp:HiddenField runat="server" ID="hdnassignID" Value='<%# Eval("dassignID") %>' />
                                                            <asp:Panel runat="server" ID="viewPanel">
                                                                <i class="fa fa-trash-o"></i>
<%--                                                                <asp:LinkButton ID="lnkBtnName" CssClass="def" runat="server" CommandName="archive" CommandArgument='<%# Eval("dassignID") %>'--%>
<%--                                                                    ToolTip="Archive" meta:resourcekey="archiveres" CausesValidation="false"--%>
<%--                                                                    OnClientClick="var ConfirmArchive = function() { return confirm($('.hdnConfirmArchive').text())};return ConfirmArchive();">--%>
<%--                                                                    <asp:Label runat="server" meta:resourcekey="archiveres1"></asp:Label>--%>
                                                                    
                                                                    
                                                                    <asp:LinkButton ID="lnkBtnName" CausesValidation="false" CssClass="def" runat="server"
                                                                    OnClientClick="var ConfirmArchive = function() { return confirm($('.hdnConfirmArchive').text())};return ConfirmArchive();"
                                                                    CommandName="archive" CommandArgument='<%# Eval("dassignID") %>'
                                                                    ToolTip="Archive" meta:resourcekey="archiveres">
                                                                        <asp:Label ID="Label7" runat="server" meta:resourcekey="archiveres1"></asp:Label>
                                                                    </asp:LinkButton>&nbsp;&nbsp;
                                                                    

                                                                </asp:Panel></ItemTemplate><ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                            Width="5%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <FooterStyle BackColor="#cccccc" Font-Bold="True" ForeColor="Black" HorizontalAlign="Left" />
                                            </asp:GridView>
                                        </div>


                                    </div>
                                    <div id="tab-3" class="tab-content">
                                        <asp:UpdatePanel ID="UpdatePanel2" runat="server">
                                            <ContentTemplate>
                                                <div style="height: 50%">
                                                    <asp:GridView ID="gridPublishDirection" runat="server" AutoGenerateColumns="False" CellPadding="0"
                                                        Width="100%" GridLines="None" DataKeyNames="dassignID" CssClass="table1 table-striped table-bordered table-hover table-checkable datatable"
                                                          EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'
                                                        BackColor="White"
                                                        meta:resourcekey="GridRecordNotfound"
                                                        OnRowDataBound="gridPublishDirection_RowDataBound" OnRowCommand="gridPublishDirection_OnRowCommand">
                                                        <HeaderStyle CssClass="aa" />
                                                        <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                                        <AlternatingRowStyle BackColor="White" />
                                                        <Columns>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                      <asp:HiddenField runat="server" ID="hdnStatusPublish" Value='<%# Eval("Status") %>' />
                                                                    <img alt="PLUS" style="cursor: pointer" class="assignplussign assignplus" src="../images/plus.png" onclick="assignplussign1(this);" />
                                                                    <asp:Panel ID="pnlOrders" runat="server" Style="display: none">
                                                                        <asp:HiddenField runat="server" ID="hdngridPublishAssignID" Value='<%#Eval("dassignID") %>' />

                                                                        <asp:HiddenField runat="server" ID="hdngridPublishCatID" Value='<%#Eval("DirCatID") %>' />
                                                                        <asp:HiddenField runat="server" ID="hdngridPublishTeamID" Value='<%#Eval("TeamID") %>' />

                                                                        <asp:HiddenField runat="server" ID="hdngridPublishElementIDs" Value='<%#Eval("ElementIDs") %>' />
                                                                        <asp:HiddenField runat="server" ID="hdngridPublishUserIDs" Value='<%#Eval("UserIDs") %>' />

                                                                        <asp:Label ID="lblNoRecordFoundQues" Visible="false" ForeColor="Red" runat="server" Text="No record found" meta:resourcekey="RecordNotfoundResource" EnableViewState="false" />
                                                                        <div class="col-md-12">
                                                                            <div class="col-md-4">
                                                                                <asp:GridView ID="gvPublishDirection_AssignList" runat="server" AutoGenerateColumns="false"
                                                                                    CssClass="table table-striped table-bordered table-hover table-checkable datatable">
                                                                                    <Columns>
                                                                                        <asp:TemplateField meta:resourcekey="Elementres2" HeaderText="Element">
                                                                                            <ItemTemplate>
                                                                                                <%#Eval("dirName") %>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                                Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />

                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>

                                                                            <div class="col-md-6">
                                                                                <asp:GridView ID="gvPublishTeamUser_AssignList" runat="server" AutoGenerateColumns="false" CssClass="table table-striped table-bordered table-hover table-checkable datatable">
                                                                                    <Columns>
                                                                                        <asp:TemplateField HeaderText="User Name" meta:resourcekey="usernameres2">
                                                                                            <ItemTemplate>
                                                                                                <%#Eval("userFirstName") %>  <%#Eval("userLastName") %>
                                                                                            </ItemTemplate>
                                                                                            <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                                            <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                                                Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />

                                                                                        </asp:TemplateField>

                                                                                    </Columns>
                                                                                </asp:GridView>
                                                                            </div>
                                                                        </div>
                                                                    </asp:Panel>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="5%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                            </asp:TemplateField>

                                                            <asp:TemplateField HeaderText="SrNo" meta:resourcekey="SrNo">
                                                                <ItemTemplate>
                                                                    <%# Container.DataItemIndex + 1 %>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs ItemStyle" Width="50px" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="2%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Template" meta:resourcekey="templaterres1">
                                                                <ItemTemplate>
                                                                    <%#Eval("catNameInLang") %>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs ItemStyle" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="15%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                <FooterTemplate>
                                                                    <asp:Label runat="server" ID="asdf">
                                                                        <asp:Literal ID="Literal17" runat="server" meta:resourcekey="TotalCount" EnableViewState="false" /></asp:Label></FooterTemplate></asp:TemplateField>
                                                            <asp:TemplateField HeaderText="User" meta:resourcekey="UserResource2">
                                                                <ItemTemplate>
                                                                    <%#Eval("TeamName") %>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs ItemStyle" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="15%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Start Date-End Date" meta:resourcekey="startdateenddateres2">
                                                                <ItemTemplate>
                                                                    <%#Eval("StartDate") %> - <%#Eval("EndDate") %></ItemTemplate><ItemStyle HorizontalAlign="Center" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="Status" meta:resourcekey="statusres1">
                                                                <ItemTemplate>
                                                                  <%--  <%#Eval("Status") %>--%>
                                                                     <asp:Label runat="server" ID="lblStatusPublish"></asp:Label>
                                                                </ItemTemplate>
                                                                <ItemStyle HorizontalAlign="Center" CssClass="hidden-xs" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="5%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="" ItemStyle-Width="50px">
                                                                <ItemTemplate>
                                                                    <asp:Panel runat="server" ID="viewPanel1">

                                                                        <asp:LinkButton runat="server" ID="lbkPublishDirection" meta:resourcekey="btnSelectres2"
                                                                            CssClass="btn btn-default black col-md-12"
                                                                            CommandName='<%# Eval("Status").ToString() == "Publish" ? "UnPublish" : "Publish" %>'
                                                                            CommandArgument='<%# Eval("dassignID") %>'
                                                                            CausesValidation="False"><%# Eval("Status").ToString() == "Publish" ? "UnPublish" : "Publish" %></asp:LinkButton><%--  <asp:LinkButton ID="lbkPublishDirection" CssClass="btn btn-default blue publishButtonStyle" runat="server"
                                                                    CommandName='<%# Eval("Status").ToString() == "Publish" ? "UnPublish" : "Publish" %>'
                                                                    CommandArgument='<%# Eval("dassignID") %>'
                                                                    ToolTip='<%# Eval("Status").ToString() == "Publish" ? "UnPublish" : "Publish" %>'><%# Eval("Status").ToString() == "Publish" ? "UnPublish" : "Publish" %></asp:LinkButton>--%></asp:Panel></ItemTemplate><ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="5%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                            </asp:TemplateField>
                                                            <asp:TemplateField HeaderText="" ItemStyle-Width="50px">
                                                                <ItemTemplate>
                                                                    <asp:HiddenField runat="server" ID="hdnassignID" Value='<%# Eval("dassignID") %>' />
                                                                    <asp:Panel runat="server" ID="viewPanel">
                                                                        <i class="fa fa-trash-o"></i>
                                                                        <%--  <asp:LinkButton ID="lnkBtnName" CssClass="def" runat="server" CommandName="archive" CommandArgument='<%# Eval("dassignID") %>'
                                                                    ToolTip="Archive" meta:resourcekey="Archive" OnClientClick="var ConfirmArchive = function() {  confirm('Are you sure you want to delete?')};return ConfirmArchive();">Delete</asp:LinkButton>--%>


                                                                        <asp:LinkButton runat="server" ID="lnkBtnName"
                                                                            CssClass="def"
                                                                            CommandName="archive"
                                                                            CommandArgument='<%# Eval("dassignID") %>'
                                                                            CausesValidation="False"
                                                                            ToolTip="Archive"
                                                                            meta:resourcekey="resarchive"
                                                                            OnClientClick="var ConfirmArchive = function() { return confirm($('.hdnConfirmArchive').text())};return ConfirmArchive();">
                                                                     <asp:Label runat="server" meta:resourcekey="linkdeleteres"></asp:Label></asp:LinkButton></asp:Panel></ItemTemplate><ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                    Width="5%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                            </asp:TemplateField>

                                                        </Columns>
                                                        <FooterStyle BackColor="#cccccc" Font-Bold="True" ForeColor="Black" HorizontalAlign="Left" />
                                                    </asp:GridView>
                                                </div>
                                                <div style="height: 50%">
                                                    <div class="form-group col-md-12" style="margin-top: 2%; ">
                                                        <div class="col-md-8">
                                                            <asp:Label runat="server" ID="Label3" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px; float: right;">
                                                                <asp:Literal ID="Literal6" runat="server" Text="Display assigned or published company direction" meta:resourcekey="FilterAssignPublishDirection" EnableViewState="false" />
                                                            </asp:Label></div><div class="col-md-4">
                                                            <div class="ddpDivision ">
                                                                <asp:DropDownList Width="250px"
                                                                    ID="ddlFilterAssignPublishDirection" runat="server" CssClass="chkliststyle form-control" AppendDataBoundItems="False"
                                                                    RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                                                                    DataValueField="value" AutoPostBack="true" OnSelectedIndexChanged="ddlFilterAssignPublishDirection_OnSelectedIndexChanged"
                                                                    Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; width: 100%; color: white; overflow: auto;">
                                                                    
                                                                    <asp:ListItem Value="All" meta:resourcekey="allres"></asp:ListItem>
                                                                    <asp:ListItem Value="Assign" meta:resourcekey="ResAssign1"></asp:ListItem>
                                                                    <asp:ListItem Value="Publish" meta:resourcekey="publishres1"></asp:ListItem>
                                                                </asp:DropDownList></div></div></div><div class="col-md-12">
                                                        <div class="col-md-4">
                                                            <asp:GridView ID="gvTeamUser_PublishList" runat="server" AutoGenerateColumns="false"
                                                                CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                                                                OnRowCommand="gvTeamUser_PublishList_RowCommand">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Employees" meta:resourcekey="employeesres1">
                                                                        <ItemTemplate>
                                                                            <%#Eval("userFirstName") %>  <%#Eval("userLastName") %>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                            Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />

                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Department" meta:resourcekey="departmentres1">
                                                                        <ItemTemplate>
                                                                            <%#Eval("TeamName") %>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                            Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />

                                                                    </asp:TemplateField>

                                                                    <asp:TemplateField HeaderText="" ItemStyle-Width="50px"
                                                                        meta:resourcekey="TemplateFieldResource3">
                                                                        <ItemTemplate>

                                                                            <asp:Panel runat="server" ID="viewPanel">

                                                                                <asp:LinkButton ID="lnkView12" CssClass="def" runat="server" CommandName="ViewRow" CommandArgument='<%# Eval("userId") %>'
                                                                                    ToolTip="View" meta:resourcekey="resView" CausesValidation="False"><asp:Label runat="server" meta:resourcekey="viewres1"></asp:Label></asp:LinkButton></asp:Panel></ItemTemplate><ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                            Width="15%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                    </asp:TemplateField>

                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                        <div class="col-md-8">
                                                            <asp:GridView ID="gvTeamUser_PublishQuesList" runat="server" AutoGenerateColumns="false" 
                                                                EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'
                                                                CssClass="table table-striped table-bordered table-hover table-checkable datatable">
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="Element" meta:resourcekey="elementres3">
                                                                        <ItemTemplate>
                                                                            <%#Eval("dirName") %>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                            Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Description" meta:resourcekey="resdescription">
                                                                        <ItemTemplate>
                                                                            <%#Eval("dirDescription") %>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                            Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="SeqNo" meta:resourcekey="resseqno">
                                                                        <ItemTemplate>
                                                                            <%#Eval("dirIndex") %>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                            Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                    </asp:TemplateField>
                                                                    <asp:TemplateField HeaderText="Template" meta:resourcekey="restemplate">
                                                                        <ItemTemplate>
                                                                            <%#Eval("catNameInLang") %>
                                                                        </ItemTemplate>
                                                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                                            Width="20%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                                    </asp:TemplateField>

                                                                </Columns>
                                                            </asp:GridView>
                                                        </div>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>


        <div class="form-group col-md-12" style="text-align: right; display: none">
            <a href="Report.aspx" class="btn black pull-right" style="border-radius: 5px; margin-right: 15px; margin-top: 5px; float: right;">
                <asp:Literal ID="Literal5" runat="server" meta:resourcekey="back" Text="back"></asp:Literal></a><a href="#" class="btn btn-primary yellow" id="btnsubmitdoc" style="border-radius: 5px; margin-right: 15px; margin-top: 5px; float: right;"
                    onclick="fnPdfHtmlData();" meta:resourcekey="btnsubmitResource1"><asp:Literal ID="Literal4" runat="server" Text="Next"></asp:Literal></a>&nbsp; <a href="#" class="btn btn-primary yellow" runat="server" id="save" style="border-radius: 5px; margin-right: 15px; margin-top: 5px; float: right; display: none"
                        onclick="fnSaveHtmlData(false);"><asp:Literal ID="Literal3" runat="server" meta:resourcekey="Save" Text="Save"></asp:Literal></a>&nbsp; </div><div style="clear: both">
        </div>
        <!-- TIME LINE -->
    </div>


    <!-- Modal content -->

    <div>
        <div aria-hidden="true" role="dialog" tabindex="-1" class="modal fade col-md-12" id="popupAddNewQuesCat" style="display: none;">
            <div class="modal-dialog" style="width: 70%">
                <div class="modal-content">
                    <div class="modal-header blue yellow-radius">
                        <h4><asp:Label runat="server" meta:resourcekey="Addcomdirectioncatres"></asp:Label></h4></div><div class="modal-body">

                        <div class="col-md-12">
                            <div class="col-md-4" style="padding-top: 20px;">
                                 <label style="float: left"><asp:Label runat="server" meta:resourcekey="comdirectioncatnameres"></asp:Label><label style="color: red;">*</label></label> </div><div class="col-md-5" style="padding-top: 10px;">
                                <asp:TextBox ID="txtname" runat="server" placeholder="Category Name(English)" meta:resourcekey="Catnameinengres"></asp:TextBox></div><div class="col-md-3" style="padding-top: 10px;">
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtname"
                                    ErrorMessage="Please Enter Category Name" CssClass="commonerrormsg" ValidationGroup="chkdoc2"
                                    Display="Dynamic" meta:resourcekey="plsentercatnameres">

                                </asp:RequiredFieldValidator></div></div><div class="col-md-12" style="display:none;">
                            <div class="col-md-4" style="padding-top: 20px;">
                               <label style="float: left"><asp:Label runat="server" meta:resourcekey="companydirectioncatindn"></asp:Label> <label style="color: red;">*</label> </label> </div><div class="col-md-5" style="padding-top: 10px;">
                                <asp:TextBox ID="txtnameDN" runat="server" placeholder="Category Name(Denish)" meta:resourcekey="catnameindanishres"></asp:TextBox></div><div class="col-md-3" style="padding-top: 10px;">
                               <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtnameDN"
                                    ErrorMessage="Please Enter Category Name" CssClass="commonerrormsg" ValidationGroup="chkdoc2"
                                    Display="Dynamic">

                                </asp:RequiredFieldValidator>--%>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="col-md-4" style="padding-top: 10px;">
                                    <label style="float: left"><asp:Label runat="server" meta:resourcekey="companydirectioncatdescres"></asp:Label></label> </div><div class="col-md-5" style="padding-top: 10px;">
                                    <asp:TextBox runat="server" ID="txtdescription" placeholder="Category Description" TextMode="MultiLine" meta:resourcekey="categorydescres"/>
                                </div>
                            </div>
                        </div>


                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="col-md-4" style="padding-top: 20px;">
                                    <label style="float: left"><asp:Label runat="server" meta:resourcekey="companydirectionseqnores"></asp:Label> <label style="color: red;">*</label></label> </div><div class="col-md-3" style="padding-top: 10px;">
                                    <asp:TextBox runat="server" ID="txtno" MaxLength="3" min="0" type="number" max="999" placeholder="Sequence Number" onkeypress="return isNumber(event)" meta:resourcekey="sequencenores"/>
                                </div>
                                <div class="col-md-4" style="padding-top: 10px;">
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtno"
                                        ErrorMessage="Please Enter Sequence Number" CssClass="commonerrormsg" ValidationGroup="chkdoc2"
                                        Display="Dynamic" meta:resourcekey="plsenterseqnores">

                                    </asp:RequiredFieldValidator></div></div></div><div class="form-group">
                            <div class="col-md-12">
                                <div class="col-md-4" style="margin-top: 20px; margin-left: -10px; color: black; font-weight: bold; margin-top: 25px;">
                                    <b>
                                        <asp:Literal ID="Literal12" runat="server" meta:resourcekey="Translations"> </asp:Literal></b></div><div class="col-md-8" style="overflow-y: scroll; height: 150px; margin-left: 10px;">
                                    <asp:GridView ID="gridTranslate" runat="server" AutoGenerateColumns="False" CellPadding="0" Style="background-color: #f9f9f9; margin-bottom: 0px !important;"
                                        Width="100%" GridLines="None" CssClass="table table-striped table-bordered table-hover table-checkable datatable">
                                        <Columns>
                                            <asp:TemplateField HeaderText="Language" meta:resourcekey="languageres">
                                                <ItemTemplate>
                                                    <asp:Label ID="Label1" runat="server" Text='<%# Eval("resLanguage") %>'></asp:Label><asp:HiddenField ID="hdnResLangID" runat="server" Value='<%# Eval("resLangID") %>'></asp:HiddenField>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" Font-Size="14px" />
                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                    Width="10%" ForeColor="White" Font-Size="15px" BorderWidth="0px" Height="20px" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Value" meta:resourcekey="valueres">
                                                <ItemTemplate>
                                                    <asp:TextBox ID="txtTranValue" Value='<%# Eval("LangText") %>' runat="server"></asp:TextBox></ItemTemplate><ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" Font-Size="14px" />
                                                <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                    Width="10%" ForeColor="White" Font-Size="15px" BorderWidth="0px" Height="20px" />
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="col-md-4" style="padding-top: 10px;">
                                </div>
                                <div class="col-md-2" style="padding-top: 10px;">
                                    <asp:Button ID="btnsave" runat="server" Text="Save" CausesValidation="true" OnClick="btnsave_Click" ValidationGroup="chkdoc2" CssClass="btn btn-primary yellow" meta:resourcekey="btnsave" />
                                </div>

                                <div class="col-md-2" style="margin-top: 10px;">
                                    <asp:Button ID="Button1" runat="server" Text="Close" ForeColor="White" BackColor="Black" CssClass="btn btn-default black" ValidationGroup="chkdoc3" meta:resourcekey="btnclose" />
                                </div>

                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>


     
     
    <div class="hover_bkgr_fricc">
        <span class="helper"></span>

        <div class="row">
            <%-- <span class="close"></span>--%>
            <div class="popupCloseButton">&times;</div><h4 class="blue header2">
                <p style="margin-top: 15px; margin-left: 20px;" class="ModalHeader">
                  
                   <asp:Literal ID="lblCompanyDirModalHeader" runat="server" 
                        EnableViewState="true" />
                    
                </p>
            </h4>
            <br />
            <div class="modal-body">

                <div class="col-md-12">
                    <div class="col-md-3" style="margin-top: 5px;">

                        <label style="float: left">
                            <asp:Label runat="server" meta:resourcekey="comdircatres"></asp:Label></label> <label style="color: red; float: left">*</label> </div><div class="col-md-5">
                        <%--       <asp:DropDownList Width="250px"
                                        ID="ddldirCat" runat="server" CssClass="chkliststyle form-control" 
                                         BorderWidth="0" Datafield="catName"
                                        DataValueField = "catID" AutoPostBack="false"
                                        Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; width: 100%; color: white; overflow: auto;">
                         </asp:DropDownList>--%>

                        <asp:DropDownList Width="250px"
                            ID="ddldirCat" runat="server" CssClass="chkliststyle form-control" AppendDataBoundItems="False"
                            RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description"
                            DataValueField="value" AutoPostBack="false"
                            Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; width: 100%; color: white; overflow: auto;">
                        </asp:DropDownList>
                    </div>

                </div>

                <div class="col-md-12">
                    <div class="col-md-3">

                        <label style="float: left; margin-top: 30px;"><asp:Label runat="server" meta:resourcekey="comdirtitleres"></asp:Label></label> <label style="color: red; margin-top: 30px; float: left">*</label> </div><div class="col-md-5" style="margin-top: 20px;">
                        <asp:TextBox ID="txttitlename" runat="server" placeholder="Title" meta:resourcekey="titleres"></asp:TextBox></div><div class="col-md-3" style="margin-top: 20px;">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txttitlename"
                            ErrorMessage="Please Enter Title" CssClass="commonerrormsg" ValidationGroup="chkdoc1"
                            Display="Dynamic" meta:resourcekey="plsentertitleres">

                        </asp:RequiredFieldValidator></div></div><div class="col-md-12 form-group">
                    <div class="col-md-3">

                        <label style="float: left;"><asp:Label runat="server" meta:resourcekey="comdirtitleres1"></asp:Label></label> <label style="color: red; float: left">*</label> </div><div class="col-md-5">
                        <asp:TextBox ID="txttitlenameDN" runat="server" placeholder="Title(Denish)" meta:resourcekey="titleindanish"></asp:TextBox></div><div class="col-md-3">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txttitlenameDN"
                            ErrorMessage="Please Enter Title" CssClass="commonerrormsg" ValidationGroup="chkdoc1"
                            Display="Dynamic" meta:resourcekey="plsentertitleres1">

                        </asp:RequiredFieldValidator></div></div><div class="col-md-12 form-group">
                    <div class="col-md-3">
                        <label style="float: left;"><asp:Label runat="server" meta:resourcekey="comdirdetailsres"></asp:Label></label> </div><div class="col-md-5">
                        <asp:TextBox ID="txtdetails" runat="server" placeholder="Description" TextMode="MultiLine" meta:resourcekey="descriptionres1"></asp:TextBox></div></div><div class="col-md-12 form-group">
                    <div class="col-md-3">

                        <label style="float: left"><asp:Label runat="server" meta:resourcekey="comdirseqnores"></asp:Label></label> <label style="color: red; margin-top: 0px; float: left">*</label> </div><div class="col-md-3">
                        <asp:TextBox runat="server" ID="txtn" placeholder="Sequence Number" MaxLength="3" min="0" type="number" max="999" onkeypress="return isNumber(event)" meta:resourcekey="seqnores" />
                    </div>
                    <div class="col-md-4">
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtn"
                            ErrorMessage="Please Enter Sequence Number" CssClass="commonerrormsg" ValidationGroup="chkdoc1"
                            Display="Dynamic" meta:resourcekey="plsenterseqnores1">

                        </asp:RequiredFieldValidator></div></div><div class="col-md-12 form-group">
                    <div class="col-md-3">
                        <asp:Label ID="lblCreatorName" runat="server" Style="float: left; font-weight: bold;" Text="Owner/Creator Name" Visible="false"></asp:Label></div><div class="col-md-3">
                        <asp:TextBox runat="server" ID="txtOwnerName" ReadOnly="true" Visible="false" />
                    </div>

                </div>

                <div class="col-md-12 form-group">
                    <div class="col-md-3">
                        <asp:Label ID="lblCreateDate" runat="server" Style="float: left; font-weight: bold;" Text="Create Date" Visible="false"></asp:Label></div><div class="col-md-3">
                        <asp:TextBox runat="server" ID="txtCreateDate" ReadOnly="true" Visible="false" />
                    </div>

                </div>
                <div class="col-md-12 form-group">

                    <div class="col-md-3">
                        <asp:Label runat="server" ID="Label4" Style="color: black; font-weight: bold; opacity: 0.8; margin-top: 4px; float: left; margin-left: 7%;">
                            <asp:Literal ID="Literal10" runat="server" Text="Mandatory" meta:resourcekey="Mandatory" EnableViewState="false" /></asp:Label></div><div class="col-md-6">
                        <asp:RadioButtonList runat="server" ID="roIsMandatory" RepeatDirection="Horizontal" CssClass="inline-rb" TextAlign="left" CellSpacing="-1" CellPadding="-1">
                            <asp:ListItem Text="Yes" meta:resourcekey="Yesres" Value="true" Selected="True" />
                            <asp:ListItem Text="No" meta:resourcekey="Nores1" Value="false" />
                        </asp:RadioButtonList>
                    </div>


                </div>

            </div>
            <div class="col-md-12 form-group" style="padding-left: 12%;">
                <div class="col-md-3">
                    <asp:Label runat="server" Visible="false" ID="Label5" Style="color: black; font-weight: bold; opacity: 0.8; margin-top: 4px; float: left; margin-left: 7%;">
                        <asp:Literal ID="Literal11" runat="server" Text="Mandatory" meta:resourcekey="Mandatory" EnableViewState="false" /></asp:Label></div><div class="col-md-6" style="margin-bottom: 15px;">
                    <asp:Button ID="btns" runat="server" Text="Save" OnClick="btns_Click" ValidationGroup="chkdoc1" CssClass="btn btn-primary yellow" meta:resourcekey="btnsaveres"/>
                    <asp:Button ID="btnUp" runat="server" Text="Update" OnClick="btnUpdate_Click" Visible="false" ValidationGroup="chkdoc1" CssClass="btn btn-primary yellow" meta:resourcekey="btnupdateres" />
                    <asp:Button ID="btnclose" runat="server" Text="Close" ForeColor="White" BackColor="Black" CssClass="btn btn-default black" ValidationGroup="chkdoc4" meta:resourcekey="btncloseres" />
                </div>

            </div>



        </div>
        <asp:HiddenField runat="server" ID="hdnQuesID" />

    </div>




    <!-- /Modal -->



    <script type="text/javascript">
        $(window).load(function () {
            $(".trigger_popup_fricc").click(function () {
                $('.ModalHeader').html($('#ContentPlaceHolder1_Label8').val());
                $('#ContentPlaceHolder1_btnUp').hide();
                $('#ContentPlaceHolder1_btns').show();

                $('#ContentPlaceHolder1_ddldirCat').val('0');
                $('#ContentPlaceHolder1_txttitlename').val('');
                $('#ContentPlaceHolder1_txttitlenameDN').val('');
                $('#ContentPlaceHolder1_txtdetails').val('');
                $('#ContentPlaceHolder1_txtn').val('');
                $('.hover_bkgr_fricc').show();
            });
            //$('.hover_bkgr_fricc').click(function () {
            //    $('.hover_bkgr_fricc').hide();
            //});
            $('.popupCloseButton').click(function () {
                $('.hover_bkgr_fricc').hide();
            });

        });
    </script>


    <script type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
              && (charCode < 48 || charCode > 57))
                return false;

            return true;
        }
    </script>

    <script type="text/javascript">
        function DisplayTab(a) {
            if (a == 1) {
                $("#tab-1").show();
                $(".maintab li").removeClass("current");
                $("#t1").addClass("current");
                $("#tab-2").hide();
                $("#tab-3").hide();
                $("#tab-4").hide();
                $("#tab-5").hide();
            }
            else if (a == 2) {
                $("#tab-1").hide();
                $("#tab-2").show();
                $(".maintab li").removeClass("current");
                $("#t2").addClass("current");
                $("#tab-3").hide();
                $("#tab-4").hide();
                $("#tab-5").hide();
                //  setTimeout(function () { DrawChartAlpPLpPDP(umId); }, 500);
            }
            else if (a == 3) {
                $("#tab-1").hide();
                $("#tab-2").hide();
                $("#tab-3").show();
                $(".maintab li").removeClass("current");
                $("#t3").addClass("current");
                $("#tab-4").hide();
                $("#tab-5").hide();
            }
            //else if (a == 4) {
            //    $("#tab-1").hide();
            //    $("#tab-2").hide();
            //    $("#tab-3").hide();
            //    $("#tab-4").show();
            //    $(".maintab li").removeClass("current");
            //    $("#t4").addClass("current");
            //    $("#tab-5").hide();

            //}
            //else if (a == 5) {
            //    $("#tab-1").hide();
            //    $("#tab-2").hide();
            //    $("#tab-3").hide();
            //    $("#tab-4").hide();
            //    $("#tab-5").show();
            //    $(".maintab li").removeClass("current");
            //    $("#t5").addClass("current");
            //}
        }
    </script>
    <script type="text/javascript">
        //// Get the modal
        //var modal = document.getElementById('myModal');

        //// Get the button that opens the modal
        //var btn = document.getElementById("myBtn");

        //// Get the <span> element that closes the modal
        //var span = document.getElementsByClassName("close")[0];

        //// When the user clicks the button, open the modal 
        //btn.onclick = function () {
        //    modal.style.display = "block";

        //    $('#ContentPlaceHolder1_txtname').val('');
        //    $('#ContentPlaceHolder1_txtnameDN').val('');
        //    $('#ContentPlaceHolder1_txtdescription').val('');
        //    $('#ContentPlaceHolder1_txtno').val('');
        //}

        //// When the user clicks on <span> (x), close the modal
        //span.onclick = function () {
        //    modal.style.display = "none";
        //}

        //// When the user clicks anywhere outside of the modal, close it
        //window.onclick = function (event) {
        //    if (event.target == modal) {
        //        modal.style.display = "none";
        //    }
        //}
    </script>


    <script type="text/javascript">
        $(document).ready(function () {
            setTimeout(function () {
                SetDatePicker();
            }, 1500);

            var yesterday = new Date((new Date()).valueOf() - 1000 * 60 * 60 * 24);

            $(".txtActStartDate").pickadate({
                format: datePickerFormate, disable: [
                    { from: [0, 0, 0], to: yesterday }
                ]
            });


            $(".txtActEndDate").pickadate({
                format: datePickerFormate, disable: [
                    { from: [0, 0, 0], to: yesterday }
                ]
            });


            $(".datepicker1").pickadate({
                format: datePickerFormate
            });

        });
        var datePickerFormate = 'dd/mm/yyyy';
        function SetDatePicker() {

            var yesterday = new Date((new Date()).valueOf() - 1000 * 60 * 60 * 24);

            $(".txtActStartDate").pickadate({
                format: datePickerFormate, disable: [
                    { from: [0, 0, 0], to: yesterday }
                ]
            });


            $(".txtActEndDate").pickadate({
                format: datePickerFormate, disable: [
                    { from: [0, 0, 0], to: yesterday }
                ]
            });


            $(".datepicker1").pickadate({
                format: datePickerFormate
            });

            ///


            $('.txtActStartDate').on('change', function () {
                $('.txtActEndDate').pickadate('picker').set('min', $(this).val());
            });
        }

        function parseJsonDate(jsonDateString) {


            //return new Date(parseInt(jsonDateString.replace('/Date(', '')));

            var formattedDate = new Date(parseInt(jsonDateString.substr(6)));
            var d = formattedDate.getDate();
            var m = formattedDate.getMonth();
            m += 1; // JavaScript months are 0-11
            var y = formattedDate.getFullYear();
            var min = formattedDate.getMinutes();
            var hour = formattedDate.getHours();
            var sec = formattedDate.getSeconds();

            //$("#txtDate").val(d + "." + m + "." + y);
            return d + "/" + m + "/" + y + " " + hour + ":" + min + ":" + sec;
        }

        function formatDate(d) {
            if (hasTime(d)) {
                var s = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
                s += ' ' + d.getHours() + ':' + zeroFill(d.getMinutes()) + ':' + zeroFill(d.getSeconds());
            } else {
                var s = (d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear();
            }

            return s;
        }
    </script>
    <script type="text/javascript">
        function SelectQuesTemplate(a,b,c) {
            c=c.trim();
            b=b.trim();
            if(a.checked)
            {
                debugger;
                $('#Ques'+b+c).next().val('1');
                if($('#ContentPlaceHolder1_hdnSelectedDirIDs').val()!='' && $('#ContentPlaceHolder1_hdnSelectedDirIDs').val()!=null && $('#ContentPlaceHolder1_hdnSelectedDirIDs').val()!="0")
                {
                  
                    var tmp=$('#ContentPlaceHolder1_hdnSelectedDirIDs').val();
                    var array = tmp.split(',');

                    if(tmp.indexOf(c)<0)
                    {
                        array.push(c);
                        $('#ContentPlaceHolder1_hdnSelectedDirIDs').val(array.join(','));
                    }
                }
                else{
                    $('#ContentPlaceHolder1_hdnSelectedDirIDs').val(c);
                }
            }
            else{
                debugger;
                $('#Ques'+b+c).next().val('0');
                if($('#ContentPlaceHolder1_hdnSelectedDirIDs').val()!='' && $('#ContentPlaceHolder1_hdnSelectedDirIDs').val()!=null && $('#ContentPlaceHolder1_hdnSelectedDirIDs').val()!="0")
                {
                    var tmp=$('#ContentPlaceHolder1_hdnSelectedDirIDs').val();
                    var array = tmp.split(',');
                    if(tmp.indexOf(c)>-1)
                    {
                        index=(array.indexOf(c)) ;
                        array.splice(index, 1);
                        $('#ContentPlaceHolder1_hdnSelectedDirIDs').val(array.join(','));
                    }
                }
                $('.MainQues'+b).prop("checked",false);
            }
            //alert($('#Ques'+b+c).next().val());
        }
        function SelectMainQuesTemplate(a,b) {
            debugger;
            b=b.trim();
            if($('.MainQues'+b).prop("checked")==false)
            {
                $('#MainQues'+b).next().val('0');
                if($('#ContentPlaceHolder1_hdnSelectedCatIDs').val()!='' && $('#ContentPlaceHolder1_hdnSelectedCatIDs').val()!=null && $('#ContentPlaceHolder1_hdnSelectedCatIDs').val()!="0")
                {
                    var tmp=$('#ContentPlaceHolder1_hdnSelectedCatIDs').val();
                    var array = tmp.split(',');
                    if(tmp.indexOf(b)>-1)
                    {
                        index=(array.indexOf(b)) ;
                        array.splice(index, 1);
                        $('#ContentPlaceHolder1_hdnSelectedCatIDs').val(array.join(','));
                    }
                }
                $('.Ques'+b).prop("checked", false);
                $('.Ques'+b).each(function() {
                    $(this).next().val('0');
                });
            }
            else
            {
                $('#MainQues'+b).next().val('1');
                if($('#ContentPlaceHolder1_hdnSelectedCatIDs').val()!='' && $('#ContentPlaceHolder1_hdnSelectedCatIDs').val()!=null && $('#ContentPlaceHolder1_hdnSelectedCatIDs').val()!="0")
                {
                    var tmp=$('#ContentPlaceHolder1_hdnSelectedCatIDs').val();
                    var array = tmp.split(',');
                    if(tmp.indexOf(b)<0)
                    {
                        array.push(b);
                        $('#ContentPlaceHolder1_hdnSelectedCatIDs').val(array.join(','));
                    }
                }
                else{
                    $('#ContentPlaceHolder1_hdnSelectedCatIDs').val(b);
                }

                $('.Ques'+b).prop("checked", true);
                
                $('.Ques'+b).each(function() {
                    $(this).next().val('1');
                });
            }
        }
        function SelectUserTemplate(a,b,c) {
            c=c.trim();
            b=b.trim();

            if(a.checked)
            {
                $('#User'+b+c).next().val('1');
                if($('#ContentPlaceHolder1_hdnSelectedUserIDs').val()!='' && $('#ContentPlaceHolder1_hdnSelectedUserIDs').val()!=null && $('#ContentPlaceHolder1_hdnSelectedUserIDs').val()!="0")
                {
                  
                    var tmp=$('#ContentPlaceHolder1_hdnSelectedUserIDs').val();
                    var array = tmp.split(',');

                    if(tmp.indexOf(c)<0)
                    {
                        array.push(c);
                        $('#ContentPlaceHolder1_hdnSelectedUserIDs').val(array.join(','));
                    }
                }
                else{
                    $('#ContentPlaceHolder1_hdnSelectedUserIDs').val(c);
                }
            }
            else{
                $('#User'+b+c).next().val('0');
                if($('#ContentPlaceHolder1_hdnSelectedUserIDs').val()!='' && $('#ContentPlaceHolder1_hdnSelectedUserIDs').val()!=null && $('#ContentPlaceHolder1_hdnSelectedUserIDs').val()!="0")
                {
                    var tmp=$('#ContentPlaceHolder1_hdnSelectedUserIDs').val();
                    var array = tmp.split(',');
                    if(tmp.indexOf(c)>-1)
                    {
                        index=(array.indexOf(c)) ;
                        array.splice(index, 1);
                        $('#ContentPlaceHolder1_hdnSelectedUserIDs').val(array.join(','));
                    }
                }
                $('.TeamUser'+b).prop("checked",false);
            }
            //alert($('#ContentPlaceHolder1_hdnSelectedUserIDs').val());
        }
        function SelectTeamUserTemplate(a,b) {
            debugger;
            b=b.trim();
            if($('.TeamUser'+b).prop("checked")==false)
            {
                if($('#ContentPlaceHolder1_hdnSelectedTeamIDs').val()!='' && $('#ContentPlaceHolder1_hdnSelectedTeamIDs').val()!=null && $('#ContentPlaceHolder1_hdnSelectedTeamIDs').val()!="0")
                {
                    var tmp=$('#ContentPlaceHolder1_hdnSelectedTeamIDs').val();
                    var array = tmp.split(',');
                    if(tmp.indexOf(b)>-1)
                    {
                        index=(array.indexOf(b)) ;
                        array.splice(index, 1);
                        $('#ContentPlaceHolder1_hdnSelectedTeamIDs').val(array.join(','));
                    }
                }
                $('.User'+b).prop("checked", false);

                $('.User'+b).each(function() {
                    $(this).next().val('0');
                });
            }
            else
            {
                if($('#ContentPlaceHolder1_hdnSelectedTeamIDs').val()!='' && $('#ContentPlaceHolder1_hdnSelectedTeamIDs').val()!=null && $('#ContentPlaceHolder1_hdnSelectedTeamIDs').val()!="0")
                {
                    var tmp=$('#ContentPlaceHolder1_hdnSelectedTeamIDs').val();
                    var array = tmp.split(',');
                    if(tmp.indexOf(b)<0)
                    {
                        array.push(b);
                        $('#ContentPlaceHolder1_hdnSelectedTeamIDs').val(array.join(','));
                    }
                }
                else{
                    $('#ContentPlaceHolder1_hdnSelectedTeamIDs').val(b);
                }

                $('.User'+b).prop("checked", true);
                $('.User'+b).each(function() {
                    $(this).next().val('1');
                });
            }
            //alert($('#ContentPlaceHolder1_hdnSelectedTeamIDs').val());
        }
    </script>


</asp:Content>
