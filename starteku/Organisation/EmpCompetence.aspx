﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/Employee.master" AutoEventWireup="true" CodeFile="EmpCompetence.aspx.cs" Inherits="Organisation_EmpCompetence" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        #tab-content div {
            display: none;
        }

        .button {
            background: none repeat scroll 0px 0px #E9E9E9;
            color: #444;
            display: block;
            font-size: 14px;
            padding: 10px;
            text-align: center;
            border: 1px solid #B2B2B2;
            background: none repeat scroll 0% 0% #038FF5;
            color: #FFF;
            position: relative;
            width: 100px;
            margin-right: 10px;
        }

        .radio_style {
            Width: 100%;
            background-color: White;
            margin-top: -16px;
        }

            .radio_style tr {
            }

                .radio_style tr td {
                    padding: 10px;
                    width: 50%;
                    background-color: white;
                    border: 0px solid gray;
                }

                    .radio_style tr td label {
                        margin-left: 10px;
                        width: 50%;
                        text-wrap: none;
                    }
    </style>

    <script type="text/javascript">
        var count = 1;

        window.onload = function () {
            var child_count = $("#tab-content > div").length;
            if (child_count == 1) {
                btn_next.style.display = "none";
                btn_pre.style.display = "none";
                btn_submit.style.display = "block";
            }
        }

        function reset_all() {
            var child_count = $("#tab-content > div").length;
            var i = 1;
            for (i = 1; i <= child_count; i++) {

                var div = document.getElementById("tab" + i);
                div.style.display = "none";
                div.style.backgroundColor = "white";
                div.style.border = "1px solid #D0D0D0";
            }
        }

        function onNext() {
            reset_all();
            var child_count = $("#tab-content > div").length;
            if (count < child_count) {
                count++;
                if (count == child_count) {
                    btn_next.style.display = "none";
                    btn_pre.style.display = "block";
                    btn_submit.style.display = "block";
                }
                else {
                    btn_next.style.display = "block";
                    btn_pre.style.display = "block";
                }
            }
            else {
                count = child_count;
                btn_next.style.display = "none";
                btn_pre.style.display = "block";
            }

            var div = document.getElementById("tab" + count);
            div.style.display = "block";
            div.style.backgroundColor = "white";
            div.style.border = "1px solid #D0D0D0";
        }

        function onPrevious() {
            reset_all();
            if (count > 1) {
                count--;
                if (count == 1) {
                    btn_next.style.display = "block";
                    btn_pre.style.display = "none";
                    btn_submit.style.display = "none";
                }
                else {
                    btn_next.style.display = "block";
                    btn_pre.style.display = "block";
                    btn_submit.style.display = "none";
                }
            }
            else {
                count = 1;
                btn_next.style.display = "block";
                btn_pre.style.display = "none";
            }
            var div = document.getElementById("tab" + count);
            div.style.display = "block";
            div.style.backgroundColor = "white";
            div.style.border = "1px solid #D0D0D0";
        }

        function ChangePointStyle() {

        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <div class="col-md-6">
        <div class="heading-sec">
            <h1>Employee Skills  <i>Welcome To Bette Bayer! </i></h1>
        </div>
    </div>
    <br />
    <div class="col-md-6" style="margin-top: 30px; width: 40%;">
        <div class="col-md-9" style="width: 100%;">
            <div class="chat-widget widget-body">
                <div class="chat-widget-head yellow">
                    <h4 style="margin-top: -8px;">New Uploaed</h4>
                </div>

                <div class="col-md-12" style="padding: 0!important;">
                    <div style="max-height: 450px; overflow: auto;">
                        <table class="skill_table" border="1" width="100%">
                            <tbody>
                                <asp:Repeater runat="server" ID="rep_Competence" OnItemDataBound="rep_Competence_ItemDataBound">
                                    <ItemTemplate>
                                        <tr>
                                            <td width="45%">
                                                <asp:UpdatePanel runat="server">
                                                    <ContentTemplate>
                                                        <asp:LinkButton ID="txt_Competence" runat="server" CommandName='<%# Eval("comId") %>' OnClick="txt_Competence_Click"><%# Eval("comCompetence") %></asp:LinkButton></td>
                                                    </ContentTemplate>
                                                </asp:UpdatePanel>
                                                <asp:HiddenField ID="hf_point" runat="server" Value='<%# Eval("point") %>' />
                                                <td width="28%"><%# Eval("point") %></td>
                                                <%--<td width="13%">
                                                <form name="form1" method="post" action="">
                                                    <label>
                                                        <input name="checkbox" value="checkbox" type="checkbox">
                                                    </label>
                                                </form>
                                            </td>--%>
                                                <td width="14%">&nbsp;
                                                <div id="pnltest" runat="server" style="margin: -25px auto auto  !important;"></div>
                                                </td>
                                        </tr>
                                    </ItemTemplate>
                                </asp:Repeater>

                            </tbody>
                        </table>
                    </div>
                    <div style="clear: both"></div>
                    <div class="col-xs-12 profile_bottom" style="width: 96%;">
                        <table style="border: 0px;" border="0px" width="100%">
                            <tbody>
                                <tr>
                                    <td width="142">Completed</td>
                                    <td width="915">
                                        <div class="Completed_satus"></div>
                                    </td>
                                    <td width="160">Active</td>
                                    <td width="34">
                                        <div class="active_satus" style="margin-left: -15px;"></div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6" style="margin-top: 30px; width: 58%; background: #f4f4f4; float: left;">
        <div id="tabmenu">
            <table style="margin: 10px 0;">
                <tr>
                    <td>
                        <a id="btn_pre" href="javascript:onPrevious();" class="button" style="display: none;">< Previous</a>
                    </td>
                    <td>
                        <a id="btn_next" href="javascript:onNext();" class="button">Next ></a>
                    </td>
                </tr>
            </table>
            <div id="tab-content" style="background-color: white;">
                <asp:Repeater ID="rep_qua_list" runat="server" OnItemDataBound="rep_qua_list_ItemDataBound">
                    <ItemTemplate>
                        <div id="tab<%# Container.ItemIndex +1 %>" style="background-color: white; border: 1px solid #D0D0D0;">
                            <h2 class="step_titel" style="margin-bottom: 0px;">QUE <%# Container.ItemIndex +1 %> :</h2>
                            <asp:HiddenField ID="hf_comId" runat="server" Value='<%# Eval("comId") %>' />
                            <asp:HiddenField ID="trueAns" runat="server" Value='<%# Eval("trueAns") %>' />
                            <p class="sub_competencie" style="margin-top: 0px;">
                                <label style="width: 100%; border: 0px; border-bottom: 1px solid #D0D0D0;"><%# Eval("comQuestion") %></label>
                                <span style="width: 100%; background-color: white;">
                                    <asp:RadioButtonList ID="rbl_answears" runat="server" RepeatColumns="2" RepeatDirection="Horizontal" CssClass="radio_style"></asp:RadioButtonList>
                                </span>
                            </p>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
                <table id="btn_submit" style="width: 100%; display: none;">
                    <tr>
                        <td></td>
                        <td style="width: 5%;">
                            <asp:Button ID="btn_submit_answear" runat="server" Text="Submit" class="button" OnClick="btn_submit_answear_Click" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>

</asp:Content>

