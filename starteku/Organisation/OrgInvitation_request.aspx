﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master" AutoEventWireup="true" CodeFile="OrgInvitation_request.aspx.cs"
    Inherits="Organisation_OrgInvitation_request" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <link href="css/tooltip.css" rel="stylesheet" />
    <style>
        .alp-point-round {
            background: #67abf2 none repeat scroll 0 0;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" meta:resourcekey="lblMsgResource1"></asp:Label>
    <div class="col-md-6">
        <div class="heading-sec" style="margin-left: 16px">
            <h1>
                <span runat="server" id="sp1"></span><i><span runat="server" id="Employee"></span>
                </i>
            </h1>
        </div>
    </div>

    <%--Header Tab Button BIG----------------------------------------------------------------------20150109--%>
    <div class="col-md-12" style="margin-top: 15px">
        <div class="col-md-2" style="height: 50px;" title="Show All Notifications">
            <div class="stat-boxes widget-body real-time plp-point-round " style="border-radius: 5px;" id="div10">
                <a href="#" onclick="showHideDiv('All');">

                    <h2>
                        <i>
                            <asp:Literal ID="Literal11" runat="server" meta:resourcekey="All" EnableViewState="false" /><%-- <%= CommonMessages.REQUESTFORHELP%>--%></i> </h2>
                </a>
            </div>
        </div>
        <div class="col-md-2" style="height: 50px;" title="Show All Notifications">
            <div class="stat-boxes widget-body real-time plp-point-round " style="border-radius: 5px;" id="divRequest">
                <a href="#" onclick="showHideDiv('ReqestDiv');">

                    <h2>
                        <i>
                            <asp:Literal ID="Literal1" runat="server" meta:resourcekey="REQUESTFORHELP" EnableViewState="false" /><%-- <%= CommonMessages.REQUESTFORHELP%>--%></i> </h2>
                </a>
            </div>
        </div>
        <div class="col-md-2 ">
            <div class="stat-boxes widget-body real-time plp-point-round " style="border-radius: 5px;" id="divsendhelp">
                <a href="#" onclick="showHideDiv('SendHelpDiv');">

                    <h2><i>
                        <asp:Literal ID="Literal2" runat="server" meta:resourcekey="INVITATIONFORHELP" EnableViewState="false" /><%--<%= CommonMessages.INVITATIONFORHELP%>--%></i></h2>
                </a>
            </div>
        </div>
        <div class="col-md-2 ">
            <div class="stat-boxes widget-body real-time plp-point-round " style="border-radius: 5px;" id="divnotification">
                <a href="#" onclick="showHideDiv('NotificationDiv');">

                    <h2><i>
                        <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Notification" EnableViewState="false" /><%-- <%= CommonMessages.Notification%>--%></i></h2>
                </a>
            </div>
        </div>
        <div class="col-md-2 ">
            <div class="stat-boxes widget-body real-time plp-point-round " style="border-radius: 5px;" id="divProvidedHelpbyOther">
                <a href="#" onclick="showHideDiv('ProvidedHelpbyOtherdiv');">

                    <h2><i>
                        <asp:Literal ID="Literal4" runat="server" meta:resourcekey="ProvidedHelpbyOther" EnableViewState="false" /><%-- <%= CommonMessages.Notification%>--%></i></h2>
                </a>
            </div>
        </div>
        <div class="col-md-2 ">
            <div class="stat-boxes widget-body real-time plp-point-round " style="border-radius: 5px;" id="divSuggestedHelp">
                <a href="#" onclick="showHideDiv('SuggestedHelpdiv');">

                    <h2><i>
                        <asp:Literal ID="Literal7" runat="server" meta:resourcekey="ProvidedHelp" EnableViewState="false" /><%-- <%= CommonMessages.Notification%>--%></i></h2>
                </a>
            </div>
        </div>
        <%-- Currently HIDDN 2015 01 13
             <div class="col-md-2 ">
                    <div class="stat-boxes widget-body real-time plp-point-round " style="border-radius: 5px;">
                        <a href="#" onclick="showHideDiv('ReplyHelpDivs');">
                            <h3>
                                $1$<asp:Label ID="lblPLP" runat="server" Text="0"></asp:Label>#1#
                                    </h3>
                         <h4>   <i>All Notification </i></h4>
                             </a>
                    </div>
                </div>--%>
        <%--<div class="col-md-2">
                    <div class="stat-boxes widget-body real-time pdp-point-round" style="border-radius: 5px;">
                        <a href="pdp_points.html">
                            <h3>
                               <asp:Label ID="lblTotal" runat="server"></asp:Label></h3>
                            <i>PDP</i> </a>
                    </div>
                </div>--%>
        <%--   <div class="col-md-2">
                    <div class="stat-boxes widget-body real-time total-point-round " style="border-radius: 5px;">
                        <a href="apl_points.aspx?All=1">
                            <h3>
                                <asp:Label ID="lblTotal" runat="server" Text="0"></asp:Label>
                            </h3>
                            <i>Total</i> </a>
                    </div>
                </div>--%>
    </div>

    <%--Send Help--------------------------------------------------------------------------------%>
    <div class="col-md-12 SendHelpDiv" style="margin-top: 20px; display: none">
        <div id="graph-wrapper">
            <div class="col-md-12">
                <div class="chart-tab">
                    <div id="tabs-container">
                        <h2 class="pointtable_h2" style="background-color: white; border-radius: 5px 5px 0 0; height: 51px; margin-bottom: -20px; padding-top: 13px; width: 100%;">
                            <asp:Literal ID="Literal5" runat="server" meta:resourcekey="INVITATIONFORHELP" EnableViewState="false" />
                            <%--<%= CommonMessages.LISTOF%>--%>  <%--<%= CommonMessages.INVITATIONFORHELP%>--%>
                            <%-- <asp:Label ID="lblname" runat="server" />--%></h2>
                        <asp:GridView ID="gvGrid" runat="server" AutoGenerateColumns="False" CellPadding="0"
                            Width="100%" GridLines="None" DataKeyNames="invId" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                            EmptyDataText='<%# Convert.ToString(GetLocalResourceObject("RecordNotfoundResource.Text")) %>' OnRowCommand="gvGrid_RowCommand"
                            OnRowDataBound="gvGrid_RowDataBound" BackColor="White" meta:resourcekey="gvGridResource1">
                            <HeaderStyle CssClass="aa" />
                            <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:TemplateField HeaderText="ID" meta:resourcekey="TemplateFieldResource1">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="FROM EMPLOYEE NAME"
                                    meta:resourcekey="TemplateFieldResource2">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUNamer1" runat="server" Text="<%# bind('userFirstName') %>" ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="DATE" meta:resourcekey="TemplateFieldResource3">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserNamer18" runat="server"
                                            Text="<%# bind('invCreatedDate') %>"
                                           ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                            
                                <asp:TemplateField HeaderText="STATUS"
                                    meta:resourcekey="STATUS">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserNamer19" runat="server"
                                            Text="<%# bind('status') %>"
                                          ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="STATUS123"
                                    meta:resourcekey="TemplateFieldResource5">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserNamer121" runat="server"
                                            Text="<%# bind('invType') %>" ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="" meta:resourcekey="TemplateFieldResource6">
                                    <ItemTemplate>
                                        <div class="vat" style="width: 15%;">
                                            <p>
                                                <i class=""></i><a href="<%# String.Format("OrgInvitation.aspx?View={0}&HelpTrackId={1}", Eval("invId"), Eval("invHelpTrackId")) %>"
                                                    title="View"></a>
                                            </p>
                                        </div>
                                   
                                        <div class="vat" id="Edit" runat="server"  style="width: 10%;">
                                   
                                         <div><i class="fa fa-pencil" ></i></div> 
                                            <div style="margin-top:-16px;margin-left:10px;">
                                                <a href="<%# String.Format("OrgInvitation.aspx?id={0}", Eval("invId")) %>"
                                                    title="Edit" > <asp:Label runat="server" ID="lblProvideHelpResource" meta:resourcekey="ProvideHelpResource"></asp:Label></a>
                                            </div>   
                                                                               
                                        </div>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="15%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <%--Request Help--------------------------------------------------------------------------------%>

    <div class="col-md-12 ReqestDiv" style="margin-top: 20px;">
        <div id="Div1">
            <div class="col-md-12">
                <div class="chart-tab">
                    <div id="Div2">
                        <h2 class="pointtable_h2" style="background-color: white; border-radius: 5px 5px 0 0; height: 51px; margin-bottom: -20px; padding-top: 13px; width: 100%;">
                            <%--<%= CommonMessages.LISTOF%>--%>  <%--<%= CommonMessages.REQUESTFORHELP%>--%>
                            <asp:Literal ID="Literal6" runat="server" meta:resourcekey="REQUESTFORHELP" EnableViewState="false" />
                            <%-- <asp:Label ID="lblname" runat="server" />--%></h2>
                        <asp:GridView ID="gvRequestData" runat="server" AutoGenerateColumns="False" CellPadding="0"
                            Width="100%" GridLines="None" DataKeyNames="invId" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                            EmptyDataText='<%# Convert.ToString(GetLocalResourceObject("RecordNotfoundResource.Text")) %>' OnRowCommand="gvGrid_RowCommand"
                            OnRowDataBound="gvRequestData_RowDataBound" BackColor="White" meta:resourcekey="gvRequestDataResource1">
                            <HeaderStyle CssClass="aa" />
                            <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>


                                <asp:TemplateField HeaderText="ID" meta:resourcekey="TemplateFieldResource7">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="TO EMPLOYEE NAME"
                                    meta:resourcekey="TemplateFieldResource8">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUNamer2" runat="server" Text="<%# bind('toname') %>" ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="15%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="DATE" meta:resourcekey="TemplateFieldResource9">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserNamer134" runat="server"
                                            Text="<%# bind('invCreatedDate') %>"
                                            ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="15%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="STATUS"
                                    meta:resourcekey="STATUS">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserNamer135" runat="server"
                                            Text="<%# bind('status') %>"
                                            ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="15%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>

                                <%--  <asp:TemplateField HeaderText="Status">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserNamer1" runat="server" 
                                            Text="<%# bind('invType') %>" ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>--%>



                                <asp:TemplateField HeaderText="" meta:resourcekey="TemplateFieldResource11">
                                    <ItemTemplate>
                                        <div class="vat" style="width: 8%;">
                                            <p>
                                                <i class=""></i><a href="<%# String.Format("OrgInvitation.aspx?View={0}&HelpTrackId={1}", Eval("invId"), Eval("invHelpTrackId")) %>"
                                                    title="View"> <asp:Label runat="server" ID="lblDetailResource" meta:resourcekey="DetailResource"></asp:Label></a>
                                            </p>
                                        </div>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="8%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <%--Help Reply List--%>

    <div class="col-md-12 helpReply" style="margin-top: 20px; display: none">
        <div>
            <div class="col-md-12">
                <div class="chart-tab">
                    <div id="Div4">
                        <h2 class="pointtable_h2" style="background-color: white; border-radius: 5px 5px 0 0; height: 51px; margin-bottom: -20px; padding-top: 13px; width: 100%;">
                            <asp:Literal ID="Literal12" runat="server" meta:resourcekey="INVITATIONTOHELP" EnableViewState="false" />

                            <%-- <asp:Label ID="lblname" runat="server" />--%>
                        </h2>
                        <asp:GridView ID="grdViewHelpList" runat="server" AutoGenerateColumns="False" CellPadding="0"
                            Width="100%" GridLines="None" DataKeyNames="invId" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                            EmptyDataText='<%# Convert.ToString(GetLocalResourceObject("RecordNotfoundResource.Text")) %>' OnRowCommand="gvGrid_RowCommand"
                            OnRowDataBound="GvGridHelpList_RowDataBound" BackColor="White" meta:resourcekey="grdViewHelpListResource1">
                            <HeaderStyle CssClass="aa" />
                            <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>


                                <asp:TemplateField HeaderText="ID" meta:resourcekey="TemplateFieldResource12">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="FROM EMPLOYEE NAME"
                                    meta:resourcekey="TemplateFieldResource13">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUNamer3" runat="server" Text="<%# bind('userFirstName') %>" ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="15%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="DATE" meta:resourcekey="TemplateFieldResource14">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserNamer137" runat="server"
                                            Text="<%# bind('invCreatedDate') %>"
                                           ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="15%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>

                                <asp:TemplateField HeaderText="STATUS"
                                    meta:resourcekey="TemplateFieldResource15">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserNamer138" runat="server"
                                            Text="<%# bind('status') %>"
                                           ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="15%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>

                                <%--  <asp:TemplateField HeaderText="Status">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserNamer1" runat="server" 
                                            Text="<%# bind('invType') %>" ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>--%>


                                <asp:TemplateField HeaderText="" meta:resourcekey="TemplateFieldResource16">
                                    <ItemTemplate>
                                        <div class="vat" style="width: 8%;">
                                            <p>
                                                <i class=""></i><a href="<%# String.Format("OrgInvitation.aspx?View={0}", Eval("invId")) %>"
                                                    title="View"></a>
                                            </p>
                                        </div>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="8%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                    </div>
                </div>
            </div>
        </div>
    </div>




    <%--Notification Lisrss--%>

    <div class="col-md-12 NotificationDiv" style="margin-top: 20px; display: none">
        <div id="Div3">
            <div class="col-md-12">
                <div class="chart-tab">
                    <div id="Div5">
                        <h2 class="pointtable_h2" style="background-color: white; border-radius: 5px 5px 0 0; height: 51px; margin-bottom: -20px; padding-top: 13px; width: 100%;">
                            <%--  <%= CommonMessages.LISTOFNOTIFICATION%>  --%>
                            <asp:Literal ID="Literal8" runat="server" meta:resourcekey="LISTOFNOTIFICATION" EnableViewState="false" />
                            <%-- <asp:Label ID="lblname" runat="server" />--%></h2>
                        <asp:GridView ID="grdNotification" runat="server" AutoGenerateColumns="False" CellPadding="0"
                            Width="100%" GridLines="None" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                            EmptyDataText='<%# Convert.ToString(GetLocalResourceObject("RecordNotfoundResource.Text")) %>'
                            BackColor="White" meta:resourcekey="grdNotificationResource1"
                            OnRowDataBound="grdNotification_RowDataBound"
                            >
                            <HeaderStyle CssClass="aa" />
                            <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:TemplateField HeaderText="ID" meta:resourcekey="TemplateFieldResource17">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="5%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="FROM EMPLOYEE NAME"
                                    meta:resourcekey="TemplateFieldResource18">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUNamer4" runat="server" Text="<%# bind('name') %>" ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Duration"
                                    meta:resourcekey="TemplateFieldResource19">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserNamer139" runat="server"
                                            Text="<%# bind('MinAgoValue') %>"
                                           ></asp:Label>
                                         <asp:Label ID="lblMinAgoText" runat="server"
                                            Text="<%# bind('MinAgoText') %>" ></asp:Label>

                                       <%-- <% if(Eval("MinAgoText").ToString()=="Days")
                                           {
                                        %>
                                        <asp:Label ID="Label1" runat="server"
                                            Text="<%# bind('MinAgoText') %>" meta:resourcekey="lblDays"></asp:Label>
                                        <%
                                           }
                                           else if(Eval("MinAgoText").ToString()=="Minutes")
                                           {
                                           }
                                           else if(Eval("MinAgoText").ToString()=="Hours")
                                           {
                                           }
                                           else if(Eval("MinAgoText").ToString()=="Second")
                                           {
                                           }
                                        %>--%>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="SUBJECT"
                                    meta:resourcekey="TemplateFieldResource20">
                                    <ItemTemplate>
                                        <a href="<%# Eval("notpage") %>">
                                            <asp:Label ID="lblUNamer5" runat="server" Text='<%# bind("notsubject") %>' ></asp:Label></a>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="25%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>


                                <asp:TemplateField HeaderText="Type"
                                    meta:resourcekey="TemplateFieldResource21">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserNamer140" runat="server"
                                            Text="<%# bind('nottype') %>" ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>



                            </Columns>
                        </asp:GridView>

                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="col-md-12 ProvidedHelpbyOtherdiv" style="margin-top: 20px; display: none">
        <div id="Div6">
            <div class="col-md-12">
                <div class="chart-tab">
                    <div id="Div7">
                        <h2 class="pointtable_h2" style="background-color: white; border-radius: 5px 5px 0 0; height: 51px; margin-bottom: -20px; padding-top: 13px; width: 100%;">
                            <%--  <%= CommonMessages.LISTOFNOTIFICATION%>  --%>
                            <asp:Literal ID="Literal9" runat="server" meta:resourcekey="ListHelpProvidedbyother" EnableViewState="false" />
                            <%-- <asp:Label ID="lblname" runat="server" />--%></h2>
                        <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" CellPadding="0"
                            Width="100%" GridLines="None" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                            EmptyDataText='<%# Convert.ToString(GetLocalResourceObject("RecordNotfoundResource.Text")) %>'
                            BackColor="White" meta:resourcekey="grdNotificationResource1"
                            OnRowDataBound="GvColleaguesInv_RowDataBound">
                            <HeaderStyle CssClass="aa" />
                            <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:TemplateField HeaderText="ID" meta:resourcekey="TemplateFieldResource17">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="5%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="FROM EMPLOYEE NAME"
                                    meta:resourcekey="TemplateFieldResource18">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUNamer6" runat="server" Text="<%# bind('name') %>" ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Duration"
                                    meta:resourcekey="TemplateFieldResource19">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserNamer12" runat="server"
                                            Text="<%# bind('MinAgoValue') %>"
                                           ></asp:Label>
                                        <asp:Label ID="lblMinAgo" runat="server"
                                            Text="<%# bind('MinAgoText') %>" ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>
                                <%--<asp:TemplateField HeaderText="SUBJECT" 
                                    meta:resourcekey="TemplateFieldResource20">
                                    <ItemTemplate>
                                      <a href="<%# Eval("invsubject") %>">  <asp:Label ID="lblUNamer" runat="server" Text='<%# bind("invsubject") %>' meta:resourcekey="lblUNamerResource5" 
                                            ></asp:Label></a>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="25%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>--%>

                                <asp:TemplateField HeaderText="STATUS"
                                    meta:resourcekey="TemplateFieldResource4">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserNamer13" runat="server"
                                            Text="<%# bind('status') %>"
                                           ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Type"
                                    meta:resourcekey="TemplateFieldResource21">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserNamer14" runat="server"
                                            Text="Provided Help" meta:resourcekey="lblUserNamer1Resource3"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="" meta:resourcekey="TemplateFieldResource6">
                                    <ItemTemplate>
                                        <div class="vat" style="width: 8%;">
                                            <p>
                                                <i class=""></i><a href="<%# String.Format("OrgInvitation.aspx?view={0}", Eval("invId")) %>"
                                                    title="View"><asp:Label runat="server" ID="lblDetailResource1" meta:resourcekey="DetailResource"></asp:Label></a>
                                            </p>
                                        </div>
                                        <%--<div class="vat" style="width: 35%;" id="Edit" runat="server">
                                            <p>
                                                 <i class=""></i><a href="<%#String.Format("OrgInvitation.aspx?view={0}", Eval("invId")) %>"
                                                    title="View"></a>

                                                    <i class="fa fa-pencil"></i><a href="<%# String.Format("OrgInvitation.aspx?view={0}", Eval("invId")) %>"
                                                    title="Edit">View</a>
                                            </p>
                                        </div>--%>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="15%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>


                            </Columns>
                        </asp:GridView>

                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="col-md-12 SuggestedHelpdiv" style="margin-top: 20px; display: none">
        <div id="Div8">
            <div class="col-md-12">
                <div class="chart-tab">
                    <div id="Div9">
                        <h2 class="pointtable_h2" style="background-color: white; border-radius: 5px 5px 0 0; height: 51px; margin-bottom: -20px; padding-top: 13px; width: 100%;">
                            <%--  <%= CommonMessages.LISTOFNOTIFICATION%>  --%>
                            <asp:Literal ID="Literal10" runat="server" meta:resourcekey="LisProvidedHelptoother" EnableViewState="false" />
                            <%-- <asp:Label ID="lblname" runat="server" />--%></h2>
                        <asp:GridView ID="GridView2" runat="server" AutoGenerateColumns="False" CellPadding="0"
                            Width="100%" GridLines="None" CssClass="table table-striped table-bordered table-hover table-checkable datatable"
                            EmptyDataText='<%# Convert.ToString(GetLocalResourceObject("RecordNotfoundResource.Text")) %>'
                            BackColor="White" meta:resourcekey="grdNotificationResource1"
                            OnRowDataBound="GvOfferHelp_RowDataBound">
                            <HeaderStyle CssClass="aa" />
                            <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                            <AlternatingRowStyle BackColor="White" />
                            <Columns>
                                <asp:TemplateField HeaderText="ID" meta:resourcekey="TemplateFieldResource17">
                                    <ItemTemplate>
                                        <%# Container.DataItemIndex + 1 %>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="5%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="TO EMPLOYEE NAME"
                                    meta:resourcekey="TemplateFieldResource8">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUNamer7" runat="server" Text="<%# bind('username') %>" ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Duration"
                                    meta:resourcekey="TemplateFieldResource19">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserNamer15" runat="server"
                                            Text="<%# bind('MinAgoValue') %>"
                                            ></asp:Label>
                                       <asp:Label ID="lbldaytime" runat="server"
                                            Text="<%# bind('MinAgoText') %>" ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>
                                <%--<asp:TemplateField HeaderText="SUBJECT" 
                                    meta:resourcekey="TemplateFieldResource20">
                                    <ItemTemplate>
                                      <a href="<%# Eval("invsubject") %>">  <asp:Label ID="lblUNamer" runat="server" Text='<%# bind("invsubject") %>' meta:resourcekey="lblUNamerResource5" 
                                            ></asp:Label></a>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="25%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>--%>

                                <asp:TemplateField HeaderText="STATUS"
                                    meta:resourcekey="TemplateFieldResource4">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserNamer16" runat="server"
                                            Text="<%# bind('status') %>"
                                            ></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="Type"
                                    meta:resourcekey="TemplateFieldResource21">
                                    <ItemTemplate>
                                        <asp:Label ID="lblUserNamer17" runat="server"
                                            Text="Provided Help" meta:resourcekey="lblUserNamer1Resource3"></asp:Label>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="10%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="" meta:resourcekey="TemplateFieldResource6">
                                    <ItemTemplate>
                                        <div class="vat" style="width: 8%;">
                                            <p>

                                                <i class=""></i><a href="<%# String.Format("OrgInvitation.aspx?view={0}&status1={1}", Eval("invId"), Eval("status")) %>"
                                                    title="View"><asp:Label runat="server" ID="lblDetailResource2" meta:resourcekey="DetailResource"></asp:Label></a>
                                            </p>
                                        </div>
                                        <%--<div class="vat" style="width: 35%;" id="Edit" runat="server">
                                            <p>
                                                 <i class=""></i><a href="<%#String.Format("OrgInvitation.aspx?view={0}", Eval("invId")) %>"
                                                    title="View"></a>

                                                    <i class="fa fa-pencil"></i><a href="<%# String.Format("OrgInvitation.aspx?view={0}", Eval("invId")) %>"
                                                    title="Edit">View</a>
                                            </p>
                                        </div>--%>
                                    </ItemTemplate>
                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                        Width="15%" ForeColor="White" BorderWidth="0px" />
                                </asp:TemplateField>


                            </Columns>
                        </asp:GridView>

                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="modal-footer" style="border-top: 0px !important;">
        <asp:Button runat="server" ID="btnBack" meta:resourcekey="Back" CssClass="btn black pull-right"
            OnClick="btnBack_click" Style="margin: 4px;" />
        <%--<a href="Employee_dashboard.aspx" onclick="" class="btn black pull-right" style="border-radius: 5px;margin-left:-10px;margin-top:5px;float:right;"><asp:Label ID="Label1" runat="server" meta:resourcekey="Back"></asp:Label></h4></a>--%>
    </div>


    <script src="../assets/assets/js/libs/jquery-1.10.2.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            SetExpandCollapse();
            setTimeout(function () {
                // ExampanCollapsMenu();

                // tabMenuClick();
                $(".myCompetence").addClass("active_page");
            }, 500);
        });

        function onLoad() {
            setTimeout(function () {
                ExampanCollapsMenu();
                //alert();
                // tabMenuClick();
                $(".myCompetence").addClass("active_page");
            }, 500);
        }

        function showHideDiv(divName) {
            if (divName == "ReqestDiv") {
                $('.ReqestDiv').show();
                $('.SendHelpDiv').hide();
                $('.helpReply').hide();
                $('.NotificationDiv').fadeOut();
                $('.ProvidedHelpbyOtherdiv').hide();
                $('.SuggestedHelpdiv').hide();

                $("#divRequest").removeClass("plp-point-round");
                $("#divRequest").addClass("alp-point-round");

                $("#divsendhelp").addClass("plp-point-round");
                $("#divnotification").addClass("plp-point-round");
                $("#divProvidedHelpbyOther").addClass("plp-point-round");
                $("#divSuggestedHelp").addClass("plp-point-round");

                $("#divsendhelp").removeClass("alp-point-round");
                $("#divnotification").removeClass("alp-point-round");
                $("#divProvidedHelpbyOther").removeClass("alp-point-round");
                $("#divSuggestedHelp").removeClass("alp-point-round");
                $("#div10").removeClass("alp-point-round");

            } else if (divName == "SendHelpDiv") {
                $('.ReqestDiv').hide();
                $('.SendHelpDiv').show();
                $('.helpReply').hide();
                $('.NotificationDiv').fadeOut();
                $('.ProvidedHelpbyOtherdiv').hide();
                $('.SuggestedHelpdiv').hide();

                $("#divsendhelp").removeClass("plp-point-round");
                $("#divsendhelp").addClass("alp-point-round");

                $("#divRequest").addClass("plp-point-round");
                $("#divnotification").addClass("plp-point-round");
                $("#divProvidedHelpbyOther").addClass("plp-point-round");
                $("#divSuggestedHelp").addClass("plp-point-round");

                $("#divRequest").removeClass("alp-point-round");
                $("#divnotification").removeClass("alp-point-round");
                $("#divProvidedHelpbyOther").removeClass("alp-point-round");
                $("#divSuggestedHelp").removeClass("alp-point-round");
                $("#div10").removeClass("alp-point-round");

            }
            else if (divName == "ReplyHelpDiv") {
                $('.ReqestDiv').hide();
                $('.SendHelpDiv').hide();
                $('.helpReply').show();
                $('.NotificationDiv').fadeOut();
                $('.ProvidedHelpbyOtherdiv').hide();
                $('.SuggestedHelpdiv').hide();
                $("#div10").removeClass("alp-point-round");



            }
            else if (divName == "NotificationDiv") {
                $('.NotificationDiv').fadeIn();
                $('.ReqestDiv').hide();
                $('.SendHelpDiv').hide();
                $('.helpReply').hide();
                $('.ProvidedHelpbyOtherdiv').hide();
                $('.SuggestedHelpdiv').hide();

                $("#divnotification").removeClass("plp-point-round");
                $("#divnotification").addClass("alp-point-round");

                $("#divRequest").addClass("plp-point-round");
                $("#divsendhelp").addClass("plp-point-round");
                $("#divProvidedHelpbyOther").addClass("plp-point-round");
                $("#divSuggestedHelp").addClass("plp-point-round");

                $("#divRequest").removeClass("alp-point-round");
                $("#divsendhelp").removeClass("alp-point-round");
                $("#divProvidedHelpbyOther").removeClass("alp-point-round");
                $("#divSuggestedHelp").removeClass("alp-point-round");
                $("#div10").removeClass("alp-point-round");

            }

            else if (divName == "ProvidedHelpbyOtherdiv") {
                $('.NotificationDiv').fadeOut();
                $('.ReqestDiv').hide();
                $('.SendHelpDiv').hide();
                $('.helpReply').hide();
                $('.ProvidedHelpbyOtherdiv').show();
                $('.SuggestedHelpdiv').hide();


                $("#divProvidedHelpbyOther").removeClass("plp-point-round");
                $("#divProvidedHelpbyOther").addClass("alp-point-round");

                $("#divRequest").addClass("plp-point-round");
                $("#divsendhelp").addClass("plp-point-round");
                $("#divnotification").addClass("plp-point-round");
                $("#divSuggestedHelp").addClass("plp-point-round");

                $("#divRequest").removeClass("alp-point-round");
                $("#divsendhelp").removeClass("alp-point-round");
                $("#divnotification").removeClass("alp-point-round");
                $("#divSuggestedHelp").removeClass("alp-point-round");
                $("#div10").removeClass("alp-point-round");
            }

            else if (divName == "SuggestedHelpdiv") {
                $('.NotificationDiv').fadeOut();
                $('.ReqestDiv').hide();
                $('.SendHelpDiv').hide();
                $('.helpReply').hide();
                $('.ProvidedHelpbyOtherdiv').hide();
                $('.SuggestedHelpdiv').show();

                $("#divSuggestedHelp").removeClass("plp-point-round");
                $("#divSuggestedHelp").addClass("alp-point-round");

                $("#divRequest").addClass("plp-point-round");
                $("#divsendhelp").addClass("plp-point-round");
                $("#divnotification").addClass("plp-point-round");
                $("#divProvidedHelpbyOther").addClass("plp-point-round");

                $("#divRequest").removeClass("alp-point-round");
                $("#divsendhelp").removeClass("alp-point-round");
                $("#divnotification").removeClass("alp-point-round");
                $("#divProvidedHelpbyOther").removeClass("alp-point-round");
                $("#div10").removeClass("alp-point-round");
            } else {
                $('.NotificationDiv').show();
                $('.ReqestDiv').show();
                $('.SendHelpDiv').show();
                $('.helpReply').show();
                $('.ProvidedHelpbyOtherdiv').show();
                $('.SuggestedHelpdiv').show();


                $("#divSuggestedHelp").addClass("plp-point-round");
                $("#div10").addClass("alp-point-round");

                $("#divRequest").addClass("plp-point-round");
                $("#divsendhelp").addClass("plp-point-round");
                $("#divnotification").addClass("plp-point-round");
                $("#divProvidedHelpbyOther").addClass("plp-point-round");

                $("#divSuggestedHelp").removeClass("alp-point-round");
                $("#divRequest").removeClass("alp-point-round");
                $("#divsendhelp").removeClass("alp-point-round");
                $("#divnotification").removeClass("alp-point-round");
                $("#divProvidedHelpbyOther").removeClass("alp-point-round");
            }
        }
    </script>

    <style type="text/css">
        .yellow {
            border-radius: 0px;
        }
    </style>
    <script src="../Scripts/tooltip/jquery.tooltipster.js" type="text/javascript"></script>
    <script src="../Scripts/tooltip/jquery.tooltipster.min.js" type="text/javascript"></script>
</asp:Content>

