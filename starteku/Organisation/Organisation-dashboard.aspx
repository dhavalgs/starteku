﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="Organisation-dashboard.aspx.cs" Inherits="Organisation.Organisation_Organisation_dashboard"
    Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Reference Page="~/Organisation/Manager-dashboard.aspx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <link rel="stylesheet" type="text/css" media="all" href="css/daterangepicker-bs3.css" />
    <!-- Date Range Picker -->
    <link rel="stylesheet" href="css/style.css" type="text/css" />
    <!-- Style -->
    <%--<link rel="stylesheet" href="css/responsive.css" type="text/css" />--%>
    <!-- Responsive -->
    <%--<link href="css/fullcalendar.css" rel="stylesheet" />
    <!-- Full calendar -->
    <link href="css/fullcalendar.print.css" rel="stylesheet" media="print" />
    <!-- Full Calendar -->
    <link href="css/fullcalendar_002.css" rel="stylesheet" />
    <!-- Full calendar -->--%>
    <!-- Script -->
    <%-- <script src="js/jquery-1.10.2.js"></script>--%>
    <!-- Jquery -->
    <!-- Script -->
    <%-- <script src="js/enscroll-0.5.2.min.js"></script>--%>
    <!-- Custom Scroll bar -->
    <%-- <script src="js/moment.js"></script>--%>
    <!-- Date Range Picker -->
    <%-- <script src="js/daterangepicker.js"></script>--%>
    <!-- Date Range Picker -->
    <%-- <script src="js/fullcalendar.min.js"></script>--%>
    <!-- Full Calendar -->

    <%--NEWUPDATE SAURIN--%>

    <link href="css/fullcalendar_002.css" rel="stylesheet" />
    <!-- Full Calendar -->
    <link href="css/fullcalendar.css" rel="stylesheet" />
    <!-- Full calendar -->
    <link href="css/fullcalendar.print.css" rel="stylesheet" media="print" />
    <link href="../Scripts/fullcalendor/fullcalendar.css" rel="stylesheet" />
    <%-- <link href="../Scripts/fullcalendor/fullcalendar.print.css" rel="stylesheet" />--%>
    <script src="../Scripts/fullcalendor/moment.min.js" type="text/javascript"></script>
    <script src="../Scripts/fullcalendor/jquery.min.js" type="text/javascript"></script>
    <script src="../Scripts/fullcalendor/fullcalendar.min.js" type="text/javascript"></script>

    <link href="../Scripts/fullcalendor/pickADate/classic.css" rel="stylesheet" />
    <link href="../Scripts/fullcalendor/pickADate/classic.date.css" rel="stylesheet" />
    <script src="../Scripts/fullcalendor/gcal.js" type="text/javascript"></script>


    <%--<script src="js/daterangepicker.js"></script>--%>
    <script src="../Scripts/fullcalendor/picker.js" type="text/javascript"></script>
    <script src="../Scripts/fullcalendor/picker.date.js" type="text/javascript"></script>
    <script src="../Scripts/fullcalendor/legacy.js" type="text/javascript"></script>



    <%--<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap/3.3.2/css/bootstrap.css" />--%>

    <!-- Include Date Range Picker -->
    <script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/1/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/1/daterangepicker-bs3.css" />

    <link href="../Scripts/fullcalendor/dATEPICKER/jquery.timepicker.css" rel="stylesheet" />
    <script src="../Scripts/fullcalendor/dATEPICKER/jquery.timepicker.js" type="text/javascript"></script>

    <link href="../Scripts/fullcalendor/dATEPICKER/bootstrap-datetimepicker.css" rel="stylesheet" />
    <link href="../Scripts/fullcalendor/dATEPICKER/bootstrap-datetimepicker.min.css" rel="stylesheet" />
    <script src="../Scripts/fullcalendor/dATEPICKER/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <%--joyride---------------------------------------------%>
    <link href="../plugins/joyride/joyride-2.1.css" rel="stylesheet" type="text/css" />
    <%--<script src="../plugins/joyride/jquery-1.10.1.js" type="text/javascript"></script>--%>
    <script src="../plugins/joyride/jquery.cookie.js" type="text/javascript"></script>
    <script src="../plugins/joyride/jquery.joyride-2.1.js" type="text/javascript"></script>
    <script src="../plugins/joyride/modernizr.mq.js" type="text/javascript"></script>

    <script type="text/javascript" src="../plugins/highchart/highstock.js"></script>
    <%-- <script src="../plugins/highchart/highcharts.js" type="text/javascript"></script>--%>
    <%--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js" type="text/javascript"></script>--%>
    <%--    <script src="../Scripts/jquery321.js" type="text/javascript"></script>--%>
    <script src="../plugins/highchart/highcharts-more.js" type="text/javascript"></script>
    <script src="../Scripts/plugins/exporting.js" type="text/javascript"></script>


    <style type="text/css">
        #lines span, #toggle span i {
            color: #333333;
            float: left;
            font-size: 13px;
            line-height: 22px;
            padding: 3px 9px;
        }

        #lines, #toggle {
            border-bottom: 2px solid #F65B5B;
            cursor: pointer;
            float: right;
            height: 32px;
            margin-left: 10px;
            margin-right: 0;
            padding: 0;
            width: 34px;
            background-color: white;
            border: 1px solid #EBEBEB;
            z-index: 100;
        }

        .chkliststyle input {
            width: auto;
            margin-bottom: 0px !important;
            margin-right: 15px;
        }

        .chkliststyle label {
            margin-top: 2px;
            vertical-align: middle;
            font-size: 13px;
        }

        .chat-widget-head {
            float: left;
            width: 100%;
        }

        #ContentPlaceHolder1_chkList input {
            width: auto;
            margin-bottom: 0px !important;
        }

        #ContentPlaceHolder1_chkList label {
            margin-top: 2px;
        }

        .scroll_checkboxes {
            height: 120px;
            width: 100%;
            padding: -5px;
            overflow: auto;
            border: 1px solid #ccc;
        }

        .HiddenText label {
            display: none;
        }

        .cke_skin_kama .cke_editor {
            display: table !important;
            width: 100%;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <script type="text/javascript">

        setTimeout(function () {
            $('.highcharts-contextbutton').on('click', function () {
                setTimeout(function () {
                    $(".highcharts-menu-item").each(function (a) {
                        if ($(this).html() == "Print chart") {
                            $(this).html($(this).html().replace('Print chart', $('#ContentPlaceHolder1_hdnPrintChart').val()));
                        } else if ($(this).html() == "Download PNG image") {
                            $(this).html($(this).html().replace('Download PNG image', $('#ContentPlaceHolder1_hdnDownloadPNG').val()));
                        }
                        else if ($(this).html() == "Download JPEG image") {
                            $(this).html($(this).html().replace('Download JPEG image', $('#ContentPlaceHolder1_hdnDownloadJPEG').val()));
                        }
                        else if ($(this).html() == "Download PDF document") {
                            $(this).html($(this).html().replace('Download PDF document', $('#ContentPlaceHolder1_hdnDownloadPDF').val()));
                        }
                        else if ($(this).html() == "Download SVG vector image") {
                            $(this).html($(this).html().replace('Download SVG vector image', $('#ContentPlaceHolder1_hdnDownloadSVG').val()));
                        }
                    });

                }, 500);
            });
        }, 2000);

    </script>

    <div style="display: none">
        <asp:Literal ID="hdnSystemUpdated" runat="server" meta:resourcekey="SystemUpdate" />

        <asp:HiddenField ID="columnchartname" runat="server" Value="Column" meta:resourcekey="Column" />
        <asp:HiddenField ID="barchartname" runat="server" Value="BarChart" meta:resourcekey="BarChart" />
        <asp:HiddenField ID="spiderchartname" runat="server" Value="SpiderChart" meta:resourcekey="SpiderChart" />

      

        <asp:HiddenField ID="hdnSelectDivisions" runat="server" Value="Select Division" meta:resourcekey="SelectDivisionsResource" />
        <asp:HiddenField ID="hdnSelectJob" runat="server" Value="Select Jobtype" meta:resourcekey="SelectJobResource" />
        <asp:HiddenField ID="hdnSelectTeam" runat="server" Value="Select Team" meta:resourcekey="SelectTeamResource" />
        <asp:HiddenField ID="Month" runat="server" Value="Month" meta:resourcekey="Month" />
        <asp:HiddenField ID="Week" runat="server" Value="Week" meta:resourcekey="Week" />
        <asp:HiddenField ID="Day" runat="server" Value="Day" meta:resourcekey="Day" />
        <asp:HiddenField ID="Today" runat="server" Value="Today" meta:resourcekey="Today" />
        <asp:HiddenField ID="sun" runat="server" Value="sun" meta:resourcekey="sun" />
        <asp:HiddenField ID="mon" runat="server" Value="mon" meta:resourcekey="mon" />
        <asp:HiddenField ID="tue" runat="server" Value="tue" meta:resourcekey="tue" />
        <asp:HiddenField ID="wed" runat="server" Value="wed" meta:resourcekey="wed" />
        <asp:HiddenField ID="thu" runat="server" Value="thu" meta:resourcekey="thu" />
        <asp:HiddenField ID="fri" runat="server" Value="fri" meta:resourcekey="fri" />
        <asp:HiddenField ID="sat" runat="server" Value="sat" meta:resourcekey="sat" />
        <asp:HiddenField ID="Sunday" runat="server" Value="sun" meta:resourcekey="Sunday" />
        <asp:HiddenField ID="Monday" runat="server" Value="mon" meta:resourcekey="Monday" />
        <asp:HiddenField ID="Tuesday" runat="server" Value="tue" meta:resourcekey="Tuesday" />
        <asp:HiddenField ID="Wednesday" runat="server" Value="wed" meta:resourcekey="Wednesday" />
        <asp:HiddenField ID="Thursday" runat="server" Value="thu" meta:resourcekey="Thursday" />
        <asp:HiddenField ID="Friday" runat="server" Value="fri" meta:resourcekey="Friday" />
        <asp:HiddenField ID="Saturday" runat="server" Value="sat" meta:resourcekey="Saturday" />
        <asp:HiddenField ID="january" runat="server" Value="sun" meta:resourcekey="january" />
        <asp:HiddenField ID="february" runat="server" Value="mon" meta:resourcekey="february" />
        <asp:HiddenField ID="march" runat="server" Value="tue" meta:resourcekey="march" />
        <asp:HiddenField ID="april" runat="server" Value="wed" meta:resourcekey="april" />
        <asp:HiddenField ID="may" runat="server" Value="thu" meta:resourcekey="may" />
        <asp:HiddenField ID="june" runat="server" Value="fri" meta:resourcekey="june" />
        <asp:HiddenField ID="july" runat="server" Value="sat" meta:resourcekey="july" />
        <asp:HiddenField ID="august" runat="server" Value="thu" meta:resourcekey="august" />
        <asp:HiddenField ID="september" runat="server" Value="fri" meta:resourcekey="september" />
        <asp:HiddenField ID="october" runat="server" Value="sat" meta:resourcekey="october" />
        <asp:HiddenField ID="november" runat="server" Value="sat" meta:resourcekey="november" />
        <asp:HiddenField ID="december" runat="server" Value="sat" meta:resourcekey="december" />
        <asp:HiddenField ID="jan" runat="server" Value="sun" meta:resourcekey="jan" />
        <asp:HiddenField ID="feb" runat="server" Value="mon" meta:resourcekey="feb" />
        <asp:HiddenField ID="mar" runat="server" Value="tue" meta:resourcekey="mar" />
        <asp:HiddenField ID="apr" runat="server" Value="wed" meta:resourcekey="apr" />
        <asp:HiddenField ID="mays" runat="server" Value="thu" meta:resourcekey="mays" />
        <asp:HiddenField ID="jun" runat="server" Value="fri" meta:resourcekey="jun" />
        <asp:HiddenField ID="jul" runat="server" Value="sat" meta:resourcekey="jul" />
        <asp:HiddenField ID="aug" runat="server" Value="thu" meta:resourcekey="aug" />
        <asp:HiddenField ID="sept" runat="server" Value="fri" meta:resourcekey="sept" />
        <asp:HiddenField ID="oct" runat="server" Value="sat" meta:resourcekey="oct" />
        <asp:HiddenField ID="nov" runat="server" Value="sat" meta:resourcekey="nov" />
        <asp:HiddenField ID="dec" runat="server" Value="sat" meta:resourcekey="dec" />
        <asp:HiddenField ID="lan" runat="server" Value="" />

        <asp:HiddenField ID="hdnChartLevel" runat="server" Value="" meta:resourcekey="ChartLevelRes" />
        <asp:HiddenField ID="hdnChartTarget" runat="server" Value="" meta:resourcekey="TargetRes" />
        <asp:HiddenField ID="hdnChartActual" runat="server" Value="" meta:resourcekey="ActualRes" />

         <asp:HiddenField ID="hdnChartMax" runat="server" Value="" meta:resourcekey="MaxRes" />

        <asp:HiddenField ID="hdnNoOption" runat="server" Value="" meta:resourcekey="NoOptionRes" />

        <asp:HiddenField ID="hdnSelectAll" runat="server" Value="Select All" meta:resourcekey="SelectAll" />
        <asp:HiddenField ID="hdnUnSelectAll" runat="server" Value="UnSelect All" meta:resourcekey="UnSelectAll" />

        <asp:HiddenField ID="hdnSelectGender" runat="server" Value="Select Gender" meta:resourcekey="SelectGenderResource" />

        <asp:HiddenField ID="hdnPrintChart" runat="server" Value="" meta:resourcekey="PrintChartRes" />
        <asp:HiddenField ID="hdnDownloadPNG" runat="server" Value="" meta:resourcekey="DownloadPNGRes" />
        <asp:HiddenField ID="hdnDownloadJPEG" runat="server" Value="" meta:resourcekey="DownloadJPEGRes" />
        <asp:HiddenField ID="hdnDownloadPDF" runat="server" Value="" meta:resourcekey="DownloadPDFRes" />
        <asp:HiddenField ID="hdnDownloadSVG" runat="server" Value="" meta:resourcekey="DownloadSVGRes" />

         <asp:HiddenField ID="hdnGraphUserSelectedID" runat="server" Value=""/>

    </div>
    <div id="eventContent" title="Event Details" style="display: none; background: none repeat scroll 0 0 white; border: 1px solid gray; border-radius: 5px; margin: 200px; padding: 15px; position: fixed; width: 300px; z-index: 2147483647; overflow: auto">
        <label class="fontcolor Subject">
            Subject:</label>
        <span id="subject"></span>
        <br />
        <label class="fontcolor Start">
            Start:
        </label>
        <span id="startTime"></span>
        <br />
        <label class="fontcolor End">
            End:
        </label>
        <span id="endTime"></span>
        <br />
        <br />
        <label class="fontcolor Message">
            Message:
        </label>
        <span id="message"></span>
        <br />
        <br />
        <br />
        <input type="button" value="Close" class="btn btn-primary yellow" onclick="$('#eventContent').hide();"
            style="float: right" />
        <p id="eventInfo">
        </p>
        <p>
            <strong></strong>
        </p>
    </div>
      <asp:HiddenField ID="lblAllManager1" runat="server" Value="all manager" meta:resourcekey="AllManagerResource1" />
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" meta:resourcekey="lblMsgResource1"></asp:Label>
    <div class="col-md-6">
        <div class="heading-sec">
            <h1>
                <%--  <%= CommonMessages.Dashboard%>--%>
                <i><span>
                    <asp:Literal ID="Literal5" runat="server" meta:resourcekey="Welcome" EnableViewState="false" /></span><span
                        runat="server" id="Dashboard"></span></i>
            </h1>
        </div>
    </div>
    <div class="col-md-6" onclick="OnLoadCall()">

        <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc; display: none">
            <i class="fa fa-calendar icon-calendar icon-large"></i><span>August 5, 2014 - September
                3, 2014</span> <b class="caret"></b>
        </div>
    </div>
    <!--<li class="range">
<a href="#">
<i class="icon-calendar"></i>
<span>August 5, 2014 - September 3, 2014</span>
<i class="icon-angle-down"></i>
</a>
</li>-->
    <div class="col-md-12" style="margin-bottom: 20px;">
        <div id="graph-wrapper">


            <h3 class="custom-heading darkblue" style="height: 45px;">
                <%-- <%= CommonMessages.EmployeeSkills%> --%>

                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="EmployeeSkills" EnableViewState="false" />
                <asp:DropDownList ID="ddldropdownDivisionGraph" runat="server" Style="width: 25%; height: 32px; float: right; display: inline; margin-bottom: 20px;"
                    CssClass="form-control" AutoPostBack="False">
                </asp:DropDownList>

                <asp:DropDownList ID="ddlJobType" runat="server"
                    Style="width: 25%; height: 32px; float: right; display: inline; margin-bottom: 20px; margin-right: 15px;"
                    CssClass="form-control" AutoPostBack="False">
                </asp:DropDownList>
                <input type="hidden" id="hdnChartType" name="hdnChartType" value="bars" />
                <%--  <a href="#" id="bars" onclick="return SetChartType('bars');"><span><i class="fa fa-bar-chart-o"></i></span></a>
                    <a href="#" id="lines" class="active" onclick="return SetChartType('lines');"><span><i class="fa fa-bullseye" style="font-size:15px;"></i></span></a>
                    <a href="#" id="toggle" onclick="return SetChartType('toggle');"><span><i class="fa fa-bar-chart-o"></i></span></a>--%>
                <div style="float: right; margin-right: 10px">

                    <a href="#" id="bars" onclick="return SetChartType('bars');"><span>

                        <img src="img/vertical-bar-chart-loss-icon.jpg" style="height: 25px; width: 25px; margin-left: 3px; margin-top: 2px;" />
                    </span></a>
                    <a href="#" id="lines" class="active" onclick="return SetChartType('lines');">
                        <i class="fa fa-bullseye" style="font-size: 15px; margin-left: 10px; margin-top: 8px; color: black"></i></a>
                    <a href="#" id="toggle" onclick="return SetChartType('bars');"><span><i class="fa fa-bar-chart-o"></i>
                    </span></a>
                </div>

            </h3>
            <div class="home_grap">
                <div>
                    <input type="hidden" value="0" id="ddlCompetence" name="ddlCompetence" />
                    <asp:DropDownList ID="managercat" runat="server" multiple="multiple">
                    </asp:DropDownList>
                </div>
                <span id="lblUserName" style="margin-left: 50%; display: none;">
                    <asp:Label runat="server" meta:resourcekey="AllManagerResource" ID="lblAllManager" /></span>

                <%--  <div class="graph-container" style="margin-top:50px">--%>


                <div id="container" style="width: 100%; height: 400px;">
                </div>

                <div id="graph-barsEmployee" style="width: 100%; height: 400px; margin-top: 10px;">
                </div>

                <div id="graph-barEmp" style="width: 100%; height: 400px;">
                </div>

                <select id="chartType" name="Chart Type" style="float: right; margin-top: -430px; margin-right: 24%; position: relative;">
                    <option value="0">
                        <asp:Label ID="Label8" runat="server" meta:resourcekey="sortbyres"></asp:Label></option>
                    <option value="1">
                        <asp:Label ID="Label7" runat="server" meta:resourcekey="hightolowres"></asp:Label></option>
                    <option value="2">
                        <asp:Label ID="Label6" runat="server" meta:resourcekey="lowtohighres"></asp:Label></option>
                </select>

                <select id="ddlUserTarget" name="Chart Type" style="float: right; margin-top: -430px; margin-right: 35px; position: relative;">
                    <option value="1" selected="selected">
                        <asp:Literal ID="Literal19" runat="server" meta:resourcekey="shorttermRes" Text="Show short term target" EnableViewState="false" /></option>
                    <option value="2">
                        <asp:Literal ID="Literal20" runat="server" meta:resourcekey="longtermRes" Text="Show long term target" EnableViewState="false" /></option>
                    <option value="3">
                        <asp:Literal ID="Literal21" runat="server" meta:resourcekey="targetJobType" Text="Show company target for JobType" EnableViewState="false" /></option>
                     <option value="4">
                        <asp:Literal ID="Literal22" runat="server" meta:resourcekey="targetDivision" Text="Show company target for Division" EnableViewState="false" /></option>
                </select>

                <%--    </div>--%>
            </div>
            <div class="col-md-12" style="background: #f4f4f4; padding-left: 0px !important;">
                <div class="col-md-1 profile-details" style="padding-left: 0px !important;">
                    <a class="black" style="float: left" onclick="GetGraphByManagerId('','')">
                        <i class="fa fa-users"></i>
                        <asp:Literal ID="Literal6" runat="server" meta:resourcekey="Manager" EnableViewState="false" /><%-- <%= CommonMessages.Manager%>--%></a>
                </div>
                <div class="col-md-9">
                    <div class="total-members">
                        <%--UserList-------------------------------------%>
                        <ul id="userList">
                            <%--<li><a href="#" title="">
                                <img src="images/colleagus/2.jpg" alt="" /></a>
                                <div class="member-hover">
                                    <a href="#" title="">
                                        <img src="images/colleagus/2.jpg" alt="" /></a> <span>JOHN DOE </span>
                                </div>
                            </li>--%>
                        </ul>
                        <%--UserList-------------------------------------%>
                        <%--    <li><a href="#" title="">
                                <img src="images/colleagus/3.jpg" alt="" /></a>
                                <div class="member-hover">
                                    <a href="#" title="">
                                        <img src="images/colleagus/3.jpg" alt="" /></a> <span>LARA SMITH </span>
                                </div>
                            </li>
                            <li><a href="#" title="">
                                <img src="images/colleagus/2.jpg" alt="" /></a>
                                <div class="member-hover">
                                    <a href="#" title="">
                                        <img src="images/colleagus/2.jpg" alt="" /></a> <span>YAMAZ DAN </span>
                                </div>
                            </li>
                            <li><a href="#" title="">
                                <img src="images/colleagus/9.jpg" alt="" /></a>
                                <div class="member-hover">
                                    <a href="#" title="">
                                        <img src="images/colleagus/9.jpg" alt="" /></a> <span>DAREN JANCE </span>
                                </div>
                            </li>
                            <li><a href="#" title="">
                                <img src="images/colleagus/10.jpg" alt="" /></a>
                                <div class="member-hover">
                                    <a href="#" title="">
                                        <img src="images/colleagus/10.jpg" alt="" /></a> <span>HELOA HAZ </span>
                                </div>
                            </li>
                            <li><a href="#" title="">
                                <img src="images/colleagus/2.jpg" alt="" /></a>
                                <div class="member-hover">
                                    <a href="#" title="">
                                        <img src="images/colleagus/2.jpg" alt="" /></a> <span>JOHN DOE </span>
                                </div>
                            </li>
                            <li><a href="#" title="">
                                <img src="images/colleagus/3.jpg" alt="" /></a>
                                <div class="member-hover">
                                    <a href="#" title="">
                                        <img src="images/colleagus/3.jpg" alt="" /></a> <span>LARA SMITH </span>
                                </div>
                            </li>
                            <li><a href="#" title="">
                                <img src="images/colleagus/2.jpg" alt="" /></a>
                                <div class="member-hover">
                                    <a href="#" title="">
                                        <img src="images/colleagus/2.jpg" alt="" /></a> <span>YAMAZ DAN </span>
                                </div>
                            </li>
                            <li><a href="#" title="">
                                <img src="images/colleagus/9.jpg" alt="" /></a>
                                <div class="member-hover">
                                    <a href="#" title="">
                                        <img src="images/colleagus/9.jpg" alt="" /></a> <span>DAREN JANCE </span>
                                </div>
                            </li>
                            <li><a href="#" title="">
                                <img src="images/colleagus/10.jpg" alt="" /></a>
                                <div class="member-hover">
                                    <a href="#" title="">
                                        <img src="images/colleagus/10.jpg" alt="" /></a> <span>HELOA HAZ </span>
                                </div>
                            </li>
                        </ul>--%>

                        <input type="hidden" id="txtvalue" value="<%=Session["Culture"] %>" />
                        <div style="display: none">
                            <%-- <asp:Repeater ID="Repeaterman" runat="server" >
                            <HeaderTemplate>
                                <ul>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <li><a href="#" title="">
                                    <img src='../Log/upload/Userimage/<%# Eval("userImage") %>' alt="" /></a>
                                    <div class="member-hover" style="width: 130px;">
                                        <a href="#" title="">
                                            <img src='../Log/upload/Userimage/<%# Eval("userImage") %>' alt="" /></a> <span>
                                                <%# Eval("name")%></span>
                                    </div>
                                </li>
                            </ItemTemplate>
                            <FooterTemplate>
                                </ul>
                            </FooterTemplate>
                        </asp:Repeater>--%>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="total-memeber-head">
                        <div class="add-btn" style="float: right">
                            <a href="#add-post-title" data-toggle="modal" title="" onclick="javascript:showpopup1();">
                                <i class="fa fa-plus pink"></i>
                                <%--<%= CommonMessages.AddNew%>  <%= CommonMessages.Manager%>--%>
                                <asp:Literal ID="Literal2" runat="server" meta:resourcekey="AddNewManager" EnableViewState="false" />
                            </a>
                        </div>
                    </div>
                    <div aria-hidden="true" role="dialog" tabindex="-1" class="modal fade" id="add-post-title"
                        style="display: none;">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header blue">
                                    <button aria-hidden="true" data-dismiss="modal" class="close" type="button" style="display: none;">
                                        ×
                                    </button>
                                    <h4 class="modal-title">
                                        <asp:Literal ID="Literal3" runat="server" meta:resourcekey="AddNewManager" EnableViewState="false" /></h4>
                                </div>
                                <div class="modal-body ">
                                    <div class="otherDiv">
                                        <asp:TextBox runat="server" ID="txtName" MaxLength="50" meta:resourcekey="txtNameResource1" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName"
                                            ErrorMessage="Please enter name." CssClass="commonerrormsg" Display="Dynamic"
                                            ValidationGroup="chkNewEmp" meta:resourcekey="RequiredFieldValidator5Resource1"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="otherDiv">
                                        <asp:TextBox runat="server" ID="txtLastname" MaxLength="50" meta:resourcekey="txtNameResource2" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtName"
                                            ErrorMessage="Please enter name." CssClass="commonerrormsg" Display="Dynamic"
                                            ValidationGroup="chkNewEmp" meta:resourcekey="RequiredFieldValidator5Resource1"></asp:RequiredFieldValidator>

                                    </div>
                                    <div class="otherDiv">
                                        <asp:TextBox runat="server" ID="txtaddress" Rows="3" TextMode="MultiLine"
                                            meta:resourcekey="txtaddressResource1" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtaddress"
                                            ErrorMessage="Please enter address." CssClass="commonerrormsg" Display="Dynamic"
                                            ValidationGroup="chkNewEmp" meta:resourcekey="RequiredFieldValidator2Resource1"></asp:RequiredFieldValidator>
                                    </div>
                                    <div class="otherDiv">
                                        <%--  <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                            <ContentTemplate>--%>
                                        <asp:TextBox runat="server" ID="txtEmail" MaxLength="50" onchange="checkDuplicateEmail();"
                                            meta:resourcekey="txtEmailResource1" ValidationGroup="chkNewEmp" />
                                        <asp:Label runat="server" ID="lblEmail" CssClass="commonerrormsg" meta:resourcekey="lblEmailResource1"></asp:Label>
                                        <%--   </ContentTemplate>
                                        </asp:UpdatePanel>--%>

                                        <asp:RequiredFieldValidator ID="rfvEmail" runat="server" CssClass="commonerrormsg"
                                            ControlToValidate="txtEmail" Display="Dynamic" ErrorMessage="Please enter email."
                                            meta:resourcekey="rfvEmailResource1" ValidationGroup="chkNewEmp"></asp:RequiredFieldValidator>
                                        <asp:RegularExpressionValidator ID="revEmail" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                            ControlToValidate="txtEmail" CssClass="commonerrormsg" Display="Dynamic" ValidationGroup="chkNewEmp"
                                            ErrorMessage="Please enter valid email." meta:resourcekey="revEmailResource1"></asp:RegularExpressionValidator>
                                    </div>
                                    <div class="otherDiv">
                                        <asp:TextBox runat="server" ID="txtMobile" MaxLength="50"
                                            meta:resourcekey="txtMobileResource1" />
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtMobile"
                                            ErrorMessage="Minimum Phone No length is 10." Display="Dynamic" CssClass="commonerrormsg"
                                            ValidationGroup="chkNewEmp" ValidationExpression=".{10}.*" meta:resourcekey="RegularExpressionValidator6Resource1" />
                                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtcphone"
                                                    ErrorMessage="Please enter mobile." CssClass="commonerrormsg" Display="Dynamic"
                                                    ValidationGroup="chk"></asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Numbers,custom"
                                                    ValidChars=" ,+,(,),-" TargetControlID="txtcphone" Enabled="True">
                                                </cc1:FilteredTextBoxExtender>--%>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtMobile"
                                            ErrorMessage="Please enter phone no." CssClass="commonerrormsg" Display="Dynamic"
                                            ValidationGroup="chkNewEmp" meta:resourcekey="RequiredFieldValidator6Resource1"></asp:RequiredFieldValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Custom, Numbers"
                                            ValidChars=" ,+,(,),-" TargetControlID="txtMobile" Enabled="True">
                                        </cc1:FilteredTextBoxExtender>
                                        <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtMobile"
                                            ErrorMessage="Minimum mobile length is 9." Display="Dynamic" CssClass="commonerrormsg"
                                            ValidationGroup="chk" ValidationExpression=".{9}.*" />
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtMobile"
                                            ErrorMessage="Please enter mobile." CssClass="commonerrormsg" Display="Dynamic"
                                            ValidationGroup="chk"></asp:RequiredFieldValidator>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers"
                                            TargetControlID="txtMobile" Enabled="True">
                                        </cc1:FilteredTextBoxExtender>--%>
                                    </div>
                                    <div class="otherDiv">
                                        <asp:DropDownList ID="ddlgender" runat="server" CssClass="form-control" Style="width: 100%; height: 32px; display: inline; margin-bottom: 20px;"
                                            meta:resourcekey="ddlgenderResource1">
                                            <asp:ListItem Value="Male" Text="Male" meta:resourcekey="ListItemResource1"></asp:ListItem>
                                            <asp:ListItem Value="Female" Text="Female" meta:resourcekey="ListItemResource2"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorddlgender" runat="server" ControlToValidate="ddlgender"
                                            ErrorMessage="Please select gender." InitialValue="0" CssClass="commonerrormsg"
                                            Display="Dynamic" ValidationGroup="chk" meta:resourcekey="selectgenderres"></asp:RequiredFieldValidator>

                                    </div>
                                    <div class="otherDiv">
                                        <asp:DropDownList ID="ddlLanguage" runat="server" Style="width: 100%; height: 32px; display: inline; margin-bottom: 20px;"
                                            CssClass="form-control">
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidatorddlLanguage" runat="server" ControlToValidate="ddlLanguage"
                                            ErrorMessage="Please select Language." InitialValue="0" CssClass="commonerrormsg"
                                            Display="Dynamic" ValidationGroup="chkNewEmp" meta:resourcekey="RequiredFieldValidatorddlLanguageResource1">
                                        </asp:RequiredFieldValidator>
                                    </div>
                                    <asp:UpdatePanel runat="server" ID="UpdatePanedrop" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <div class="otherDiv">
                                                <asp:DropDownList ID="ddlDepartment" runat="server" Style="width: 100%; height: 32px; display: inline; margin-bottom: 20px;"
                                                    CssClass="form-control" OnSelectedIndexChanged="ddlcompetence_SelectedIndexChanged"
                                                    AutoPostBack="True">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorddlDepartment" runat="server" ControlToValidate="ddlDepartment"
                                                    ErrorMessage="Please select Division." InitialValue="0" CssClass="commonerrormsg"
                                                    Display="Dynamic" ValidationGroup="chkNewEmp" meta:resourcekey="selectdivisionres"></asp:RequiredFieldValidator>
                                            </div>
                                            <div class="otherDiv">
                                                <asp:DropDownList ID="ddljobtype1" runat="server" Style="width: 100%; height: 32px; margin-bottom: 20px;"
                                                    CssClass="form-control" OnSelectedIndexChanged="ddljobtype_SelectedIndexChanged"
                                                    AutoPostBack="True">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorddljobtype1" runat="server" ControlToValidate="ddljobtype1"
                                                    ErrorMessage="Please select job type." InitialValue="0" CssClass="commonerrormsg"
                                                    Display="Dynamic" ValidationGroup="chkNewEmp" meta:resourcekey="selectjobtyperes"></asp:RequiredFieldValidator>
                                            </div>

                                            <div class="otherDiv">
                                                <asp:DropDownList ID="ddlTeam" runat="server" Style="width: 100%; height: 32px; margin-bottom: 20px;"
                                                    CssClass="form-control" meta:resourcekey="ddlTeamResource1"
                                                    AutoPostBack="false">
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidatorddlTeam" runat="server" ControlToValidate="ddlTeam"
                                                    ErrorMessage="Please select Team." InitialValue="0" CssClass="commonerrormsg"
                                                    Display="Dynamic" ValidationGroup="chkNewEmp" meta:resourcekey="selectteamres"></asp:RequiredFieldValidator>
                                            </div>

                                            <div class="col-md-6 scroll_checkboxes" style="display: none; height: 400px; overflow: auto;"
                                                id="CompetenceCheckboxes">
                                                <div id="Div1" runat="server">
                                                    <asp:HyperLink runat="server" onclick="fnSelectAll(this);" meta:resourcekey="lnkSelectAll" />
                                                    <%-- <a href="#" runat="server" meta:resourcekey="SelectAll" onclick="fnSelectAll();" title="Select All">Select All</a>--%>
                                                </div>
                                                <label class="c-label" style="width: 100%;">
                                                    <asp:Literal ID="Literal8" runat="server" meta:resourcekey="Category" EnableViewState="false" />
                                                    <%--  <%= CommonMessages.CATEGORY%>--%></label><br />

                                                <div id="accordion-resizer" class="ui-widget-content" style="border: none;">
                                                    <div id="accordion">

                                                        <div id="NoRecords" runat="server" visible="false">
                                                            <asp:Literal ID="Literal18" runat="server" meta:resourcekey="None" EnableViewState="false" />
                                                        </div>
                                                        <asp:Repeater ID="repCompCat" runat="server" OnItemDataBound="Repeater1_ItemDataBound">
                                                            <HeaderTemplate>
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <div style="margin-bottom: 2%;">
                                                                    <asp:Label runat="server" ID="lblDivIdName" Visible="false"><%# DataBinder.Eval(Container.DataItem, "catName")%> </asp:Label>
                                                                    <asp:Label runat="server" ID="lblDivIdName1" Visible="false"><%# DataBinder.Eval(Container.DataItem, "catNameDN")%> </asp:Label>
                                                                    <asp:Label runat="server" ID="lblCompCategoryName"> </asp:Label>
                                                                </div>
                                                                <div>
                                                                    <asp:Repeater ID="repCompAdd" runat="server" OnItemDataBound="Repeater2_ItemDataBound">
                                                                        <HeaderTemplate>
                                                                            <table style="margin-top: -2%;">
                                                                                <tbody>
                                                                        </HeaderTemplate>
                                                                        <ItemTemplate>
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:CheckBoxList Width="250px" ID="chkList" runat="server" CssClass="chkliststyle chkcomname"
                                                                                        RepeatColumns="1" BorderWidth="0px" Datafield="description" DataValueField="value"
                                                                                        Style="width: 100%; overflow: auto;">
                                                                                    </asp:CheckBoxList>
                                                                                    <asp:Label runat="server" ID="hdnChkboxValue" Text='<%# DataBinder.Eval(Container.DataItem, "comId") %>'
                                                                                        Style="display: none"></asp:Label>
                                                                                </td>
                                                                            </tr>
                                                                        </ItemTemplate>
                                                                        <FooterTemplate>
                                                                            </tbody> </table>
                                                                        </FooterTemplate>
                                                                    </asp:Repeater>
                                                                </div>
                                                            </ItemTemplate>
                                                            <FooterTemplate>
                                                            </FooterTemplate>
                                                        </asp:Repeater>
                                                    </div>
                                                </div>
                                            </div>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </div>
                                <%--NEXT Button--%>
                                <div class="modal-footer" id="nextButtons">
                                    <%-- <asp:Button runat="server" ID="btnNext" Text="Next" CssClass="btn btn-primary yellow"
                        ValidationGroup="chk" OnClientClick="ShowCompotenceCategory(true);" Style="border-radius: 5px;" />--%>
                                    <%-- <input id="" type="button" onclick="GoNext()" value="Next" class="btn btn-primary yellow"
                                           style="border-radius: 5px;" validationgroup="chkNewEmp" meta:resourcekey="Close"/> --%>
                                    <%--   <asp:Literal ID="Literal13" runat="server" meta:resourcekey="Close" EnableViewState="false" />
                                    </input>--%>

                                    <button class="btn btn-primary yellow" type="button" validationgroup="chkNewEmp" onclick="GoNext()">
                                        <%-- <%= CommonMessages.Close%>--%>
                                        <asp:Literal ID="Literal13" runat="server" meta:resourcekey="Next" EnableViewState="false" />
                                    </button>

                                    <button data-dismiss="modal" class="btn btn-primary black" type="button" onclick="ShowCompotenceCategory(false)">
                                        <asp:Literal ID="Literal9" runat="server" meta:resourcekey="Close" EnableViewState="false" />

                                    </button>
                                </div>
                                <%--<div class="modal-footer">
                                    <a href="#" id="btnsubmit" title="Submit" class="btn btn-primary yellow"
                                       onclick="AddNewUser();" style="border-radius: 5px;"
                                        meta:resourcekey="btnsubmitResource1"  >Submit</a>
                                    <button data-dismiss="modal" class="btn btn-default black" type="button">
                                        $1$ <%= CommonMessages.Close%> #1#
                                        <asp:Literal ID="Literal4" runat="server" meta:resourcekey="Close" EnableViewState="false" />
                                    </button>
                                </div>--%>
                                <%--Save Button--%>
                                <div class="modal-footer" id="submitButtons" style="display: none">
                                    <%-- <input id="Button2" type="button" onclick="ShowCompotenceCategory(false);" value="Back"
                                        class="btn btn-primary yellow" style="border-radius: 5px;" validationgroup="chkNewEmp" />--%>

                                    <%-- <button class="btn btn-primary yellow" type="button" onclick="$('.otherDiv').show();" validationgroup="chkNewEmp">
                                        <asp:Literal ID="Literal14" runat="server" meta:resourcekey="Back" EnableViewState="false" />
                                    </button>--%>
                                    <button class="btn btn-primary yellow" type="button" onclick="$('.otherDiv').show();" validationgroup="chkNewEmp" id="btnbak">
                                        <asp:Literal ID="Literal14" runat="server" meta:resourcekey="Back" EnableViewState="false" />
                                    </button>
                                    <%--  <asp:Button runat="server" ID="Button1" Text="Submit" CssClass="btn btn-primary yellow"
                                                ValidationGroup="chkNewEmp" OnClick="btnsubmit_click" Style="border-radius: 5px;"
                                                meta:resourcekey="Submit" />--%>
                                    <asp:Button runat="server" ID="Button1" meta:resourcekey="Submit" CssClass="btn btn-primary yellow"
                                        ValidationGroup="chkNewEmp" OnClientClick="AddNewUser();return false;" Style="border-radius: 5px;" />
                                    <%--<asp:Button runat="server" ID="btnclose1" Text="Close" CssClass="btn btn-default black"
                         OnClick="btnclose_click1" class="btn btn-default black"/>--%>
                                    <button data-dismiss="modal" class="btn btn-primary black" type="button" onclick="ShowCompotenceCategory(false)">
                                        <%-- <%= CommonMessages.Close%>--%>
                                        <asp:Literal ID="Literal10" runat="server" meta:resourcekey="Close" EnableViewState="false" />
                                    </button>
                                </div>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Widget Visitor -->
    <!-- ORDR REVIEWS -->
    <div class="col-md-6">
        <div class="contact-list widget-body" id="div_contactlist">
            <div class="timeline-head yellow">

                <asp:Label ID="lblcontactlist" CssClass="aaa" runat="server" meta:resourcekey="MyCalender" EnableViewState="false"></asp:Label>
                <%--<span class="aaa">My Calendar </span>--%>
            </div>
            <%--Add Appointment--%>
            <div class="col-md-8">
                <div class="invite_btn">
                    <input id="Text1" class="datepicker btn btn-primary yellow" style="border-radius: 5px;" type="text" runat="server" value="Go To Date" meta:resourcekey="GoToDate" />
                    <a href="#AddApointment" data-toggle="modal" title="">
                        <button style="border: 0px;" class="btn btn-primary yellow" type="button">
                            <asp:Literal ID="Literal4" runat="server" meta:resourcekey="AddAppointment1" EnableViewState="false" /></button></a>
                </div>
                <div aria-hidden="true" role="dialog" tabindex="-1" class="modal fade" id="AddApointment"
                    style="display: none;">
                    <div class="modal-dialog" style="margin-top: 60px;">
                        <div class="modal-content">
                            <div class="modal-header blue">
                                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                                    ×
                                </button>
                                <h4 class="modal-title">
                                    <asp:Literal ID="Literal7" runat="server" meta:resourcekey="SendAppointment" EnableViewState="false" /></h4>
                            </div>
                            <div class="modal-body">
                                <%-- <input type="text" value="" placeholder="new note" id="Text1" runat="server" />--%>
                                <asp:UpdatePanel runat="server" ID="UpdatePanel12" UpdateMode="Conditional">
                                    <ContentTemplate>
                                        <asp:TextBox runat="server" ID="txtto" AccessKey="u" onkeyup="GetCourt(this.id);"
                                            AutoPostBack="True"
                                            placeholder="To..." meta:resourcekey="txttoResource1"></asp:TextBox>

                                    </ContentTemplate>
                                </asp:UpdatePanel>
                                <%-- <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>--%>
                                <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.1/jquery-ui.min.js"></script>
                                <script type="text/javascript">
                                    function GetCourt(id) {
                                        //var $accui = jQuery.noConflict(true);

                                        $("#" + id).autocomplete({
                                            source: function (request, response) {
                                                $.ajax({
                                                    type: "POST",
                                                    contentType: "application/json; charset=utf-8",
                                                    url: "Calendar.aspx/GetAutoCompleteCourt",
                                                    data: "{'court':'" + document.getElementById('<%=txtto.ClientID %>').value + "'}",
                                                    dataType: "json",
                                                    success: function (data) {
                                                        response(data.d);
                                                    },
                                                    error: function (result) {
                                                        alert("Error");
                                                    }
                                                });
                                            }
                                        });
                                    }
                                </script>
                                <input type="text" placeholder="Subject" id="txtsubject" runat="server" />
                                <div style="float: left;">
                                    <div id="Div2" class="pull-right" style="padding: 0px; background: none !important; color: #000;">
                                        <%--  <i class="fa fa-calendar icon-calendar icon-large" style="color: #999999">Date </i>--%>
                                        <input id="Label1" class="drpAppointment  btn btn-default" type="text" value="Select Date Range" />
                                        <%-- <asp:Label ID="Label1"  runat="server" Text="Label"
                                            ></asp:Label>--%>
                                        <%-- <span>August 5, 2014 - September 3, 2014</span>--%> <%--<b class="caret"></b>--%>
                                    </div>
                                </div>

                                <div style="float: left;">
                                    <input type="text" class="timepicker1  btn btn-default" style="margin-left: 10px; width: 100px;" placeholder="Start Time" id="Text2" />
                                    <input type="text" class="timepicker2  btn btn-default" style="margin-left: 10px; width: 100px;" placeholder="End Time" id="Text3" />
                                </div>
                                <textarea placeholder="Message" rows="3" id="txtmessge" runat="server"></textarea>
                            </div>
                            <div class="modal-footer">
                                <%-- <asp:Button ID="btnSend" runat="server" Text="Send" class="btn btn-primary yellow" OnClick="btnSend_click" OnClientClick="myFunction()"/>--%>
                                <input type="button" onclick="myFunction()" id="btnSend" value="Save" class="btn btn-primary yellow">
                                <%--<button class="btn btn-primary yellow" type="button" OnClick="btnSend_click">
                                        Send</button>--%>
                                <%--  <asp:LinkButton ID="LinkButton1" runat="server" CssClass="addNote animate" OnClick="btnnote_click">+
                                                </asp:LinkButton>--%>
                                <button data-dismiss="modal" class="btn btn-default black" type="button" id="btnclose">
                                    <asp:Literal ID="Literal11" runat="server" meta:resourcekey="Close" EnableViewState="false" />
                                </button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                </div>

            </div>

            <div id='calendar1' style="padding: 10px; margin-top: 0px; background-color: #f4f4f4; border-radius: 5px;">
            </div>
        </div>
    </div>
    <!-- TIME LINE -->
    <%--   <div class="col-md-6">
      <asp:UpdatePanel runat="server" ID="UpdatePanellist" UpdateMode="Conditional">
        <ContentTemplate>
        <div class="chat-widget widget-body">
            <div class="chat-widget-head yellow">
                <h4 id="chatterName">
                    Chat</h4>
                <div class="add-btn">
                  <a href="#contactList" title="" onmouseover="ShowHideContactListForChat();" style="float: right"><i class="fa fa-plus pink"></i>Show/Hide Contact List </a>
                </div>
            </div>
            
            
              <ul id="scrollbox6" style="bottom:0">
                  <asp:UpdatePanel runat="server" ID="timerUpdate"><ContentTemplate>
                                                                       <asp:Label id="lblDefaultText" runat="server">Please select user from contact list.</asp:Label>
                  <asp:Timer runat="server" Enabled="True" OnTick="OnTick_RefreshChatList" ID="chatrefreshTick" Interval="5000"></asp:Timer>
                                <asp:Repeater ID="rpt_chat" runat="server" OnItemDataBound="rpt_chat_ItemDataBound">
                                    <ItemTemplate>
                                        <li runat="server" id="listItem" class="reply">
                                            <div class="chat-thumb">
                                                <img src='../Log/upload/Userimage/<%# Eval("img1") %>'>
                                            </div>
                                            <div class="chat-desc">
                                                <p>
                                                    <%#Eval("messubject")%>
                                                    <asp:HiddenField ID="mesfromUserId" runat="server" Value="<%# bind('mesfromUserId') %>" />
                                                </p>
                                                <i class="chat-time"> </i>
                                            </div>
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                      </ContentTemplate>

                  </asp:UpdatePanel>
                            </ul>
             
            <div class="chat-widget widget-body" style="height: 436px;margin:9.5% 60.5% 0;position: absolute;width:35%;display: none" id="contactList">
                            <div class="chat-widget-head yellow yellow-radius">
                                <h4 style="margin: -5px 0px 0px;">
                                    Contacts</h4>
                                <div class="add-btn">
                                </div>
                            </div>
                            <ul id="Ul1" style="overflow: auto;height: 384px">
                                <asp:Repeater ID="lst_contacts" runat="server" OnItemCommand="lst_contacts_OnItemCommand">
                                    <ItemTemplate>
                                        <li style="margin-bottom: 0px;"  id="li" runat="server">
                                            <label style="width: 100%;">
                                                <asp:LinkButton ID="lb_contact" runat="server" Text='<%# Eval("name") %>' Style="color: Black;
                                        font-weight: normal" CommandName="Message" CommandArgument='<%# Eval("userId") %>' 
                                                OnClientClick="HighlighThis();UpdateChatName(this);" meta:resourcekey="lb_contactResource1"></asp:LinkButton>
                                            </label>
                                        </li>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </ul>
                        </div>
           
            <div class="reply-sec" style="margin-left: 20px; " runat="server" id="replyChat">
                             
                                <asp:TextBox ID="txtmessage" runat="server" placeholder="TYPE YOUR MESSAGE HERE"
                                    
                                    Style="border: 1px solid #c6c6c6; border-radius: 2px; float: left; font-family: open sans;
                                     height: 37px; letter-spacing: 0.3px; margin: 10px 0; padding: 0 10px; width: 85%;" 
                                    meta:resourcekey="txtmessageResource1"></asp:TextBox>           
                               
                 <asp:LinkButton ID="lnkCatName" runat="server" CssClass="fa fa-comments-o black"
                                    Style="font-size: 22px; height: 37px; line-height: 33px; margin: 10px 0 10px 10px;
                                            text-align: center; width: 52px; -webkit-border-radius: 2px; -moz-border-radius: 2px;
-ms-border-radius: 2px; -o-border-radius: 2px; border-radius: 2px;" OnClick="Button1_Click"
                                     OnClientClick="highlightOnSend();ShowHideReplyDiv(true);" 
                                    ></asp:LinkButton>
                <br />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtmessage"
                                    ErrorMessage="Please Enter Messge." CssClass="commonerrormsg" Display="Dynamic"
                                    ValidationGroup="chk" Style="float: left;" 
                                    meta:resourcekey="RequiredFieldValidator5Resource1"></asp:RequiredFieldValidator>
                            </div>
            </div>
        </div>
            
            </ContentTemplate>
        
    </asp:UpdatePanel>
    </div>--%>
    <div class="col-md-6">
        <div class="contact-list widget-body">
            <div class="timeline-head yellow">
                <asp:Label ID="Label2" CssClass="aa" runat="server" meta:resourcekey="MyContactlist" EnableViewState="false"></asp:Label>
            </div>
            <div class="">
                <div class="my-photos">
                    <ul id="name_scroll1">
                        <li>
                            <asp:Label ID="Label333" CssClass="aa" runat="server" meta:resourcekey="NoRecordFound" EnableViewState="false"></asp:Label>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="">
                <div class="chat-widget widget-body">
                    <ul id="scrollbox6" style="height: 396px;">
                        <li>
                            <asp:Label ID="Label4" CssClass="aa" runat="server" meta:resourcekey="NoRecordFound" EnableViewState="false"></asp:Label>
                        </li>

                    </ul>
                    <div class="reply-sec">
                        <div class="reply-sec-MESSAGE" style="margin-left: 20px;">
                            <input type="text" id="message1" style="padding: 10px;" runat="server" meta:resourcekey="messagetext" />
                            <a href="#" title="" class="black" onclick="insertmessage(); return false;"><i class="fa fa-comments-o"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Chat Widget -->
    <!-- Recent Post -->
    <%--Model dilouge |Saurin | 2015-02-23|-------------%>
    <div class="col-md-3">
    </div>
    <div id="dialog" title="Graph Data" style="display: none">
        <p>
            There is no data available to draw graph.
        </p>
    </div>
    <!---------------------------------------JoyRide >> Tip Content--------------------------------------------------------- -->
    <ol id="joyRideTipContent">
        <%--<li data-class="menu-profile" data-text="Next" class="custom" data-button="End Tour" data-options="tipLocation:right-middle">
        <h5>Welcome! Lets have a tour on Starteku.</h5>
        <p>You can control all the details for you tour stop.</p>
      </li>--%>
        <%-- <li data-class="heading-sec" data-button="stop" data-button="Already visited!" data-options="tipLocation:top;tipAnimation:fade">
        <h2>Stop #2</h2>
        <p>Get the details right by styling Joyride with a custom stylesheet!</p>
      </li>
      <li data-id="Head1" data-button="Next" data-text="Already visited!" data-options="tipLocation:right">
        <h2>Stop #3</h2>
        <p>It works right aligned.</p>
      </li>--%>
        <li data-button="Close" data-button="previous">
            <div runat="server" id="welcomeMsg"></div>
            <div class="tourTickbtn">
                <input id="chkTurnOffTour" onclick="joyrideEnd(); OnLoadCall();" type="checkbox" title="" />
                <p style="margin-left: 15px; margin-top: -16px">
                    <asp:Literal ID="Literal12" runat="server" meta:resourcekey="TurnOffJoyRide" />
                </p>
            </div>
            <br />
            <br />
        </li>
        <%--<li data-button="Close">
            <h4>
                Congratulations <span class="WelComeUsername"></span>!
            </h4>
            <br />
            <p>
                You have now received :
            </p>
            <p>
                PLP points : <span class="plpSpan">0 </span>
            </p>
            <p>
                ALP points : <span class="alpSpan">0 </span>
            </p>
            <p>
                PDP points : <span class="pdpSpan">0 </span>
            </p>
            <p>
                Since your <span class="lastLogin">00:00 </span>!</p>
        </li>--%>
    </ol>
    <%--point welcom MESSAGE--%>
    <%--<ol id="welcomeWithPoint" style="display: none">
        <li data-button="Close" data-button="previous">
            <h4>
                Congratulations <span class="WelComeUsername"></span>!
            </h4>
            <br />
            <p>
                You have now received :
            </p>
            <p>
                PLP points : <span class="plpSpan">0 </span>
            </p>
            <p>
                ALP points : <span class="alpSpan">0 </span>
            </p>
            <p>
                PDP points : <span class="pdpSpan">0 </span>
            </p>
            <p>
                Since your <span class="lastLogin">00:00 </span>!</p>
            <div class="tourTickbtn">
            </div>
            <br />
            <br />
        </li>
       
    </ol>--%>
    <%--<script src="js/fullcalendar.js" type="text/javascript"></script>--%>
    <%--<script src="../Scripts/chartCat.js" type="text/javascript"></script>--%>

    <link href="../assets/css/multiselect.css" rel="stylesheet" />
    <script src="../assets/js/multiselect.js" type="text/javascript"></script>

    <script src="../Scripts/flotJs.js" type="text/javascript"></script>
    <script src="../Scripts/chartCat.js" type="text/javascript"></script>
    <script src="js/chart-line-and-graph.js?v=2.7" type="text/javascript"></script>
    <%--  <script src="//code.jquery.com/ui/1.11.3/jquery-ui.js" type="text/javascript"></script>
    <script src="http://code.jquery.com/jquery-migrate-1.0.0.js" type="text/javascript"></script>--%>
    <script src="../assets/js/jquery.noty.packaged.js" type="text/javascript"></script>

    <script type="text/javascript">
        function fnSelectAll(a) {
            if ($("#accordion-resizer :input[type='checkbox']").prop('checked') == true) {
                $(a).text($('#ContentPlaceHolder1_hdnSelectAll').val());
                $("#accordion-resizer :input[type='checkbox']").prop('checked', false);
            }
            else {

                $(a).text($('#ContentPlaceHolder1_hdnUnSelectAll').val());
                $("#accordion-resizer :input[type='checkbox']").prop('checked', true);
            }

        }
    </script>


    <script type="text/javascript">

        $(document).ready(function () {

            if (readCookie("CollapseClick") === null || readCookie("CollapseClick") === "") {

                createCookie("CollapseClick", "Expand", 1);
                ExampanCollapsMenu();
            }
            else {
                SetExpandCollapse();
            }



        });
        function zomlan() {
            if ($('#ContentPlaceHolder1_lan').val() == 'Danish') {
                $zopim.livechat.setLanguage('da');
            }
        }
        function weeknamechange() {
            $(".fc-month-button").html($('#ContentPlaceHolder1_Month').val());
            $(".fc-agendaWeek-button").html($('#ContentPlaceHolder1_Week').val());

            $(".fc-agendaDay-button").html($('#ContentPlaceHolder1_Day').val());
            $(".fc-today-button").html($('#ContentPlaceHolder1_Today').val());

            $(".fc-jsun").text($('#ContentPlaceHolder1_sun').val());
            $(".fc-jmon").text($('#ContentPlaceHolder1_mon').val());
            $(".fc-jtue").text($('#ContentPlaceHolder1_tue').val());
            $(".fc-jwed").text($('#ContentPlaceHolder1_wed').val());
            $(".fc-jthu").text($('#ContentPlaceHolder1_thu').val());
            $(".fc-jfri").text($('#ContentPlaceHolder1_fri').val());
            $(".fc-jsat").text($('#ContentPlaceHolder1_sat').val());
        }

        function joyrideEnd() {



            var checkedValue = $('#chkTurnOffTour:checked').val();

            $.ajax({
                type: "POST",
                url: 'WebService1.asmx/JoyRideTourShowHide',
                data: "{'checkedValue':'" + checkedValue + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    $("#divResult").html(msg.d);
                },
                error: function (e) {
                    $("#divResult").html("WebSerivce unreachable");
                }
            });

        }
        function setUsername() {
            $(".WelComeUsername").text($("#span2").text());
            $(".plpSpan").text($("#lblPLP").text());
            $(".alpSpan").text($("#lblALP").text());
            $(".pdpSpan").text($("#lblpdp").text());
            $(".lastLogin").text($("#login").text());
        }
        function JoyrideStart() {

            setUsername();
            $('#joyRideTipContent').joyride({
                autoStart: true,
                postStepCallback: function (index, tip) {
                    if (index == 2) {
                        $(this).joyride('set_li', false, 1);
                    }
                }, exposed: [],
                modal: true,
                expose: true,
                next_button: true,      // true or false to control whether a next button is used
                prev_button: true,
            });
            $(".joyride-content-wrapper").mouseleave(function () { OnLoadCall(); });

        }
        jQuery(document).ready(function ($) {

            scrollBottom();
            $("#scrollbox6 li a").first().addClass("highlightContactName");
            HighlighThis();
            $("#scrollbox6 li a").first().addClass("highlightContactName");
            setTimeout(function () { $("#scrollbox6 li a").first().click(); $("#replyChat").hide(); }, 2000); //this to make sure that 1st li has been selected adn color of text is green.

            $("#ContentPlaceHolder1_lnkCatName").mouseover(function () {
                scrollBottom();
                ShowHideReplyDiv(true);
            });



        });
        function myFunction() {

            var txtto = $('#<%=txtto.ClientID %>').val();
            var txtsubject = $('#<%=txtsubject.ClientID %>').val();
            var txtmessge = $('#<%=txtmessge.ClientID %>').val();
            var fromTime = $(".timepicker1");
            var toTime = $(".timepicker2");
            var add = $("#Label1");
            //alert(add.val());
            if (txtto != '' && txtsubject != '' && txtmessge != '' && add.val() != '') {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Calendar.aspx/InsertData",
                    data: "{'txtto':'" + txtto + "','txtsubject':'" + txtsubject + "','txtmessge':'" + txtmessge + "','add':'" + add.val() + "','fromTime':'" + fromTime.val() + "','toTime':'" + toTime.val() + "'}",
                    dataType: "json",
                    success: function (data) {
                        var obj = data.d;
                        if (obj == 'true') {
                            $('#<%=txtto.ClientID %>').val('');
                            $('#<%=txtsubject.ClientID %>').val('');
                            $('#<%=txtmessge.ClientID %>').val('');
                            alert('Save Successfully');
                            document.getElementById('btnclose').click();
                        }
                    },
                    error: function (result) {
                        alert("Error");
                    }
                });
            }
            else {
                // modelPopupDilouge("Error", "Please enter all the fields", false);
                alert('Please enter all the fields');
                return false;
            }
        }
        function OnLoadCall() {
            setTimeout(function () {
                //GetAllUserByType(2);
                // GetData(""); //for chart
                // DrawCalendarData();
                scrollBottom();
            }, 10000);


            $('.timepicker1').timepicker({
                minuteStep: 1,
                template: 'modal',
                appendWidgetTo: 'body',
                showSeconds: true,
                showMeridian: false,
                defaultTime: true
            });
            $('.timepicker2').timepicker({
                minuteStep: 1,
                template: 'modal',
                appendWidgetTo: 'body',
                showSeconds: true,
                showMeridian: false,
                defaultTime: false
            });
            $(".drpAppointment").daterangepicker({
                format: 'DD/MM/YYYY',
                startDate: moment().subtract(29, 'days'),
                endDate: moment(),
                // showDropdowns: true,
                // showWeekNumbers: true,
            });
            $(".datepicker").pickadate({
                // from: -30, to: true ,
                selectMonths: true,
                selectYears: true,
                formatSubmit: 'dd/MM/yyyy',
                onSet: function (dt) {
                    console.log('Just set stuff:', dt);
                    // alert($(".datepicker").val());



                    //var myDate = new Date("February 3 2001");


                    // alert(dtt);

                    setTimeout(function () {
                        GoToDateFC();

                    }, 1000);


                }
            });
            ddpGraphUpdate();
            ddpVal();
            // GetAllUserByType(2);
            // GetData(""); //for chart
            var calendarData;
            GetCalendarData();
            interval_Userdetails();

            setTimeout(function () {

                DrawCalendarData();
                scrollBottom();
                GetAllUserByType(2);
                // GetData(""); //for chart

            }, 4000);

        }

        var datetimeFC;
        function GoToDateFC() {

            var myDate = new Date($(".datepicker").val());
            var month = myDate.getMonth() + 1;
            var dtt = (myDate.getFullYear() + "/" + month + "/" + myDate.getDate());
            datetimeFC = dtt;
            //  $('#calendar1').fullCalendar('gotoDate', '2014/12/05');
            setTimeout(function () {

                $('#calendar1').fullCalendar('gotoDate', datetimeFC);
                weeknamechange();
                /* alert(datetimeFC);*/
            }, 3000);

            // alert(dtt);

        }
        var calendarData;
        function GetCalendarData() {
            $.ajax({
                type: "POST",
                contentType: "application/json;charset=utf-8",
                url: "Calendar.aspx/GetCalendarData",
                data: "{}",
                dataType: "json",
                success: function (msg) {
                    $("#divResult").html(msg.d);
                    var data = msg.d;
                    calendarData = data;
                    //alert(data.d);
                },
                error: function (data) {
                    //  alert("Error");
                }
            });
        }
        function InsertData(title, start, end, allDay) {


            if (title != '' || title != 'undefined' || title != 'null') {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Calendar.aspx/InsertDataCalendar",
                    data: "{'title':'" + title + "','start':'" + start + "','end':'" + end + "','allDay':'" + allDay + "'}",
                    dataType: "json",
                    success: function (data) {
                        var obj = data.d;
                        if (obj == 'true') {
                            alert('Save SuccessFully');
                            document.getElementById('btnclose').click();
                        }
                    },
                    error: function (result) {
                        // alert("Error");
                    }
                });
            }
        }
        function DrawCalendarData() {
            var calendar = $('#calendar1').fullCalendar({

                monthNames: [$('#ContentPlaceHolder1_january').val(), $('#ContentPlaceHolder1_february').val(), $('#ContentPlaceHolder1_march').val(), $('#ContentPlaceHolder1_april').val(), $('#ContentPlaceHolder1_may').val(), $('#ContentPlaceHolder1_june').val(), $('#ContentPlaceHolder1_july').val(), $('#ContentPlaceHolder1_august').val(), $('#ContentPlaceHolder1_september').val(), $('#ContentPlaceHolder1_october').val(), $('#ContentPlaceHolder1_november').val(), $('#ContentPlaceHolder1_december').val()],
                monthNamesShort: [$('#ContentPlaceHolder1_jan').val(), $('#ContentPlaceHolder1_feb').val(), $('#ContentPlaceHolder1_mar').val(), $('#ContentPlaceHolder1_apr').val(), $('#ContentPlaceHolder1_mays').val(), $('#ContentPlaceHolder1_jun').val(), $('#ContentPlaceHolder1_jul').val(), $('#ContentPlaceHolder1_aug').val(), $('#ContentPlaceHolder1_sept').val(), $('#ContentPlaceHolder1_oct').val(), $('#ContentPlaceHolder1_nov').val(), $('#ContentPlaceHolder1_dec').val()],
                dayNames: [$('#ContentPlaceHolder1_Sunday').val(), $('#ContentPlaceHolder1_Monday').val(), $('#ContentPlaceHolder1_Tuesday').val(), $('#ContentPlaceHolder1_Wednesday').val(), $('#ContentPlaceHolder1_Thursday').val(), $('#ContentPlaceHolder1_Friday').val(), $('#ContentPlaceHolder1_Saturday').val()],
                dayNamesShort: [$('#ContentPlaceHolder1_sun').val(), $('#ContentPlaceHolder1_mon').val(), $('#ContentPlaceHolder1_tue').val(), $('#ContentPlaceHolder1_wed').val(), $('#ContentPlaceHolder1_thu').val(), $('#ContentPlaceHolder1_fri').val(), $('#ContentPlaceHolder1_sat').val()],

                googleCalendarApiKey: 'AIzaSyDcnW6WejpTOCffshGDDb4neIrXVUA1EAE',
                overlap: true,

                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                selectable: true,
                selectHelper: true,
                select: function (start, end, allDay) {
                    var title = prompt('Event Title:');
                    InsertData(title, start, end, allDay);

                    //webservice that save records
                    if (title) {
                        calendar.fullCalendar('renderEvent',
                            {
                                title: title,
                                start: start,
                                end: end,
                                allDay: allDay
                            },
                            true // make the event "stick"
                        );
                    }
                    calendar.fullCalendar('unselect');
                },
                editable: true,

                /*   events: 'usa__en@holiday.calendar.google.com',*/
                eventSources: [
            {
                googleCalendarId: 'usa__en@holiday.calendar.google.com',
                editable: false,
                color: '#6B6B6B',
                className: "blockFs",
                startEditable: false,
                allDay: true,
                selectable: false

            },
            {
                events: calendarData
            }
                ],


                eventRender: function (event, element) {
                    element.attr('href', 'javascript:void(0);');
                    element.click(function () {
                        if (event.message != null) {
                            $("#message").html((event.message));
                        } else {
                            $("#Message").hide();
                        }
                        if (event.start != null) {
                            $("#startTime").html((event.start).format('MMM Do h:mm A'));
                        } else {
                            $("#Start").hide();
                        }
                        if (event.end != null) {
                            $("#endTime").html((event.end).format('MMM Do h:mm A'));
                        } else {
                            $("#End").hide();
                        }
                        if (event.title != null) {
                            $("#subject").html((event.title));
                        } else {
                            $("#Subject").hide();
                        }


                        // $("#endTime").html(moment(event.end).format('MMM Do h:mm A'));
                        // $("#eventInfo").html(event.description);
                        // $("#eventLink").attr('href', event.url);
                        //$("#eventContent").modalPopLite({ openButton: '#clicker', closeButton: '#close-btn' });
                        $("#eventContent").dialog({
                            show: {
                                effect: "blind",
                                duration: 1000
                            },
                            hide: {
                                effect: "fade",
                                duration: 1000
                            },

                            open: function () {
                                jQuery('.ui-widget-overlay').bind('click', function () {
                                    jQuery('.modelDilougBoxCalandar').dialog('close');
                                });
                            }

                        });
                        $('.modelDilougBoxCalandar').show();

                    });
                }
            });
            weeknamechange();
            zomlan();


        }
        function DrawCalendarData1() {
            var calendar = $('#calendar').fullCalendar({
                header: {
                    left: 'prev,next today',
                    center: 'title',
                    right: 'month,agendaWeek,agendaDay'
                },
                googleCalendarApiKey: 'AIzaSyBrmJ6hf79lHtIxOoVLs9_7sSVM_OL6j3c',
                events: {
                    googleCalendarId: 'en-gb.danish#holiday@group.v.calendar.google.com',
                    className: 'gcal-event' // an option!
                },
                selectable: true,
                selectHelper: true,
                select: function (start, end, allDay) {
                    var title = prompt('Event Title:');
                    InsertData(title, start, end, allDay);

                    //webservice that save records
                    if (title) {
                        calendar.fullCalendar('renderEvent',
                            {
                                title: title,
                                start: start,
                                end: end,
                                allDay: allDay
                            },
                            true // make the event "stick"
                        );
                    }
                    calendar.fullCalendar('unselect');
                },
                editable: true,
                // events: calendarData,
                /* viewRender: function(view, element) {
                     $('#calendar').fullCalendar( 'gotoDate', 2014, 4, 24 );
                 },*/
                eventRender: function (event, element) {
                    element.attr('href', 'javascript:void(0);');
                    element.click(function () {
                        if (event.message != null) {
                            $("#message").html((event.message));
                        } else {
                            $("#Message").hide();
                        }
                        if (event.start != null) {
                            $("#startTime").html((event.start).format('MMM Do h:mm A'));
                        } else {
                            $("#Start").hide();
                        }
                        if (event.end != null) {
                            $("#endTime").html((event.end).format('MMM Do h:mm A'));
                        } else {
                            $("#End").hide();
                        }
                        if (event.title != null) {
                            $("#subject").html((event.title));
                        } else {
                            $("#Subject").hide();
                        }


                        // $("#endTime").html(moment(event.end).format('MMM Do h:mm A'));
                        // $("#eventInfo").html(event.description);
                        // $("#eventLink").attr('href', event.url);
                        //$("#eventContent").modalPopLite({ openButton: '#clicker', closeButton: '#close-btn' });
                        $("#eventContent").show();
                    });
                }
            });

        }
        var x;
        function UpdateChatName(name) {

            setTimeout(function () {
                x = name;
                $('#chatterName').text('Chat : ' + x.text);
            }, 1000);
        }
        function scrollBottom() {

            setTimeout(function () {
                $('#scrollbox6').scrollTop(100000000000000000);

            }, 500);
        }
        function highlightOnSend() {
            var id = $(".highlightContactName").attr("id");
            $("#" + id).addClass("highlightContactName");
            setTimeout(function () { $("#" + id).addClass("highlightContactName"); HighlighThis(); }, 500);
            setTimeout(function () { $("#" + id).addClass("highlightContactName"); HighlighThis(); }, 1000);
            HighlighThis();
        }
        function SubmitFrm(cn) {
            window.location = cn;
        }
        function HighlighThis() {

            $("#scrollbox6 li a").click(function () {
                $("#scrollbox6 li a").css("color", "black");
                $("#scrollbox6 li a").removeClass("highlightContactName");
                a = jQuery(this);
                $("#" + a.attr("id")).addClass("highlightContactName");
                setTimeout(function () { $("#" + a.attr("id")).addClass("highlightContactName"); HighlighThis(); }, 500);
                setTimeout(function () { $("#" + a.attr("id")).addClass("highlightContactName"); HighlighThis(); }, 1000);
                setTimeout(function () { $("#" + a.attr("id")).addClass("highlightContactName"); }, 2000);
            });

        }

        function ShowHideContactListForChat() {
            setTimeout(function () {
                $('#contactList').toggle('slow');
                ShowHideReplyDiv();
            }, 1000);
        }
        var chatOn = false;
        function ShowHideReplyDiv(force) {

            if (force) {
                $("#replyChat").show();
                return;
            }
            if ($('#chatterName').text() == "Chat") {
                $("#replyChat").hide();
            } else {
                if (chatOn) {
                    $("#replyChat").show();
                    chatOn = false;
                } else {
                    $("#replyChat").hide();
                    chatOn = true;
                }
            }


        }
        function showpopup1() {
            document.getElementById("ContentPlaceHolder1_txtName").value = "";
            document.getElementById("ContentPlaceHolder1_txtaddress").value = "";
            document.getElementById("ContentPlaceHolder1_txtEmail").value = "";
            document.getElementById("ContentPlaceHolder1_txtMobile").value = "";

            document.getElementById("ContentPlaceHolder1_ddlDepartment").value = "0";
            document.getElementById("ContentPlaceHolder1_ddljobtype1").value = "0";
            document.getElementById("ContentPlaceHolder1_ddlTeam").value = "0";
            document.getElementById("ContentPlaceHolder1_ddlgender").value = "0";


            $("#ContentPlaceHolder1_txtLastname").val("");
            //$("#ContentPlaceHolder1_ddlLanguage").val("English");
            $("#<%=ddlLanguage.ClientID%>").val("0");
            $("#<%=ddlTeam.ClientID%>").val("0");
            $("#ContentPlaceHolder1_ddlDepartment").val("0");
            $("#ContentPlaceHolder1_ddljobtype1").val("0");
            $(".otherDiv").show();
            $("#CompetenceCheckboxes").hide();
            $("#submitButtons").hide();
            $("#nextButtons").show();

            $("#ContentPlaceHolder1_RequiredFieldValidator1").hide();
            $("#ContentPlaceHolder1_RequiredFieldValidator2").hide();
            $("#ContentPlaceHolder1_RequiredFieldValidator3").hide();
            $("#ContentPlaceHolder1_revEmail").hide();
            $("#ContentPlaceHolder1_RequiredFieldValidator4").hide();
            $("#ContentPlaceHolder1_RegularExpressionValidator6").hide();

            $("#ContentPlaceHolder1_RequiredFieldValidatorddlLanguage").hide();
            $("#ContentPlaceHolder1_RequiredFieldValidatorddljobtype1").hide();
            $("#ContentPlaceHolder1_RequiredFieldValidatorddlDepartment").hide();
            $("#ContentPlaceHolder1_RequiredFieldValidatorddlTeam").hide();
            $("#ContentPlaceHolder1_RequiredFieldValidatorddlgender").hide();

        }

        function checkDuplicateEmail() {

            var email = $("#<%=txtEmail.ClientID%>").val();
            if (email.length > 4) {
                console.log(email);
                $.ajax({
                    type: "POST",
                    async: true,
                    url: "webservice1.asmx/UserEmailCheckDuplication",
                    data: "{'email':'" + email + "'}",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (e) {
                        $("#divResult").html(e.d);
                        var msg = e.d;
                        if (msg == "Success") {
                            $("#<%=lblEmail.ClientID%>").text("");
                        } else {
                            setTimeout(function () {
                                $("#<%=lblEmail.ClientID%>").text(msg);
                            }, 500);
                        }


                        //return msg.d;

                    },
                    error: function (e) {
                        $("#divResult").html("WebSerivce unreachable");

                    }
                });
            }
        }

        var ddGraphDivisionVal = 0;
        var ddGraphJobTypeVal = 0;

        function ddpGraphUpdate() {
            $("#<%=ddldropdownDivisionGraph.ClientID%>").change(function () {

                console.log("changed");
                ddGraphDivisionVal = $("#<%=ddldropdownDivisionGraph.ClientID%>").val();
                // alert(selectedText);
                console.log("selected text= " + ddGraphDivisionVal);
                // alert(ddGraphDivisionVal);
                GetDataByDivJobTypeID("", ddGraphDivisionVal, ddGraphJobTypeVal);

            });

            $("#<%=ddlJobType.ClientID%>").change(function () {

                console.log("changed");
                ddGraphJobTypeVal = $("#<%=ddlJobType.ClientID%>").val();
                // alert(selectedText);
                console.log("selected text= " + ddGraphJobTypeVal);
                //alert(ddGraphJobTypeVal);
                GetDataByDivJobTypeID("", ddGraphDivisionVal, ddGraphJobTypeVal);
            });
        }
        var selectedText;
        function ddpVal() {
            $("#<%=ddlDepartment.ClientID%>").change(function () {

                console.log("changed");
                var selectedText = $("#<%=ddlDepartment.ClientID%>").val();
                // alert(selectedText);
                console.log("selected text= " + selectedText);
            });
        }
        function generate(type, text, layout) {
            var n = noty({
                text: text,
                type: type,
                dismissQueue: true,
                timeout: 10000,
                closeWith: ['click'],
                layout: layout,
                theme: 'defaultTheme',
                maxVisible: 10
            });
            console.log('html: ' + n.options.id);
        }

        function generate1(type, text) {
            var n = noty({
                text: text,
                type: type,
                dismissQueue: true,
                timeout: 10000,
                closeWith: ['click'],
                layout: "topCenter",
                theme: 'defaultTheme',
                maxVisible: 10
            });
            console.log('html: ' + n.options.id);
        }

        function generateAll() {
            generate('alert', 'alert');
            generate('information', 'information');
            generate('error', 'e');
            generate('warning', 'w');
            generate('notification', 'n');
            generate('success', 's');
        }



        var userList;
        function GetAllUserByType(userType) {
            $.ajax({
                type: "POST",
                async: true,
                url: "webservice1.asmx/GetAllUserByType",
                data: "{'userTypeId':'" + userType + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (e) {
                    $("#divResult").html(e.d);
                    var msg = e.d;

                    userList = msg;
                    console.log(msg);
                    if (userList != "error") {
                        RanderUser(userList);
                    } else {
                        //alert("");
                    }
                },
                error: function (e) {
                    $("#divResult").html("WebSerivce unreachable");
                }
            });
        }

        function RanderUser(userList) {
            var html = "";
            $("#userList").html('');
            var dataUser = JSON.parse(userList);

            $.each(dataUser.Table, function (i, item) {

                //html += '<li><a href="#" title=""><img src="../Log/upload/Userimage/' + item.userImage + '" alt="" /></a>';
                //html += ' <div class="member-hover"><a href="#" title=""><img src="../Log/upload/Userimage/' + item.userImage + '" alt="" /></a> ';
                //html += '<span>' + item.name + '</span></div></li>';
                // item.UserID
                // item.Username
                html += '<li><a href="#" title="" onclick ="GetGraphById(' + item.userId + ',\'' + item.name + '\')' + '">';
                html += '<img src="../Log/upload/Userimage/' + item.userImage + '" alt="" /></a>';
                html += ' <div class="member-hover" style="width: 130px;"><a href="#" title=""><img src="../Log/upload/Userimage/' + item.userImage + '" alt="" /></a> ';
                html += '<span>' + item.name + '</span></div></li>';
            });
            $("#userList").append(html);

        }
    </script>
    <script>
        var imageto; var subjectto;
        function Default_Userdetails() {
            interval_Userdetails();
        }

        function click() {

            $(".contact_name").click(function () {

                $(".timeline-head .aa").text("Chat : " + ($(this).find('.chat_notification1').text()));
            });
            //$(".contact_name").click(function () { alert($(this).text()); });
        }
        function set_DownScrol() {
            $("#scrollbox6").animate({ scrollTop: $("#scrollbox6").height() + 9999 }, "slow");
        }

        var interval1 = 0;
        var ajex;
        function interval_Userdetails() {

            var Count;
            //            var notificationCount = [];
            Userdetails();
            interval1 = setInterval(function () {
                if (ajex != null) {
                    ajex.abort();
                }
                ajex = Userdetails();
            }, 20000);
        }


        var touserid = '0';
        function Userdetails() {
            var html = "";
            $("#name_scroll1").html('');
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "Organisation-dashboard.aspx/Userdetails",
                data: "{}",
                dataType: "json",
                success: function (data) {
                    if (data.d == null) {
                        window.location.href = "Organisation-dashboard.aspx";
                    }
                    cat = data.d;
                    for (var i = 0; i < data.d.length; i++) {
                        imageto = data.d[i].sessionimage;
                        html += "<li class='contact_name' onclick ='GetmessgeById(" + data.d[i].userId + ")'><a href='#div_contactlist'>";
                        if (data.d[i].notification != 0) {
                            html += "<div class='chat_notification'>" + data.d[i].notification + "</div>";
                        }
                        html += "<img src='../Log/upload/Userimage/" + data.d[i].Image + "' alt=''/>";
                        html += "</a><p class='chat_notification1'><a href='#div_contactlist' title='' >" + data.d[i].name + "</a></p>";
                        html += "</li>";
                    }
                    if (data.d.length == 0) {
                        //html += "No records found.";
                        html += '<li><asp:Label ID="Label3" CssClass="aa" runat="server" meta:resourcekey="NoRecordFound" EnableViewState="false"></asp:Label></li>';
                    }
                    $("#name_scroll1").append(html);
                    click();
                },
                error: function (result) {
                    //  alert("Error");

                }
            });
        }

        function GetmessgeById(userId) {
            if (userId != '0') {
                click();

                touserid = userId;
                var html = "";
                $("#scrollbox6").html('');
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "Organisation-dashboard.aspx/GetmessgeById",
                    data: "{'userId':'" + userId + "'}",
                    dataType: "json",
                    success: function (data) {
                        cat = data.d;
                        for (var i = 0; i < data.d.length; i++) {
                            var cls = "reply";
                            if (data.d[i].mesfromUserId == data.d[i].sessionUserId) {
                                cls = "reply1";
                            }
                            html += "<li class='" + cls + "'><div class='chat-thumb'><img src='../Log/upload/Userimage/" + data.d[i].Image + "' alt=''/></div>";
                            html += "<div class='chat-desc'><p>" + data.d[i].messubject + "</p><i class='chat-time'>" + data.d[i].masCreatedDate + "</i>";
                            html += "</div></li>";
                        }
                        setInterval(function () {
                            GetmessgeById_afterinsert(touserid);
                        }, 10000);

                        if (data.d.length == 0) {
                            html += "No records found.";
                        }
                        $("#scrollbox6").append(html);
                    },
                    error: function (result) {
                        //  alert("Error");

                    }
                });
            }
        }


        function insertmessage() {
            subjectto = '';

            var message = document.getElementById("ContentPlaceHolder1_message1").value;
            //debugger;
            if (touserid != '0') {
                if (message != '') {
                    subjectto = message;
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "chart.aspx/insertmessage",
                        data: "{'message':'" + message + "','touserid':'" + touserid + "'}",
                        dataType: "json",
                        success: function (data) {
                            var obj = data.d;
                            if (obj.search('true') == 0) {
                                document.getElementById("ContentPlaceHolder1_message1").value = '';
                                // GetmessgeById(touserid);
                                //GetmessgeById_afterinsert(touserid)
                                //debugger;
                                binddata();
                            }
                        },
                        error: function (result) {
                            // alert("Error");
                        }
                    });
                }
                else {
                    alert('Please enter fields');
                    return false;
                }
            }
            else {
                alert('Please Select Contact');
                document.getElementById("ContentPlaceHolder1_message1").value = '';
                return false;
            }
        }


        function GetmessgeById_afterinsert(userId) {
            if (userId != '0') {
                touserid = userId;
                var html = "";
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "chart.aspx/GetAllmessagebyId_stauts",
                    data: "{'userId':'" + userId + "'}",
                    dataType: "json",
                    success: function (data) {
                        cat = data.d;
                        for (var i = 0; i < data.d.length; i++) {
                            var cls = "reply";
                            if (data.d[i].mesfromUserId == data.d[i].sessionUserId) {
                                cls = "reply1";
                            }
                            html += "<li class='" + cls + "'><div class='chat-thumb'><img src='../Log/upload/Userimage/" + data.d[i].Image + "' alt=''/></div>";
                            html += "<div class='chat-desc'><p>" + data.d[i].messubject + "</p><i class='chat-time'>" + data.d[i].masCreatedDate + "</i>";
                            html += "</div></li>";
                        }
                        $("#scrollbox6").append(html);
                        set_DownScrol();
                    },
                    error: function (result) {
                        //alert("Error");

                    }
                });
            }
        }

        function GoToDate() {
            $('#calendar').fullCalendar('gotoDate', '22/04/2015');
        }

        function binddata() {

            var now = new Date();
            var date = now.format("dd/M/yy h:mm tt");
            var html = "";
            html += "<li class='reply1'><div class='chat-thumb'><img src='../Log/upload/Userimage/" + imageto + "' alt=''/></div>";
            html += "<div class='chat-desc'><p>" + subjectto + "</p><i class='chat-time'>" + date + "</i>";
            html += "</div></li>";
            $("#scrollbox6").append(html);
            set_DownScrol();
        }
        function CallCompetence() {

            if ($('#ContentPlaceHolder1_txtName').val() == '' || $('#ContentPlaceHolder1_txtlastName').val() == ''
                || $('#ContentPlaceHolder1_txtaddress').val() == '' || $('#ContentPlaceHolder1_txtEmail').val() == ''
                || $('#ContentPlaceHolder1_txtMobile').val() == '' || $('#ContentPlaceHolder1_ddlgender').val() == '0'
                || $('#ContentPlaceHolder1_ddlLanguage').val() == '0' || $('#ContentPlaceHolder1_jobtype1').val() == '0' || $('#ContentPlaceHolder1_ddlDepartment').val() == '0'
                || $('#ContentPlaceHolder1_ddlTeam').val() == '0') {

                ValidatorEnable($("[id$='RequiredFieldValidator1']")[0], true);
                ValidatorEnable($("[id$='RequiredFieldValidator2']")[0], true);
                ValidatorEnable($("[id$='RequiredFieldValidator3']")[0], true);
                ValidatorEnable($("[id$='RequiredFieldValidator4']")[0], true);

                ValidatorEnable($("[id$='RequiredFieldValidatorddlLanguage']")[0], true);

                ValidatorEnable($("[id$='RequiredFieldValidatorddljobtype1']")[0], true);

                ValidatorEnable($("[id$='RequiredFieldValidatorddlDepartment']")[0], true);
                ValidatorEnable($("[id$='RequiredFieldValidatorddlTeam']")[0], true);
                ValidatorEnable($("[id$='RequiredFieldValidatorddlgender']")[0], true);

                ValidatorEnable($("[id$='revEmail']")[0], true);
            }
            else {

                ShowCompotenceCategory(true);
            }
        }
        function GoNext() {

            if ($("#<%=lblEmail.ClientID%>").text() != "") {
                alert("Email already exists.");
                return;
            }
            else {
                CallCompetence();
            }
        }

        var msgTest = "";
        function AddNewUser() {
            //debugger;
            if (!CheckValidations("chkNewEmp")) {
                return;
            }

            generate($(".Information").text(), 'Please wait, we are processing to add new user.', 'bottomCenter');
            setTimeout(function () {
                modelPopupDilouge($(".AddNewUser").text(), $(".NewManAddedSuccess").text(), true);
                $(".close").click();
            }, 3000);
            //            $("#add-post-title").modal("toggle");
            var Name = $("#<%=txtName.ClientID%>").val();
            var LastName = $("#<%=txtLastname.ClientID%>").val();
            var email = $("#<%=txtEmail.ClientID%>").val();
            var mobile = $("#<%=txtMobile.ClientID%>").val();
            var address = $("#<%=txtaddress.ClientID%>").val();
            var userdepId = document.getElementById("ContentPlaceHolder1_ddlDepartment").value;
            var jobtype = document.getElementById("ContentPlaceHolder1_ddljobtype1").value;

            var team = document.getElementById("ContentPlaceHolder1_ddlTeam").value;
            var gender = $("#<%=ddlgender.ClientID%>").find("option:selected").val();
            var Language = $("#<%=ddlLanguage.ClientID%>").find("option:selected").text();
            var selectedValues = [];
            $("input:checked").each(function () {
                selectedValues.push($(this).val());
            });
            if (selectedValues.length > 0) {
                // alert("Selected Value(s): " + selectedValues);
            } else {
                // alert("No item has been selected.");
            }

            var selectedText1 = $("#<%=ddlDepartment.ClientID%>").val();
            //  alert($("#<%=ddlDepartment.ClientID%>").val());

            $.ajax({
                type: "POST",
                async: true,
                url: "Organisation-dashboard.aspx/AddNewEmp",
                data: "{'email':'" + email + "','address':'" + address + "','mobile':'" + mobile + "','userdepId':'" + userdepId
                    + "','jobtype':'" + jobtype + "','selectedValues':'" + selectedValues + "','Name':'" + Name
                    + "','LastName':'" + LastName + "','Language':'" + Language + "','Team':'" + team + "','gender':'" + gender + "'}",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (e) {
                    window.location.reload();
                    msgTest = e;
                    $("#divResult").html(e.d);
                    var msg = e.d;
                    if (msg == "success") {
                        //generate('information', 'Email has been submitted to new manager.', 'bottomCenter');
                        generate($(".Information").text(), $(".EmailSubmittedToNewManager").text(), 'bottomCenter');
                        // GetAllUserByType(2);
                        $(".close").click();
                        // modelPopupDilouge($(".AddNewUser").text(), "New manager added successfully.", true);
                        setTimeout(function () {
                            modelPopupDilouge($(".AddNewUser").text(), $(".NewManAddedSuccess").text(), true);
                        });

                        $("#add-post-title").modal("hide");

                    } else {
                        modelPopupDilouge($(".AddNewUser").text(), msg.d, true);
                        //modelPopupDilouge("Add New User", msg.d, false);

                    }
                },
                error: function (e) {
                    $("#divResult").html("WebSerivce unreachable");
                },
                complete: function () {
                    window.location.reload(true);
                }
            });
            // GetAllUserByType(2);

        }


    </script>
    <script type="text/javascript">
        function SetChartType(a) {

            $('#hdnChartType').val(a);


        }
    </script>

    <script type="text/javascript">
        $(document).ready(function () {

            $('#ContentPlaceHolder1_managercat').multiselect({

                //enableFiltering: true,
                includeSelectAllOption: true,
                //buttonWidth: '400px',
                selectAllText: 'All Competence',
                //  //nonSelectedText: 'None Location selected',
                allSelectedText: 'All Competence',
                //  nSelectedText: 'selected',
                //  numberDisplayed: 2,


                buttonText: function (options) {

                    if (options.length === 0) {

                        return $('#ContentPlaceHolder1_hdnNoOption').val();

                    }
                    var labels = [];
                    var num = [];

                    options.each(function () {
                        if ($(this).attr('value') !== undefined) {
                            labels.push($(this).text());
                            num.push($(this).val());

                        }
                    });
                    $('#ddlCompetence').val(num.join(','));

                    return labels.join(', ');
                },
                onChange: function (event) {

                    GetData("");


                },


            });

        });
    </script>

    <script type="text/javascript">
        $(document).ready(function () {

            $("#btnbak").click(function () {

                $("#CompetenceCheckboxes").hide();
                $("#nextButtons").show();
                $("#submitButtons").hide();
            });

        });
    </script>
    <div style="display: none">
        <asp:Label ID="Literal15" runat="server" CssClass="AddNewUser" meta:resourcekey="AddNewManager" />
        <asp:Label ID="Literal16" runat="server" CssClass="NewManAddedSuccess" meta:resourcekey="NewManAddedSuccess" />
        <asp:Label ID="Literal17" runat="server" CssClass="EmailSubmittedToNewManager" meta:resourcekey="EmailSubmittedToNewManager" />
        <asp:Label ID="Label5" runat="server" CssClass="Information" meta:resourcekey="information" />

    </div>
</asp:Content>
