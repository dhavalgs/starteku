﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="Profile.aspx.cs" Inherits="Organisation_Profile" Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .ProfileImage {
            width: 150px;
            height: 150px;
            border-radius: 100px;
            border: 1px solid blue;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg"
        meta:resourcekey="lblMsgResource1"></asp:Label>
    <div class="col-md-6">
        <div class="heading-sec">
            <h1>
                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="Profile" EnableViewState="false" />
                <%--  <%= CommonMessages.Profile%>--%> <i><span runat="server" id="Profile1"></span></i>
            </h1>
        </div>
    </div>
    <br />
    <br />
    <div class="col-md-6 range ">
        <%--<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
            <i class="fa fa-calendar-o icon-calendar icon-large"></i>
            <span>August 5, 2014 - September 3, 2014</span>  <b class="caret"></b>
        </div>--%>
    </div>
    <div class="col-md-12">
        <div class="chat-widget widget-body" style="background: #fff;" id="edit" runat="server">
            <div class="chat-widget-head yellow">
                <h4 style="margin: -6px 0px 0px;" ><asp:Label ID="lblPersonal" runat="server" meta:resourcekey="PersonalInfoResource"  Text="Personal Information" /></h4>
                
                <%--<h4 style="margin: -6px 0px 0px;">Personal Information</h4>--%>
            </div>
            <div class="indicatesRequireFiled">
                <i>* 
                    <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Indicatesrequiredfield" EnableViewState="false" />
                    <%--<%= CommonMessages.Indicatesrequiredfield%>--%></i>
            </div>
            <div class="col-md-6">
                <div style="width: 100%; float: left;">
                    <div class="col-md-4">
                        <asp:Image ID="img_profile" CssClass="ProfileImage" ImageUrl="~/Organisation/images/sign-in1.jpg"
                            runat="server" Style="border: 0px;"
                            meta:resourcekey="img_profileResource1" />
                    </div>
                    <div class="col-md-8" style="padding-top: 10%;">
                         <input type="button"  meta:resourcekey="flupload1Resource1" runat="server" class="btn btn-primary yellow col-md-3 col-xs-3 col-sm-3 col-lg-3" onclick="$('#ContentPlaceHolder1_flupload1').click()" value="Browse" style="height: 40px; margin-bottom: 10px; float: left;" />
                        <asp:FileUpload ID="flupload1" runat="server" class="btn btn-primary yellow"
                            meta:resourcekey="flupload1Resource1" onchange="readURL(this)" style="display:none; width: 25%;border-radius: 0px;border: medium none;width: 25%;border-radius: 0px;border: medium none;height: 26px;padding-bottom: 8%;"></asp:FileUpload>
                        <asp:TextBox runat="server" ReadOnly="true" id="fluploadText" class="yellow col-md-9 col-xs-6 col-sm-6 col-lg-9" style="border-radius: 0px; color: white; padding-left: 2%; height: 40px; float: left; "/>
                        <%--<input runat="server" id="fluploadText" class="yellow" style="position: absolute; left: 47%; border: medium none; background-color: transparent; height: 22%; border-radius: 0px; top: 46.5%; color: white; width: 45%; padding-left: 2%;" type="text"/>--%>
                        <asp:RegularExpressionValidator ID="reFile1" runat="server" ControlToValidate="flupload1"
                            CssClass="legend" Display="Dynamic" ErrorMessage="Please upload only .gif | .jpeg | .jpg   | .png image."
                            ValidationExpression="^.*\.(JPG|jpg|jpeg|PNG|png|JPEG)$" ForeColor="Red"
                            ValidationGroup="chk1" meta:resourcekey="reFile1Resource1"></asp:RegularExpressionValidator>
                        <br />
                        


                    </div>
                    <div class="col-md-8">
                    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                            <ContentTemplate>
                                <%if (!img_profile.ImageUrl.Contains("ofile_img.png"))
                                  {%>
                                <asp:Button ID="Button1" type="button" class="btn btn-primary black" Style="background-color: gray;" OnClick="btnRemoveImg_Click" OnClientClick="removeImg()" runat="server" Text="Remove Image" meta:resourcekey="RemoveImageResource"></asp:Button>
                                <%} %>
                                 <asp:HiddenField ID="hdnIsRemoveImage" Value="0" runat="server" />
                            </ContentTemplate>
                        </asp:UpdatePanel>
                        </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label">
                        <%-- <%= CommonMessages.FirstName%>--%>
                        <asp:Literal ID="Literal3" runat="server" meta:resourcekey="FirstName" EnableViewState="false" />
                        :<span class="starValidation">*</span></label>
                    <asp:TextBox runat="server" ID="txtfName" MaxLength="80" onkeyup="run(this)"
                        CssClass="input-style" meta:resourcekey="txtfNameResource1" /><br />
                   <%-- <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                        TargetControlID="txtfName" Enabled="True" ValidChars=" ">
                    </cc1:FilteredTextBoxExtender>--%>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtfName"
                        ErrorMessage="Please enter First Name." CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator5Resource1"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label">
                        <%--<%= CommonMessages.LastName%>--%>
                        <asp:Literal ID="Literal4" runat="server" meta:resourcekey="LastName" EnableViewState="false" />:<span class="starValidation">*</span></label>
                    <asp:TextBox runat="server" ID="txtlname" MaxLength="80" onkeyup="run(this)"
                        CssClass="input-style" meta:resourcekey="txtlnameResource1" /><br />
                   <%-- <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                        TargetControlID="txtlname" Enabled="True" ValidChars=" ">
                    </cc1:FilteredTextBoxExtender>--%>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtlname"
                        ErrorMessage="Please enter Last name." CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator1Resource1"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label">
                        Email:<span class="starValidation">*</span></label>
                    <asp:TextBox runat="server" ID="txtEmail" MaxLength="80" onkeyup="run(this)"
                        CssClass="input-style" meta:resourcekey="txtEmailResource1" /><br />
                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" CssClass="commonerrormsg"
                        ControlToValidate="txtEmail" Display="Dynamic" ValidationGroup="chk"
                        ErrorMessage="Please enter email." meta:resourcekey="rfvEmailResource1"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revEmail" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                        ControlToValidate="txtEmail" CssClass="commonerrormsg" Display="Dynamic" ValidationGroup="chk"
                        ErrorMessage="Please enter valid email."
                        meta:resourcekey="revEmailResource1"></asp:RegularExpressionValidator>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label" style="width: 100%">
                        <%-- <%= CommonMessages.Gender%>--%>
                        <asp:Literal ID="Literal5" runat="server" meta:resourcekey="Gender" EnableViewState="false" />:*</label>
                    <asp:DropDownList ID="ddlgender" runat="server" SCssClass="form-control" Style="width: 100%; height: 36px; border: 1px solid #d4d4d4; display: inline;"
                        meta:resourcekey="ddlgenderResource1">
                        <asp:ListItem Value="Male" meta:resourcekey="ListItemResource1"> </asp:ListItem>
                        <asp:ListItem Value="Female"   meta:resourcekey="ListItemResource2"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label">
                        <%-- <%= CommonMessages.Password%>--%>
                        <asp:Literal ID="Literal6" runat="server" meta:resourcekey="Password" EnableViewState="false" />:<span class="starValidation">*</span></label>
                    <asp:TextBox ID="txtPassword" class="input-style" runat="server" MaxLength="20"
                        TextMode="Password" meta:resourcekey="txtPasswordResource1" /><br />
                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" CssClass="commonerrormsg"
                        ControlToValidate="txtPassword" Display="Dynamic" ValidationGroup="chk"
                        ErrorMessage="Password is required." meta:resourcekey="rfvPasswordResource1"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revPassword" runat="server" ControlToValidate="txtpassword"
                        ValidationExpression="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4}.*$" CssClass="commonerrormsg"
                        Display="Dynamic" ValidationGroup="chk"
                        ErrorMessage="Password must be at least 4 characters, and must include at least one upper case letter, one lower case letter, and one numeric digit"
                        meta:resourcekey="revPasswordResource1" />
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label">
                        <%--  <%= CommonMessages.ConfirmPassword%>--%>
                        <asp:Literal ID="Literal7" runat="server" meta:resourcekey="ConfirmPassword" EnableViewState="false" />:<span class="starValidation">*</span></label>
                    <asp:TextBox ID="txtConfirmPassword" type="text" class="input-style" runat="server"
                        MaxLength="20" TextMode="Password"
                        meta:resourcekey="txtConfirmPasswordResource1" /><br />
                    <asp:CompareValidator ID="cvPasswordNotMatch" ControlToValidate="txtConfirmPassword"
                        ControlToCompare="txtPassword" runat="server" CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk"
                        ErrorMessage="Password and confirm password does not match."
                        meta:resourcekey="cvPasswordNotMatchResource1"></asp:CompareValidator>
                    <asp:RequiredFieldValidator ID="rfvConfirmPassword" runat="server" CssClass="commonerrormsg"
                        ControlToValidate="txtConfirmPassword" Display="Dynamic" ErrorMessage="Confirm password is required."
                        ValidationGroup="chk" meta:resourcekey="rfvConfirmPasswordResource1"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label">
                        <%-- <%= CommonMessages.Phone%>--%>
                        <asp:Literal ID="Literal8" runat="server" meta:resourcekey="Phone" EnableViewState="false" />:<span class="starValidation">*</span></label>
                    <asp:TextBox runat="server" ID="txtMobile" CssClass="input-style"
                        MaxLength="15" meta:resourcekey="txtMobileResource1"></asp:TextBox><br />
                    <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtMobile"
                        ErrorMessage="Minimum mobile length is 9." Display="Dynamic" CssClass="commonerrormsg"
                        ValidationGroup="chk" ValidationExpression=".{9}.*" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtMobile"
                        ErrorMessage="Please enter mobile." CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk"></asp:RequiredFieldValidator>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Numbers"
                        TargetControlID="txtMobile" Enabled="True">
                    </cc1:FilteredTextBoxExtender>--%>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtMobile"
                        ErrorMessage="Minimum mobile length is 10." Display="Dynamic" CssClass="commonerrormsg"
                        ValidationGroup="chk" ValidationExpression=".{10}.*"
                        meta:resourcekey="RegularExpressionValidator6Resource1" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtMobile"
                        ErrorMessage="Please enter mobile." CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator6Resource1"></asp:RequiredFieldValidator>
                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Custom, Numbers"
                        ValidChars=" ,+,(,),-" TargetControlID="txtMobile" Enabled="True">
                    </cc1:FilteredTextBoxExtender>
                </div>
            </div>
            <asp:UpdatePanel ID="qw" runat="server">
                <ContentTemplate>
                    <div class="col-md-6">
                        <div class="inline-form">
                            <label class="c-label" style="width: 100%">
                                <%-- <%= CommonMessages.Country%>--%>
                                <asp:Literal ID="Literal9" runat="server" meta:resourcekey="Country" EnableViewState="false" />:<span class="starValidation">*</span></label>
                            <asp:DropDownList ID="ddlcountry" runat="server" CssClass="form-control" Style="width: 100%; height: 36px; border: 1px solid #d4d4d4; display: inline;"
                                AutoPostBack="True"
                                OnSelectedIndexChanged="ddlcountry_SelectedIndexChanged"
                                meta:resourcekey="ddlcountryResource1">
                            </asp:DropDownList>
                            <br />
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="ddlcountry"
                                ErrorMessage="Please select Country." InitialValue="0" CssClass="commonerrormsg"
                                Display="Dynamic" ValidationGroup="chk"
                                meta:resourcekey="RequiredFieldValidator14Resource1"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group" id="state" runat="server" style="display: none">
                        <div class="col-md-6">
                            <div class="inline-form">
                                <label class="c-label" style="width: 100%">
                                    <%-- <%= CommonMessages.State%>--%>
                                    <asp:Literal ID="Literal10" runat="server" meta:resourcekey="State" EnableViewState="false" />:<span class="starValidation">*</span></label>
                                <asp:DropDownList ID="ddlstate" runat="server" CssClass="form-control" Style="width: 100%; height: 36px; border: 1px solid #d4d4d4; display: inline;"
                                    AutoPostBack="True"
                                    OnSelectedIndexChanged="ddlstate_SelectedIndexChanged"
                                    meta:resourcekey="ddlstateResource1">
                                </asp:DropDownList>
                                <br />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlstate"
                                    ErrorMessage="Please select State." InitialValue="0" CssClass="commonerrormsg"
                                    Display="Dynamic" ValidationGroup="chk"
                                    meta:resourcekey="RequiredFieldValidator2Resource1"></asp:RequiredFieldValidator>
                            </div>
                        </div>
                    </div>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="ddlcountry" EventName="SelectedIndexChanged" />
                </Triggers>
            </asp:UpdatePanel>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label">
                        <%--  <%= CommonMessages.City%>--%>
                        <asp:Literal ID="Literal11" runat="server" meta:resourcekey="City" EnableViewState="false" />:<span class="starValidation">*</span></label>
                    <asp:TextBox runat="server" ID="txtcity" CssClass="input-style" MaxLength="50"
                        meta:resourcekey="txtcityResource1"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" CssClass="commonerrormsg"
                        ControlToValidate="txtcity" Display="Dynamic" ValidationGroup="chk"
                        ErrorMessage="city is required."
                        meta:resourcekey="RequiredFieldValidator4Resource1"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="col-md-6" style="display: none">
                <div class="inline-form">
                    <label class="c-label">
                        DOB:</label>
                    <asp:TextBox runat="server" ID="txtfromdate" MaxLength="10" CssClass="input-style" contentEditable="false"
                        meta:resourcekey="txtfromdateResource1" />
                    <%--<cc1:CalendarExtender ID="CalendarExtender2" PopupPosition="TopLeft" runat="server"
                        OnClientDateSelectionChanged="checkDate" TargetControlID="txtfromdate" 
                        Format="dd/MM/yyyy" Enabled="True">
                    </cc1:CalendarExtender>
                    <br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtfromdate"
                        ErrorMessage="Please enter special price start date." CssClass="commonerrormsg"
                        Display="Dynamic" Visible="False" ValidationGroup="chk" 
                        meta:resourcekey="RequiredFieldValidator13Resource1"></asp:RequiredFieldValidator>--%>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label">
                        <%--Employee  Number:--%>
                        <asp:Literal ID="Literal21" runat="server" meta:resourcekey="EmployeeNumber" EnableViewState="false" />:</label>
                    <asp:TextBox runat="server" ID="txtEmployeeNumber" MaxLength="12" CssClass="input-style" />
                    <br />
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtEmployeeNumber"
                        ErrorMessage="Please enter special price start date." CssClass="commonerrormsg"
                        Display="Dynamic" Visible="False" ValidationGroup="chk" 
                        meta:resourcekey="RequiredFieldValidator13Resource1"></asp:RequiredFieldValidator>--%>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label">
                        Address:<span class="starValidation">*</span></label>
                    <asp:TextBox runat="server" ID="txtaddress" CssClass="input-style" MaxLength="500"
                        TextMode="MultiLine" meta:resourcekey="txtaddressResource1"></asp:TextBox><br />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" CssClass="commonerrormsg"
                        ControlToValidate="txtaddress" Display="Dynamic" ValidationGroup="chk"
                        ErrorMessage="Address is required."
                        meta:resourcekey="RequiredFieldValidator3Resource1"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label">
                        Zip:</label>
                    <asp:TextBox runat="server" ID="txtzip" CssClass="input-style" MaxLength="15"
                        meta:resourcekey="txtzipResource1"></asp:TextBox>
                </div>
            </div>
            <%--<div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label" style="width: 100%">
                          <%= CommonMessages.Language%>:</label>
                    <asp:DropDownList ID="ddllanguages" runat="server" SCssClass="form-control" Style="width: 100%;
                        height: 36px; border: 1px solid #d4d4d4; display: inline;" 
                        meta:resourcekey="ddllanguagesResource1">
                        <asp:ListItem Value="English" Text="English" 
                            meta:resourcekey="ListItemResource3"></asp:ListItem>
                        <asp:ListItem Value="Denish" Text="Denish" meta:resourcekey="ListItemResource4"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>--%>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label" style="width: 100%">
                        <%-- <%= CommonMessages.Language%>--%>
                        <asp:Literal ID="Literal12" runat="server" meta:resourcekey="Language" EnableViewState="false" />:</label>
                    <asp:DropDownList ID="ddllanguages" runat="server" SCssClass="form-control" Style="width: 100%; height: 36px; border: 1px solid #d4d4d4; display: inline;"
                        meta:resourcekey="ddllanguagesResource1">
                        <asp:ListItem Value="English" Text="English" meta:resourcekey="ListItemResource3"></asp:ListItem>
                        <asp:ListItem Value="Danish" Text="Danish" meta:resourcekey="ListItemResource4"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <%--<div class="col-xs-12 profile_bottom">
                <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="btn black pull-right"
                    ValidationGroup="chk" CausesValidation="false" OnClick="btnCancel_click" />
                <asp:Button runat="server" ID="btnsubmit" Text="Submit" CssClass="btn blue pull-right"
                    ValidationGroup="chk" OnClick="btnsubmit_click" />
            </div>--%>
            <div class="modal-footer" style="border: 0px;">
                <asp:Button runat="server" ID="btnsubmit" class="btn btn-primary yellow" Text="Save"
                    OnClick="btnsubmit_click" ValidationGroup="chk"
                    Style="border-radius: 5px;" meta:resourcekey="btnsubmitResource1"></asp:Button>
                <asp:Button runat="server" ID="btnCancel" class="btn btn-primary black" Text="Cancel"
                    OnClick="btnCancel_click" meta:resourcekey="btnCancelResource1"></asp:Button>
            </div>
        </div>
        <div class="chat-widget widget-body" style="background: #fff;" id="view" runat="server">
            <div class="chat-widget-head yellow">
                <h4 style="margin: -6px 0px 0px;">Personal Information</h4>
            </div>
            <div class="col-md-6">
                <div style="width: 100%; float: left;">
                    <label class="c-label">
                        <asp:Image ID="Image1" CssClass="ProfileImage" ImageUrl="~/Organisation/images/sign-in1.jpg"
                            runat="server" Style="border: 0px;"
                            meta:resourcekey="Image1Resource1" />
                    </label>
                    <div style="margin-top: 80px; margin-left: 170px; display: none;">
                        <asp:FileUpload ID="FileUpload1" runat="server"
                            meta:resourcekey="FileUpload1Resource1"></asp:FileUpload>
                        <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="flupload1"
                            CssClass="legend" Display="Dynamic" ErrorMessage="Please upload only .gif | .jpeg | .jpg   | .png image."
                            ValidationExpression="^.*\.(JPG|jpg|jpeg|PNG|png|JPEG)$" ForeColor="Red" ValidationGroup="chk1"></asp:RegularExpressionValidator>--%>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label" style="width: 100%">
                        <%--  <%= CommonMessages.FirstName%>--%>
                        <asp:Literal ID="Literal13" runat="server" meta:resourcekey="FirstName" EnableViewState="false" />:<span class="starValidation"></span></label>
                    <asp:Label ID="lblfname" runat="server" meta:resourcekey="lblfnameResource1"></asp:Label>
                    <%--<asp:TextBox runat="server" Text="" ID="TextBox1" MaxLength="80" onkeyup="run(this)"
                        CssClass="input-style" />--%><br />
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label" style="width: 100%">
                        <%--  <%= CommonMessages.LastName%>--%>
                        <asp:Literal ID="Literal14" runat="server" meta:resourcekey="LastName" EnableViewState="false" />:<span class="starValidation"></span></label>
                    <asp:Label ID="lbllname" runat="server" class="c-label"
                        meta:resourcekey="lbllnameResource1"></asp:Label>
                    <%--<asp:TextBox runat="server" Text="" ID="TextBox2" MaxLength="80" onkeyup="run(this)"
                        CssClass="input-style" />--%><br />
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label" style="width: 100%">
                        Email:<span class="starValidation"></span></label>
                    <asp:Label ID="lblemail" runat="server" meta:resourcekey="lblemailResource1"></asp:Label>
                    <%-- <asp:TextBox runat="server" Text="" ID="TextBox3" MaxLength="80" onkeyup="run(this)"
                        CssClass="input-style" />--%><br />
                </div>
            </div>
            <div class="col-md-6" style="display: none;">
                <div class="inline-form">
                    <label class="c-label" style="width: 100%">
                        <%--   <%= CommonMessages.Gender%>--%>
                        <asp:Literal ID="Literal15" runat="server" meta:resourcekey="Gender" EnableViewState="false" />:<span class="starValidation"></span></label>
                    <asp:Label ID="lblgender" runat="server" meta:resourcekey="lblgenderResource1"></asp:Label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label" style="width: 100%">
                        <%-- <%= CommonMessages.Phone%>--%>
                        <asp:Literal ID="Literal16" runat="server" meta:resourcekey="Phone" EnableViewState="false" />:<span class="starValidation"></span></label>
                    <asp:Label ID="lblphone" runat="server" meta:resourcekey="lblphoneResource1"></asp:Label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label" style="width: 100%">
                        <%-- <%= CommonMessages.Country%>--%>
                        <asp:Literal ID="Literal17" runat="server" meta:resourcekey="Country" EnableViewState="false" />:<span class="starValidation"></span></label>
                    <asp:Label ID="lblcountry" runat="server"
                        meta:resourcekey="lblcountryResource1"></asp:Label>
                </div>
            </div>
            <div class="form-group" id="Div1" runat="server">
                <div class="col-md-6">
                    <div class="inline-form">
                        <label class="c-label" style="width: 100%">
                            <%--   <%= CommonMessages.State%>--%>
                            <asp:Literal ID="Literal18" runat="server" meta:resourcekey="State" EnableViewState="false" />:<span class="starValidation"></span></label>
                        <asp:Label ID="lblstate" runat="server" meta:resourcekey="lblstateResource1"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label" style="width: 100%">
                        <%--  <%= CommonMessages.City%> --%>
                        <asp:Literal ID="Literal19" runat="server" meta:resourcekey="City" EnableViewState="false" />:<span class="starValidation"></span></label>
                    <asp:Label ID="lblcity" runat="server" meta:resourcekey="lblcityResource1"></asp:Label>
                </div>
            </div>
            <div class="col-md-6" style="display: none;">
                <div class="inline-form">
                    <label class="c-label" style="width: 100%">
                        DOB:</label>
                    <asp:Label ID="lbldbo" runat="server" meta:resourcekey="lbldboResource1"></asp:Label>
                </div>
            </div>
            <div class="col-md-6" style="display: none;">
                <div class="inline-form">
                    <label class="c-label" style="width: 100%">
                        <asp:Literal ID="Literal22" runat="server" meta:resourcekey="EmployeeNumber" EnableViewState="false" />:</label>
                    <asp:Label ID="lblempnumber" runat="server" meta:resourcekey="lbldboResource1"></asp:Label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label" style="width: 100%">
                        Address:<span class="starValidation"></span></label>
                    <asp:Label ID="lbladdress" runat="server"
                        meta:resourcekey="lbladdressResource1"></asp:Label>
                </div>
            </div>
            <div class="col-md-6" style="display: none;">
                <div class="inline-form">
                    <label class="c-label" style="width: 100%">
                        Zip:</label>
                    <asp:Label ID="lblzip" runat="server" meta:resourcekey="lblzipResource1"></asp:Label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label" style="width: 100%">
                        <%--<%= CommonMessages.Language%>--%>
                        <asp:Literal ID="Literal20" runat="server" meta:resourcekey="Language" EnableViewState="false" />:</label>
                    <asp:Label ID="lblLanguage" runat="server"
                        meta:resourcekey="lblLanguageResource1"></asp:Label>
                </div>
            </div>
            <div class="col-xs-12 profile_bottom">
                <asp:Button runat="server" ID="Button2" Text="Back" CssClass="btn blue pull-right"
                    ValidationGroup="chk" OnClick="btnback_click"
                    meta:resourcekey="Button2Resource1" />
            </div>
        </div>
    </div>
   
    <%--<asp:Label runat="server" ID="lblDataDisplayTitle" Text="Profile"></asp:Label>--%>
    <script type="text/javascript">
        $(document).ready(function () {
            //$("#ContentPlaceHolder1_flupload1");
        });

        function checkDate(sender, args) {
            // debugger;
            //            if (sender._selectedDate = new Date()) {
            //                alert("You cannot select a current Date!");
            //                var date = new Date();
            //                date.setDate(date.getDate() - 1);
            //                sender._selectedDate = date;
            //                // set the date back to the current date
            //                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            //            }
            //            else 
            if (sender._selectedDate > new Date()) {
                alert("You cannot select a future Date!");
                var date = new Date();
                date.setDate(date.getDate() - 2);
                sender._selectedDate = date;

                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }

        }
        function readURL(input) {

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function (e) {
                    $('#ContentPlaceHolder1_img_profile').attr('src', e.target.result);
                    $("#ContentPlaceHolder1_fluploadText").val(input.files[0].name);
                };

               
                reader.readAsDataURL(input.files[0]);
            }
        }
        function removeImg() {
            $('#ContentPlaceHolder1_img_profile').attr("src", '~/Organisation/images/ofile_img.png');
            $("#hdnIsRemoveImage").val('1');
        }
    </script>

</asp:Content>
