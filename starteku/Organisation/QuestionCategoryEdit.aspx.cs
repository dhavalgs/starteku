﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using System.Threading;
using System.Globalization;
using startetku.Business.Logic;

public partial class Organisation_QuestionEdit : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "my", " SetExpandCollapse();", true);

        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {

            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                GetAllQuestionCategoryByID();
            }
        }
    }
    #region Culture
    protected override void InitializeCulture()
    {
        // string language = "French";
        string language = Convert.ToString(Session["Culture"]);

        string languageId = "";

        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!String.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }

        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion

    protected void GetAllQuestionCategoryByID()
    {
        //ddlQuesCat
        QuestionBM obj = new QuestionBM();
        obj.queIsActive = true;
        obj.queIsDeleted = false;
        obj.quesCatID = Convert.ToInt32(Request.QueryString["id"]);

        obj.GetAllQuestionCategoryList();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["qcatID"])))
                txtqcatID.Value = Convert.ToString(ds.Tables[0].Rows[0]["qcatID"]);
            
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["qcatName"])))
                txtqcatName.Text = Convert.ToString(ds.Tables[0].Rows[0]["qcatName"]);


            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["qcatNameDN"])))
                txtqcatNameDN.Text = Convert.ToString(ds.Tables[0].Rows[0]["qcatNameDN"]);


            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["qcatDescription"])))
                txtDESCRIPTION.Text = Convert.ToString(ds.Tables[0].Rows[0]["qcatDescription"]);

        }


    }


    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        string msg = "";
        if (!String.IsNullOrEmpty(Request.QueryString["id"]))
        {
           msg= updateQuestionCategory();
        }
        if (!String.IsNullOrEmpty(msg))
        {
            if (msg == "success")
            {
                lblMsg.Text = CommonModule.msgRecordInsertedSuccessfully;
                lblMsg.ForeColor = System.Drawing.Color.White;

                Response.Redirect("QuestionCategory.aspx");
            }
            else
            {
                lblMsg.Text = CommonModule.msgAlreadyExists;
            }
        }
        else
        {
            lblMsg.Text = CommonModule.msgSomeProblemOccure;
        }
        
    }
    protected void btnCancel_click(object sender, EventArgs e)
    {
        Response.Redirect("QuestionCategory.aspx");
    }
    #endregion


    protected string updateQuestionCategory()
    {
        string returnMsg = "";
        try
        {
            // check Duplicate
            var companyId = Convert.ToInt32(Session["OrgCompanyId"]);
            var userId = Convert.ToInt32(Session["OrgUserId"]);
            var logginUserType = Convert.ToInt32(Session["OrguserType"]);
            QuestionBM obj = new QuestionBM();

            obj.quesCatID =Convert.ToInt32( txtqcatID.Value);
            obj.quesCategoryName = txtqcatName.Text;
            obj.quesCategoryNameDN = txtqcatNameDN.Text;
            obj.quesDescription = txtDESCRIPTION.Text;

            obj.CreatedByID = userId;
            obj.queCompanyId = companyId;

            obj.queIsActive = true;
            obj.queIsDeleted = false;
            obj.queCreatedDate = DateTime.Now;
            obj.InsertQuestionCategoryData();
            DataSet ds = obj.ds;

            returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);

        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex);
        }
        return returnMsg;
    }
    
}