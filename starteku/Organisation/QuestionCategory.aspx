﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Organisation/OrganisationMaster.master"
    CodeFile="QuestionCategory.aspx.cs"
    Inherits="Organisation_QuestionCategory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">

    <style>
        .tabs-menu li {
            width: 33.33%;
        }

        .tab {
            padding-top: 0px !important;
        }

        .tab-content {
            padding: 0px !important;
        }



        .dropdown-menu > li > a {
            display: block;
            padding: 8px 65px !important;
            padding-bottom: 8px;
            clear: both;
            font-weight: normal;
            line-height: 1.428571429;
            color: #333;
            white-space: nowrap;
        }

        .radio input[type="radio"], .radio-inline input[type="radio"], .checkbox input[type="checkbox"], .checkbox-inline input[type="checkbox"] {
            float: left;
            margin-left: -80px !important;
        }

        .radiobuttonlist {
            font: 12px Verdana, sans-serif;
            color: #000; /* non selected color */
        }

        .inline-rb input[type="radio"] {
            width: auto;
            margin-right: 10px;
            margin-left: 20px;
        }

        .inline-rb label {
            display: inline;
        }
    </style>
    <%--joyride---------------------------------------------%>
    <link href="../plugins/joyride/joyride-2.1.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/multiselect.css" rel="stylesheet" />
    <link href="../Styles/UpdateCustom.css" rel="stylesheet" type="text/css" />
    <script src="../plugins/joyride/jquery.cookie.js" type="text/javascript"></script>
    <script src="../plugins/joyride/jquery.joyride-2.1.js" type="text/javascript"></script>
    <script src="../plugins/joyride/modernizr.mq.js" type="text/javascript"></script>

    <script src="../plugins/highchart/highcharts.js" type="text/javascript"></script>


    <script src="../plugins/highchart/highcharts-more.js" type="text/javascript"></script>
    <script src="../Scripts/plugins/exporting.js" type="text/javascript"></script>
    <script src="../Scripts/jquery321.js"></script>

    <script src="../assets/js/multiselect.js" type="text/javascript"></script>
    <script type="text/javascript" src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {

            setTimeout(function () {
                SetExpandCollapse();
                //ExampanCollapsMenu();
                //ExampanCollapssubMenu();
                // tabMenuClick();
                $(".OrgDivisionList").addClass("active_page");
            }, 500);
        });
    </script>
    <div class="col-md-6">
        <div class="heading-sec">
            <asp:Label runat="server" ID="Label12" CssClass="lblModel" meta:resourcekey="Quecatres" Font-Bold="True" ForeColor="white" Font-Size="22px"></asp:Label>
                   
        </div>
    </div>
    <div class="col-md-12" style="margin-top: 20px;">
        <asp:HiddenField ID="hdnSelectQues" runat="server" Value="Select Question Category" meta:resourcekey="SelectQuesCatResource" />
        <asp:HiddenField ID="hdnSelectQuesTemplate" runat="server" Value="Select Template" meta:resourcekey="SelectQuesTemplateResource" />
        <div id="Div1">
            <div class="col-md-12">
                <div class="add-btn1" style="float: right;">
                   

                    <a href="#popupAddNewQuesCat" data-toggle="modal" title="" style="display: none">
                        <asp:Label runat="server" ID="lblQuestionCat" CssClass="lblModel" meta:resourcekey="AddQuestionCat" Style="font-size: 19px; margin-top: 4px;"></asp:Label>
                    </a>
                    <asp:Button runat="server" ID="btnAddQuesCat" Text="Add Question Category" CssClass="btn btn-default yellow " meta:resourcekey="AddQuestionCat"
                        type="button" Style="border-radius: 5px; height: 38px; color: white;" OnClick="OpenCreateQuestionCat" />
                </div>

                <div style="clear: both;"></div>
            </div>
        </div>
    </div>

       <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" meta:resourcekey="lblMsgResource1"></asp:Label>
    <%--Category--%>
    <div>
        <div aria-hidden="true" role="dialog" tabindex="-1" class="modal fade col-md-12" id="popupAddNewQuesCat" style="display: none;">
            <div class="modal-dialog" style="width: 55%">
                <div class="modal-content">
                    <div class="modal-header blue yellow-radius">
                        <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                            ×
                        </button>
                        <h4 class="modal-title">
                            <asp:Label runat="server" ID="Label3" CssClass="" Style="color: white; font-size: 19px; margin-top: 4px;">
                                <asp:Literal ID="Literal5" runat="server" Text="Add Question Category" meta:resourcekey="AddQuestionCat"
                                    EnableViewState="true" />
                            </asp:Label></h4></div><div class="modal-body">
                        <div class="form-group">
                            <div id="Div3" runat="server" style="clear: both" visible="false">
                                <div class="col-md-3" style="padding-top: 10px;">
                                    <asp:Label runat="server" ID="Label6" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                        <asp:Literal ID="Literal6" runat="server" meta:resourcekey="Error" EnableViewState="false" />
                                    </asp:Label></div><div class="col-md-9">
                                    <asp:Label runat="server" ID="Label7" ForeColor="red" />
                                </div>
                            </div>
                        </div>
                        <br />

                        <div class="form-group">
                            <div class="col-md-3" style="padding-top: 10px;">
                                <asp:Label runat="server" ID="Label9" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                    <asp:Literal ID="Literal11" runat="server" Text="Question Category Name" meta:resourcekey="QuestionCat_Name" EnableViewState="false" />
                                </asp:Label></div><div class="col-md-9" style="margin-top: 10px;">
                                <asp:TextBox runat="server" ID="txtqcatName" placeholder="Enter Question Category" MaxLength="50" meta:resourcekey="addquecatres" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="txtqcatName"
                                    ErrorMessage="Please enter Question Category." CssClass="commonerrormsg" Display="Dynamic"
                                    ValidationGroup="chkdoc1"></asp:RequiredFieldValidator></div></div><div class="form-group">
                            <div class="col-md-3" style="padding-top: 10px;">
                                
                                <asp:Label runat="server" ID="Label8" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                    <asp:Literal ID="Literal10" runat="server" Text="Question Category Name (Danish)" meta:resourcekey="QuestionCat_Name1" EnableViewState="false" />
                                </asp:Label>
                            </div><div class="col-md-9" style="margin-top: 10px;">
                                <asp:TextBox runat="server" ID="txtqcatNameDN" placeholder="Enter Question Category" MaxLength="50" meta:resourcekey="addquecatres1"/>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtqcatNameDN"
                                    ErrorMessage="Please enter Question Category." CssClass="commonerrormsg" Display="Dynamic"
                                    ValidationGroup="chkdoc1"></asp:RequiredFieldValidator></div></div><div class="form-group">
                            <div class="col-md-3" style="padding-top: 10px;">
                                
                                <asp:Label runat="server" ID="Label11" CssClass="" Style="color: black; font-weight: bold; margin-top: 4px;">
                                    <asp:Literal ID="Literal13" runat="server" Text="Description" EnableViewState="false" meta:resourcekey="DescriptionRes"/>
                                </asp:Label>
                            </div><div class="col-md-9">
                                <asp:TextBox runat="server" ID="txtqcatDescription" placeholder="Enter Description for the category" TextMode="MultiLine" MaxLength="200" meta:resourcekey="Descripres" />

                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">

                        <asp:Button runat="server" ID="btnCreateCat"
                            Text="Create" CssClass="btn btn-primary yellow"
                            type="button" OnClick="CreateQuestionCategory" OnClientClick="CheckValidations('chkdoc1');closeModelCancel('popupAddNewQuesCat');"
                            Style="border-radius: 5px; width: 100px; height: 38px; margin-left: 359px;" meta:resourcekey="btncreate" />
                        <%--  <asp:Button runat="server" 
                           CssClass="btn btn-primary yellow"
                             OnClick="btnCreateCat_click" Text="Create Category"
                            Style="border-radius: 5px; width: 150px; height: 38px; margin-left: 359px;" />--%>
                        <asp:Button runat="server" ID="Button2"
                            Text="Close" CssClass="btn btn-default black"
                            type="button" ValidationGroup="chkdoc1" OnClientClick="closeModelCancel('popupAddNewQuesCat');"
                            Style="border-radius: 5px; width: 100px; height: 38px;" meta:resourcekey="btnclose"/>


                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12" style="margin-top: 20px;" id="htmlData">
        <div id="graph-wrapper">
            <div class="col-md-12">

                <div class="chart-tab">
                    <div id="tabs-container">
                        <div class="loaderDiv" style="display: none">
                            <div class="progress small-progress">
                                <div style="width: 40%" aria-valuemax="100" aria-valuemin="0" aria-valuenow="40"
                                    role="progressbar" class="progress-bar blue">
                                </div>
                            </div>

                            <div class="home_grap">
                                <div id="ContainerDevelopment" style="min-width: 310px; height: 400px; margin: 0 auto">
                                    <div id="waitDevelopment" style="margin: 190px 602px">
                                        <img src="../images/wait.gif" /> </div></div></div></div><div class="tab">

                            <div id="tab-1" class="tab-content">

                                <div class="chart-tab manager_table">
                                    <div id="tabs-container manager_table">
                                        <asp:GridView ID="gvGrid_QuestionCat" runat="server" AutoGenerateColumns="False" CellPadding="0"
                                            Width="100%" GridLines="None" DataKeyNames="qcatID" CssClass="table1 table-striped table-bordered table-hover table-checkable datatable"
                                            EmptyDataText='<%# CommonModule.msgGridRecordNotfound %>'
                                            BackColor="White"
                                            meta:resourcekey="GridRecordNotfound"
                                            OnRowDataBound="gvGrid_QuestionCat_RowDataBound"
                                           OnRowCommand="gvGrid_QuestionCat_RowCommand">
                                            <%--  OnRowDataBound="gvGrid_Question_RowDataBound"
                                            OnRowCreated="gvGrid_RowCreated"
                                            OnPreRender="gvGrid_PreRender">--%>


                                            <HeaderStyle CssClass="aa" />
                                            <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                            <AlternatingRowStyle BackColor="White" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="SrNo" meta:resourcekey="SrNo">
                                                    <ItemTemplate>
                                                        <%# Container.DataItemIndex + 1 %>
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                        Width="3%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />


                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="Question Category" meta:resourcekey="TemplateFieldResource2">
                                                    <ItemTemplate>
                                                          <%if (Convert.ToString(Session["Culture"]) == "Danish")
                                                          {%>
                                                         <asp:Label ID="lblName1" runat="server" Text='<%# Eval("qcatNameDN") %>'></asp:Label>
                                                         <%} %>
                                                        <%else
                                                          { %>
                                                        <asp:Label ID="lblName" runat="server" Text='<%# Eval("qcatName") %>'></asp:Label>
                                                          <%} %>
                                                        <asp:HiddenField ID="divCompanyId" runat="server" Value='<%# Eval("qcatCompanyID") %>' />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Font-Size="14px" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                        Width="50%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                </asp:TemplateField>

                                                <asp:TemplateField HeaderText="" ItemStyle-Width="50px" meta:resourcekey="TemplateFieldResource3">
                                                    <ItemTemplate>
                                                        <div class="vat" style="width: 85px;" id="divEdit" runat="server" >
                                                            
                                                                <i class="fa fa-pencil"></i>
                                                            <a href="<%# String.Format("QuestionCategoryEdit.aspx?id={0}", Eval("qcatID")) %>"
                                                                    title="Edit">
                                                                <asp:Label runat="server" ID="lbledit" meta:resourcekey="btnedit"></asp:Label>
                                                            </a>

                                                        </div>
                                                        <div class="total" style="width: 70px;" id="divDeletet" runat="server">
                                                            
                                                                <i class="fa fa-trash-o"></i>
                                                                <asp:LinkButton ID="lnkBtnName" runat="server" CommandName="archive" CommandArgument='<%# Eval("qcatID") %>'
                                                                    ToolTip="Delete " OnClientClick="return confirm('Are you sure you want to Delete  this record?');"
                                                                    meta:resourcekey="lnkBtnNameResource1">Delete </asp:LinkButton>

                                                        </div></ItemTemplate><ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" Font-Size="14px" />
                                                    <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                        Width="10%" ForeColor="White" Font-Size="18px" BorderWidth="0px" Height="55px" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <FooterStyle BackColor="#cccccc" Font-Bold="True" ForeColor="Black" HorizontalAlign="Left" />
                                        </asp:GridView>
                                    </div>
                                </div>
                            </div>

                            <div id="tab-2" class="tab-content">
                            </div>

                            <div id="tab-3" class="tab-content">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function closeModel(a) {

            if (CheckValidations('chkdoc')) { $('#' + a).modal('hide'); }

        }


        function closeModelCancel(a) { $('#' + a).modal('hide'); }
    </script>
    <script type="text/javascript">
        $(document).ready(function () {

            $('#ContentPlaceHolder1_ddtTemplate').multiselect({

                //enableFiltering: true,
                includeSelectAllOption: true,
                //buttonWidth: '400px',
                selectAllText: 'All Template',
                //  //nonSelectedText: 'None Location selected',
                allSelectedText: 'All Template',
                //  nSelectedText: 'selected',
                //  numberDisplayed: 2,


                buttonText: function (options) {

                    if (options.length === 0) {

                        return 'No option selected';

                    }
                    var labels = [];
                    var num = [];

                    options.each(function () {
                        if ($(this).attr('value') !== undefined) {
                            labels.push($(this).text());
                            num.push($(this).val());

                        }
                    });
                    $('#ContentPlaceHolder1_ddtTemplateVal').val(num.join(','));
                    return labels.join(',');
                },
                //onChange: function (event) {

                //    GetData("");


                //},


            });

        });
    </script>

</asp:Content>

