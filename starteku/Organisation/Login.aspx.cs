﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using System.Threading;
using System.Globalization;
using starteku_BusinessLogic;
using starteku_BusinessLogic.Model;

public partial class Organisation_Login : System.Web.UI.Page
{
    #region Page Event
    startetkuEntities1 db = new startetkuEntities1();
    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (!IsPostBack)
        {
           
            //cookieExpires_type();
            checkRememberLogin();
            // Setlanguage();
        }
       
        LogoMaster logo = db.LogoMasters.FirstOrDefault();
       
        if (logo != null)
        {

            imgLoginLogo.ImageUrl = "../Log/upload/Userimage/" + logo.LoginLogo;
            ViewState["imgLoginLogo"] = logo.LoginLogo;
        }
       
    }
    #endregion

    #region method
    protected void updateUserMasterLastLoginDatetime()
    {
        UserBM obj = new UserBM();
            obj.userId = Convert.ToInt32(Session["OrgUserId"]);
        obj.updateUserMasterLastLoginDatetime(Convert.ToInt32(obj.userId), DateTime.Now);
    }
    protected void checkRememberLogin()
    {
        if (Request.Cookies["OrgstartekuUserLogin"] != null)
        {
            if (Request.Cookies["OrgstartekuUserLogin"].Values["OrgUserName"] != null)
            {
                txtUsername.Text = Convert.ToString(Request.Cookies["OrgstartekuUserLogin"].Values["OrgUserName"]);
                // txtPassword.Text = Convert.ToString(Request.Cookies["OrgstartekuUserLogin"].Values["OrgPassword"]);
                txtPassword.Attributes.Add("Value", Convert.ToString(Request.Cookies["OrgstartekuUserLogin"].Values["OrgPassword"]));
            }
        }

        if (Request.Cookies["setCulturee"] != null)
        {
            if (Request.Cookies["setCulturee"].Values["languagename"] != null)
            {
                ResourceLanguageBM obj = new ResourceLanguageBM();
                if (!string.IsNullOrEmpty(Request.Cookies["setCulturee"].Values["languagename"]))
                {
                    DataSet resds = obj.GetResourceLanguage(Request.Cookies["setCulturee"].Values["languagename"]);
                    if (Request.Cookies["setCulturee"].Values["languagename"].EndsWith(resds.Tables[0].Rows[0]["resLanguage"].ToString()))
                    {
                        Session["Culture"] = resds.Tables[0].Rows[0]["resCulture"].ToString();
                        SetCulture(resds.Tables[0].Rows[0]["resCulture"].ToString());
                    }
                }


                //ResourceLanguageBM objlang = new ResourceLanguageBM();
                //DataSet resds = objlang.GetAllResourceLanguage();
                //if (!string.IsNullOrEmpty(Request.Cookies["setCulturee"].Values["languagename"]))
                //{
                //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
                //    {
                //        if (Request.Cookies["setCulturee"].Values["languagename"].EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
                //        {
                //            Session["Culture"] = resds.Tables[0].Rows[i]["resCulture"].ToString();
                //            SetCulture(resds.Tables[0].Rows[i]["resCulture"].ToString());
                //        }
                //    }
                //    //if (language.EndsWith("Danish")) languageId = "da-DK";
                //    //else languageId = "en-GB";
                //   // SetCulture(languageId);
                //}
                //  Session["Culture"] = Convert.ToString(Request.Cookies["setCulturee"].Values["languagename"]);
            }
        }
    }

    protected void setRememberMe()
    {
        if (cbRememberme.Checked)
        {
            HttpCookie cookie = new HttpCookie("OrgstartekuUserLogin");
            cookie.Values.Add("OrgUserName", txtUsername.Text);
            cookie.Expires = DateTime.Now.AddDays(30);
            Response.Cookies.Add(cookie);
        }
    }
    protected void setlanguage(String name)
    {
        HttpCookie cookie = new HttpCookie("setCulturee");
        cookie.Values.Add("languagename", name);
        //cookie.Values.Add("Password", txtPassword.Text);
        cookie.Expires = DateTime.Now.AddDays(30);
        Response.Cookies.Add(cookie);
    }

    //public void cookie_type()
    //{
    //    HttpCookie cookie = new HttpCookie("littleJackLoggedType");
    //    cookie.Values.Add("LoggedType", "admin");
    //    cookie.Expires = DateTime.Now.AddDays(30);
    //    Response.Cookies.Add(cookie);
    //}

    protected void cookieExpires_type()
    {
        HttpCookie cookie = new HttpCookie("OrgstartekuLoggedType");
        cookie.Expires = DateTime.Now.AddDays(-1);
        Response.Cookies.Add(cookie);
    }
    protected void InsertUpdateLog()
    {
        //Common.WriteLog("InsertUpdateLog");

        //Lets log : current user goin to read doc. 

        var userId = Convert.ToInt32(Session["OrgUserId"]);
        try
        {
            var allowPoint = true;

            if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["userDateTime"])))
            {
                var lastlogintime = Convert.ToDateTime(Session["userDateTime"]);
               // Common.WriteLog("lastlogintime" + lastlogintime);
                var timespan = DateTime.Now - lastlogintime;
               // Common.WriteLog("timespan" + timespan);


                if (timespan.TotalHours > 24)
                {
                 //   Common.WriteLog("allowPoint");
                    allowPoint = true;
                }
                else
                {
                  //  Common.WriteLog("notallowPoint");
                    allowPoint = false;
                }
            }
            // insert point
            var point = LogMasterLogic.InsertUpdateLogParam(
                userId,
                Common.PLP(), Common.PointForSignIn(),
                string.Format("UserID : {0} sign in", Convert.ToString(Session["OrgUserName"])), allowPoint, 0, 0);
           // Common.WriteLog("point" + point);
            if (allowPoint)
                PointBM.InsertUpdatePoint(userId, point, 0);
            //
        }
        catch (DataException ex)
        {
            Common.WriteLog("error in InsertUpdateLog  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            throw ex;
        }

        try
        {
            //return;
            //LogMaster obj = new LogMaster();
            //obj.LogIsActive = true;
            //obj.LogIsDeleted = false;
            //obj.LogCreateDate = DateTime.Now;
            //obj.LogIp = Common.GetVisitorIPAddress(true);
            //obj.LogUserId = Convert.ToInt32(Session["OrgUserId"]);
            //obj.LogCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            //obj.LogUserType = Convert.ToInt32(Session["OrguserType"]);
            //obj.LogDescription = obj.UserLogin();   // User log description..
            //obj.LogOutTime = new DateTime();
            //obj.LogCreateDate = DateTime.Now;
            //obj.LogBrowser = Common.GetWebBrowserName();
            //obj.LogPointInfo = string.Empty;
            //Common.WriteLog("obj.LogBrowser" + obj.LogBrowser);
            //// 
            //Common.WriteLog("obj.LogUserType" + obj.LogUserType);
            //if (obj.LogUserType == 3)
            //{
            //    Common.WriteLog("userDateTime" + Convert.ToString(Session["userDateTime"]));
            //    if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["userDateTime"])))
            //    {
            //        var lastlogintime = Convert.ToDateTime(Session["userDateTime"]);
            //        Common.WriteLog("lastlogintime" + lastlogintime);
            //        var timespan = DateTime.Now - lastlogintime;
            //        Common.WriteLog("timespan" + timespan);
            //        if (timespan.TotalHours > 24)
            //        {
            //            Common.WriteLog("SignIn");
            //            obj.LogPLP = LogMaster.GetPointByPointName("SignIn");
            //            Common.WriteLog("obj.LogPLP" + obj.LogPLP);
            //        }
            //        else
            //        {
            //            obj.LogPLP = 0;
            //        }
            //    }
            //    else
            //    {
            //        obj.LogPLP = LogMaster.GetPointByPointName("SignIn");
            //        Common.WriteLog("obj.LogPLP_else" + obj.LogPLP);
            //    }
            //}


            //obj.InsertUpdateLog();
        }
        catch (DataException ex)
        {
            Common.WriteLog("error in InsertUpdateLog  " + " error message  " + ex.Message + "  error stack trace  " + ex.StackTrace + " " + ex.Source + "  inner exception  " + Convert.ToString(ex.InnerException));
            throw ex;
        }
    }
    #endregion

    #region Button Event
    protected void btnLogin_Click(object sender, EventArgs e)
    {
       // Common.WriteLog("btnLogin_Click");
        UserBM obj = new UserBM();
        obj.userEmail = txtUsername.Text.Trim();
        obj.userPassword = CommonModule.encrypt(Convert.ToString(txtPassword.Text));
        obj.userType = 1;
        obj.userMasterLoginOrganisation();
        DataSet dsObj = obj.ds;

        if (dsObj != null)
        {
            if (dsObj.Tables[0].Rows.Count > 0)
            {

                if (cbRememberme.Checked)
                {
                    setRememberMe();
                }
               
               
                SetLangId(Convert.ToString(dsObj.Tables[0].Rows[0]["languageCulture"]));


                Session["languageCulture"] = Convert.ToString(dsObj.Tables[0].Rows[0]["languageCulture"]);
                Session["OrgUserName"] = Convert.ToString(dsObj.Tables[0].Rows[0]["userFirstName"]);
                Session["FullUserName"] = Convert.ToString(dsObj.Tables[0].Rows[0]["userFirstName"]) + " " + Convert.ToString(dsObj.Tables[0].Rows[0]["userLastName"]);
                Session["OrgCreatedBy"] = Convert.ToString(dsObj.Tables[0].Rows[0]["userCreateBy"]);
                Session["OrgUserId"] = Convert.ToString(dsObj.Tables[0].Rows[0]["userId"]);
                Session["OrguserType"] = Convert.ToString(dsObj.Tables[0].Rows[0]["userType"]);

                Session["OrgUserImage"] = Convert.ToString(dsObj.Tables[0].Rows[0]["userImage"]);
                Session["OrguserEmail"] = Convert.ToString(dsObj.Tables[0].Rows[0]["userEmail"]);
                Session["userDateTime"] = Convert.ToString(dsObj.Tables[0].Rows[0]["userLastLoginDateTime"]);
                Session["Aemcompid"] = Convert.ToString(dsObj.Tables[0].Rows[0]["Aemcompid"]);
                Session["Cemcompid"] = Convert.ToString(dsObj.Tables[0].Rows[0]["comdirCompID"]);
                Session["Compcompid"] = Convert.ToString(dsObj.Tables[0].Rows[0]["compCompID"]);
                Session["Pointcompid"] = Convert.ToString(dsObj.Tables[0].Rows[0]["pointCompID"]);
                Session["Quescompid"] = Convert.ToString(dsObj.Tables[0].Rows[0]["quesCompID"]);
                updateUserMasterLastLoginDatetime();
                
                if (Convert.ToInt32(dsObj.Tables[0].Rows[0]["userType"]) == 1)
                {
                    // Session.Timeout = 80;
                    Session["OrgCompanyId"] = Convert.ToString(dsObj.Tables[0].Rows[0]["userId"]);
                    Session["Culture"] = Convert.ToString(dsObj.Tables[0].Rows[0]["languageName"]);
                  
                    Session["OrgCreatedBy"] = "0";
                    


                    Response.Redirect("Organisation-dashboard.aspx");

                }
                if (Convert.ToInt32(dsObj.Tables[0].Rows[0]["userType"]) == 2)
                {
                    // Session.Timeout = 80;
                    Session["OrgCompanyId"] = Convert.ToString(dsObj.Tables[0].Rows[0]["userCompanyId"]);
                 

                    Session["userdepId"] = Convert.ToString(dsObj.Tables[0].Rows[0]["userdepId"]);
                    Session["userJobType"] = Convert.ToString(dsObj.Tables[0].Rows[0]["userJobType"]);
                    Session["Culture"] = Convert.ToString(dsObj.Tables[0].Rows[0]["languageName"]);

                    InsertUpdateLog();
                    Response.Redirect("Manager-dashboard.aspx");

                }
                if (Convert.ToInt32(dsObj.Tables[0].Rows[0]["userType"]) == 3)
                {//Denish
                    // Session.Timeout = 80;
                   // Common.WriteLog("btnLogin_Click");
                    Session["OrgCompanyId"] = Convert.ToString(dsObj.Tables[0].Rows[0]["userCompanyId"]);
                   
                    Session["userJobType"] = Convert.ToString(dsObj.Tables[0].Rows[0]["userJobType"]);
                    // Session["Culture"] = "en-GB";
                   // Common.WriteLog("login" + Convert.ToString(dsObj.Tables[0].Rows[0]["languageName"]));
                    Session["Culture"] = Convert.ToString(dsObj.Tables[0].Rows[0]["languageName"]);

                    // Common.WriteLog("InsertUpdateLog_1");
                    InsertUpdateLog();

                    // Common.WriteLog("InsertUpdateLog_done");
                    Response.Redirect("Employee_dashboard.aspx");

                }

               
               


            }
            else
            {
                lblMsg.Text = CommonModule.msgInvalidUserNameOrPassword;
            }
        }
        else
        {
            lblMsg.Text = CommonModule.msgInvalidUserNameOrPassword;
        }
    }


    public void SetLangId(string culture)
    {
        var db =new startetkuEntities1();
        try
        {
            var getId = db.ResourceLanguages.FirstOrDefault(o => o.resCulture.ToLower() == culture);
            if (getId != null)
            {
                Session["resLangID"] = getId.resLangID;

            }

        }
        catch (Exception)
        {
            
            throw;
        }
    }
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        if (Request.Cookies["setCulturee"] != null)
        {
            if (Request.Cookies["setCulturee"].Values["languagename"] != null)
            {
                Session["Culture"] = Convert.ToString(Request.Cookies["setCulturee"].Values["languagename"]);
            }
        }
        //string language = Request.Form["__EventTarget"];

        string language = Convert.ToString(Session["Culture"]);
        //string language = Convert.ToString("Denish");
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Denish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName))
                SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    //protected void Setlanguage()
    //{
    //    try
    //    {
    //        //string language = Convert.ToString(Session["Culture"]);
    //        string language = "Denish";
    //        string languageId = "";
    //        if (!string.IsNullOrEmpty(language))
    //        {
    //            if (language.EndsWith("Denish")) languageId = "da-DK";
    //            else languageId = "en-GB";
    //            SetCulture(languageId);
    //        }

    //        if (Session["Language"] != null)
    //        {
    //            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
    //        }
    //    }
    //    catch { }
    //}
    //protected void SetCulture(string languageId)
    //{
    //    try
    //    {
    //        Session["Language"] = languageId;
    //        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
    //        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    //    }
    //    catch { }
    //}
    #endregion
}