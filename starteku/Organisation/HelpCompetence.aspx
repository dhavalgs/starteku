﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="HelpCompetence.aspx.cs" Inherits="Organisation_HelpCompetence"
    Culture="auto" meta:resourcekey="PageResource1" UICulture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .HiddenText label {
            display: none;
        }
    </style>
    <style type="text/css">
        .yellow {
            border-radius: 0px;
        }

        .treeNode {
        }

            .treeNode input {
                width: auto;
                margin: 5px;
                float: left !important;
            }

        .chat-widget-head {
            float: left;
            width: 100%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div style="display: none">
        <asp:Literal ID="localText" runat="server" meta:resourcekey="Local" EnableViewState="false" />
        <asp:Literal ID="AchiveText" runat="server" meta:resourcekey="Achive" EnableViewState="false" />

    </div>
    <asp:HiddenField ID="hdnNeedHelpSubjectEnglish" runat="server" meta:resourcekey="NeedHelpSubjectEnglishResource" />
    <asp:HiddenField ID="hdnNeedHelpSubjectDenish" runat="server" meta:resourcekey="NeedHelpSubjectDenishResource" />
    <asp:HiddenField ID="hdnSendInvitation" runat="server" meta:resourcekey="SendInvitationResource"/>
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" meta:resourcekey="lblMsgResource1"></asp:Label>
    <div class="col-md-6">
        <%--<div class="heading-sec">
            <h1>Dashboard  <i>Welcome To Bette Bayer!</i></h1>
        </div>--%>
        <div class="heading-sec">
            <h1>
                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="CompetenceSkills" EnableViewState="false" />
                <i><span runat="server" id="Skills"></span></i>
            </h1>
        </div>
    </div>
    <div class="col-md-12">
        <div id="graph-wrapper">
            <h3 class="custom-heading darkblue">
                <%-- Competence Skills--%>
                <span runat="server" id="Headname"></span>
                <asp:HiddenField ID="hdnRequestHelp" meta:resourcekey="RequestHelpResource" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hdnComparison" meta:resourcekey="ComparisonResource" runat="server"></asp:HiddenField>
                  <asp:HiddenField ID="hdnRequest" meta:resourcekey="Request" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hdnRequestHelpSendSuccess" meta:resourcekey="RequestHelpSendSuccessfully" runat="server"></asp:HiddenField>
            </h3>
        </div>
        <!-- Widget Visitor -->
        <!-- ORDR REVIEWS -->
    </div>



    <asp:UpdatePanel runat="server" ID="UpdatePanedrop" UpdateMode="Conditional">
        <ContentTemplate>
            <div class="container comptable_last">
                <div class="col-md-12 comp_table yellow1" style="padding: 0!important; margin-left: 0px; margin-top: 10px; width: 100%; display: none">
                    <div class="chat-widget widget-body" style="border-radius: 5px;">
                        <div class="chat-widget-head yellow1 yellow-radius">
                            <h4></h4>
                        </div>
                    </div>
                    <div style="background: #fff;">
                        <asp:Repeater ID="rptAll" runat="server" OnItemCommand="rptAll_OnItemCommand">
                            <HeaderTemplate>
                                <table width="100%" border="1" bordercolor="#dfdfdf" class="skill_table" id="tblContacts">
                                    <thead>
                                        <th style="text-align: center"></th>
                                        <th style="text-align: center;">Total
                                        </th>
                                        <th style="text-align: center">Job Type
                                        </th>
                                        <th style="text-align: center">Division
                                        </th>
                                    </thead>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td width="40%">
                                        <asp:Label ID="lbltotal" runat="server" Text='<%# Eval("comCompetence") %>' meta:resourcekey="lbltotalResource1"></asp:Label>
                                        <asp:HiddenField ID="comId" runat="server" Value="<%# bind('skillComId') %>" />
                                    </td>
                                    <td width="10%">
                                        <asp:Button runat="server" ID="btntotal" Text='<%# Eval("total") %>' CssClass="yellow"
                                            Style="border-radius: 100%; height: 25px; width: 25px; color: white;" CommandName="total"
                                            CommandArgument='<%# Eval("skillComId") %>' meta:resourcekey="btntotalResource1" />
                                    </td>
                                    <td width="15%">
                                        <asp:Button runat="server" ID="btnJobtype" Text='<%# Eval("Jobtype") %>' CssClass="yellow"
                                            Style="border-radius: 100%; height: 25px; width: 25px; color: white;" CommandName="Jobtype"
                                            CommandArgument='<%# Eval("skillComId") %>' meta:resourcekey="btnJobtypeResource1" />
                                    </td>
                                    <td width="12%">
                                        <asp:Button runat="server" ID="btnDivision" Text='<%# Eval("Division") %>' CssClass="yellow"
                                            Style="border-radius: 100%; height: 25px; width: 25px; color: white;" CommandName="Division"
                                            CommandArgument='<%# Eval("skillComId") %>' meta:resourcekey="btnDivisionResource1" />
                                    </td>
                                </tr>
                            </ItemTemplate>
                            <FooterTemplate>
                                </table>
                            </FooterTemplate>
                        </asp:Repeater>
                    </div>
                </div>
            </div>
            <div class="col-md-12 widget-body comp_table yellow1" style="padding: 0!important; margin-left: 0px; margin-top: 10px; width: 100%;"
                id="dvsubretr" runat="server">
                <div class="chat-widget widget-body" style="border-radius: 5px;">
                    <div class="chat-widget-head yellow">
                        <h4 style="width: 98.5%;">

                            <span runat="server" id="Competencetitle" style="float: left;"></span>



                            <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Competence" EnableViewState="false" />
                            <%--  <%= CommonMessages.Competence%>--%>
                            <span runat="server" id="spAchive1" style="border: 1px solid bule; float: right; display:none;"></span>
                            <span runat="server" id="spAchive" style="border: 1px solid bule; float: right;"></span><span runat="server" id="Splocal" style="border: 1px solid bule; float: right; padding-left: 27px; padding-right: 20px;"></span>
                        </h4>
                    </div>
                </div>
                <asp:Repeater ID="Repeater1" runat="server" OnItemCommand="Repeater1_OnItemCommand"
                    OnItemDataBound="rptAll_ItemDataBound">
                    <HeaderTemplate>
                        <table width="100%" border="1" bordercolor="#dfdfdf" class="skill_table" id="tblContacts" style="background-color: #fff;">
                            <thead>
                                <th style="text-align: center">
                                    <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Employee" EnableViewState="false" />
                                </th>
                                <th style="text-align: center;">
                                    <asp:Literal ID="Literal8" runat="server" meta:resourcekey="Division" EnableViewState="false" />
                                </th>
                                <th style="text-align: center">
                                    <asp:Literal ID="Literal9" runat="server" meta:resourcekey="JobType" EnableViewState="false" />
                                </th>
                                <th style="text-align: center">
                                    <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Local" EnableViewState="false" />
                                </th>
                                <th style="text-align: center">
                                    <asp:Literal ID="Literal10" runat="server" meta:resourcekey="Achive" EnableViewState="false" />
                                </th>
                                <div id="divMessage" runat="server">
                                    <th style="text-align: center" id="tdMessage">
                                        <asp:Literal ID="Literal5" runat="server" meta:resourcekey="Message" EnableViewState="false" />
                                    </th>
                                </div>
                                <div id="row" runat="server">
                                    <th style="text-align: center">
                                        <asp:Literal ID="Literal4" runat="server" meta:resourcekey="RequestHelp" EnableViewState="false" />
                                    </th>
                                </div>
                            </thead>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:Label ID="lblFirstName" runat="server" Text='<%# Eval("userFirstName") %>' meta:resourcekey="lblFirstNameResource1"></asp:Label>
                                <asp:HiddenField ID="comId" runat="server" Value="<%# bind('skillComId') %>" />
                            </td>
                            <td>
                                <asp:Label ID="lbldivName" runat="server" Text='<%# Eval("divName") %>' meta:resourcekey="lbldivNameResource1"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblJobtype" runat="server" Text='<%# Eval("jobName") %>' meta:resourcekey="lblJobtypeResource1"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lbllocal" runat="server" Text='<%# Eval("skillLocal") %>' meta:resourcekey="lbllocalResource1"></asp:Label>
                            </td>
                            <td>
                                <asp:Label ID="lblskillAchive" runat="server" Text='<%# Eval("skillAchive") %>' meta:resourcekey="lblskillAchiveResource1"></asp:Label>
                            </td>
                            <div id="divMessageButton" runat="server">
                                <td id="tdMessageButton">
                                    <asp:Button runat="server" ID="btnDivision" Text="Message" CssClass="yellow" Style="border-radius: 5px; height: 33px; width: 87px; color: white;"
                                        CommandName="Message" CommandArgument='<%# Eval("skillUserId") %>'
                                        meta:resourcekey="Message" />
                                    <%-- <asp:Button runat="server" ID="btnDivision" Text="Message" CssClass="yellow" Style="border-radius: 5px;
                                    height: 33px; width: 87px; color: white;" 
                                    PostBackUrl='<%# String.Format("Chatroom.aspx?mes={0}", Eval("skillUserId")) %>'                                     
                                    meta:resourcekey="Message" />--%>
                                </td>
                            </div>
                            <div id="Emptyrow" runat="server">
                                <td width="12%">
                                    <asp:Button runat="server" ID="btnInvite" CssClass="yellow" Style="border-radius: 5px; height: 33px; width: 150px; color: white;"
                                        CommandName="Invite" CommandArgument='<%# Eval("skillUserId") %>'
                                        meta:resourcekey="btnInviteResource1" Text="<%# Label2.Text %>">

                                        <%--  --%>
                                    </asp:Button>
                                </td>
                            </div>
                        </tr>
                    </ItemTemplate>
                    <FooterTemplate>
                        </table>
                    </FooterTemplate>
                </asp:Repeater>
                <div style="display: none">
                    <asp:Literal ID="Label2" runat="server" meta:resourcekey="RequestHelp"></asp:Literal>
                </div>
            </div>
            <asp:LinkButton ID="lnkcalpopup" runat="server" meta:resourcekey="lnkcalpopupResource1" />
            <cc1:ModalPopupExtender ID="mpecal" runat="server" PopupControlID="pnlPopupcal" TargetControlID="lnkcalpopup"
                CancelControlID="btnClosecal" BackgroundCssClass="modalBackground" DynamicServicePath=""
                Enabled="True">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlPopupcal" runat="server" CssClass="modalPopup" Style="z-index: 100001 !important; background-color: White; width: 40%; top: 117px !important; display: none;"
                meta:resourcekey="pnlPopupcalResource1">
                <div class="modal-header blue" style="color: #ffffff;">
                    <h4 class="modal-title">To :
                        <asp:Literal ID="litname" runat="server" EnableViewState="false" />
                        <%--<asp:Literal ID="Literal5" runat="server" meta:resourcekey="Message" EnableViewState="false" />--%>
                    </h4>
                    <div style="float: right; margin-top: -26px;">
                        <asp:Button ID="btnClosecal" runat="server" Text="×" CssClass="close" meta:resourcekey="btnClosecalResource1" />
                    </div>
                </div>
                <div class="s-body">
                    <table border="0" cellpadding="3" cellspacing="0" width="100%">
                        <tr>
                            <td style="padding-left: 25px;">
                                <asp:TextBox runat="server" placeholder="Message" ID="txtcname" Style="border: 1px solid #d4d4d4; float: left; font-size: 14px; padding: 10px 13px; margin-bottom: 20px; margin-left: -9px; margin-top: 9px; width: 100%; transition: all 0.4s ease 0s;"
                                    TextMode="MultiLine" meta:resourcekey="txtcnameResource1" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtcname"
                                    ErrorMessage="Please enter Name." CssClass="commonerrormsg" Display="Dynamic"
                                    ValidationGroup="chhk" meta:resourcekey="RequiredFieldValidator1Resource1"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <input type="button" onclick="myFunction()" id="btnSend" value="Send" class="btn btn-primary yellow"
                        style="border-radius: 5px;" />
                    <input type="button" onclick="Close()" id="btnclose" value="Close" class="btn btn-default black" />
                </div>
            </asp:Panel>
            <asp:LinkButton ID="lnkcl2" runat="server" meta:resourcekey="lnkcl2Resource1" />
            <cc1:ModalPopupExtender ID="mpe2" runat="server" PopupControlID="pnlinvite" TargetControlID="lnkcl2"
                CancelControlID="btncl" BackgroundCssClass="modalBackground" DynamicServicePath=""
                Enabled="True">
            </cc1:ModalPopupExtender>
            <asp:Panel ID="pnlinvite" runat="server" CssClass="modalPopup" Style="z-index: 100001 !important; background-color: White; width: 40%; top: 117px !important; display: none;"
                meta:resourcekey="pnlinviteResource1">
                <div class="modal-header blue" style="color: #ffffff;">
                    <h4 class="modal-title">
                        <asp:Literal ID="Literal6" runat="server" meta:resourcekey="RequestHelp" EnableViewState="false" />
                    </h4>
                    <div style="float: right; margin-top: -26px;">
                        <asp:Button ID="btncl" runat="server" Text="×" CssClass="close" meta:resourcekey="btnclResource1" />
                    </div>
                </div>
                <div class="s-body">
                    <table border="0" cellpadding="3" cellspacing="0" width="100%">
                        <tr>
                            <td style="padding-left: 25px;">
                                <asp:TextBox runat="server" placeholder="Invition" ID="txtinvite" Style="border: 1px solid #d4d4d4; float: left; font-size: 14px; margin-bottom: 20px; padding: 10px 13px; height: 195px; width: 100%; transition: all 0.4s ease 0s; margin-left: -10px; margin-top: 20px;"
                                    TextMode="MultiLine" meta:resourcekey="txtinviteResource1" />
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtinvite"
                                    ErrorMessage="Please Enter Message." CssClass="commonerrormsg" Display="Dynamic"
                                    ValidationGroup="chhk" meta:resourcekey="RequiredFieldValidator2Resource1"></asp:RequiredFieldValidator>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-left: 25px; display: none">
                                <label for="date1">
                                    Date:</label>
                                <asp:TextBox runat="server" ID="txtDate" MaxLength="10" CssClass="input-style" contentEditable="false" onmouseover="checkDate();"
                                    placeholder="Enter Date" />
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <input type="button" onclick="myInvition()" id="Button2" value="Send" class="btn btn-primary yellow"
                        style="border-radius: 5px;" />
                    <input type="button" onclick="Close1()" id="Button3" value="Close" class="btn btn-default black" />
                </div>
            </asp:Panel>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--Provide Help--%>
    <asp:LinkButton ID="lnkGiveHelp" runat="server" meta:resourcekey="lnkGiveHelpResource1" />
    <cc1:ModalPopupExtender ID="mpeGiveHelp" runat="server" PopupControlID="panelGiveHelp"
        TargetControlID="lnkGiveHelp" CancelControlID="btncl" BackgroundCssClass="modalBackground"
        DynamicServicePath="" Enabled="True">
    </cc1:ModalPopupExtender>
    <asp:Panel ID="panelGiveHelp" runat="server" CssClass="modalPopup" Style="z-index: 100001 !important; background-color: White; width: 40%; top: 117px !important; display: none;"
        meta:resourcekey="panelGiveHelpResource1">
        <div class="modal-header blue" style="color: #ffffff;">
            <h4 class="modal-title">
                <asp:Literal ID="Literal7" runat="server" meta:resourcekey="RequestHelp" EnableViewState="false" /></h4>
            <div style="float: right; margin-top: -26px;">
                <asp:Button ID="Button1" runat="server" Text="×" CssClass="close" meta:resourcekey="Button1Resource1" />
            </div>
        </div>
        <div class="s-body">
            <table border="0" cellpadding="3" cellspacing="0" width="100%">
                <tr>
                    <td style="padding-left: 25px;">
                        <asp:TextBox runat="server" placeholder="Reuest Help" ID="txtInviteGiveHelp" Style="border: 1px solid #FFF; float: left; font-size: 15px; margin-bottom: 20px; padding: 10px 13px; width: 100%; transition: all 0.4s ease 0s;"
                            TextMode="MultiLine" meta:resourcekey="txtInviteGiveHelpResource1" />
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="txtInviteGiveHelp"
                            ErrorMessage="Please enter name." CssClass="commonerrormsg" Display="Dynamic"
                            ValidationGroup="chhk" meta:resourcekey="RequiredFieldValidator3Resource1"></asp:RequiredFieldValidator>
                    </td>
                </tr>
                <tr>
                    <td style="padding-left: 25px;">
                        <div style="float: left">
                            <asp:Label ID="Label1" runat="server" meta:resourcekey="ShareDocument">Share Document (optional) : </asp:Label>
                        </div>
                        <div style="float: left">
                            <asp:FileUpload ID="FileUpload1" runat="server" meta:resourcekey="FileUpload1Resource1" />
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="FileUpload1"
                                CssClass="legend" Display="Dynamic" ErrorMessage="Please upload only .pdf File."
                                ValidationExpression="^.*\.(PDF|pdf)$" ForeColor="Red" ValidationGroup="chk"
                                meta:resourcekey="plsUploadFile"></asp:RegularExpressionValidator>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="modal-footer">
            <asp:Button runat="server" OnClientClick="CheckValidations(chk)" OnClick="SubmitGiveHelpInvitation"
                ID="btnSubmitGiveHelp" Text="Save" class="btn btn-primary yellow" Style="border-radius: 5px;"
                meta:resourcekey="Save" />
            <asp:Button ID="Button4" runat="server" Text="Close" CssClass="btn btn-default black"
                meta:resourcekey="Close" />
        </div>
    </asp:Panel>
    <!-- TIME LINE -->
    <!-- Recent Post -->
    <div class="col-md-3">
    </div>
    <!-- Twitter Widget -->
    <!-- Weather Widget -->
    <div class="modal-footer" style="border-top: 0px !important;">
        <asp:Button runat="server" ID="btnBack" Text="Save Competence" CssClass="btn black pull-right"
                                    OnClick="btnBack_Click" style="margin: 4px;"  meta:resourcekey="Back" EnableViewState="false"/>

        <%--<a href="#" onclick="javascript:history.back(); return false;" class="btn black pull-right" style="border-radius: 5px; margin-left: 0px; margin-top: 5px; float: right;">
            <asp:Literal ID="Literal11" runat="server" meta:resourcekey="Back" EnableViewState="false" />  
        </a>--%>
    </div>

    <script>
        function Close() {
            document.getElementById('ContentPlaceHolder1_btnClosecal').click();

        }
        function Close1() {
            document.getElementById('ContentPlaceHolder1_btncl').click();

        }
        function myFunction() {
            var txtto = $('#<%=txtcname.ClientID %>').val();

            if (txtto != '') {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "HelpCompetence.aspx/UpdateData",
                    data: "{'txtto':'" + txtto + "'}",
                    dataType: "json",
                    success: function (data) {
                        var obj = data.d;
                        if (obj == 'true') {
                            $('#<%=txtcname.ClientID %>').val('');
                            modelPopupDilouge("Message", "Message send successfully", true);
                            // alert('Message Send SuccessFully');
                            document.getElementById('ContentPlaceHolder1_btnClosecal').click();
                        }
                    },
                    error: function (result) {
                        alert("Error");
                    }
                });
            }
            else {
                alert('Please enter message');
                return false;
            }

        }

        function myInvition() {
            
            var txtto = $('#<%=txtinvite.ClientID %>').val();
            //var date = $('#<%=txtDate.ClientID %>').val();
            var date = "";
            var compTitle = $('#<%=Competencetitle.ClientID %>').text();
            var Ach = $('#<%=spAchive1.ClientID %>').text();
            var engSub = $('#<%=hdnNeedHelpSubjectEnglish.ClientID%>').val();
            var DenishSub = $('#<%=hdnNeedHelpSubjectDenish.ClientID%>').val();
            if (txtto != '') {
                //generate('warning', '<b> Please wait : </b> We are processing to send invitation.', 'bottomCenter');
                generate('warning', $('#<%=hdnSendInvitation.ClientID %>').val(), 'bottomCenter');
                document.getElementById('ContentPlaceHolder1_btncl').click();
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "HelpCompetence.aspx/Invitation",
                    data: "{'txtto':'" + txtto + "','date':'" + date + "','compTitle':'" + compTitle + "','Ach':'" + Ach + "','engSub':'" + engSub + "','DenishSub':'" + DenishSub + "'}",
                    dataType: "json",
                    success: function (data) {
                        var obj = data.d;
                        if (obj == 'true') {
                            $('#<%=txtinvite.ClientID %>').val('');
                            //$('#<%=txtDate.ClientID %>').val('');./;',
                            
                            modelPopupDilouge($('#ContentPlaceHolder1_hdnRequest').val(), $('#ContentPlaceHolder1_hdnRequestHelpSendSuccess').val(), true);
                            
                            //alert('Send Invition SuccessFully');
                            //document.getElementById('ContentPlaceHolder1_btncl').click();
                        }
                    },
                    error: function (result) {
                        //alert("Error");
                    }
                });
            }
            else {

                generate('warning', '<b>Warning : </b> Invitation message can not be blank', 'bottomCenter');
                return false;
            }

        }

    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8/jquery.min.js" type="text/javascript"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"
        type="text/javascript"></script>
    <link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="Stylesheet" type="text/css" />
    <script type="text/javascript">
        function checkDate() {
            $("#<%=txtDate.ClientID %>").datepicker({
                changeMonth: true,
                changeYear: true,
                numberOfMonths: 1,
                dateFormat: 'dd/mm/yy',
                // yearRange: '-100y:c+nn',
                yearRange: '1950:+200',
                minDate: 0
            });
        }
        $(document).ready(function () {
            setTimeout(function () {
                $(".active").click();
                // tabMenuClick();
                $(".myCompetence").addClass("active_page");
            }, 500);
        });
        function onLoad() {
  
            setTimeout(function () {
               // ExampanCollapsMenu();

                // tabMenuClick();
                $(".myCompetence").addClass("active_page");
            }, 500);
        }

    </script>
</asp:Content>
