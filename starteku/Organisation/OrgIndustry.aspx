﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Organisation/OrganisationMaster.master"
    AutoEventWireup="true" CodeFile="OrgIndustry.aspx.cs" Inherits="Organisation_OrgIndustry" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .treeNode
        {
        }
        
        .treeNode input
        {
            width: auto;
            margin: 5px;
            float: left !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg"></asp:Label>
    <div class="col-md-6">
        <div class="heading-sec">
            <%--<h1>
                <asp:Label runat="server" ID="lblDataDisplayTitle" Text="Industry"></asp:Label>
                <i>Welcome to Flat Lab </i></h1>--%>
            <h1>
                Industry <i>
                   <span runat="server" id="Industry"></span></i></h1>
        </div>
    </div>
    <br />
    <br />
    <div class="col-md-6 range ">
        <%--<div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
            <i class="fa fa-calendar-o icon-calendar icon-large"></i>
            <span>August 5, 2014 - September 3, 2014</span>  <b class="caret"></b>
        </div>--%>
    </div>
    <div class="col-md-12">
        <div class="chat-widget widget-body" style="background: #fff;">
            <div class="chat-widget-head yellow">
                <h4>
                    Add Industry
                </h4>
            </div>
            <div class="indicatesRequireFiled">
                <i>* Indicates required field</i>
            </div>
            <div class="col-md-6" style="width: 100%;">
                <div class="inline-form">
                    <label class="c-label">
                        Industry:</label>
                    <asp:DropDownList ID="ddlIndustry" runat="server" Style="width: 100px; height: 32px;
                        display: inline;" Visible="false">
                    </asp:DropDownList>
                    <br />
                    <br />
                    <asp:TreeView ID="TreeView1" runat="server" ShowCheckBoxes="All" onclick="client_OnTreeNodeChecked(event)"
                        OnTreeNodePopulate="TreeView_TreeNodePopulate" Style="cursor: pointer" ShowLines="True"
                        NodeStyle-CssClass="treeNode" />
                </div>
            </div>
            <div class="col-md-6">
                <div class="inline-form">
                    <label class="c-label">
                        Industry Name:</label>
                    <asp:TextBox runat="server" Text="" ID="txtIndustryName" MaxLength="50" CssClass="form-control" />
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtIndustryName"
                        ErrorMessage="Please enter Industry name." CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk"></asp:RequiredFieldValidator>
                </div>
            </div>
            <div class="col-xs-12 profile_bottom">
                <asp:Button runat="server" ID="btnsubmit" Text="Save" CssClass="btn blue pull-right"
                    ValidationGroup="chk" OnClick="btnsubmit_click" />
                <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="btn black pull-right"
                    ValidationGroup="chk" CausesValidation="false" OnClick="btnCancel_click" />
            </div>
        </div>
    </div>
</asp:Content>
