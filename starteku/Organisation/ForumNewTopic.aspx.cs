﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using starteku_BusinessLogic.View;
using System.Threading;
using System.Globalization;
using startetku.Business.Logic;

public partial class Organisation_ForumNewTopic : System.Web.UI.Page
{
    #region define object
    private ForumMasterBM obj = new ForumMasterBM();
    #endregion
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            Conatct.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            ViewState["catParentId"] = "0";
            GetForumTagsByforumCompanyId();
            GetAllForumCategoryByCompanyId();

            GetAllJobTypeByCompanyId();
            GetAllDivisionByCompanyId();
            CompetenceSelectAll();


        }

    }
    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static List<JsonView> GetForumTagsByforumCompanyId()
    {
        try
        {
            var obj1 = new ForumMasterBM();
            obj1.forumCatCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
            obj1.forumCatIsActive = true;
            obj1.forumCatIsDeleted = false;
            obj1.GetForumTagsByforumCompanyId();
            DataSet ds = obj1.ds;

            List<JsonView> tag = new List<JsonView>();
            if (ds.Tables[0].Rows.Count > 0)
            {
                var myTaglist = ds.Tables[0].AsEnumerable()
                          .Select(r => new UserMasterView
                          {
                              UserId = r.Field<int>("tagId"),
                              UserCategory = r.Field<string>("tagName")
                          });


                foreach (var i in myTaglist)
                {
                    tag.Add(GetViewJsonData(i));
                }
                return tag.ToList();

            }
            return null;
        }
        catch (Exception ex)
        {

            return null;

        }
        finally
        {

        }
    }

    private static JsonView GetViewJsonData(UserMasterView userMasterView)
    {
        var view = new JsonView();
        view.id = userMasterView.UserId;
        view.label = userMasterView.UserCategory;
        view.value = userMasterView.UserCategory;
        return view;
    }

    private void GetAllForumCategoryByCompanyId()
    {
        try
        {

            CategoryBM obj = new CategoryBM();
            obj.depIsActive = true;
            obj.depIsDeleted = false;
            obj.depCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.GetAllCompetenceCategory();
            DataSet ds = obj.ds;
            ViewState["ds"] = ds;

            if (ds != null)
            {
                ddForumCat.Items.Clear();
                ddForumCat.DataSource = ds.Tables[0];
                ddForumCat.DataTextField = "catName";
                ddForumCat.DataValueField = "catId";
                ddForumCat.DataBind();
                ddForumCat.Items.Insert(0, new ListItem(CommonModule.dropDownSelectForumCategory, CommonModule.dropDownZeroValue));

            }
        }
        catch (Exception ex)
        {


        }
        finally
        {

        }
    }

    #endregion

    #region method
    protected void GetAllDivisionByCompanyId()
    {
        DivisionBM obj = new DivisionBM();
        obj.depIsActive = true;
        obj.depIsDeleted = false;
        obj.depCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);


        obj.GetAllDivisionsbyCompanyId();


        DataSet ds = obj.ds;
        ViewState["ds"] = ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (Convert.ToString(Session["Culture"]) == "Danish")
                {
                    ds.Tables[0].Columns["divName"].ColumnName = "abcd1";
                    ds.Tables[0].Columns["divNameDN"].ColumnName = "divName";

                }


                chkListDivision.DataSource = ds.Tables[0];
                chkListDivision.DataTextField = "divName";
                chkListDivision.DataValueField = "divId";
                chkListDivision.DataBind();
                chkListDivision.Items.Insert(0, hdnAll.Value);

            }

        }
    }

    /*----------------------GET Jobtype BY Id -----------------------------------------*/
    protected void GetAllJobTypeByCompanyId()
    {
        JobTypeBM obj = new JobTypeBM();
        obj.jobIsActive = true;
        obj.jobIsDeleted = false;
        obj.jobCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);


        obj.GetAllJobTypeByCompanyId();


        DataSet ds = obj.ds;
        ViewState["ds"] = ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (Convert.ToString(Session["Culture"]) == "Danish")
                {
                    ds.Tables[0].Columns["jobName"].ColumnName = "abcd";
                    ds.Tables[0].Columns["jobNameDN"].ColumnName = "jobName";

                }

                chkListJobtype.DataSource = ds.Tables[0];
                chkListJobtype.DataTextField = "jobName";
                chkListJobtype.DataValueField = "jobId";
                chkListJobtype.DataBind();
                chkListJobtype.Items.Insert(0, hdnAll.Value);

            }

        }
    }
    protected void CompetenceSelectAll()
    {


        CompetenceMasterBM obj = new CompetenceMasterBM();
        obj.comIsActive = true;
        obj.comIsDeleted = false;
        obj.comCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllCompetenceMasterAdd();


        DataSet ds = obj.ds;
        ViewState["ds"] = ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                var str = Session["Culture"];
                if (Convert.ToString(Session["Culture"]) == "Danish")
                {
                    ds.Tables[0].Columns["comCompetence"].ColumnName = "abcd";
                    ds.Tables[0].Columns["comCompetenceDN"].ColumnName = "comCompetence";


                }

                ddlCompetences.DataTextField = "comCompetence";
                ddlCompetences.DataValueField = "comId";
                ddlCompetences.DataSource = ds.Tables[0];
                ddlCompetences.DataBind();
                ddlCompetences.Items.Insert(0, new ListItem(hdnSelectCompetence.Value, "0"));

            }
            else
            {
                ddlCompetences.DataSource = null;
                ddlCompetences.DataBind();

                //ddlcompetencefilter.DataSource = null;
                //ddlcompetencefilter.DataBind();
            }
        }
        else
        {
            ddlCompetences.DataSource = null;
            ddlCompetences.DataBind();


        }


        DivisionBM obj1 = new DivisionBM();
        obj1.depIsActive = true;
        obj1.depIsDeleted = false;
        obj1.depCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj1.GetAllDocCategory();

        DataSet ds1 = obj1.ds;
        ViewState["ds1"] = ds1;

        if (ds1 != null)
        {
            if (ds1.Tables[0].Rows.Count > 0)
            {
                var str = Session["Culture"];
                if (Convert.ToString(Session["Culture"]) == "Danish")
                {
                    ds1.Tables[0].Columns["dcName"].ColumnName = "abcd";
                    ds1.Tables[0].Columns["dcNameDN"].ColumnName = "dcName";


                }

                ddlDocumentCategory.DataTextField = "dcName";
                ddlDocumentCategory.DataValueField = "dcCatId";
                ddlDocumentCategory.DataSource = ds1.Tables[0];
                ddlDocumentCategory.DataBind();
                ddlDocumentCategory.Items.Insert(0, new ListItem(hdnSelectCategoty.Value, "0"));


            }
            else
            {
                ddlDocumentCategory.DataSource = null;
                ddlDocumentCategory.DataBind();


                //ddlcompetencefilter.DataSource = null;
                //ddlcompetencefilter.DataBind();
            }
        }
        else
        {
            ddlDocumentCategory.DataSource = null;
            ddlDocumentCategory.DataBind();


        }

    }
    #endregion

    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        /*obj.doccompetence = ddlCompetences.SelectedItem.Value;*/

        try
        {
            if (ddlDocumentCategory.SelectedIndex == 0)
            {
                obj.fmCategoryId = 0;
                obj.fmCategoryName = String.Empty;
            }
            else
            {
                obj.fmCategoryId = Convert.ToInt32(ddlDocumentCategory.SelectedItem.Value);
                obj.fmCategoryName = ddlDocumentCategory.SelectedItem.Text;
            }

            obj.fmCreatedDate = DateTime.Now;
            obj.fmIsActive = true;
            obj.fmIsDeleted = false;
            obj.fmQuestionName = txtName.Text;
            obj.fmDescription = txtDescription.Text;
            obj.fmCreateByUserID = Convert.ToInt32(Session["OrgUserId"]);
            obj.fmCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.fmTagNames = tag1.Text;
            obj.fmJobTypeId = chkListJobtype.SelectedItem.Value;
            obj.fmDivId = chkListDivision.SelectedItem.Value;
            obj.fmIsCompDevDoc = chkIsCompDevDoc.SelectedItem.Value;
            obj.fmLevel = rdoLevel.SelectedItem.Text;
            if (ddlCompetences.SelectedIndex == 0)
            {
                obj.fmCompId = 0;
                obj.fmCompetence = String.Empty;
            }
            else
            {
                obj.fmCompId = Convert.ToInt32(ddlCompetences.SelectedItem.Value);
                obj.fmCompetence = ddlCompetences.SelectedItem.Text;
            }

            obj.InsertForum();

            Response.Redirect("ForumAll.aspx");

        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "btnsubmit_click");
        }

    }

    protected void btnCancel_click(object sender, EventArgs e)
    {

    }

    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Danish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
}