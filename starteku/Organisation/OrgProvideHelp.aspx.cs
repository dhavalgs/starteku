﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using System.Configuration;
using startetku.Business.Logic;
using System.Threading;
using System.Globalization;
using starteku_BusinessLogic.Model;


public partial class Organisation_OrgProvideHelp : System.Web.UI.Page
{
    string temp = "0";
    private string userid = "0";
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.ClientScript.RegisterStartupScript(Page.GetType(), "my", " SetExpandCollapse();", true);

        aa();
        if (String.IsNullOrEmpty(Convert.ToString(Session["OrgUserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            ViewState["PreviousPage"] = Request.UrlReferrer;
            Eemployee.InnerHtml = GetLocalResourceObject("Welcome.Text").ToString() + "  " + Convert.ToString(Session["OrgUserName"]) + "!";
            if (!String.IsNullOrEmpty(Request.QueryString["Comp"]))
            {
                temp = Convert.ToString(Request.QueryString["Comp"]);
                Label2.Visible = false;
                back.Visible = false;
                pnlShowDoc.Visible = false;

            }
            if (!String.IsNullOrEmpty(Request.QueryString["ToUser"]))
            {
                userid = Convert.ToString(Request.QueryString["ToUser"]);
                Label2.Visible = false;
                back.Visible = false;
                pnlShowDoc.Visible = false;

            }
            else if (!String.IsNullOrEmpty(Request.QueryString["view"]))
            {
                temp = Convert.ToString(Request.QueryString["view"]);
                ddlstastus.Visible = false;
                submit.Visible = false;
                uploaddoc.Visible = false;
                // pnlShowDoc.Visible = false;
                helpresponse.Visible = false;
            }
            if (String.IsNullOrEmpty(Request.QueryString["HelpTrackId"]))
            {


                invHelpReply.Visible = false;

            }
            else
            {
                GetHelpResponseByTrackId();
            }

            GetAllEmployeebyid();
            GetAllDocuments();


            //Update notification counter, and notification list =2015-11-16 Saurin
            var notId = Request.QueryString["notIf"];
            if (!string.IsNullOrWhiteSpace(notId))
            {
                NotificationBM obj = new NotificationBM();
                obj.notId = Convert.ToInt32(notId);
                obj.notPopUpStatus = false;
                obj.UpdateNotificationbyid();
            }

        }
    }

    protected void Page_PreInit(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Convert.ToString(Session["OrguserType"])))
        {
            if (Convert.ToString(Session["OrguserType"]) == "3")
            {
                this.Page.MasterPageFile = "~/Organisation/Employee.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "1"))
            {

                this.Page.MasterPageFile = "~/Organisation/OrganisationMaster.master";
            }
            else if ((Convert.ToString(Session["OrguserType"]) == "2"))
            {
                this.Page.MasterPageFile = "~/Organisation/ManagerMaster.master";
            }
        }
    }

    #region method
    public string DocName1 = "";
    public string DocName2 = "";
    protected void GetAllEmployeebyid()
    {
        CompetenceMasterBM compObj = new CompetenceMasterBM();
        compObj.comId = Convert.ToInt32(temp);
        compObj.GetAllCompetenceAddbyid();
        DataSet dscomp = compObj.ds;
        if (dscomp != null)
        {
            if (dscomp.Tables[0].Rows.Count > 0)
            {
                if (Convert.ToString(Session["Culture"]) == "Danish")
                {
                    lblCompetence.Text = Convert.ToString(dscomp.Tables[0].Rows[0]["comCompetenceDN"]);

                }
                else
                {
                    lblCompetence.Text = Convert.ToString(dscomp.Tables[0].Rows[0]["comCompetence"]);    
                }
                
            }
        }
        UserBM userObj = new UserBM();
        userObj.userId = Convert.ToInt32(userid);
        userObj.GetAllEmployeebyid();
        DataSet userDs = userObj.ds;
        if (userDs != null)
        {
            if (userDs.Tables[0].Rows.Count > 0)
            {
                lblName.Text = Convert.ToString(userDs.Tables[0].Rows[0]["userFirstName"]) + " " + Convert.ToString(userDs.Tables[0].Rows[0]["userLastName"]);

               

            }
        }
    }

    protected void UpdateUser()
    {
        string QueryString = "0";
        QueryString = Convert.ToString(Request.QueryString["id"]);
        InvitationBM obj = new InvitationBM();
        obj.invId = Convert.ToInt32(QueryString);
        if (ddlstastus.SelectedValue == "1")
        {
            obj.invStatus = 1;
            //obj.skillPoint = Convert.ToInt32(txtpoint.Text);
        }
        else if (ddlstastus.SelectedValue == "2")
        {
            obj.invStatus = 2;
        }
        else if (ddlstastus.SelectedValue == "3")
        {
            obj.invStatus = 3;
        }
        obj.UpdateInvitionRequest();
        if (obj.ReturnBoolean == true)
        {
            //if (ddlstastus.SelectedValue == "1")
            //{
            //    Updatepoint();
            //}
            //insertskillchild();
            //Response.Redirect("OrgInvitation_request.aspx?msg=upd");
            Response.Redirect(Convert.ToString(ViewState["PreviousPage"])+"?msg=upd");
        }

    }

    /*Saurin|20150109|*/
    protected void SubmitHelpDetails()
    {
        GetSessionData();
        String filePath = "";
        DocumentBM obj = new DocumentBM();
        DataSet invids = new DataSet();
        var docFileNameFriendly = string.Empty;
        var docAttachmentName = string.Empty;

        obj.docDescription = txtInviteGiveHelp.Text;

        if (FileUpload1.HasFile)
        {
            filePath = System.DateTime.Now.ToString("MMddyyhhmmss") + "_" + FileUpload1.PostedFile.FileName;

            FileUpload1.SaveAs(Server.MapPath("../") + "Log/upload/Document/" + System.IO.Path.GetFileName(Common.CleanFileName(filePath)));
            docAttachmentName = System.IO.Path.GetFileName(Common.CleanFileName(filePath));
            docFileNameFriendly = docAttachmentName;
            Session["docFileName"] = docAttachmentName;

        }
        else
        {
            //InsertUpdateLog(Convert.ToInt32(Session["invfromUserId"]));
            if (!string.IsNullOrWhiteSpace(txtInviteGiveHelp.Text))
            {
                invids = WebService1.ProvideHelp(txtInviteGiveHelp.Text, Convert.ToString(Request.QueryString["ToUser"]), Convert.ToString(Request.QueryString["Comp"]), GetLocalResourceObject("OfferHelpSubjectEng.Text").ToString(), GetLocalResourceObject("OfferHelpSubjectDN.Text").ToString());
                if (invids != null)
                {
                    if (invids.Tables[2].Rows.Count > 0)
                    {
                        if (!String.IsNullOrEmpty(Convert.ToString(invids.Tables[2].Rows[0]["HelpProbivdeId"])))
                        {
                            int count = Convert.ToInt32(invids.Tables[2].Rows[0]["HelpProbivdeId"]);

                            if (count == 0)
                            {
                                InsertUpdateLog(Convert.ToInt32(Convert.ToString(Request.QueryString["ToUser"])));
                            }

                        }
                    }
                }

            }
            //Response.Redirect("OrgInvitation_request.aspx?msg=upd");


            return;
        }
        var docFileNameInSystem = Common.CleanFileName(docAttachmentName);
        docAttachmentName = Common.CleanFileName(docAttachmentName);
        const string docTypes = "pdf";
        var userId = Convert.ToInt32(Session["OrgUserId"]);

        var docCreatedDt = DateTime.Now;
        var docCompnyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.docDepId = 0;
        obj.docApprovedStatus = false;
        obj.doccompetence = txtInviteGiveHelp.Text;


        DateTime docUpdatedDate = new DateTime();

        string docCreatedBy = string.Empty;
        int docDepIds = 0;
        string docTitle = docAttachmentName;
        bool docApprovedStatus = false;

        //
        var aa = new List<string>();

        string divisionIds = string.Join(",", aa.ToArray());
        aa.Clear();
        //

        string jobTypeIds = string.Join(",", aa.ToArray());


        var isPublic = false;

        var keywords = string.Empty;

        var repository = string.Empty;

        var txtDescriptions = txtInviteGiveHelp.Text;

        var docShareToId = Convert.ToInt32(Convert.ToString(Request.QueryString["ToUser"]));

        obj.InsertDocument(docFileNameFriendly, docFileNameInSystem, docAttachmentName, docTypes, userId, docCreatedDt, docUpdatedDate, false, true, docCompnyId, docCreatedBy, docDepIds, docTitle, string.Empty, docApprovedStatus, divisionIds, jobTypeIds, isPublic, keywords, repository, txtDescriptions, docShareToId, 0, "");
        
           
        //WebService1.Invitation(txtInviteGiveHelp.Text);

        invids = WebService1.ProvideHelp(txtInviteGiveHelp.Text, Convert.ToString(Request.QueryString["ToUser"]), Convert.ToString(Request.QueryString["Comp"]), GetLocalResourceObject("OfferHelpSubjectEng.Text").ToString(), GetLocalResourceObject("OfferHelpSubjectDN.Text").ToString());
        if (invids != null)
        {
            if (invids.Tables[2].Rows.Count > 0)
            {
                if (!String.IsNullOrEmpty(Convert.ToString(invids.Tables[2].Rows[0]["HelpProbivdeId"])))
                {
                    int count = Convert.ToInt32(invids.Tables[2].Rows[0]["HelpProbivdeId"]);

                    if (count == 0)
                    {
                        InsertUpdateLog(docShareToId);
                    }

                }
            }
        }


    }

    private void GetSessionData()
    {
        /*Session["InviteToUserId"] =temp;*/
        Session["Inviteformail"] = "Provide Help Competence";
        Session["Invitemessge"] = "Reply Help";

        //Session["InviteComId"] = Request.QueryString["Lid"];
        Session["notificationmsg"] = "sent you invitation to provide help"; //--2015-12-07
        Session["notificationmsgReply"] = "Provided help";


        // txtInviteGiveHelp.Text =  @" have sent you Invitation";

    }
    protected void insertKnowledge_Point(Int32 point)
    {
        // Session["InviteComId"];
        Knowledge_PointBM obj = new Knowledge_PointBM();
        obj.Knowledge_ComId = Convert.ToInt32(Session["InviteComId1"]);
        obj.Knowledge_CompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.Knowledge_point = Convert.ToString(point);
        obj.Knowledge_UserId = Convert.ToInt32(Session["OrgUserId"]);
        obj.CreatedDate = DateTime.Now;
        obj.IsDeleted = false;
        obj.IsActive = true;
        obj.InsertKnowledge_Point();

    }

    protected void InsertUpdateLog(int needHelpUserID)
    {

        //Givehelp  point  to   loggedin user
        var db = new startetkuEntities1();
        var username = (from user in db.UserMasters where user.userId == needHelpUserID select new { user.userFirstName, user.userLastName }).FirstOrDefault();
        
        LogMasterLogic obj = new LogMasterLogic();

        //obj.LogDescription = string.Format("{0} Provide Help to  userID : {1} ", Convert.ToString(Session["OrgUserName"]), needHelpUserID);   // User log description..
        obj.LogDescription = string.Format("{0} offer help to  userID : {1} ", Convert.ToString(Session["OrgUserName"]), username.userFirstName + " " + username.userLastName);   // User log description..
        var userID = Convert.ToInt32(Session["OrgUserId"]);

        var isPointAllow = true;

        var point = LogMasterLogic.InsertUpdateLogParam(userID, Common.ALP(), Common.SuggestSession(), obj.LogDescription, isPointAllow, needHelpUserID,0);





        if (isPointAllow)
            PointBM.InsertUpdatePoint(userID, 0, point);//check parameter position >> point :::  3rd pos : ALP, 2nd : PLP  -----------------

        //Provide Point if suggested a doc file

        /* var isPointForSUggestDoc=
         var pointForDocSuggest = LogMaster.InsertUpdateLogParam(userID, Common.ALP(), Common.SuggestSession(), obj.LogDescription, isPointAllow);

         if (isPointAllow)
             PointBM.InsertUpdatePoint(userID, 0, point);//check parameter position >> point :::  3rd pos : ALP, 2nd : PLP*/


        //Give point  to  user who need  help

        //obj.LogDescription = string.Format("userID : {0} Take help from {1} ", needHelpUserID, Convert.ToString(Session["OrgUserName"]));   // User log description..

        //point = LogMaster.InsertUpdateLogParam(needHelpUserID, Common.ALP(), Common.RequestSession(), obj.LogDescription, isPointAllow);

        //if (isPointAllow)
        //    PointBM.InsertUpdatePoint(needHelpUserID, 0, point); //check parameter position >> point :::  3rd pos : ALP, 2nd : PLP   -------- AS per requirement Updated by suhani 27/1/2016 ------ Only help provider get point for same requsting perosn and same competence

        if (point > 0)
        {
            insertKnowledge_Point(point);
        }
    }
    protected void txtsearch_TextChanged(object sender, EventArgs e)
    {
        GetAllDocuments();
        //ScriptManager.RegisterClientScriptBlock(this.Page, this.Page.GetType(), "script", "<script type='text/javascript'>AnotherFunction();</script>", false);

    }
    public void GetAllDocuments()
    {
        try
        {
            DocumentBM obj = new DocumentBM();
            obj.docIsActive = true;
            obj.GetAllDocument();
            DataSet ds = obj.ds;

            DataView dv = new DataView();
            dv = ds.Tables[0].DefaultView;

            // dv.RowFilter = ("docUserId =  '" + Convert.ToInt32(Session["OrgUserId"]) + "'");

            dv.RowFilter = "(docUserId = '" + Convert.ToInt32(Session["OrgUserId"]) + "' or docIsPublic = 'True'  ) ";


            //dv.RowFilter = "(docUserId = '" + Convert.ToInt32(Session["OrgUserId"]) + "' or docIsPublic = 1  ) ";

            dv.RowFilter += "and (docFileName_Friendly LIKE '%" + Convert.ToString(txtsearch.Text.Trim()) + "%' OR docKeywords like '%" + Convert.ToString(txtsearch.Text.Trim()) + "%') ";
            // dv.RowFilter = string.Concat("userFullname LIKE '%", Convert.ToString(txtserch.Value), "%'");

            DataTable dtitm = dv.ToTable();
            DataSet ds1 = new DataSet();
            ds1.Tables.Add(dtitm);

            if (ds1.Tables[0].Rows.Count > 0)
            {
                rep_document.DataSource = ds1;
                rep_document.DataBind();
            }
        }
        catch (Exception ex)
        {

        }
    }

    #endregion

    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        if (string.IsNullOrEmpty(txtInviteGiveHelp.Text) && !FileUpload1.HasFile && string.IsNullOrEmpty(lblYourDocName.Text))
        {
            return;
            //Add condition if Helptext or DocumentSharing missing then do not add entry in database.. Use RETURN. !Saurin ! 20150112
        }

        SubmitHelpDetails();
        if (!String.IsNullOrEmpty(Request.QueryString["id"]))
        {
            UpdateUser();
        }

        Response.Redirect("myCompetence.aspx");
    }
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        string language = Convert.ToString(Session["Culture"]);
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Danish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Language"] != null)
        {
            if (!Session["Language"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Language"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Language"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion

    public void GetHelpResponseByTrackId()
    {
        var trackId = 0;
        if (String.IsNullOrEmpty(Request.QueryString["HelpTrackId"]))
        {
            rptHelpReply.DataSource = null;
            rptHelpReply.DataBind();
            return;
        }
        else
        {
            trackId = Convert.ToInt32(Request.QueryString["HelpTrackId"]);
        }


        InvitationBM obj = new InvitationBM();
        obj.invIsActive = true;
        obj.invIsDeleted = false;
        obj.invFromUserId = Convert.ToInt32(Session["OrgUserId"]);
        obj.invCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.invHelpTrackId = trackId;
        obj.GetHelpResponseByTrackId();

        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                rptHelpReply.DataSource = ds.Tables[0];
                rptHelpReply.DataBind();
                // rptHelpReply.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                invHelpReply.Visible = false;
                rptHelpReply.DataSource = null;
                rptHelpReply.DataBind();
            }
        }
        else
        {
            rptHelpReply.DataSource = null;
            rptHelpReply.DataBind();
            invHelpReply.Visible = false;
        }
    }


    #region Repeater




    protected void rep_document_OnItemCommand(object source, RepeaterCommandEventArgs e)
    {
        //


        lblLabelOfSelectedDOc.Visible = true;
        lblYourDocName.Text = e.CommandArgument.ToString();
        Session["lblDocSpecificPathName"] = lblYourDocName.Text;
    }

    #endregion

    protected void rptHelpReply_OnItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

            Label docname1 = e.Item.FindControl("rptLblDocName1") as Label;
            Label docname11 = e.Item.FindControl("lbl") as Label;


            Panel docs1 = e.Item.FindControl("pnlDoc1") as Panel;
            Panel docs2 = e.Item.FindControl("pnlDoc2") as Panel;

            if (docname1 != null && string.IsNullOrWhiteSpace(docname1.Text))
            {
                if (docs1 != null) docs1.Visible = false;
            }
            var docname2 = e.Item.FindControl("rptLblDocName2") as Label;
            if (docname2 != null && string.IsNullOrWhiteSpace(docname2.Text))
            {
                if (docs2 != null) docs2.Visible = false;
            }
        }
    }

    protected void aa()
    {

    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect(Convert.ToString(ViewState["PreviousPage"]));
    }
}