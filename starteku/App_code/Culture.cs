﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Data;
using System.Net.Mail;
using System.Configuration;
using System.Globalization;
using System.Threading;
using System.Resources;
using System.Reflection;

/// <summary>
/// Summary description for Culture
/// </summary>
public class Culture
{
    public Culture()
    {

    }

    public void CultureResource(string lanname)
    {
        string languagename;
        string cul;

        //nl = locList.SelectedIndex.ToString();
        languagename = Convert.ToString(lanname.ToLower());

        switch (languagename)
        {
            case "English":
                cul = "en-GB";
                break;
            case "Danish":
                cul = "da-DK";
                break;
            default:
                cul = "en-GB";
                break;
        }

        // set this thread's current culture to the culture associated with the selected locale
        CultureInfo newCulture = new CultureInfo(cul);
        Thread.CurrentThread.CurrentCulture = newCulture;

        CultureInfo cc, cuic;
        cc = Thread.CurrentThread.CurrentCulture;
        cuic = Thread.CurrentThread.CurrentUICulture;
        //string filePath = ConfigurationManager.AppSettings["sitepath"].ToString() + "Resources/" + languageCulture + ".xml";

    }

}
    public class CommonMessages
    {  //this.Label1.Text = rm.GetString("HelloString");
       // ResourceManager rm = new ResourceManager("MappinPartyApp.Resources.Labels", Assembly.GetExecutingAssembly());
        static ResourceManager rm = new ResourceManager("Resources.CommonResource", Assembly.Load("App_GlobalResources"));
        //static ResourceManager rm = new ResourceManager("Resources.CommonResource", Assembly.Load("App_LocalResources"));
        public static string emailhead2 { get { return rm.GetString("amessagefromshowcasebot"); } }
        public static string AddNewDivision { get { return rm.GetString("AddNewDivision"); } }
        public static string Welcome { get { return rm.GetString("Welcome"); } }
        public static string Close { get { return rm.GetString("Close"); } }
        public static string NotEditable { get { return rm.GetString("NotEditable"); } }
        public static string msgRecordHasBeenDeletedSuccessfully { get { return rm.GetString("msgRecordHasBeenDeletedSuccessfully"); } }
        public static string msgRecordInsertedSuccessfully { get { return rm.GetString("msgRecordInsertedSuccessfully"); } }
        public static string msgRecordArchiveSucces { get { return rm.GetString("msgRecordArchiveSucces"); } }
        public static string msgRecordUpdatedSuccss { get { return rm.GetString("msgRecordUpdatedSuccss"); } }
        public static string Indicatesrequiredfield { get { return rm.GetString("Indicatesrequiredfield"); } }
        public static string Name { get { return rm.GetString("Name"); } }
        public static string Description { get { return rm.GetString("Description"); } }
        public static string AddNew { get { return rm.GetString("AddNew"); } }
        public static string Competence { get { return rm.GetString("Competence"); } }
        public static string AddSubCompetence { get { return rm.GetString("AddSubCompetence"); } }
        public static string PendingRequest { get { return rm.GetString("PendingRequest"); } }
        public static string View { get { return rm.GetString("View"); } }
        public static string PendingRequestInformatio { get { return rm.GetString("PendingRequestInformatio"); } }
        public static string EmployeeName { get { return rm.GetString("EmployeeName"); } }
        public static string Documents { get { return rm.GetString("Documents"); } }
        public static string AddNewDocument { get { return rm.GetString("AddNewDocument"); } }
        public static string Settings { get { return rm.GetString("Settings"); } }
        public static string NewfileUploadwarning { get { return rm.GetString("NewfileUploadwarning"); } }
        public static string Knowledgesharingrequests { get { return rm.GetString("Knowledgesharingrequests"); } }
        public static string Competencelevelschange { get { return rm.GetString("Competencelevelschange"); } }
        public static string Library { get { return rm.GetString("Library"); } }
        public static string Withindivision { get { return rm.GetString("Withindivision"); } }
        public static string Fulldatabase { get { return rm.GetString("Fulldatabase"); } }
        public static string SearchResult { get { return rm.GetString("SearchResult"); } }
        public static string Manuals { get { return rm.GetString("Manuals"); } }
        public static string NewUploaedfiles { get { return rm.GetString("NewUploaedfiles"); } }
        public static string AddNewContact { get { return rm.GetString("AddNewContact"); } }
        public static string Employee { get { return rm.GetString("Employee"); } }
        public static string AddNewEmployee { get { return rm.GetString("AddNewEmployee"); } }
        public static string Address { get { return rm.GetString("Address"); } }
        public static string PhoneNo { get { return rm.GetString("PhoneNo"); } }
        public static string EditEemployeeInformation { get { return rm.GetString("EditEemployeeInformation"); } }
        public static string PendingHeplRequest { get { return rm.GetString("PendingHeplRequest"); } }
        public static string Message { get { return rm.GetString("Message"); } }
        public static string EmployeeSkills { get { return rm.GetString("EmployeeSkills"); } }
        public static string value { get { return rm.GetString("value"); } }
        public static string check { get { return rm.GetString("check"); } }
        public static string Local { get { return rm.GetString("Local"); } }
        public static string All { get { return rm.GetString("All"); } }
        public static string Help { get { return rm.GetString("Help"); } }
        public static string NeedHelp { get { return rm.GetString("NeedHelp"); } }
        public static string NoDataToDisplay { get { return rm.GetString("NoDataToDisplay"); } }
        public static string Achieve { get { return rm.GetString("Achieve"); } }
        public static string Set { get { return rm.GetString("Set"); } }
        public static string FirstName { get { return rm.GetString("FirstName"); } }
        public static string LastName { get { return rm.GetString("LastName"); } }
        public static string Gender { get { return rm.GetString("Gender"); } }
        public static string Password { get { return rm.GetString("Password"); } }
        public static string ConfirmPassword { get { return rm.GetString("ConfirmPassword"); } }
        public static string Phone { get { return rm.GetString("Phone"); } }
        public static string Country { get { return rm.GetString("Country"); } }
        public static string State { get { return rm.GetString("State"); } }
        public static string City { get { return rm.GetString("City"); } }
        public static string Language { get { return rm.GetString("Language"); } }
        public static string Profile { get { return rm.GetString("Profile"); } }
        public static string CATEGORY { get { return rm.GetString("CATEGORY"); } }
        public static string Next { get { return rm.GetString("Next"); } }
        public static string Back { get { return rm.GetString("Back"); } }
        public static string Dashboard { get { return rm.GetString("Dashboard"); } }
        public static string Manager { get { return rm.GetString("Manager"); } }
        public static string Department { get { return rm.GetString("Department"); } }
        public static string Archive { get { return rm.GetString("Archive"); } }
        public static string Recordnotfound { get { return rm.GetString("Recordnotfound"); } }
        public static string ParentDepartment { get { return rm.GetString("ParentDepartment"); } }
        public static string DepartmentName{ get { return rm.GetString("DepartmentName"); } }
        public static string Industry { get { return rm.GetString("Industry"); } }
        public static string JobType { get { return rm.GetString("JobType"); } }
        public static string Level { get { return rm.GetString("Level"); } }
        public static string Company { get { return rm.GetString("Company"); } }
        public static string CompanyName { get { return rm.GetString("CompanyName"); } }
        public static string CompanyPhone { get { return rm.GetString("CompanyPhone"); } }
        public static string CompanyAddress { get { return rm.GetString("CompanyAddress"); } }
        public static string MobileNo { get { return rm.GetString("MobileNo"); } }
        public static string CompanyList { get { return rm.GetString("CompanyList"); } }
        public static string Divisions { get { return rm.GetString("Divisions"); } }
        public static string LogOut { get { return rm.GetString("LogOut"); } }
        public static string ChangePassword { get { return rm.GetString("ChangePassword"); } }
        public static string AddCategory { get { return rm.GetString("AddCategory"); } }
        public static string AddCompetence { get { return rm.GetString("AddCompetence"); } }
        public static string Report { get { return rm.GetString("Report"); } }
        public static string SetPoint { get { return rm.GetString("SetPoint"); } }
        public static string Contact { get { return rm.GetString("Contact"); } }
        public static string AddEmployee { get { return rm.GetString("AddEmployee"); } }
        public static string Allrequest { get { return rm.GetString("Allrequest"); } }
        public static string SubmitDOU { get { return rm.GetString("SubmitDOU"); } }
        public static string Notification { get { return rm.GetString("Notification"); } }
        public static string Attachments { get { return rm.GetString("Attachments"); } }
        public static string Public { get { return rm.GetString("Public"); } }
        public static string Private { get { return rm.GetString("Private"); } }
        public static string CompetenceNotification { get { return rm.GetString("CompetenceNotification"); } }
        public static string MessageNotification { get { return rm.GetString("MessageNotification"); } }
        public static string PassiveLearningPoints { get { return rm.GetString("PassiveLearningPoints"); } }
        public static string ActiveLearningPoints { get { return rm.GetString("ActiveLearningPoints"); } }
        public static string Account { get { return rm.GetString("Account"); } }
        public static string SignIn { get { return rm.GetString("SignIn"); } }
        public static string CompetencesSkill { get { return rm.GetString("CompetencesSkill"); } }
        public static string OpenCompetencesScreen { get { return rm.GetString("OpenCompetencesScreen"); } }
        public static string FinalFillCompetences { get { return rm.GetString("FinalFillCompetences"); } }
        public static string ReadDoc { get { return rm.GetString("ReadDoc"); } }
        public static string ReadSuggestDoc { get { return rm.GetString("ReadSuggestDoc"); } }
        public static string RequestSession { get { return rm.GetString("RequestSession"); } }
        public static string SuggestSession { get { return rm.GetString("RequestSession"); } }
        public static string GiveHelp { get { return rm.GetString("GiveHelp"); } }
        public static string SuggestDoc { get { return rm.GetString("SuggestDoc"); } }
        public static string SuggestDoctoother { get { return rm.GetString("SuggestDoctoother"); } }
        public static string PublicDoc { get { return rm.GetString("PublicDoc"); } }
        public static string Viewrequest { get { return rm.GetString("Viewrequest"); } }
        public static string Competencelevel { get { return rm.GetString("Competencelevel"); } }
        public static string ChatRooms { get { return rm.GetString("ChatRooms"); } }
        public static string ViewPoint { get { return rm.GetString("ViewPoint"); } }
        public static string Point { get { return rm.GetString("Point"); } }
        public static string PointSummary { get { return rm.GetString("PointSummary"); } }
        public static string Comparison { get { return rm.GetString("Comparison"); } }
        public static string YourBottom3Competence { get { return rm.GetString("YourBottom3Competence"); } }
        public static string YourTop3Competence { get { return rm.GetString("YourTop3Competence"); } }
        public static string Invitationrequest { get { return rm.GetString("Invitationrequest"); } }
        public static string RequestDate { get { return rm.GetString("RequestDate"); } }
        public static string HelpResponse { get { return rm.GetString("HelpResponse"); } }
        public static string ShareDocument { get { return rm.GetString("ShareDocument"); } }
        public static string SelectedDocumentName { get { return rm.GetString("SelectedDocumentName"); } }
        public static string Search { get { return rm.GetString("Search"); } }
        public static string REQUESTFORHELP { get { return rm.GetString("REQUESTFORHELP"); } }
        public static string INVITATIONFORHELP { get { return rm.GetString("INVITATIONFORHELP"); } }
        public static string LISTOF { get { return rm.GetString("LISTOF"); } }
        public static string LISTOFNOTIFICATION { get { return rm.GetString("LISTOFNOTIFICATION"); } }
        public static string ReplyDate { get { return rm.GetString("ReplyDate"); } }
        public static string personaldevelopmentpoints { get { return rm.GetString("personaldevelopmentpoints"); } }
       
    }


