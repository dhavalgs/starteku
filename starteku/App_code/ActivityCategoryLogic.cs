﻿using starteku_BusinessLogic;
using starteku_BusinessLogic.Model;
using System;
using System.Collections.Generic;
using System.Linq;


/// <summary>
/// Summary description for ActivityCategoryLogic
/// </summary>
/// developed by Jainam Shah 17-3-2017
public class ActivityCategoryLogic
{

    public static List<ActivityCategory_Get_Result> GetActivityCategoryList(int resLangId, int actCatCreateBy, int isActive = -1, int isPublic = -1, int catId = 0, int isActCompEnabled = -1)
    {
        var db = new startetkuEntities1();

        var actCatList = new List<ActivityCategory_Get_Result>();
        try
        {
            actCatList = db.ActivityCategory_Get(resLangId, actCatCreateBy, isActive, isPublic, -1, catId, "", isActCompEnabled, -1, -1).ToList();

            //if (actCatCreateBy > 0)
            //{
            //    actCatList = db.ActivityCategoryMasters.Where(o => o.ActCatCreatedBy == actCatCreateBy  ).ToList();
            //}
            //else
            //{
            //    actCatList = db.ActivityCategoryMasters.Where(o => o.ActCatsActive == isActive).ToList();
            //}

        }
        catch (Exception ex)
        {

            ExceptionLogger.LogException(ex);
            return new List<ActivityCategory_Get_Result>();
        }
        finally
        {

            CommonAttributes.DisposeDBObject(ref db);
        }

        return actCatList;

    }
}