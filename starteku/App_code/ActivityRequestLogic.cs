﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using starteku_BusinessLogic;
using starteku_BusinessLogic.Model;

/// <summary>
/// Summary description for ActivityRequestLogic
/// </summary>
public static class ActivityRequestLogic
{
	static ActivityRequestLogic()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static string GetActivityPriorityStatusByNumber(string actReqStatus)
    {

        var status = StringFor.High;
         switch (actReqStatus)
            {
                case "1":
                    {
                        status = StringFor.High;
                        break;
                    }
                case "2":
                    {
                        status = StringFor.Medium;
                        break;
                    }
                case "3":
                    {
                        status = StringFor.Low;
                        break;
                    }
               

            }
        return status;
    }

   

    public static List<GetActivityRequestLists_Result> GetActivityRequestList(int actToUserId)
    {
        var db=new startetkuEntities1();
        try
        {
            var getLists = db.GetActivityRequestLists(actToUserId, 0, 0,0,"",true,0).ToList();
            return getLists;

        }
        catch (Exception ex)
        {

            ExceptionLogger.LogException(ex);
            return new List<GetActivityRequestLists_Result>();
        }
        finally
        {
            CommonAttributes.DisposeDBObject(ref db);
        }
    }
}