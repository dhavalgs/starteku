﻿using starteku_BusinessLogic.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for DbOperation
/// </summary>
public class DbOperation
{
    public DbOperation()
    {

    }


    public static startetkuEntities1 GetDb()
    {
        return new startetkuEntities1();
    }

    public static void DisposeDBObject(ref startetkuEntities1 db)
    {
        db.Dispose();
    }
}