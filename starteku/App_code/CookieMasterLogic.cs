﻿using System;
using System.Collections.Generic;
using System.Data.Objects;
using System.Linq;
using System.Web;
using starteku_BusinessLogic;
using starteku_BusinessLogic.Model;

/// <summary>
/// Summary description for CookieMasterLogic
/// </summary>
public class CookieMasterLogic
{
	public CookieMasterLogic()
	{
		//
		// TODO: Add constructor logic here
		//
	}

    public static string DbCookieNew(string cookieName, string cookieVal, DateTime expireDateTime)
    {
        var db = new startetkuEntities1();
        try
        {
            var curDate = DateTime.Now;
            var getCookie = db.CookieMasters.FirstOrDefault(o => o.CookieName == cookieName && EntityFunctions.DiffHours(o.CookieCreatedDateTime, curDate) < 24);
            if (getCookie != null)
            {
                return getCookie.CookieValue;
            }
            else
            {
                getCookie = new CookieMaster();
                getCookie.CookieCreatedDateTime = DateTime.Now;
                getCookie.CookieValue = cookieVal;
                getCookie.CookieUpdateDateTime = DateTime.Now;
                getCookie.CookieStatus = "";
                getCookie.CookieName = cookieName;
                db.CookieMasters.Add(getCookie);
                db.SaveChanges();
            }

        }
        catch (Exception ex)
        {


            ExceptionLogger.LogException(ex);

        }
        finally
        {
            db.Dispose();

        }
        return String.Empty;
    }
}