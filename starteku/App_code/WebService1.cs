﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using starteku_BusinessLogic;
using starteku_BusinessLogic.Model;
using starteku_BusinessLogic.View;
using startetku.Business.Logic;

/// <summary>
/// Summary description for WebService1
/// </summary>
[WebService(Namespace = "https://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
[System.Web.Script.Services.ScriptService]
public class WebService1 : System.Web.Services.WebService
{

    public WebService1()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    [WebMethod]
    public string HelloWorld()
    {
        return "Hello World";
    }

    [WebMethod]
    public string TestMethod(String data)
    {
        return data;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string JoyRideTourShowHide(string checkedValue)
    {
        if (string.IsNullOrWhiteSpace((HttpContext.Current.Session["OrgUserId"]).ToString())) return "";

        var userId = Convert.ToInt32((HttpContext.Current.Session["OrgUserId"]).ToString());
        bool turnOffFutureTour = checkedValue != "undefined"; // checkbox ticked then pass true, else false pass

        JoyrideTourBM.JoyRideTourShowHideData(userId, true, false, turnOffFutureTour);
        return "";
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string UpdateNotification(string notIdQ)
    {
        if (string.IsNullOrWhiteSpace((HttpContext.Current.Session["OrgUserId"]).ToString())) return "";
        var userId = Convert.ToInt32((HttpContext.Current.Session["OrgUserId"]).ToString());
        NotificationBM.UpdateNotificationByNotToUserId(false, userId);
        return string.Empty;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string UpdateMessge(string notIdQ)
    {
        if (string.IsNullOrWhiteSpace((HttpContext.Current.Session["OrgUserId"]).ToString())) return "";
        var userId = Convert.ToInt32((HttpContext.Current.Session["OrgUserId"]).ToString());
        NotificationBM.UpdateMessageByNotToUserId(false, userId);
        return string.Empty;
    }



    #region Point-Log

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string InsertUpdatePoint(string uId, string toId, string url, string docId)
    {


        //uId is a user who orignally uploaded document. 
        if (string.IsNullOrWhiteSpace((HttpContext.Current.Session["OrgUserId"]).ToString())) return "";


        var userId = Convert.ToInt32((HttpContext.Current.Session["OrgUserId"]).ToString());


        var documentId = Convert.ToInt32(docId);

        var docDetails = DocumentBM.GetDocumentById(documentId).FirstOrDefault();

        var docIsPrivate = docDetails != null && !docDetails.IsPublic;

        //Update Code :20150107: If USER going to read his own uploaded file then  do no give any point.. |Saurin |
        var allowPoint = docIsPrivate;
        if (userId == Convert.ToInt32(uId))
        {
            allowPoint = false; // that means point will not be given..
        }


        //Lets log : current user goin to read doc. 
        var point = LogMasterLogic.InsertUpdateLogParam(
            userId,
            Common.ALP(), Common.ALPPointForReadDoc(),
            string.Format(
                "UserID : {0} read  document : {1},Document was uploaded by userid : {0}, Document File path : {2}",
                userId, url, uId), allowPoint, 0, 0);

        //check user has authority to read this document
        if (docDetails != null &&
            (toId.Trim() == "0" || (Convert.ToInt32(toId) == userId) || docDetails.UserId == userId))
        // zero means all user allowed to read
        {

            //User has autherity then lets give him point to read this doc : ACTIVE POINT
            if (allowPoint)
                PointBM.InsertUpdatePoint(userId, 0, point);



            //Lets provide points to user who uploaded document : Passive point
            //Lets write log again that we are providing passive point to a user who upload this document

            if (!string.IsNullOrWhiteSpace(uId))
            {
                var uid = Convert.ToInt32(uId);
                var user = UserBM.GetUserById(Convert.ToInt32(uid)).FirstOrDefault();
                if (user != null)
                {
                    point = LogMasterLogic.InsertUpdateLogParam(
                        Convert.ToInt32(uid), "PLP", Common.PLPPointForReadDocByOther(),
                        string.Format(
                            "UserID : {0}'s document has been read by : {1} , Document Path: {2} ",
                            uId, userId, url, userId), docDetails != null && !docDetails.IsPublic, 0, 0);


                    //now give point to user  in Point master table, only when doc is private, for public doc. user already awarded with point.
                    if (allowPoint)
                        PointBM.InsertUpdatePoint(user.UserId, point, 0);
                }
            }

        }
        else
        {
            return "error:WrongUser";
        }
        return string.Empty;
    }

    public void InsertUpdateLog()
    {
        LogMasterLogic obj = new LogMasterLogic();
        obj.LogIsActive = true;
        obj.LogIsDeleted = false;
        obj.LogCreateDate = DateTime.Now;
        obj.LogIp = Common.GetVisitorIPAddress(true);
        obj.LogUserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        obj.LogCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        obj.LogUserType = Convert.ToInt32(HttpContext.Current.Session["OrguserType"]);

        obj.LogDescription = string.Format("{0} has read {1} document ", Convert.ToString(Session["OrgUserName"]), "");
        // User log description..

        obj.LogOutTime = new DateTime();
        obj.LogCreateDate = DateTime.Now;
        obj.LogBrowser = Common.GetWebBrowserName();
        obj.LogPointInfo = string.Empty;

        // 
        if (obj.LogUserType == 3)
        {
            if (!string.IsNullOrWhiteSpace(Convert.ToString(Session["userDateTime"])))
            {
                obj.LogPLP = LogMasterLogic.GetPointByPointName("ReadDOC", Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]));
            }

        }

        obj.InsertUpdateLog();

    }

    #endregion

    [WebMethod(EnableSession = true)]
    public static DataSet Invitation(string txtto, int InvitationId, string EngSub, string DnSub)
    {
        //string msg = string.Empty;
        DataSet invids = new DataSet();
        invids = null;

        InvitationBM objAtt = new InvitationBM();
        objAtt.invfromUserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        objAtt.invToUserId = Convert.ToInt32(HttpContext.Current.Session["invfromUserId"]);
        objAtt.invsubject = txtto;
        objAtt.invCreatedDate = DateTime.Now;
        objAtt.invIsDeleted = false;
        objAtt.invIsActive = true;
        objAtt.invCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        objAtt.invtype = Convert.ToString(HttpContext.Current.Session["Invitemessge"]);
        objAtt.invComId = Convert.ToInt32(HttpContext.Current.Session["InviteComId"]);

        objAtt.invStatus = Convert.ToInt32(1);
        if (string.IsNullOrWhiteSpace(Convert.ToString(HttpContext.Current.Session["invHelpTrackId"])))
        {
            return invids;
            //return msg = "false";
        }
        objAtt.invHelpTrackId = Convert.ToInt32(HttpContext.Current.Session["invHelpTrackId"]);

        //Add docfilename and suggestpath
        objAtt.invDocSuggestPath = Convert.ToString(HttpContext.Current.Session["lblDocSpecificPathName"]);
        objAtt.invDocName = Convert.ToString(HttpContext.Current.Session["docFileName"]);
        //
        string invDocName = objAtt.invDocName;
        string invDocSuggestPath = objAtt.invDocSuggestPath;
        if (!objAtt.InsertInvition())
        {
            //msg = "false";
            //invids = objAtt.ds;
            return invids;
        }
        else
        {

            invids = objAtt.ds;

            #region Notification

            UserBM obj1 = new UserBM();
            obj1.SetUserId = Convert.ToInt32(HttpContext.Current.Session["invfromUserId"]);
            obj1.GetUserSettingById();
            DataSet ds1 = obj1.ds;
            if (ds1.Tables[0].Rows.Count > 0)
            {
                if (!String.IsNullOrEmpty(Convert.ToString(ds1.Tables[0].Rows[0]["SetInvitationNotification"])))
                {
                    if (Convert.ToBoolean(ds1.Tables[0].Rows[0]["SetNewFileUploadwarning"]))
                    {

                        NotificationBM obj = new NotificationBM();
                        obj.notsubject = Convert.ToString(HttpContext.Current.Session["notificationmsgReply"]);
                        obj.notUserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
                        obj.notIsActive = true;
                        obj.notIsDeleted = false;
                        obj.notPopUpStatus = true;
                        obj.notCreatedDate = DateTime.Now;
                        obj.notCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
                        //obj.notpage = "OrgInvitation.aspx?id=" + Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
                        //obj.notpage = "OrgInvitation_request.aspx";
                        if (invids != null)
                        {
                            if (invids.Tables[0].Rows.Count > 0)
                            {
                                if (invids.Tables[3].Rows.Count > 0)
                                {
                                    obj.notpage = "OrgInvitation.aspx?View=" + InvitationId + "&HelpTrackId=" + Convert.ToInt32(invids.Tables[3].Rows[0]["invHelpTrackId"]);
                                }
                            }
                        }
                        else
                        {
                            obj.notpage = "OrgInvitation_request.aspx";
                        }
                        //obj.notpage = "OrgInvitation.aspx?id=" + Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
                        obj.nottype = "Invite";
                        obj.notToUserId = Convert.ToInt32(HttpContext.Current.Session["invfromUserId"]);
                        obj.InsertNotification();

                        #region Send mail
                        Template template = CommonModule.getTemplatebyname1("Accept to help", obj.notToUserId);
                        //String subject = Convert.ToString(HttpContext.Current.Session["Inviteformail"]) + " for " +
                        //                 Convert.ToString(HttpContext.Current.Session["OrgUserName"]);
                        //String confirmMail =
                        //    CommonModule.getTemplatebyname("Accept to help", obj.notToUserId);
                        string confirmMail = template.TemplateName;
                        if (!String.IsNullOrEmpty(confirmMail))
                        {
                            UserBM Cust = new UserBM();
                            Cust.userId = Convert.ToInt32(HttpContext.Current.Session["invfromUserId"]);
                            Cust.SelectPasswordByUserId();
                            DataSet ds = Cust.ds;
                            if (ds != null)
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    //Session["Inviteformail"]
                                    String name = Convert.ToString(ds.Tables[0].Rows[0]["userFirstName"]);
                                    string fromid = Convert.ToString(ConfigurationManager.AppSettings["fromid"]);
                                    String empname = Convert.ToString(HttpContext.Current.Session["OrgUserName"]);

                                    string tempString = confirmMail;
                                    String subject = EngSub;
                                    if (template.Language == "Danish")
                                    {
                                        subject = DnSub;
                                    }
                                    tempString = tempString.Replace("###name###", name);
                                    tempString = tempString.Replace("###empname###", empname);
                                    if (!string.IsNullOrEmpty(invDocName))
                                    {
                                        string str = ConfigurationManager.AppSettings["siteurl"] + "Log/upload/Document/" + invDocName;
                                        invDocName = "<a href='" + str + "'>" + invDocName + "</a>";
                                    }
                                    if (!string.IsNullOrEmpty(invDocSuggestPath))
                                    {
                                        string str1 = ConfigurationManager.AppSettings["siteurl"] + "Log/upload/Document/" + invDocSuggestPath;
                                        invDocSuggestPath = "<a href='" + str1 + "'>" + invDocSuggestPath + "</a>";
                                    }
                                    tempString = tempString.Replace("###Message###", txtto);
                                    tempString = tempString.Replace("###Link1###", invDocSuggestPath);
                                    tempString = tempString.Replace("###Link###", invDocName);
                                    CommonModule.SendMailToUser(Convert.ToString(ds.Tables[0].Rows[0]["userEmail"]),
                                                                subject, tempString, fromid, Convert.ToString(HttpContext.Current.Session["OrgCompanyId"]));
                                }
                            }
                        }

                        #endregion
                    }
                }
            }

            #endregion

            // msg = "true";
            HttpContext.Current.Session["invfromUserId"] = "";
            HttpContext.Current.Session["Invitemessge"] = "";
            HttpContext.Current.Session["InviteComId"] = "";
            HttpContext.Current.Session["notificationmsg"] = "";
            HttpContext.Current.Session["lblDocSpecificPathName"] = "";
            HttpContext.Current.Session["docFileName"] = "";

        }

        //return msg;
        return invids;
    }

    [WebMethod(EnableSession = true)]
    public static DataSet ProvideHelp(string txtto, string touserid, string compId, string EngSub, string DnSub)
    {
        //string msg = string.Empty;
        DataSet invids = new DataSet();
        invids = null;

        InvitationBM objAtt = new InvitationBM();
        objAtt.invfromUserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
        objAtt.invToUserId = Convert.ToInt32(touserid);
        objAtt.invsubject = txtto;
        objAtt.invCreatedDate = DateTime.Now;
        objAtt.invUpdatedDate = DateTime.Now;

        objAtt.invIsDeleted = false;
        objAtt.invIsActive = true;
        objAtt.invCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
        objAtt.invtype = Convert.ToString("ProHelp");
        objAtt.invComId = Convert.ToInt32(compId);
        objAtt.invStatus = Convert.ToInt32(3);
        //if (string.IsNullOrWhiteSpace(Convert.ToString(HttpContext.Current.Session["invHelpTrackId"])))
        //{
        //    return invids;
        //    //return msg = "false";
        //}
        objAtt.invHelpTrackId = 0;

        //Add docfilename and suggestpath
        objAtt.invDocSuggestPath = Convert.ToString(HttpContext.Current.Session["lblDocSpecificPathName"]);
        objAtt.invDocName = Convert.ToString(HttpContext.Current.Session["docFileName"]);

        string invDocName = objAtt.invDocName;
        string invDocSuggestPath = objAtt.invDocSuggestPath;

        //
        if (!objAtt.InsertInvition())
        {
            //msg = "false";
            //invids = objAtt.ds;
            return invids;
        }
        else
        {

            invids = objAtt.ds;

            #region Notification

            UserBM obj1 = new UserBM();
            obj1.SetUserId = Convert.ToInt32(touserid);
            obj1.GetUserSettingById();
            DataSet ds1 = obj1.ds;
            if (ds1.Tables[0].Rows.Count > 0)
            {
                if (!String.IsNullOrEmpty(Convert.ToString(ds1.Tables[0].Rows[0]["SetInvitationNotification"])))
                {
                    if (Convert.ToBoolean(ds1.Tables[0].Rows[0]["SetNewFileUploadwarning"]))
                    {
                        NotificationBM obj = new NotificationBM();
                        obj.notsubject = Convert.ToString(HttpContext.Current.Session["notificationmsgReply"]);
                        obj.notUserId = Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
                        obj.notIsActive = true;
                        obj.notIsDeleted = false;
                        obj.notPopUpStatus = true;
                        obj.notCreatedDate = DateTime.Now;
                        obj.notCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
                        //obj.notpage = "OrgInvitation.aspx?id=" + Convert.ToInt32(HttpContext.Current.Session["OrgUserId"]);
                        //obj.notpage = "OrgInvitation_request.aspx";
                        if (invids != null)
                        {
                            if (invids.Tables[0].Rows.Count > 0)
                            {
                                obj.notpage = "OrgInvitation.aspx?view=" + invids.Tables[0].Rows[0]["lastid"];
                            }
                            else
                            {
                                obj.notpage = "OrgInvitation_request.aspx";
                            }
                        }
                        else
                        {
                            obj.notpage = "OrgInvitation_request.aspx";
                        }

                        obj.nottype = "ProHelp";
                        obj.notToUserId = Convert.ToInt32(touserid);
                        obj.InsertNotification();

                        #region Send mail

                        // String subject = "Offer help";
                        //String confirmMail =
                        //    CommonModule.getTemplatebyname("Offer Help", obj.notToUserId);

                        Template template = CommonModule.getTemplatebyname1("Offer Help", obj.notToUserId);
                        string confirmMail = template.TemplateName;
                        if (!String.IsNullOrEmpty(confirmMail))
                        {
                            UserBM Cust = new UserBM();
                            Cust.userId = Convert.ToInt32(touserid);
                            Cust.SelectPasswordByUserId();
                            DataSet ds = Cust.ds;
                            if (ds != null)
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    //Session["Inviteformail"]
                                    String name = Convert.ToString(ds.Tables[0].Rows[0]["userFirstName"]);
                                    string fromid = Convert.ToString(ConfigurationManager.AppSettings["fromid"]);
                                    String empname = Convert.ToString(HttpContext.Current.Session["OrgUserName"]);
                                    String level = "";
                                    String competence = "";
                                    if (ds.Tables[1].Rows.Count > 0)
                                    {
                                        level = Convert.ToString(ds.Tables[1].Rows[0]["skillAchive"]);

                                        competence = Convert.ToString(ds.Tables[1].Rows[0]["comCompetence"]);

                                    }
                                    string tempString = confirmMail;
                                    String subject = EngSub;
                                    if (template.Language == "Danish")
                                    {
                                        subject = DnSub;
                                    }
                                    tempString = tempString.Replace("###name###", name);
                                    tempString = tempString.Replace("###empname###", empname);
                                    tempString = tempString.Replace("###Message###", txtto);
                                    if (!string.IsNullOrEmpty(invDocName))
                                    {
                                        string str = ConfigurationManager.AppSettings["siteurl"] + "Log/upload/Document/" + invDocName;
                                        invDocName = "<a href='" + str + "'>" + invDocName + "</a>";
                                    }
                                    if (!string.IsNullOrEmpty(invDocSuggestPath))
                                    {
                                        string str1 = ConfigurationManager.AppSettings["siteurl"] + "Log/upload/Document/" + invDocSuggestPath;
                                        invDocSuggestPath = "<a href='" + str1 + "'>" + invDocSuggestPath + "</a>";
                                    }
                                    tempString = tempString.Replace("###Link1###", invDocSuggestPath);
                                    tempString = tempString.Replace("###Link###", invDocName);
                                    CommonModule.SendMailToUser(Convert.ToString(ds.Tables[0].Rows[0]["userEmail"]),
                                                                subject, tempString, fromid, Convert.ToString(HttpContext.Current.Session["OrgCompanyId"]));
                                }
                            }
                        }

                        #endregion
                    }
                }
            }

            #endregion

            // msg = "true";
            HttpContext.Current.Session["invfromUserId"] = "";
            HttpContext.Current.Session["Invitemessge"] = "";
            HttpContext.Current.Session["InviteComId"] = "";
            HttpContext.Current.Session["notificationmsg"] = "";
            HttpContext.Current.Session["lblDocSpecificPathName"] = "";
            HttpContext.Current.Session["docFileName"] = "";

        }

        //return msg;
        return invids;

    }
    public static DataView GetView(DataSet ds, string filter, string sort)
    {
        try
        {
            DataView dv = new DataView(ds.Tables[0]);
            dv.Sort = sort;
            dv.RowFilter = filter;
            return dv;
        }
        catch (Exception ex)
        {

            Common.WriteLog("GetView===============Error============" + ex.StackTrace);
            return null;
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public List<string> GetCompetenceListByDivJobTypeId(string dataNeedsOf, int divid, int jobtypeid)
    {
        var userid = 0;
        var usertype = 3;
        if (string.IsNullOrWhiteSpace(dataNeedsOf))
        {
            userid = Convert.ToInt32(Session["OrgUserId"]);
            usertype = Convert.ToInt32(Session["OrguserType"]);
        }
        else
        {
            userid = Convert.ToInt32(dataNeedsOf);
            var userdetails = UserBM.GetUserByIdSPL(userid).FirstOrDefault();
            if (userdetails != null) usertype = userdetails.UserType;
        }


        var listCompetence = new List<string>();
        EmployeeSkillBM obj = new EmployeeSkillBM();
        obj.skillIsActive = true;
        obj.skillIsDeleted = false;
        obj.skillUserId = userid;
        obj.skillCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);

        //Check user is Manager or employee.
        if (usertype == 2) //Manager
        {
            obj.GetEmpAvgSkillByManager();


        }
        else if (usertype == 3) //Employee
        {
            obj.GetSkillByUserId();
        }
        else //Company
        {
            obj.skillUserId = 0;
            obj.GetEmpAvgSkillByManager();
        }


        var filter = "";

        if (divid == 0)
        {

            if (jobtypeid == 0)
            {
                filter = "";
            }
            else
            {
                filter = "userJobType=" + jobtypeid;
            }
        }
        else
        {
            if (jobtypeid == 0)
            {
                filter = "userdivId=" + divid;
            }
            else
            {
                filter = "userdivId=" + divid + "and userJobType=" + jobtypeid;
            }

        }


        DataView dv = GetView(obj.ds, filter, "");



        if (dv != null)
        {

            var ds = dv.ToTable();
            var mylist = "[ ";
            //var mylist="[";
            string mylist1 = "[ ";
            string mylist2 = "[ ";
            string mylist3 = "[ ";
            if (ds != null)
            {
                if (ds.Rows.Count > 0)
                {
                    String competences = null;


                    int i;
                    for (i = 0; i <= ds.Rows.Count - 1; i++)
                    {
                        competences = Convert.ToString(ds.Rows[i]["comCompetence"]);
                        String local = Convert.ToString(ds.Rows[i]["skillLocal"]);
                        String achive = Convert.ToString(ds.Rows[i]["skillAchive"]);
                        String target = Convert.ToString(ds.Rows[i]["skilltarget"]);

                        local = local.Replace(',', '.');
                        achive = achive.Replace(',', '.');
                        target = target.Replace(',', '.');


                        //mylist += local + "," + competences + "], [";
                        mylist1 += string.Format("[\"{1}\",{0}],", local, competences);
                        mylist2 += string.Format("[\"{1}\",{0}],", achive, competences);
                        mylist3 += string.Format("[\"{1}\",{0}],", target, competences);
                        /*switch (dataNeedsOf)
                        {
                            case "local":
                                mylist1 += string.Format("[\"{1}\",{0}],", local, competences);
                                break;
                            case "achive":
                                mylist2 += string.Format("[\"{1}\",{0}],", achive, competences);
                                break;
                            case "target":
                                mylist3 += string.Format("[\"{1}\",{0}],", target, competences);
                                break;
                        }*/
                        /*listCompetence.Add(local + ",\"" + competences + "\"], [");*/
                    }
                    mylist1 = mylist1.Remove(mylist1.Length - 1);
                    mylist2 = mylist2.Remove(mylist2.Length - 1);
                    mylist3 = mylist3.Remove(mylist3.Length - 1);
                    mylist1 += "]";
                    mylist2 += "]";
                    mylist3 += "]";

                    mylist += "]";

                }
            }

            listCompetence.Add(mylist1);
            listCompetence.Add(mylist2);
            listCompetence.Add(mylist3);
            // return listCompetence;
        }

      //  ExceptionLogger.LogException(new Exception(), 0, "listCompetence " + Newtonsoft.Json.JsonConvert.SerializeObject(listCompetence));
        return listCompetence;
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public List<string> GetCompetenceList(string dataNeedsOf)
    {
        var userId = HttpContext.Current.Request.QueryString["umId"];
        var userid = 0;
        var usertype = 3;
        if (string.IsNullOrWhiteSpace(dataNeedsOf) || dataNeedsOf == "undefined")
        {
            userid = Convert.ToInt32(Session["OrgUserId"]);
            usertype = Convert.ToInt32(Session["OrguserType"]);
        }
        else
        {
            userid = Convert.ToInt32(dataNeedsOf);
            var userdetails = UserBM.GetUserByIdSPL(userid).FirstOrDefault();
            if (userdetails != null) usertype = userdetails.UserType;
        }


        var listCompetence = new List<string>();
        EmployeeSkillBM obj = new EmployeeSkillBM();
        obj.skillIsActive = true;
        obj.skillIsDeleted = false;
        obj.skillUserId = userid;
        obj.skillCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);

        //Check user is Manager or employee.
        if (usertype == 2) //Manager
        {
            obj.GetEmpAvgSkillByManager();
            //obj.GetSkillByUserId();

        }
        else if (usertype == 3) //Employee
        {
            obj.GetSkillByUserId();
        }
        else //Company
        {
            obj.skillUserId = 0;
            obj.GetEmpAvgSkillByManager();
        }
        DataSet ds = obj.ds;
        var mylist = "[ ";
        //var mylist="[";
        string mylist1 = "[ ";
        string mylist2 = "[ ";
        string mylist3 = "[ ";
        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                String competences = null;

                int i;
                for (i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    if (Convert.ToString(Session["Culture"]) == "Danish")
                    {
                        competences = Convert.ToString(ds.Tables[0].Rows[i]["comCompetenceDN"]);
                    }
                    else
                    {
                        competences = Convert.ToString(ds.Tables[0].Rows[i]["comCompetence"]);
                    }
                    String local = Convert.ToString(ds.Tables[0].Rows[i]["skillLocal"]);
                    String achive = Convert.ToString(ds.Tables[0].Rows[i]["skillAchive"]);
                    String target = Convert.ToString(ds.Tables[0].Rows[i]["skilltarget"]);
                    //mylist += local + "," + competences + "], [";

                    local = local.Replace(',', '.');
                    achive = achive.Replace(',', '.');
                    target = target.Replace(',', '.');

                    mylist1 += string.Format("[\"{1}\",{0}],", local, competences);
                    mylist2 += string.Format("[\"{1}\",{0}],", achive, competences);
                    mylist3 += string.Format("[\"{1}\",{0}],", target, competences);
                    /*switch (dataNeedsOf)
                    {
                        case "local":
                            mylist1 += string.Format("[\"{1}\",{0}],", local, competences);
                            break;
                        case "achive":
                            mylist2 += string.Format("[\"{1}\",{0}],", achive, competences);
                            break;
                        case "target":
                            mylist3 += string.Format("[\"{1}\",{0}],", target, competences);
                            break;
                    }*/
                    /*listCompetence.Add(local + ",\"" + competences + "\"], [");*/
                }


                mylist1 = mylist1.Remove(mylist1.Length - 1);
                mylist2 = mylist2.Remove(mylist2.Length - 1);
                mylist3 = mylist3.Remove(mylist3.Length - 1);
                mylist1 += "]";
                mylist2 += "]";
                mylist3 += "]";

                mylist += "]";


            }
        }

        listCompetence.Add(mylist1);
        listCompetence.Add(mylist2);
        listCompetence.Add(mylist3);
        // return listCompetence;
      //  ExceptionLogger.LogException(new Exception(), 0, "listCompetence " + Newtonsoft.Json.JsonConvert.SerializeObject(listCompetence));
        return listCompetence;
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public List<string> GetComList(string dataNeedsOf, int val, string mul, int TargetType = 1, int TeamID = 0)
    {
        var userId = HttpContext.Current.Request.QueryString["umId"];
        var userid = 0;
        var usertype = 3;
       // int TeamID = 0;
        var listCompetence = new List<string>();
        bool flag = false;
        try
        {

            EmployeeSkillBM obj = new EmployeeSkillBM();
            if (string.IsNullOrWhiteSpace(dataNeedsOf) || dataNeedsOf == "undefined")
            {
                obj.LoginUserID = 0;
                userid = Convert.ToInt32(Session["OrgUserId"]);
                usertype = Convert.ToInt32(Session["OrguserType"]);
                flag = true;
            }
            else
            {
                userid = Convert.ToInt32(dataNeedsOf);
                obj.LoginUserID = userid;
                var userdetails = UserBM.GetUserByIdSPL(userid).FirstOrDefault();
                if (userdetails != null) usertype = userdetails.UserType;
            }



           
            obj.skillIsActive = true;
            obj.skillIsDeleted = false;
            obj.skillUserId = userid;
            obj.skillCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
            obj.temp = val;
            obj.mulsel = mul;
            obj.TargetType = TargetType;
            obj.skillTeamID = TeamID;
            //Check user is Manager or employee.
            if (usertype == 2) //Manager
            {
                if (flag == true)
                {
                    obj.SpEmpAvgSkillByManager();
                }
                else
                {
                    obj.mulselrecord();
                }
               // obj.SpEmpAvgSkillByManager();
            }
            else if (usertype == 3) //Employee
            {
                obj.mulselrecord();
                //obj.GetSkillByUserId();
            }
            else //Company
            {
                obj.skillUserId = 0;
                obj.SpEmpAvgSkillByManager();
            }
            DataSet ds = obj.ds;
            var mylist = "[ ";

            string mylist1 = "[ ";
            string mylist2 = "[ ";
            string mylist3 = "[ ";
            string mylist4 = "[ ";

            string mylist5 = "[ ";

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    String competences = null;

                    int i;
                    for (i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                    {
                        if (Convert.ToString(Session["Culture"]) == "Danish")
                        {
                            competences = Convert.ToString(ds.Tables[0].Rows[i]["comCompetenceDN"]);
                        }
                        else
                        {
                            competences = Convert.ToString(ds.Tables[0].Rows[i]["comCompetence"]);
                        }
                        String local = Convert.ToString(ds.Tables[0].Rows[i]["skillLocal"]);
                        String achive = Convert.ToString(ds.Tables[0].Rows[i]["skillAchive"]);
                        String target = Convert.ToString(ds.Tables[0].Rows[i]["skilltarget"]);
                        String mv = Convert.ToString(ds.Tables[0].Rows[i]["maxval"]);

                        String mvSet = Convert.ToString(ds.Tables[0].Rows[i]["MaxValueSet"]);

                        //mylist += local + "," + competences + "], [";

                        local = local.Replace(',', '.');
                        achive = achive.Replace(',', '.');
                        target = target.Replace(',', '.');
                        mv = mv.Replace(',', '.');

                        if (flag == false)
                        {
                            mvSet = mvSet.Replace(',', '.');
                        }
                        else
                        {
                            mvSet = mv.Replace(',', '.');
                        }

                        mylist1 += string.Format("[\"{1}\",{0}],", local, competences);
                        mylist2 += string.Format("[\"{1}\",{0}],", achive, competences);
                        mylist3 += string.Format("[\"{1}\",{0}],", target, competences);
                        mylist4 += string.Format("{0},", mv);

                        mylist5 += string.Format("{0},", mvSet);
                        /*switch (dataNeedsOf)
                        {
                            case "local":
                                mylist1 += string.Format("[\"{1}\",{0}],", local, competences);
                                break;
                            case "achive":
                                mylist2 += string.Format("[\"{1}\",{0}],", achive, competences);
                                break;
                            case "target":
                                mylist3 += string.Format("[\"{1}\",{0}],", target, competences);
                                break;
                        }*/
                        /*listCompetence.Add(local + ",\"" + competences + "\"], [");*/
                    }


                    mylist1 = mylist1.Remove(mylist1.Length - 1);
                    mylist2 = mylist2.Remove(mylist2.Length - 1);
                    mylist3 = mylist3.Remove(mylist3.Length - 1);
                    mylist4 = mylist4.Remove(mylist4.Length - 1);

                    mylist5 = mylist5.Remove(mylist5.Length - 1);

                    mylist1 += "]";
                    mylist2 += "]";
                    mylist3 += "]";
                    mylist4 += "]";

                    mylist5 += "]";

                    mylist += "]";


                }
            }

            listCompetence.Add(mylist1);
            listCompetence.Add(mylist2);
            listCompetence.Add(mylist3);
            listCompetence.Add(mylist4);

            listCompetence.Add(mylist5);
            // return listCompetence;
           // ExceptionLogger.LogException(new Exception(), 0, "listCompetence " + Newtonsoft.Json.JsonConvert.SerializeObject(listCompetence));

        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "WebService1.cs->GetComList");
        }
        return listCompetence;
    }



    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public List<string> GetComListManager(string dataNeedsOf, int val, string mul, int TargetType = 1,int TeamID=0)
    {
        var userId = HttpContext.Current.Request.QueryString["umId"];
        var userid = 0;
        var usertype = 3;
        var listCompetence = new List<string>();
        bool flag = false;
        try
        {
            EmployeeSkillBM obj = new EmployeeSkillBM();

            if (string.IsNullOrWhiteSpace(dataNeedsOf) || dataNeedsOf == "undefined")
            {
                obj.LoginUserID = 0;
                userid = Convert.ToInt32(Session["OrgUserId"]);
                usertype = Convert.ToInt32(Session["OrguserType"]);
                flag = true;
            }
            else
            {
                userid = Convert.ToInt32(dataNeedsOf);
                obj.LoginUserID = userid;
                var userdetails = UserBM.GetUserByIdSPL(userid).FirstOrDefault();
                if (userdetails != null) usertype = userdetails.UserType;
            }



           
            obj.skillIsActive = true;
            obj.skillIsDeleted = false;
            obj.skillUserId = userid;
            obj.skillCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);
            obj.temp = val;
            obj.mulsel = mul;
            obj.TargetType = TargetType;

            obj.skillTeamID = TeamID;
            //Check user is Manager or employee.
            if (usertype == 2) //Manager
            {
                obj.SpEmpAvgSkillByManager();
            }
            else if (usertype == 3) //Employee
            {
                obj.mulselrecord();
                //obj.GetSkillByUserId();
            }
            else //Company
            {
                obj.skillUserId = 0;
                obj.SpEmpAvgSkillByManager();
            }

            DataSet ds = obj.ds;
            var mylist = "[ ";

            string mylist1 = "[ ";
            string mylist2 = "[ ";
            string mylist3 = "[ ";
            string mylist4 = "[ ";

            string mylist5 = "[ ";

            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    String competences = null;

                    int i;
                    for (i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                    {
                        if (Convert.ToString(Session["Culture"]) == "Danish")
                        {
                            competences = Convert.ToString(ds.Tables[0].Rows[i]["comCompetenceDN"]);
                        }
                        else
                        {
                            competences = Convert.ToString(ds.Tables[0].Rows[i]["comCompetence"]);
                        }
                        String local = Convert.ToString(ds.Tables[0].Rows[i]["skillLocal"]);
                        String achive = Convert.ToString(ds.Tables[0].Rows[i]["skillAchive"]);
                        String target = Convert.ToString(ds.Tables[0].Rows[i]["skilltarget"]);
                        String mv = Convert.ToString(ds.Tables[0].Rows[i]["maxval"]);

                        String mvSet = Convert.ToString(ds.Tables[0].Rows[i]["MaxValueSet"]);
                        //mylist += local + "," + competences + "], [";

                        local = local.Replace(',', '.');
                        achive = achive.Replace(',', '.');
                        target = target.Replace(',', '.');
                        mv = mv.Replace(',', '.');

                       // mvSet = mvSet.Replace(',', '.');
                        if (flag == false)
                        {
                            mvSet = mvSet.Replace(',', '.');
                        }
                        else
                        {
                            mvSet = mv.Replace(',', '.');
                        }

                        mylist1 += string.Format("[\"{1}\",{0}],", local, competences);
                        mylist2 += string.Format("[\"{1}\",{0}],", achive, competences);
                        mylist3 += string.Format("[\"{1}\",{0}],", target, competences);
                        mylist4 += string.Format("{0},", mv);

                        mylist5 += string.Format("{0},", mvSet);
                        /*switch (dataNeedsOf)
                        {
                            case "local":
                                mylist1 += string.Format("[\"{1}\",{0}],", local, competences);
                                break;
                            case "achive":
                                mylist2 += string.Format("[\"{1}\",{0}],", achive, competences);
                                break;
                            case "target":
                                mylist3 += string.Format("[\"{1}\",{0}],", target, competences);
                                break;
                        }*/
                        /*listCompetence.Add(local + ",\"" + competences + "\"], [");*/
                    }


                    mylist1 = mylist1.Remove(mylist1.Length - 1);
                    mylist2 = mylist2.Remove(mylist2.Length - 1);
                    mylist3 = mylist3.Remove(mylist3.Length - 1);
                    mylist4 = mylist4.Remove(mylist4.Length - 1);

                    mylist5 = mylist5.Remove(mylist5.Length - 1);

                    mylist1 += "]";
                    mylist2 += "]";
                    mylist3 += "]";
                    mylist4 += "]";

                    mylist5 += "]";

                    mylist += "]";


                }
            }

            listCompetence.Add(mylist1);
            listCompetence.Add(mylist2);
            listCompetence.Add(mylist3);
            listCompetence.Add(mylist4);

            listCompetence.Add(mylist5);
            // return listCompetence;
            // ExceptionLogger.LogException(new Exception(), 0, "listCompetence " + Newtonsoft.Json.JsonConvert.SerializeObject(listCompetence));

        }
        catch (Exception ex)
        {
            ExceptionLogger.LogException(ex, 0, "WebService1.cs->GetComList");
        }
        return listCompetence;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public List<string> GetTotalAlpPlpGroupByDateByUserId(string dataNeedsOf,int TeamID)
    {
        //Check usertype: 
        //if user is manager then get records from competences
        var usertype = Convert.ToInt32(Session["OrguserType"]);
        if (usertype == 3)
        {
            var listAlpPlp = new List<string>();
            LogMasterLogic lm = new LogMasterLogic();
            lm.LogIsActive = true;
            lm.LogIsDeleted = false;
            lm.LogUserId = Convert.ToInt32(Session["OrgUserId"]);
            lm.TeamID = TeamID;
            lm.GetTotalAlpPlpGroupByDateByUserId();
            var myAlpPlpList = "[ ";
            string myALP = "[ ";
            string myPLP = "[ ";
            DataSet ds1 = lm.ds;
            if (ds1 != null)
            {
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    String myDate = null;

                    int i;
                    for (i = 0; i <= ds1.Tables[0].Rows.Count - 1; i++)
                    {
                        myDate = Convert.ToString(ds1.Tables[0].Rows[i]["logDate"]);
                        String alp = Convert.ToString(ds1.Tables[0].Rows[i]["ALP"]);
                        String plp = Convert.ToString(ds1.Tables[0].Rows[i]["PLP"]);

                        myALP += string.Format("[\"{1}\",{0}],", alp, myDate);
                        myPLP += string.Format("[\"{1}\",{0}],", plp, myDate);
                    }
                    myALP = myALP.Remove(myALP.Length - 1);
                    myPLP = myPLP.Remove(myPLP.Length - 1);

                    myALP += "]";
                    myPLP += "]";

                    myAlpPlpList += "]";
                }
            }

            listAlpPlp.Add(myALP);
            listAlpPlp.Add(myPLP);


            return listAlpPlp;
        }
        if (usertype == 2 || usertype == 3)
        {

            var listAlpPlp = new List<string>();
            LogMasterLogic lm = new LogMasterLogic();
            lm.LogIsActive = true;
            lm.LogIsDeleted = false;
            lm.LogUserId = Convert.ToInt32(Session["OrgUserId"]);
            lm.TeamID = TeamID;
            lm.GetAVGofAlpPlpGroupByDateByManagerId();
            var myAlpPlpList = "[ ";
            string myALP = "[ ";
            string myPLP = "[ ";
            DataSet ds1 = lm.ds;
            if (ds1 != null)
            {
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    String myDate = null;

                    int i;
                    for (i = 0; i <= ds1.Tables[0].Rows.Count - 1; i++)
                    {
                        myDate = Convert.ToString(ds1.Tables[0].Rows[i]["logDate"]);
                        String alp = Convert.ToString(ds1.Tables[0].Rows[i]["ALP"]);
                        String plp = Convert.ToString(ds1.Tables[0].Rows[i]["PLP"]);

                        myALP += string.Format("[\"{1}\",{0}],", alp, myDate);
                        myPLP += string.Format("[\"{1}\",{0}],", plp, myDate);
                    }
                    myALP = myALP.Remove(myALP.Length - 1);
                    myPLP = myPLP.Remove(myPLP.Length - 1);

                    myALP += "]";
                    myPLP += "]";

                    myAlpPlpList += "]";
                }
                else
                {
                    myALP += "]";
                    myPLP += "]";

                    myAlpPlpList += "]";
                }
            }
            else
            {
                myALP += "]";
                myPLP += "]";

                myAlpPlpList += "]";
            }

            listAlpPlp.Add(myALP);
            listAlpPlp.Add(myPLP);


            return listAlpPlp;
        }
        return null;

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public List<string> GetTotalAlpPlpGroupByMonthByUserId(string dataNeedsOf)
    {
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Convert.ToString(HttpContext.Current.Session["Language"]));
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(Convert.ToString(HttpContext.Current.Session["Language"]));
        //Check usertype: 
        //if user is manager then get records from competences
        var userid = 0;
        var usertype = 3;

        string JanRes = Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Janres1.Text"));
        string FebRes = Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Febres1.Text"));
        string MarRes = Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Marres1.Text"));
        string AprRes = Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Aprres1.Text"));
        string MayRes = Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Mayres1.Text"));
        string JunRes = Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Junres1.Text"));
        string JulRes = Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Julres1.Text"));
        string AugRes = Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Augres1.Text"));
        string SepRes = Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Sepres1.Text"));
        string OctRes = Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Octres1.Text"));
        string NovRes = Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Novres1.Text"));
        string DecRes = Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Decres1.Text"));

        string myDateNew = null;
        if (string.IsNullOrWhiteSpace(dataNeedsOf))
        {
            userid = Convert.ToInt32(Session["OrgUserId"]);
            usertype = Convert.ToInt32(Session["OrguserType"]);
        }
        else
        {
            userid = Convert.ToInt32(dataNeedsOf);
            var userdetails = UserBM.GetUserByIdSPL(userid).FirstOrDefault();
            if (userdetails != null) usertype = userdetails.UserType;
        }

        if (usertype == 3 || usertype == 2)
        {
            var listAlpPlp = new List<string>();
            LogMasterLogic lm = new LogMasterLogic();
            lm.LogIsActive = true;
            lm.LogIsDeleted = false;
            lm.LogUserId = Convert.ToInt32(userid);
            lm.GetTotalAlpPlpGroupByMonthByUserId();
            var myAlpPlpList = "[ ";
            string myALP = "[ ";
            string myPLP = "[ ";
            DataSet ds1 = lm.ds;
            if (ds1 != null)
            {
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    String myDate = null;
                    

                    int i;
                    for (i = 0; i <= ds1.Tables[0].Rows.Count - 1; i++)
                    {
                        myDate = Convert.ToString(ds1.Tables[0].Rows[i]["logMonth"]);
                        myDateNew = myDate.Replace("Jan", JanRes).Replace("Feb", FebRes).Replace("Mar", MarRes).Replace("Apr", AprRes).Replace("May", MayRes).Replace("Jun", JunRes).Replace("Jul", JulRes).Replace("Aug", AugRes).Replace("Sep",SepRes).Replace("Oct", OctRes).Replace("Nov", NovRes).Replace("Dec", DecRes);
//                       
                       
                                   
                                       
                        String alp = Convert.ToString(ds1.Tables[0].Rows[i]["ALP"]);
                        String plp = Convert.ToString(ds1.Tables[0].Rows[i]["PLP"]);

                        myALP += string.Format("[\"{1}\",{0}],", alp, myDateNew);
                        myPLP += string.Format("[\"{1}\",{0}],", plp, myDateNew);
                    }
                    myALP = myALP.Remove(myALP.Length - 1);
                    myPLP = myPLP.Remove(myPLP.Length - 1);

                    myALP += "]";
                    myPLP += "]";

                    myAlpPlpList += "]";
                }
            }

            listAlpPlp.Add(myALP);
            listAlpPlp.Add(myPLP);


            return listAlpPlp;
        }
        if (usertype == 2 || usertype == 3)
        {
            return null;
            var listAlpPlp = new List<string>();
            LogMasterLogic lm = new LogMasterLogic();
            lm.LogIsActive = true;
            lm.LogIsDeleted = false;
            lm.LogUserId = Convert.ToInt32(Session["OrgUserId"]);
            lm.GetAVGofAlpPlpGroupByDateByManagerId();
            var myAlpPlpList = "[ ";
            string myALP = "[ ";
            string myPLP = "[ ";
            DataSet ds1 = lm.ds;
            if (ds1 != null)
            {
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    String myDate = null;

                    int i;
                    for (i = 0; i <= ds1.Tables[0].Rows.Count - 1; i++)
                    {
                        myDate = Convert.ToString(ds1.Tables[0].Rows[i]["logDate"]);
                        String alp = Convert.ToString(ds1.Tables[0].Rows[i]["ALP"]);
                        String plp = Convert.ToString(ds1.Tables[0].Rows[i]["PLP"]);

                        myALP += string.Format("[\"{1}\",{0}],", alp, myDate);
                        myPLP += string.Format("[\"{1}\",{0}],", plp, myDate);
                    }
                    myALP = myALP.Remove(myALP.Length - 1);
                    myPLP = myPLP.Remove(myPLP.Length - 1);

                    myALP += "]";
                    myPLP += "]";

                    myAlpPlpList += "]";
                }
            }

            listAlpPlp.Add(myALP);
            listAlpPlp.Add(myPLP);


            return listAlpPlp;
        }
        return null;

    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public List<string> GetPDPpointByUserIdByMonth(string dataNeedsOf,int TeamID=0)
    {
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(Convert.ToString(HttpContext.Current.Session["Language"]));
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(Convert.ToString(HttpContext.Current.Session["Language"]));

        string JanRes = Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Janres1.Text"));
        string FebRes = Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Febres1.Text"));
        string MarRes = Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Marres1.Text"));
        string AprRes = Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Aprres1.Text"));
        string MayRes = Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Mayres1.Text"));
        string JunRes = Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Junres1.Text"));
        string JulRes = Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Julres1.Text"));
        string AugRes = Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Augres1.Text"));
        string SepRes = Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Sepres1.Text"));
        string OctRes = Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Octres1.Text"));
        string NovRes = Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Novres1.Text"));
        string DecRes = Convert.ToString(HttpContext.GetLocalResourceObject("~/Organisation/PersonalDevelopmentPlan.aspx", "Decres1.Text"));

        var userid = 0;
        var usertype = 3;
        if (string.IsNullOrWhiteSpace(dataNeedsOf))
        {
            userid = Convert.ToInt32(Session["OrgUserId"]);
            usertype = Convert.ToInt32(Session["OrguserType"]);
        }
        else
        {
            userid = Convert.ToInt32(dataNeedsOf);
            var userdetails = UserBM.GetUserByIdSPL(userid).FirstOrDefault();
            if (userdetails != null) usertype = userdetails.UserType;
        }


        var listCompetence = new List<string>();
        SubCompetenceBM obj = new SubCompetenceBM();
        obj.subIsActive = true;
        obj.subIsDeleted = false;
        //Check user is Manager or employee.
        if (usertype == 2) //Manager
        {

            obj.GetPDPpointByManagerId(userid,TeamID);

        }
        else if (usertype == 3) // emp
        {

            obj.GetPDPpointByUserIdByMonth(userid);
        }

        DataSet ds = obj.ds;

        string mylist1 = "[ ";

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                String competences = null;
                String competencesNew = null;

                int i;
                for (i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    competences = Convert.ToString(ds.Tables[0].Rows[i]["monthName"]);

                    competencesNew = competences.Replace("Jan",JanRes).Replace("Feb", FebRes).Replace("Mar", MarRes).Replace("Apr", AprRes).Replace("May", MayRes).Replace("Jun", JunRes).Replace("Jul", JulRes).Replace("Aug", AugRes).Replace("Sep", SepRes).Replace("Oct", OctRes).Replace("Nov", NovRes).Replace("Dec", DecRes);



                    String local = Convert.ToString(ds.Tables[0].Rows[i]["PDPTotal"]);

                    mylist1 += string.Format("[\"{1}\",{0}],", local, competencesNew);

                }
                mylist1 = mylist1.Remove(mylist1.Length - 1);

                mylist1 += "]";
            }
        }


        listCompetence.Add(mylist1);

        return listCompetence;
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public List<string> GetPDPpointByManagerId(string dataNeedsOf,Int32 TeamID)
    {
        var userid = 0;
        var usertype = 3;
        if (string.IsNullOrWhiteSpace(dataNeedsOf))
        {
            userid = Convert.ToInt32(Session["OrgUserId"]);
            usertype = Convert.ToInt32(Session["OrguserType"]);
        }
        else
        {
            userid = Convert.ToInt32(dataNeedsOf);
            var userdetails = UserBM.GetUserByIdSPL(userid).FirstOrDefault();
            if (userdetails != null) usertype = userdetails.UserType;
        }


        var listCompetence = new List<string>();
        SubCompetenceBM obj = new SubCompetenceBM();
        //Check user is Manager or employee.
        if (usertype == 2) //Manager
        {
            obj.subIsActive = true;
            obj.subIsDeleted = false;
            obj.GetPDPpointByManagerId(userid, TeamID);

        }
        else
        {
            return null;
        }

        DataSet ds = obj.ds;

        string mylist1 = "[ ";

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                String competences = null;

                int i;
                for (i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                {
                    competences = Convert.ToString(ds.Tables[0].Rows[i]["name"]);
                    String local = Convert.ToString(ds.Tables[0].Rows[i]["PDPTotal"]);

                    mylist1 += string.Format("[\"{1}\",{0}],", local, competences);

                }
                mylist1 = mylist1.Remove(mylist1.Length - 1);

                mylist1 += "]";
            }
        }


        listCompetence.Add(mylist1);

        return listCompetence;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public List<JsonView> GetSkillByUserIdByMonth(string dataNeedsOf)
    {
        var userid = 0;
        var usertype = 3;
        if (string.IsNullOrWhiteSpace(dataNeedsOf))
        {
            userid = Convert.ToInt32(Session["OrgUserId"]);
            usertype = Convert.ToInt32(Session["OrguserType"]);
        }
        else
        {
            userid = Convert.ToInt32(dataNeedsOf);
            var userdetails = UserBM.GetUserByIdSPL(userid).FirstOrDefault();
            if (userdetails != null) usertype = userdetails.UserType;
        }


        var listCompetence = new List<string>();
        EmployeeSkillBM obj = new EmployeeSkillBM();
        obj.skillIsActive = true;
        obj.skillIsDeleted = false;
        obj.skillUserId = userid;
        obj.skillCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);

        //Check user is Manager or employee.
        if (usertype == 2) //Manager
        {
            obj.GetEmpAvgSkillByManager();


        }
        else if (usertype == 3) //Employee
        {
            obj.GetSkillByUserId();
        }
        else //Company
        {
            obj.skillUserId = 0;
            obj.GetEmpAvgSkillByManager();
        }
        DataSet ds = obj.ds;

        /*DataTable tblMEN = ds.Tables[0];
        DataRow[] results = ds.Select("skillMonth = 'zz'");*/


        DataView dv = new DataView();
        dv = ds.Tables[0].DefaultView;
        // dv.RowFilter = ("skillMonth!=null =  '" + Convert.ToInt32(Session["OrgCompanyId"]) + "'");



        var dataLocal = new List<int>();
        var dataAchive = new List<int>();
        var dataTarget = new List<int>();
        var dataMonth = new List<string>();
        var returnList = new List<JsonView>();
        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {

                DataTable dt = ds.Tables[0];

                foreach (DataRow row in dt.Rows)
                {

                    foreach (DataColumn column in dt.Columns)
                    {
                        var colNm = column.ColumnName;
                        var data = row[column].ToString();
                        if (colNm == "skilltarget")
                        {
                            dataTarget.Add(Convert.ToInt32(row[column]));
                        }
                        else if (colNm == "skillAchive")
                        {
                            dataAchive.Add(Convert.ToInt32(row[column]));
                        }
                        else if (colNm == "skillLocal")
                        {
                            dataLocal.Add(Convert.ToInt32(row[column]));
                        }
                        if (colNm == "skillMonth")
                        {
                            if (!dataMonth.Any(str => str.Contains(Convert.ToString(row[column]))))
                            {
                                dataMonth.Add(Convert.ToString(row[column]));
                            }
                        }
                    }
                }
                returnList.Add(GetIntoList("Target", dataTarget));
                returnList.Add(GetIntoList("Basic", dataLocal));
                returnList.Add(GetIntoList("Actual", dataAchive));
                return returnList;
            }
        }
        return returnList;
    }
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public List<string> GetSkillByUserIdGetMonth(string dataNeedsOf)
    {
        var userid = 0;
        var usertype = 3;
        if (string.IsNullOrWhiteSpace(dataNeedsOf))
        {
            userid = Convert.ToInt32(Session["OrgUserId"]);
            usertype = Convert.ToInt32(Session["OrguserType"]);
        }
        else
        {
            userid = Convert.ToInt32(dataNeedsOf);
            var userdetails = UserBM.GetUserByIdSPL(userid).FirstOrDefault();
            if (userdetails != null) usertype = userdetails.UserType;
        }



        EmployeeSkillBM obj = new EmployeeSkillBM();
        obj.skillIsActive = true;
        obj.skillIsDeleted = false;
        obj.skillUserId = userid;
        obj.skillCompanyId = Convert.ToInt32(HttpContext.Current.Session["OrgCompanyId"]);

        //Check user is Manager or employee.
        if (usertype == 2) //Manager
        {
            obj.GetEmpAvgSkillByManager();


        }
        else if (usertype == 3) //Employee
        {
            obj.GetSkillByUserId();
        }
        else //Company
        {
            obj.skillUserId = 0;
            obj.GetEmpAvgSkillByManager();
        }
        DataSet ds = obj.ds;



        DataView dv = new DataView();



        var dataMonth = new List<string>();
        var returnList = new List<JsonView>();
        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {

                DataTable dt = ds.Tables[0];

                foreach (DataRow row in dt.Rows)
                {

                    foreach (DataColumn column in dt.Columns)
                    {
                        var colNm = column.ColumnName;

                        if (colNm == "skillMonth")
                        {
                            if (!dataMonth.Any(str => str.Contains(Convert.ToString(row[column]))))
                            {
                                dataMonth.Add(Convert.ToString(row[column]));
                            }
                        }
                    }
                }

                return dataMonth;
            }
        }
        return dataMonth;
    }

    private JsonView GetIntoList(string name, List<int> dataList)
    {
        var view = new JsonView();
        view.name = name;
        view.data = dataList;
        return view;
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public List<string> getNotificationPopUP(String txtName)
    {
        var userlist = new List<String>();
        NotificationBM obj = new NotificationBM();
        obj.notPopUpStatus = true;
        obj.notIsActive = true;
        obj.userCreateBy = Convert.ToString(HttpContext.Current.Session["OrgUserId"]);
        obj.GetAllPopUPNotification();
        DataSet ds = obj.ds;


        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                obj.notId = Convert.ToInt32(ds.Tables[0].Rows[0]["notId"]);
                obj.notPopUpStatus = false;
                // obj.UpdateNotificationbyid();

                NotificationBM.UpdateNotificationPopup(obj.notId);



                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["name"])))
                {
                    userlist.Add(Convert.ToString(ds.Tables[0].Rows[0]["name"]));
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["notsubject"])))
                {
                    userlist.Add(Convert.ToString(ds.Tables[0].Rows[0]["notsubject"]));
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["nottype"])))
                {
                    userlist.Add(Convert.ToString(ds.Tables[0].Rows[0]["nottype"]));
                }

                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userImage"])))
                {
                    String Image = "../Log/upload/Userimage/" + Convert.ToString(ds.Tables[0].Rows[0]["userImage"]);
                    userlist.Add(Image);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["notpage"])))
                {
                    userlist.Add(Convert.ToString(ds.Tables[0].Rows[0]["notpage"]));
                }

                return userlist;
            }
        }
        return userlist;
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public List<string> getNotificationPopUPmanager(String txtName)
    {
        var userlist = new List<String>();

        //NotificationBM obj = new NotificationBM();
        //obj.notIsActive = true;
        //obj.notIsDeleted = false;
        //obj.userCreateBy = Convert.ToString(Session["OrgUserId"]);
        //obj.GetAllNotification();
        //DataSet ds = obj.ds;

        NotificationBM obj = new NotificationBM();
        obj.notPopUpStatus = true;
        obj.notIsActive = true;
        obj.userCreateBy = Convert.ToString(HttpContext.Current.Session["OrgUserId"]);
        obj.GetAllPopUPNotification();
        DataSet ds = obj.ds;
        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                obj.notId = Convert.ToInt32(ds.Tables[0].Rows[0]["notId"]);
                obj.notPopUpStatus = false;
                //obj.UpdateNotificationbyid();

                NotificationBM.UpdateNotificationPopup(obj.notId);
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["name"])))
                {
                    userlist.Add(Convert.ToString(ds.Tables[0].Rows[0]["name"]));
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["notsubject"])))
                {
                    userlist.Add(Convert.ToString(ds.Tables[0].Rows[0]["notsubject"]));
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["nottype"])))
                {
                    userlist.Add(Convert.ToString(ds.Tables[0].Rows[0]["nottype"]));
                }

                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userImage"])))
                {
                    String Image = "../Log/upload/Userimage/" + Convert.ToString(ds.Tables[0].Rows[0]["userImage"]);
                    userlist.Add(Image);
                }
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["notpage"])))
                {
                    userlist.Add(Convert.ToString(ds.Tables[0].Rows[0]["notpage"]));
                }

                return userlist;
            }
        }
        return userlist;
    }
    /*Check duplicate email of user |Saurin | 2015 03 01*/
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string UserEmailCheckDuplication(string email)
    {
        try
        {


            UserBM obj2 = new UserBM();
            obj2.useremailCheckDuplication(email, -1);
            String returnMsg = obj2.ReturnString;
            if (returnMsg == "")
            {
            }
            else
            {
                if (returnMsg == "userEmail")
                    return CommonModule.msgEmailAlreadyExists;
                else
                {
                    return "Success";
                }

            }
        }
        catch (Exception)
        {

            throw;
        }
        return "Success";
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string AddNewUser(string name, string email, string phoneNumber, string address, int ddlDepartment_SelectedValue)
    {
        try
        {
            string filePath = "";
            UserBM obj2 = new UserBM();
            obj2.useremailCheckDuplication(email, -1);
            String returnMsg = obj2.ReturnString;
            if (returnMsg == "")
            {

                UserBM obj = new UserBM();
                obj.userFirstName = name;
                obj.userLastName = "";
                obj.userZip = "";
                obj.userAddress = address;
                obj.userCountryId = Convert.ToInt32(0);
                obj.userStateId = Convert.ToInt32(0);
                //obj.userCityId = Convert.ToInt32(0);
                obj.userCityId = "";
                obj.usercontact = phoneNumber;
                obj.userEmail = email;
                obj.userType = 2;
                obj.userCreatedDate = DateTime.Now;
                obj.userIsActive = true;
                obj.userIsDeleted = false;
                obj.userCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
                string pass = CommonModule.Generate_Random_password();
                obj.userPassword = CommonModule.encrypt(pass.Trim());
                obj.userGender = "";
                obj.userImage = "ofile_img.png";
                obj.userLevel = 1;
                obj.userdepId = "";
                obj.userJobType = 0;
                obj.userdepId = Convert.ToString(ddlDepartment_SelectedValue); //Saurin :20150429
                obj.InsertOrganisation();
                if (obj.ReturnBoolean == true)
                {
                    CommonModule.sendmail(email, name, pass);
                    //  GetAllEmployeeList();
                }
            }
            else
            {
                if (returnMsg == "userEmail")
                {
                    return CommonModule.msgEmailAlreadyExists; ;
                }
                return CommonModule.msgProblemInsertRecord;
                // lblMsg.Text = CommonModule.msgEmailAlreadyExists;
                //else
                // lblMsg.Text = CommonModule.msgProblemInsertRecord;
            }
        }
        catch (Exception)
        {

            throw;
        }
        return "success";

    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string GetAllUserByType(int userTypeId)
    {
        try
        {
            UserBM obj = new UserBM();
            obj.userIsActive = true;
            obj.userIsDeleted = false;
            obj.userType = userTypeId;
            obj.userCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.userCreateBy = Convert.ToString(Session["OrgUserId"]); ;
            obj.GetAllEmployee();
            DataSet ds = obj.ds;
            return CommonModule.ConverTableToJson(ds);
        }
        catch (Exception ex)
        {

            ExceptionLogger.LogException(ex);
        }
        return "";
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string GetAllUserByType_Team(int userTypeId, int TeamID)
    {
        try
        {
            UserBM obj = new UserBM();
            obj.userIsActive = true;
            obj.userIsDeleted = false;
            obj.userType = userTypeId;
            obj.userCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.userCreateBy = Convert.ToString(Session["OrgUserId"]);
            obj.TeamID = TeamID;
            obj.GetUserByType_TeamID();
            DataSet ds = obj.ds;
            return CommonModule.ConverTableToJson(ds);
        }
        catch (Exception ex)
        {

            ExceptionLogger.LogException(ex);
        }
        return "";
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public List<string> notification_Count(String txtName)
    {
        var Countlist = new List<String>();

        NotificationBM obj = new NotificationBM();
        obj.notIsActive = true;
        obj.notIsDeleted = false;
        obj.userCreateBy = Convert.ToString(Session["OrgUserId"]);
        obj.GetAllNotification();
        DataSet ds = obj.ds;
        if (ds != null)
        {
            if (ds.Tables[2].Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[2].Rows[0]["count1"])))
                {
                    Countlist.Add(Convert.ToString(ds.Tables[2].Rows[0]["count1"]));
                }
                else
                {
                    Countlist.Add(Convert.ToString("0"));
                }
            }
            else
            {
                Countlist.Add(Convert.ToString("0"));
            }
            if (ds.Tables[1].Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[1].Rows[0]["count1"])))
                {
                    Countlist.Add(Convert.ToString(ds.Tables[1].Rows[0]["count1"]));
                }
                else
                {
                    Countlist.Add(Convert.ToString("0"));
                }
            }
            else
            {
                Countlist.Add(Convert.ToString("0"));
            }
            return Countlist;
        }
        return Countlist;
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public List<string> rep_notificationAll(String txtName)
    {
        var notificationAll = new List<String>();
        try
        {
            NotificationBM obj = new NotificationBM();
            obj.notIsActive = true;
            obj.notIsDeleted = false;
            obj.userCreateBy = Convert.ToString(Session["OrgUserId"]);
            obj.GetAllNotification();
            DataSet ds = obj.ds;
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataView dv = new DataView();
                    dv = ds.Tables[0].DefaultView;
                    String messge = "Message";
                    dv.RowFilter = ("nottype <>  '" + messge + "'");
                    DataTable dtitm = dv.ToTable();
                    DataSet ds1 = new DataSet();
                    ds1.Tables.Add(dtitm);
                    if (ds1.Tables[0].Rows.Count > 0)
                    {
                        notificationAll.Add(CommonModule.ConverTableToJson(ds1));
                    }
                    else
                    { notificationAll.Add("0"); }

                    DataView dv1 = new DataView();
                    dv1 = ds.Tables[0].DefaultView;
                    dv1.RowFilter = ("nottype =  '" + messge + "'");
                    DataTable dtitm1 = dv1.ToTable();
                    DataSet ds2 = new DataSet();
                    ds2.Tables.Add(dtitm1);
                    if (ds2.Tables[0].Rows.Count > 0)
                    {
                        notificationAll.Add(CommonModule.ConverTableToJson(ds2));
                    }
                    else
                    { notificationAll.Add("0"); }

                }
            }

            return notificationAll;
        }
        catch (Exception)
        {

            throw;
        }
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public List<string> rep_notificationAll_manager(String txtName)
    {
        var notificationAll = new List<String>();
        try
        {
            NotificationBM obj = new NotificationBM();
            obj.notIsActive = true;
            obj.notIsDeleted = false;
            obj.userCreateBy = Convert.ToString(Session["OrgUserId"]);
            obj.GetAllNotification();
            DataSet ds = obj.ds;
            if (ds != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    notificationAll.Add(CommonModule.ConverTableToJson(ds));
                }
                else
                {
                    notificationAll.Add("0");
                }
            }
            else
            {
                notificationAll.Add("0");
            }

            return notificationAll;
        }
        catch (Exception)
        {

            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string GetAllEmployeecreatebymanager(int userTypeId)
    {
        try
        {
            UserBM obj = new UserBM();
            obj.userIsActive = true;
            obj.userIsDeleted = false;
            obj.userType = userTypeId;
            obj.userCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
            obj.userCreateBy = Convert.ToString(Session["OrgUserId"]);
            obj.GetAllEmployeecreatebymanager();
            DataSet ds = obj.ds;
            return CommonModule.ConverTableToJson(ds);
        }
        catch (Exception)
        {

            throw;
        }
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string GetAllResourceLanguage()
    {
        try
        {
            ResourceLanguageBM obj = new ResourceLanguageBM();
            obj.GetAllResourceLanguage();
            DataSet ds = obj.ds;
            return CommonModule.ConverTableToJson(ds);
        }
        catch (Exception)
        {

            throw;
        }
    }

}
