﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default2.aspx.cs" Inherits="Default2" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="https://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <div class="row">
        <div class="col-md-12">
            <div class="widget box">
                <div class="widget-header">
                    <h4>
                        <i class="icon-reorder"></i>Update Resource</h4>
                    <div class="toolbar no-padding">
                        <div class="btn-group">
                            <span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
                        </div>
                    </div>
                </div>
                <div style="font-size: 14pt; width: 280px; font-family: Verdana; height: 150px; border-right: #ff33ff thin solid;
                    border-top: #ff33ff thin solid; vertical-align: middle; border-left: #ff33ff thin solid;
                    border-bottom: #ff33ff thin solid; color: #0000ff;">
                    <asp:Label ID="Label1" runat="server"></asp:Label>:<asp:TextBox ID="txtResourceValue"
                        runat="server"></asp:TextBox><br />
                    <br />
                    <asp:Button ID="Button1" runat="server" Text="Update" OnClick="Button1_Click" />
                    <asp:Label ID="Label2" runat="server"></asp:Label><br />
                    <br />
                    <a href="UpdateResource.aspx">Back</a>
                </div>
            </div>
        </div>
    </div>
    </div>




    </form>
</body>
</html>
