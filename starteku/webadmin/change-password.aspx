﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webadmin/AdminMasterPage.master" AutoEventWireup="true" CodeFile="change-password.aspx.cs" Inherits="webadmin_change_password" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<div class="row">
<asp:Label ID="lblMsg" runat="server"></asp:Label>
        <div class="col-md-12">
            <div class="col-md-350width">
                <div class="indicatesRequireFiled">
                    * Indicates required field
                </div>
                <div class="widget box">
                    <div class="widget-header">
                        <h4>
                            <i class="icon-reorder"></i>
                            <asp:Label runat="server" ID="lblDataDisplayTitle"></asp:Label>
                        </h4>
                    </div>
                    <div class="widget-content">
                        <%--   <form class="form-horizontal row-border" action="#">--%>
                        <div class="form-horizontal row-border">
                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                    Old Password:<span class="starValidation">*</span></label>
                                <div class="col-md-10">
                                    <asp:TextBox ID="txtOldPassword" type="text" class="form-control" runat="server"
                                        MaxLength="20" TextMode="Password" />
                                    <asp:RequiredFieldValidator ID="rfvEmail" runat="server" CssClass="commonerrormsg"
                                        ControlToValidate="txtOldPassword" Display="Dynamic" ValidationGroup="chk" ErrorMessage="Please enter old password."></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                    Password:<span class="starValidation">*</span></label>
                                <div class="col-md-10">
                                    <asp:TextBox ID="txtPassword" type="text" class="form-control" runat="server" MaxLength="20"
                                        TextMode="Password" />
                                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" CssClass="commonerrormsg"
                                        ControlToValidate="txtPassword" Display="Dynamic" ValidationGroup="chk" ErrorMessage="Password is required."></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="revPassword" runat="server" ControlToValidate="txtpassword"
                                        ValidationExpression="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,8}$" CssClass="commonerrormsg"
                                        Display="Dynamic" ValidationGroup="chk" ErrorMessage="
Password must be at least 4 characters, no more than 8 characters, and must include at least one upper case letter, one lower case letter, and one numeric digit" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                    Confirm Password:<span class="starValidation">*</span></label>
                                <div class="col-md-10">
                                    <asp:TextBox ID="txtConfirmPassword" type="text" class="form-control" runat="server"
                                        MaxLength="20" TextMode="Password" />
                                    <asp:CompareValidator ID="cvPasswordNotMatch" ControlToValidate="txtConfirmPassword"
                                        ControlToCompare="txtPassword" runat="server" CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="chk" ErrorMessage="Password and confirm password does not match."></asp:CompareValidator>
                                    <asp:RequiredFieldValidator ID="rfvConfirmPassword" runat="server" CssClass="commonerrormsg"
                                        ControlToValidate="txtConfirmPassword" Display="Dynamic" ErrorMessage="Confirm password is required."
                                        ValidationGroup="chk"></asp:RequiredFieldValidator></div>
                            </div>
                        </div>
                        <div class="form-horizontal row-border">
                            <div class="btn-toolbar">
                                <asp:Button runat="server" ID="Button1" Text="Save" CssClass=" btn btn-success" OnClick="btnsubmit_click"
                                    ValidationGroup="chk" />
                                <asp:Button runat="server" ID="Button2" Text="Cancel" CssClass="btn" ValidationGroup="chk"
                                    CausesValidation="false" OnClick="btnCancel_click" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--=== no-padding ===-->
    <div class="row">
    </div>
    <!-- /no-padding -->
    <!--===
    Normal ===-->
    <!-- /Normal -->
    <!-- /Normal -->
    <!--=== no-padding and table-tabletools
    ===-->
    <div class="row">
    </div>
    <!-- /no-padding and table-tabletools -->
    <!--===
    no-padding and table-colvis ===-->
    <div class="row">
    </div>
    <!-- /no-padding and
    table-colvis -->
    <!--=== Horizontal Scrolling ===-->
    <div class="row">
    </div>
    <!--
    /Normal -->
</asp:Content>

