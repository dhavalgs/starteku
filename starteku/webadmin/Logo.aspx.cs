﻿using starteku_BusinessLogic;
using starteku_BusinessLogic.Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class webadmin_Logo : System.Web.UI.Page
{
    startetkuEntities1 db = new startetkuEntities1();

    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (String.IsNullOrEmpty(Convert.ToString(Session["UserId"])))
        {
            Response.Redirect("login.aspx");
        }

        if (!IsPostBack)
        {

            LogoMaster logo = db.LogoMasters.FirstOrDefault();
            if (logo != null)
            {


                imgLoginLogo.ImageUrl = "../Log/upload/Userimage/" + logo.LoginLogo;
                ViewState["imgLoginLogo"] = logo.LoginLogo;
                imgPageLogo.ImageUrl = "../Log/upload/Userimage/" + logo.PageLogo;
                ViewState["imgPageLogo"] = logo.PageLogo;
                txtSystemname.Text = logo.SystemName;
                txtSystemnameDN.Text = logo.SystemNameDN;
                hdnLogoId.Value = Convert.ToString(logo.LogoId);
            }
        }
        else
        {
            Thread.Sleep(1500);
        }

       
        
    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        
        
           
            Int32 Logoid = Convert.ToInt32(hdnLogoId.Value);
            LogoMaster logo = db.LogoMasters.FirstOrDefault(x => x.LogoId == Logoid);
            if (logo != null)
            {
                logo.SystemName = txtSystemname.Text;
                logo.SystemNameDN = txtSystemnameDN.Text;
                if (flupload1.HasFile)
                {

                   
                    
                    string Filename = "LoginLogo" + Path.GetExtension(flupload1.PostedFile.FileName);
                    flupload1.SaveAs(Server.MapPath("../") + "Log/upload/Userimage/" + System.IO.Path.GetFileName(Filename.Replace(' ', '0')));
                    logo.LoginLogo = System.IO.Path.GetFileName(Filename.Replace(' ', '0'));

                }
                else
                {

                    logo.LoginLogo = Convert.ToString(ViewState["imgLoginLogo"]);

                }
                if (flupload2.HasFile)
                {

                    string Filename = "Pagelogo" + Path.GetExtension(flupload2.PostedFile.FileName);
                    flupload2.SaveAs(Server.MapPath("../") + "Log/upload/Userimage/" + System.IO.Path.GetFileName(Filename.Replace(' ', '0')));
                    logo.PageLogo = System.IO.Path.GetFileName(Filename.Replace(' ', '0'));

                }
                else
                {

                    logo.PageLogo = Convert.ToString(ViewState["imgPageLogo"]);
                    
                }
                db.SaveChanges();
                 
               ScriptManager.RegisterStartupScript(this.Page, this.GetType(), "tmp", "<script type='text/javascript'> generateTimer('warning', 'Please do ctrl+F5 if you did not get immediate effect.', 'topCenter', '5000');</script>", false);
              // Response.Redirect("Dashboard.aspx");  
              
            }

        
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("Dashboard.aspx");
    }
}