﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webadmin/AdminMasterPage.master"
    AutoEventWireup="true" CodeFile="Eeditresource.aspx.cs" Inherits="webadmin_Eeditresource" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <div class="row">
        <div class="col-md-12">
            <div class="widget box">
                <div class="widget-header">
                    <h4>
                        <i class="icon-reorder"></i>Update Resource</h4>
                    <div class="toolbar no-padding">
                        <div class="btn-group">
                            <span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
                        </div>
                    </div>
                </div>
                <div style="font-size: 14pt; color: #0000ff;" class="form-group">
                    <br />
                    <asp:Label ID="Label1" runat="server" class="col-md-2 control-label"></asp:Label><br />
                    <br />
                    <div class="col-md-10">
                        <asp:TextBox ID="txtResourceValue" runat="server" CssClass="form-control"></asp:TextBox><br />
                    </div>
                    <br />
                    <br />
                </div>
                <div class="form-horizontal row-border">
                    <div class="btn-toolbar">
                        <asp:Button runat="server" ID="Button1" Text="Update" CssClass="btn btn-success"
                            OnClick="Button1_Click" />
                        <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="btn" OnClick="btnCancel_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
