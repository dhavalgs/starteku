﻿<%@ Page Title="" Language="C#" meta:resourcekey="PageResource" MasterPageFile="~/webadmin/AdminMasterPage.master" AutoEventWireup="true" 
    CodeFile="WEBActivityCategory.aspx.cs" Inherits="webadmin_WEBActivityCategory" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

     <link href="css/ajaxtab.css" rel="stylesheet" />
   

    <script type="text/javascript">
        function ShowCreateLable(isCreate) {
            $(".mdlHeaderLbl").hide();
            $(".lblCreate").show();
            $("#ContentPlaceHolder1_Buttonupdate").hide();
            $("#ContentPlaceHolder1_btnsubmit").show();


        }

        function ShowEditLable() {
            $(".mdlHeaderLbl").hide();
            $(".lblUpdate").show();
            $("#ContentPlaceHolder1_Buttonupdate").show();
            $("#ContentPlaceHolder1_btnsubmit").hide();
            //$("#ContentPlaceHolder1_txtActName").val(tag);
        }

        function focusOn() {

            setTimeout(function () {
                //alert();
                $('#ContentPlaceHolder1_txtDepartmentName').focus();
            }, 1500);
        }
    </script>




</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">





     <div class="col-md-6" style="display:none">
        <div class="heading-sec">
            <h1 style="margin-left: 15px;">
                <%-- <%= CommonMessages.CATEGORY%> --%>
                <asp:Literal ID="Literal1" runat="server" meta:resourcekey="ACTIVITYCATEGORY" EnableViewState="false" /><i><span
                    runat="server" id="Category"></span></i>
            </h1>
        </div>
    </div>





    <div class="col-md-12" style="margin-top: 20px;">
        <div id="graph-wrapper">
            <div class="col-md-12"  >
                <asp:Button runat="server" ID="Button1" Text="Submit" CssClass="btn btn-primary yellow"
                    Style="border-radius: 5px;" meta:resourcekey="AddNewActivityCategory" OnClick="Test_click" Visible="False" />


                <a href="#add-post-title" data-toggle="modal" title="" style="margin-bottom: 15px; display: none">
                    <button id="ac" style="border: 0px;" class="btn btn-primary yellow lrg-btn flat-btn add_user" type="button" onclick="ShowCreateLable(true);">
                        <%--   <%= CommonMessages.AddNew%> <%= CommonMessages.CATEGORY%>--%>
                        <asp:Literal ID="Literal2" runat="server" meta:resourcekey="AddNewActivityCategory" EnableViewState="false" />
                    </button>
                </a>

                <div aria-hidden="true" role="dialog" tabindex="-1" class="modal fade" id="add-post-title"
                    style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header blue yellow-radius" style="border-radius: 0px;">
                                <button aria-hidden="true" data-dismiss="modal" class="close" type="button">
                                    ×
                                </button>
                                <h4 class="modal-title">
                                    <%--  <%= CommonMessages.AddNew%> <%= CommonMessages.CATEGORY%>--%>
                                    <asp:Label ID="lblCreate" runat="server" meta:resourcekey="ModelAddNewActivityCategory" Text="Label" CssClass="mdlHeaderLbl lblCreate"></asp:Label>
                                    <asp:Label ID="LblUpdate" runat="server" meta:resourcekey="EditActivityCategory" Text="Label" CssClass="mdlHeaderLbl lblUpdate"></asp:Label>
                                    <%--<asp:Literal ID="Literal3" runat="server" meta:resourcekey="AddNewActivityCategory" EnableViewState="false" Visible="True" />
                                    <asp:Literal ID="Literal7" runat="server" meta:resourcekey="EditActivityCategory" EnableViewState="false" Visible="False" />--%>
                                </h4>
                            </div>


                            <div class="modal-body">
                                <div class="form-group">
                                    <div class="col-md-4" style="padding-top: 10px;">
                                        <asp:Label runat="server" ID="Label5" CssClass="" Text="Activity Category:" meta:resourcekey="MODELACTIVITYCATEGORY" Style="color: black; font-weight: bold; margin-top: 4px;" Visible="True"></asp:Label>

                                    </div>
                                    <div class="col-md-8">


                                        <asp:TextBox runat="server" ID="txtActName" MaxLength="50" meta:resourcekey="FileName" />
                                        <%--meta:resourcekey="txttitleResource1"--%>
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtActName"
                                            ErrorMessage="Please enter activity category." CssClass="commonerrormsg" Display="Dynamic"
                                            ValidationGroup="chkdoc"></asp:RequiredFieldValidator>

                                    </div>
                                </div>
<%--------------------------------------------------------------------------------------------------------------%>
                                 <div class="form-group">
                                    <div class="col-md-4">
                                        <asp:Literal ID="Literal7" meta:resourcekey="public" runat="server"></asp:Literal>

                                    </div>
                                    <br />

                                    <div class="col-md-8">
                                        <asp:RadioButtonList runat="server" ID="rdoActCatPublic" RepeatDirection="Horizontal" CssClass="inline-rb " TextAlign="Right">
                                            <asp:ListItem Text="Yes" meta:resourcekey="Yes" Value="1" />
                                            <asp:ListItem Text="No" meta:resourcekey="No" Value="2" Selected="True" />
                                        </asp:RadioButtonList>
                                    </div>

                                </div>

<%--------------------------------------------------------12-4-2017 code by jainam shah--------------------------------------------------------------%>

                                <div class="form-group">
                                    <div class="col-md-4">
                                        <asp:Literal ID="Literal5" meta:resourcekey="ReqE" runat="server"></asp:Literal>

                                    </div>
                                    <br />

                                    <div class="col-md-8">
                                        <asp:RadioButtonList runat="server" ID="rdoActCatReqEnabled" RepeatDirection="Horizontal" CssClass="inline-rb " TextAlign="Right">
                                            <asp:ListItem Text="Yes" meta:resourcekey="Yes" Value="1" />
                                            <asp:ListItem Text="No" meta:resourcekey="No" Value="2" Selected="True" />
                                        </asp:RadioButtonList>
                                    </div>

                                </div>


                                <div class="form-group">
                                    <div class="col-md-4">
                                        <asp:Literal ID="Literal6" meta:resourcekey="CompE" runat="server"></asp:Literal>

                                    </div>
                                    <br />

                                    <div class="col-md-8">
                                        <asp:RadioButtonList runat="server" ID="rdoActCompEnabled" RepeatDirection="Horizontal" CssClass="inline-rb " TextAlign="Right">
                                            <asp:ListItem Text="Yes" meta:resourcekey="Yes" Value="1" />
                                            <asp:ListItem Text="No" meta:resourcekey="No" Value="2" Selected="True" />
                                        </asp:RadioButtonList>
                                    </div>

<%-----------------------------------------------------------------------------------------------------------------------------------------------------%>

                                     <div class="form-group">
                                    <div class="col-md-4">
                                        <asp:Literal ID="Literal9" meta:resourcekey="Developmentplan" runat="server"></asp:Literal>

                                    </div>
                                    <br />

                                    <div class="col-md-8">
                                        <asp:RadioButtonList runat="server" ID="rdoActCatDevelopmentplan" RepeatDirection="Horizontal" CssClass="inline-rb " TextAlign="Right">
                                            <asp:ListItem Text="Yes" meta:resourcekey="Yes" Value="1" />
                                            <asp:ListItem Text="No" meta:resourcekey="No" Value="2" Selected="True" />
                                        </asp:RadioButtonList>
                                    </div>

                                </div>

<%-------------------------------------------------------------------------12-4-2017 code by jainam shah--------------------------------------------------------------------------------%>

                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <div class="col-md-4" style="margin-top:20px;">
                                            <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Translations" > </asp:Literal>
                                        </div>

                                        <div class="col-md-8" style="overflow-y: scroll; height: 150px;">
                                            <asp:GridView ID="gridTranslate" runat="server" AutoGenerateColumns="False" CellPadding="0" Style="background-color: #f9f9f9;margin-bottom:0px !important;"
                                                Width="100%" GridLines="None" CssClass="table table-striped table-bordered table-hover table-checkable datatable">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Language">
                                                        <ItemTemplate>
                                                            <asp:Label ID="Label1" runat="server" Text='<%# Eval("resLanguage") %>'></asp:Label>
                                                            <asp:HiddenField ID="hdnResLangID" runat="server" Value='<%# Eval("resLangID") %>'></asp:HiddenField>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" Font-Size="14px" />
                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                            Width="10%" ForeColor="White" Font-Size="15px" BorderWidth="0px" Height="20px" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Value">
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="txtTranValue" Value='<%# Eval("LangText") %>' runat="server"></asp:TextBox>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" Font-Size="14px" />
                                                        <HeaderStyle HorizontalAlign="Left" CssClass="invoice-head chat-widget-head yellow"
                                                            Width="10%" ForeColor="White" Font-Size="15px" BorderWidth="0px" Height="20px" />
                                                    </asp:TemplateField>

                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <asp:Button runat="server" ID="btnsubmit" Text="Submit" CssClass="btn btn-primary yellow"
                                    ValidationGroup="chkdoc"
                                    Style="border-radius: 5px;" meta:resourcekey="btnsubmitResource1"   />


                                <asp:Button runat="server" ID="Buttonupdate" Text="Update" CssClass="btn btn-primary yellow" ValidationGroup="chkdoc"
                                    Style="border-radius: 5px;" meta:resourcekey="btnsubmitResourceupdate" Visible="False" />


                                <button data-dismiss="modal" class="btn btn-default black" type="button">
                                    <%-- Close<%= CommonMessages.Close%>--%>
                                    <asp:Literal ID="Literal4" runat="server" meta:resourcekey="Close" EnableViewState="false" />
                                </button>
                            </div>
                        </div>
                        <!-- /.modal-content -->
                    </div>
                </div>

                <br />
                <br />

                <div style="clear: both;"></div>
                <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" meta:resourcekey="lblMsgResource1" Visible="False"></asp:Label>
                <br />

                <asp:Label ID="LabelUpdated" runat="server" Text="Record Updated Successfully" Visible="False" meta:resourcekey="lblMsgResource1" ForeColor="White"></asp:Label>
                <asp:Label ID="Labeldeleted" runat="server" Text="Record deleted Successfully" Visible="False" meta:resourcekey="lblMsgResource2" ForeColor="White"></asp:Label>
                <asp:Label ID="LabelAlready" runat="server" Text="Opps The Activity is Already Available." Visible="False" meta:resourcekey="lblMsgResource3" ForeColor="White"></asp:Label>
                <asp:Label ID="LabelAdded" runat="server" Text="Activity Category Added Successfully" Visible="False" meta:resourcekey="lblMsgResource4" ForeColor="White"></asp:Label>
           
                     <table><tr><td><asp:Label runat="server" ID="Label19" Text="Filter By Company:"></asp:Label></td>
                               
<td><asp:DropDownList Width="250px" ID="DropDownList1" AutoPostBack="True" runat="server" CssClass="chkliststyle form-control" onselectedindexchanged="getc"
                        RepeatDirection="Vertical" RepeatColumns="1" BorderWidth="0" Datafield="description" 
                        DataValueField="value" ><%--Style="background: rgb(2, 64, 109) none repeat scroll 0% 0%; border-radius: 5px; color: white; overflow: auto;" Visible="True"--%>
                    </asp:DropDownList></td></tr></table><br/>



                <asp:Label ID="hdnConfirmArchive" Style="display: none;" CssClass="abcde" meta:resourcekey="ConfirmArchive" runat="server" />
                
                
                <div class="chart-tab manager_table">

                     <div id="tabs-container manager_table">


                         <div class="widget box">
                       <div class="widget-header">
                    <h4>
                        <i class="icon-reorder"></i><%-- <%= CommonMessages.Company%>--%>
                        <asp:Literal ID="Literal10" runat="server" meta:resourcekey="Company" Text="Activity Category" enableviewstate="false"/></h4>
                    <div class="toolbar no-padding">
                        <div class="btn-group">
                            <span class="btn btn-xs widget-collapse"><i class="icon-angle-down"></i></span>
                        </div>
                    </div>
                </div>


                    <div class="widget-content no-padding">

                    <div class="btn-toolbar" style="text-align: right;">
                    </div>

                     <cc1:TabContainer ID="TabContainer1" runat="server" 
                        CssClass="fancy fancy-green" ActiveTabIndex="0" 
                        >
                        <cc1:TabPanel ID="tbpnluser" runat="server" 
                            >
                             <HeaderTemplate>
                               Activity Category
                            </HeaderTemplate>
                             <ContentTemplate>


                    <asp:GridView ID="gvGrid" runat="server" AutoGenerateColumns="False" CellPadding="0" Style="background-color: #f9f9f9;"  EmptyDataText="No Record Found"
                        Width="100%" GridLines="None" CssClass="table table-striped table-bordered table-hover table-checkable datatable" 
                        OnRowCommand="gvGrid_RowCommand">

                        <HeaderStyle CssClass="aa" />
                        <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>

                            <asp:TemplateField HeaderText="Id" meta:resourcekey="ID">
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1 %>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" Font-Size="14px" />
                                 <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="CATEGORY" meta:resourcekey="ActCatName">

                                <ItemTemplate>
                                    <asp:Label ID="lblActCatName" runat="server" Text='<%# Eval("ActCatName") %>'></asp:Label>

                                </ItemTemplate>

                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Font-Size="14px" />
                                 <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="Request enabled" meta:resourcekey="ActCatPublic">
                                <ItemTemplate>
                                    <asp:Label ID="lblActCatPublic" runat="server" Text='<%# (Boolean.Parse(Eval("ActCatPublic").ToString())) ? GetLocalResourceObject("Yes.Text").ToString() : GetLocalResourceObject("No.Text").ToString() %>'></asp:Label>


                                </ItemTemplate>
                               <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                            </asp:TemplateField>





                            <asp:TemplateField HeaderText="Request enabled" meta:resourcekey="TemplateFieldResource3">
                                <ItemTemplate>
                                    <asp:Label ID="lblActCatReqEnabled" runat="server" Text='<%# (Boolean.Parse(Eval("ActCatReqEnabled").ToString())) ? GetLocalResourceObject("Yes.Text").ToString() : GetLocalResourceObject("No.Text").ToString() %>'></asp:Label>


                                </ItemTemplate>
                               <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Competence enabled" meta:resourcekey="ActCompEnabled">
                                <ItemTemplate>

                                    <asp:Label ID="lblActCompEnabled" runat="server" Text='<%# (Boolean.Parse(Eval("ActCompEnabled").ToString())) ? GetLocalResourceObject("Yes.Text").ToString() :GetLocalResourceObject("No.Text").ToString() %>'></asp:Label>


                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="Request enabled" meta:resourcekey="ActCatDeveplan">
                                <ItemTemplate>
                                    <asp:Label ID="lblActCatDeveplan" runat="server" Text='<%# (Boolean.Parse(Eval("ActCatDeveplan").ToString())) ? GetLocalResourceObject("Yes.Text").ToString() : GetLocalResourceObject("No.Text").ToString() %>'></asp:Label>


                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                            </asp:TemplateField>





                            <asp:TemplateField HeaderText="Action" ItemStyle-Width="50px" meta:resourcekey="Action">
                                <ItemTemplate>

                                    <div class="vat" style="width: 110px; display: none" id="catEdit" >
                                        <p>
                                            <i class="fa fa-pencil"></i>
                                            <asp:LinkButton ID="LinkButton1" CssClass="def" runat="server" CommandName="Edit_CAT" CommandArgument='<%# Eval("ActCatId") %>'
                                                meta:resourcekey="Edit" OnClientClick="ShowEditLable();">Edit </asp:LinkButton>
                                        </p>
                                    </div>

                                    <div id="Div1" class="total" style="width: 75px;" runat="server">
                                        <p>
                                            <i class="fa fa-trash-o"></i>

                                            <asp:LinkButton ID="lnkBtnName" CssClass="def" runat="server" CommandName="Archive_CAT" CommandArgument='<%# Eval("ActCatId") %>'
                                                meta:resourcekey="lnkBtnNameResource1" OnClientClick="return ArchiveMsg()">Archive
                                            </asp:LinkButton>
                                        </p>

                                        

                                    </div>

                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" Font-Size="14px" />
                               <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                              </ContentTemplate>
                        </cc1:TabPanel>


                           <cc1:TabPanel ID="tbpnlusrdetails" runat="server">
                            <HeaderTemplate>
                                Archive
                            </HeaderTemplate>
                            <ContentTemplate>
                                 <asp:GridView ID="gvGridArchive" runat="server" AutoGenerateColumns="False" CellPadding="0" Style="background-color: #f9f9f9;"
                        Width="100%" GridLines="None" CssClass="table table-striped table-bordered table-hover table-checkable datatable"  EmptyDataText="No Record Found"
                        OnRowCommand="gvGrid_RowCommand">

                        <HeaderStyle CssClass="aa" />
                        <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>

                            <asp:TemplateField HeaderText="Id" meta:resourcekey="ID">
                                <ItemTemplate>
                                    <%# Container.DataItemIndex + 1 %>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" Font-Size="14px" />
                                 <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="CATEGORY" meta:resourcekey="ActCatName">

                                <ItemTemplate>
                                    <asp:Label ID="lblActCatName" runat="server" Text='<%# Eval("ActCatName") %>'></asp:Label>

                                </ItemTemplate>

                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Font-Size="14px" />
                                 <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="Request enabled" meta:resourcekey="ActCatPublic">
                                <ItemTemplate>
                                    <asp:Label ID="lblActCatPublic" runat="server" Text='<%# (Boolean.Parse(Eval("ActCatPublic").ToString())) ? GetLocalResourceObject("Yes.Text").ToString() : GetLocalResourceObject("No.Text").ToString() %>'></asp:Label>


                                </ItemTemplate>
                               <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                            </asp:TemplateField>





                            <asp:TemplateField HeaderText="Request enabled" meta:resourcekey="TemplateFieldResource3">
                                <ItemTemplate>
                                    <asp:Label ID="lblActCatReqEnabled" runat="server" Text='<%# (Boolean.Parse(Eval("ActCatReqEnabled").ToString())) ? GetLocalResourceObject("Yes.Text").ToString() : GetLocalResourceObject("No.Text").ToString() %>'></asp:Label>


                                </ItemTemplate>
                               <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="Competence enabled" meta:resourcekey="ActCompEnabled">
                                <ItemTemplate>

                                    <asp:Label ID="lblActCompEnabled" runat="server" Text='<%# (Boolean.Parse(Eval("ActCompEnabled").ToString())) ? GetLocalResourceObject("Yes.Text").ToString() :GetLocalResourceObject("No.Text").ToString() %>'></asp:Label>


                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                            </asp:TemplateField>


                            <asp:TemplateField HeaderText="Request enabled" meta:resourcekey="ActCatDeveplan">
                                <ItemTemplate>
                                    <asp:Label ID="lblActCatDeveplan" runat="server" Text='<%# (Boolean.Parse(Eval("ActCatDeveplan").ToString())) ? GetLocalResourceObject("Yes.Text").ToString() : GetLocalResourceObject("No.Text").ToString() %>'></asp:Label>


                                </ItemTemplate>
                                <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                            </asp:TemplateField>





                            <asp:TemplateField HeaderText="Action" ItemStyle-Width="50px" meta:resourcekey="Action">
                                <ItemTemplate>

                                    <div class="vat" style="width: 110px; display: none" id="catEdit" >
                                        <p>
                                            <i class="fa fa-pencil"></i>
                                            <asp:LinkButton ID="LinkButton1" CssClass="def" runat="server" CommandName="Edit_CAT" CommandArgument='<%# Eval("ActCatId") %>'
                                                meta:resourcekey="Edit" OnClientClick="ShowEditLable();">Edit </asp:LinkButton>
                                        </p>
                                    </div>

                                     <div id="Div2" class="total"  runat="server"><%--style="width: 75px;"--%>
                                        <p>
                                            <i class="fa fa-trash-o"></i>
                                             <asp:LinkButton ID="LinkButton3" CssClass="def" runat="server" CommandName="Restore_CAT" CommandArgument='<%# Eval("ActCatId") %>'
                                                meta:resourcekey="Restore" OnClientClick="return RestoreMsg()">Restore </asp:LinkButton>
                                       </p>
                                    </div>

                                    <div id="Div1" class="total"  runat="server">
                                        <p>
                                            <i class="fa fa-trash-o"></i>
                                            <asp:LinkButton ID="lnkBtnName" CssClass="def" runat="server" CommandName="Delete_CAT" CommandArgument='<%# Eval("ActCatId") %>'
                                                meta:resourcekey="Delete" OnClientClick="return DeleteMsg()">Delete </asp:LinkButton>
                                        </p>
                                    </div>

                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" Font-Size="14px" />
                                <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>

                              </ContentTemplate>
                        </cc1:TabPanel>






                    </cc1:TabContainer>


                        </div>

                         </div>
                         </div>












                </div>
            </div>
        </div>
    </div>
    </div>
    <!-- /no-padding -->
    <!--=== no-padding and table-tabletools ===-->
    <div class="row">
    </div>
    <!-- /no-padding and table-tabletools -->
    <!--=== no-padding and table-colvis ===-->
    <div class="row">
    </div>
    <!-- /no-padding and table-colvis -->
    <!--=== Horizontal Scrolling ===-->

    <div style="display: none">

        <asp:Label ID="lblDeleteMsg" CssClass="lblDel" runat="server">
            <asp:Literal ID="Literal8" meta:resourcekey="AreYouSureToDelete" runat="server" Visible="True"></asp:Literal>

        </asp:Label>

        <asp:Label ID="Label2" CssClass="lblRes" runat="server">
            <asp:Literal ID="Literal11" meta:resourcekey="AreYouSureToRestore" runat="server" Visible="True"></asp:Literal>

        </asp:Label>

        <asp:Label ID="Label3" CssClass="lblArc" runat="server">
            <asp:Literal ID="Literal12" meta:resourcekey="AreYouSureToArchive" runat="server" Visible="True"></asp:Literal>

        </asp:Label>

    </div>

    <div class="row">
    </div>
    <!-- /Normal -->
    <!-- /Page Content -->
    <!-- DataTables -->
    <script type="text/javascript" src="../plugins/datatables/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="../plugins/datatables/tabletools/TableTools.min.js"></script>
    <script type="text/javascript">

        function DeleteMsg() {

            return confirm($(".lblDel").text());

        }

        function RestoreMsg() {

            return confirm($(".lblRes").text());

        }

        function ArchiveMsg() {

            return confirm($(".lblArc").text());

        }


        $(document).ready(function () {

            setTimeout(function () {
                SetExpandCollapse();

            }, 500);
        });
        $(document).ready(function () {

        });

        function xyz() {
            return confirm($(".abcde").text());

        }
    </script>

    <asp:HiddenField ID="hdnActCatId" runat="server" Value="Null" />















</asp:Content>

