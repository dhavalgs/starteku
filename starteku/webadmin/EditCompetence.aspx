﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webadmin/AdminMasterPage.master" AutoEventWireup="true" CodeFile="EditCompetence.aspx.cs" Inherits="webadmin_EditCompetence" 
culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" src="../Scripts/ckeditor/ckeditor.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" 
        meta:resourcekey="lblMsgResource1"></asp:Label>
 <div class="row">
        <div class="col-md-12">
            <div class="col-md-12">
                <div class="indicatesRequireFiled">
                    *  <%--<%= CommonMessages.Indicatesrequiredfield%>--%>
                    <asp:Literal ID="Literal9" runat="server" meta:resourcekey="Indicatesrequiredfield" enableviewstate="false"/>

                </div>
                <div class="widget box">
                    <div class="widget-header">
                        <h4>
                            <i class="icon-reorder"></i>
                            <asp:Label runat="server" ID="lblDataDisplayTitle" Text="Competence" 
                                meta:resourcekey="lblDataDisplayTitleResource1"></asp:Label>
                        </h4>
                    </div>
                    <div class="widget-content">
                        <%--   <form class="form-horizontal row-border" action="#">--%>
                        <div class="form-horizontal row-border">
<%--Category----------------------------------------------------------------------------------%>
                             <div class="form-group">
                                <label class="col-md-2 control-label">
                                   <%-- <%= CommonMessages.CATEGORY%>--%>
                                    <asp:Literal ID="Literal1" runat="server" meta:resourcekey="CATEGORY" enableviewstate="false"/> :<span class="starValidation">*</span></label>
                                <div class="col-md-10">
                                      <asp:DropDownList ID="ddlcategory" runat="server" Style="width: 100%; height: 32px;
                            display: inline;" meta:resourcekey="ddlcategoryResource1">
                        </asp:DropDownList>
                       
                          <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="ddlcategory"
                            ErrorMessage="Please select Category." InitialValue="0" CssClass="commonerrormsg"
                            Display="Dynamic" ValidationGroup="chk" 
                            meta:resourcekey="RequiredFieldValidator14Resource1"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            

                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                   <%-- <%= CommonMessages.Competence%>--%>
                                    <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Competence" enableviewstate="false"/>:<span class="starValidation">*</span></label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="txtCompetence" MaxLength="50" 
                                        CssClass="form-control" meta:resourcekey="txtCompetenceResource1" />
                                   
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtCompetence"
                                        ErrorMessage="Please Enter Country Name." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator5Resource1"></asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                   <%-- <%= CommonMessages.Competence%>--%>
                                    <asp:Literal ID="Literal3" runat="server" meta:resourcekey="Competenceindanish" enableviewstate="false"/>:<span class="starValidation">*</span></label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="txtCompetenceDN" MaxLength="50" 
                                        CssClass="form-control" meta:resourcekey="txtCompetenceResource2" />
                                   
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtCompetenceDN"
                                        ErrorMessage="Please Enter competence Name in danish." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator1Resource1"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            
<%--LEVEL-------------------------------------------------------------%>
                            
                             <div class="form-group">
                                
                               <asp:Repeater ID="Repeater1" runat="server">
                        <ItemTemplate>
                             <label class="col-md-2 control-label">
                                       <%-- <%= CommonMessages.Level%>--%>
                                 
                                          <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Level" enableviewstate="false" /> :<%# Container.ItemIndex +1 %></label><div class="col-md-10">
                                          <table style="width: 100%">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="TextBox1" Text='<%# Eval("comchildlevel") %>' runat="server" CssClass="form-control ckeditor"
                                                TextMode="MultiLine" meta:resourcekey="TextBox1Resource1"></asp:TextBox>
                                                   <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="TextBox1"
                                                ErrorMessage="Please enter answers." CssClass="commonerrormsg" Display="Dynamic"
                                                ValidationGroup="chk" Height="50px" 
                                        meta:resourcekey="RequiredFieldValidator2Resource1"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                              </div>
                            
                            
                                            <label class="col-md-2 control-label"><asp:Literal ID="Literal4" runat="server" meta:resourcekey="LevelInDanish" enableviewstate="false"/> :<%# Container.ItemIndex +1 %></label>
                                          <div class="col-md-10">
                                          <table style="width: 100%">
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="TextBoxDN" Text='<%# Eval("comchildlevelDN") %>' runat="server" CssClass="form-control ckeditor"
                                                TextMode="MultiLine" meta:resourcekey="TextBox1Resource1"></asp:TextBox>
                                                   <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="TextBox1"
                                                ErrorMessage="Please enter level in danish." CssClass="commonerrormsg" Display="Dynamic"
                                                ValidationGroup="chk" Height="50px" 
                                        meta:resourcekey="RequiredFieldValidator3Resource1"></asp:RequiredFieldValidator>
                                            </td>
                                        </tr>
                                    </table>
                                    </div>
                            
                               
                        </ItemTemplate>
                    </asp:Repeater>
                            </div>
                           
                           
                           
 <%--Submit buttons---------------------------------------------------------------------------------------%>
                           

                            <div class="form-horizontal row-border">
                                <div class="btn-toolbar">                                    
                                    <asp:Button runat="server" ID="btnsubmit" Text="Save" CssClass="btn btn-success"
                                         ValidationGroup="chk" OnClick="btnsubmit_click" 
                                        meta:resourcekey="btnsubmitResource1"/>
                                    <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="btn" ValidationGroup="chk"
                                        CausesValidation="False" OnClick="btnCancel_click" 
                                        meta:resourcekey="btnCancelResource1" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--=== no-padding ===-->
    <div class="row">
    </div>
    <div class="row">
    </div>
    <!-- /no-padding and table-tabletools -->
    <!--=== no-padding and table-colvis ===-->
    <div class="row">
    </div>
    <!-- /no-padding and table-colvis -->
    <!--=== Horizontal Scrolling ===-->
    <div class="row">
    </div>
    <!-- /Normal -->
    <!-- /Page Content -->
</asp:Content>

