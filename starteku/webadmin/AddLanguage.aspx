﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddLanguage.aspx.cs" Inherits="webadmin_AddLanguage" MasterPageFile="~/webadmin/AdminMasterPage.master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg"
        meta:resourcekey="lblMsgResource1" Style="padding-left: 15px;"></asp:Label>
    <div class="row">

        <div class="col-md-12">
            <div class="col-md-12">
                <div class="indicatesRequireFiled">
                    * <%-- <%= CommonMessages.Indicatesrequiredfield%>--%>
                    <asp:Literal ID="Literal9" runat="server" meta:resourcekey="Indicatesrequiredfield" EnableViewState="false" />
                </div>
                <div class="widget box">
                    <div class="widget-header">
                        <h4>
                            <i class="icon-reorder"></i>
                            <asp:Label runat="server" ID="lblDataDisplayTitle" Text="Manage Language"
                                meta:resourcekey="lblDataDisplayTitleResource1"></asp:Label>
                        </h4>
                    </div>
                    <div class="widget-content no-padding">
                        <div class="btn-toolbar" style="text-align: right;">
                        </div>
                        <cc1:TabContainer ID="TabContainer1" runat="server"
                            CssClass="fancy fancy-green" ActiveTabIndex="0"
                            meta:resourcekey="TabContainer1Resource1">
                            <cc1:TabPanel ID="TabPanel1" runat="server"
                                meta:resourcekey="tbpnlusrdetailsResource1">
                                <HeaderTemplate>
                                    <asp:Literal ID="Literal4" runat="server" Text="Active Language" EnableViewState="false" />
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <asp:UpdatePanel runat="server" ID="UpdatePanedrop" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:GridView ID="gvSystemLang" runat="server" AutoGenerateColumns="False"
                                                CellPadding="0" Width="100%" GridLines="None" 
                                                DataKeyNames="resLangID" OnRowCommand="gvSystemLang_RowCommand" OnRowDataBound="gvSystemLang_RowDataBound"
                                                CssClass="table table-striped table-bordered table-hover table-checkable datatable1"
                                                OnRowEditing="gvSystemLang_RowEditing" OnRowUpdating="gvSystemLang_RowUpdating" OnRowCancelingEdit="gvSystemLang_RowCancelingEdit">
                                                <HeaderStyle CssClass="aa" />
                                                <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                                <AlternatingRowStyle BackColor="White" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Sr.No" meta:resourcekey="TemplateFieldResource4">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblResID" runat="server" Text="<%# bind('resLangID') %>"
                                                                Visible="false"></asp:Label>
                                                            <%# Container.DataItemIndex + 1 %>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                                        <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Language Name"
                                                        meta:resourcekey="TemplateFieldResource5">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblName" runat="server" Text="<%# bind('resLanguage') %>"
                                                                meta:resourcekey="lblaNamerResource1"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                        <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Date Format"
                                                        meta:resourcekey="TemplateFieldResource5">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblDate" runat="server" Text="<%# bind('resDateFormat') %>"
                                                                meta:resourcekey="lblaNamerResource1"></asp:Label>
                                                        </ItemTemplate>
                                                        <EditItemTemplate>
                                                            <asp:DropDownList ID="ddlDateFormat" runat="server">
                                                                <asp:ListItem>dd/MM/yy</asp:ListItem>
                                                                <asp:ListItem>dd/MM/yyyy</asp:ListItem>
                                                                <asp:ListItem>MM/dd/yy</asp:ListItem>
                                                                <asp:ListItem>MM/dd/yyyy</asp:ListItem>
                                                            </asp:DropDownList>
                                                        </EditItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                        <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Status"
                                                        meta:resourcekey="TemplateFieldResource5">
                                                        <ItemTemplate>
                                                            <asp:Label ID="lblStatus" runat="server" Text="<%# bind('resIsActive') %>"
                                                                meta:resourcekey="lblaNamerResource1"></asp:Label>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                        <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Action"
                                                        meta:resourcekey="TemplateFieldResource6">
                                                        <ItemTemplate>
                                                            <span class="btn-group">


                                                                <asp:LinkButton ID="lnkbtnActive" runat="server" CommandName="active" CommandArgument='<%# Eval("resLangID") %>'
                                                                    ToolTip="Restore" OnClientClick="return confirm('Are you sure you want to make language Active?');"
                                                                    Text="Active" Visible="false"></asp:LinkButton>

                                                                <asp:LinkButton ID="lnkbtnInActive" runat="server" CommandName="inactive" Visible="false"
                                                                    CommandArgument='<%# Eval("resLangID") %>' Text="InActive" ToolTip="Delete Permanently"
                                                                    OnClientClick="return confirm('Are you sure you want to make language InActive?');"></asp:LinkButton>


                                                            </span>
                                                        </ItemTemplate>
                                                        <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                                        <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="10%" />
                                                    </asp:TemplateField>
                                                    <asp:CommandField ShowEditButton="True" ControlStyle-CssClass="icon-pencil" EditText="" />
                                                </Columns>
                                            </asp:GridView>
                                        </ContentTemplate>
                                    </asp:UpdatePanel>
                                </ContentTemplate>
                            </cc1:TabPanel>
                            <cc1:TabPanel ID="tbpnluser" runat="server"
                                meta:resourcekey="tbpnluserResource1">
                                <HeaderTemplate>
                                    <asp:Literal ID="Literal2" runat="server" Text="Add Language" EnableViewState="false" />
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">
                                            <%-- <%= CommonMessages.Country%>--%>
                                            <asp:Literal ID="Literal1" runat="server" Text="Language" EnableViewState="false" />:<span class="starValidation">*</span></label>
                                        <div class="col-md-10">
                                            <asp:DropDownList ID="ddlLanguage" class="col-md-5" ValidationGroup="chk" runat="server" Style="height: 32px; display: inline;">
                                            </asp:DropDownList>
                                            <br />
                                            <br />
                                            <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="ddlLanguage"
                                                ErrorMessage="Please select language." InitialValue="" CssClass="commonerrormsg"
                                                Display="Dynamic" ValidationGroup="chk"></asp:RequiredFieldValidator>

                                        </div>
                                    </div>
                                    <div class="form-horizontal row-border">
                                        <div class="btn-toolbar">
                                            <asp:Button runat="server" ID="btnsubmit" Text="Save" CssClass="btn btn-success"
                                                ValidationGroup="chk" OnClick="btnsubmit_Click"
                                                meta:resourcekey="btnsubmitResource1" />
                                            <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="btn" ValidationGroup="chk"
                                                CausesValidation="False" OnClick="btnCancel_Click"
                                                meta:resourcekey="btnCancelResource1" />
                                        </div>
                                    </div>

                                </ContentTemplate>
                            </cc1:TabPanel>
                            <cc1:TabPanel ID="tbpnlusrdetails" runat="server">
                                <HeaderTemplate>
                                    <asp:Literal ID="Literal3" runat="server" Text="Available Language" EnableViewState="false" />
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <asp:GridView ID="gvLanguage" runat="server" AutoGenerateColumns="False"
                                        CellPadding="0" Width="100%" GridLines="None" EmptyDataText="No Record Found"
                                        DataKeyNames="lng_ID" CssClass="table table-striped table-bordered table-hover table-checkable datatable">

                                        <HeaderStyle CssClass="aa" />
                                        <EmptyDataRowStyle ForeColor="Red"></EmptyDataRowStyle>
                                        <AlternatingRowStyle BackColor="White" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="Sr.No" meta:resourcekey="TemplateFieldResource1">
                                                <ItemTemplate>
                                                    <%# Container.DataItemIndex + 1 %>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" Width="50px" />
                                                <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Language Name" meta:resourcekey="TemplateFieldResource2">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblaNamer" runat="server" Text="<%# bind('lng_Name') %>"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                            </asp:TemplateField>
                                            <asp:TemplateField HeaderText="Status" meta:resourcekey="TemplateFieldResource3">
                                                <ItemTemplate>
                                                    <asp:Label ID="lblaNamer" runat="server" Text="<%# bind('lng_IsActive') %>"></asp:Label>
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                                <HeaderStyle HorizontalAlign="Left" CssClass="hidden-xs" />
                                            </asp:TemplateField>

                                        </Columns>
                                    </asp:GridView>
                                </ContentTemplate>
                            </cc1:TabPanel>

                        </cc1:TabContainer>
                    </div>

                </div>
            </div>
        </div>

    </div>
    <!--=== no-padding ===-->
    <div class="row">
    </div>
    <div class="row">
    </div>
    <!-- /no-padding and table-tabletools -->
    <!--=== no-padding and table-colvis ===-->
    <div class="row">
    </div>
    <!-- /no-padding and table-colvis -->
    <!--=== Horizontal Scrolling ===-->
    <div class="row">
    </div>
    <!-- /Normal -->
    <!-- /Page Content -->



</asp:Content>


