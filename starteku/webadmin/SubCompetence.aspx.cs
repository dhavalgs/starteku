﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using startetku.Business.Logic;

public partial class webadmin_SubCompetence : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["UserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            subCompetenceSelectAll();
            subCompetenceSelectArchiveAll();
        }
    }
    #endregion

    #region method

    protected void SetDefaultMessage()
    {
        if (!string.IsNullOrEmpty(Convert.ToString(Request.QueryString["msg"])))
        {
            if (Convert.ToString(Request.QueryString["msg"]) == "del")
            {
                lblMsg.Text = CommonModule.msgRecordHasBeenDeletedSuccessfully;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "ins")
            {
                lblMsg.Text = CommonModule.msgRecordInsertedSuccessfully;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "upd")
            {
                lblMsg.Text = CommonModule.msgRecordUpdatedSuccss;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
            else if (Convert.ToString(Request.QueryString["msg"]) == "arc")
            {
                lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                lblMsg.ForeColor = System.Drawing.Color.White;
            }
        }

    }
    protected void subCompetenceSelectAll()
    {
        SubCompetenceBM obj = new SubCompetenceBM();
        obj.subIsActive = true;
        obj.subIsDeleted = false;
        obj.subComId = Convert.ToInt32(Request.QueryString["id"]);
        obj.GetAllSubCompetenceMaster();
        DataSet ds = obj.ds;
        ViewState["ds"] = ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvGrid.DataSource = ds.Tables[0];
                gvGrid.DataBind();

                gvGrid.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvGrid.DataSource = null;
                gvGrid.DataBind();
            }
        }
        else
        {
            gvGrid.DataSource = null;
            gvGrid.DataBind();
        }
    }
    protected void subCompetenceSelectArchiveAll()
    {
        SubCompetenceBM obj = new SubCompetenceBM();
        obj.subIsActive = false;
        obj.subIsDeleted = false;
        obj.subComId = Convert.ToInt32(Request.QueryString["id"]);
        obj.GetAllSubCompetenceMaster();
        DataSet ds = obj.ds;


        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvArchive.DataSource = ds.Tables[0];
                gvArchive.DataBind();

                gvArchive.HeaderRow.TableSection = TableRowSection.TableHeader;
            }
            else
            {
                gvArchive.DataSource = null;
                gvArchive.DataBind();
            }
        }
        else
        {
            gvArchive.DataSource = null;
            gvArchive.DataBind();
        }
    }
  
    #endregion

    #region grid Event
    protected void gvGrid_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "archive")
        {
            SubCompetenceBM obj = new SubCompetenceBM();
            obj.subId = Convert.ToInt32(e.CommandArgument);
            obj.subIsActive = false;
            obj.subIsDeleted = false;
            obj.subCompetenceMasterUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    subCompetenceSelectAll();
                    subCompetenceSelectArchiveAll();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.White;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
        

    }
    protected void gvArchive_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "archive")
        {

            SubCompetenceBM obj = new SubCompetenceBM();
            obj.subId = Convert.ToInt32(e.CommandArgument);
            obj.subIsActive = true;
            obj.subIsDeleted = false;
            obj.subCompetenceMasterUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    subCompetenceSelectAll();
                    subCompetenceSelectArchiveAll();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.Green;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
        else if (e.CommandName == "permanentlydelete")
        {
            
            SubCompetenceBM obj = new SubCompetenceBM();
            obj.subId = Convert.ToInt32(e.CommandArgument);
            obj.subIsActive = false;
            obj.subIsDeleted = true;
            obj.subCompetenceMasterUpdate();

            DataSet ds = obj.ds;
            if (ds != null)
            {
                string returnMsg = Convert.ToString(ds.Tables[0].Rows[0]["returnStatement"]);
                if (returnMsg == "success")
                {
                    subCompetenceSelectAll();
                    subCompetenceSelectArchiveAll();
                    lblMsg.Text = CommonModule.msgRecordArchiveSucces;
                    lblMsg.ForeColor = System.Drawing.Color.Green;
                }
                else
                {
                    lblMsg.Text = CommonModule.msgSomeProblemOccure;
                }
            }
            else
            {
                lblMsg.Text = CommonModule.msgSomeProblemOccure;
            }
        }
    }
    #endregion


    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
       // InsertsubCompetenceMaster();
       // subCompetenceSelectAll();
    }
    protected void btnupdate_click(object sender, EventArgs e)
    {
        Int32 id = Convert.ToInt32(ViewState["id"]);
       // UpdateSubCompetence(id);
       // subCompetenceSelectAll();
    }
    protected void btnclose_click1(object sender, EventArgs e)
    {
        //clear();
    }
    protected void btnsave_click(object sender, EventArgs e)
    {
        Response.Redirect("EditSubCompetence.aspx?id="+ Request.QueryString["id"]);
        //Response.Redirect("AdditionalNappyChange.aspx?cent=" + arg2.ToString() + "&rm=" + arg3.ToString() + "&lgId=" + arg4.ToString());
    }
    #endregion
}