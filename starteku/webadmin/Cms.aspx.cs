﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using System.Threading;
using System.Globalization;

public partial class webadmin_Cms : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["UserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                GetAllCMSbyid();
            }
        }
    }
    #endregion

    #region method
    protected void SetDefaultMessage()
    {

        string id = "?id=";
        string childurl = "child.aspx";
        if (Request.RawUrl.ToString().ToLower().Contains(childurl.ToLower()))
        {
            if (Request.RawUrl.ToString().ToLower().Contains(id.ToLower()))
            {
                lblDataDisplayTitle.Text = "Edit Children";

            }
            else
            {
                lblDataDisplayTitle.Text = "Add Children";
            }
        }
    }
    protected void InsertCMS()
    {
        try
        {
            if (String.IsNullOrEmpty(txtDescription.Text))
            {
                lblmsgDescription.Text = "Please enter Description.";
            }

            if (String.IsNullOrEmpty(txtDescriptionDN.Text))
            {
                lblmsgDescriptionDN.Text = "Please enter Description in Danish.";
            }
            else
            {
                CmsBM obj = new CmsBM();
                obj.cmsName = txtName.Text;
                obj.cmsDescription = txtDescription.Text;
                obj.cmsDescriptionDN = txtDescriptionDN.Text;
                obj.cmsCompanyId = 0;
                obj.cmsIsActive = true;
                obj.cmsIsDeleted = false;
                obj.cmsCreatedDate = DateTime.Now;
                obj.InsertCms();
                if (obj.ReturnBoolean == true)
                {
                    Response.Redirect("CmsList.aspx?msg=ins");
                }
            }
        }
        catch (DataException ex)
        {
            throw ex;
        }

    }
    protected void updateCMS()
    {
        try
        {
            CmsBM obj = new CmsBM();
            obj.cmsId = Convert.ToInt32(Request.QueryString["id"]);
            obj.cmsName = txtName.Text;
            obj.cmsDescription = txtDescription.Text;
            obj.cmsDescriptionDN = txtDescriptionDN.Text;
            obj.cmsUpdatedDate = DateTime.Now;
            obj.UpdateCms();
            //  DataSet ds = obj.ds;
            if (obj.ReturnBoolean == true)
            {
                Response.Redirect("CmsList.aspx?msg=upd");
            }
        }
        catch (DataException ex)
        {
            throw ex;
        }

    }
    protected void GetAllCMSbyid()
    {
        try
        {
            CmsBM obj = new CmsBM();
            obj.cmsId = Convert.ToInt32(Request.QueryString["id"]);
            obj.GetAllcmsbyid();
            DataSet ds = obj.ds;

            if (ds != null)
            {
                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["cmsName"])))
                    txtName.Text = Convert.ToString(ds.Tables[0].Rows[0]["cmsName"]);

                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["cmsDescription"])))
                    txtDescription.Text = Convert.ToString(ds.Tables[0].Rows[0]["cmsDescription"]);

                if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["cmsDescriptionDN"])))
                    txtDescriptionDN.Text = Convert.ToString(ds.Tables[0].Rows[0]["cmsDescriptionDN"]);
            }
        }
        catch (DataException ex)
        {
            throw ex;
        }
    }

    #endregion


    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Request.QueryString["id"]))
        {
            updateCMS();
        }
        else
        {
            InsertCMS();
        }
    }
    protected void btnCancel_click(object sender, EventArgs e)
    {
        Response.Redirect("CmsList.aspx");
    }
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        string language = Convert.ToString(Session["Cultureadmin"]);
        //string language = "Denish";
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Denish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Languageadmin"] != null)
        {
            if (!Session["Languageadmin"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Languageadmin"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Languageadmin"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
}