﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webadmin/AdminMasterPage.master"
    AutoEventWireup="true" CodeFile="Registration.aspx.cs" Inherits="webadmin_Registration" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <style type="text/css">
        .chkboxlist
        {
            border: 1px #CCCCCC solid;
        }
        
        .chkboxlist_over
        {
            height: 365px;
            border: 1px #CCCCCC solid;
            overflow: scroll;
        }
        
        #ContentPlaceHolder1_chk_list_Departments
        {
            width: 100%;
        }
        
        #ContentPlaceHolder1_chk_list_Departments tr
        {
            height: 30px;
            color: gray;
        }
        
        #ContentPlaceHolder1_chk_list_Departments tr :hover
        {
            background-color: #CCCCCC;
            color: white;
        }
        
        #ContentPlaceHolder1_chk_list_Departments tr input
        {
            margin: 10px;
        }
        
        #ContentPlaceHolder1_chk_list_Departments tr td label
        {
            vertical-align: 3px;
            user-select: none;
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            width: 90%;
        }
        
        #ContentPlaceHolder1_chk_list_Division
        {
            width: 100%;
        }
        
        #ContentPlaceHolder1_chk_list_Division tr
        {
            height: 30px;
            color: gray;
        }
        
        #ContentPlaceHolder1_chk_list_Division tr :hover
        {
            background-color: #CCCCCC;
            color: white;
        }
        
        #ContentPlaceHolder1_chk_list_Division tr input
        {
            margin: 10px;
        }
        
        #ContentPlaceHolder1_chk_list_Division tr td label
        {
            vertical-align: 3px;
            user-select: none;
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            -khtml-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            width: 90%;
        }
    </style>
    <script type="text/javascript">
        $(document).ready(function () {
            if ($("#contener_chk_list").height() >= 365) {
                $("#contener_chk_list").height(200).css({
                    "border": "1px #CCCCCC solid",
                    "overflow": "scroll"
                });
            }
            else {
                $("#contener_chk_list").css({
                    "border": "1px #CCCCCC solid"
                });
            }

            if ($("#contener_chk_list1").height() >= 365) {
                $("#contener_chk_list1").height(200).css({
                    "border": "1px #CCCCCC solid",
                    "overflow": "scroll"
                });
            }
            else {
                $("#contener_chk_list1").css({
                    "border": "1px #CCCCCC solid"
                });
            }

        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" 
        meta:resourcekey="lblMsgResource1"></asp:Label>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12">
                        <div class="col-md-12">
                            <div class="indicatesRequireFiled">
                                *<%--<%= CommonMessages.Indicatesrequiredfield%>--%>
                                <asp:Literal ID="Literal9" runat="server" meta:resourcekey="Indicatesrequiredfield" enableviewstate="false"/>

                            </div>
                            <div class="widget box">
                                <div class="widget-header">
                                    <h4>
                                        <i class="icon-reorder"></i>
                                        <asp:Label runat="server" ID="lblDataDisplayTitle" Text="Registration" 
                                            meta:resourcekey="lblDataDisplayTitleResource1"></asp:Label>
                                    </h4>
                                </div>
                                <div class="widget-content">
                                    <div class="form-horizontal row-border">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                        <%-- <%= CommonMessages.CompanyName%>--%>
                                          <asp:Literal ID="Literal1" runat="server" meta:resourcekey="CompanyName" enableviewstate="false"/>:<span class="starValidation">*</span></label>
                                            <div class="col-md-10">
                                                <asp:TextBox runat="server" ID="txtcomname" MaxLength="50" onkeyup="run(this)"
                                                    CssClass="form-control" meta:resourcekey="txtcomnameResource1" />
                                                <span style="color: Gray; font-weight: normal;">[Max 50 alphanumeric]</span><br />
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                    TargetControlID="txtcomname" Enabled="True" ValidChars=" ">
                                                </cc1:FilteredTextBoxExtender>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ControlToValidate="txtcomname"
                                                    ErrorMessage="Please enter Company Name." CssClass="commonerrormsg" Display="Dynamic"
                                                    ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator7Resource1"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                           <%--  <%= CommonMessages.CompanyPhone%>--%>
                                              <asp:Literal ID="Literal2" runat="server" meta:resourcekey="CompanyPhone" enableviewstate="false"/>:<span class="starValidation">*</span></label>
                                            <div class="col-md-10">
                                                <asp:TextBox runat="server" ID="txtcphone" CssClass="form-control" 
                                                    MaxLength="15" meta:resourcekey="txtcphoneResource1"></asp:TextBox>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtcphone"
                                                    ErrorMessage="Minimum mobile length is 10." Display="Dynamic" CssClass="commonerrormsg"
                                                    ValidationGroup="chk" ValidationExpression=".{10}.*" 
                                                    meta:resourcekey="RegularExpressionValidator1Resource1" />
                                                <%--<asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtcphone"
                                                    ErrorMessage="Minimum mobile length is 10." Display="Dynamic" CssClass="commonerrormsg"
                                                    ValidationGroup="chk" ValidationExpression="^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$" />--%>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ControlToValidate="txtcphone"
                                                    ErrorMessage="Please enter mobile." CssClass="commonerrormsg" Display="Dynamic"
                                                    ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator8Resource1"></asp:RequiredFieldValidator>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" FilterType="Custom, Numbers"
                                                    ValidChars=" ,+,(,),-" TargetControlID="txtcphone" Enabled="True">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                               <%-- <%= CommonMessages.CompanyAddress%>--%>
                                                 <asp:Literal ID="Literal3" runat="server" meta:resourcekey="CompanyAddress" enableviewstate="false"/>:<span class="starValidation">*</span></label>
                                            <div class="col-md-10">
                                                <asp:TextBox runat="server" ID="txtcaddress" CssClass="form-control" MaxLength="500"
                                                    TextMode="MultiLine" meta:resourcekey="txtcaddressResource1"></asp:TextBox>
                                                <span style="color: Gray; font-weight: normal;">[Max 500 alphanumeric]</span><br />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" CssClass="commonerrormsg"
                                                    ControlToValidate="txtcaddress" Display="Dynamic" ValidationGroup="chk" 
                                                    ErrorMessage="Address is required." 
                                                    meta:resourcekey="RequiredFieldValidator9Resource1"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                <%-- <%= CommonMessages.FirstName%>--%>
                                                  <asp:Literal ID="Literal4" runat="server" meta:resourcekey="FirstName" enableviewstate="false"/>:<span class="starValidation">*</span></label>
                                            <div class="col-md-10">
                                                <asp:TextBox runat="server" ID="txtfName" MaxLength="50" onkeyup="run(this)"
                                                    CssClass="form-control" meta:resourcekey="txtfNameResource1" />
                                                <span style="color: Gray; font-weight: normal;">[Max 50 alphanumeric]</span><br />
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                    TargetControlID="txtfName" Enabled="True" ValidChars=" ">
                                                </cc1:FilteredTextBoxExtender>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtfName"
                                                    ErrorMessage="Please enter First Name." CssClass="commonerrormsg" Display="Dynamic"
                                                    ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator5Resource1"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                               <%-- <%= CommonMessages.LastName%>--%>
                                                <asp:Literal ID="Literal5" runat="server" meta:resourcekey="LastName" enableviewstate="false"/>:<span class="starValidation">*</span></label>
                                            <div class="col-md-10">
                                                <asp:TextBox runat="server" ID="txtlname" MaxLength="50" onkeyup="run(this)"
                                                    CssClass="form-control" meta:resourcekey="txtlnameResource1" />
                                                <span style="color: Gray; font-weight: normal;">[Max 50 alphanumeric]</span><br />
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                    TargetControlID="txtlname" Enabled="True" ValidChars=" ">
                                                </cc1:FilteredTextBoxExtender>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtlname"
                                                    ErrorMessage="Please enter Last name." CssClass="commonerrormsg" Display="Dynamic"
                                                    ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator1Resource1"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                               <%-- <%= CommonMessages.Gender%>--%>
                                                 <asp:Literal ID="Literal6" runat="server" meta:resourcekey="Gender" enableviewstate="false"/>:<span class="starValidation">*</span></label>
                                            <div class="col-md-10">
                                                <asp:DropDownList ID="ddlgender" runat="server" Style="width: 100px; height: 32px;
                                                    display: inline;" meta:resourcekey="ddlgenderResource1">
                                                    <asp:ListItem Value="Male" Text="Male" meta:resourcekey="ListItemResource1"></asp:ListItem>
                                                    <asp:ListItem Value="Female" Text="Female" meta:resourcekey="ListItemResource2"></asp:ListItem>
                                                </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                Email:<span class="starValidation">*</span></label>
                                            <div class="col-md-10">
                                                <asp:TextBox runat="server" ID="txtEmail" MaxLength="80" onkeyup="run(this)"
                                                    CssClass="form-control" meta:resourcekey="txtEmailResource1" />
                                                <asp:RequiredFieldValidator ID="rfvEmail" runat="server" CssClass="commonerrormsg"
                                                    ControlToValidate="txtEmail" Display="Dynamic" ValidationGroup="chk" 
                                                    ErrorMessage="Please enter email." meta:resourcekey="rfvEmailResource1"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revEmail" runat="server" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                    ControlToValidate="txtEmail" CssClass="commonerrormsg" Display="Dynamic" ValidationGroup="chk"
                                                    ErrorMessage="Please enter valid email." 
                                                    meta:resourcekey="revEmailResource1"></asp:RegularExpressionValidator>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                 <%-- <%= CommonMessages.Password%>--%>
                                                  <asp:Literal ID="Literal7" runat="server" meta:resourcekey="Password" enableviewstate="false"/>:<span class="starValidation">*</span></label>
                                            <div class="col-md-10">
                                                <asp:TextBox ID="txtPassword" class="form-control" runat="server" MaxLength="20"
                                                    TextMode="Password" meta:resourcekey="txtPasswordResource1" />
                                                <asp:RequiredFieldValidator ID="rfvPassword" runat="server" CssClass="commonerrormsg"
                                                    ControlToValidate="txtPassword" Display="Dynamic" ValidationGroup="chk" 
                                                    ErrorMessage="Password is required." meta:resourcekey="rfvPasswordResource1"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="revPassword" runat="server" ControlToValidate="txtpassword"
                                                    ValidationExpression="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4}.*$" CssClass="commonerrormsg"
                                                    Display="Dynamic" ValidationGroup="chk" 
                                                    ErrorMessage="Password must be at least 4 characters, no more than 8 characters, and must include at least one upper case letter, one lower case letter, and one numeric digit" 
                                                    meta:resourcekey="revPasswordResource1" />
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                              <%--  <%= CommonMessages.ConfirmPassword%>--%>
                                                <asp:Literal ID="Literal8" runat="server" meta:resourcekey="ConfirmPassword" enableviewstate="false"/>:<span class="starValidation">*</span></label>
                                            <div class="col-md-10">
                                                <asp:TextBox ID="txtConfirmPassword" type="text" class="form-control" runat="server"
                                                    MaxLength="20" TextMode="Password" 
                                                    meta:resourcekey="txtConfirmPasswordResource1" />
                                                <asp:CompareValidator ID="cvPasswordNotMatch" ControlToValidate="txtConfirmPassword"
                                                    ControlToCompare="txtPassword" runat="server" CssClass="commonerrormsg" Display="Dynamic"
                                                    ValidationGroup="chk" 
                                                    ErrorMessage="Password and confirm password does not match." 
                                                    meta:resourcekey="cvPasswordNotMatchResource1"></asp:CompareValidator>
                                                <asp:RequiredFieldValidator ID="rfvConfirmPassword" runat="server" CssClass="commonerrormsg"
                                                    ControlToValidate="txtConfirmPassword" Display="Dynamic" ErrorMessage="Confirm password is required."
                                                    ValidationGroup="chk" meta:resourcekey="rfvConfirmPasswordResource1"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                        <%--  <%= CommonMessages.MobileNo%>--%>
                                          <asp:Literal ID="Literal10" runat="server" meta:resourcekey="MobileNo" enableviewstate="false"/>:<span class="starValidation"></span></label>
                                            <div class="col-md-10">
                                                <asp:TextBox runat="server" ID="txtMobile" CssClass="form-control" 
                                                    MaxLength="15" meta:resourcekey="txtMobileResource1"></asp:TextBox>
                                                <%-- <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ControlToValidate="txtMobile"
                                                    ErrorMessage="Minimum mobile length is 9." Display="Dynamic" CssClass="commonerrormsg"
                                                    ValidationGroup="chk" ValidationExpression="^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$" />--%>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtcphone"
                                                    ErrorMessage="Minimum mobile length is 10." Display="Dynamic" CssClass="commonerrormsg"
                                                    ValidationGroup="chk" ValidationExpression=".{10}.*" 
                                                    meta:resourcekey="RegularExpressionValidator2Resource1" />
                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="txtMobile"
                                                    ErrorMessage="Please enter mobile." CssClass="commonerrormsg" Display="Dynamic"
                                                    ValidationGroup="chk"></asp:RequiredFieldValidator>--%>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Custom, Numbers"
                                                    ValidChars=" ,+,(,),-" TargetControlID="txtMobile" Enabled="True">
                                                </cc1:FilteredTextBoxExtender>
                                            </div>
                                        </div>
                                        <asp:UpdatePanel ID="qw" runat="server">
                                            <ContentTemplate>
                                                <div class="form-group">
                                                    <label class="col-md-2 control-label">
                                                   <%-- <%= CommonMessages.Country%>--%>
                                                    <asp:Literal ID="Literal11" runat="server" meta:resourcekey="Country" enableviewstate="false"/>:<span class="starValidation">*</span></label>
                                                    <div class="col-md-10">
                                                        <asp:DropDownList ID="ddlcountry" runat="server" CssClass="form-control" 
                                                            Style="display: inline;" AutoPostBack="True"
                                                        OnSelectedIndexChanged="ddlcountry_SelectedIndexChanged" 
                                                            meta:resourcekey="ddlcountryResource1">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator14" runat="server" ControlToValidate="ddlcountry"
                                                            ErrorMessage="Please select Country." InitialValue="0" CssClass="commonerrormsg"
                                                            Display="Dynamic" ValidationGroup="chk" 
                                                            meta:resourcekey="RequiredFieldValidator14Resource1"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                                <div class="form-group" id="state" runat="server" style="display: none">
                                                    <label class="col-md-2 control-label">
                                                      <%--  <%= CommonMessages.State%>--%>
                                                         <asp:Literal ID="Literal12" runat="server" meta:resourcekey="State" enableviewstate="false"/>:<span class="starValidation">*</span></label>
                                                    <div class="col-md-10">
                                                        <asp:DropDownList ID="ddlstate" runat="server" CssClass="form-control" 
                                                            Style=" display: inline;" 
                                                            OnSelectedIndexChanged="ddlstate_SelectedIndexChanged" 
                                                            meta:resourcekey="ddlstateResource1">
                                                        </asp:DropDownList>
                                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="ddlstate"
                                                            ErrorMessage="Please select State." InitialValue="0" CssClass="commonerrormsg"
                                                            Display="Dynamic" ValidationGroup="chk" 
                                                            meta:resourcekey="RequiredFieldValidator2Resource1"></asp:RequiredFieldValidator>
                                                    </div>
                                                </div>
                                            </ContentTemplate>
                                            <Triggers>
                                                <asp:AsyncPostBackTrigger ControlID="ddlcountry" EventName="SelectedIndexChanged" />
                                            </Triggers>
                                        </asp:UpdatePanel>
                                        <div class="form-group">
                                             <label class="col-md-2 control-label">
                                              <%-- <%= CommonMessages.City%>--%>
                                               <asp:Literal ID="Literal13" runat="server" meta:resourcekey="City" enableviewstate="false"/>:<span class="starValidation">*</span></label>
                                            <div class="col-md-10">
                                                <asp:TextBox runat="server" ID="txtcity" CssClass="form-control" MaxLength="50" meta:resourcekey="txtcityResource1"
                                                    ></asp:TextBox>
                                                <span style="color: Gray; font-weight: normal;">[Max 500 alphanumeric]</span><br />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator41" runat="server" CssClass="commonerrormsg"
                                                    ControlToValidate="txtcity" Display="Dynamic" ValidationGroup="chk" 
                                                    ErrorMessage="Address is required." 
                                                    meta:resourcekey="RequiredFieldValidator41Resource1"></asp:RequiredFieldValidator>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                DOB:
                                            </label>
                                            <div class="col-md-10">
                                             <asp:TextBox runat="server" ID="txtfromdate" MaxLength="10" CssClass="form-control"
                                                   contentEditable="false"  Width="170px" style="display: initial;cursor: default;" 
                                                    meta:resourcekey="txtfromdateResource1" />                
                                                 <img id="imgDateofBirth2"  src="../images/Calendar_scheduleHS.png" style="margin:2px; height: 26px;width: 26px;"/>                      
                                                <cc1:CalendarExtender ID="CalendarExtender2" PopupPosition="TopLeft" runat="server" Enabled="True"
                                                    TargetControlID="txtfromdate" Format="dd/MM/yyyy" PopupButtonID="imgDateofBirth2" OnClientDateSelectionChanged="checkDate">
                                                </cc1:CalendarExtender> 
                                                <cc1:CalendarExtender ID="CalendarExtender1" PopupPosition="TopLeft" runat="server"
                                                    TargetControlID="txtfromdate" Format="dd/MM/yyyy" 
                                                    PopupButtonID="txtfromdate" OnClientDateSelectionChanged="checkDate" 
                                                    Enabled="True">
                                                </cc1:CalendarExtender>          
                                                <%--<asp:TextBox runat="server" Text="" ID="txtfromdate" MaxLength="10" CssClass="form-control" ReadOnly="true"/>
                                                <cc1:CalendarExtender ID="CalendarExtender2" PopupPosition="TopLeft" runat="server"
                                                    TargetControlID="txtfromdate" Format="dd/MM/yyyy">
                                                </cc1:CalendarExtender>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator13" runat="server" ControlToValidate="txtfromdate"
                                                    ErrorMessage="Please enter special price start date." CssClass="commonerrormsg"
                                                    Display="Dynamic" Visible="false" ValidationGroup="chk"></asp:RequiredFieldValidator>--%>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                <%-- <%= CommonMessages.Address%>--%>
                                                 <asp:Literal ID="Literal14" runat="server" meta:resourcekey="Address" enableviewstate="false"/>:<span class="starValidation"></span></label>
                                            <div class="col-md-10">
                                                <asp:TextBox runat="server" ID="txtaddress" CssClass="form-control" MaxLength="500"
                                                    TextMode="MultiLine" meta:resourcekey="txtaddressResource1"></asp:TextBox>
                                                <span style="color: Gray; font-weight: normal;">[Max 500 alphanumeric]</span><br />
                                                <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" CssClass="commonerrormsg"
                                                    ControlToValidate="txtaddress" Display="Dynamic" ValidationGroup="chk" ErrorMessage="Address is required."></asp:RequiredFieldValidator>--%>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">
                                                Zip:<span class="starValidation"></span></label>
                                            <div class="col-md-10">
                                                <asp:TextBox runat="server" ID="txtzip" CssClass="form-control" MaxLength="15" 
                                                    meta:resourcekey="txtzipResource1"></asp:TextBox>
                                            </div>
                                        </div>
                                        <div class="form-group" style="display:none;">
                                            <label class="col-md-2 control-label">
                                                Image: <span class="starValidation"></span>
                                            </label>
                                            <div class="col-md-10">
                                                <asp:Image ID="img" runat="server" Width="100px" Height="100px" 
                                                    meta:resourcekey="imgResource1" />
                                                <asp:FileUpload ID="flupload1" runat="server" 
                                                    meta:resourcekey="flupload1Resource1"></asp:FileUpload>
                                                <span style="color: Gray; font-weight: normal;"></span>
                                                <br />
                                                <asp:RegularExpressionValidator ID="reFile1" runat="server" ControlToValidate="flupload1"
                                                    CssClass="legend" Display="Dynamic" ErrorMessage="Please upload only .gif | .jpeg | .jpg   | .png image."
                                                    ValidationExpression="^.*\.(JPG|jpg|jpeg|PNG|png|JPEG)$" ForeColor="Red" 
                                                    ValidationGroup="chk1" meta:resourcekey="reFile1Resource1"></asp:RegularExpressionValidator>
                                            </div>
                                        </div>
                                        <div class="form-group" style="display:none;">
                                            <label class="col-md-2 control-label">
                                                Departments:<span class="starValidation"></span></label>
                                            <div class="col-md-10">
                                                <div id="contener_chk_list" class="chkboxlist">
                                                    <asp:CheckBoxList ID="chk_list_Departments" runat="server" 
                                                        meta:resourcekey="chk_list_DepartmentsResource1">
                                                    </asp:CheckBoxList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group" style="display:none;">
                                            <label class="col-md-2 control-label">
                                                Division:<span class="starValidation"></span></label>
                                            <div class="col-md-10">
                                                <div id="contener_chk_list1" class="chkboxlist">
                                                    <asp:CheckBoxList ID="chk_list_Division" runat="server" 
                                                        meta:resourcekey="chk_list_DivisionResource1">
                                                    </asp:CheckBoxList>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-horizontal row-border">
                                            <div class="btn-toolbar">
                                                <asp:Button runat="server" ID="btnsubmit" Text="Save" CssClass="btn btn-success"
                                                    ValidationGroup="chk" OnClick="btnsubmit_click" 
                                                    meta:resourcekey="btnsubmitResource1" />
                                                <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="btn" ValidationGroup="chk"
                                                    CausesValidation="False" OnClick="btnCancel_click" 
                                                    meta:resourcekey="btnCancelResource1" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        function checkDate(sender, args) {

            if (sender._selectedDate > new Date()) {
                alert("You cannot select a future Date!");
                sender._selectedDate = new Date();
                // set the date back to the current date
                sender._textbox.set_Value(sender._selectedDate.format(sender._format))
            }

        }
        </script>
</asp:Content>
