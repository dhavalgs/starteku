﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webadmin/AdminMasterPage.master"
    AutoEventWireup="true" CodeFile="EditSubCompetence.aspx.cs" Inherits="webadmin_EditSubCompetence" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg"></asp:Label>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12">
                <div class="indicatesRequireFiled">
                    * Indicates required field
                </div>
                <div class="widget box">
                    <div class="widget-header">
                        <h4>
                            <i class="icon-reorder"></i>
                            <asp:Label runat="server" ID="lblDataDisplayTitle" Text="Competence"></asp:Label>
                        </h4>
                    </div>
                    <div class="widget-content">
                        <%--   <form class="form-horizontal row-border" action="#">--%>
                        <div class="form-horizontal row-border">
                        <div class="form-group" id="divCompetence" runat="server">
                                <label class="col-md-2 control-label">
                                   Competence:<span class="starValidation"></span></label>
                                <div class="col-md-2 control-label" style="margin-left:-156px;">
                                    <asp:Label ID="lblCompetence" runat="server" Text="Label" ></asp:Label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                   Sub Competence Name:<span class="starValidation">*</span></label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" Text="" ID="txtcountryName" MaxLength="50" CssClass="form-control" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtcountryName"
                                        ErrorMessage="Please Enter Sub Competence Name." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="chk"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                   Description:<span class="starValidation">*</span></label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" Text="" ID="txtDescription" MaxLength="50" CssClass="form-control" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDescription"
                                        ErrorMessage="Please Enter Description ." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="chk"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <div class="form-horizontal row-border">
                                <div class="btn-toolbar">
                                    <asp:Button runat="server" ID="btnsubmit" Text="Save" CssClass="btn btn-success"
                                        ValidationGroup="chk" OnClick="btnsubmit_click" />
                                    <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="btn" ValidationGroup="chk"
                                        CausesValidation="false" OnClick="btnCancel_click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--=== no-padding ===-->
    <div class="row">
    </div>
    <div class="row">
    </div>
    <!-- /no-padding and table-tabletools -->
    <!--=== no-padding and table-colvis ===-->
    <div class="row">
    </div>
    <!-- /no-padding and table-colvis -->
    <!--=== Horizontal Scrolling ===-->
    <div class="row">
    </div>
    <!-- /Normal -->
    <!-- /Page Content -->
</asp:Content>
