﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="resetpassword.aspx.cs" Inherits="webadmin_resetpassword" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <title>Forgot Password</title>
    <!--=== CSS ===-->
    <!-- Bootstrap -->
    <link href="../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme -->
    <link href="../assets/css/main.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/plugins.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/responsive.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/icons.css" rel="stylesheet" type="text/css" />
    <link href="../assets/css/StyleSheet.css" rel="stylesheet" type="text/css" />
    <!-- Login -->
    <link href="../assets/css/login.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../assets/css/fontawesome/font-awesome.min.css">
    <!--[if IE 7]>
		<link rel="stylesheet" href="assets/css/fontawesome/font-awesome-ie7.min.css">
	<![endif]-->
    <!--[if IE 8]>
		<link href="assets/css/ie8.css" rel="stylesheet" type="text/css" />
	<![endif]-->
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700' rel='stylesheet'
        type='text/css'>
    <!--=== JavaScript ===-->
    <script type="text/javascript" src="../assets/js/libs/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="../bootstrap/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../assets/js/libs/lodash.compat.min.js"></script>
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
		<script src="assets/js/libs/html5shiv.js"></script>
	<![endif]-->
    <!-- Beautiful Checkboxes -->
    <script type="text/javascript" src="../plugins/uniform/jquery.uniform.min.js"></script>
    <!-- Form Validation -->
    <script type="text/javascript" src="../plugins/validation/jquery.validate.min.js"></script>
    <!-- Slim Progress Bars -->
    <script type="text/javascript" src="../plugins/nprogress/nprogress.js"></script>
    <!-- App -->
    <script type="text/javascript" src="../assets/js/login.js"></script>
    <script>
        $(document).ready(function () {
            "use strict";

            Login.init(); // Init login JavaScript
        });
    </script>
</head>
<body class="login">
    <form id="Form1" runat="server">
    <!-- Logo -->
    <div class="logo">
       <%-- <img src="../assets/img/strateku_centreret_fv.png" alt="logo" />--%>
         <asp:Image runat="server" ID="imgLoginLogo" AlternateText="logo" style="max-width: 15%;"></asp:Image>
        <!--	<strong>ME</strong>LON-->
    </div>
    <!-- /Logo -->
    <!-- Login Box -->
    <div class="box">
        <div class="content">
            <!-- Login Formular -->
            <!-- Title -->
            <h3 class="form-title">
                Reset Password?</h3>
            <div class="form-group">
                <!--<label for="email">Email:</label>-->
                <div class="input-icon">
                    <i class="icon-envelope"></i>
                    <asp:TextBox ID="txtPassword" runat="server" CssClass="form-control" placeholder="Enter Password"
                        MaxLength="200"></asp:TextBox>
                    <asp:RequiredFieldValidator ID="rfvPassword" runat="server" CssClass="commonerrormsg"
                        ControlToValidate="txtPassword" Display="Dynamic" ValidationGroup="chk" ErrorMessage="Password is required."></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="revPassword" runat="server" ControlToValidate="txtpassword"
                        ValidationExpression="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,8}$" CssClass="commonerrormsg"
                        Display="Dynamic" ValidationGroup="chk" ErrorMessage="
Password must be at least 4 characters, no more than 8 characters, and must include at least one upper case letter, one lower case letter, and one numeric digit" />
                </div>
            </div>
            <div class="form-group">
                <!--<label for="email">Email:</label>-->
                <div class="input-icon">
                    <i class="icon-envelope"></i>
                    <asp:TextBox ID="txtConfirmPassword" runat="server" CssClass="form-control" placeholder="Enter ConfirmPassword"
                        MaxLength="200"></asp:TextBox>
                    <asp:CompareValidator ID="cvPasswordNotMatch" ControlToValidate="txtConfirmPassword"
                        ControlToCompare="txtPassword" runat="server" CssClass="commonerrormsg" Display="Dynamic"
                        ValidationGroup="chk" ErrorMessage="Password and confirm password does not match."></asp:CompareValidator>
                    <asp:RequiredFieldValidator ID="rfvConfirmPassword" runat="server" CssClass="commonerrormsg"
                        ControlToValidate="txtConfirmPassword" Display="Dynamic" ErrorMessage="Confirm password is required."
                        ValidationGroup="chk"></asp:RequiredFieldValidator>
                </div>
            </div>
            
            <!-- /Input Fields -->
            <asp:Button runat="server" ID="btnsubmit" Text="Submit" CssClass="submit btn btn-default btn-block"
                ValidationGroup="chk" OnClick="btnForgot_ServerClick" />
            <!-- /Forgot Password Formular -->
            <!-- Shows up if reset-button was clicked -->
            <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg"></asp:Label>
            <div class="forgot-password-done hide-default">
                <i class="icon-ok success-icon"></i>
                <!-- Error-Alternative: <i class="icon-remove danger-icon"></i> -->
                <span></span>
            </div>
        </div>
        <!-- /.content -->
    </div>
    <!-- /Login Box -->
    <!-- Single-Sign-On (SSO) -->
    <!--<div class="single-sign-on">
		<span>or</span>

		<button class="btn btn-facebook btn-block">
			<i class="icon-facebook"></i> Sign in with Facebook
		</button>

		<button class="btn btn-twitter btn-block">
			<i class="icon-twitter"></i> Sign in with Twitter
		</button>

		<button class="btn btn-google-plus btn-block">
			<i class="icon-google-plus"></i> Sign in with Google
		</button>
	</div>-->
    <!-- /Single-Sign-On (SSO) -->
    <!-- Footer -->
    <div class="footer">
        <%-- <a href="login.aspx" class="sign-up">Don't have an account yet? <strong>Sign Up</strong></a>--%>
    </div>
    <!-- /Footer -->
    </form>
</body>
</html>
