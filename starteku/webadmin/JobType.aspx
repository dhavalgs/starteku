﻿<%@ Page Title="" Language="C#" MasterPageFile="~/webadmin/AdminMasterPage.master"
    AutoEventWireup="true" CodeFile="JobType.aspx.cs" Inherits="webadmin_JobType" culture="auto" meta:resourcekey="PageResource1" uiculture="auto" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <asp:Label runat="server" ID="lblMsg" CssClass="commonerrormsg" 
        meta:resourcekey="lblMsgResource1"></asp:Label>
    <div class="row">
        <div class="col-md-12">
            <div class="col-md-12">
                <div class="indicatesRequireFiled">
                    *  <asp:Literal ID="Literal5" runat="server" meta:resourcekey="Indicatesrequiredfield" EnableViewState="false" />
                  
                </div>
                <div class="widget box">
                    <div class="widget-header">
                        <h4>
                            <i class="icon-reorder"></i>
                            <asp:Label runat="server" ID="lblDataDisplayTitle" Text="Job Type" 
                                meta:resourcekey="lblDataDisplayTitleResource1"></asp:Label>
                        </h4>
                    </div>
                    <div class="widget-content">
                        <%--   <form class="form-horizontal row-border" action="#">--%>
                        <div class="form-horizontal row-border">
                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                 <%--   <%= CommonMessages.JobType%>--%>
                                <asp:Literal ID="Literal9" runat="server" meta:resourcekey="JobType" enableviewstate="false"/>:<span class="starValidation">*</span></label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="txtJobTypeName" MaxLength="50" 
                                        CssClass="form-control" meta:resourcekey="txtJobTypeNameResource1" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ControlToValidate="txtJobTypeName"
                                        ErrorMessage="Please enter Job Type Name." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator5Resource1"></asp:RequiredFieldValidator>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                 <%--   <%= CommonMessages.JobType%>--%>
                                <asp:Literal ID="Literal3" runat="server" meta:resourcekey="JobTypeinDanish" enableviewstate="false"/>:<span class="starValidation">*</span></label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="txtJobTypeNameDn" MaxLength="50" 
                                        CssClass="form-control" meta:resourcekey="txtJobTypeNameDnResource1" />
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtJobTypeNameDn"
                                        ErrorMessage="Please enter Job Type Name In Danish." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="chk" meta:resourcekey="RequiredFieldValidator1Resource1"></asp:RequiredFieldValidator>
                                </div>
                            </div>
                            <%--<div class="form-group">
                                <label class="col-md-2 control-label">
                                    Division:<span class="starValidation">*</span></label>
                                <div class="col-md-10">
                                    <asp:DropDownList ID="ddlDepartment" runat="server" Style="width: 100%; height: 32px;
                                        display: inline;">
                                    </asp:DropDownList>
                                </div>
                            </div>--%>
                            <%--internal style--%>
                            <style type="text/css">
                                .mycheckbox input[type="checkbox"]
                                {
                                    margin-right: 5px;
                                }
                            </style>

                            <div class="form-group" >
                                <label class="col-md-2 control-label">
                                    <%-- <%= CommonMessages.CATEGORY%>--%>
                                     <asp:Literal ID="Literal1" runat="server" meta:resourcekey="CATEGORY" enableviewstate="false"/>:<span class="starValidation">*</span></label>
                                <div class="col-md-10"  Style=" overflow: auto;border:1px solid #ccc;max-height: 200px;width: 68%;margin-left: 13px; ">
                                    <asp:CheckBoxList Width="100%" ID="chkList" runat="server" CssClass="FormText mycheckbox"
                                        RepeatColumns="1"  Datafield="description" DataValueField="value" 
                                        meta:resourcekey="chkListResource1">
                                    </asp:CheckBoxList>
                                    <asp:CustomValidator ID="CustomValidator1" ErrorMessage="Please select at least one item."
                                        ForeColor="Red" ClientValidationFunction="ValidateCheckBoxList" runat="server"
                                        ValidationGroup="chk" meta:resourcekey="CustomValidator1Resource1" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-2 control-label">
                                   <%-- <%= CommonMessages.Description%>--%>
                                     <asp:Literal ID="Literal2" runat="server" meta:resourcekey="Description" enableviewstate="false"/>:<%--<span class="starValidation">*</span>--%></label>
                                <div class="col-md-10">
                                    <asp:TextBox runat="server" ID="txtDESCRIPTION" MaxLength="500" TextMode="MultiLine"
                                        Rows="5" CssClass="form-control" 
                                        meta:resourcekey="txtDESCRIPTIONResource1" />
                                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtDESCRIPTION"
                                        ErrorMessage="Please Enter DESCRIPTION." CssClass="commonerrormsg" Display="Dynamic"
                                        ValidationGroup="chk"></asp:RequiredFieldValidator>--%>
                                </div>
                            </div>
                            <div class="form-horizontal row-border">
                                <div class="btn-toolbar">
                                    <asp:Button runat="server" ID="btnsubmit" Text="Save" CssClass="btn btn-success"
                                        ValidationGroup="chk" OnClick="btnsubmit_click" 
                                        meta:resourcekey="btnsubmitResource1" />
                                    <asp:Button runat="server" ID="btnCancel" Text="Cancel" CssClass="btn" ValidationGroup="chk"
                                        CausesValidation="False" OnClick="btnCancel_click" 
                                        meta:resourcekey="btnCancelResource1" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </div>
    </div>
    <!--=== no-padding ===-->
    <div class="row">
    </div>
    <div class="row">
    </div>
    <!-- /no-padding and table-tabletools -->
    <!--=== no-padding and table-colvis ===-->
    <div class="row">
    </div>
    <!-- /no-padding and table-colvis -->
    <!--=== Horizontal Scrolling ===-->
    <div class="row">
    </div>
    <!-- /Normal -->
    <!-- /Page Content -->

    <script type="text/javascript">
        function ValidateCheckBoxList(sender, args) {
            var checkBoxList = document.getElementById("<%=chkList.ClientID %>");
            var checkboxes = checkBoxList.getElementsByTagName("input");
            var isValid = false;
            for (var i = 0; i < checkboxes.length; i++) {
                if (checkboxes[i].checked) {
                    isValid = true;
                    break;
                }
            }
            args.IsValid = isValid;
        }
    </script>

</asp:Content>
