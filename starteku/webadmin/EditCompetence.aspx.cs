﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using startetku.Business.Logic;
using System.Threading;
using System.Globalization;

public partial class webadmin_EditCompetence : System.Web.UI.Page
{
    string id = "";
    string id1 = "";

    public static String GroupName
    {
        get
        {
            return "Test";
        }

    }
    DataTable dt = new DataTable();
    int queid = 0;
    DataSet ds;
    public static int ans = 0;

    #region Page Event
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["UserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            GetAllcompetenceCategory();
            firstimeload();
           
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                GetAllcompetenceCategory();
               // GetAllCompetenceMasterbyid();
            }
        }
    }
    #endregion

    #region method
    protected void SetDefaultMessage()
    {

        string id = "?id=";
        string childurl = "child.aspx";
        if (Request.RawUrl.ToString().ToLower().Contains(childurl.ToLower()))
        {
            if (Request.RawUrl.ToString().ToLower().Contains(id.ToLower()))
            {
                lblDataDisplayTitle.Text = "Edit Children";

            }
            else
            {
                lblDataDisplayTitle.Text = "Add Children";
            }
        }
    }
    protected void InsertCompetenceMaster()
    {
        CompetenceMasterBM obj = new CompetenceMasterBM();
        obj.comCompetence = txtCompetence.Text;
        obj.comCompetenceDN = txtCompetenceDN.Text;
        obj.comUserId = Convert.ToInt32(Session["UserId"]);
        obj.comIsActive = true;
        obj.comIsDeleted = false;
        obj.comCreatedDate = DateTime.Now;
        obj.comCompanyId = 0;
        obj.comCreateBy = 0;
        obj.comDivId = 0;
        obj.comcategoryId = Convert.ToInt32(ddlcategory.SelectedValue);
        queid = obj.InsertCompetenceMasteAdd();
        insertCompetencechild(queid);
        ViewState["queid"] = queid;
        // save_Que_and_ans();
        if (queid > 0)
        {
            Response.Redirect("Competence.aspx?msg=ins");
        }

        else
        {
            lblMsg.Text = CommonModule.msgProblemInsertRecord;
        }

    }
    protected void insertCompetencechild(Int32 id)
    {
        foreach (RepeaterItem item in Repeater1.Items)
        {
            TextBox box = (TextBox)item.FindControl("TextBox1");
            TextBox box1 = (TextBox)item.FindControl("TextBoxDN");
            CompetenceMasterBM obj = new CompetenceMasterBM();
            obj.comchildcomID = id;
            obj.comchildlevel = box.Text;
            obj.comchildlevelDN = box1.Text;
            obj.comIsActive = true;
            obj.comIsDeleted = false;
            obj.comCreatedDate = DateTime.Now;
            obj.InsertCompetencechild();
        }
    }
    protected void GetAllCompetenceAddbyid()
    {
        CompetenceMasterBM obj = new CompetenceMasterBM();
        obj.comId = Convert.ToInt32(Request.QueryString["id"]);
        obj.GetAllCompetenceAddbyid();
        ds = obj.ds;

        if (ds != null)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comCompetence"])))
                txtCompetence.Text = Convert.ToString(ds.Tables[0].Rows[0]["comCompetence"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comCompetenceDN"])))
                txtCompetenceDN.Text = Convert.ToString(ds.Tables[0].Rows[0]["comCompetenceDN"]);


            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comcategoryId"])))
                ddlcategory.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["comcategoryId"]);

            Repeater1.DataSource = ds.Tables[1];
            Repeater1.DataBind();

        }





    }
    protected void firstimeload()
    {
        GetAllcompetenceCategory();
        if (!string.IsNullOrEmpty(Request.QueryString["id"]))
        {
            GetAllCompetenceAddbyid();
            //int ans = ds.Tables[1].Rows.Count ;
            //for (int i = 0; i < ans; i++)
            //{
            //    ds.Tables[1].Rows.RemoveAt(ds.Tables[1].Rows.Count - 1);
            //}
            //if (ds.Tables[1].Rows.Count > Convert.ToInt32(5))
            //{
            //    int ans = ds.Tables[1].Rows.Count - Convert.ToInt32(5);
            //    for (int i = 0; i < ans; i++)
            //    {
            //        ds.Tables[1].Rows.RemoveAt(ds.Tables[1].Rows.Count - 1);
            //    }

            //}
            //else
            //{
            //    int ans = Convert.ToInt32(5) - ds.Tables[1].Rows.Count;
            //    for (int i = 0; i < ans; i++)
            //    {
            //        DataRow dr = ds.Tables[1].NewRow();
            //        dr["answer"] = " ";
            //        ds.Tables[1].Rows.Add(dr);
            //    }
            //}
            //Repeater1.DataSource = ds.Tables[1];
            //Repeater1.DataBind();
        }
        else
        {
            if (Repeater1.Controls.Count <= 0)
            {
                int val = Convert.ToInt32(5);
                dt.Columns.Add("comchildlevel", typeof(String));
                dt.Columns.Add("comchildlevelDN", typeof(String));

                for (int i = 0; i < val; i++)
                {
                    DataRow dr = dt.NewRow();
                    dr["comchildlevel"] = "";
                    dr["comchildlevelDN"] = "";
                    dt.Rows.Add(dr);
                }

                Repeater1.DataSource = dt;
                Repeater1.DataBind();
            }
            //else
            //{
            //    if ((Repeater1.Controls.Count) < Convert.ToInt32(5))
            //    {
            //        dt.Columns.Add("answer", typeof(String));
            //        for (int i = 0; i < Repeater1.Controls.Count; i++)
            //        {
            //            TextBox box = (TextBox)Repeater1.Controls[i].FindControl("TextBox1");
            //            RadioButton rbMyRadio = (RadioButton)Repeater1.Controls[i].FindControl("RadioButton1");

            //            DataRow dr = dt.NewRow();
            //            dr["answer"] = box.Text;
            //            dt.Rows.Add(dr);
            //        }
            //        int add_new = Convert.ToInt32(5) - (Repeater1.Controls.Count);
            //        for (int j = 0; j < add_new; j++)
            //        {
            //            DataRow dr = dt.NewRow();
            //            dr["answer"] = "";
            //            dt.Rows.Add(dr);
            //        }
            //    }
            //    else
            //    {
            //        dt.Columns.Add("answer", typeof(String));
            //        for (int i = 0; i < Repeater1.Controls.Count; i++)
            //        {
            //            TextBox box = (TextBox)Repeater1.Controls[i].FindControl("TextBox1");
            //            RadioButton rbMyRadio = (RadioButton)Repeater1.Controls[i].FindControl("RadioButton1");

            //            DataRow dr = dt.NewRow();
            //            dr["answer"] = box.Text;
            //            dt.Rows.Add(dr);
            //        }
            //        int remove_new = (Repeater1.Controls.Count) - Convert.ToInt32(5);
            //        for (int j = 0; j < remove_new; j++)
            //        {
            //            dt.Rows.RemoveAt(dt.Rows.Count - 1);
            //        }
            //    }
            //    Repeater1.DataSource = dt;
            //    Repeater1.DataBind();
            //}
        }
    }
    protected void updateCompetenceMaster()
    {
        CompetenceMasterBM obj = new CompetenceMasterBM();
        obj.comId = Convert.ToInt32(Request.QueryString["id"]);
        obj.comCompetence = txtCompetence.Text;
        obj.comCompetenceDN = txtCompetenceDN.Text;
        obj.comUpdatedDate = DateTime.Now;
        obj.comcategoryId = Convert.ToInt32(ddlcategory.SelectedValue);
        obj.UpdateCompetenceAdd();
        DeleteAnswearsbyId(Convert.ToInt32(Request.QueryString["id"]));
        insertCompetencechild(Convert.ToInt32(Request.QueryString["id"]));
        if (obj.ReturnBoolean == true)
        {
            Response.Redirect("Competence.aspx?msg=upd");
        }

        else
        {
            lblMsg.Text = CommonModule.msgProblemInsertRecord;
        }
    }
    protected void DeleteAnswearsbyId(int comId)
    {
        CompetenceMasterBM obj = new CompetenceMasterBM();
        obj.comId = comId;
        obj.DeleteCompetencechildbyId();
    }
    protected void GetAllCompetenceMasterbyid()
    {
        CompetenceMasterBM obj = new CompetenceMasterBM();
        obj.comId = Convert.ToInt32(Request.QueryString["id"]);
        obj.GetAllCompetenceMasterbyid();
        DataSet ds = obj.ds;
        if (ds != null)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comCompetence"])))
                txtCompetence.Text = Convert.ToString(ds.Tables[0].Rows[0]["comCompetence"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["comcategoryId"])))
                ddlcategory.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["comcategoryId"]);
        }

    }
    protected void GetAllcompetenceCategory()
    {
        CompetenceMasterBM obj = new CompetenceMasterBM();
        obj.catIsActive = true;
        obj.catIsDeleted = false;
        obj.catCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllcompetenceCategory();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {

                ddlcategory.DataSource = ds.Tables[0];
                ddlcategory.DataTextField = "catName";
                ddlcategory.DataValueField = "catId";
                ddlcategory.DataBind();
            }
            else
            {
                ddlcategory.Items.Clear();
                ddlcategory.Items.Insert(0, new ListItem(CommonModule.dropDownCategory, CommonModule.dropDownZeroValue));
            }

        }
        else
        {
            ddlcategory.Items.Clear();
            ddlcategory.Items.Insert(0, new ListItem(CommonModule.dropDownCategory, CommonModule.dropDownZeroValue));
        }

    }
    #endregion

    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
       
        if (!String.IsNullOrEmpty(Request.QueryString["id"]))
        {
            updateCompetenceMaster();
        }
        else
        {
            InsertCompetenceMaster();
        }
    }
    protected void btnCancel_click(object sender, EventArgs e)
    {
        Response.Redirect("Competence.aspx");
    }
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        string language = Convert.ToString(Session["Cultureadmin"]);
        //string language = "Denish";
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Denish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Languageadmin"] != null)
        {
            if (!Session["Languageadmin"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Languageadmin"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Languageadmin"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
}