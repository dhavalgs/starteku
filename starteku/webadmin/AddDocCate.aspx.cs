﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using startetku.Business.Logic;

public partial class webadmin_AddDocCate : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["UserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            ViewState["catParentId"] = "0";
            // PopulateRootLevel();
            GetAllDocCategory();
            GetAllcompetenceCategory();
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                GetAllDocCatebyid();
            }
        }
    }
    #endregion

    #region method

    protected void GetAllcompetenceCategory()
    {
        CompetenceMasterBM obj = new CompetenceMasterBM();
        obj.catIsActive = true;
        obj.catIsDeleted = false;
        obj.catCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllcompetenceCategory();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {


                chkList.DataSource = ds.Tables[0];
                chkList.DataTextField = "catName";
                chkList.DataValueField = "catId";
                chkList.DataBind();


                //chkList.Items.Insert(0, new ListItem(CommonModule.dropDownCategory, CommonModule.dropDownZeroValue));


            }
            else
            {


                chkList.Items.Clear();
                //ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownCategory, CommonModule.dropDownZeroValue));
            }


        }
        else
        {
            chkList.Items.Clear();
            //ddlDepartment.Items.Clear();
            //ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownCategory, CommonModule.dropDownZeroValue));
        }

    }
    protected void SetDefaultMessage()
    {

        string id = "?id=";
        string childurl = "child.aspx";
        if (Request.RawUrl.ToString().ToLower().Contains(childurl.ToLower()))
        {
            if (Request.RawUrl.ToString().ToLower().Contains(id.ToLower()))
            {
                lblDataDisplayTitle.Text = "Edit Children";
            }
            else
            {
                lblDataDisplayTitle.Text = "Add Children";
            }
        }
    }
    protected void GetAllDocCategory()
    {
        DivisionBM obj = new DivisionBM();
        obj.depIsActive = true;
        obj.depIsDeleted = false;
        obj.depCompanyId = 0;
        obj.GetAllDocCategory();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlDepartment.Items.Clear();

                ddlDepartment.DataSource = ds.Tables[0];
                ddlDepartment.DataTextField = "dcName";
                ddlDepartment.DataValueField = "dcCatId";
                ddlDepartment.DataBind();

                ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
            }
            else
            {
                ddlDepartment.Items.Clear();
                ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddlDepartment.Items.Clear();
            ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
        }

    }
    protected void InsertDocCat()
    {
        DivisionBM obj2 = new DivisionBM();
        obj2.DocCateCheckDuplication(txtDepartmentName.Text, -1, -1);
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        {
            DivisionBM obj = new DivisionBM();
            obj.depName = txtDepartmentName.Text;
            obj.depNameDN = txtDepartmentNameDN.Text;
            //if (Convert.ToInt32(ddlDepartment.SelectedValue) == 0)
            //{
            //    obj.depPerentId = 0;
            //}
            //else
            //{
            //    obj.depPerentId = Convert.ToInt32(ddlDepartment.SelectedValue);
            //}
            obj.depPerentId = getvalue();
            obj.depCompanyId = 0;
            obj.depIsActive = true;
            obj.depIsDeleted = false;
            obj.depCreatedDate = DateTime.Now;
            obj.depDepId = 0;
            obj.divDescription = txtDESCRIPTION.Text;
            // insert job category
            string str_clk_list_QualityArea = string.Empty;
            for (int i = 0; i < chkList.Items.Count; i++)
            {
                if (chkList.Items[i].Selected)
                {
                    if (string.IsNullOrEmpty(str_clk_list_QualityArea))
                    {
                        str_clk_list_QualityArea = chkList.Items[i].Value;
                    }
                    else
                    {
                        str_clk_list_QualityArea = str_clk_list_QualityArea + "," + chkList.Items[i].Value;
                    }
                }
            }
            //
            obj.divcatId = str_clk_list_QualityArea;
            obj.InsertDocCat();
            if (obj.ReturnBoolean == true)
            {
                Response.Redirect("DocumentCategoryList.aspx?msg=ins");
            }
        }
        else
        {
            if (returnMsg == "divName")
                lblMsg.Text = CommonModule.msgNameAlreadyExists;
            else
                lblMsg.Text = CommonModule.msgProblemInsertRecord;
        }
    }
    protected void updateDocCat()
    {
        DivisionBM obj2 = new DivisionBM();
        obj2.DivisionCheckDuplication(txtDepartmentName.Text, Convert.ToInt32(Request.QueryString["id"]), 0);
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        {
            DivisionBM obj = new DivisionBM();
            obj.depId = Convert.ToInt32(Request.QueryString["id"]);
            obj.depName = txtDepartmentName.Text;
            obj.depNameDN = txtDepartmentNameDN.Text;
            //if (Convert.ToInt32(ddlDepartment.SelectedValue) == 0)
            //{
            //    obj.depPerentId = 0;
            //}
            //else
            //{
            //    obj.depPerentId = Convert.ToInt32(ddlDepartment.SelectedValue);
            //}   
            obj.depPerentId = getvalue();
            obj.divDescription = txtDESCRIPTION.Text;
            obj.depUpdatedDate = DateTime.Now;
            // insert job category
            string str_clk_list_QualityArea = string.Empty;
            for (int i = 0; i < chkList.Items.Count; i++)
            {
                if (chkList.Items[i].Selected)
                {
                    if (string.IsNullOrEmpty(str_clk_list_QualityArea))
                    {
                        str_clk_list_QualityArea = chkList.Items[i].Value;
                    }
                    else
                    {
                        str_clk_list_QualityArea = str_clk_list_QualityArea + "," + chkList.Items[i].Value;
                    }
                }
            }
            //
            obj.divcatId = str_clk_list_QualityArea;
            obj.UpdateDocCate();
            if (obj.ReturnBoolean == true)
            {
                Response.Redirect("DocumentCategoryList.aspx?msg=upd");
            }
        }
        else
        {
            if (returnMsg == "divName")
                lblMsg.Text = CommonModule.msgNameAlreadyExists;
            else
                lblMsg.Text = CommonModule.msgProblemInsertRecord;
        }

    }
    protected void GetAllDocCatebyid()
    {
        DivisionBM obj = new DivisionBM();
        obj.depId = Convert.ToInt32(Request.QueryString["id"]);
        obj.GetAllDocCatebyid();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["dcName"])))
                txtDepartmentName.Text = Convert.ToString(ds.Tables[0].Rows[0]["dcName"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["dcNameDN"])))
                txtDepartmentNameDN.Text = Convert.ToString(ds.Tables[0].Rows[0]["dcNameDN"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["dcDescription"])))
                txtDESCRIPTION.Text = Convert.ToString(ds.Tables[0].Rows[0]["dcDescription"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["dcParentId"])))
                ddlDepartment.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["dcParentId"]);
            ViewState["catParentId"] = Convert.ToString(ds.Tables[0].Rows[0]["dcParentId"]);


            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["dcCategoryId"])))
            //var x= "1";
            {

                var catIds = Convert.ToString(ds.Tables[0].Rows[0]["dcCategoryId"]);
                string[] list = catIds.Split(Convert.ToChar(","));
                //list.AddRange(values.Split(new char[] { ',' }));


                for (var j = 0; j <= chkList.Items.Count - 1; j++)
                {
                    foreach (var i in list)
                    {
                        if (chkList.Items[j].Value == i)
                        {
                            chkList.Items[j].Selected = true;
                            //  break;
                        }
                    }
                }
            }
        }

    }
    #endregion

    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        if (txtDepartmentName.Text == "")
        {
            RequiredFieldValidator5.Visible = true;
        }

        else if (txtDepartmentNameDN.Text == "")
        {
            txtDepartmentNameDNValidator1.Visible = true;
        }

        else if (chkList.SelectedIndex == -1)
        {
            Literal6.Visible = true;
        }

        else
        {
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                updateDocCat();
            }
            else
            {
                InsertDocCat();

            }
        }
    }
    protected void btnCancel_click(object sender, EventArgs e)
    {
        Response.Redirect("DocumentCategoryList.aspx");
    }
    #endregion


    #region TreeView
    private void FillCategoryChecklist(TreeNodeCollection nodes, int id)
    {

        foreach (TreeNode child in nodes)
        {

            if (Convert.ToString(id) == Convert.ToString(child.Value))
            {
                child.Checked = true;

            }

            FillCategoryChecklist(child.ChildNodes, id);
        }


    }
    protected void TreeView_TreeNodePopulate(object sender, TreeNodeEventArgs e)
    {
        int ParentCatagoryID = Int32.Parse(e.Node.Value);
        PopulateSubLevel(ParentCatagoryID, e.Node);
        FillCategoryChecklist(TreeView1.Nodes, Convert.ToInt32(ViewState["catParentId"]));
    }
    private void PopulateSubLevel(int ParentCatagoryID, TreeNode parentNode)
    {
        //Your sublevel Datatable ie. dtSub

        DivisionBM obj = new DivisionBM();
        obj.depPerentId = ParentCatagoryID;
        obj.GetDivisionsFromParentId();
        DataSet ds = obj.ds;
        PopulateNodes(ds.Tables[0], parentNode.ChildNodes);


    }
    private void PopulateNodes(DataTable dt, TreeNodeCollection nodes)
    {
        foreach (DataRow dr in dt.Rows)
        {
            TreeNode tn = new TreeNode();
            tn.Value = dr["divId"].ToString();
            tn.Text = dr["divName"].ToString();
            nodes.Add(tn);
            //If node has child nodes, then enable on-demand populating
            tn.PopulateOnDemand = ((int)(dr["catChildCount"]) > 0);
        }
    }
    private void PopulateRootLevel()
    {
        DataTable table = new DataTable();
        DataColumn column = new DataColumn();
        column.DataType = System.Type.GetType("System.Int32");
        column.ColumnName = "divId";
        table.Columns.Add(column);

        // Create second column.
        column = new DataColumn();
        column.DataType = Type.GetType("System.String");
        column.ColumnName = "divName";
        table.Columns.Add(column);

        column = new DataColumn();
        column.DataType = Type.GetType("System.Int32");
        column.ColumnName = "catChildCount";
        table.Columns.Add(column);

        DataRow dr = table.NewRow();
        dr["divId"] = 0;
        dr["divName"] = "Root";
        dr["catChildCount"] = 10;
        table.Rows.Add(dr);
        table.AcceptChanges();


        DivisionBM obj = new DivisionBM();
        obj.depPerentId = 0;
        obj.GetDivisionsFromParentId();
        DataSet ds = obj.ds;
        PopulateNodes(table, TreeView1.Nodes);

    }
    private Int32 getCategory(TreeNodeCollection treeNode, int ret)
    {
        foreach (TreeNode child in treeNode)
        {
            if (child.Checked == true)
            {
                ret = Convert.ToInt32(child.Value);
                return ret;
            }

            ret = getCategory(child.ChildNodes, ret);
        }
        return ret;
    }

    protected Int32 getvalue()
    {

        if (TreeView1.CheckedNodes.Count > 0)
        {

            foreach (TreeNode node in TreeView1.CheckedNodes)
            {

                if (node.Parent != null)
                {
                    string checkedValue = node.Text.ToString();
                    Int32 ret = Convert.ToInt32(node.Value);
                    return ret;
                }

            }
            foreach (TreeNode node in TreeView1.Nodes)
            {
                if (node.Checked == true)
                {
                    string checkedValue = node.Text.ToString();
                    Int32 ret = Convert.ToInt32(node.Value);
                    return ret;
                }
            }
            //stablish the session variable only when the foreach has finished
            //Session["listActivity"] = listActivity;
        }
        return 0;
    }
    #endregion

    #region Culture
    protected override void InitializeCulture()
    {
        string language = Convert.ToString(Session["Cultureadmin"]);
        //string language = "Denish";
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Denish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Languageadmin"] != null)
        {
            if (!Session["Languageadmin"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Languageadmin"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Languageadmin"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
}