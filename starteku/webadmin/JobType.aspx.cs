﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using startetku.Business.Logic;
using System.Threading;
using System.Globalization;

public partial class webadmin_JobType : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Convert.ToString(Session["UserId"])))
        {
            Response.Redirect("login.aspx");
        }
        if (!IsPostBack)
        {
            GetAllJobType();
            GetAllcompetenceCategory(); 
            if (!String.IsNullOrEmpty(Request.QueryString["id"]))
            {
                GetAllJobtypebyid();
            }
        }
    }
    #endregion

    #region method

    protected void GetAllcompetenceCategory()
    {
        CompetenceMasterBM obj = new CompetenceMasterBM();
        obj.catIsActive = true;
        obj.catIsDeleted = false;
        obj.catCompanyId = Convert.ToInt32(Session["OrgCompanyId"]);
        obj.GetAllcompetenceCategory();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                

                chkList.DataSource = ds.Tables[0];
                chkList.DataTextField = "catName";
                chkList.DataValueField = "catId";
                chkList.DataBind();


                //chkList.Items.Insert(0, new ListItem(CommonModule.dropDownCategory, CommonModule.dropDownZeroValue));

                
            }
            else
            {
                

                chkList.Items.Clear();
                //ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownCategory, CommonModule.dropDownZeroValue));
            }


        }
        else
        {
            chkList.Items.Clear();
            //ddlDepartment.Items.Clear();
            //ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownCategory, CommonModule.dropDownZeroValue));
        }

    }
    protected void GetAllJobType()
    {

        return; //saurin :  currently division is not visible for jobtype. confirm with Kamlesh on 20141217
        //DivisionBM obj = new DivisionBM();
        //obj.depIsActive = true;
        //obj.depIsDeleted = false;
        //obj.depCompanyId = 0;
        //obj.GetAllDivisionsbyCompanyId();
        //DataSet ds = obj.ds;

        //if (ds != null)
        //{
        //    if (ds.Tables[0].Rows.Count > 0)
        //    {
        //        ddlDepartment.Items.Clear();

        //        ddlDepartment.DataSource = ds.Tables[0];
        //        ddlDepartment.DataTextField = "divName";
        //        ddlDepartment.DataValueField = "divId";
        //        ddlDepartment.DataBind();

        //        ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownSelectDivisions, CommonModule.dropDownZeroValue));


               
        //    }
        //    else
        //    {
        //        ddlDepartment.Items.Clear();
        //        ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownSelectDepartments, CommonModule.dropDownZeroValue));
        //    }
        //}
        //else
        //{
        //    ddlDepartment.Items.Clear();
        //    ddlDepartment.Items.Insert(0, new ListItem(CommonModule.dropDownSelectDepartments, CommonModule.dropDownZeroValue));
        //}

    }
    protected void SetDefaultMessage()
    {

        string id = "?id=";
        string childurl = "child.aspx";
        if (Request.RawUrl.ToString().ToLower().Contains(childurl.ToLower()))
        {
            if (Request.RawUrl.ToString().ToLower().Contains(id.ToLower()))
            {
                lblDataDisplayTitle.Text = "Edit Children";

            }
            else
            {
                lblDataDisplayTitle.Text = "Add Children";
            }
        }
    }
    protected void InsertJobtype()
    {
        JobTypeBM obj2 = new JobTypeBM();
        obj2.JobTypeCheckDuplication(txtJobTypeName.Text, -1,-1);
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        {
            JobTypeBM obj = new JobTypeBM();
            obj.jobName = txtJobTypeName.Text;
            obj.jobNameDN = txtJobTypeNameDn.Text;
            obj.jobCompanyId = 0;
            obj.jobIsActive = true;
            obj.jobIsDeleted = false;
            obj.jobCreatedDate = DateTime.Now;
            obj.jobDescription = txtDESCRIPTION.Text;
            obj.jobDepId = 0;// Convert.ToInt32(ddlDepartment.SelectedValue);


            // insert job category
            string str_clk_list_QualityArea = string.Empty;
            for (int i = 0; i < chkList.Items.Count; i++)
            {
                if (chkList.Items[i].Selected)
                {
                    if (string.IsNullOrEmpty(str_clk_list_QualityArea))
                    {
                        str_clk_list_QualityArea = chkList.Items[i].Value;
                    }
                    else
                    {
                        str_clk_list_QualityArea = str_clk_list_QualityArea + "," + chkList.Items[i].Value;
                    }
                }
            }
            //
            obj.jobcatid = str_clk_list_QualityArea;

            obj.InsertJobType();
            //  DataSet ds = obj.ds;
            if (obj.ReturnBoolean == true)
            {
                Response.Redirect("JobTypeList.aspx?msg=ins");
            }
        }
        else
        {
            if (returnMsg == "jobName")
                lblMsg.Text = CommonModule.msgNameAlreadyExists;
            else
                lblMsg.Text = CommonModule.msgProblemInsertRecord;

        }

    }
    protected void updateJobtype()
    {
        JobTypeBM obj2 = new JobTypeBM();
        obj2.JobTypeCheckDuplication(txtJobTypeName.Text, Convert.ToInt32(Request.QueryString["id"]), 0);
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        {
            JobTypeBM obj = new JobTypeBM();
            obj.jobId = Convert.ToInt32(Request.QueryString["id"]);
            obj.jobName = txtJobTypeName.Text;
            obj.jobNameDN = txtJobTypeNameDn.Text;
            obj.jobUpdatedDate = DateTime.Now;
            obj.jobDescription = txtDESCRIPTION.Text;
            obj.jobDepId = 0;// Convert.ToInt32(ddlDepartment.SelectedValue);
            // update job category
            string str_clk_list_QualityArea = string.Empty;
            for (int i = 0; i < chkList.Items.Count; i++)
            {
                if (chkList.Items[i].Selected)
                {
                    if (string.IsNullOrEmpty(str_clk_list_QualityArea))
                    {
                        str_clk_list_QualityArea = chkList.Items[i].Value;
                    }
                    else
                    {
                        str_clk_list_QualityArea = str_clk_list_QualityArea + "," + chkList.Items[i].Value;
                    }
                }
            }
            obj.jobcatid = str_clk_list_QualityArea;
            //
            obj.UpdateJobType();
            //  DataSet ds = obj.ds;
            if (obj.ReturnBoolean == true)
            {
                Response.Redirect("JobTypeList.aspx?msg=upd");
            }
        }
        else
        {
            if (returnMsg == "jobName")
                lblMsg.Text = CommonModule.msgNameAlreadyExists;
            else
                lblMsg.Text = CommonModule.msgProblemInsertRecord;
        }
    }
    protected void GetAllJobtypebyid()
    {
        JobTypeBM obj = new JobTypeBM();
        obj.jobId = Convert.ToInt32(Request.QueryString["id"]);
        obj.GetAllJobTypebyid();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["jobName"])))
                txtJobTypeName.Text = Convert.ToString(ds.Tables[0].Rows[0]["jobName"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["jobNameDN"])))
                txtJobTypeNameDn.Text = Convert.ToString(ds.Tables[0].Rows[0]["jobNameDN"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["jobDescription"])))
                txtDESCRIPTION.Text = Convert.ToString(ds.Tables[0].Rows[0]["jobDescription"]);

            //if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["jobDepId"])))
            //   ddlDepartment.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["jobDepId"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["jobcatId"])))
            //var x= "1";
            {

                var catIds = Convert.ToString(ds.Tables[0].Rows[0]["jobcatId"]);
                string[] list = catIds.Split(Convert.ToChar(","));
                //list.AddRange(values.Split(new char[] { ',' }));


                for (var j = 0; j <= chkList.Items.Count - 1; j++)
                {
                    foreach (var i in list)
                    {
                        if (chkList.Items[j].Value.Contains(i))
                        {
                            chkList.Items[j].Selected = true;
                            //  break;
                        }
                    }
                }
            }
        }

    }

    #endregion


    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Request.QueryString["id"]))
        {
            updateJobtype();
        }
        else
        {
            InsertJobtype();
        }
    }
    protected void btnCancel_click(object sender, EventArgs e)
    {
        Response.Redirect("JobTypeList.aspx");
    }
    #endregion


    #region Culture
    protected override void InitializeCulture()
    {
        string language = Convert.ToString(Session["Cultureadmin"]);
        //string language = "Denish";
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Denish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Languageadmin"] != null)
        {
            if (!Session["Languageadmin"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Languageadmin"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Languageadmin"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
}