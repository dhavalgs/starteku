﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using starteku_BusinessLogic;
using System.Data;
using System.IO;
using System.Threading;
using System.Globalization;

public partial class webadmin_Profile : System.Web.UI.Page
{
    #region Page Event
    protected void Page_Load(object sender, EventArgs e)
    {
       // CalendarExtender2.EndDate = DateTime.Now;   //to dissable future  Date      
        //UserId
        int UserId = Convert.ToInt32(Session["UserId"]);
        if (UserId == 0)
            Response.Redirect("Login.aspx");

        if (!IsPostBack)
        {
            GetAllCountry();
            state.Attributes.Add("style", "display:block");
            //city.Attributes.Add("style", "display:block");
            ResourceLanguageBM obj = new ResourceLanguageBM();
            DataSet resds = obj.GetAllResourceLanguage();
            ddllanguages.DataSource = resds;
            ddllanguages.DataTextField = "resLanguage";
            ddllanguages.DataValueField = "resCulture";
            ddllanguages.DataBind();

            GetAllEmployeebyid();
            
        }
    }
    #endregion

    #region method
    protected void SetDefaultMessage()
    {
        string id = "?id=";
        string childurl = "child.aspx";
        if (Request.RawUrl.ToString().ToLower().Contains(childurl.ToLower()))
        {
            if (Request.RawUrl.ToString().ToLower().Contains(id.ToLower()))
            {
                lblDataDisplayTitle.Text = "Edit Children";

            }
            else
            {
                lblDataDisplayTitle.Text = "Add Children";
            }
        }
    }
    protected void GetAllCountry()
    {
        CountryBM obj = new CountryBM();
        obj.couIsActive = true;
        obj.couIsDeleted = false;
        //obj.GetAllCountry();
        obj.GetAllCountrymaster();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlcountry.Items.Clear();
                ddlcountry.DataSource = ds.Tables[0];
                ddlcountry.DataTextField = "couName";
                ddlcountry.DataValueField = "couId";
                ddlcountry.DataBind();
                ddlcountry.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
            }
            else
            {
                ddlcountry.Items.Clear();
                ddlcountry.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddlcountry.Items.Clear();
            ddlcountry.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
        }


    }
    protected void GetState(int contryid)
    {
        state.Attributes.Add("style", "display:block");

        StateBM obj = new StateBM();
        obj.staCountryId = contryid;
        obj.staIsActive = true;
        obj.staIsDeleted = false;
        //obj.GetStatebyCountryid();
        obj.GetStatebyCountryidMaster();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlstate.Items.Clear();
                ddlstate.DataSource = ds.Tables[0];
                ddlstate.DataTextField = "staName";
                ddlstate.DataValueField = "staId";
                ddlstate.DataBind();

                //  ddlstate.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
            }
            else
            {
                ddlstate.Items.Clear();
                ddlstate.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
            }
        }
        else
        {
            ddlstate.Items.Clear();
            ddlstate.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
        }
        //Getcity(Convert.ToInt32(ddlstate.SelectedValue));
    }
    //protected void Getcity(int stateid)
    //{
    //    city.Attributes.Add("style", "display:block");

    //    CityBM obj = new CityBM();
    //    obj.citStateId = stateid;
    //    obj.citIsActive = true;
    //    obj.citIsDeleted = false;
    //    obj.Getcitybystateid();
    //    DataSet ds = obj.ds;

    //    if (ds != null)
    //    {
    //        if (ds.Tables[0].Rows.Count > 0)
    //        {
    //            ddlcity.Items.Clear();
    //            ddlcity.DataSource = ds.Tables[0];
    //            ddlcity.DataTextField = "citName";
    //            ddlcity.DataValueField = "citId";
    //            ddlcity.DataBind();

    //            //  ddlstate.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
    //        }
    //        else
    //        {
    //            ddlcity.Items.Clear();
    //            ddlcity.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
    //        }
    //    }
    //    else
    //    {
    //        ddlcity.Items.Clear();
    //        ddlcity.Items.Insert(0, new ListItem(CommonModule.dropDownSelect, CommonModule.dropDownZeroValue));
    //    }

    //}
    protected void redirectpage(string msg, string paage)
    {
        //string message = "You will now be redirected to ASPSnippets Home Page.";
        string message = msg;
        //string url = "Login.aspx?msg=ins";
        string url = paage;
        string script = "window.onload = function(){ alert('";
        script += message;
        script += "');";
        script += "window.location = '";
        script += url;
        script += "'; }";
        ClientScript.RegisterStartupScript(this.GetType(), "Redirect", script, true);

    }
    protected void UpdateUser(int UserId)
    {
        string filePath = "";
        UserBM obj2 = new UserBM();
        obj2.useremailCheckDuplication(txtEmail.Text, Convert.ToInt32(UserId));
        String returnMsg = obj2.ReturnString;
        if (returnMsg == "")
        {
            UserBM obj = new UserBM();
            obj.userId = Convert.ToInt32(UserId);
            obj.userFirstName = txtfName.Text;
            obj.userLastName = txtlname.Text;
            obj.userZip = txtzip.Text;
            obj.userAddress = txtaddress.Text;
            obj.userCountryId = Convert.ToInt32(ddlcountry.SelectedValue);
            obj.userStateId = Convert.ToInt32(ddlstate.SelectedValue);
            obj.userCityId = txtcity.Text;
            obj.usercontact = txtMobile.Text;
            obj.userEmail = txtEmail.Text;
            //obj.userCityId = Convert.ToInt32(ddlcity.SelectedValue);
            obj.userUpdatedDate = DateTime.Now;
            obj.userPassword = CommonModule.encrypt(txtPassword.Text.Trim());

            if (txtfromdate.Text != "")
            {
                //obj.userDOB = Convert.ToDateTime(CommonModule.FormatdtEnter(txtfromdate.Text));
                try
                {
                    obj.userDOB = DateTime.ParseExact(txtfromdate.Text, "dd/MM/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None);
                }
                catch (Exception ex)
                {
                    obj.userDOB = Convert.ToDateTime(txtfromdate.Text);
                }
            }
            obj.userGender = ddlgender.SelectedValue;
            if (flupload1.HasFile)
            {
                filePath = System.DateTime.Now.ToString("MMddyyhhmmss") + "_" + flupload1.PostedFile.FileName;
                flupload1.SaveAs(Server.MapPath("../") + "Log/upload/Userimage/" + System.IO.Path.GetFileName(filePath.Replace(' ', '0')));
                obj.userImage = System.IO.Path.GetFileName(filePath.Replace(' ', '0'));
            }
            else
            {
                obj.userImage = Convert.ToString(ViewState["userImage"]);

            }
            obj.userNumber = "";
            obj.UpdateUsernew();

            #region thumbnail
            if (File.Exists(MapPath("../") + "Log/Upload/Userimage/" + System.IO.Path.GetFileName(filePath.Replace(' ', '0'))))
            {
                System.Drawing.Image img = System.Drawing.Image.FromFile(Server.MapPath("../") + "Log/Upload/Userimage/" + System.IO.Path.GetFileName(filePath.Replace(' ', '0')));
                System.Drawing.Image singleProduct = img.GetThumbnailImage(100, 100, null, IntPtr.Zero);               
                img.Dispose();
                singleProduct.Save(Server.MapPath("../") + "Log/Upload/thumbnail/" + System.IO.Path.GetFileName(filePath.Replace(' ', '0')));
                

            }
            #endregion


            if (obj.ReturnBoolean == true)
            {
                Response.Redirect("dashboard.aspx?msg=upd");
            }
        }
        else
        {
            if (returnMsg == "userEmail")
                lblMsg.Text = CommonModule.msgEmailAlreadyExists;
            else
                lblMsg.Text = CommonModule.msgProblemInsertRecord;
        }
    }
    protected void GetAllEmployeebyid()
    {
        UserBM obj = new UserBM();
        obj.userId = Convert.ToInt32(Session["UserId"]);
        obj.GetAllEmployeebyid();
        DataSet ds = obj.ds;

        if (ds != null)
        {
            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userFirstName"])))
                txtfName.Text = Convert.ToString(ds.Tables[0].Rows[0]["userFirstName"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userLastName"])))
                txtlname.Text = Convert.ToString(ds.Tables[0].Rows[0]["userLastName"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userZip"])))
                txtzip.Text = Convert.ToString(ds.Tables[0].Rows[0]["userZip"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userAddress"])))
                txtaddress.Text = Convert.ToString(ds.Tables[0].Rows[0]["userAddress"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userCountryId"])))
                ddlcountry.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["userCountryId"]);

               GetState(Convert.ToInt32(ddlcountry.SelectedValue));

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userStateId"])))
                ddlstate.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["userStateId"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userCityId"])))
                //ddlcity.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["userCityId"]);
               txtcity.Text = Convert.ToString(ds.Tables[0].Rows[0]["userCityId"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["usercontact"])))
                txtMobile.Text = Convert.ToString(ds.Tables[0].Rows[0]["usercontact"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userEmail"])))
                txtEmail.Text = Convert.ToString(ds.Tables[0].Rows[0]["userEmail"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userDOB"])))
                txtfromdate.Text = Convert.ToString(ds.Tables[0].Rows[0]["userDOB"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userPassword"])))
            {
                string Password = CommonModule.decrypt(Convert.ToString(ds.Tables[0].Rows[0]["userPassword"]));
                txtPassword.Attributes.Add("value", Password);
                txtConfirmPassword.Attributes.Add("value", Password);
            }

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userGender"])))
                ddlgender.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["userGender"]);

            if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["userImage"])))
                ViewState["userImage"] = Convert.ToString(ds.Tables[0].Rows[0]["userImage"]);
               img.ImageUrl = "~/Log/upload/thumbnail/" + Convert.ToString(ds.Tables[0].Rows[0]["userImage"]);

               if (!string.IsNullOrEmpty(Convert.ToString(ds.Tables[0].Rows[0]["languageName"])))
                   ddllanguages.SelectedValue = Convert.ToString(ds.Tables[0].Rows[0]["languageName"]);
               


        }

    }
    protected void Updatelanguagemaster(int OrgUserId)
    {
        UserBM obj = new UserBM();
        obj.languageUserId = Convert.ToInt32(OrgUserId);
        String lag = Convert.ToString(ddllanguages.SelectedValue);

        obj.languageName = ddllanguages.SelectedItem.Text;
        obj.languageCulture = ddllanguages.SelectedItem.Value;
        Session["Cultureadmin"] = ddllanguages.SelectedItem.Text;
        //if (lag == "English")
        //{
        //    obj.languageName = "English";
        //    obj.languageCulture = "en-GB";
        //    Session["Culture"] = "English";
        //}
        //else
        //{
        //    obj.languageName = "Denish";
        //    obj.languageCulture = "da-DK";
        //    Session["Culture"] = "Denish";
        //}
        //obj.userGender = ddlgender.SelectedValue;
        obj.Updatelanguagemaster();

    }
    #endregion

    #region Button Event
    protected void btnsubmit_click(object sender, EventArgs e)
    {
        int UserId = Convert.ToInt32(Session["UserId"]);
        if (UserId != 0)
        {
            Updatelanguagemaster(UserId);
            UpdateUser(UserId);
          
        }
    }
    protected void btnCancel_click(object sender, EventArgs e)
    {
        Response.Redirect("dashboard.aspx");
    }
    #endregion

    #region SelectedIndexChanged
    protected void ddlcountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        GetState(Convert.ToInt32(ddlcountry.SelectedValue));
    }
    protected void ddlstate_SelectedIndexChanged(object sender, EventArgs e)
    {
        //Getcity(Convert.ToInt32(ddlstate.SelectedValue));
    }
    #endregion
    
    #region Culture
    protected override void InitializeCulture()
    {
        string language = Convert.ToString(Session["Cultureadmin"]);
        //string language = "Denish";
        string languageId = "";
        //if (!string.IsNullOrEmpty(language))
        //{
        //    if (language.EndsWith("Denish")) languageId = "da-DK";
        //    else languageId = "en-GB";
        //    SetCulture(languageId);
        //}
        //ResourceLanguageBM obj = new ResourceLanguageBM();
        //DataSet resds = obj.GetAllResourceLanguage();
        //if (!string.IsNullOrEmpty(language))
        //{
        //    for (int i = 0; i < resds.Tables[0].Rows.Count; i++)
        //    {
        //        if (language.EndsWith(resds.Tables[0].Rows[i]["resLanguage"].ToString()))
        //        {
        //            languageId = resds.Tables[0].Rows[i]["resCulture"].ToString();
        //        }
        //    }

        //    SetCulture(languageId);
        //}
        ResourceLanguageBM obj = new ResourceLanguageBM();
        if (!string.IsNullOrEmpty(language))
        {
            DataSet resds = obj.GetResourceLanguage(language);
            languageId = resds.Tables[0].Rows[0]["resCulture"].ToString();

            SetCulture(languageId);
        }


        if (Session["Languageadmin"] != null)
        {
            if (!Session["Languageadmin"].ToString().StartsWith(Thread.CurrentThread.CurrentCulture.TwoLetterISOLanguageName)) SetCulture(Session["Languageadmin"].ToString());
        }

        base.InitializeCulture();
    }
    protected void SetCulture(string languageId)
    {
        Session["Languageadmin"] = languageId;
        Thread.CurrentThread.CurrentCulture = CultureInfo.CreateSpecificCulture(languageId);
        Thread.CurrentThread.CurrentUICulture = new CultureInfo(languageId);
    }
    #endregion
}