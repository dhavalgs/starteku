﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Reflection;
using System.Resources;
using System.IO;
using System.Xml;
using System.Collections;

public partial class Default2 : System.Web.UI.Page
{
    public string filename;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            filename = Request.QueryString["file"];
            filename = Request.PhysicalApplicationPath + "App_GlobalResources\\" + filename;
            string key = Request.QueryString["key"];
            Label1.Text = key;
            ResXResourceSet rset = new ResXResourceSet(filename);
            txtResourceValue.Text = rset.GetString(key);
        }

    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        filename = Request.QueryString["file"];
        int id = Convert.ToInt32(Request.QueryString["id"]);
        filename = Request.PhysicalApplicationPath + "App_GlobalResources\\" + filename;
        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(filename);
        XmlNodeList nlist = xmlDoc.GetElementsByTagName("data");
        XmlNode childnode = nlist.Item(id);
        childnode.Attributes["xml:space"].Value = "default";
        xmlDoc.Save(filename);
        XmlNode lastnode = childnode.SelectSingleNode("value");
        lastnode.InnerText = txtResourceValue.Text;
        xmlDoc.Save(filename);
        Label2.Text = "Resource File Updated...";

        //String filename = Request.QueryString["file"];
        // ResXResourceWriter rsxw = new ResXResourceWriter(path);
        // bool added = false;
        // if (File.Exists(path))
        // {
        //     ResXResourceReader reader = new ResXResourceReader(path);
        //     foreach (DictionaryEntry node in reader)
        //     {
        //         if (key == node.Key.ToString())
        //         {
        //             rsxw.AddResource(key, value);
        //             added = true;
        //         }
        //         else rsxw.AddResource(node.Key.ToString(), node.Value);
        //     }
        // }
        // if (!added) rsxw.AddResource(key, value);
        // rsxw.Close();

    }
}